server {
        listen 80;
        listen [::]:80;

        server_name csgofacts.com;
        return 301 https://$server_name$request_uri;
}

server {
	client_max_body_size 300M;
	
        server_name csgofacts.com;
	
	listen 443 ssl http2;
	listen [::]:443 ssl http2;


        ssl_certificate /etc/letsencrypt/live/csgofacts.com/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/csgofacts.com/privkey.pem;

    	# modern configuration
    	ssl_protocols TLSv1.3;
    	ssl_prefer_server_ciphers off;
    	
	# HSTS (ngx_http_headers_module is required) (63072000 seconds)
	add_header Strict-Transport-Security "max-age=63072000" always;
        
	root /home/csgo/build;
        index index.html;

	location /api/ {
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
		proxy_pass http://localhost:8081/api/;
	}

        location / {
            try_files $uri /index.html;
        }
}