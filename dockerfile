FROM golang:1.15

RUN apt-get update
RUN apt-get -y install autoconf automake libtool curl make g++ unzip
RUN wget https://github.com/google/protobuf/releases/download/v2.5.0/protobuf-2.5.0.tar.gz
RUN tar xvf protobuf-2.5.0.tar.gz
WORKDIR "/go/protobuf-2.5.0"
RUN ./autogen.sh
RUN ./configure --prefix=/usr
RUN make
RUN make install
WORKDIR "/go"

VOLUME ["`pwd`/CSGO-demo-analyzer/"]