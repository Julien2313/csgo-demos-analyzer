package model

// TeamStats simple type storing team info from the demo
type TeamStats struct {
	ID *uint `gorm:"primaryKey"`

	DemoID *int64 `gorm:"not null"`
	Demo   *Demo

	DuelsTeam    []*DuelsTeam   `gorm:"constraint:OnDelete:CASCADE"`
	PlayersStats []*PlayerStats `gorm:"constraint:OnDelete:CASCADE"`

	Score *int `gorm:"not null"`
}
