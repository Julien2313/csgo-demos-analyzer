package model_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestUser_GenerateRandomMatrixKills(t *testing.T) {
	assert.Len(t, model.GenerateRandomMatrixKills(model.GenerateRandomTeamsStats(helpers.Int64Ptr(0), "0")), 10)
}

func TestUser_GenerateRandomMatrixFlashs(t *testing.T) {
	assert.Len(t, model.GenerateRandomMatrixFlashs(model.GenerateRandomTeamsStats(helpers.Int64Ptr(0), "0")), 10)
}
