package model

import (
	"math/rand"
	"strconv"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

// GenerateRandomPlayerStats generate a random player
func GenerateRandomPlayerStats(demoID *int64, steamID *string) *PlayerStats {
	const sizeUsername = 15
	if steamID == nil {
		steamID = helpers.StrPtr(strconv.FormatUint(rand.Uint64(), 10))
	}
	color := common.Color(rand.Int() % 5)
	return &PlayerStats{
		SteamID:        steamID,
		Username:       helpers.StrPtr(helpers.RandomString(sizeUsername)),
		ScoreFacts:     GenerateRandomScoreFacts(*steamID),
		Color:          &color,
		MVPsFacts:      helpers.IntPtr(rand.Int() % 10),
		Rank:           helpers.IntPtr(rand.Int() % 19),
		Kills:          helpers.IntPtr(rand.Int() % 50),
		Deaths:         helpers.IntPtr(rand.Int() % 31),
		Assists:        helpers.IntPtr(rand.Int() % 50),
		Marks:          GenerateRandomMarks(demoID),
		DuelsPlayer:    GenerateRandomDuelsPlayers(),
		WeaponPatterns: GenerateRandomsWeaponPatterns(),
	}
}
