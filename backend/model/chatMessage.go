package model

import (
	"math/rand"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

// ChatMessage representes
type ChatMessage struct {
	ID *uint `gorm:"primaryKey"`

	RoundID *int64 `gorm:"not null"`
	Round   *Round `gorm:"foreignKey:RoundID"`

	Sender     *string `gorm:"not null"`
	SideSender *int    `gorm:"not null"`
	Message    *string `gorm:"not null"`
	IsChatAll  *bool   `gorm:"not null"`
}

// GenerateRandomChatMessage generate a random ChatMessage
func GenerateRandomChatMessage() *ChatMessage {
	return &ChatMessage{
		Sender:     helpers.StrPtr(helpers.RandomString(5)),
		SideSender: helpers.IntPtr(rand.Int()%2 + 2),
		Message:    helpers.StrPtr(helpers.RandomString(50)),
		IsChatAll:  helpers.BoolPtr((rand.Int() % 2) == 0),
	}
}

// GenerateRandomChatMessages generate randoms ChatMessages
func GenerateRandomChatMessages(nbrMessage int) []*ChatMessage {
	chatMessages := []*ChatMessage{}

	for numMessage := 0; numMessage < nbrMessage; numMessage++ {
		chatMessages = append(chatMessages, GenerateRandomChatMessage())
	}

	return chatMessages
}
