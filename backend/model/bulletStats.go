package model

import (
	"math/rand"

	"github.com/google/uuid"
)

// BulletStats type with stats about bullet
type BulletStats struct {
	UUID uuid.UUID `gorm:"primaryKey;default:uuid_generate_v4();index"`

	WeaponPatternUUID *uuid.UUID     `gorm:"not null"`
	WeaponPattern     *WeaponPattern `gorm:"foreignKey:WeaponPatternUUID"`

	BulletNumber     int     `gorm:"not null"`
	NbrBulletFired   int64   `gorm:"not null"`
	NbrBulletHit     int64   `gorm:"not null"`
	NbrBulletHitHS   int64   `gorm:"not null"`
	NbrBulletKill    int64   `gorm:"not null"`
	CumulativeDeltaX float64 `gorm:"not null"`
	CumulativeDeltaY float64 `gorm:"not null"`
}

// GenerateRandomsBulletStats generate random BulletStats
func GenerateRandomsBulletStats(numBullet int) *BulletStats {
	return &BulletStats{
		BulletNumber:     numBullet,
		NbrBulletFired:   rand.Int63n(1000),
		NbrBulletHit:     rand.Int63n(1000),
		NbrBulletHitHS:   rand.Int63n(1000),
		NbrBulletKill:    rand.Int63n(1000),
		CumulativeDeltaX: rand.Float64() * 1000,
		CumulativeDeltaY: rand.Float64() * 1000,
	}
}

// GenerateRandomsBulletsStats generate randoms BulletStats
func GenerateRandomsBulletsStats() []*BulletStats {
	var bulletsStats []*BulletStats

	for numBulletsStats := 0; numBulletsStats < 7; numBulletsStats++ {
		bulletsStats = append(bulletsStats, GenerateRandomsBulletStats(numBulletsStats))
	}

	return bulletsStats
}
