package model_test

import (
	"testing"

	"github.com/maxatome/go-testdeep/td"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestUser_StoreGeneratedUser(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	userFromDB := model.User{}
	user := model.StoreGeneratedUser(localDB)
	require.NoError(t, localDB.Where("id = ?", *user.ID).First(&userFromDB).Error)

	userFromDB.CreatedAt = nil
	userFromDB.UpdatedAt = nil
	userFromDB.APIValveWorking = nil
	require.NoError(t, helpers.CheckHashPassword(*userFromDB.Password, *user.Password))

	user.Password = nil
	userFromDB.Password = nil

	td.Cmp(t, userFromDB, *user)
}
