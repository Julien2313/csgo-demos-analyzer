package model

import (
	"fmt"
	"math/rand"
	"strconv"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gorm.io/gorm"
)

// StoreGeneratedUser store a generated user
func StoreGeneratedUser(db *gorm.DB) *User {
	user := GenerateRandomUser(false)

	userToStore := &User{
		Mail:               helpers.StrPtr(*user.Mail),
		Password:           helpers.StrPtr(*user.Password),
		SteamID:            helpers.StrPtr(*user.SteamID),
		SizeUsed:           helpers.FloatPtr(*user.SizeUsed),
		ShareCode:          helpers.StrPtr(*user.ShareCode),
		AccessTokenHistory: helpers.StrPtr(*user.AccessTokenHistory),
	}

	if err := userToStore.HashPassword(); err != nil {
		panic(err)
	}

	if err := db.Create(userToStore).Error; err != nil {
		panic(err)
	}

	user.ID = userToStore.ID

	return user
}

func generateRandomMail() *string {
	const (
		sizeLocal  = 10
		sizeDomain = 10
		sizeExt    = 10
	)

	str1 := helpers.RandomString(sizeLocal)
	str2 := helpers.RandomString(sizeDomain)
	str3 := helpers.RandomString(sizeExt)

	return helpers.StrPtr(fmt.Sprintf("%s@%s.%s", str1, str2, str3))
}

// GenerateRandomSteamIDStr generates random steamID
func GenerateRandomSteamIDStr() *string {
	return helpers.StrPtr(strconv.FormatUint(rand.Uint64(), 10))
}

// GenerateRandomShareCode generates a random share code
func GenerateRandomShareCode() *string {
	const sizeUndercode = 5
	sharecode := fmt.Sprintf("CSGO-%s-%s-%s-%s-%s",
		helpers.RandomString(sizeUndercode),
		helpers.RandomString(sizeUndercode),
		helpers.RandomString(sizeUndercode),
		helpers.RandomString(sizeUndercode),
		helpers.RandomString(sizeUndercode))

	return &sharecode
}

// GenerateRandomAccessTokenHistory generates a random access token history
func GenerateRandomAccessTokenHistory() *string {
	sharecode := fmt.Sprintf("%s-%s-%s",
		helpers.RandomString(4),
		helpers.RandomString(5),
		helpers.RandomString(4))

	return &sharecode
}

func generateRandomPassword(hashed bool) *string {
	const sizePass = 10
	str := helpers.RandomString(sizePass)

	if hashed {
		var err error
		str, err = helpers.HashPassword(str)

		if err != nil {
			panic(err)
		}
	}

	return &str
}

func generateRandomSize() *float64 {
	size := rand.Float64()*20 + 80
	sizeRounded := helpers.Round(size, 4)

	return &sizeRounded
}

// GenerateRandomCredentials generate a random mail and pass in an user
func GenerateRandomCredentials(hashed bool) *User {
	return &User{
		Mail:     generateRandomMail(),
		Password: generateRandomPassword(hashed),
	}
}

// GenerateRandomUser generate a random user
func GenerateRandomUser(hashed bool) *User {
	user := GenerateRandomCredentials(hashed)
	return &User{
		Mail:               user.Mail,
		Password:           user.Password,
		SteamID:            GenerateRandomSteamIDStr(),
		ShareCode:          GenerateRandomShareCode(),
		AccessTokenHistory: GenerateRandomAccessTokenHistory(),
		SizeUsed:           generateRandomSize(),
	}
}
