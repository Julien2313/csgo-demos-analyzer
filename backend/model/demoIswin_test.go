package model

import (
	"math/rand"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

func TestDemo_IsWin(t *testing.T) {
	user := GenerateRandomUser(true)
	user.ID = helpers.UintPtr(50)

	demo := GenerateRandomDemo(user, *user.SteamID)
	demo.VersionAnalyzer = helpers.Int64Ptr(rand.Int63())

	demo.TeamsStats = []*TeamStats{{Score: helpers.IntPtr(0)}, {Score: helpers.IntPtr(0)}}

	demo.TeamsStats[0].Score = helpers.IntPtr(16)
	demo.TeamsStats[1].Score = helpers.IntPtr(14)

	for numTeam, team := range demo.TeamsStats {
		for _, player := range team.PlayersStats {
			if numTeam == 0 {
				assert.Equal(t, Won, demo.IsWin(player.SteamID))
			} else {
				assert.Equal(t, Lost, demo.IsWin(player.SteamID))
			}
		}
	}

	demo.TeamsStats[0].Score = helpers.IntPtr(2)
	demo.TeamsStats[1].Score = helpers.IntPtr(16)

	for numTeam, team := range demo.TeamsStats {
		for _, player := range team.PlayersStats {
			if numTeam == 1 {
				assert.Equal(t, Won, demo.IsWin(player.SteamID))
			} else {
				assert.Equal(t, Lost, demo.IsWin(player.SteamID))
			}
		}
	}

	demo.TeamsStats[0].Score = helpers.IntPtr(15)
	demo.TeamsStats[1].Score = helpers.IntPtr(15)

	for _, team := range demo.TeamsStats {
		for _, player := range team.PlayersStats {
			assert.Equal(t, Draw, demo.IsWin(player.SteamID))
		}
	}

	assert.Equal(t, Undefined, demo.IsWin(helpers.StrPtr("468446654")))
}
