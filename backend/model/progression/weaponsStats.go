package progression

import (
	"time"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

type WeaponsPattern []model.Demo

type WeaponsStatsTimeFrameReturn struct {
	TimeFrame          TimeFrame                                    `json:"-"`
	TimeFrameStr       string                                       `json:"timeFrame"`
	Index              int                                          `json:"index"`
	WeaponStats        WeaponsStatsReturn                           `json:"weaponStats"`
	WeaponTypeStats    map[common.EquipmentType]*WeaponsStatsReturn `json:"weaponTypeStats"`
	WeaponstatsPerSide map[int]*WeaponsStatsReturn                  `json:"weaponstatsPerSide"`

	weaponsStats         map[int64]map[common.EquipmentType]*WeaponsStatsReturn
	generalsStats        map[int64]*WeaponsStatsReturn
	generalsStatsPerSide map[int64]map[int]*WeaponsStatsReturn
}

// WeaponsStatsReturn stats for a weapon
type WeaponsStatsReturn struct {
	WeaponType  common.EquipmentType  `json:"weaponType"`
	WeaponClass common.EquipmentClass `json:"weaponClass"`
	WeaponName  string                `json:"weaponName"`

	FirstBulletAccuracy DatasetsReturn `json:"firstBulletAccuracy"`
	FirstBulletHS       DatasetsReturn `json:"firstBulletHS"`
	BulletAccuracy      DatasetsReturn `json:"bulletAccuracy"`
	HSPercentage        DatasetsReturn `json:"hSPercentage"`
	NbrBulletsFired     DatasetsReturn `json:"nbrBulletsFired"`

	AverageVelocityAttacker DatasetsReturn `json:"averageVelocityAttacker"`

	NbrBulletFired int `json:"-"`
	NbrBulletHit   int `json:"-"`
	NbrBulletHS    int `json:"-"`

	NbrFirstBulletFired int `json:"-"`
	NbrFirstBulletHit   int `json:"-"`
	NbrFirstBulletHS    int `json:"-"`

	CumulativeVelocityAttacker float64 `json:"-"`
	NbrGame                    int     `json:"-"`
}

// GetProgression returns the progression of the demos
func (wp WeaponsPattern) GetProgression(
	weaponsStatsTimeFramesReturn []*WeaponsStatsTimeFrameReturn,
	steamID string,
) error {
	for _, weaponsStatsTimeFrame := range weaponsStatsTimeFramesReturn {
		weaponsStatsTimeFrame.WeaponTypeStats = make(map[common.EquipmentType]*WeaponsStatsReturn)
		weaponsStatsTimeFrame.weaponsStats = make(map[int64]map[common.EquipmentType]*WeaponsStatsReturn)
		weaponsStatsTimeFrame.generalsStats = make(map[int64]*WeaponsStatsReturn)
		weaponsStatsTimeFrame.generalsStatsPerSide = make(map[int64]map[int]*WeaponsStatsReturn)
		weaponsStatsTimeFrame.WeaponstatsPerSide = make(map[int]*WeaponsStatsReturn)
		weaponsStatsTimeFrame.TimeFrameStr = weaponsStatsTimeFrame.TimeFrame.String()

		for _, demo := range wp {
			for _, team := range demo.TeamsStats {
				for _, player := range team.PlayersStats {
					if *player.SteamID != steamID {
						// should not append due to the preload condition
						continue
					}
					date := demo.Date

					dateModulo := weaponsStatsTimeFrame.TimeFrame.GetUnixTimeFrame(date)
					if weaponsStatsTimeFrame.weaponsStats[dateModulo] == nil {
						weaponsStatsTimeFrame.weaponsStats[dateModulo] = make(map[common.EquipmentType]*WeaponsStatsReturn)
						weaponsStatsTimeFrame.generalsStats[dateModulo] = &WeaponsStatsReturn{}
						weaponsStatsTimeFrame.generalsStatsPerSide[dateModulo] = make(map[int]*WeaponsStatsReturn)
					}

					weaponsStatsTimeFrame.generalsStats[dateModulo].NbrGame++
					weaponsStatsTimeFrame.generalsStats[dateModulo].CumulativeVelocityAttacker +=
						float64(*player.Marks.AverageVelocityShoots * *player.Marks.NbrBulletsFired)
					weaponsStatsTimeFrame.generalsStats[dateModulo].NbrBulletFired += *player.Marks.NbrBulletsFired
					weaponsStatsTimeFrame.generalsStats[dateModulo].NbrBulletHit += *player.Marks.NbrBulletsHit
					weaponsStatsTimeFrame.generalsStats[dateModulo].NbrBulletHS += *player.Marks.NbrBulletsHS
					weaponsStatsTimeFrame.generalsStats[dateModulo].NbrFirstBulletFired += *player.Marks.NbrFirstBulletsFired
					weaponsStatsTimeFrame.generalsStats[dateModulo].NbrFirstBulletHit += *player.Marks.NbrFirstBulletsHit
					weaponsStatsTimeFrame.generalsStats[dateModulo].NbrFirstBulletHS += *player.Marks.NbrFirstBulletsHS

					for _, round := range demo.Rounds {
						for _, marks := range round.MarksPerRound {
							if *marks.SteamID != steamID {
								continue
							}
							side := *marks.Side
							if weaponsStatsTimeFrame.generalsStatsPerSide[dateModulo][side] == nil {
								weaponsStatsTimeFrame.generalsStatsPerSide[dateModulo][side] = &WeaponsStatsReturn{}
							}
							weaponsStatsTimeFrame.generalsStatsPerSide[dateModulo][side].NbrGame++
							weaponsStatsTimeFrame.generalsStatsPerSide[dateModulo][side].CumulativeVelocityAttacker +=
								float64(*marks.AverageVelocityShoots * *marks.NbrBulletsFired)

							weaponsStatsTimeFrame.generalsStatsPerSide[dateModulo][side].NbrBulletFired += *marks.NbrBulletsFired
							weaponsStatsTimeFrame.generalsStatsPerSide[dateModulo][side].NbrBulletHit += *marks.NbrBulletsHit
							weaponsStatsTimeFrame.generalsStatsPerSide[dateModulo][side].NbrBulletHS += *marks.NbrBulletsHS
							weaponsStatsTimeFrame.generalsStatsPerSide[dateModulo][side].NbrFirstBulletFired += *marks.NbrFirstBulletsFired
							weaponsStatsTimeFrame.generalsStatsPerSide[dateModulo][side].NbrFirstBulletHit += *marks.NbrFirstBulletsHit
							weaponsStatsTimeFrame.generalsStatsPerSide[dateModulo][side].NbrFirstBulletHS += *marks.NbrFirstBulletsHS

							break
						}
					}

					for _, weapon := range player.Marks.WeaponsMarks {
						if weaponsStatsTimeFrame.weaponsStats[dateModulo][*weapon.WeaponType] == nil {
							weaponsStatsTimeFrame.weaponsStats[dateModulo][*weapon.WeaponType] = &WeaponsStatsReturn{}
						}

						weaponStatstimeFrame := weaponsStatsTimeFrame.weaponsStats[dateModulo][*weapon.WeaponType]
						weaponStatstimeFrame.NbrGame++
						weaponStatstimeFrame.CumulativeVelocityAttacker +=
							float64(*weapon.AverageVelocityShoots * *player.Marks.NbrBulletsFired)
						weaponStatstimeFrame.NbrBulletFired += *weapon.NbrBulletsFired
						weaponStatstimeFrame.NbrBulletHit += *weapon.NbrBulletsHit
						weaponStatstimeFrame.NbrBulletHS += *weapon.NbrBulletsHS
						weaponStatstimeFrame.NbrFirstBulletFired += *weapon.NbrFirstBulletsFired
						weaponStatstimeFrame.NbrFirstBulletHit += *weapon.NbrFirstBulletsHit
						weaponStatstimeFrame.NbrFirstBulletHS += *weapon.NbrFirstBulletsHS
					}

					break
				}
			}
		}
	}

	for _, weaponsStatsTimeFrame := range weaponsStatsTimeFramesReturn {
		for date, statPerSide := range weaponsStatsTimeFrame.generalsStatsPerSide {
			for side := range statPerSide {
				var (
					firstBulletAccuracy     float64
					firstBulletHS           float64
					bulletAccuracy          float64
					hsPercentage            float64
					averageVelocityAttacker float64
				)

				period := weaponsStatsTimeFrame.generalsStatsPerSide[date]
				if period == nil {
					continue
				}
				periodPerSide := period[side]
				if periodPerSide == nil {
					continue
				}

				if periodPerSide.NbrFirstBulletFired != 0 {
					firstBulletAccuracy = float64(periodPerSide.NbrFirstBulletHit) / float64(periodPerSide.NbrFirstBulletFired) * 100.0
				}
				if periodPerSide.NbrFirstBulletHit != 0 {
					firstBulletHS = float64(periodPerSide.NbrFirstBulletHS) / float64(periodPerSide.NbrFirstBulletHit) * 100.0
				}
				if periodPerSide.NbrBulletFired != 0 {
					bulletAccuracy = float64(periodPerSide.NbrBulletHit) / float64(periodPerSide.NbrBulletFired) * 100.0
				}

				if periodPerSide.NbrBulletFired > 0 {
					averageVelocityAttacker = periodPerSide.CumulativeVelocityAttacker / float64(periodPerSide.NbrBulletFired)
				}

				if periodPerSide.NbrBulletHit != 0 {
					hsPercentage = float64(periodPerSide.NbrBulletHS) / float64(periodPerSide.NbrBulletHit) * 100.0
				}

				if weaponsStatsTimeFrame.WeaponstatsPerSide[side] == nil {
					weaponsStatsTimeFrame.WeaponstatsPerSide[side] = &WeaponsStatsReturn{}
				}
				weaponsStatsTimeFrame.WeaponstatsPerSide[side].AverageVelocityAttacker.Data = append(
					weaponsStatsTimeFrame.WeaponstatsPerSide[side].AverageVelocityAttacker.Data,
					PointReturn{
						X: date,
						Y: averageVelocityAttacker,
					},
				)
				weaponsStatsTimeFrame.WeaponstatsPerSide[side].NbrBulletsFired.Data = append(
					weaponsStatsTimeFrame.WeaponstatsPerSide[side].NbrBulletsFired.Data,
					PointReturn{
						X: date,
						Y: float64(weaponsStatsTimeFrame.generalsStatsPerSide[date][side].NbrBulletFired),
					})
				weaponsStatsTimeFrame.WeaponstatsPerSide[side].FirstBulletAccuracy.Data = append(
					weaponsStatsTimeFrame.WeaponstatsPerSide[side].FirstBulletAccuracy.Data,
					PointReturn{
						X: date,
						Y: firstBulletAccuracy,
					},
				)
				weaponsStatsTimeFrame.WeaponstatsPerSide[side].FirstBulletHS.Data = append(
					weaponsStatsTimeFrame.WeaponstatsPerSide[side].FirstBulletHS.Data,
					PointReturn{
						X: date,
						Y: firstBulletHS,
					})
				weaponsStatsTimeFrame.WeaponstatsPerSide[side].BulletAccuracy.Data = append(
					weaponsStatsTimeFrame.WeaponstatsPerSide[side].BulletAccuracy.Data,
					PointReturn{
						X: date,
						Y: bulletAccuracy,
					})
				weaponsStatsTimeFrame.WeaponstatsPerSide[side].HSPercentage.Data = append(
					weaponsStatsTimeFrame.WeaponstatsPerSide[side].HSPercentage.Data,
					PointReturn{
						X: date,
						Y: hsPercentage,
					})
			}
		}
		for date := range weaponsStatsTimeFrame.generalsStats {
			var (
				firstBulletAccuracy     float64
				firstBulletHS           float64
				bulletAccuracy          float64
				hsPercentage            float64
				averageVelocityAttacker float64
			)

			generalStats := weaponsStatsTimeFrame.generalsStats[time.Unix(date, 0).Unix()]
			if generalStats == nil {
				continue
			}

			if generalStats.NbrBulletFired < 100 {
				continue
			}

			if generalStats.NbrFirstBulletFired != 0 {
				firstBulletAccuracy = float64(generalStats.NbrFirstBulletHit) / float64(generalStats.NbrFirstBulletFired) * 100.0
			}
			if generalStats.NbrFirstBulletHit != 0 {
				firstBulletHS = float64(generalStats.NbrFirstBulletHS) / float64(generalStats.NbrFirstBulletHit) * 100.0
			}

			bulletAccuracy = float64(generalStats.NbrBulletHit) / float64(generalStats.NbrBulletFired) * 100.0

			averageVelocityAttacker = generalStats.CumulativeVelocityAttacker / float64(generalStats.NbrBulletFired)

			if generalStats.NbrBulletHit != 0 {
				hsPercentage = float64(generalStats.NbrBulletHS) / float64(generalStats.NbrBulletHit) * 100.0
			}

			weaponStats := &weaponsStatsTimeFrame.WeaponStats
			weaponStats.AverageVelocityAttacker.Data = append(weaponStats.AverageVelocityAttacker.Data, PointReturn{
				X: date,
				Y: averageVelocityAttacker,
			})
			weaponStats.NbrBulletsFired.Data = append(weaponStats.NbrBulletsFired.Data, PointReturn{
				X: date,
				Y: float64(generalStats.NbrBulletFired),
			})
			weaponStats.FirstBulletAccuracy.Data = append(weaponStats.FirstBulletAccuracy.Data, PointReturn{
				X: date,
				Y: firstBulletAccuracy,
			})
			weaponStats.FirstBulletHS.Data = append(weaponStats.FirstBulletHS.Data, PointReturn{
				X: date,
				Y: firstBulletHS,
			})
			weaponStats.BulletAccuracy.Data = append(weaponStats.BulletAccuracy.Data, PointReturn{
				X: date,
				Y: bulletAccuracy,
			})
			weaponStats.HSPercentage.Data = append(weaponStats.HSPercentage.Data, PointReturn{
				X: date,
				Y: hsPercentage,
			})
		}
	}

	var limitMinimumBullets = map[common.EquipmentClass][2]int{
		1: {50, 20},
		2: {100, 25},
		3: {50, 15},
		4: {100, 25},
		7: {50, 20},
	}

	for _, weaponsStatsTimeFrame := range weaponsStatsTimeFramesReturn {
		for date, stats := range weaponsStatsTimeFrame.weaponsStats {
			for weaponType := range stats {
				class := weaponType.Class()
				if weaponType == common.EqScout ||
					weaponType == common.EqAWP ||
					weaponType == common.EqScar20 {
					class = 7
				}
				if weaponsStatsTimeFrame.WeaponTypeStats[weaponType] == nil {
					weaponsStatsTimeFrame.WeaponTypeStats[weaponType] = &WeaponsStatsReturn{
						WeaponName:  weaponType.String(),
						WeaponType:  weaponType,
						WeaponClass: class,
					}
				}
				weapon := weaponsStatsTimeFrame.WeaponTypeStats[weaponType]
				var (
					firstBulletAccuracy float64
					firstBulletHS       float64
					bulletAccuracy      float64
					hsPercentage        float64
				)

				dayOfWeek := weaponsStatsTimeFrame.weaponsStats[date]
				if dayOfWeek == nil {
					continue
				}
				weaponOfWeek := dayOfWeek[weaponType]
				if weaponOfWeek == nil {
					continue
				}

				if weaponOfWeek.NbrBulletFired > limitMinimumBullets[class][0] {
					bulletAccuracy = float64(weaponOfWeek.NbrBulletHit) / float64(weaponOfWeek.NbrBulletFired) * 100.0
					if weaponOfWeek.NbrBulletHit > 0 {
						hsPercentage = float64(weaponOfWeek.NbrBulletHS) / float64(weaponOfWeek.NbrBulletHit) * 100.0
					}
					weapon.BulletAccuracy.Data = append(weapon.BulletAccuracy.Data, PointReturn{
						X: date,
						Y: bulletAccuracy,
					})
					weapon.HSPercentage.Data = append(weapon.HSPercentage.Data, PointReturn{
						X: date,
						Y: hsPercentage,
					})
				}

				if weaponOfWeek.NbrFirstBulletFired > limitMinimumBullets[class][1] {
					firstBulletAccuracy = float64(weaponOfWeek.NbrFirstBulletHit) / float64(weaponOfWeek.NbrFirstBulletFired) * 100.0
					if weaponOfWeek.NbrFirstBulletHit > 0 {
						firstBulletHS = float64(weaponOfWeek.NbrFirstBulletHS) / float64(weaponOfWeek.NbrFirstBulletHit) * 100.0
					}
					weapon.FirstBulletAccuracy.Data = append(weapon.FirstBulletAccuracy.Data, PointReturn{
						X: date,
						Y: firstBulletAccuracy,
					})
					weapon.FirstBulletHS.Data = append(weapon.FirstBulletHS.Data, PointReturn{
						X: date,
						Y: firstBulletHS,
					})
				}
			}
		}
	}

	return nil
}
