package progression

import "time"

//PointReturn one point for a dataset
type PointReturn struct {
	X int64   `json:"x"`
	Y float64 `json:"y"`
}

// DatasetsReturn main type returned containing all the datasets
type DatasetsReturn struct {
	Data []PointReturn `json:"data"`
}

type TimeFrame byte

const (
	UnknownTimeStamp TimeFrame = iota
	DailyTimeStamp
	WeeklyTimeStamp
	MonthlyTimeStamp
	YearlyTimeStamp
)

// WeekStart return a time.Time from a year and a week
func WeekStart(year, week int) time.Time {
	// Start from the middle of the year:
	t := time.Date(year, 7, 1, 0, 0, 0, 0, time.UTC)

	// Roll back to Monday:
	if wd := t.Weekday(); wd == time.Sunday {
		t = t.AddDate(0, 0, -6)
	} else {
		t = t.AddDate(0, 0, -int(wd)+1)
	}

	// Difference in weeks:
	_, w := t.ISOWeek()
	t = t.AddDate(0, 0, (week-w)*7)

	return t
}

func (tf TimeFrame) String() string {
	switch tf {
	case DailyTimeStamp:
		return "Daily"
	case WeeklyTimeStamp:
		return "Weekly"
	case MonthlyTimeStamp:
		return "Monthly"
	case YearlyTimeStamp:
		return "Yearly"
	default:
		return "Unknown"
	}
}

func (tf TimeFrame) GetUnixTimeFrame(date *time.Time) int64 {
	switch tf {
	case DailyTimeStamp:
		return time.Date(date.Year(), date.Month(), date.Day(), 0, 0, 0, 0, date.Location()).Unix()
	case WeeklyTimeStamp:
		year, week := date.ISOWeek()
		return WeekStart(year, week).Unix()
	case MonthlyTimeStamp:
		return time.Date(date.Year(), date.Month(), 0, 0, 0, 0, 0, date.Location()).Unix()
	default:
		return 0
	}
}
