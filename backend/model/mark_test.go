package model_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestUser_ReplaceWithBetterMark(t *testing.T) {
	mSrc := model.Marks{
		Accuracy:           helpers.FloatPtr(10),
		HS:                 helpers.FloatPtr(10),
		GrenadesValueDeath: helpers.FloatPtr(10),
		Damage:             helpers.IntPtr(10),
		UtilityDamage:      helpers.IntPtr(10),
	}
	mDestc := model.Marks{
		Accuracy:           helpers.FloatPtr(20),
		HS:                 helpers.FloatPtr(30),
		GrenadesValueDeath: helpers.FloatPtr(-40),
		Damage:             helpers.IntPtr(50),
		UtilityDamage:      helpers.IntPtr(60),
	}
	model.ReplaceWithBetterMark(&mSrc, mDestc)
	assert.Equal(t, 20.0, *mSrc.Accuracy)
	assert.Equal(t, 30.0, *mSrc.HS)
	assert.Equal(t, -40.0, *mSrc.GrenadesValueDeath)
	assert.Equal(t, 50, *mSrc.Damage)
	assert.Equal(t, 60, *mSrc.UtilityDamage)
}

func TestUser_ReplaceWithBadderMark(t *testing.T) {
	mSrc := model.Marks{
		Accuracy:           helpers.FloatPtr(10),
		HS:                 helpers.FloatPtr(10),
		GrenadesValueDeath: helpers.FloatPtr(10),
		Damage:             helpers.IntPtr(10),
		UtilityDamage:      helpers.IntPtr(10),
	}
	mDestc := model.Marks{
		Accuracy:           helpers.FloatPtr(-20),
		HS:                 helpers.FloatPtr(-30),
		GrenadesValueDeath: helpers.FloatPtr(40),
		Damage:             helpers.IntPtr(-50),
		UtilityDamage:      helpers.IntPtr(-60),
	}
	model.ReplaceWithBadderMark(&mSrc, mDestc)
	assert.Equal(t, -20.0, *mSrc.Accuracy)
	assert.Equal(t, -30.0, *mSrc.HS)
	assert.Equal(t, 40.0, *mSrc.GrenadesValueDeath)
	assert.Equal(t, -50, *mSrc.Damage)
	assert.Equal(t, -60, *mSrc.UtilityDamage)
}
