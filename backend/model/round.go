package model

import (
	"math/rand"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

// Round describes a round
type Round struct {
	ID *uint `gorm:"primaryKey"`

	DemoID *int64 `gorm:"not null"`
	Demo   *Demo

	ScoreFactsPerPlayer []*ScoreFacts    `gorm:"constraint:OnDelete:CASCADE"`
	MarksPerRound       []*MarksPerRound `gorm:"constraint:OnDelete:CASCADE"`

	HeatMapKills []*HeatMapKill `gorm:"constraint:OnDelete:CASCADE"`
	HeatMapDmgs  []*HeatMapDmg  `gorm:"constraint:OnDelete:CASCADE"`

	NumRound    *int `gorm:"not null"`
	Frames      []*Frame
	ChatMessage []*ChatMessage
	SideWin     *int `gorm:""`
}

// GenerateRandomRounds generate random Rounds
func GenerateRandomRounds(nbrRound int, steamsID []string) []*Round {
	rounds := []*Round{}
	for numRound := 1; numRound <= nbrRound; numRound++ {
		rounds = append(rounds, GenerateRandomRound(numRound, steamsID))
	}
	return rounds
}

// GenerateRandomRound generate a random Round
func GenerateRandomRound(numRound int, steamsID []string) *Round {
	return &Round{
		NumRound:            &numRound,
		ChatMessage:         GenerateRandomChatMessages(rand.Intn(50) + 1),
		Frames:              GenerateRandomFrames(),
		SideWin:             helpers.IntPtr(rand.Int()%2 + 2),
		ScoreFactsPerPlayer: GenerateRandomScoresFacts(steamsID),
		MarksPerRound:       GenerateRandomMarksPerRound(steamsID, numRound),
		HeatMapDmgs:         GenerateRandomsHeatMapsDmgs(rand.Intn(20) + 30),
		HeatMapKills:        GenerateRandomsHeatMapsKills(rand.Intn(5) + 4),
	}
}
