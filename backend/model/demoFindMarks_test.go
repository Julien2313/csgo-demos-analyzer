package model_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestUser_FindBestMarks(t *testing.T) {
	demo := model.GenerateRandomDemoWithStats(&model.User{ID: helpers.UintPtr(0)}, "0", 1)
	demo.TeamsStats[1].PlayersStats[2].Marks = &model.Marks{
		Accuracy:           helpers.FloatPtr(100),
		HS:                 helpers.FloatPtr(100),
		GrenadesValueDeath: helpers.FloatPtr(0),
		Damage:             helpers.IntPtr(10000),
		UtilityDamage:      helpers.IntPtr(10000),
	}
	bestMarks := demo.FindBestMarks()

	assert.Equal(t, 100.0, *bestMarks.Accuracy)
	assert.Equal(t, 100.0, *bestMarks.HS)
	assert.Equal(t, 0.0, *bestMarks.GrenadesValueDeath)
	assert.Equal(t, 10000, *bestMarks.Damage)
	assert.Equal(t, 10000, *bestMarks.UtilityDamage)
}

func TestUser_FindBaddestMark(t *testing.T) {
	demo := model.GenerateRandomDemoWithStats(&model.User{ID: helpers.UintPtr(0)}, "0", 1)
	demo.TeamsStats[1].PlayersStats[2].Marks = &model.Marks{
		Accuracy:           helpers.FloatPtr(0),
		HS:                 helpers.FloatPtr(0),
		GrenadesValueDeath: helpers.FloatPtr(10000),
		Damage:             helpers.IntPtr(-300),
		UtilityDamage:      helpers.IntPtr(-200),
	}
	bestMarks := demo.FindBaddestMarks()

	assert.Equal(t, 0.0, *bestMarks.Accuracy)
	assert.Equal(t, 0.0, *bestMarks.HS)
	assert.Equal(t, 10000.0, *bestMarks.GrenadesValueDeath)
	assert.Equal(t, -300, *bestMarks.Damage)
	assert.Equal(t, -200, *bestMarks.UtilityDamage)
}
