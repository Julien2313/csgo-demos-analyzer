package model

// SteamID temporary type while I find another way to do this
type SteamID struct {
	ID      *uint   `gorm:"primaryKey"`
	SteamID *string `gorm:"not null"`

	DemoID *int64 `gorm:"not null"`
	Demo   *Demo
}

// GenerateRandomSteamID generates random steamID
func GenerateRandomSteamID() *SteamID {
	return &SteamID{SteamID: GenerateRandomSteamIDStr()}
}

// GenerateRandomSteamsID generates random steamID
func GenerateRandomSteamsID(steamIDOwner string, nbrPlayer int) []*SteamID {
	steamID := []*SteamID{}
	if nbrPlayer > 0 {
		steamID = append(steamID, &SteamID{SteamID: &steamIDOwner})
	}

	for numPlayer := 1; numPlayer < nbrPlayer; numPlayer++ {
		steamID = append(steamID, GenerateRandomSteamID())
	}
	return steamID
}
