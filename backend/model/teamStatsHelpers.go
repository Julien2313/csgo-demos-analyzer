package model

import (
	"math/rand"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

// GenerateRandomTeam generate a random team
func GenerateRandomTeam(demoID int64) *TeamStats {
	return &TeamStats{
		Score: helpers.IntPtr(rand.Int() % 17),
	}
}

// GenerateRandomTeamsStats generates 2 random teams
func GenerateRandomTeamsStats(demoID *int64, steamID string) []*TeamStats {
	return []*TeamStats{
		{
			Score: helpers.IntPtr(rand.Int() % 17),
			PlayersStats: []*PlayerStats{
				GenerateRandomPlayerStats(demoID, &steamID),
				GenerateRandomPlayerStats(demoID, nil),
				GenerateRandomPlayerStats(demoID, nil),
				GenerateRandomPlayerStats(demoID, nil),
				GenerateRandomPlayerStats(demoID, nil),
			},
			DuelsTeam: GenerateRandomDuelsTeams(),
		},
		{
			Score: helpers.IntPtr(rand.Int() % 17),
			PlayersStats: []*PlayerStats{
				GenerateRandomPlayerStats(demoID, nil),
				GenerateRandomPlayerStats(demoID, nil),
				GenerateRandomPlayerStats(demoID, nil),
				GenerateRandomPlayerStats(demoID, nil),
				GenerateRandomPlayerStats(demoID, nil),
			},
			DuelsTeam: GenerateRandomDuelsTeams(),
		},
	}
}

// GenerateMatrixForTeams generates random matrix
func GenerateMatrixForTeams(teams []*TeamStats) {
	for _, team := range teams {
		for _, player := range team.PlayersStats {
			player.MatrixFlashs = GenerateRandomMatrixFlashs(teams)
			player.MatrixKills = GenerateRandomMatrixKills(teams)
		}
	}
}
