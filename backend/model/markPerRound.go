package model

import (
	"math/rand"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

// MarksPerRound represents the marks of a player for 1 round
type MarksPerRound struct {
	ID *uint `gorm:"primaryKey"`

	RoundID *int64 `gorm:"not null"`
	Round   *Round `gorm:"foreignKey:RoundID"`

	SteamID   *string `gorm:"not null"`
	Side      *int    `gorm:"not null"`
	ConstTeam *int    `gorm:"not null"`

	NbrBulletsFired      *int `gorm:"not null"`
	NbrBulletsHit        *int `gorm:"not null"`
	NbrBulletsHS         *int `gorm:"not null"`
	NbrFirstBulletsFired *int `gorm:"not null"`
	NbrFirstBulletsHit   *int `gorm:"not null"`
	NbrFirstBulletsHS    *int `gorm:"not null"`

	Accuracy            *float64 `gorm:"not null"`
	HS                  *float64 `gorm:"not null"`
	FirstBulletAccuracy *float64 `gorm:"not null"`
	FirstBulletHS       *float64 `gorm:"not null"`

	Damage                 *int `gorm:"not null"`
	UtilityDamage          *int `gorm:"not null"`
	AverageVelocityShoots  *int `gorm:"not null"`
	AverageDeltaXCrossHair *int `gorm:"not null"`
	AverageDeltaYCrossHair *int `gorm:"not null"`
	NbrDeaths              *int `gorm:"not null"`
	NbrKills               *int `gorm:"not null"`
}

// GenerateRandomMarkPerRound generate a random MarksPerRound
func GenerateRandomMarkPerRound(steamID string, side, constTeam int) *MarksPerRound {
	return &MarksPerRound{
		SteamID:   &steamID,
		Side:      &side,
		ConstTeam: &constTeam,

		NbrBulletsFired:      helpers.IntPtr(rand.Intn(4000)),
		NbrBulletsHit:        helpers.IntPtr(rand.Intn(2000)),
		NbrBulletsHS:         helpers.IntPtr(rand.Intn(1000)),
		NbrFirstBulletsFired: helpers.IntPtr(rand.Intn(1000)),
		NbrFirstBulletsHit:   helpers.IntPtr(rand.Intn(500)),
		NbrFirstBulletsHS:    helpers.IntPtr(rand.Intn(250)),

		Accuracy:            helpers.FloatPtr(rand.Float64() * 100),
		HS:                  helpers.FloatPtr(rand.Float64() * 100),
		FirstBulletAccuracy: helpers.FloatPtr(rand.Float64() * 100),
		FirstBulletHS:       helpers.FloatPtr(rand.Float64() * 100),

		Damage:                 helpers.IntPtr(rand.Intn(5000)),
		UtilityDamage:          helpers.IntPtr(rand.Intn(500)),
		AverageVelocityShoots:  helpers.IntPtr(rand.Intn(250)),
		AverageDeltaXCrossHair: helpers.IntPtr(rand.Intn(50)),
		AverageDeltaYCrossHair: helpers.IntPtr(rand.Intn(50)),
		NbrDeaths:              helpers.IntPtr(rand.Intn(30)),
		NbrKills:               helpers.IntPtr(rand.Intn(40)),
	}
}

// GenerateRandomMarksPerRound generate randoms MarksPerRound
func GenerateRandomMarksPerRound(steamsID []string, numRound int) []*MarksPerRound {
	marksPerRound := []*MarksPerRound{}

	for numSteamID, steamID := range steamsID {
		var (
			side int
			team int
		)
		if numSteamID < 5 {
			team = 2
		} else {
			team = 3
		}
		side = team
		if numRound <= 15 {
			side = 5 - side
		}

		marksPerRound = append(marksPerRound, GenerateRandomMarkPerRound(steamID, side, team))
	}

	return marksPerRound
}
