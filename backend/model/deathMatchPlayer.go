package model

import (
	"math/rand"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

// DeathMatchPlayer stats about a player during a deathmatch
type DeathMatchPlayer struct {
	ID      *uint   `gorm:"primaryKey"`
	SteamID *string `gorm:"not null"`

	DeathMatchDemoID *int64          `gorm:"not null"`
	DeathMatchDemo   *DeathMatchDemo `gorm:"foreignKey:DeathMatchDemoID"`

	WeaponsStats []*DeathMatchWeaponStats `gorm:"constraint:OnDelete:CASCADE;foreignKey:DeathMatchPlayerID"`

	FirstBulletAccuracy *float64 `gorm:"not null"`
	FirstBulletHS       *float64 `gorm:"not null"`
	BulletAccuracy      *float64 `gorm:"not null"`
	HSPercentage        *float64 `gorm:"not null"`

	NbrBulletFired *int `gorm:"not null"`
	NbrBulletHit   *int `gorm:"not null"`
	NbrBulletHS    *int `gorm:"not null"`

	NbrFirstBulletFired *int `gorm:"not null"`
	NbrFirstBulletHit   *int `gorm:"not null"`
	NbrFirstBulletHS    *int `gorm:"not null"`

	AverageVelocityAttacker *float64 `gorm:"not null"`
}

// GenerateRandomDeathMatchPlayer generate a random DeathMatchPlayer
func GenerateRandomDeathMatchPlayer(steamID *string) *DeathMatchPlayer {
	nbrBulletFired := rand.Intn(1000)
	nbrBulletHit := rand.Intn(nbrBulletFired + 1)
	nbrBulletHS := rand.Intn(nbrBulletHit + 1)

	nbrFirstBulletFired := rand.Intn(nbrBulletFired + 1)
	nbrFirstBulletHit := rand.Intn(nbrFirstBulletFired + 1)
	nbrFirstBulletHS := rand.Intn(nbrFirstBulletHit + 1)

	return &DeathMatchPlayer{
		SteamID:             steamID,
		FirstBulletAccuracy: helpers.FloatPtr(rand.Float64() * 100),
		FirstBulletHS:       helpers.FloatPtr(rand.Float64() * 100),
		BulletAccuracy:      helpers.FloatPtr(rand.Float64() * 100),
		HSPercentage:        helpers.FloatPtr(rand.Float64() * 100),

		WeaponsStats: GenerateRandomWeaponsStats(1000),

		NbrBulletFired: &nbrBulletFired,
		NbrBulletHit:   &nbrBulletHit,
		NbrBulletHS:    &nbrBulletHS,

		NbrFirstBulletFired: &nbrFirstBulletFired,
		NbrFirstBulletHit:   &nbrFirstBulletHit,
		NbrFirstBulletHS:    &nbrFirstBulletHS,

		AverageVelocityAttacker: helpers.FloatPtr(rand.Float64() * 250),
	}
}

// GenerateRandomDeathMatchPlayers generate randoms DeathMatchPlayer
func GenerateRandomDeathMatchPlayers(steamID string) []*DeathMatchPlayer {
	var players []*DeathMatchPlayer
	for numPlayer := 0; numPlayer < rand.Intn(3)+5; numPlayer++ {
		if numPlayer == 0 {
			players = append(players, GenerateRandomDeathMatchPlayer(&steamID))
		} else {
			players = append(players, GenerateRandomDeathMatchPlayer(GenerateRandomSteamIDStr()))
		}
	}

	return players
}
