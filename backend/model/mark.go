package model

import (
	"math/rand"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

// Marks reflect how well a player played (not how much he has an impact)
type Marks struct {
	ID *uint `gorm:"primaryKey"`

	Accuracy            *float64 `gorm:"not null"`
	HS                  *float64 `gorm:"not null"`
	FirstBulletAccuracy *float64 `gorm:"not null"`
	FirstBulletHS       *float64 `gorm:"not null"`

	NbrBulletsFired *int `gorm:"not null"`
	NbrBulletsHit   *int `gorm:"not null"`
	NbrBulletsHS    *int `gorm:"not null"`

	NbrFirstBulletsFired *int `gorm:"not null"`
	NbrFirstBulletsHit   *int `gorm:"not null"`
	NbrFirstBulletsHS    *int `gorm:"not null"`

	GrenadesValueDeath *float64 `gorm:"not null"`

	Damage        *int `gorm:"not null"`
	UtilityDamage *int `gorm:"not null"`

	AverageVelocityShoots  *int `gorm:"not null"`
	AverageDeltaXCrossHair *int `gorm:"not null"`
	AverageDeltaYCrossHair *int `gorm:"not null"`

	NbrDeaths *int `gorm:"not null"`
	NbrKills  *int `gorm:"not null"`

	WeaponsMarks []*WeaponMarks
}

// ReplaceWithBetterMark if marks in src is better than dest, replace
func ReplaceWithBetterMark(dest *Marks, src Marks) {
	if *src.Accuracy > *dest.Accuracy {
		*dest.Accuracy = *src.Accuracy
	}
	if *src.HS > *dest.HS {
		*dest.HS = *src.HS
	}
	if *src.UtilityDamage > *dest.UtilityDamage {
		*dest.UtilityDamage = *src.UtilityDamage
	}
	if *src.Damage > *dest.Damage {
		*dest.Damage = *src.Damage
	}
	if *src.GrenadesValueDeath < *dest.GrenadesValueDeath {
		*dest.GrenadesValueDeath = *src.GrenadesValueDeath
	}
}

// ReplaceWithBadderMark if marks in src is badder than dest, replace
func ReplaceWithBadderMark(dest *Marks, src Marks) {
	if *src.Accuracy < *dest.Accuracy {
		*dest.Accuracy = *src.Accuracy
	}
	if *src.HS < *dest.HS {
		*dest.HS = *src.HS
	}
	if *src.UtilityDamage < *dest.UtilityDamage {
		*dest.UtilityDamage = *src.UtilityDamage
	}
	if *src.Damage < *dest.Damage {
		*dest.Damage = *src.Damage
	}
	if *src.GrenadesValueDeath > *dest.GrenadesValueDeath {
		*dest.GrenadesValueDeath = *src.GrenadesValueDeath
	}
}

// GenerateRandomMarks generate a random marks
func GenerateRandomMarks(demoID *int64) *Marks {
	return &Marks{
		Accuracy:               helpers.FloatPtr(rand.Float64() * 100),
		HS:                     helpers.FloatPtr(rand.Float64() * 100),
		FirstBulletAccuracy:    helpers.FloatPtr(rand.Float64() * 100),
		FirstBulletHS:          helpers.FloatPtr(rand.Float64() * 100),
		NbrBulletsFired:        helpers.IntPtr(rand.Int() % 3000),
		NbrBulletsHit:          helpers.IntPtr(rand.Int() % 400),
		NbrBulletsHS:           helpers.IntPtr(rand.Int() % 100),
		NbrFirstBulletsFired:   helpers.IntPtr(rand.Int() % 1500),
		NbrFirstBulletsHit:     helpers.IntPtr(rand.Int() % 200),
		NbrFirstBulletsHS:      helpers.IntPtr(rand.Int() % 50),
		GrenadesValueDeath:     helpers.FloatPtr(rand.Float64() * 1000),
		Damage:                 helpers.IntPtr(rand.Int()%3200 - 200),
		UtilityDamage:          helpers.IntPtr(rand.Int()%200 - 20),
		WeaponsMarks:           GenerateRandomWeaponsMarks(),
		AverageVelocityShoots:  helpers.IntPtr(rand.Int() % 250),
		AverageDeltaXCrossHair: helpers.IntPtr(rand.Int() % 360),
		AverageDeltaYCrossHair: helpers.IntPtr(rand.Int() % 360),
		NbrKills:               helpers.IntPtr(rand.Int() % 30),
		NbrDeaths:              helpers.IntPtr(rand.Int() % 30),
	}
}
