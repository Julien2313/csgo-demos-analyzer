package model_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestUser_ValidateOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	user := model.GenerateRandomUser(false)
	assert.NoError(t, localDB.Create(user).Error)
	assert.NoError(t, localDB.Updates(user).Error)
	user.Mail = nil
	assert.NoError(t, localDB.Updates(user).Error)
}

func TestUser_ValidateNOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	user := model.GenerateRandomUser(false)
	user.Mail = helpers.StrPtr("sdfglomjbqsdfgmojsbdfg")
	assert.EqualError(t, localDB.Create(user).Error, fmt.Sprintf("Error validator : mail isn't valid : %s", *user.Mail))
	assert.EqualError(t, localDB.Updates(user).Error, fmt.Sprintf("Error validator : mail isn't valid : %s", *user.Mail))
}
