package model

import (
	"fmt"
	"math/rand"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

// DuelsTeam duels for the team
type DuelsTeam struct {
	ID *uint `gorm:"primaryKey"`

	TeamStatsID *uint `gorm:"not null"`
	TeamStats   *TeamStats

	Matchup *string `gorm:"not null"`

	NbrOccurence *int `gorm:"not null"`
	NbrWin       *int `gorm:"not null"`
}

// DuelsPlayer duels for the player
type DuelsPlayer struct {
	ID *uint `gorm:"primaryKey"`

	PlayerStatsID *uint `gorm:"not null"`
	PlayerStats   *PlayerStats

	Matchup *string `gorm:"not null"`

	NbrOccurence *int `gorm:"not null"`
	NbrWin       *int `gorm:"not null"`
}

// GenerateRandomDuelsPlayers for tests, generates random duels
func GenerateRandomDuelsPlayers() []*DuelsPlayer {
	duels := []*DuelsPlayer{}

	for a := 1; a <= 5; a++ {
		for e := 1; e <= 5; e++ {
			nbrOccur := rand.Intn(30)
			duels = append(duels, &DuelsPlayer{
				Matchup:      helpers.StrPtr(fmt.Sprintf("%dv%d", a, e)),
				NbrOccurence: &nbrOccur,
				NbrWin:       helpers.IntPtr(rand.Intn(nbrOccur + 1)),
			})
		}
	}

	return duels
}

// GenerateRandomDuelsTeams for tests, generates random duels
func GenerateRandomDuelsTeams() []*DuelsTeam {
	duels := []*DuelsTeam{}

	for a := 1; a <= 5; a++ {
		for e := 1; e <= 5; e++ {
			nbrOccur := rand.Intn(30)
			duels = append(duels, &DuelsTeam{
				Matchup:      helpers.StrPtr(fmt.Sprintf("%dv%d", a, e)),
				NbrOccurence: &nbrOccur,
				NbrWin:       helpers.IntPtr(rand.Intn(nbrOccur + 1)),
			})
		}
	}

	return duels
}
