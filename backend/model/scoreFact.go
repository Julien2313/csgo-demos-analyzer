package model

import (
	"math/rand"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

// ScoreFacts show how much the player made an impact during the round ?
// in example, accuracy would me in a mark, not here, because the player could have a very bad accuracy,
// but still have a big impact
type ScoreFacts struct {
	ID *uint `gorm:"primaryKey"`

	RoundID *uint
	Round   *Round

	SteamID *string `gorm:"not null"`

	RatioScore *float64 `gorm:"not null"`
	TotalScore *float64 `gorm:"not null"`

	DamageScore     *float64 `gorm:"not null"`
	KillScore       *float64 `gorm:"not null"`
	DeathScore      *float64 `gorm:"not null"`
	AssistKillScore *float64 `gorm:"not null"`

	DropScore        *float64 `gorm:"not null"`
	FlashScore       *float64 `gorm:"not null"`
	RevangedScore    *float64 `gorm:"not null"`
	BombDefuseScore  *float64 `gorm:"not null"`
	BombPlantedScore *float64 `gorm:"not null"`
}

// GenerateRandomScoreFacts generate a random score
func GenerateRandomScoreFacts(steamID string) *ScoreFacts {
	return &ScoreFacts{
		SteamID: &steamID,

		RatioScore: helpers.FloatPtr(rand.Float64()*100 - 20),
		TotalScore: helpers.FloatPtr(rand.Float64()*100 - 20),

		DamageScore: helpers.FloatPtr(rand.Float64()*100 - 20),
		KillScore:   helpers.FloatPtr(rand.Float64()*100 - 20),
		DeathScore:  helpers.FloatPtr(rand.Float64()*100 - 20),

		DropScore:        helpers.FloatPtr(rand.Float64()*100 - 20),
		FlashScore:       helpers.FloatPtr(rand.Float64()*100 - 20),
		RevangedScore:    helpers.FloatPtr(rand.Float64()*100 - 20),
		AssistKillScore:  helpers.FloatPtr(rand.Float64()*100 - 20),
		BombDefuseScore:  helpers.FloatPtr(rand.Float64()*100 - 20),
		BombPlantedScore: helpers.FloatPtr(rand.Float64()*100 - 20),
	}
}

// GenerateRandomScoresFacts generate a random score
func GenerateRandomScoresFacts(steamsID []string) []*ScoreFacts {
	scoresFacts := []*ScoreFacts{}

	for _, steamID := range steamsID {
		scoresFacts = append(scoresFacts, GenerateRandomScoreFacts(steamID))
	}

	return scoresFacts
}
