package model

import (
	"math/rand"
	"time"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

// HeatMapKill type to store kills
type HeatMapKill struct {
	ID *int64 `gorm:"primaryKey"`

	RoundID *int64 `gorm:"not null"`
	Round   *Round

	DurationSinceRoundBegan *time.Duration `gorm:"not null"`

	WeaponKiller  *common.EquipmentType `gorm:"index"`
	SteamIDKiller *string               `gorm:"index"`
	SideKiller    *int
	KillerPosX    *float64
	KillerPosY    *float64

	ActiveWeaponVictim *common.EquipmentType `gorm:"index"`
	SteamIDVictim      *string               `gorm:"index"`
	SideVictim         *int
	VictimPosX         *float64
	VictimPosY         *float64
}

// HeatMapDmg type to store dmgs
type HeatMapDmg struct {
	ID *int64 `gorm:"primaryKey"`

	RoundID *int64 `gorm:"not null"`
	Round   *Round

	DurationSinceRoundBegan *time.Duration `gorm:"not null"`

	WeaponShooter  *common.EquipmentType `gorm:"index"`
	SteamIDShooter *string               `gorm:"index"`
	SideShooter    *int
	ShooterPosX    *float64
	ShooterPosY    *float64

	ActiveWeaponVictim *common.EquipmentType `gorm:"index"`
	SteamIDVictim      *string               `gorm:"index"`
	SideVictim         *int
	VictimPosX         *float64
	VictimPosY         *float64

	Dmg *int `gorm:"not null"`
}

// GenerateRandomHeatMapKill generate a HeatMapKill
func GenerateRandomHeatMapKill() *HeatMapKill {
	weaponKiller := common.EquipmentType(rand.Intn(507))
	activeWeaponVictim := common.EquipmentType(rand.Intn(507))

	return &HeatMapKill{
		DurationSinceRoundBegan: helpers.DurationPtr(time.Second * time.Duration(rand.Intn(115))),
		WeaponKiller:            &weaponKiller,
		SteamIDKiller:           GenerateRandomSteamIDStr(),
		SideKiller:              helpers.IntPtr(rand.Intn(2)),
		KillerPosX:              helpers.FloatPtr(rand.Float64() - 10*20),
		KillerPosY:              helpers.FloatPtr(rand.Float64() - 10*20),
		ActiveWeaponVictim:      &activeWeaponVictim,
		SteamIDVictim:           GenerateRandomSteamIDStr(),
		SideVictim:              helpers.IntPtr(rand.Intn(2)),
		VictimPosX:              helpers.FloatPtr(rand.Float64() - 10*20),
		VictimPosY:              helpers.FloatPtr(rand.Float64() - 10*20),
	}
}

// GenerateRandomsHeatMapsKills generate []*HeatMapKill
func GenerateRandomsHeatMapsKills(nbrHeatMap int) []*HeatMapKill {
	heatmap := []*HeatMapKill{}

	for numHeatMap := 0; numHeatMap < nbrHeatMap; numHeatMap++ {
		heatmap = append(heatmap, GenerateRandomHeatMapKill())
	}

	return heatmap
}

// GenerateRandomHeatMapDmg generate a HeatMapDmg
func GenerateRandomHeatMapDmg() *HeatMapDmg {
	weaponKiller := common.EquipmentType(rand.Intn(507))
	activeWeaponVictim := common.EquipmentType(rand.Intn(507))

	return &HeatMapDmg{
		DurationSinceRoundBegan: helpers.DurationPtr(time.Second * time.Duration(rand.Intn(115))),
		WeaponShooter:           &weaponKiller,
		SteamIDShooter:          GenerateRandomSteamIDStr(),
		SideShooter:             helpers.IntPtr(rand.Intn(2)),
		ShooterPosX:             helpers.FloatPtr(rand.Float64() - 10*20),
		ShooterPosY:             helpers.FloatPtr(rand.Float64() - 10*20),
		ActiveWeaponVictim:      &activeWeaponVictim,
		SteamIDVictim:           GenerateRandomSteamIDStr(),
		SideVictim:              helpers.IntPtr(rand.Intn(2)),
		VictimPosX:              helpers.FloatPtr(rand.Float64() - 10*20),
		VictimPosY:              helpers.FloatPtr(rand.Float64() - 10*20),
		Dmg:                     helpers.IntPtr(rand.Intn(100)),
	}
}

// GenerateRandomsHeatMapsDmgs generate []*HeatMapDmg
func GenerateRandomsHeatMapsDmgs(nbrHeatMap int) []*HeatMapDmg {
	heatmap := []*HeatMapDmg{}

	for numHeatMap := 0; numHeatMap < nbrHeatMap; numHeatMap++ {
		heatmap = append(heatmap, GenerateRandomHeatMapDmg())
	}

	return heatmap
}
