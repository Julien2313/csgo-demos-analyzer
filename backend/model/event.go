package model

import (
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

// EventTypeBDD the type of event, kill, bomb planted...
type EventTypeBDD int

// constances of events
const (
	NilEvt EventTypeBDD = iota
	KillEvt
	BombPlantEvt
	BombDefusedEvt
	BombExplodeEvt
	RoundFreezetimeEndEvt
)

// Event interface for events during a round (kill, bomb planted etc)
type Event interface {
	GetType() EventTypeBDD
	SetFrame(*Frame)
	GetIDType() (*int64, string)
	MarshalJSON() ([]byte, error)
}

// KillEvent event for a kill
type KillEvent struct {
	ID *int64 `gorm:"primaryKey"`

	FrameID *int64 `gorm:"not null"`
	Frame   *Frame

	TimeKill *time.Duration `gorm:"not null"`

	IsHeadShot *bool `gorm:"not null"`
	IsWallBang *bool `gorm:"not null"`

	KillerName *string `gorm:"not null"`
	KillerSide *int    `gorm:"not null"`

	Weapon *string `gorm:"not null"`

	VictimSide *int    `gorm:"not null"`
	VictimName *string `gorm:"not null"`
}

// GetType return the type of the event
func (KillEvent) GetType() EventTypeBDD {
	return KillEvt
}

// SetFrame set the frame IF to the event
func (ke *KillEvent) SetFrame(frame *Frame) {
	// ke.Frame = frame
	ke.FrameID = frame.ID
}

// GetIDType return the event ID from DB and the name of the table
func (ke *KillEvent) GetIDType() (*int64, string) {
	return ke.ID, "kill_event"
}

// MarshalJSON get the json of the event
func (ke *KillEvent) MarshalJSON() ([]byte, error) {
	var timeKill string

	if *ke.TimeKill < 0 {
		*ke.TimeKill = 0
	}
	timeKill = helpers.FmtDuration(*ke.TimeKill)

	killer, err := json.Marshal(*ke.KillerName)
	if err != nil {
		return []byte{}, errors.New("couldn't marshal KillerName in evt")
	}

	victim, err := json.Marshal(*ke.VictimName)
	if err != nil {
		return []byte{}, errors.New("couldn't marshal VictimName in evt")
	}

	return []byte(fmt.Sprintf(`{
	"type":%d,
	"timeKill":"%s",
	"killerSide":%d,
	"victimSide":%d,
	"killer":%s,
	"weaponName":"%s",
	"victimName":%s,
	"sideKiller":%d,
	"isHeadShot":%t,
	"isWallBang":%t
}`,
			KillEvt,
			timeKill,
			*ke.KillerSide,
			*ke.VictimSide,
			string(killer),
			*ke.Weapon,
			string(victim),
			*ke.KillerSide,
			*ke.IsHeadShot,
			*ke.IsWallBang)),
		nil
}

// BombDefusedEvent event when a bomb has been defused
type BombDefusedEvent struct {
	ID *int64 `gorm:"primaryKey"`

	FrameID *int64 `gorm:"not null"`
	Frame   *Frame

	Defuser       *string        `gorm:"not null"`
	TimeRemainded *time.Duration `gorm:"not null"`
}

// GetType return the type of the event
func (BombDefusedEvent) GetType() EventTypeBDD {
	return BombDefusedEvt
}

// SetFrame set the frame IF to the event
func (bde *BombDefusedEvent) SetFrame(frame *Frame) {
	// bde.Frame = frame
	bde.FrameID = frame.ID
}

// GetIDType return the event ID from DB and the name of the table
func (bde *BombDefusedEvent) GetIDType() (*int64, string) {
	return bde.ID, "bomb_defused_event"
}

// MarshalJSON get the json of the event
func (bde *BombDefusedEvent) MarshalJSON() ([]byte, error) {
	defuser, err := json.Marshal(*bde.Defuser)
	if err != nil {
		return []byte{}, errors.New("could marshal data in evt")
	}

	return []byte(fmt.Sprintf(`{
	"type":%d,
	"defuser":%s,
	"timeRemainded":%f
}`, BombDefusedEvt, string(defuser), bde.TimeRemainded.Seconds())), nil
}

// BombPlantedEvent event when a bomb has been planted
type BombPlantedEvent struct {
	ID *int64 `gorm:"primaryKey"`

	FrameID *int64 `gorm:"not null"`
	Frame   *Frame

	TimePlanted *time.Duration `gorm:"not null"`

	Planter  *string `gorm:"not null"`
	Bombsite *string `gorm:"not null"`
}

// GetType return the type of the event
func (BombPlantedEvent) GetType() EventTypeBDD {
	return BombPlantEvt
}

// GetIDType return the event ID from DB and the name of the table
func (bpe *BombPlantedEvent) GetIDType() (*int64, string) {
	return bpe.ID, "bomb_planted_event"
}

// SetFrame set the frame IF to the event
func (bpe *BombPlantedEvent) SetFrame(frame *Frame) {
	// bpe.Frame = frame
	bpe.FrameID = frame.ID
}

// MarshalJSON get the json of the event
func (bpe *BombPlantedEvent) MarshalJSON() ([]byte, error) {
	planter, err := json.Marshal(*bpe.Planter)
	if err != nil {
		return []byte{}, errors.New("could marshal data in evt")
	}

	return []byte(fmt.Sprintf(`{
	"type":%d,
	"timePlanted":"%s",
	"planter":%s,
	"bombsite":"%s"
}`, BombPlantEvt, helpers.FmtDuration(*bpe.TimePlanted), string(planter), *bpe.Bombsite)), nil
}

// BombExplodeEvent event when a bomb exploded
type BombExplodeEvent struct {
	ID *int64 `gorm:"primaryKey"`

	FrameID *int64 `gorm:"not null"`
	Frame   *Frame
}

// GetType return the type of the event
func (BombExplodeEvent) GetType() EventTypeBDD {
	return BombExplodeEvt
}

// SetFrame set the frame IF to the event
func (bee *BombExplodeEvent) SetFrame(frame *Frame) {
	// bee.Frame = frame
	bee.FrameID = frame.ID
}

// GetIDType return the event ID from DB and the name of the table
func (bee *BombExplodeEvent) GetIDType() (*int64, string) {
	return bee.ID, "bomb_exploded_event"
}

// MarshalJSON get the json of the event
func (bee *BombExplodeEvent) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf(`{
	"type":%d
}`, BombExplodeEvt)), nil
}

// RoundFreezetimeEndEvent event when a freezetime ended, should be removed soon
type RoundFreezetimeEndEvent struct {
	ID *int64 `gorm:"primaryKey"`

	FrameID *int64 `gorm:"not null"`
	Frame   *Frame
}

// GetType return the type of the event
func (RoundFreezetimeEndEvent) GetType() EventTypeBDD {
	return RoundFreezetimeEndEvt
}

// SetFrame set the frame IF to the event
func (rfee *RoundFreezetimeEndEvent) SetFrame(frame *Frame) {
	// rfee.Frame = frame
	rfee.FrameID = frame.ID
}

// GetIDType return the event ID from DB and the name of the table
func (rfee *RoundFreezetimeEndEvent) GetIDType() (*int64, string) {
	return rfee.ID, "round_freezetime_end_event"
}

// MarshalJSON get the json of the event
func (rfee *RoundFreezetimeEndEvent) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf(`{
	"type":%d
}`, RoundFreezetimeEndEvt)), nil
}

// GenerateRandomKillEvent helper that generate random KillEvent
func GenerateRandomKillEvent() *KillEvent {
	return &KillEvent{
		TimeKill:   helpers.DurationPtr(time.Second * time.Duration(rand.Int()) * 115),
		IsHeadShot: helpers.BoolPtr(rand.Int()%2 == 0),
		IsWallBang: helpers.BoolPtr(rand.Int()%2 == 0),
		KillerName: helpers.StrPtr(helpers.RandomString(10)),
		KillerSide: helpers.IntPtr(rand.Int()%2 + 2),
		Weapon:     helpers.StrPtr(helpers.RandomString(10)),
		VictimSide: helpers.IntPtr(rand.Int()%2 + 2),
		VictimName: helpers.StrPtr(helpers.RandomString(10)),
	}
}

// GenerateRandomBombPlantedEvent helper that generate random BombPlantedEvent
func GenerateRandomBombPlantedEvent() *BombPlantedEvent {
	return &BombPlantedEvent{
		TimePlanted: helpers.DurationPtr(time.Second * time.Duration(rand.Int()) * 115),
		Planter:     helpers.StrPtr(helpers.RandomString(10)),
		Bombsite:    helpers.StrPtr(helpers.RandomString(1)),
	}
}

// GenerateRandomBombDefusedEvent helper that generate random BombDefusedEvent
func GenerateRandomBombDefusedEvent() *BombDefusedEvent {
	return &BombDefusedEvent{
		Defuser:       helpers.StrPtr(helpers.RandomString(10)),
		TimeRemainded: helpers.DurationPtr(time.Second * time.Duration(rand.Int()) * 115),
	}
}

// GenerateRandomBombExplodeEvent helper that generate random BombExplodeEvent
func GenerateRandomBombExplodeEvent() *BombExplodeEvent {
	return &BombExplodeEvent{}
}

// GenerateRandomRoundFreezetimeEndEvent helper that generate random RoundFreezetimeEndEvent
func GenerateRandomRoundFreezetimeEndEvent() *RoundFreezetimeEndEvent {
	return &RoundFreezetimeEndEvent{}
}

// GenerateRandomEvents generate a random Event
func GenerateRandomEvents() []Event {
	numEvent := EventTypeBDD(rand.Int() % 5)
	switch numEvent {
	case NilEvt:
		return nil
	case KillEvt:
		return []Event{GenerateRandomKillEvent()}
	case BombPlantEvt:
		return []Event{GenerateRandomBombPlantedEvent()}
	case BombDefusedEvt:
		return []Event{GenerateRandomBombDefusedEvent()}
	case BombExplodeEvt:
		return []Event{GenerateRandomBombExplodeEvent()}
	case RoundFreezetimeEndEvt:
		return []Event{GenerateRandomRoundFreezetimeEndEvent()}
	}

	return nil
}
