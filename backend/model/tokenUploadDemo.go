package model

import (
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

// TokenUploadDemo saving token that allowed user to upload demo
type TokenUploadDemo struct {
	ID *int64

	UserID *uint `gorm:"not null"`
	User   *User

	Token     *string `gorm:"not null"`
	IPAllowed *string `gorm:"not null"`

	LastUsed *time.Time
}

// Generate generates a random token
func (t *TokenUploadDemo) Generate() {
	t.Token = helpers.StrPtr(helpers.RandomString(64))
}

// GenerateRandomToken generates random token
func GenerateRandomToken(userID uint) TokenUploadDemo {
	return TokenUploadDemo{
		UserID:    &userID,
		Token:     helpers.StrPtr(helpers.RandomString(64)),
		IPAllowed: helpers.StrPtr(fmt.Sprintf("%d.%d.%d.%d", rand.Intn(256), rand.Intn(256), rand.Intn(256), rand.Intn(256))),
	}
}

// GenerateRandomTokens generates random tokens
func GenerateRandomTokens(userID uint, nbrToken int) []TokenUploadDemo {
	var tokens []TokenUploadDemo

	for numToken := 0; numToken < nbrToken; numToken++ {
		tokens = append(tokens, GenerateRandomToken(userID))
	}

	return tokens
}
