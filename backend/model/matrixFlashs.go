package model

// MatrixFlashs matrix to store for one game the amount of time every player has flashed every other players
type MatrixFlashs struct {
	ID *uint `gorm:"primaryKey"`

	PlayerStatsID *uint `gorm:"not null"`
	PlayerStats   *PlayerStats

	FlashedSteamID  *string `gorm:"not null"`
	FlashedUsername *string `gorm:"not null"`

	NbrSecFlashs *float64 `gorm:"not null"`
}
