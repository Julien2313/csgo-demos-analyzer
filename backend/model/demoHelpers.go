package model

import (
	"fmt"
	"math/rand"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

func generateRandomName() *string {
	const sizeNameDemo = 10
	str := helpers.RandomString(sizeNameDemo)

	return helpers.StrPtr(fmt.Sprintf("%s.dem", str))
}

// GenerateRandomDemoWithStats generate a random demo with all the stats
func GenerateRandomDemoWithStats(user *User, steamID string, versionAnalyzer int64) *Demo {
	demoID := int64(*helpers.RandUIntPSQL())
	steamsIDs := GenerateRandomSteamsID(steamID, 10)
	steamsID := []string{}
	for _, steamID := range steamsIDs {
		steamsID = append(steamsID, *steamID.SteamID)
	}

	demo := &Demo{
		ID:              &demoID,
		UserID:          user.ID,
		Sharecode:       GenerateRandomShareCode(),
		HashFile:        []byte(helpers.RandomString(20)),
		Downloaded:      helpers.BoolPtr(true),
		Source:          DemoSource(rand.Intn(4)),
		GameMode:        Competitive,
		VersionAnalyzer: &versionAnalyzer,
		Date:            helpers.RandDate(),
		TeamsStats:      GenerateRandomTeamsStats(&demoID, steamID),
		MapName:         helpers.StrPtr(helpers.MapNames[rand.Int()%len(helpers.MapNames)]),
		SteamsIDs:       steamsIDs,
		Path:            generateRandomName(),
		Name:            generateRandomName(),
		Size:            helpers.Int64Ptr(int64(rand.Int31n(40*1024*1024) + 60*1024*1024)),
		ReplayURL:       helpers.StrPtr("/location/" + *generateRandomName()),
		Rounds:          GenerateRandomRounds(30, steamsID),
	}
	GenerateMatrixForTeams(demo.TeamsStats)

	return demo
}

// GenerateRandomDemo generate a random demo
func GenerateRandomDemo(user *User, steamID string) *Demo {
	demoID := int64(*helpers.RandUIntPSQL())
	return &Demo{
		ID:              &demoID,
		UserID:          user.ID,
		Sharecode:       GenerateRandomShareCode(),
		HashFile:        []byte(helpers.RandomString(20)),
		Source:          DemoSource(rand.Intn(4)),
		GameMode:        Competitive,
		Downloaded:      helpers.BoolPtr(rand.Int()%2 == 0),
		VersionAnalyzer: helpers.Int64Ptr(0),
		Date:            helpers.RandDate(),
		Path:            generateRandomName(),
		Name:            generateRandomName(),
		Size:            helpers.Int64Ptr(int64(rand.Int31n(40*1024*1024) + 60*1024*1024)),
		ReplayURL:       helpers.StrPtr("/location/" + *generateRandomName()),
	}
}

// AddSteamsIDs helper that replace the steamsID struct in the demo
func (d *Demo) AddSteamsIDs(steamID string, nbrPlayer int) {
	d.SteamsIDs = GenerateRandomSteamsID(steamID, nbrPlayer)
}
