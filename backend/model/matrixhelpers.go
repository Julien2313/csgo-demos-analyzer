package model

import (
	"math/rand"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

// GenerateRandomMatrixKills generate a random matrix kills
func GenerateRandomMatrixKills(teams []*TeamStats) []*MatrixKills {
	matrix := []*MatrixKills{}
	for _, team := range teams {
		for _, player := range team.PlayersStats {
			matrix = append(matrix, &MatrixKills{
				KilledSteamID:  player.SteamID,
				KilledUsername: player.Username,
				NbrKills:       helpers.IntPtr(rand.Int() % 30),
			})
		}
	}
	return matrix
}

// GenerateRandomMatrixFlashs generate a random matrix flashs
func GenerateRandomMatrixFlashs(teams []*TeamStats) []*MatrixFlashs {
	matrix := []*MatrixFlashs{}
	for _, team := range teams {
		for _, player := range team.PlayersStats {
			matrix = append(matrix, &MatrixFlashs{
				FlashedSteamID:  player.SteamID,
				FlashedUsername: player.Username,
				NbrSecFlashs:    helpers.FloatPtr(rand.Float64() * 20),
			})
		}
	}
	return matrix
}
