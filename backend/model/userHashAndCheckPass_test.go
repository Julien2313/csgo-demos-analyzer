package model

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestUser_PasswordCheck(t *testing.T) {
	user := GenerateRandomUser(false)
	password := *user.Password

	require.NoError(t, user.HashPassword())
	assert.NoError(t, user.CheckPassword(password))
	assert.Error(t, user.CheckPassword("nopass"))
}
