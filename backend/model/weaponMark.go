package model

import (
	"math/rand"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

// WeaponMarks reflect how well a player played with a weapon(not how much he has an impact)
type WeaponMarks struct {
	ID *uint `gorm:"primaryKey"`

	MarksID *uint  `gorm:"not null"`
	Marks   *Marks `gorm:"foreignKey:MarksID"`

	WeaponType *common.EquipmentType `gorm:"not null"`

	NbrBulletsFired      *int `gorm:"not null"`
	NbrBulletsHit        *int `gorm:"not null"`
	NbrBulletsHS         *int `gorm:"not null"`
	NbrFirstBulletsFired *int `gorm:"not null"`
	NbrFirstBulletsHit   *int `gorm:"not null"`
	NbrFirstBulletsHS    *int `gorm:"not null"`

	Accuracy            *float64 `gorm:"not null"`
	HS                  *float64 `gorm:"not null"`
	FirstBulletAccuracy *float64 `gorm:"not null"`
	FirstBulletHS       *float64 `gorm:"not null"`

	AverageVelocityShoots  *int `gorm:"not null"`
	AverageDeltaXCrossHair *int `gorm:"not null"`
	AverageDeltaYCrossHair *int `gorm:"not null"`
	Damage                 *int `gorm:"not null"`
	NbrDeaths              *int `gorm:"not null"`
	NbrKills               *int `gorm:"not null"`
}

// GenerateRandomWeaponMarks generate a random marks
func GenerateRandomWeaponMarks() *WeaponMarks {
	weaponsType := []int{1, 2, 3, 4, 7}
	weaponType := common.EquipmentType(weaponsType[rand.Intn(5)])
	return &WeaponMarks{
		WeaponType:             &weaponType,
		NbrBulletsFired:        helpers.IntPtr(rand.Int() % 500),
		NbrBulletsHit:          helpers.IntPtr(rand.Int() % 150),
		NbrBulletsHS:           helpers.IntPtr(rand.Int() % 50),
		NbrFirstBulletsFired:   helpers.IntPtr(rand.Int() % 500),
		NbrFirstBulletsHit:     helpers.IntPtr(rand.Int() % 100),
		NbrFirstBulletsHS:      helpers.IntPtr(rand.Int() % 50),
		Accuracy:               helpers.FloatPtr(rand.Float64() * 100),
		HS:                     helpers.FloatPtr(rand.Float64() * 100),
		FirstBulletAccuracy:    helpers.FloatPtr(rand.Float64() * 100),
		FirstBulletHS:          helpers.FloatPtr(rand.Float64() * 100),
		Damage:                 helpers.IntPtr(rand.Int()%3200 - 200),
		AverageVelocityShoots:  helpers.IntPtr(rand.Int() % 250),
		AverageDeltaXCrossHair: helpers.IntPtr(rand.Int() % 360),
		AverageDeltaYCrossHair: helpers.IntPtr(rand.Int() % 360),
		NbrDeaths:              helpers.IntPtr(rand.Int() % 30),
		NbrKills:               helpers.IntPtr(rand.Int() % 30),
	}
}

// GenerateRandomWeaponsMarks generate a random marks
func GenerateRandomWeaponsMarks() []*WeaponMarks {
	var weaponsMarks []*WeaponMarks

	for numWeapon := 0; numWeapon < rand.Intn(5)+3; numWeapon++ {
		weaponsMarks = append(weaponsMarks, GenerateRandomWeaponMarks())
	}

	return weaponsMarks
}
