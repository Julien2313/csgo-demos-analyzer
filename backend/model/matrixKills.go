package model

// MatrixKills matrix to store for one game the of time every player has killed every other players from the enemy team
type MatrixKills struct {
	ID *uint `gorm:"primaryKey"`

	PlayerStatsID *uint `gorm:"not null"`
	PlayerStats   *PlayerStats

	KilledSteamID  *string `gorm:"not null"`
	KilledUsername *string `gorm:"not null"`

	NbrKills *int `gorm:"not null"`
}
