package model

import (
	"math/rand"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

// PlayerCard struct that store all the data for a player needed to display its state
type PlayerCard struct {
	Frame *Frame

	IsAlive          bool
	IsConnected      bool
	IsControllingBot bool
	Side             int
	PrimaryWeapon    string
	Pistol           string
	Grenades         string
	HasC4            bool
	PlayerName       string
	SteamID          string
	Health           int
	Armor            int
	HasHelmet        bool
	HasDefuseKit     bool
	Money            int
}

// GenerateRandomPlayerCard generate randoms playersCard
func GenerateRandomPlayerCard() *PlayerCard {
	return &PlayerCard{
		IsAlive:          rand.Int()%2 == 0,
		IsConnected:      rand.Int()%2 == 0,
		IsControllingBot: rand.Int()%2 == 0,
		Side:             rand.Int()%2 + 2,
		PrimaryWeapon:    helpers.RandomString(10),
		Pistol:           helpers.RandomString(10),
		Grenades:         helpers.RandomString(10),
		HasC4:            rand.Int()%2 == 0,
		PlayerName:       helpers.RandomString(10),
		SteamID:          *GenerateRandomSteamIDStr(),
		Health:           rand.Int() % 100,
		Armor:            rand.Int() % 100,
		HasHelmet:        rand.Int()%2 == 0,
		HasDefuseKit:     rand.Int()%2 == 0,
		Money:            rand.Int() % 16000,
	}
}

// GenerateRandomPlayersCard generate a random playerCard
func GenerateRandomPlayersCard() []*PlayerCard {
	playersCard := []*PlayerCard{}
	for numPlayersCard := 0; numPlayersCard < 10; numPlayersCard++ {
		playersCard = append(playersCard, GenerateRandomPlayerCard())
	}
	return playersCard
}
