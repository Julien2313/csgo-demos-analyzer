package model

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

func TestDemo_FindPlayer(t *testing.T) {
	user := GenerateRandomUser(true)
	user.ID = helpers.UintPtr(50)

	demo := GenerateRandomDemo(user, *user.SteamID)

	for _, team := range demo.TeamsStats {
		for _, player := range team.PlayersStats {
			assert.Equal(t, *player.SteamID, *demo.FindPlayer(player.SteamID).SteamID)
		}
	}

	assert.Nil(t, demo.FindPlayer(helpers.StrPtr("36456874684")))
}
