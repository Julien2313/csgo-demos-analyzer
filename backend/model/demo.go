package model

import (
	"errors"
	"io/ioutil"
	"regexp"
	"time"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model/modelproto"
	"google.golang.org/protobuf/proto"
)

// ErrUserDataMissing data from user are missing
var ErrUserDataMissing = errors.New("data from user are missing")

// ErrURLNotFound url of replay hasn't been found
var ErrURLNotFound = errors.New("url of replay hasn't been found")

// Demo demo type with generals data
type Demo struct {
	ID        *int64 `gorm:"primaryKey;auto_increment:false"`
	CreatedAt *time.Time
	UpdatedAt *time.Time
	Sharecode *string `gorm:"unique"`
	HashFile  []byte  `gorm:"not null; unique"`

	VersionAnalyzer *int64       `gorm:"not null"`
	Downloaded      *bool        `gorm:"not null"`
	Source          DemoSource   `gorm:"not null"`
	GameMode        DemoGameMode `gorm:"not null"`

	Rounds     []*Round     `gorm:"constraint:OnDelete:CASCADE"`
	TeamsStats []*TeamStats `gorm:"constraint:OnDelete:CASCADE"`

	UserID *uint `gorm:"not null"`
	User   *User

	SteamsIDs []*SteamID

	Date    *time.Time `gorm:"not null"`
	MapName *string    `gorm:""`

	Path *string `gorm:"not null"`
	Name *string `gorm:"not null"`
	Size *int64  `gorm:"not null"`

	ReplayURL *string `gorm:""`
}

// DemoSource source of the demo
type DemoSource byte

// DemoSource consts for the source of demos
const (
	Unknownsource DemoSource = iota
	Valve
	Faceit
	ESEA
	UserUpload
)

// SafeSources source that I can belive, in example valve, faceit or esea
var SafeSources = []DemoSource{Valve, Faceit, ESEA}

// UnsafeSources source that I can't belive and checks must be performed
// like demos uploaded by the uthers
var UnsafeSources = []DemoSource{UserUpload}

// AllSources every sources
var AllSources = []DemoSource{Unknownsource, Valve, Faceit, ESEA, UserUpload}

// DemoGameMode game mode of the demo
type DemoGameMode byte

// DemoGameMode consts for game mode of demos
const (
	UnknownGameMode DemoGameMode = iota
	Competitive
	DM
	Wingman
)

// EndState state of the game linked to an user, won lost draw
type EndState byte

// EndState consts for an end stat of a round
const (
	Undefined EndState = iota
	Won
	Lost
	Draw
)

// ParseDemoInfoFromFile function that parse a .info file from a reader
func ParseDemoInfoFromFile(filename string) (*Demo, error) {
	demo := &Demo{}

	demoBytes, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	demoInfo := &modelproto.DemoInfo{}
	if err := proto.Unmarshal(demoBytes, demoInfo); err != nil {
		return nil, err
	}

	demo.ID = demoInfo.DemoGlobalData.ID
	demo.Date = helpers.TimePtr(time.Unix(*demoInfo.DemoGlobalData.Timestamp, 0))

	re := regexp.MustCompile(`http://replay.*\.dem\.bz2`)

	demo.ReplayURL = helpers.StrPtr(string(re.Find(demoBytes)))
	if len(*demo.ReplayURL) == 0 {
		return nil, ErrURLNotFound
	}

	return demo, nil
}

// IsWin if demo is analyzed, return if the player won, lost or drawed
func (d Demo) IsWin(steamID *string) EndState {
	if steamID == nil ||
		d.VersionAnalyzer == nil || *d.VersionAnalyzer == 0 ||
		d.TeamsStats == nil || len(d.TeamsStats) != 2 {
		return Undefined
	}

	for numTeam, team := range d.TeamsStats {
		for _, player := range team.PlayersStats {
			if *player.SteamID == *steamID {
				switch {
				case *d.TeamsStats[numTeam].Score > *d.TeamsStats[1-numTeam].Score:
					return Won
				case *d.TeamsStats[numTeam].Score < *d.TeamsStats[1-numTeam].Score:
					return Lost
				default:
					return Draw
				}
			}
		}
	}

	return Undefined
}

// FindPlayer return the struct player from the user's steam ID
func (d Demo) FindPlayer(steamID *string) *PlayerStats {
	if steamID == nil ||
		d.TeamsStats == nil || len(d.TeamsStats) != 2 {
		return nil
	}

	for _, team := range d.TeamsStats {
		for _, player := range team.PlayersStats {
			if *player.SteamID == *steamID {
				return player
			}
		}
	}

	return nil
}

// FindBestMarks find in a demo, with all the best marks, even if they are not from the same player
func (d Demo) FindBestMarks() *Marks {
	bestMarks := &Marks{
		Accuracy:           helpers.FloatPtr(0),
		HS:                 helpers.FloatPtr(0),
		GrenadesValueDeath: helpers.FloatPtr(10000),
		Damage:             helpers.IntPtr(0),
		UtilityDamage:      helpers.IntPtr(0),
	}

	for _, team := range d.TeamsStats {
		for _, player := range team.PlayersStats {
			ReplaceWithBetterMark(bestMarks, *player.Marks)
		}
	}

	return bestMarks
}

// FindBaddestMarks find in a demo, with all the baddest marks, even if they are not from the same player
func (d Demo) FindBaddestMarks() *Marks {
	baddestMarks := &Marks{
		Accuracy:           helpers.FloatPtr(100),
		HS:                 helpers.FloatPtr(100),
		GrenadesValueDeath: helpers.FloatPtr(0),
		Damage:             helpers.IntPtr(100000),
		UtilityDamage:      helpers.IntPtr(100000),
	}

	for _, team := range d.TeamsStats {
		for _, player := range team.PlayersStats {
			ReplaceWithBadderMark(baddestMarks, *player.Marks)
		}
	}

	return baddestMarks
}
