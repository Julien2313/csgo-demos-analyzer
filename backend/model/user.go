package model

import (
	"fmt"
	"regexp"
	"time"

	"github.com/qor/validations"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gorm.io/gorm"
)

// LengthSteamID the length of a steamID
const LengthSteamID = 17

// User struct of an user
type User struct {
	ID        *uint `gorm:"primaryKey"`
	CreatedAt *time.Time
	UpdatedAt *time.Time

	SteamID            *string `gorm:""`
	ShareCode          *string `gorm:""`
	AccessTokenHistory *string `gorm:""`
	APIValveWorking    *bool   `gorm:"not null;default:false"`

	Mail     *string `gorm:"not null;unique"`
	Password *string `gorm:"not null"`

	Demos        []*Demo
	TokensUpload []*TokenUploadDemo
	// stored in GB
	SizeUsed *float64 `gorm:"not null;default:0"`
}

// BeforeSave check for the type user
func (u *User) BeforeSave(db *gorm.DB) error {
	if u.Mail != nil {
		userMaildRegex := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9]" +
			"(?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
		if !userMaildRegex.MatchString(*u.Mail) {
			return db.AddError(validations.NewError(u,
				"mail",
				fmt.Sprintf("Error validator : mail isn't valid : %s", *u.Mail)))
		}
	}

	return nil
}

// HashPassword hash the password of the user
func (u *User) HashPassword() error {
	var err error
	*u.Password, err = helpers.HashPassword(*u.Password)

	return err
}

// CheckPassword check if the password hashed of the user match the password given in param
func (u *User) CheckPassword(password string) error {
	return helpers.CheckHashPassword(*u.Password, password)
}
