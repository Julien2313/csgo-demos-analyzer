package model

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestDemo_ParseDemoInfoFromFile(t *testing.T) {
	pathDemoInfo := "../../csgo-stock-demo/demoInfo.dem.info"

	demo, err := ParseDemoInfoFromFile(pathDemoInfo)
	require.NoError(t, err)

	assert.Equal(t, int64(3428570545837310244), *demo.ID)
	assert.Equal(t, int64(1596552574), demo.Date.Unix())
	assert.Equal(t, "http://replay132.valve.net/730/003428575878039208099_0406538403.dem.bz2", *demo.ReplayURL)

	demo, err = ParseDemoInfoFromFile("wrongpath")
	assert.Nil(t, demo)
	assert.Error(t, err)
}
