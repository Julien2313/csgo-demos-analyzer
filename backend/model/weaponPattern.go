package model

import (
	"math/rand"

	"github.com/google/uuid"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
)

// WeaponPattern pattern of a weapon
type WeaponPattern struct {
	UUID uuid.UUID `gorm:"primaryKey;default:uuid_generate_v4();index"`

	PlayerStatsID *int64       `gorm:"not null"`
	PlayerStats   *PlayerStats `gorm:"foreignKey:PlayerStatsID"`

	BulletStats []*BulletStats `gorm:"foreignKey:WeaponPatternUUID"`

	WeaponType common.EquipmentType `gorm:"not null"`
}

// GenerateRandomsWeaponPattern generate random WeaponPattern
func GenerateRandomsWeaponPattern() *WeaponPattern {
	return &WeaponPattern{
		BulletStats: GenerateRandomsBulletsStats(),
		WeaponType: []common.EquipmentType{
			common.EqAK47,
			common.EqM4A4,
			common.EqAUG,
			common.EqM4A1,
			common.EqSG553,
			common.EqFamas,
			common.EqGalil,
			common.EqMP9,
			common.EqMac10,
		}[rand.Intn(9)],
	}
}

// GenerateRandomsWeaponPatterns generate randoms WeaponPattern
func GenerateRandomsWeaponPatterns() []*WeaponPattern {
	var weaponPatterns []*WeaponPattern

	for numWeaponPattern := 0; numWeaponPattern < 3; numWeaponPattern++ {
		weaponPatterns = append(weaponPatterns, GenerateRandomsWeaponPattern())
	}

	return weaponPatterns
}
