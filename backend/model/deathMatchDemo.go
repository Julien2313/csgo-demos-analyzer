package model

import (
	"math/rand"
	"time"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

// DeathMatchDemo data about the deathmatch
type DeathMatchDemo struct {
	ID        *int64 `gorm:"primaryKey;autoIncrement:false"`
	CreatedAt *time.Time
	UpdatedAt *time.Time
	HashFile  []byte `gorm:"unique"`

	UserID *uint `gorm:"not null"`
	User   *User

	VersionAnalyzer *int64 `gorm:"not null"`

	DeathMatchPlayers []*DeathMatchPlayer `gorm:"constraint:OnDelete:CASCADE;foreignKey:DeathMatchDemoID"`

	Date    *time.Time `gorm:"not null"`
	MapName *string    `gorm:""`

	Path *string `gorm:"not null; unique"`
	Name *string `gorm:"not null; unique"`
}

// GenerateRandomDeathMatchDemoStats generate a random DeathMatchDemo with stats
func GenerateRandomDeathMatchDemoStats(userID uint, steamID string, date time.Time) DeathMatchDemo {
	return DeathMatchDemo{
		ID:                helpers.Int64Ptr(rand.Int63()),
		HashFile:          []byte(helpers.RandomString(16)),
		UserID:            &userID,
		VersionAnalyzer:   helpers.Int64Ptr(rand.Int63()),
		DeathMatchPlayers: GenerateRandomDeathMatchPlayers(steamID),
		Date:              &date,
		Path:              helpers.StrPtr(helpers.RandomString(20)),
		Name:              helpers.StrPtr(helpers.RandomString(20)),
	}
}

// GenerateRandomDeathMatchDemo generate a random DeathMatchDemo
func GenerateRandomDeathMatchDemo(userID uint, steamID string, date time.Time) DeathMatchDemo {
	return DeathMatchDemo{
		ID:              helpers.Int64Ptr(rand.Int63()),
		HashFile:        []byte(helpers.RandomString(16)),
		UserID:          &userID,
		VersionAnalyzer: helpers.Int64Ptr(rand.Int63()),
		Date:            &date,
		Path:            helpers.StrPtr(helpers.RandomString(20)),
		Name:            helpers.StrPtr(helpers.RandomString(20)),
	}
}

// GenerateRandomsDeathMatchDemo generate randoms DeathMatchDemo
func GenerateRandomsDeathMatchDemo(userID uint, steamID string, date time.Time, nbrDemos int) []DeathMatchDemo {
	var dmDemos []DeathMatchDemo
	for numDemo := 0; numDemo < nbrDemos; numDemo++ {
		dmDemos = append(dmDemos, GenerateRandomDeathMatchDemoStats(userID, steamID, date))
	}

	return dmDemos
}
