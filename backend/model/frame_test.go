package model_test

import (
	"encoding/json"
	"testing"

	"github.com/maxatome/go-testdeep/td"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestFrame_BeforeSave(t *testing.T) {
	frame := model.GenerateRandomFrame()
	require.NoError(t, frame.BeforeSave(nil))

	playersCardByte, err := json.Marshal(frame.PlayersCard)
	require.NoError(t, err)
	td.Cmp(t, frame.PlayersCardByte, playersCardByte)
}
