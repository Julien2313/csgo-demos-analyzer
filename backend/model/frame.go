package model

import (
	"encoding/json"
	"fmt"
	"math/rand"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gorm.io/gorm"
)

// Frame reprensent a frame, with the current data of the player, and if there is one,
// an event attached to it
type Frame struct {
	ID *int64 `gorm:"primaryKey"`

	RoundID *int64 `gorm:"not null"`
	Round   *Round

	NumFrame *int `gorm:"not null"`

	Grenades             []*Grenade         `gorm:"-"`
	GrenadesByte         []byte             `gorm:"not null"`
	PlayersCard          []*PlayerCard      `gorm:"-"`
	PlayersCardByte      []byte             `gorm:"not null"`
	PlayersPositions     []*PlayerPositions `gorm:"-"`
	PlayersPositionsByte []byte             `gorm:"not null"`
	BombLastPosDownX     *float64           `gorm:"not null"`
	BombLastPosDownY     *float64           `gorm:"not null"`
	BombLastPosDownZ     *float64           `gorm:"not null"`
	PlayerCarrierSteamID *string            `gorm:"not null"`
	Timer                *string            `gorm:"not null"`
	BombPlanted          *bool              `gorm:"not null"`

	Events     []Event `gorm:"-"`
	EventsByte []byte  `gorm:"not null"`
}

// AfterFind hooks that feed struct Event and unmarshal playercard, as it is stored in []bytes
func (f *Frame) AfterFind(tx *gorm.DB) error {
	if err := json.Unmarshal(f.PlayersCardByte, &f.PlayersCard); err != nil {
		return err
	}
	if len(f.PlayersPositionsByte) > 0 {
		if err := json.Unmarshal(f.PlayersPositionsByte, &f.PlayersPositions); err != nil {
			return err
		}
	}

	return json.Unmarshal(f.GrenadesByte, &f.Grenades)
}

// BeforeSave marshal playercard, as it is stored in []bytes
func (f *Frame) BeforeSave(tx *gorm.DB) error {
	var err error
	f.EventsByte, err = json.Marshal(f.Events)
	if err != nil {
		return err
	}

	f.PlayersCardByte, err = json.Marshal(f.PlayersCard)
	if err != nil {
		return err
	}

	f.GrenadesByte, err = json.Marshal(f.Grenades)
	if err != nil {
		return err
	}

	f.PlayersPositionsByte, err = json.Marshal(f.PlayersPositions)
	if err != nil {
		return err
	}

	return nil
}

// GenerateRandomFrame generate a random Frame
func GenerateRandomFrame() *Frame {
	return &Frame{
		NumFrame:             helpers.IntPtr(rand.Int() % 50000),
		Timer:                helpers.StrPtr(fmt.Sprintf("%2d:%2d", rand.Int()%2, rand.Int()%55)),
		PlayersCard:          GenerateRandomPlayersCard(),
		Grenades:             GenerateRandomGrenades(),
		BombLastPosDownX:     helpers.FloatPtr(rand.Float64()*2000 - 1000),
		BombLastPosDownY:     helpers.FloatPtr(rand.Float64()*2000 - 1000),
		BombLastPosDownZ:     helpers.FloatPtr(rand.Float64()*2000 - 1000),
		PlayerCarrierSteamID: GenerateRandomSteamIDStr(),
		Events:               GenerateRandomEvents(),
		BombPlanted:          helpers.BoolPtr(rand.Int()%2 == 0),
		PlayersPositions:     GenerateRandomPlayersPositions(),
	}
}

// GenerateRandomFrames generate random Frames
func GenerateRandomFrames() []*Frame {
	var frames []*Frame
	for numFrame := 0; numFrame < 10; numFrame++ {
		frames = append(frames, GenerateRandomFrame())
	}

	return frames
}
