package model

import "github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"

// PlayerStats simple type storing player info from the demo
type PlayerStats struct {
	ID *uint `gorm:"primaryKey"`

	TeamStatsID *uint `gorm:"not null"`
	TeamStats   *TeamStats

	MarksID *uint `gorm:"not null"`
	Marks   *Marks

	SteamID  *string       `gorm:"not null"`
	Username *string       `gorm:"not null"`
	Rank     *int          `gorm:""`
	Color    *common.Color `gorm:"not null"`

	ScoreFactsID *uint `gorm:"not null"`
	ScoreFacts   *ScoreFacts
	MVPsFacts    *int `gorm:"not null;default:0"`

	Kills   *int `gorm:"not null"`
	Deaths  *int `gorm:"not null"`
	Assists *int `gorm:"not null"`

	MatrixKills  []*MatrixKills  `gorm:"constraint:OnDelete:CASCADE"`
	MatrixFlashs []*MatrixFlashs `gorm:"constraint:OnDelete:CASCADE"`

	DuelsPlayer []*DuelsPlayer `gorm:"constraint:OnDelete:CASCADE"`

	WeaponPatterns []*WeaponPattern `gorm:"foreignKey:PlayerStatsID"`
}
