package model

import (
	"math/rand"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

// DeathMatchWeaponStats stats about weapon
type DeathMatchWeaponStats struct {
	ID *uint `gorm:"primaryKey"`

	DeathMatchPlayerID *int64            `gorm:"not null"`
	DeathMatchPlayer   *DeathMatchPlayer `gorm:"foreignKey:DeathMatchPlayerID"`

	WeaponID *common.EquipmentType `gorm:"not null"`

	FirstBulletAccuracy *float64 `gorm:"not null"`
	FirstBulletHS       *float64 `gorm:"not null"`
	BulletAccuracy      *float64 `gorm:"not null"`
	HSPercentage        *float64 `gorm:"not null"`

	NbrBulletFired *int `gorm:"not null"`
	NbrBulletHit   *int `gorm:"not null"`
	NbrBulletHS    *int `gorm:"not null"`

	NbrFirstBulletFired *int `gorm:"not null"`
	NbrFirstBulletHit   *int `gorm:"not null"`
	NbrFirstBulletHS    *int `gorm:"not null"`

	AverageVelocityAttacker *float64 `gorm:"not null"`
}

// GenerateRandomWeaponStats generate a random DeathMatchWeaponStats
func GenerateRandomWeaponStats(nbrBulletFired int) *DeathMatchWeaponStats {
	weaponsType := []int{1, 2, 3, 4, 7}
	weaponID := common.EquipmentType(weaponsType[rand.Intn(5)])
	nbrBulletHit := rand.Intn(nbrBulletFired + 1)
	nbrBulletHS := rand.Intn(nbrBulletHit + 1)

	nbrFirstBulletFired := rand.Intn(nbrBulletFired + 1)
	nbrFirstBulletHit := rand.Intn(nbrFirstBulletFired + 1)
	nbrFirstBulletHS := rand.Intn(nbrFirstBulletHit + 1)
	return &DeathMatchWeaponStats{
		FirstBulletAccuracy: helpers.FloatPtr(rand.Float64() * 100),
		FirstBulletHS:       helpers.FloatPtr(rand.Float64() * 100),
		BulletAccuracy:      helpers.FloatPtr(rand.Float64() * 100),
		HSPercentage:        helpers.FloatPtr(rand.Float64() * 100),

		WeaponID: &weaponID,

		NbrBulletFired: &nbrBulletFired,
		NbrBulletHit:   &nbrBulletHit,
		NbrBulletHS:    &nbrBulletHS,

		NbrFirstBulletFired: &nbrFirstBulletFired,
		NbrFirstBulletHit:   &nbrFirstBulletHit,
		NbrFirstBulletHS:    &nbrFirstBulletHS,

		AverageVelocityAttacker: helpers.FloatPtr(rand.Float64() * 100 * 250),
	}
}

// GenerateRandomWeaponsStats generate randoms DeathMatchWeaponStats
func GenerateRandomWeaponsStats(maxNbrBulletFired int) []*DeathMatchWeaponStats {
	var dmWeapons []*DeathMatchWeaponStats

	for numW := 0; numW < rand.Intn(6)+1; numW++ {
		dmWeapons = append(dmWeapons, GenerateRandomWeaponStats(rand.Intn(maxNbrBulletFired)))
	}

	return dmWeapons
}
