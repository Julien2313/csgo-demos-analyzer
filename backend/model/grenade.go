package model

import (
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
)

// GrenadeState represente the state of a grenade
type GrenadeState int

// consts of the stats of the grenades
const (
	Nil GrenadeState = iota
	Thrown
	Start
	Stop
	Explode
	Expired
	Destroyed
)

// Grenade struct that store all the data for a grenade needed to display its state
type Grenade struct {
	Frame *Frame
	ID    int64

	PositionX float64
	PositionY float64
	PositionZ float64

	Fire []float64

	State GrenadeState

	GrenadeType common.EquipmentType // 50x
}

// GenerateRandomGrenade generate randoms Grenade
func GenerateRandomGrenade() *Grenade {
	return &Grenade{}
}

// GenerateRandomGrenades generate a random Grenade
func GenerateRandomGrenades() []*Grenade {
	grenades := []*Grenade{}
	for numGrenade := 0; numGrenade < 10; numGrenade++ {
		grenades = append(grenades, GenerateRandomGrenade())
	}
	return grenades
}
