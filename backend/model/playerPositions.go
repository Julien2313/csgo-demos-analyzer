package model

import (
	"math/rand"
)

// PlayerPositions struct that store the positions of a player
type PlayerPositions struct {
	Frame *Frame

	SteamID        string
	NbrShoots      int
	IsAlive        bool
	IsDefusing     bool
	IsPlanting     bool
	Side           int
	PositionX      float64
	PositionY      float64
	PositionZ      float64
	ViewDirectionX float64
}

// GenerateRandomPlayerPositions generate randoms PlayerPositions
func GenerateRandomPlayerPositions() *PlayerPositions {
	return &PlayerPositions{
		IsAlive:        rand.Int()%2 == 0,
		Side:           rand.Int()%2 + 2,
		PositionX:      rand.Float64()*2000 - 1000,
		PositionY:      rand.Float64()*2000 - 1000,
		PositionZ:      rand.Float64()*2000 - 1000,
		ViewDirectionX: rand.Float64() * 360,
	}
}

// GenerateRandomPlayersPositions generate randoms PlayerPositions
func GenerateRandomPlayersPositions() []*PlayerPositions {
	playersPositions := []*PlayerPositions{}
	for numPlayerPositions := 0; numPlayerPositions < 10; numPlayerPositions++ {
		playersPositions = append(playersPositions, GenerateRandomPlayerPositions())
	}
	return playersPositions
}
