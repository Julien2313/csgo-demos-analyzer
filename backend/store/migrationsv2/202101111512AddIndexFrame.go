package migrationsv2

import (
	"github.com/go-gormigrate/gormigrate/v2"
	"gorm.io/gorm"
)

var v202101111512AddIndexFrame = gormigrate.Migration{
	ID: "1202101111512",
	Migrate: func(tx *gorm.DB) error {
		if err := tx.Exec("CREATE INDEX idx_round_id ON frames (round_id)").Error; err != nil {
			return err
		}

		return nil
	},
	Rollback: func(tx *gorm.DB) error {
		return nil
	},
}
