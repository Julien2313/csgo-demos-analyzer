package migrationsv2

import (
	"github.com/go-gormigrate/gormigrate/v2"
	"gorm.io/gorm"
)

type teamStatsV202101081552 struct {
	ID        *uint                     `gorm:"primaryKey"`
	DuelsTeam []*duelsTeamV202101081552 `gorm:"constraint:OnDelete:CASCADE;foreignKey:TeamStatsID"`
}

func (teamStatsV202101081552) TableName() string {
	return "team_stats"
}

type playerStatsV202101081552 struct {
	ID          *uint                       `gorm:"primaryKey"`
	DuelsPlayer []*duelsPlayerV202101081552 `gorm:"constraint:OnDelete:CASCADE;foreignKey:PlayerStatsID"`
}

func (playerStatsV202101081552) TableName() string {
	return "player_stats"
}

type duelsTeamV202101081552 struct {
	ID *uint `gorm:"primaryKey"`

	TeamStatsID *uint `gorm:"not null"`
	TeamStats   *teamStatsV202101081552

	Matchup *string `gorm:"not null"`

	NbrOccurence *int `gorm:"not null"`
	NbrWin       *int `gorm:"not null"`
}

func (duelsTeamV202101081552) TableName() string {
	return "duels_teams"
}

type duelsPlayerV202101081552 struct {
	ID *uint `gorm:"primaryKey"`

	PlayerStatsID *uint `gorm:"not null"`
	PlayerStats   *playerStatsV202101081552

	Matchup *string `gorm:"not null"`

	NbrOccurence *int `gorm:"not null"`
	NbrWin       *int `gorm:"not null"`
}

func (duelsPlayerV202101081552) TableName() string {
	return "duels_players"
}

var v202101081552AddDuels = gormigrate.Migration{
	ID: "1202101081552",
	Migrate: func(tx *gorm.DB) error {
		if err := tx.AutoMigrate(&teamStatsV202101081552{}); err != nil {
			return err
		}
		if err := tx.AutoMigrate(&playerStatsV202101081552{}); err != nil {
			return err
		}
		if err := tx.AutoMigrate(&duelsTeamV202101081552{}); err != nil {
			return err
		}
		if err := tx.AutoMigrate(&duelsPlayerV202101081552{}); err != nil {
			return err
		}

		return nil
	},
	Rollback: func(tx *gorm.DB) error {
		if err := tx.Migrator().DropTable(duelsTeamV202101081552{}); err != nil {
			return err
		}
		if err := tx.Migrator().DropTable(duelsPlayerV202101081552{}); err != nil {
			return err
		}
		return nil
	},
}
