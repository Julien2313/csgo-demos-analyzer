package migrationsv2

import (
	"time"

	"github.com/go-gormigrate/gormigrate/v2"
	"gorm.io/gorm"
)

type tokenUploadDemoV212002241610 struct {
	ID        *uint `gorm:"primaryKey"`
	CreatedAt *time.Time

	UserID *uint              `gorm:"not null"`
	User   *userV202011092037 `gorm:"foreignKey:UserID"`

	Token     *string `gorm:"not null"`
	IPAllowed *string `gorm:"not null"`

	LastUsed *time.Time
}

func (tokenUploadDemoV212002241610) TableName() string {
	return "token_upload_demos"
}

type userV212002241610 struct {
	ID *uint `gorm:"primaryKey"`

	TokensUpload []*tokenUploadDemoV212002241610 `gorm:"foreignKey:UserID"`
}

func (userV212002241610) TableName() string {
	return "users"
}

var v212002241610AddTokens = gormigrate.Migration{
	ID: "212002241610",
	Migrate: func(tx *gorm.DB) error {
		if err := tx.AutoMigrate(&userV212002241610{}); err != nil {
			return err
		}
		if err := tx.AutoMigrate(&tokenUploadDemoV212002241610{}); err != nil {
			return err
		}

		return nil
	},
	Rollback: func(tx *gorm.DB) error {
		if err := tx.Migrator().DropTable(tokenUploadDemoV212002241610{}); err != nil {
			return err
		}
		return nil
	},
}
