package migrationsv2

import (
	"bufio"
	"bytes"
	"compress/bzip2"
	"compress/gzip"
	"crypto/sha256"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/go-gormigrate/gormigrate/v2"
	"gorm.io/gorm"
)

type demoV212003041950 struct {
	ID       *int64 `gorm:"primaryKey;autoIncrement:false"`
	HashFile []byte `gorm:"unique"`
}

func (demoV212003041950) TableName() string {
	return "demos"
}

var v212003041950AddHashDemo = gormigrate.Migration{
	ID: "212003041950",
	Migrate: func(tx *gorm.DB) error {
		if err := tx.AutoMigrate(&demoV212003041950{}); err != nil {
			return err
		}
		var demos []demoV202011092037
		if err := tx.Where("downloaded = true").Find(&demos).Error; err != nil {
			return err
		}

		for _, demo := range demos {
			fileDemo, err := os.Open(*demo.Path)
			if err != nil {
				return err
			}

			var demoUnzippedReader io.Reader
			switch filepath.Ext(*demo.Name) {
			case ".bz2":
				demoUnzippedReader = bzip2.NewReader(bufio.NewReader(fileDemo))
			case ".gz":
				demoZippedReader, err := gzip.NewReader(bufio.NewReader(fileDemo))
				if err != nil {
					return err
				}

				t, err := ioutil.ReadAll(demoZippedReader)
				if err != nil {
					return err
				}
				demoUnzippedReader = bytes.NewReader(t)
			case ".dem":
				demoUnzippedReader = bufio.NewReader(bufio.NewReader(fileDemo))
			default:
				demoUnzippedReader = bufio.NewReader(bufio.NewReader(fileDemo))
			}

			demoUnzippedBytes, err := ioutil.ReadAll(demoUnzippedReader)
			if err != nil {
				return err
			}

			hasher := sha256.New()
			if _, err := hasher.Write(demoUnzippedBytes); err != nil {
				return err
			}

			demo := demoV212003041950{
				ID: demo.ID,
			}
			if err := tx.Model(&demo).Update("hash_file", hasher.Sum(nil)).Error; err != nil {
				fileDemo.Close()
				return err
			}

			fileDemo.Close()
		}
		return nil
	},
	Rollback: func(tx *gorm.DB) error {
		if err := tx.Migrator().DropColumn(demoV212003041950{}, "hash_file"); err != nil {
			return err
		}
		return nil
	},
}
