package migrationsv2

import (
	"github.com/go-gormigrate/gormigrate/v2"
	"gorm.io/gorm"
)

type marksPerRoundV202104071801 struct {
	ID *uint `gorm:"primaryKey"`

	RoundID *int64              `gorm:"not null"`
	Round   *roundV202104071801 `gorm:"foreignKey:RoundID"`

	SteamID   *string `gorm:"not null"`
	Side      *int    `gorm:"not null"`
	ConstTeam *int    `gorm:"not null"`

	NbrBulletsFired      *int `gorm:"not null"`
	NbrBulletsHit        *int `gorm:"not null"`
	NbrBulletsHS         *int `gorm:"not null"`
	NbrFirstBulletsFired *int `gorm:"not null"`
	NbrFirstBulletsHit   *int `gorm:"not null"`
	NbrFirstBulletsHS    *int `gorm:"not null"`

	Accuracy            *float64 `gorm:"not null"`
	HS                  *float64 `gorm:"not null"`
	FirstBulletAccuracy *float64 `gorm:"not null"`
	FirstBulletHS       *float64 `gorm:"not null"`

	Damage                 *int `gorm:"not null"`
	UtilityDamage          *int `gorm:"not null"`
	AverageVelocityShoots  *int `gorm:"not null"`
	AverageDeltaXCrossHair *int `gorm:"not null"`
	AverageDeltaYCrossHair *int `gorm:"not null"`
	NbrDeaths              *int `gorm:"not null"`
	NbrKills               *int `gorm:"not null"`
}

func (marksPerRoundV202104071801) TableName() string {
	return "marks_per_rounds"
}

type roundV202104071801 struct {
	ID            *uint                         `gorm:"primaryKey"`
	MarksPerRound []*marksPerRoundV202104071801 `gorm:"foreignKey:RoundID"`
}

func (roundV202104071801) TableName() string {
	return "rounds"
}

var v202104071801AddMarksPerRound = gormigrate.Migration{
	ID: "1202104071801",
	Migrate: func(tx *gorm.DB) error {
		if err := tx.AutoMigrate(&marksPerRoundV202104071801{}); err != nil {
			return err
		}
		if err := tx.AutoMigrate(&roundV202104071801{}); err != nil {
			return err
		}
		return nil

	},
	Rollback: func(tx *gorm.DB) error {
		if err := tx.Migrator().DropTable(marksPerRoundV202104071801{}); err != nil {
			return err
		}
		return nil
	},
}
