package migrationsv2

import (
	"github.com/go-gormigrate/gormigrate/v2"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gorm.io/gorm"
)

type weaponMarksV202103141834 struct {
	ID *uint `gorm:"primaryKey"`

	MarksID *uint               `gorm:"not null"`
	Marks   *marksV202103141834 `gorm:"foreignKey:MarksID"`

	WeaponType *common.EquipmentType `gorm:"not null"`

	NbrBulletsFired      *int `gorm:"not null"`
	NbrBulletsHit        *int `gorm:"not null"`
	NbrBulletsHS         *int `gorm:"not null"`
	NbrFirstBulletsFired *int `gorm:"not null"`
	NbrFirstBulletsHit   *int `gorm:"not null"`
	NbrFirstBulletsHS    *int `gorm:"not null"`

	Accuracy            *float64 `gorm:"not null"`
	HS                  *float64 `gorm:"not null"`
	FirstBulletAccuracy *float64 `gorm:"not null"`
	FirstBulletHS       *float64 `gorm:"not null"`

	Damage                 *int `gorm:"not null"`
	AverageVelocityShoots  *int `gorm:"not null"`
	AverageDeltaXCrossHair *int `gorm:"not null"`
	AverageDeltaYCrossHair *int `gorm:"not null"`
	NbrDeaths              *int `gorm:"not null"`
	NbrKills               *int `gorm:"not null"`
}

func (weaponMarksV202103141834) TableName() string {
	return "weapon_marks"
}

type marksV202103141834 struct {
	ID *uint `gorm:"primaryKey"`

	FirstBulletAccuracy *float64 `gorm:""`
	FirstBulletHS       *float64 `gorm:""`

	NbrBulletsFired      *int `gorm:""`
	NbrBulletsHit        *int `gorm:""`
	NbrBulletsHS         *int `gorm:""`
	NbrFirstBulletsFired *int `gorm:""`
	NbrFirstBulletsHit   *int `gorm:""`
	NbrFirstBulletsHS    *int `gorm:""`

	AverageVelocityShoots  *int `gorm:""`
	AverageDeltaXCrossHair *int `gorm:""`
	AverageDeltaYCrossHair *int `gorm:""`

	NbrDeaths *int `gorm:""`
	NbrKills  *int `gorm:""`

	WeaponMarks []*weaponMarksV202103141834 `gorm:"constraint:OnDelete:CASCADE;foreignKey:MarksID"`
}

func (marksV202103141834) TableName() string {
	return "marks"
}

var v202103141834AddWeaponsStatsMM = gormigrate.Migration{
	ID: "1202103141834",
	Migrate: func(tx *gorm.DB) error {
		if err := tx.AutoMigrate(&weaponMarksV202103141834{}); err != nil {
			return err
		}

		if err := tx.Session(&gorm.Session{AllowGlobalUpdate: true}).
			Updates(marksV202103141834{
				FirstBulletAccuracy:    helpers.FloatPtr(0.0),
				FirstBulletHS:          helpers.FloatPtr(0.0),
				NbrBulletsFired:        helpers.IntPtr(0),
				NbrBulletsHit:          helpers.IntPtr(0),
				NbrBulletsHS:           helpers.IntPtr(0),
				NbrFirstBulletsFired:   helpers.IntPtr(0),
				NbrFirstBulletsHit:     helpers.IntPtr(0),
				NbrFirstBulletsHS:      helpers.IntPtr(0),
				AverageVelocityShoots:  helpers.IntPtr(0),
				AverageDeltaXCrossHair: helpers.IntPtr(0),
				AverageDeltaYCrossHair: helpers.IntPtr(0),
				NbrDeaths:              helpers.IntPtr(0),
				NbrKills:               helpers.IntPtr(0),
			}).Error; err != nil {
			return err
		}

		if err := tx.Exec("ALTER TABLE marks ALTER COLUMN average_delta_y_cross_hair SET NOT NULL").Error; err != nil {
			return err
		}

		if err := tx.Exec("ALTER TABLE marks ALTER COLUMN first_bullet_hs SET NOT NULL").Error; err != nil {
			return err
		}

		if err := tx.Exec("ALTER TABLE marks ALTER COLUMN nbr_bullets_hs SET NOT NULL").Error; err != nil {
			return err
		}

		if err := tx.Exec("ALTER TABLE marks ALTER COLUMN nbr_first_bullets_hit SET NOT NULL").Error; err != nil {
			return err
		}

		if err := tx.Exec("ALTER TABLE marks ALTER COLUMN nbr_bullets_fired SET NOT NULL").Error; err != nil {
			return err
		}

		if err := tx.Exec("ALTER TABLE marks ALTER COLUMN nbr_bullets_hit SET NOT NULL").Error; err != nil {
			return err
		}

		if err := tx.Exec("ALTER TABLE marks ALTER COLUMN average_velocity_shoots SET NOT NULL").Error; err != nil {
			return err
		}

		if err := tx.Exec("ALTER TABLE marks ALTER COLUMN nbr_deaths SET NOT NULL").Error; err != nil {
			return err
		}

		if err := tx.Exec("ALTER TABLE marks ALTER COLUMN nbr_kills SET NOT NULL").Error; err != nil {
			return err
		}

		if err := tx.Exec("ALTER TABLE marks ALTER COLUMN first_bullet_accuracy SET NOT NULL").Error; err != nil {
			return err
		}

		if err := tx.Exec("ALTER TABLE marks ALTER COLUMN nbr_first_bullets_fired SET NOT NULL").Error; err != nil {
			return err
		}

		if err := tx.Exec("ALTER TABLE marks ALTER COLUMN nbr_first_bullets_hs SET NOT NULL").Error; err != nil {
			return err
		}

		if err := tx.Exec("ALTER TABLE marks ALTER COLUMN average_delta_x_cross_hair SET NOT NULL").Error; err != nil {
			return err
		}

		if err := tx.Session(&gorm.Session{AllowGlobalUpdate: true}).
			Model(&demoV212003041950{}).
			Update("version_analyzer", "1").
			Error; err != nil {
			return err
		}
		return nil
	},
	Rollback: func(tx *gorm.DB) error {
		if err := tx.Migrator().DropTable(weaponMarksV202103141834{}); err != nil {
			return err
		}
		return nil
	},
}
