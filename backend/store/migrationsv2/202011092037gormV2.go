package migrationsv2

import (
	"time"

	"github.com/go-gormigrate/gormigrate/v2"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"gorm.io/gorm"
)

type userV202011092037 struct {
	ID        *uint `gorm:"primaryKey"`
	CreatedAt *time.Time
	UpdatedAt *time.Time

	SteamID            *string `gorm:""`
	ShareCode          *string `gorm:""`
	AccessTokenHistory *string `gorm:""`
	APIValveWorking    *bool   `gorm:"not null;default:false"`

	Mail     *string `gorm:"not null;unique"`
	Password *string `gorm:"not null"`

	Demos []*demoV202011092037 `gorm:"foreignKey:UserID"`
	// stored in GB
	SizeUsed *float64 `gorm:"not null;default:0"`
}

func (userV202011092037) TableName() string {
	return "users"
}

type demoV202011092037 struct {
	ID        *int64 `gorm:"primaryKey;autoIncrement:false"`
	CreatedAt *time.Time
	UpdatedAt *time.Time

	VersionAnalyzer *int64 `gorm:"not null"`
	Downloaded      *bool  `gorm:"not null"`

	Rounds     []*roundV202011092037     `gorm:"constraint:OnDelete:CASCADE;foreignKey:DemoID"`
	TeamsStats []*teamStatsV202011092037 `gorm:"constraint:OnDelete:CASCADE;foreignKey:DemoID"`

	UserID *uint              `gorm:"not null"`
	User   *userV202011092037 `gorm:"foreignKey:UserID"`

	SteamsIDs []*steamIDV202011092037 `gorm:"constraint:OnDelete:CASCADE;foreignKey:DemoID"`

	Date    *time.Time `gorm:"not null"`
	MapName *string    `gorm:""`

	Path *string `gorm:"not null"`
	Name *string `gorm:"not null"`
	Size *int64  `gorm:"not null"`

	ReplayURL *string `gorm:""`
}

func (demoV202011092037) TableName() string {
	return "demos"
}

type matrixKillsV202011092037 struct {
	ID *uint `gorm:"primaryKey"`

	PlayerStatsID *uint                     `gorm:"not null"`
	PlayerStats   *playerStatsV202011092037 `gorm:"foreignKey:PlayerStatsID"`

	KilledSteamID  *string `gorm:"not null"`
	KilledUsername *string `gorm:"not null"`

	NbrKills *int `gorm:"not null"`
}

func (matrixKillsV202011092037) TableName() string {
	return "matrix_kills"
}

type matrixFlashsV202011092037 struct {
	ID *uint `gorm:"primaryKey"`

	PlayerStatsID *uint                     `gorm:"not null"`
	PlayerStats   *playerStatsV202011092037 `gorm:"foreignKey:PlayerStatsID"`

	FlashedSteamID  *string `gorm:"not null"`
	FlashedUsername *string `gorm:"not null"`

	NbrSecFlashs *float64 `gorm:"not null"`
}

func (matrixFlashsV202011092037) TableName() string {
	return "matrix_flashs"
}

type marksV202011092037 struct {
	ID *uint `gorm:"primaryKey"`

	Accuracy           *float64 `gorm:"not null"`
	HS                 *float64 `gorm:"not null"`
	GrenadesValueDeath *float64 `gorm:"not null"`
	Damage             *int     `gorm:"not null"`
	UtilityDamage      *int     `gorm:"not null"`
}

func (marksV202011092037) TableName() string {
	return "marks"
}

type playerStatsV202011092037 struct {
	ID *uint `gorm:"primaryKey"`

	TeamStatsID *uint                   `gorm:"not null"`
	TeamStats   *teamStatsV202011092037 `gorm:"foreignKey:TeamStatsID"`

	MarksID *uint               `gorm:"not null"`
	Marks   *marksV202011092037 `gorm:"foreignKey:MarksID"`

	SteamID  *string       `gorm:"not null"`
	Username *string       `gorm:"not null"`
	Rank     *int          `gorm:""`
	Color    *common.Color `gorm:"not null"`

	ScoreFactsID *uint                    `gorm:"not null"`
	ScoreFacts   *scoreFactsV202011092037 `gorm:"foreignKey:ScoreFactsID"`
	MVPsFacts    *int                     `gorm:"not null;default:0"`

	Kills   *int `gorm:"not null"`
	Deaths  *int `gorm:"not null"`
	Assists *int `gorm:"not null"`

	MatrixKills  []*matrixKillsV202011092037  `gorm:"constraint:OnDelete:CASCADE;foreignKey:PlayerStatsID"`
	MatrixFlashs []*matrixFlashsV202011092037 `gorm:"constraint:OnDelete:CASCADE;foreignKey:PlayerStatsID"`
}

func (playerStatsV202011092037) TableName() string {
	return "player_stats"
}

type scoreFactsV202011092037 struct {
	ID *uint `gorm:"primaryKey"`

	RoundID *uint
	Round   *roundV202011092037 `gorm:"foreignKey:RoundID"`

	SteamID *string `gorm:"not null"`

	RatioScore *float64 `gorm:"not null"`
	TotalScore *float64 `gorm:"not null"`

	DamageScore     *float64 `gorm:"not null"`
	KillScore       *float64 `gorm:"not null"`
	DeathScore      *float64 `gorm:"not null"`
	AssistKillScore *float64 `gorm:"not null"`

	DropScore        *float64 `gorm:"not null"`
	FlashScore       *float64 `gorm:"not null"`
	RevangedScore    *float64 `gorm:"not null"`
	BombDefuseScore  *float64 `gorm:"not null"`
	BombPlantedScore *float64 `gorm:"not null"`
}

func (scoreFactsV202011092037) TableName() string {
	return "score_facts"
}

type steamIDV202011092037 struct {
	ID      *uint   `gorm:"primaryKey"`
	SteamID *string `gorm:"not null"`

	DemoID *int64             `gorm:"not null"`
	Demo   *demoV202011092037 `gorm:"foreignKey:DemoID"`
}

func (steamIDV202011092037) TableName() string {
	return "steam_ids"
}

type teamStatsV202011092037 struct {
	ID *uint `gorm:"primaryKey"`

	DemoID *int64             `gorm:"not null"`
	Demo   *demoV202011092037 `gorm:"foreignKey:DemoID"`

	PlayersStats []*playerStatsV202011092037 `gorm:"constraint:OnDelete:CASCADE;foreignKey:TeamStatsID"`

	Score *int `gorm:"not null"`
}

func (teamStatsV202011092037) TableName() string {
	return "team_stats"
}

type roundV202011092037 struct {
	ID *uint `gorm:"primaryKey"`

	DemoID *int64             `gorm:"not null"`
	Demo   *demoV202011092037 `gorm:"foreignKey:DemoID"`

	ScoreFactsPerPlayer []*scoreFactsV202011092037 `gorm:"constraint:OnDelete:CASCADE;foreignKey:RoundID"`

	NumRound *int                  `gorm:"not null"`
	Frames   []*frameV202011092037 `gorm:"constraint:OnDelete:CASCADE;foreignKey:RoundID"`
	SideWin  *int                  `gorm:""`
}

func (roundV202011092037) TableName() string {
	return "rounds"
}

type frameV202011092037 struct {
	ID *uint `gorm:"primaryKey"`

	RoundID *int64              `gorm:"not null"`
	Round   *roundV202011092037 `gorm:"foreignKey:RoundID"`

	NumFrame *int `gorm:"not null"`

	PlayersCardByte      []byte   `gorm:"not null"`
	BombLastPosDownX     *float64 `gorm:"not null"`
	BombLastPosDownY     *float64 `gorm:"not null"`
	BombLastPosDownZ     *float64 `gorm:"not null"`
	PlayerCarrierSteamID *string  `gorm:"not null"`

	EventID   int64
	EventType string
}

func (frameV202011092037) TableName() string {
	return "frames"
}

var v202011092037gormV2 = gormigrate.Migration{
	ID: "1202011092037",
	Migrate: func(tx *gorm.DB) error {
		if err := tx.Migrator().DropTable(matrixKillsV202011092037{}); err != nil {
			return err
		}
		if err := tx.Migrator().DropTable(matrixFlashsV202011092037{}); err != nil {
			return err
		}
		if err := tx.Migrator().DropTable(marksV202011092037{}); err != nil {
			return err
		}
		if err := tx.Migrator().DropTable(playerStatsV202011092037{}); err != nil {
			return err
		}
		if err := tx.Migrator().DropTable(scoreFactsV202011092037{}); err != nil {
			return err
		}
		if err := tx.Migrator().DropTable(roundV202011092037{}); err != nil {
			return err
		}
		if err := tx.Migrator().DropTable(steamIDV202011092037{}); err != nil {
			return err
		}
		if err := tx.Migrator().DropTable(teamStatsV202011092037{}); err != nil {
			return err
		}
		if err := tx.Migrator().DropTable(frameV202011092037{}); err != nil {
			return err
		}

		if err := tx.AutoMigrate(&userV202011092037{}); err != nil {
			return err
		}
		if err := tx.AutoMigrate(&demoV202011092037{}); err != nil {
			return err
		}
		if err := tx.AutoMigrate(&marksV202011092037{}); err != nil {
			return err
		}
		if err := tx.AutoMigrate(&scoreFactsV202011092037{}); err != nil {
			return err
		}
		if err := tx.AutoMigrate(&playerStatsV202011092037{}); err != nil {
			return err
		}
		if err := tx.AutoMigrate(&matrixKillsV202011092037{}); err != nil {
			return err
		}
		if err := tx.AutoMigrate(&matrixFlashsV202011092037{}); err != nil {
			return err
		}
		if err := tx.AutoMigrate(&steamIDV202011092037{}); err != nil {
			return err
		}
		if err := tx.AutoMigrate(&teamStatsV202011092037{}); err != nil {
			return err
		}
		if err := tx.AutoMigrate(&frameV202011092037{}); err != nil {
			return err
		}

		if err := tx.Session(&gorm.Session{AllowGlobalUpdate: true}).
			Model(&demoV202011092037{}).Update("version_analyzer", 1).Error; err != nil {
			return err
		}

		return nil
	},
	Rollback: func(tx *gorm.DB) error {
		if err := tx.Migrator().DropTable(userV202011092037{}); err != nil {
			return err
		}
		if err := tx.Migrator().DropTable(demoV202011092037{}); err != nil {
			return err
		}
		if err := tx.Migrator().DropTable(matrixKillsV202011092037{}); err != nil {
			return err
		}
		if err := tx.Migrator().DropTable(matrixFlashsV202011092037{}); err != nil {
			return err
		}
		if err := tx.Migrator().DropTable(marksV202011092037{}); err != nil {
			return err
		}
		if err := tx.Migrator().DropTable(playerStatsV202011092037{}); err != nil {
			return err
		}
		if err := tx.Migrator().DropTable(scoreFactsV202011092037{}); err != nil {
			return err
		}
		if err := tx.Migrator().DropTable(roundV202011092037{}); err != nil {
			return err
		}
		if err := tx.Migrator().DropTable(steamIDV202011092037{}); err != nil {
			return err
		}
		if err := tx.Migrator().DropTable(teamStatsV202011092037{}); err != nil {
			return err
		}
		if err := tx.Migrator().DropTable(frameV202011092037{}); err != nil {
			return err
		}

		return nil
	},
}
