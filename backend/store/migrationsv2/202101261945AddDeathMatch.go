package migrationsv2

import (
	"time"

	"github.com/go-gormigrate/gormigrate/v2"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"gorm.io/gorm"
)

type dmWeaponStatsV202101261945 struct {
	ID *uint `gorm:"primaryKey"`

	DeathMatchPlayerID *int64                 `gorm:"not null"`
	DeathMatchPlayer   *dmPlayerV202101261945 `gorm:"foreignKey:DeathMatchPlayerID"`

	WeaponID *common.EquipmentType `gorm:"not null"`

	FirstBulletAccuracy *float64 `gorm:"not null"`
	FirstBulletHS       *float64 `gorm:"not null"`
	BulletAccuracy      *float64 `gorm:"not null"`
	HSPercentage        *float64 `gorm:"not null"`

	NbrBulletFired *int `gorm:"not null"`
	NbrBulletHit   *int `gorm:"not null"`
	NbrBulletHS    *int `gorm:"not null"`

	NbrFirstBulletFired *int `gorm:"not null"`
	NbrFirstBulletHit   *int `gorm:"not null"`
	NbrFirstBulletHS    *int `gorm:"not null"`

	AverageVelocityAttacker *float64 `gorm:"not null"`
}

func (dmWeaponStatsV202101261945) TableName() string {
	return "death_match_weapon_stats"
}

type dmPlayerV202101261945 struct {
	ID      *uint   `gorm:"primaryKey"`
	SteamID *string `gorm:"not null"`

	DeathMatchDemoID *int64               `gorm:"not null"`
	DeathMatchDemo   *dmDemoV202101261945 `gorm:"foreignKey:DeathMatchDemoID"`

	WeaponsStats []*dmWeaponStatsV202101261945 `gorm:"constraint:OnDelete:CASCADE;foreignKey:DeathMatchPlayerID"`

	FirstBulletAccuracy *float64 `gorm:"not null"`
	FirstBulletHS       *float64 `gorm:"not null"`
	BulletAccuracy      *float64 `gorm:"not null"`
	HSPercentage        *float64 `gorm:"not null"`

	NbrBulletFired *int `gorm:"not null"`
	NbrBulletHit   *int `gorm:"not null"`
	NbrBulletHS    *int `gorm:"not null"`

	NbrFirstBulletFired *int `gorm:"not null"`
	NbrFirstBulletHit   *int `gorm:"not null"`
	NbrFirstBulletHS    *int `gorm:"not null"`

	AverageVelocityAttacker *float64 `gorm:"not null"`
}

func (dmPlayerV202101261945) TableName() string {
	return "death_match_players"
}

type dmDemoV202101261945 struct {
	ID        *int64 `gorm:"primaryKey;autoIncrement:false"`
	CreatedAt *time.Time
	UpdatedAt *time.Time
	HashFile  []byte `gorm:"unique"`

	UserID *uint              `gorm:"not null"`
	User   *userV202011092037 `gorm:"foreignKey:UserID"`

	VersionAnalyzer *int64 `gorm:"not null"`

	DeathMatchPlayers []*dmPlayerV202101261945 `gorm:"constraint:OnDelete:CASCADE;foreignKey:DeathMatchDemoID"`

	Date    *time.Time `gorm:"not null"`
	MapName *string    `gorm:""`

	Path *string `gorm:"not null; unique"`
	Name *string `gorm:"not null; unique"`
}

func (dmDemoV202101261945) TableName() string {
	return "death_match_demos"
}

var v202101261945AddDeathMatch = gormigrate.Migration{
	ID: "1202101261945",
	Migrate: func(tx *gorm.DB) error {
		if err := tx.AutoMigrate(&dmWeaponStatsV202101261945{}); err != nil {
			return err
		}
		if err := tx.AutoMigrate(&dmPlayerV202101261945{}); err != nil {
			return err
		}
		if err := tx.AutoMigrate(&dmDemoV202101261945{}); err != nil {
			return err
		}

		return nil
	},
	Rollback: func(tx *gorm.DB) error {
		if err := tx.Migrator().DropTable(dmWeaponStatsV202101261945{}); err != nil {
			return err
		}
		if err := tx.Migrator().DropTable(dmPlayerV202101261945{}); err != nil {
			return err
		}
		if err := tx.Migrator().DropTable(dmDemoV202101261945{}); err != nil {
			return err
		}
		return nil
	},
}
