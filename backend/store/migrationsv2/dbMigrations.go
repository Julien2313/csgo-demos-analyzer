package migrationsv2

import "github.com/go-gormigrate/gormigrate/v2"

// Migrations List of migrations to do on startup
var Migrations = []*gormigrate.Migration{
	&v202011092037gormV2,
	&v202011252219AddGrenades,
	&v202012251124AddChatMessage,
	&v202101081552AddDuels,
	&v202101111512AddIndexFrame,
	&v202101112259AddHeatMap,
	&v202101261945AddDeathMatch,
	&v212002241610AddTokens,
	&v212003041950AddHashDemo,
	&v202103141834AddWeaponsStatsMM,
	&v202104071801AddMarksPerRound,
	&v1202104272019AddSourceDemo,
	&v1202106012016AddSprayPattern,
}
