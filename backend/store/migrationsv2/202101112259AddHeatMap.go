package migrationsv2

import (
	"time"

	"github.com/go-gormigrate/gormigrate/v2"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"gorm.io/gorm"
)

type roundV202101112259 struct {
	ID *uint `gorm:"primaryKey"`

	HeatMapKills []*heatMapKillV202101112259 `gorm:"foreignKey:RoundID; constraint:OnDelete:CASCADE"`
	HeatMapDmgs  []*heatMapKillV202101112259 `gorm:"foreignKey:RoundID; constraint:OnDelete:CASCADE"`
}

func (roundV202101112259) TableName() string {
	return "rounds"
}

type heatMapKillV202101112259 struct {
	ID *int64 `gorm:"primaryKey"`

	RoundID *int64              `gorm:"not null"`
	Round   *roundV202101112259 `gorm:"foreignKey:RoundID"`

	DurationSinceRoundBegan *time.Duration `gorm:"not null"`

	WeaponKiller  *common.EquipmentType `gorm:"index"`
	SteamIDKiller *string               `gorm:"index"`
	SideKiller    *int
	KillerPosX    *float64
	KillerPosY    *float64

	ActiveWeaponVictim *common.EquipmentType `gorm:"index"`
	SteamIDVictim      *string               `gorm:"index"`
	SideVictim         *int
	VictimPosX         *float64
	VictimPosY         *float64
}

func (heatMapKillV202101112259) TableName() string {
	return "heat_map_kills"
}

type heatMapDmgV202101112259 struct {
	ID *int64 `gorm:"primaryKey"`

	RoundID *int64              `gorm:"not null"`
	Round   *roundV202101112259 `gorm:"foreignKey:RoundID"`

	DurationSinceRoundBegan *time.Duration `gorm:"not null"`

	WeaponShooter  *common.EquipmentType `gorm:"index"`
	SteamIDShooter *string               `gorm:"index"`
	SideShooter    *int
	ShooterPosX    *float64
	ShooterPosY    *float64

	ActiveWeaponVictim *common.EquipmentType `gorm:"index"`
	SteamIDVictim      *string               `gorm:"index"`
	SideVictim         *int
	VictimPosX         *float64
	VictimPosY         *float64

	Dmg *int `gorm:"not null"`
}

func (heatMapDmgV202101112259) TableName() string {
	return "heat_map_dmgs"
}

var v202101112259AddHeatMap = gormigrate.Migration{
	ID: "1202101112259",
	Migrate: func(tx *gorm.DB) error {
		if err := tx.AutoMigrate(&roundV202101112259{}); err != nil {
			return err
		}
		if err := tx.AutoMigrate(&heatMapKillV202101112259{}); err != nil {
			return err
		}
		if err := tx.AutoMigrate(&heatMapDmgV202101112259{}); err != nil {
			return err
		}
		return nil
	},
	Rollback: func(tx *gorm.DB) error {
		if err := tx.Migrator().DropTable(heatMapKillV202101112259{}); err != nil {
			return err
		}
		if err := tx.Migrator().DropTable(heatMapDmgV202101112259{}); err != nil {
			return err
		}
		return nil
	},
}
