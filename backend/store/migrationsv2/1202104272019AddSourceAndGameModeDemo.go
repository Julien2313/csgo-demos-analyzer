package migrationsv2

import (
	"github.com/go-gormigrate/gormigrate/v2"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gorm.io/gorm"
)

type demoV202104272019 struct {
	Sharecode *string `gorm:"unique"`
	Source    byte    `gorm:""`
	GameMode  byte    `gorm:""`
}

func (demoV202104272019) TableName() string {
	return "demos"
}

var v1202104272019AddSourceDemo = gormigrate.Migration{
	ID: "202104272019",
	Migrate: func(tx *gorm.DB) error {
		if err := tx.AutoMigrate(&demoV202104272019{}); err != nil {
			return err
		}

		if err := tx.Session(&gorm.Session{AllowGlobalUpdate: true}).
			Model(&demoV202104272019{}).Update("source", model.Valve).Error; err != nil {
			return err
		}

		if err := tx.Exec("ALTER TABLE demos ALTER COLUMN source SET NOT NULL").Error; err != nil {
			return err
		}

		if err := tx.Session(&gorm.Session{AllowGlobalUpdate: true}).
			Model(&demoV202104272019{}).Update("game_mode", model.Competitive).Error; err != nil {
			return err
		}

		if err := tx.Exec("ALTER TABLE demos ALTER COLUMN game_mode SET NOT NULL").Error; err != nil {
			return err
		}

		return nil
	},
	Rollback: func(tx *gorm.DB) error {
		if err := tx.Migrator().DropColumn(demoV212003041950{}, "source"); err != nil {
			return err
		}
		if err := tx.Migrator().DropColumn(demoV212003041950{}, "game_mode"); err != nil {
			return err
		}
		if err := tx.Migrator().DropColumn(demoV212003041950{}, "sharecode"); err != nil {
			return err
		}
		return nil
	},
}
