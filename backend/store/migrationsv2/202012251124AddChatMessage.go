package migrationsv2

import (
	"github.com/go-gormigrate/gormigrate/v2"
	"gorm.io/gorm"
)

type roundV202012251124 struct {
	ID          *uint                       `gorm:"primaryKey"`
	ChatMessage []*chatMessageV202012251124 `gorm:"foreignKey:RoundID"`
}

func (roundV202012251124) TableName() string {
	return "rounds"
}

type chatMessageV202012251124 struct {
	ID *uint `gorm:"primaryKey"`

	RoundID *int64              `gorm:"not null"`
	Round   *roundV202011092037 `gorm:"foreignKey:RoundID"`

	Sender     *string `gorm:"not null"`
	SideSender *int    `gorm:"not null"`
	Message    *string `gorm:"not null"`
	IsChatAll  *bool   `gorm:"not null"`
}

func (chatMessageV202012251124) TableName() string {
	return "chat_messages"
}

var v202012251124AddChatMessage = gormigrate.Migration{
	ID: "1202012251124",
	Migrate: func(tx *gorm.DB) error {
		if err := tx.AutoMigrate(&chatMessageV202012251124{}); err != nil {
			return err
		}
		if err := tx.AutoMigrate(&roundV202012251124{}); err != nil {
			return err
		}

		return nil
	},
	Rollback: func(tx *gorm.DB) error {
		if err := tx.Migrator().DropTable(chatMessageV202012251124{}); err != nil {
			return err
		}
		return nil
	},
}
