package migrationsv2

import (
	"github.com/go-gormigrate/gormigrate/v2"
	"gorm.io/gorm"
)

type frameV202011252219 struct {
	GrenadesByte         []byte  `gorm:"not null"`
	PlayersPositionsByte []byte  `gorm:"not null;default:'{}'"`
	Timer                *string `gorm:"not null"`
	BombPlanted          *bool   `gorm:"not null"`

	EventsByte []byte `gorm:"not null;default:'{}'"`
}

func (frameV202011252219) TableName() string {
	return "frames"
}

var v202011252219AddGrenades = gormigrate.Migration{
	ID: "1202011252219",
	Migrate: func(tx *gorm.DB) error {
		if err := tx.AutoMigrate(&frameV202011252219{}); err != nil {
			return err
		}
		if err := tx.Migrator().DropColumn(frameV202011252219{}, "event_id"); err != nil {
			return err
		}
		if err := tx.Migrator().DropColumn(frameV202011252219{}, "event_type"); err != nil {
			return err
		}

		return nil
	},
	Rollback: func(tx *gorm.DB) error {
		if err := tx.Migrator().DropColumn(frameV202011252219{}, "grenades_byte"); err != nil {
			return err
		}
		if err := tx.Migrator().DropColumn(frameV202011252219{}, "players_positions_byte"); err != nil {
			return err
		}
		if err := tx.Migrator().DropColumn(frameV202011252219{}, "timer"); err != nil {
			return err
		}
		if err := tx.Migrator().DropColumn(frameV202011252219{}, "bomb_planted"); err != nil {
			return err
		}
		if err := tx.Migrator().DropColumn(frameV202011252219{}, "events_byte"); err != nil {
			return err
		}
		if err := tx.Migrator().AddColumn(frameV202011092037{}, "event_id"); err != nil {
			return err
		}
		if err := tx.Migrator().AddColumn(frameV202011092037{}, "event_type"); err != nil {
			return err
		}
		return nil
	},
}
