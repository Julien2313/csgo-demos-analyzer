package migrationsv2

import (
	"github.com/go-gormigrate/gormigrate/v2"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"gorm.io/gorm"
)

type playerStatsV202106012016 struct {
	ID             *uint                         `gorm:"primaryKey"`
	WeaponPatterns []*weaponPatternV202106012016 `gorm:"foreignKey:PlayerStatsID"`
}

func (playerStatsV202106012016) TableName() string {
	return "player_stats"
}

type weaponPatternV202106012016 struct {
	UUID *string `gorm:"primaryKey;default:uuid_generate_v4();index"`

	PlayerStatsID *int64                    `gorm:"not null"`
	PlayerStats   *playerStatsV202106012016 `gorm:"foreignKey:PlayerStatsID"`

	BulletStats []*bulletStatsV202106012016 `gorm:"foreignKey:WeaponPatternUUID"`

	WeaponType common.EquipmentType `gorm:"not null"`
}

func (weaponPatternV202106012016) TableName() string {
	return "weapon_patterns"
}

type bulletStatsV202106012016 struct {
	UUID *string `gorm:"primaryKey;default:uuid_generate_v4();index"`

	WeaponPatternUUID *string                     `gorm:"not null"`
	WeaponPattern     *weaponPatternV202106012016 `gorm:"foreignKey:WeaponPatternUUID"`

	BulletNumber     int     `gorm:"not null"`
	NbrBulletFired   int64   `gorm:"not null"`
	NbrBulletHit     int64   `gorm:"not null"`
	NbrBulletHitHS   int64   `gorm:"not null"`
	NbrBulletKill    int64   `gorm:"not null"`
	CumulativeDeltaX float64 `gorm:"not null"`
	CumulativeDeltaY float64 `gorm:"not null"`
}

func (bulletStatsV202106012016) TableName() string {
	return "bullet_stats"
}

var v1202106012016AddSprayPattern = gormigrate.Migration{
	ID: "202106012016",
	Migrate: func(tx *gorm.DB) error {
		if err := tx.AutoMigrate(&playerStatsV202106012016{}); err != nil {
			return err
		}
		if err := tx.AutoMigrate(&weaponPatternV202106012016{}); err != nil {
			return err
		}
		if err := tx.AutoMigrate(&bulletStatsV202106012016{}); err != nil {
			return err
		}
		return nil
	},
	Rollback: func(tx *gorm.DB) error {
		if err := tx.Migrator().DropTable(weaponPatternV202106012016{}); err != nil {
			return err
		}
		if err := tx.Migrator().DropTable(bulletStatsV202106012016{}); err != nil {
			return err
		}
		return nil
	},
}
