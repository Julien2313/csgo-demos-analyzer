package store

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-gormigrate/gormigrate/v2"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store/migrationsv2"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

// SchemaTest used for tests
const SchemaTest = "test"

// SchemaTestDao used to test the controller
const SchemaTestDao = "testdao"

// SchemaTestController used to test the dao
const SchemaTestController = "testcontroller"

// SchemaTestModel used to test the model
const SchemaTestModel = "testmodel"

// SchemaTestAnalyzerDownload used to test the downloaders
const SchemaTestAnalyzerDownload = "testanalyzerdownload"

// SchemaTestAnalyzer used to test the analyzer
const SchemaTestAnalyzer = "testanalyzer"

// SchemaTestDemoClassifier used to test the demo classifier
const SchemaTestDemoClassifier = "testdemoclassifier"

// SchemaTestServer used to test the server
const SchemaTestServer = "testserver"

// SchemaDev used for dev
const SchemaDev = "dev"

// SchemaProd used for prod
const SchemaProd = "public"

// InitDB initialisation de la connexion à la DB
func InitDB(reset bool, schema string, logDB *custlogger.Logger) *gorm.DB {
	db := CreateDBInstance(logDB, schema)
	myOptions := gormigrate.DefaultOptions

	if reset {
		err := db.Exec(fmt.Sprintf("DROP SCHEMA IF EXISTS %s CASCADE; CREATE SCHEMA %s;", schema, schema)).Error
		if err != nil {
			logDB.Errorf(err, "-----ATTENTION !-----\n-----\n-----\n-----\n-----\n----- "+
				"Erreur pendant le rollback : %s "+
				"\n-----\n-----\n-----\n-----\n---------------------", err)
			panic(err)
		}
		logDB.Tracef("-----ATTENTION !-----\n-----\n-----\n-----\n-----\n----- " +
			"La base a été réinitialisée, MOFO \n-----\n-----\n-----\n-----\n---------------------")
	}

	if err := db.Exec(`CREATE EXTENSION IF NOT EXISTS "uuid-ossp";`).Error; err != nil {
		logDB.Errorf(err, "-----ATTENTION !-----\n-----\n-----\n-----\n-----\n----- "+
			"Error while installing extension : %s "+
			"\n-----\n-----\n-----\n-----\n---------------------", err)
		panic(err)
	}

	if err := db.Exec("select uuid_generate_v4();").Error; err != nil {
		if err := db.Exec(`DROP EXTENSION "uuid-ossp" cascade;`).Error; err != nil {
			panic(err)
		}
		if err := db.Exec(`CREATE EXTENSION IF NOT EXISTS "uuid-ossp";`).Error; err != nil {
			panic(err)
		}
		if err := db.Exec("select uuid_generate_v4();").Error; err != nil {
			panic(err)
		}

		if !reset {
			if err := resetDefaultUUIDValue(db); err != nil {
				panic(err)
			}
		}
	}

	m := gormigrate.New(db, myOptions, migrationsv2.Migrations)
	if err := m.Migrate(); err != nil {
		logDB.Errorf(err, "Could not migrate : %v", err)
		panic(err)
	}

	return db
}

func resetDefaultUUIDValue(db *gorm.DB) error {
	if err := db.Exec("ALTER TABLE weapon_patterns ALTER COLUMN uuid SET DEFAULT uuid_generate_v4();").Error; err != nil {
		return err
	}
	if err := db.Exec("ALTER TABLE bullet_stats ALTER COLUMN uuid SET DEFAULT uuid_generate_v4();").Error; err != nil {
		return err
	}

	return nil
}

// CreateDBInstance Création de l'instance de la DB
func CreateDBInstance(logDB *custlogger.Logger, schema string) *gorm.DB {
	const (
		host     = "127.0.0.1"
		port     = "5431"
		user     = "csgo"
		dbname   = "csgo"
		password = "postgrescsgo"
		sslmode  = "disable"
	)

	var (
		err error
		db  *gorm.DB
	)

	if schema == "" {
		schema = "test"
	}

	var loggerDB logger.Interface

	if logDB == nil || logDB.DefaultOutput == nil {
		loggerDB = NewCustomLogger(
			log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
			Config{
				SlowThreshold: 1000 * time.Millisecond,
				LogLevel:      logger.Info,
				Colorful:      false,
			},
		)
	} else {
		loggerDB = NewCustomLogger(
			log.New(logDB.DefaultOutput, "\r\n", 0), // io writer
			Config{
				SlowThreshold: 1000 * time.Millisecond,
				LogLevel:      logger.Info,
				Colorful:      false,
			},
		)
	}

	if db, err = gorm.Open(postgres.Open(
		fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=%s search_path=%s",
			host,
			port,
			user,
			dbname,
			password,
			sslmode,
			schema)), &gorm.Config{Logger: loggerDB}); err != nil {

		logDB.Errorf(err, "Error initializing db on 5431 : %v", err)

		if db, err = gorm.Open(postgres.Open(
			"host=postgres port=5432 user=csgo dbname=csgo password=postgrescsgo sslmode=disable"),
			&gorm.Config{Logger: loggerDB}); err != nil {
			logDB.Errorf(err, "Error initializing gitlab-ci db : %v", err)
		} else {
			logDB.Trace("-----ATTENTION !-----\n-----\n-----\n-----\n-----\n----- La base \"postgres:5432\" est utilisée." +
				"\n----- Elle n'est prévue pour être utilisée que dans gitlab CI.\n-----\n-----\n-----\n-----\n--------------")
		}
	}

	DB, err := db.DB()
	if err != nil {
		logDB.Errorf(err, "Error pinging db : %v", err)
		os.Exit(1)
	}

	if err = DB.Ping(); err != nil {
		logDB.Errorf(err, "Error pinging db : %v", err)
		os.Exit(1)
	}

	DB.SetMaxIdleConns(10)
	DB.SetMaxOpenConns(25)

	logDB.Trace("Connected to DB successfully")

	return db
}

//GetDB récupérer la DB
func GetDB(c *gin.Context) *gorm.DB {
	return c.MustGet("DB").(*gorm.DB)
}

//GetDBOverride récupérer la DB, elle ne sera jamais rollback
func GetDBOverride(c *gin.Context) *gorm.DB {
	return c.MustGet("DBOverride").(*gorm.DB)
}
