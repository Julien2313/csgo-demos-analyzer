package middlewares

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/memstore"
	"github.com/gin-gonic/gin"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

var sessionStore sessions.Store

func init() {
	const maxAge = 3600 * 24 * 7

	coockie := helpers.RandomString(64)
	sessionStore = memstore.NewStore([]byte(coockie))
	sessionStore.Options(sessions.Options{
		Secure:   true,
		HttpOnly: true,
		Path:     "/api/",
		MaxAge:   maxAge,
	})
}

// Session store token
func Session() gin.HandlerFunc {
	return sessions.Sessions("csgo_demo_analyzer_session", sessionStore)
}

// AuthRequired next if user is in token session
func AuthRequired(logBack *custlogger.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		session := sessions.Default(c)
		if session.Get(helpers.Userkey) == nil {
			helpers.SendError(c, logBack, helpers.ErrBadTokenConnection(), nil, nil)
			return
		}

		c.Next()
	}
}
