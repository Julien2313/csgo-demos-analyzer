package middlewares

import (
	"sync"
	"time"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"golang.org/x/time/rate"
)

// LimiterPerUser map of limiterUser
type LimiterPerUser map[string]Visitor

// Visitor struct of one limiter
type Visitor struct {
	limiter  *rate.Limiter
	lastSeen time.Time
}

func cleanupVisitors(mLimiter *sync.Mutex, limiterPerUser LimiterPerUser) {
	for {
		time.Sleep(time.Minute)

		mLimiter.Lock()
		for identifier, visitor := range limiterPerUser {
			if time.Since(visitor.lastSeen) > 3*time.Minute {
				delete(limiterPerUser, identifier)
			}
		}
		mLimiter.Unlock()
	}
}

// DefaultLimiter return a default limiter
func DefaultLimiter(
	c *gin.Context,
	mLimiter *sync.Mutex,
	limiterPerUser LimiterPerUser,
	limitPerSecond rate.Limit,
) bool {
	var limiterStr string

	uidC := sessions.Default(c).Get(helpers.Userkey)
	if uidC != nil {
		limiterStr = uidC.(string)
	} else {
		limiterStr = c.ClientIP()
	}

	mLimiter.Lock()
	defer mLimiter.Unlock()

	userLimit, exists := limiterPerUser[limiterStr]
	if !exists {
		userLimit = Visitor{
			limiter:  rate.NewLimiter(limitPerSecond, 1),
			lastSeen: time.Now(),
		}
		limiterPerUser[limiterStr] = userLimit
	}

	return userLimit.limiter.Allow()
}

// InitLimiter return limiter
func InitLimiter(
	logger *custlogger.Logger,
	checkerLimit func(*gin.Context, *sync.Mutex, LimiterPerUser, rate.Limit) bool,
	limitPerSecond rate.Limit,
) gin.HandlerFunc {
	mLimiter := &sync.Mutex{}
	limiterPerUser := make(LimiterPerUser)

	go cleanupVisitors(mLimiter, limiterPerUser)

	return LimitQuery(logger, mLimiter, checkerLimit, limiterPerUser, limitPerSecond)
}

// InitDefaultLimiter return default limiter
func InitDefaultLimiter(
	logger *custlogger.Logger,
	limitPerSecond rate.Limit,
	useRateLimit bool,
) gin.HandlerFunc {
	if !useRateLimit {
		return func(c *gin.Context) {}
	}
	mLimiter := &sync.Mutex{}
	limiterPerUser := make(LimiterPerUser)

	go cleanupVisitors(mLimiter, limiterPerUser)

	return LimitQuery(logger, mLimiter, DefaultLimiter, limiterPerUser, limitPerSecond)
}

// LimitQuery simple handler to limit the number of query per user
func LimitQuery(
	logger *custlogger.Logger,
	mLimiter *sync.Mutex,
	checkerLimit func(*gin.Context, *sync.Mutex, LimiterPerUser, rate.Limit) bool,
	limiterPerUser LimiterPerUser,
	limitPerSecond rate.Limit,
) gin.HandlerFunc {
	return func(c *gin.Context) {
		if !checkerLimit(c, mLimiter, limiterPerUser, limitPerSecond) {
			helpers.SendError(c, logger, helpers.ErrTooManyRequest(), nil, nil)
			return
		}

		c.Next()
	}
}
