package middlewares

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// Rollback simple middleware de rollback en cas d'erreur
func Rollback(db *gorm.DB, useRollback bool) gin.HandlerFunc {
	return func(c *gin.Context) {
		if useRollback {
			// dbTemp := db.Begin()
			// dbOverride := db.Begin()

			c.Set("DB", db)
			c.Set("DBOverride", db)

			c.Next()
			// dbOverride.Commit()

			// statusCode := c.Writer.Status()

			// if statusCode/100 != 2 {
			// 	dbTemp.Rollback()
			// } else {
			// 	dbTemp.Commit()
			// }
		} else {
			c.Set("DB", db)
			c.Set("DBOverride", db)

			c.Next()
		}
	}
}
