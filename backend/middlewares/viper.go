package middlewares

import (
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

// StoreViper simple middleware pour store Viper
func StoreViper(viper *viper.Viper) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("viper", viper)
		c.Next()
	}
}

// GetViper return viper from contexte
func GetViper(c *gin.Context) *viper.Viper {
	return c.MustGet("viper").(*viper.Viper)
}
