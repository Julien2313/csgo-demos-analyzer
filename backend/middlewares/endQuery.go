package middlewares

import (
	"sync"

	"github.com/gin-gonic/gin"
)

// EndQuery simple middleware to free resource when query gas been done and DB commited
func EndQuery() gin.HandlerFunc {
	return func(c *gin.Context) {
		var wQuery sync.WaitGroup

		c.Set("wQuery", &wQuery)
		wQuery.Add(1)
		defer wQuery.Done()

		c.Next()
	}
}
