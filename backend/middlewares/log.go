package middlewares

import (
	"bytes"
	"io/ioutil"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
)

// bodyLogWriter type permettant d'extraire les données renvoyé au client pour les logs
type bodyLogWriter struct {
	gin.ResponseWriter
	Body *bytes.Buffer
}

func (w bodyLogWriter) Write(b []byte) (int, error) {
	w.Body.Write(b)

	return w.ResponseWriter.Write(b)
}

// Logs logs everything, do not use it with credentials
func Logs(logGin *custlogger.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()
		path := c.Request.URL.Path
		raw := c.Request.URL.RawQuery

		blw := &bodyLogWriter{Body: bytes.NewBufferString(""), ResponseWriter: c.Writer}
		c.Writer = blw

		var data []byte

		var err error

		if data, err = c.GetRawData(); err != nil {
			data = []byte(err.Error())
		} else {
			c.Request.Body = ioutil.NopCloser(strings.NewReader(string(data)))
		}

		c.Next()

		end := time.Now()
		latency := end.Sub(start)

		clientIP := c.ClientIP()
		method := c.Request.Method
		statusCode := c.Writer.Status()

		comment := c.Errors.ByType(gin.ErrorTypePrivate).String()

		if raw != "" {
			path = path + "?" + raw
		}

		logGin.Tracef("end: %s, statusCode: %d, latency:%s, clientIP:%s, method:%s, path:%s, comment:%s, size :%dbytes\n",
			end.Format("2006/01/02 - 15:04:05"),
			statusCode,
			latency.String(),
			clientIP,
			method,
			path,
			comment,
			len(data),
		)

		cookies := c.Request.Cookies()
		if len(cookies) > 0 {
			logGin.Trace("Cookies :")

			for _, cookie := range cookies {
				logGin.Trace("  ", cookie.Name, cookie.String())
			}
		}

		const maxLenghtLogs = 1000
		if len(string(data)) > maxLenghtLogs {
			logGin.Tracef("data:%s", string(data)[0:maxLenghtLogs])
		} else {
			logGin.Tracef("data:%s", string(data))
		}
		if len(blw.Body.String()) > maxLenghtLogs {
			logGin.Tracef("response:%s\n", blw.Body.String()[0:maxLenghtLogs])
		} else {
			logGin.Tracef("response:%s\n", blw.Body.String())
		}
	}
}

// EasyLogs logs eveyrthing but data from client
func EasyLogs(logGin *custlogger.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()
		path := c.Request.URL.Path
		raw := c.Request.URL.RawQuery

		blw := &bodyLogWriter{Body: bytes.NewBufferString(""), ResponseWriter: c.Writer}
		c.Writer = blw

		var (
			data []byte
			err  error
		)

		if data, err = c.GetRawData(); err != nil {
			data = []byte(err.Error())
		} else {
			c.Request.Body = ioutil.NopCloser(strings.NewReader(string(data)))
			data = []byte{}
		}

		c.Next()

		end := time.Now()
		latency := end.Sub(start)

		clientIP := c.ClientIP()
		method := c.Request.Method
		statusCode := c.Writer.Status()

		comment := c.Errors.ByType(gin.ErrorTypePrivate).String()

		if raw != "" {
			path = path + "?" + raw
		}

		logGin.Tracef("end: %s, statusCode: %d, latency:%s, clientIP:%s, method:%s, path:%s, comment:%s, size :%dbytes\n",
			end.Format("2006/01/02 - 15:04:05"),
			statusCode,
			latency.String(),
			clientIP,
			method,
			path,
			comment,
			len(data),
		)

		cookies := c.Request.Cookies()
		if len(cookies) > 0 {
			logGin.Trace("Cookies :")

			for _, cookie := range cookies {
				logGin.Trace("  ", cookie.Name, cookie.String())
			}
		}

		const maxLenghtLogs = 1000
		if len(string(data)) > maxLenghtLogs {
			logGin.Tracef("data:%s", string(data)[0:maxLenghtLogs])
		} else {
			logGin.Tracef("data:%s", string(data))
		}

		logGin.Tracef("response:%s\n", blw.Body.String())
	}
}
