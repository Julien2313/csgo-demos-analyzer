package server

// import (
// 	"math/rand"
// 	"net/http"
// 	"os"
// 	"testing"

// 	"github.com/stretchr/testify/assert"
// 	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao/testsdao"
// 	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
// 	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helperstest"
// 	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model/modelproto"
// )

// const methodeUpload = http.MethodPost
// const urlUpload = "/api/v1/demo"

// func TestControllerDemo_UploadMustBeConnected(t *testing.T) {
// 	assert.Nil(t, model.CheckMustBeConnected(t, true, nil, methodeUpload, urlUpload))
// }

// func TestControllerDemo_UploadOK(t *testing.T) {
// 	httpCookie := model.PerformTestAuth(*testsdao.StoreGeneratedUser(userDAO))
// 	pathDemo := "./sampleDemo.dem"
// 	randomNameDem := helpers.RandomStringChar(10) + ".dem"
// 	demoInfo := &modelproto.DemoInfo_DemoGlobalData{
// 		ID:        helpers.Int64Ptr(rand.Int63()),
// 		Timestamp: helpers.Int64Ptr(helpers.RandDate().Unix()),
// 	}
// 	defer func() {
// 		assert.NoError(t, os.Remove(randomNameDem))
// 	}()
// 	assert.NoError(t, helpers.CopyFile(pathDemo, randomNameDem))

// 	resp := model.SendRequestWithDemo(methodeUpload, urlUpload, httpCookie, randomNameDem, demoInfo)
// 	// TODO because we parse it to check user's steamID, it's a corrupted demo, so doesn't work...
// 	assert.Equal(t, http.StatusForbidden, resp.Code)
// }

// func TestControllerDemo_UploadNoInfo(t *testing.T) {
// 	httpCookie := model.PerformTestAuth(*testsdao.StoreGeneratedUser(userDAO))
// 	pathDemo := "./sampleDemo.dem"
// 	randomName := helpers.RandomStringChar(10) + ".dem"
// 	defer func() {
// 		assert.NoError(t, os.Remove(randomName))
// 	}()
// 	assert.NoError(t, helpers.CopyFile(pathDemo, randomName))
// 	resp := model.SendRequestWithDemo(methodeUpload, urlUpload, httpCookie, randomName, nil)
// 	assert.Equal(t, http.StatusForbidden, resp.Code)
// }

// func TestControllerDemo_UploadExists(t *testing.T) {
// 	httpCookie := model.PerformTestAuth(*testsdao.StoreGeneratedUser(userDAO))
// 	pathDemo := "./sampleDemo.dem"
// 	randomNameDem := helpers.RandomStringChar(10) + ".dem"
// 	demoInfo := &modelproto.DemoInfo_DemoGlobalData{
// 		ID:        helpers.Int64Ptr(rand.Int63()),
// 		Timestamp: helpers.Int64Ptr(helpers.RandDate().Unix()),
// 	}
// 	defer func() {
// 		assert.NoError(t, os.Remove(randomNameDem))
// 	}()
// 	assert.NoError(t, helpers.CopyFile(pathDemo, randomNameDem))

// 	resp := model.SendRequestWithDemo(methodeUpload, urlUpload, httpCookie, randomNameDem, demoInfo)
// 	// TODO because we parse it to check user's steamID, it's a corrupted demo, so doesn't work...
// 	assert.Equal(t, http.StatusForbidden, resp.Code)

// 	resp = model.SendRequestWithDemo(methodeUpload, urlUpload, httpCookie, randomNameDem, demoInfo)
// 	// TODO because we parse it to check user's steamID, it's a corrupted demo, so doesn't work...
// 	assert.Equal(t, http.StatusForbidden, resp.Code)
// }

// func TestControllerDemo_UploadBadHeader(t *testing.T) {
// 	httpCookie := model.PerformTestAuth(*testsdao.StoreGeneratedUser(userDAO))
// 	pathDemo := "./badHeader.dem"
// 	randomNameDem := helpers.RandomStringChar(10) + ".dem"
// 	demoInfo := &modelproto.DemoInfo_DemoGlobalData{
// 		ID:        helpers.Int64Ptr(rand.Int63()),
// 		Timestamp: helpers.Int64Ptr(helpers.RandDate().Unix()),
// 	}
// 	defer func() {
// 		assert.NoError(t, os.Remove(randomNameDem))
// 	}()
// 	assert.NoError(t, helpers.CopyFile(pathDemo, randomNameDem))
// 	resp := model.SendRequestWithDemo(methodeUpload, urlUpload, httpCookie, pathDemo, demoInfo)
// 	assert.Equal(t, http.StatusForbidden, resp.Code)
// }

// func TestControllerDemo_UploadNotADemo(t *testing.T) {
// 	httpCookie := model.PerformTestAuth(*testsdao.StoreGeneratedUser(userDAO))
// 	pathDemo := "./notADemo.dem"
// 	randomNameDem := helpers.RandomStringChar(10) + ".dem"
// 	demoInfo := &modelproto.DemoInfo_DemoGlobalData{
// 		ID:        helpers.Int64Ptr(rand.Int63()),
// 		Timestamp: helpers.Int64Ptr(helpers.RandDate().Unix()),
// 	}
// 	fDemo, err := os.Create(pathDemo)
// 	assert.NoError(t, err)
// 	defer func() {
// 		assert.NoError(t, os.Remove(randomNameDem))
// 	}()
// 	assert.NoError(t, helpers.CopyFile(pathDemo, randomNameDem))
// 	resp := model.SendRequestWithDemo(methodeUpload, urlUpload, httpCookie, randomNameDem, demoInfo)
// 	assert.Equal(t, http.StatusForbidden, resp.Code)
// 	assert.NoError(t, fDemo.Close())
// }

// func TestControllerDemo_UploadNoDemo(t *testing.T) {
// 	httpCookie := model.PerformTestAuth(*testsdao.StoreGeneratedUser(userDAO))
// 	resp := model.SendRequestWithToken(nil, methodeUpload, urlUpload, httpCookie)
// 	assert.Equal(t, http.StatusForbidden, resp.Code)
// }
