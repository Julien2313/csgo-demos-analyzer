package server_test

import (
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/server"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
)

func TestInitializeRoutes(t *testing.T) {
	s := &server.Server{SchemaDB: store.SchemaTestServer}

	assert.EqualError(
		t,
		s.InitializeRoutes(nil, nil, nil, nil, nil, "", "", 0, nil, nil, true), server.ErrRouterNotInit.Error(),
	)
	s.Router = gin.Default()
	assert.NoError(t, s.InitializeRoutes(nil, nil, nil, nil, nil, "", "", 0, nil, nil, true))
}
