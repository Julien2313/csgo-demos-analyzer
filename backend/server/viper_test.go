package server

import (
	"testing"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestViper(t *testing.T) {
	config := viper.New()
	require.NoError(t, SetupViper(config, "test.env"))
	assert.Equal(t, "itsatest", config.GetString("TEST"))
}
