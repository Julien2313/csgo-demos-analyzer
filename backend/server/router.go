package server

import (
	"errors"
	"time"

	"github.com/gin-contrib/gzip"
	"github.com/gin-gonic/gin"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/controller"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/middlewares"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzerdeathmatch"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/democlassifier"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/downloaderinfo"
	"golang.org/x/time/rate"
	"gorm.io/gorm"
)

// ErrRouterNotInit router has not been init
var ErrRouterNotInit = errors.New("ErrRouterNotInit")

// InitializeRoutes initialize routes, middlewares, CORS
func (s *Server) InitializeRoutes(db *gorm.DB,
	dc *democlassifier.DemoClassifier,
	di *downloaderinfo.DownloaderInfo,
	adm *analyzerdeathmatch.AnalyzerDeathMatch,
	a *analyzer.Analyzer,
	IP, keyAPI string,
	analyzerVersion int64,
	logGin, logBack *custlogger.Logger,
	useRateLimit bool,
) error {
	if s.Router == nil {
		return ErrRouterNotInit
	}

	s.Router.Use(gin.Recovery())
	s.Router.Use(helpers.ReturnCORSSetup(IP))

	s.Router.NoRoute(func(c *gin.Context) {
		// NoRoute default page if route doesn't exist
		helpers.SendError(c,
			logBack,
			helpers.ErrNotFound(),
			nil,
			errors.New("URL not found : "+c.Request.URL.Path),
		)
	})

	statsController := controller.NewStatsController(logBack, keyAPI)
	demoController := controller.NewDemoController(logBack, dc, di, adm, a, analyzerVersion)
	userController := controller.NewUserController(logBack, di, analyzerVersion)
	authController := controller.NewAuthController(logBack)

	api := s.Router.Group("/api")
	api.Use(middlewares.Session())
	api.Use(gzip.Gzip(gzip.DefaultCompression))
	api.Use(middlewares.EndQuery())
	api.Use(middlewares.Rollback(db, false))
	{
		v1NoAuth(
			api,
			authController, userController, demoController, statsController,
			logGin, logBack,
			useRateLimit,
		)
		v1Auth(
			api,
			authController, userController, demoController, statsController,
			logGin, logBack,
			useRateLimit,
		)
	}

	return nil
}

func v1NoAuth(
	api *gin.RouterGroup,
	authController controller.AuthController,
	userController controller.UserController,
	demoController controller.DemoController,
	statsController controller.StatsController,
	logGin, logBack *custlogger.Logger,
	useRateLimit bool,
) {
	v1NoAuth := api.Group("/v1")
	{
		v1NoAuthUser := v1NoAuth.Group("/user")
		v1NoAuthUser.Use(middlewares.EasyLogs(logGin))
		{
			v1NoAuthUser.POST("/login",
				middlewares.InitDefaultLimiter(logGin, rate.Every(time.Second), useRateLimit),
				authController.Authentication,
			)
			v1NoAuthUser.POST("/register",
				middlewares.InitDefaultLimiter(logGin, rate.Every(time.Second), useRateLimit),
				userController.Register,
			)
			v1NoAuthUser.GET("/dateCalendar/:steamID",
				middlewares.InitDefaultLimiter(logGin, rate.Every(time.Second), useRateLimit),
				userController.GetDateCalendar,
			)
		}

		v1NoAuth.Use(middlewares.Logs(logGin))
		demo := v1NoAuth.Group("/demo")
		{
			demo.GET("/user/:steamID",
				middlewares.InitDefaultLimiter(logGin, rate.Every(time.Second), useRateLimit),
				demoController.GetAllFromUser)
			demo.POST("/upload/:token",
				middlewares.InitDefaultLimiter(logGin, rate.Every(time.Second*2), useRateLimit),
				demoController.Upload,
			)
			demo.POST("/uploadbyurl",
				middlewares.InitDefaultLimiter(logGin, rate.Every(time.Second*2), useRateLimit),
				demoController.UploadByURL,
			)
			demo.GET("/lastAnalyzed",
				middlewares.InitDefaultLimiter(logGin, rate.Every(time.Second), useRateLimit),
				demoController.GetLastAnalyzed,
			)
		}

		stats := v1NoAuth.Group("/stats")
		{
			stats.GET("/demo/:demoID",
				middlewares.InitDefaultLimiter(logGin, rate.Every(time.Second*2), useRateLimit),
				statsController.Get,
			)
			stats.GET("/heatmaps/:steamID/:mapName",
				middlewares.InitDefaultLimiter(logGin, rate.Every(time.Second), useRateLimit),
				statsController.GetHeatMaps,
			)
			stats.GET("/pattern/:steamID",
				middlewares.InitDefaultLimiter(logGin, rate.Every(time.Second), useRateLimit),
				statsController.GetPatterns,
			)
		}
	}
}

func v1Auth(
	api *gin.RouterGroup,
	authController controller.AuthController,
	userController controller.UserController,
	demoController controller.DemoController,
	statsController controller.StatsController,
	logGin, logBack *custlogger.Logger,
	useRateLimit bool,
) {
	v1 := api.Group("/v1")
	v1.Use(middlewares.Logs(logGin))
	v1.Use(middlewares.AuthRequired(logBack))
	{
		v1.GET("/auth",
			middlewares.InitDefaultLimiter(logGin, rate.Every(time.Second/4), useRateLimit),
			authController.CheckAuth,
		)
		v1.POST("/logout",
			middlewares.InitDefaultLimiter(logGin, rate.Every(time.Second), useRateLimit),
			authController.Logout,
		)

		user := v1.Group("/user")
		{
			user.GET("/me",
				middlewares.InitDefaultLimiter(logGin, rate.Every(time.Second/2), useRateLimit),
				userController.GetCurrentUser,
			)
			user.GET("/progression",
				middlewares.InitDefaultLimiter(logGin, rate.Every(time.Second*2), useRateLimit),
				userController.Progression,
			)
			user.POST("/steamid",
				middlewares.InitDefaultLimiter(logGin, rate.Every(time.Second/2), useRateLimit),
				userController.SetSteamID,
			)
			user.POST("/sharecode",
				middlewares.InitDefaultLimiter(logGin, rate.Every(time.Second/2), useRateLimit),
				userController.SetShareCode,
			)
			user.POST("/authenticationcodehitory",
				middlewares.InitDefaultLimiter(logGin, rate.Every(time.Second/2), useRateLimit),
				userController.SetAccessTokenHistory,
			)
			user.GET("/checktokens",
				middlewares.InitDefaultLimiter(logGin, rate.Every(time.Second), useRateLimit),
				userController.CheckTokens,
			)
			user.POST("/createToken",
				middlewares.InitDefaultLimiter(logGin, rate.Every(time.Second), useRateLimit),
				userController.CreateToken,
			)
			user.GET("/tokens",
				middlewares.InitDefaultLimiter(logGin, rate.Every(time.Second/2), useRateLimit),
				userController.GetTokens,
			)
		}
		demo := v1.Group("/demo")
		{
			demo.POST("/checknewdemo",
				middlewares.InitDefaultLimiter(logGin, rate.Every(time.Second*10), useRateLimit),
				demoController.CheckNewDemo,
			)
			demo.POST("/upload",
				middlewares.InitDefaultLimiter(logGin, rate.Every(time.Second*5), useRateLimit),
				demoController.Upload,
			)
		}
		stats := v1.Group("/stats")
		{
			stats.GET("/deathmatch",
				middlewares.InitDefaultLimiter(logGin, rate.Every(time.Second*3), useRateLimit),
				statsController.GetDeathMatchs)
			stats.GET("/competitives",
				middlewares.InitDefaultLimiter(logGin, rate.Every(time.Second*3), useRateLimit),
				statsController.GetCompetitives)
		}
	}
}
