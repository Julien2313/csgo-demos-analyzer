package server_test

import (
	"log"
	"math/rand"
	"os"
	"testing"
	"time"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var logTest *custlogger.Logger

var _testDBGlobal *gorm.DB
var fastLogger logger.Interface

func TestMain(m *testing.M) {
	rand.Seed(time.Now().Unix())

	logTest = &custlogger.Logger{}
	logTest.SetupInstanceLogggerTest()

	_testDBGlobal = store.InitDB(true, store.SchemaTestServer, logTest)
	fastLogger = logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			LogLevel: logger.Silent,
		},
	)

	os.Exit(m.Run())
}
