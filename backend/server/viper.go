package server

import (
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/viper"
)

// SetupViper configure des variables automatiqument
func SetupViper(viper *viper.Viper, filename string) error {
	if os.Getenv("CSGO_demo_analyzer") == "production" {
		filename += ".prod"
	}

	if err := godotenv.Overload(filename); err != nil {
		return err
	}

	viper.SetDefault("DEBUG", false)
	viper.SetDefault("IP", "localhost")
	viper.SetDefault("PORT_SERVER", 22)
	viper.SetDefault("DIR_UPLOAD", "./storage")
	viper.SetDefault("DIR_UPLOAD_DM", "./storage/deathmatch/")
	viper.SetDefault("BOILER_WRITER_BIN", "./bin-boiler/boiler-writter")

	viper.SetEnvPrefix("CSGO")
	viper.AutomaticEnv()

	return nil
}
