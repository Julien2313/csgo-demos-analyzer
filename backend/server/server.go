package server

import (
	"context"
	"errors"
	"fmt"
	"math/rand"
	"net/http"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzerdeathmatch"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/democlassifier"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/downloaderdem"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/downloaderinfo"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/telegram"
	"gorm.io/gorm"
)

// Server general struct of the server
type Server struct {
	Router     *gin.Engine
	Config     *viper.Viper
	Logger     *custlogger.Logger
	httpServer *http.Server
	SchemaDB   string

	db  *gorm.DB
	dd  *downloaderdem.DownloaderDem
	dc  *democlassifier.DemoClassifier
	di  *downloaderinfo.DownloaderInfo
	a   *analyzer.Analyzer
	adm *analyzerdeathmatch.AnalyzerDeathMatch
}

// ErrLoadViper error when can't load viper
const ErrLoadViper = "couldn't load viper : %s"

// ErrAnalyzerNotStarted analyzer not started
var ErrAnalyzerNotStarted = errors.New("analyzer not started")

// ErrAnalyzerDeathMatchNotStarted analyzer deathmatch not started
var ErrAnalyzerDeathMatchNotStarted = errors.New("analyzerDeathMatchNotStarted not started")

// ErrDBNotSetup DB not started
var ErrDBNotSetup = errors.New("DB not started")

const analyzerVersion = 202107142359
const analyzerDeathMatchVersion = 202101280831

func (s *Server) setupViper(pathEnv string) error {
	s.Config = viper.New()
	if pathEnv == "" {
		pathEnv = "./.env"
	}

	return SetupViper(s.Config, pathEnv)
}

func (s *Server) setupLogger() {
	logServer := &custlogger.Logger{}
	logServer.SetupInstanceLoggger("server")
	s.Logger = logServer
}

func (s *Server) setupDB(schema string) {
	logDB := &custlogger.Logger{}
	logDB.SetupInstanceLoggger("db")
	s.db = store.InitDB(false, schema, logDB)
}

// SetDB set the db of the server
func (s *Server) SetDB(db *gorm.DB) {
	s.db = db
}

func (s *Server) startAnalyzer() error {
	if s.db == nil {
		return ErrDBNotSetup
	}

	logAnalyzer := &custlogger.Logger{}
	logAnalyzer.SetupInstanceLoggger("analyzer")

	var err error
	s.a, err = analyzer.Start(s.db, logAnalyzer, analyzerVersion)

	return err
}

func (s *Server) startAnalyzerDeathMatch() error {
	if s.db == nil {
		return ErrDBNotSetup
	}

	logAnalyzerDeathMatch := &custlogger.Logger{}
	logAnalyzerDeathMatch.SetupInstanceLoggger("analyzerDeathMatch")

	var err error
	s.adm, err = analyzerdeathmatch.Start(
		s.db,
		logAnalyzerDeathMatch,
		analyzerDeathMatchVersion,
		s.SchemaDB,
		s.Config.GetString("DIR_UPLOAD_DM"),
		s.Config.GetInt("PORT_SERVER"),
	)

	return err
}

func (s *Server) startDownloaderDem() error {
	if s.a == nil {
		return ErrAnalyzerNotStarted
	}

	if s.db == nil {
		return ErrDBNotSetup
	}

	logDownlaoderDem := &custlogger.Logger{}
	logDownlaoderDem.SetupInstanceLoggger("downloaderDem")

	var err error
	s.dd, err = downloaderdem.Start(s.db, s.a, logDownlaoderDem)

	return err
}

func (s *Server) startDemoClassifier() error {
	if s.a == nil {
		return ErrAnalyzerNotStarted
	}
	if s.adm == nil {
		return ErrAnalyzerDeathMatchNotStarted
	}

	if s.db == nil {
		return ErrDBNotSetup
	}

	logDemoClassifier := &custlogger.Logger{}
	logDemoClassifier.SetupInstanceLoggger("downloaderDem")

	var err error
	s.dc, err = democlassifier.Start(s.db, s.a, s.adm, logDemoClassifier)

	return err
}

// SetDemoClassifier set the demo classifier of the server
func (s *Server) SetDemoClassifier(dc *democlassifier.DemoClassifier) {
	s.dc = dc
}

// SetDownloaderInfo set the downlaoder info of the server
func (s *Server) SetDownloaderInfo(di *downloaderinfo.DownloaderInfo) {
	s.di = di
}

func (s *Server) startDownloaderInfo() {
	logDownlaoderInfo := &custlogger.Logger{}
	logDownlaoderInfo.SetupInstanceLoggger("downlaoderInfo")
	s.di = downloaderinfo.Start(s.db, s.dd, logDownlaoderInfo,
		s.Config.GetString("KEY"), s.Config.GetString("BOILER_WRITER_BIN"), s.Config.GetString("DIR_UPLOAD"))
}

func (s *Server) startGin() error {
	logGin := &custlogger.Logger{}
	logBack := &custlogger.Logger{}

	logGin.SetupInstanceLoggger("gin-gonic")
	logBack.SetupInstanceLoggger("back")

	gin.DefaultWriter = logGin.DefaultOutput
	gin.DefaultErrorWriter = logGin.DefaultOutput

	s.Router = gin.Default()

	return s.InitializeRoutes(s.db, s.dc, s.di, s.adm, s.a, s.Config.GetString("IP"),
		s.Config.GetString("KEY"), analyzerVersion, logGin, logBack, true)
}

// Start start the server
func (s *Server) Start(pathEnv string) {
	rand.Seed(time.Now().UnixNano())

	s.setupLogger()

	if err := s.setupViper(pathEnv); err != nil {
		panic(fmt.Errorf(ErrLoadViper, err.Error()))
	}

	s.setupDB(s.Config.GetString("PG_SCHEMA"))
	if err := s.startAnalyzer(); err != nil {
		s.Logger.Errorf(err, "Couldn't start analyzer : %s", err.Error())
		panic(err)
	}
	if err := s.startAnalyzerDeathMatch(); err != nil {
		s.Logger.Errorf(err, "Couldn't start analyzerDeathMatch : %s", err.Error())
		panic(err)
	}

	if err := s.startDownloaderDem(); err != nil {
		s.Logger.Errorf(err, "Couldn't start downloaderdem : %s", err.Error())
		panic(err)
	}

	if err := s.startDemoClassifier(); err != nil {
		s.Logger.Errorf(err, "Couldn't start DemoClassifier : %s", err.Error())
		panic(err)
	}
	// if err := s.startDownloaderDeathMatch(); err != nil {
	// 	s.Logger.Errorf(err, "Couldn't start downloaderdeathmatch : %s", err.Error())
	// 	panic(err)
	// }

	s.startDownloaderInfo()

	if s.Config.IsSet("TOKEN_TELEGRAM") {
		telegram.Start(s.Config.GetString("TOKEN_TELEGRAM"), s.Config.GetInt64("CHATID_TELEGRAM"), s.Config.GetBool("DEBUG"))
	}

	if err := s.startGin(); err != nil {
		s.Logger.Errorf(err, "Couldn't start gin : %s", err.Error())
		panic(err)
	}

	s.httpServer = &http.Server{
		Addr:    fmt.Sprintf(":%d", s.Config.GetInt("PORT")),
		Handler: s.Router,
	}

	go func() {
		if err := s.httpServer.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			s.Logger.Errorf(err, "Couldn't start the server : %s", err.Error())
		}
	}()
}

// Shutdown shutdown the server
func (s *Server) Shutdown() {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := s.httpServer.Shutdown(ctx); err != nil {
		s.Logger.Error(err, "Server forced to shutdown:", err)
	}

	<-ctx.Done()

	s.Logger.Info("server shutdown correctly")
	s.Logger.Info("trying to shutdown thread")

	var wgShutdown sync.WaitGroup

	wgShutdown.Add(3)

	go func() {
		if err := s.dd.Shutdown(); err != nil {
			s.Logger.Error(err, "error while shuting down downlaoderdem : ", err)
		}
		s.Logger.Info("switched off downlaoderdem")
		wgShutdown.Done()
	}()

	go func() {
		s.di.Shutdown()
		s.Logger.Info("switched off downlaoderinfo")
		wgShutdown.Done()
	}()

	go func() {
		s.adm.Shutdown()
		s.Logger.Info("switched off analyzerdeathmatch")
		wgShutdown.Done()
	}()

	done := make(chan interface{})

	go func() {
		wgShutdown.Wait()
		done <- struct{}{}
	}()

	select {
	case <-time.After(time.Second * 65):
		panic("couldn't shutdown threads fast enough, check logs to know which one")
	case <-done:
		s.Logger.Info("threads shutdown correctly")
	}
}
