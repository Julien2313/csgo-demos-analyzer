package server

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzerdeathmatch"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/downloaderinfo"
	"gorm.io/gorm"
)

func TestServer_setupViper(t *testing.T) {
	server := Server{}
	assert.Error(t, server.setupViper(""))
	assert.NoError(t, server.setupViper("./test.env"))
}

func TestServer_setupLogger(t *testing.T) {
	server := Server{}
	server.setupLogger()
	require.NotNil(t, server.Logger)
}

func TestServer_setupDB(t *testing.T) {
	server := Server{SchemaDB: store.SchemaTestServer}
	server.setupDB(store.SchemaTestServer)
	require.NotNil(t, server.db)
}

func TestServer_SetDB(t *testing.T) {
	server := Server{}
	db := &gorm.DB{}
	server.SetDB(db)
	require.NotNil(t, server.db)
}

func TestServer_startDemoClassifier(t *testing.T) {
	server := Server{}
	assert.EqualError(t, server.startDemoClassifier(), ErrAnalyzerNotStarted.Error())
	server.a = &analyzer.Analyzer{}
	assert.EqualError(t, server.startDemoClassifier(), ErrAnalyzerDeathMatchNotStarted.Error())
	server.adm = &analyzerdeathmatch.AnalyzerDeathMatch{}
	assert.EqualError(t, server.startDemoClassifier(), ErrDBNotSetup.Error())
	server.setupDB(store.SchemaTestServer)
	assert.NoError(t, server.startAnalyzer())
	assert.NoError(t, server.startDemoClassifier())
	require.NoError(t, server.dc.Shutdown())
}

func TestServer_startAnalyzer(t *testing.T) {
	server := Server{}
	assert.EqualError(t, server.startAnalyzer(), ErrDBNotSetup.Error())
	server.setupDB(store.SchemaTestServer)
	assert.NoError(t, server.startAnalyzer())
	server.a.Shutdown()
}

func TestServer_startDownloaderDem(t *testing.T) {
	server := Server{}
	assert.EqualError(t, server.startDownloaderDem(), ErrAnalyzerNotStarted.Error())
	server.a = &analyzer.Analyzer{}
	assert.EqualError(t, server.startDownloaderDem(), ErrDBNotSetup.Error())
	server.setupDB(store.SchemaTestServer)
	assert.NoError(t, server.startAnalyzer())
	assert.NoError(t, server.startDownloaderDem())
	assert.NoError(t, server.dd.Shutdown())
}

func TestServer_SetDownloaderInfo(t *testing.T) {
	server := Server{}
	server.SetDownloaderInfo(&downloaderinfo.DownloaderInfo{})
	assert.NotNil(t, server.di)
}

func TestServer_startDownloaderInfo(t *testing.T) {
	// assert.True(t, false)
}

func TestServer_Start(t *testing.T) {
	// assert.True(t, false)
}

func TestServer_Shutdown(t *testing.T) {
	// assert.True(t, false)
}
