package controller_test

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/controller"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/mock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/server"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
)

const (
	methodeCreateToken = http.MethodPost
	urlCreateToken     = "/api/v1/user/createToken"
)

func TestControllerUser_CreateToken(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	userDAO := dao.NewUserDAO(localDB)
	userPicked := mock.UsersMocked[rand.Intn(mock.NbrUser)]
	localUserGenerated := model.StoreGeneratedUser(localDB)
	localUserGenerated.SteamID = userPicked.SteamID
	require.NoError(t, userDAO.UpdateSteamID(localUserGenerated))

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, nil,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	httpCookie := performTestAuth(serverTest.Router, *localUserGenerated)

	bytesBadIP, err := json.Marshal(controller.ConfigTokenGet{IP: helpers.RandomString(15)})
	require.NoError(t, err)

	resp := sendRequestWithToken(serverTest.Router,
		bytesBadIP,
		methodeCreateToken,
		urlCreateToken,
		httpCookie)
	require.Equal(t, http.StatusBadRequest, resp.Code)

	resp = sendRequestWithToken(serverTest.Router,
		nil,
		methodeCreateToken,
		urlCreateToken,
		httpCookie)
	require.Equal(t, http.StatusBadRequest, resp.Code)

	goodIP := controller.ConfigTokenGet{
		IP: fmt.Sprintf("%d.%d.%d.%d", rand.Intn(256), rand.Intn(256), rand.Intn(256), rand.Intn(256)),
	}
	bytesGoodIP, err := json.Marshal(goodIP)
	require.NoError(t, err)
	resp = sendRequestWithToken(serverTest.Router,
		bytesGoodIP,
		methodeCreateToken,
		urlCreateToken,
		httpCookie)
	assert.Equal(t, http.StatusOK, resp.Code)

	var tokenFromDB model.TokenUploadDemo
	require.NoError(t, localDB.Where("ip_allowed = ?", goodIP.IP).Find(&tokenFromDB).Error)

	require.Equal(t, goodIP.IP, *tokenFromDB.IPAllowed)
}
