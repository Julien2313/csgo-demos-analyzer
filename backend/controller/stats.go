package controller

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model/progression"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
)

// StatsController controller of the games
type StatsController struct {
	LoggerBack *custlogger.Logger
	KeyAPI     string
}

// NewStatsController Retourne une instance du controller
func NewStatsController(logBack *custlogger.Logger, KeyAPI string) StatsController {
	return StatsController{logBack, KeyAPI}
}

// ScoreFactsReturn score returned when requesting
type ScoreFactsReturn struct {
	NumRound *int    `json:"numRound"`
	SteamID  *string `json:"steamID"`

	RatioScore *float64 `json:"ratioScore"`
	TotalScore *float64 `json:"totalScore"`

	DamageScore     *float64 `json:"damageScore"`
	KillScore       *float64 `json:"killScore"`
	AssistKillScore *float64 `json:"assistKillScore"`
	DeathScore      *float64 `json:"deathScore"`

	DropScore        *float64 `json:"dropScore"`
	FlashScore       *float64 `json:"flashScore"`
	RevangedScore    *float64 `json:"revangedScore"`
	BombDefuseScore  *float64 `json:"bombDefuseScore"`
	BombPlantedScore *float64 `json:"bombPlantedScore"`
}

func copyTSRFrom(s *model.ScoreFacts) *ScoreFactsReturn {
	return &ScoreFactsReturn{
		SteamID:          s.SteamID,
		RatioScore:       s.RatioScore,
		TotalScore:       s.TotalScore,
		DamageScore:      s.DamageScore,
		KillScore:        s.KillScore,
		DeathScore:       s.DeathScore,
		DropScore:        s.DropScore,
		FlashScore:       s.FlashScore,
		RevangedScore:    s.RevangedScore,
		AssistKillScore:  s.AssistKillScore,
		BombDefuseScore:  s.BombDefuseScore,
		BombPlantedScore: s.BombPlantedScore,
	}
}

// WeaponMarksReturn weaponMarks returned when requesting
type WeaponMarksReturn struct {
	Accuracy            *float64 `json:"accuracy"`
	HS                  *float64 `json:"hs"`
	FirstBulletAccuracy *float64 `json:"firstBulletAccuracy"`
	FirstBulletHS       *float64 `json:"firstBulletHS"`

	NbrBulletsFired *int `json:"nbrBulletsFired"`
	NbrBulletsHit   *int `json:"nbrBulletsHit"`
	NbrBulletsHS    *int `json:"nbrBulletsHS"`

	NbrFirstBulletsFired *int `json:"nbrFirstBulletsFired"`
	NbrFirstBulletsHit   *int `json:"nbrFirstBulletsHit"`
	NbrFirstBulletsHS    *int `json:"nbrFirstBulletsHS"`

	Damage *int `json:"damage"`

	AverageVelocityShoots  *int `json:"averageVelocityShoots"`
	AverageDeltaXCrossHair *int `json:"averageDeltaXCrossHair"`
	AverageDeltaYCrossHair *int `json:"averageDeltaYCrossHair"`

	NbrDeaths *int `json:"nbrDeaths"`
	NbrKills  *int `json:"nbrKills"`
}

func copyTWMRFrom(m []*model.WeaponMarks) []*WeaponMarksReturn {
	var mReturn []*WeaponMarksReturn

	for _, weaponMarks := range m {
		mReturn = append(mReturn, &WeaponMarksReturn{
			Accuracy:               weaponMarks.Accuracy,
			HS:                     weaponMarks.HS,
			FirstBulletAccuracy:    weaponMarks.FirstBulletAccuracy,
			FirstBulletHS:          weaponMarks.FirstBulletHS,
			NbrBulletsFired:        weaponMarks.NbrBulletsFired,
			NbrBulletsHit:          weaponMarks.NbrBulletsHit,
			NbrBulletsHS:           weaponMarks.NbrBulletsHS,
			NbrFirstBulletsFired:   weaponMarks.NbrFirstBulletsFired,
			NbrFirstBulletsHit:     weaponMarks.NbrFirstBulletsHit,
			NbrFirstBulletsHS:      weaponMarks.NbrFirstBulletsHS,
			Damage:                 weaponMarks.Damage,
			AverageVelocityShoots:  weaponMarks.AverageVelocityShoots,
			AverageDeltaXCrossHair: weaponMarks.AverageDeltaXCrossHair,
			AverageDeltaYCrossHair: weaponMarks.AverageDeltaYCrossHair,
			NbrDeaths:              weaponMarks.NbrDeaths,
			NbrKills:               weaponMarks.NbrKills,
		})
	}

	return mReturn
}

// MarksReturn marks returned when requesting
type MarksReturn struct {
	Accuracy            *float64 `json:"accuracy"`
	HS                  *float64 `json:"hs"`
	FirstBulletAccuracy *float64 `json:"firstBulletAccuracy"`
	FirstBulletHS       *float64 `json:"firstBulletHS"`

	NbrBulletsFired *int `json:"nbrBulletsFired"`
	NbrBulletsHit   *int `json:"nbrBulletsHit"`
	NbrBulletsHS    *int `json:"nbrBulletsHS"`

	NbrFirstBulletsFired *int `json:"nbrFirstBulletsFired"`
	NbrFirstBulletsHit   *int `json:"nbrFirstBulletsHit"`
	NbrFirstBulletsHS    *int `json:"nbrFirstBulletsHS"`

	UtilityDamage      *int     `json:"utilityDamage"`
	Damage             *int     `json:"damage"`
	GrenadesValueDeath *float64 `json:"grenadesValueDeath"`

	AverageVelocityShoots  *int `json:"averageVelocityShoots"`
	AverageDeltaXCrossHair *int `json:"averageDeltaXCrossHair"`
	AverageDeltaYCrossHair *int `json:"averageDeltaYCrossHair"`

	NbrDeaths *int `json:"nbrDeaths"`
	NbrKills  *int `json:"nbrKills"`

	WeaponsMarks []*WeaponMarksReturn `json:"weaponsMarks"`
}

func copyTMRFrom(m *model.Marks) *MarksReturn {
	return &MarksReturn{
		Accuracy:               m.Accuracy,
		HS:                     m.HS,
		FirstBulletAccuracy:    m.FirstBulletAccuracy,
		FirstBulletHS:          m.FirstBulletHS,
		NbrBulletsFired:        m.NbrBulletsFired,
		NbrBulletsHit:          m.NbrBulletsHit,
		NbrBulletsHS:           m.NbrBulletsHS,
		NbrFirstBulletsFired:   m.NbrFirstBulletsFired,
		NbrFirstBulletsHit:     m.NbrFirstBulletsHit,
		NbrFirstBulletsHS:      m.NbrFirstBulletsHS,
		UtilityDamage:          m.UtilityDamage,
		Damage:                 m.Damage,
		GrenadesValueDeath:     m.GrenadesValueDeath,
		AverageVelocityShoots:  m.AverageVelocityShoots,
		AverageDeltaXCrossHair: m.AverageDeltaXCrossHair,
		AverageDeltaYCrossHair: m.AverageDeltaYCrossHair,
		NbrDeaths:              m.NbrDeaths,
		NbrKills:               m.NbrKills,
		WeaponsMarks:           copyTWMRFrom(m.WeaponsMarks),
	}
}

// MarksPerRoundReturn marks returned when requesting
type MarksPerRoundReturn struct {
	SteamID   *string `json:"steamID"`
	Side      *int    `json:"side"`
	ConstTeam *int    `json:"constTeam"`

	Accuracy            *float64 `json:"accuracy"`
	HS                  *float64 `json:"hs"`
	FirstBulletAccuracy *float64 `json:"firstBulletAccuracy"`
	FirstBulletHS       *float64 `json:"firstBulletHS"`

	NbrBulletsFired *int `json:"nbrBulletsFired"`
	NbrBulletsHit   *int `json:"nbrBulletsHit"`
	NbrBulletsHS    *int `json:"nbrBulletsHS"`

	NbrFirstBulletsFired *int `json:"nbrFirstBulletsFired"`
	NbrFirstBulletsHit   *int `json:"nbrFirstBulletsHit"`
	NbrFirstBulletsHS    *int `json:"nbrFirstBulletsHS"`

	UtilityDamage *int `json:"utilityDamage"`
	Damage        *int `json:"damage"`

	AverageVelocityShoots  *int `json:"averageVelocityShoots"`
	AverageDeltaXCrossHair *int `json:"averageDeltaXCrossHair"`
	AverageDeltaYCrossHair *int `json:"averageDeltaYCrossHair"`

	NbrDeaths *int `json:"nbrDeaths"`
	NbrKills  *int `json:"nbrKills"`
}

func copyTMPRRFrom(mmp *model.MarksPerRound) *MarksPerRoundReturn {
	return &MarksPerRoundReturn{
		SteamID:                mmp.SteamID,
		Side:                   mmp.Side,
		ConstTeam:              mmp.ConstTeam,
		Accuracy:               mmp.Accuracy,
		HS:                     mmp.HS,
		FirstBulletAccuracy:    mmp.FirstBulletAccuracy,
		FirstBulletHS:          mmp.FirstBulletHS,
		NbrBulletsFired:        mmp.NbrBulletsFired,
		NbrBulletsHit:          mmp.NbrBulletsHit,
		NbrBulletsHS:           mmp.NbrBulletsHS,
		NbrFirstBulletsFired:   mmp.NbrFirstBulletsFired,
		NbrFirstBulletsHit:     mmp.NbrFirstBulletsHit,
		NbrFirstBulletsHS:      mmp.NbrFirstBulletsHS,
		UtilityDamage:          mmp.UtilityDamage,
		Damage:                 mmp.Damage,
		AverageVelocityShoots:  mmp.AverageVelocityShoots,
		AverageDeltaXCrossHair: mmp.AverageDeltaXCrossHair,
		AverageDeltaYCrossHair: mmp.AverageDeltaYCrossHair,
		NbrDeaths:              mmp.NbrDeaths,
		NbrKills:               mmp.NbrKills,
	}
}

// PlayerStatsReturn player returned when requesting
type PlayerStatsReturn struct {
	ID      *uint   `json:"id"`
	SteamID *string `json:"steamID"`

	ScoreFacts *ScoreFactsReturn `json:"scoreFacts"`
	MVPsFacts  *int              `json:"MVPsFacts"`

	Marks *MarksReturn `json:"marks"`

	Rank       *int          `json:"rank"`
	Username   *string       `json:"username"`
	LinkAvatar *string       `json:"linkAvatar"`
	Color      *common.Color `json:"color"`

	Kills  *int `json:"kills"`
	Deaths *int `json:"deaths"`

	Assists *int           `json:"assists"`
	Duels   *HeatMapReturn `json:"duels"`
}

func copyTPRFrom(p model.PlayerStats) *PlayerStatsReturn {
	teamPlayer := &PlayerStatsReturn{
		ID:         p.ID,
		SteamID:    p.SteamID,
		ScoreFacts: copyTSRFrom(p.ScoreFacts),
		Marks:      copyTMRFrom(p.Marks),
		MVPsFacts:  p.MVPsFacts,
		Rank:       p.Rank,
		Username:   p.Username,
		Color:      p.Color,
		Kills:      p.Kills,
		Deaths:     p.Deaths,
		Assists:    p.Assists,
	}

	teamPlayer.Duels = &HeatMapReturn{}

	for x := 5; x > 0; x-- {
		duels := make(map[string]interface{})
		for _, duel := range p.DuelsPlayer {
			if strconv.Itoa(x) == string((*duel.Matchup)[0]) {
				if *duel.NbrOccurence == 0 {
					duels[string((*duel.Matchup)[2])] = "None"
				} else {
					duels[string((*duel.Matchup)[2])] = helpers.Round(float64(*duel.NbrWin)/float64(*duel.NbrOccurence), 2)
				}
			}
		}
		duels["matchup"] = strconv.Itoa(x)
		teamPlayer.Duels.Data = append(teamPlayer.Duels.Data, duels)
		teamPlayer.Duels.Keys = append(teamPlayer.Duels.Keys, strconv.Itoa(x))
	}

	return teamPlayer
}

// TeamReturn team returned when requesting
type TeamReturn struct {
	Score   *int                 `json:"score"`
	Players []*PlayerStatsReturn `json:"players"`
	Duels   *HeatMapReturn       `json:"duels"`
}

// MatrixKillsReturn matrixKills when requesting
type MatrixKillsReturn struct {
	KilledSteamID  *string `json:"killedSteamID"`
	KilledUsername *string `json:"killedUsername"`
	NbrKills       *int    `json:"nbrKills"`
}

// HeatMapReturn heatmap for nivo.rock
type HeatMapReturn struct {
	Data []map[string]interface{} `json:"data"`
	Keys []string                 `json:"keys"`
}

// HeatMapKillReturn heat map of a kill
type HeatMapKillReturn struct {
	DurationSinceRoundBegan *float64 `json:"durationSinceRoundBegan"`

	WeaponKiller  *common.EquipmentType `json:"weaponKiller"`
	SteamIDKiller *string               `json:"steamIDKiller"`
	SideKiller    *int                  `json:"sideKiller"`
	KillerPosX    *float64              `json:"killerPosX"`
	KillerPosY    *float64              `json:"killerPosY"`

	ActiveWeaponVictim *common.EquipmentType `json:"activeWeaponVictim"`
	SteamIDVictim      *string               `json:"steamIDVictim"`
	SideVictim         *int                  `json:"sideVictim"`
	VictimPosX         *float64              `json:"victimPosX"`
	VictimPosY         *float64              `json:"victimPosY"`
}

func copyTHMKRFrom(h []*model.HeatMapKill) []*HeatMapKillReturn {
	hReturn := []*HeatMapKillReturn{}
	for _, heatMapKill := range h {
		hReturn = append(hReturn, &HeatMapKillReturn{
			DurationSinceRoundBegan: helpers.FloatPtr(heatMapKill.DurationSinceRoundBegan.Seconds()),
			WeaponKiller:            heatMapKill.WeaponKiller,
			SteamIDKiller:           heatMapKill.SteamIDKiller,
			SideKiller:              heatMapKill.SideKiller,
			KillerPosX:              heatMapKill.KillerPosX,
			KillerPosY:              heatMapKill.KillerPosY,
			ActiveWeaponVictim:      heatMapKill.ActiveWeaponVictim,
			SteamIDVictim:           heatMapKill.SteamIDVictim,
			SideVictim:              heatMapKill.SideVictim,
			VictimPosX:              heatMapKill.VictimPosX,
			VictimPosY:              heatMapKill.VictimPosY,
		})
	}

	return hReturn
}

// HeatMapDmgReturn heat map of a shoot
type HeatMapDmgReturn struct {
	DurationSinceRoundBegan *float64 `json:"durationSinceRoundBegan"`

	WeaponShooter  *common.EquipmentType `json:"weaponShooter"`
	SteamIDShooter *string               `json:"steamIDShooter"`
	SideShooter    *int                  `json:"sideShooter"`
	ShooterPosX    *float64              `json:"shooterPosX"`
	ShooterPosY    *float64              `json:"shooterPosY"`

	ActiveWeaponVictim *common.EquipmentType `json:"activeWeaponVictim"`
	SteamIDVictim      *string               `json:"steamIDVictim"`
	SideVictim         *int                  `json:"sideVictim"`
	VictimPosX         *float64              `json:"victimPosX"`
	VictimPosY         *float64              `json:"victimPosY"`

	Dmg *int `json:"dmg"`
}

func copyTHMDRFrom(h []*model.HeatMapDmg) []*HeatMapDmgReturn {
	hReturn := []*HeatMapDmgReturn{}
	for _, heatMapDmg := range h {
		hReturn = append(hReturn, &HeatMapDmgReturn{
			DurationSinceRoundBegan: helpers.FloatPtr(heatMapDmg.DurationSinceRoundBegan.Seconds()),
			WeaponShooter:           heatMapDmg.WeaponShooter,
			SteamIDShooter:          heatMapDmg.SteamIDShooter,
			SideShooter:             heatMapDmg.SideShooter,
			ShooterPosX:             heatMapDmg.ShooterPosX,
			ShooterPosY:             heatMapDmg.ShooterPosY,
			ActiveWeaponVictim:      heatMapDmg.ActiveWeaponVictim,
			SteamIDVictim:           heatMapDmg.SteamIDVictim,
			SideVictim:              heatMapDmg.SideVictim,
			VictimPosX:              heatMapDmg.VictimPosX,
			VictimPosY:              heatMapDmg.VictimPosY,
			Dmg:                     heatMapDmg.Dmg,
		})
	}

	return hReturn
}

// WeaponPatternReturn weapon pattern returned to the front
type WeaponPatternReturn struct {
	BulletStats    []*BulletStatsReturn `json:"bulletStats"`
	PerfectPattern []*BulletStatsReturn `json:"perfectPattern"`
	MarkPattern    float64              `json:"markPattern"`

	WeaponType string `json:"weaponType"`
}

// BulletStatsReturn bullet stats returned to the front
type BulletStatsReturn struct {
	BulletNumber     int     `json:"bulletNumber"`
	NbrBulletFired   int64   `json:"nbrBulletFired"`
	NbrBulletHit     int64   `json:"nbrBulletHit"`
	NbrBulletHitHS   int64   `json:"nbrBulletHitHS"`
	NbrBulletKill    int64   `json:"nbrBulletKill"`
	CumulativeDeltaX float64 `json:"cumulativeDeltaX"`
	CumulativeDeltaY float64 `json:"cumulativeDeltaY"`
}

func copyTBSRFrom(bss []*model.BulletStats) []*BulletStatsReturn {
	var bssr []*BulletStatsReturn
	for _, bs := range bss {
		bssr = append(bssr, &BulletStatsReturn{
			BulletNumber:     bs.BulletNumber,
			NbrBulletFired:   bs.NbrBulletFired,
			NbrBulletHit:     bs.NbrBulletHit,
			NbrBulletHitHS:   bs.NbrBulletHitHS,
			NbrBulletKill:    bs.NbrBulletKill,
			CumulativeDeltaX: bs.CumulativeDeltaX,
			CumulativeDeltaY: bs.CumulativeDeltaY,
		})
	}

	return bssr
}

// Get return the stats of one game
func (sc *StatsController) Get(c *gin.Context) {
	db := store.GetDB(c)
	userDao := dao.NewUserDAO(db)
	user, _ := userDao.GetUserFromContext(c)

	demoID, err := helpers.ToInt64(c.Param("demoID"))
	if err != nil {
		helpers.SendError(c,
			sc.LoggerBack,
			helpers.ErrInvalidInputURL(),
			helpers.MakeEasyDetail(helpers.URL, "demoID"),
			err,
		)
		return
	}

	demo := &model.Demo{ID: &demoID}
	demoDao := dao.NewDemoDAO(db, sc.LoggerBack)

	if exists, err := demoDao.Exists(*demo); err != nil {
		helpers.SendError(c, sc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	} else if !exists {
		helpers.SendError(c, sc.LoggerBack, helpers.ErrNotFound(), nil, err)
		return
	}

	if err := demoDao.GetWithStats(demo); err != nil {
		helpers.SendError(c, sc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	demoTmp := copyTDRFrom(*demo)
	demoTmp.BestMarks = copyTMRFrom(demo.FindBestMarks())
	demoTmp.BaddestMarks = copyTMRFrom(demo.FindBaddestMarks())

	if *demo.VersionAnalyzer != 0 {
		demoTmp.Score = &[]int{*demo.TeamsStats[0].Score, *demo.TeamsStats[1].Score}
		if user != nil && user.SteamID != nil {
			demoTmp.EndState = demo.IsWin(user.SteamID)
		}

		for _, team := range demo.TeamsStats {
			teamPlayers := []*PlayerStatsReturn{}

			for _, player := range team.PlayersStats {
				flashs := make(map[string]interface{})
				for _, flash := range player.MatrixFlashs {
					flashedUsername := helpers.TruncatString(*flash.FlashedUsername, 15)
					flashs[flashedUsername] = helpers.Round(*flash.NbrSecFlashs, 1)
				}
				username := helpers.TruncatString(*player.Username, 15)
				flashs["flasher"] = username
				demoTmp.MatrixFlashs.Data = append(demoTmp.MatrixFlashs.Data, flashs)
				demoTmp.MatrixFlashs.Keys = append(demoTmp.MatrixFlashs.Keys, username)

				kills := make(map[string]interface{})
				for _, kill := range player.MatrixKills {
					killedUsername := helpers.TruncatString(*kill.KilledUsername, 15)
					kills[killedUsername] = kill.NbrKills
				}
				kills["killer"] = username
				demoTmp.MatrixKills.Data = append(demoTmp.MatrixKills.Data, kills)
				demoTmp.MatrixKills.Keys = append(demoTmp.MatrixKills.Keys, username)

				teamPlayer := copyTPRFrom(*player)
				teamPlayers = append(teamPlayers, teamPlayer)
			}

			localTeam := &TeamReturn{
				Players: teamPlayers,
				Score:   team.Score,
				Duels:   &HeatMapReturn{},
			}

			for x := 5; x > 0; x-- {
				duels := make(map[string]interface{})
				for _, duel := range team.DuelsTeam {
					if strconv.Itoa(x) == string((*duel.Matchup)[0]) {
						if *duel.NbrOccurence == 0 {
							duels[string((*duel.Matchup)[2])] = "None"
						} else {
							duels[string((*duel.Matchup)[2])] = helpers.Round(float64(*duel.NbrWin)/float64(*duel.NbrOccurence), 2)
						}
					}
				}
				duels["matchup"] = strconv.Itoa(x)
				localTeam.Duels.Data = append(localTeam.Duels.Data, duels)
				localTeam.Duels.Keys = append(localTeam.Duels.Keys, strconv.Itoa(x))
			}

			demoTmp.Teams = append(demoTmp.Teams, localTeam)
		}
	} else {
		demoTmp.Score = &[]int{0, 0}
	}

	getLinksAvatar(*sc.LoggerBack, sc.KeyAPI, demoTmp.Teams)

	helpers.SendData(c, http.StatusOK, demoTmp)
}

func getLinksAvatar(logger custlogger.Logger, key string, teams []*TeamReturn) {
	req, err := http.NewRequest("GET", "https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v2", nil)
	if err != nil {
		logger.Error(err, "error while setuping request for steam api for avatar", err)
		return
	}

	q := req.URL.Query()

	var steamIDs string

	for _, team := range teams {
		for _, player := range team.Players {
			steamIDs += fmt.Sprintf("%s, ", *player.SteamID)
		}
	}

	q.Add("key", key)
	q.Add("steamids", steamIDs)
	req.URL.RawQuery = q.Encode()

	client := http.Client{}

	dataGot, err := client.Do(req)
	if err != nil {
		logger.Error(err, "error while requesting steam api for next share code", err)
		return
	}

	defer func() {
		if err := dataGot.Body.Close(); err != nil {
			logger.Error(err, "error while closing body response", err)
		}
	}()

	bodyBytes, err := ioutil.ReadAll(dataGot.Body)
	if err != nil {
		logger.Error(err, "error while reading request response", err)
		return
	}

	if dataGot.StatusCode == http.StatusOK {
		linksAvatar := struct {
			Response struct {
				Players []struct {
					SteamID string `json:"steamid"`
					Avatar  string `json:"avatar"`
				} `json:"players"`
			} `json:"response"`
		}{}

		if err := json.Unmarshal(bodyBytes, &linksAvatar); err != nil {
			return
		}

		for numLinkAvatar, linkAvatar := range linksAvatar.Response.Players {
			for _, team := range teams {
				for _, player := range team.Players {
					if *player.SteamID == linkAvatar.SteamID {
						player.LinkAvatar = &linksAvatar.Response.Players[numLinkAvatar].Avatar
					}
				}
			}
		}
	}
}

// WeaponsStatsDemosReturn type representing differents stats of weapon divided
type WeaponsStatsDemosReturn struct {
	WeaponStatsPerDay           WeaponsStatsReturn                           `json:"weaponStatsPerDay"`
	WeaponTypeStatsPerDay       map[common.EquipmentType]*WeaponsStatsReturn `json:"weaponTypeStatsPerDay"`
	WeaponMarksPerRoundPerDay   map[int]*WeaponsStatsReturn                  `json:"weaponMarksPerRoundPerDay"`
	WeaponStatsPerWeek          WeaponsStatsReturn                           `json:"weaponStatsPerWeek"`
	WeaponTypeStatsPerWeek      map[common.EquipmentType]*WeaponsStatsReturn `json:"weaponTypeStatsPerWeek"`
	WeaponMarksPerRoundPerWeek  map[int]*WeaponsStatsReturn                  `json:"weaponMarksPerRoundPerWeek"`
	WeaponStatsPerMonth         WeaponsStatsReturn                           `json:"weaponStatsPerMonth"`
	WeaponTypeStatsPerMonth     map[common.EquipmentType]*WeaponsStatsReturn `json:"weaponTypeStatsPerMonth"`
	WeaponMarksPerRoundPerMonth map[int]*WeaponsStatsReturn                  `json:"weaponMarksPerRoundPerMonth"`
}

// WeaponsStatsReturn stats for a weapon
type WeaponsStatsReturn struct {
	WeaponType  common.EquipmentType  `json:"weaponType"`
	WeaponClass common.EquipmentClass `json:"weaponClass"`
	WeaponName  string                `json:"weaponName"`

	FirstBulletAccuracy DatasetsReturn `json:"firstBulletAccuracy"`
	FirstBulletHS       DatasetsReturn `json:"firstBulletHS"`
	BulletAccuracy      DatasetsReturn `json:"bulletAccuracy"`
	HSPercentage        DatasetsReturn `json:"hSPercentage"`
	NbrBulletsFired     DatasetsReturn `json:"nbrBulletsFired"`

	AverageVelocityAttacker DatasetsReturn `json:"averageVelocityAttacker"`

	NbrBulletFired int `json:"-"`
	NbrBulletHit   int `json:"-"`
	NbrBulletHS    int `json:"-"`

	NbrFirstBulletFired int `json:"-"`
	NbrFirstBulletHit   int `json:"-"`
	NbrFirstBulletHS    int `json:"-"`

	CumulativeVelocityAttacker float64 `json:"-"`
	NbrGame                    int     `json:"-"`
}

// GetDeathMatchs return the stats of the DM
func (sc *StatsController) GetDeathMatchs(c *gin.Context) {
	helpers.SendData(c, http.StatusNotImplemented, nil)
}

// GetCompetitives return the stats of the competitives games
func (sc *StatsController) GetCompetitives(c *gin.Context) {
	db := store.GetDB(c)
	userDao := dao.NewUserDAO(db)
	user, _ := userDao.GetUserFromContext(c)
	if user == nil || user.SteamID == nil {
		helpers.SendError(c, sc.LoggerBack, helpers.ErrBadRequest(), nil, nil)
		return
	}

	filters := FiltersGetDemos{}
	if err := c.ShouldBindQuery(&filters); err != nil {
		helpers.SendError(c, sc.LoggerBack, helpers.ErrInvalidInputURL(), nil, err)
		return
	}

	demoDAO := dao.NewDemoDAO(db, sc.LoggerBack)
	demos, err := demoDAO.GetAllFromUserWithMarks(
		*user.SteamID,
		filters.Sources,
		filters.Maps,
		filters.DateFrom,
		filters.DateTo,
	)
	if err != nil {
		helpers.SendError(c, sc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	weaponsStatsTimeFramesReturn := []*progression.WeaponsStatsTimeFrameReturn{
		{TimeFrame: progression.DailyTimeStamp, Index: 0},
		{TimeFrame: progression.WeeklyTimeStamp, Index: 1},
		{TimeFrame: progression.MonthlyTimeStamp, Index: 2},
	}
	var weaponsPattern progression.WeaponsPattern = demos

	if err := weaponsPattern.GetProgression(weaponsStatsTimeFramesReturn, *user.SteamID); err != nil {
		helpers.SendError(c, sc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	helpers.SendData(c, http.StatusOK, weaponsStatsTimeFramesReturn)
}

// GetHeatMaps return the heatmaps of one map for a specific player
func (sc *StatsController) GetHeatMaps(c *gin.Context) {
	db := store.GetDB(c)
	steamID := c.Param("steamID")
	if steamID == "" {
		sc.LoggerBack.Info("no steamID")
		helpers.SendError(c, sc.LoggerBack, helpers.ErrBadRequest(), nil, nil)
		return
	}

	mapName := c.Param("mapName")
	if mapName == "" {
		sc.LoggerBack.Info("no mapName")
		helpers.SendError(c, sc.LoggerBack, helpers.ErrBadRequest(), nil, nil)
		return
	}

	demoDao := dao.NewDemoDAO(db, sc.LoggerBack)
	demos, err := demoDao.GetAllHeatMapFromUserFromMap(steamID, mapName, nil)
	if err != nil {
		helpers.SendError(c, sc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	heatMapKills := []*HeatMapKillReturn{}
	heatMapDmgs := []*HeatMapDmgReturn{}
	for _, demo := range demos {
		for _, round := range demo.Rounds {
			heatMapKills = append(heatMapKills, copyTHMKRFrom(round.HeatMapKills)...)
			heatMapDmgs = append(heatMapDmgs, copyTHMDRFrom(round.HeatMapDmgs)...)
		}
	}

	helpers.SendData(c, http.StatusOK, []interface{}{heatMapKills, heatMapDmgs})
}

// GetPatterns return the patterns for a specific player
func (sc *StatsController) GetPatterns(c *gin.Context) {
	db := store.GetDB(c)
	steamID := c.Param("steamID")
	if steamID == "" {
		sc.LoggerBack.Infof("no steamID")
		helpers.SendError(c, sc.LoggerBack, helpers.ErrBadRequest(), nil, nil)
		return
	}

	demoDao := dao.NewDemoDAO(db, sc.LoggerBack)
	playerStats, err := demoDao.GetAllPatternsFromUser(steamID, nil)
	if err != nil {
		helpers.SendError(c, sc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	mapWeaponPatternsReturn := make(map[common.EquipmentType]*WeaponPatternReturn)

	mapMarkPerWeapon := make(map[common.EquipmentType]*struct {
		CumulativeDeltaX float64
		CumulativeDeltaY float64
		nbrBullet        int64
		nbrBulletHit     int64
	})
	for _, playerStat := range playerStats {
		for _, weaponPattern := range playerStat.WeaponPatterns {
			perfectPattern, exists := modelanalyzer.PerfectPatterns[weaponPattern.WeaponType]
			if !exists {
				continue
			}

			if _, exists := mapWeaponPatternsReturn[weaponPattern.WeaponType]; !exists {
				mapWeaponPatternsReturn[weaponPattern.WeaponType] = &WeaponPatternReturn{
					WeaponType: weaponPattern.WeaponType.String(),
				}
			}

			pattern := mapWeaponPatternsReturn[weaponPattern.WeaponType]
			pattern.PerfectPattern = copyTBSRFrom(perfectPattern)

			for numBullet, bullet := range weaponPattern.BulletStats {
				if numBullet == 0 {
					continue
				}
				for len(pattern.BulletStats) < numBullet+1 {
					pattern.BulletStats = append(pattern.BulletStats, &BulletStatsReturn{
						BulletNumber: len(pattern.BulletStats),
					})
				}
				if bullet.NbrBulletFired == 0 {
					continue
				}
				if _, exists := mapMarkPerWeapon[weaponPattern.WeaponType]; !exists {
					mapMarkPerWeapon[weaponPattern.WeaponType] = &struct {
						CumulativeDeltaX float64
						CumulativeDeltaY float64
						nbrBullet        int64
						nbrBulletHit     int64
					}{}
				}

				markWeaponType := mapMarkPerWeapon[weaponPattern.WeaponType]
				markWeaponType.CumulativeDeltaX += bullet.CumulativeDeltaX / float64(bullet.NbrBulletFired)
				markWeaponType.CumulativeDeltaY += bullet.CumulativeDeltaY / float64(bullet.NbrBulletFired)
				markWeaponType.nbrBullet++
				markWeaponType.nbrBulletHit += bullet.NbrBulletHit

				pattern.BulletStats[numBullet].CumulativeDeltaX += bullet.CumulativeDeltaX
				pattern.BulletStats[numBullet].CumulativeDeltaY += bullet.CumulativeDeltaY
				pattern.BulletStats[numBullet].NbrBulletFired += bullet.NbrBulletFired
				pattern.BulletStats[numBullet].NbrBulletHit += bullet.NbrBulletHit
				pattern.BulletStats[numBullet].NbrBulletHitHS += bullet.NbrBulletHitHS
				pattern.BulletStats[numBullet].NbrBulletKill += bullet.NbrBulletKill
			}
		}
	}

	var weaponPatternReturn []*WeaponPatternReturn
	for weaponType, mapWeaponPatternReturn := range mapWeaponPatternsReturn {
		markWeapon := mapMarkPerWeapon[weaponType]
		mapWeaponPatternReturn.MarkPattern =
			math.Max(0,
				(math.Sqrt(2)-math.Sqrt(math.Abs(markWeapon.CumulativeDeltaX/float64(markWeapon.nbrBullet))))/math.Sqrt(2)*100+
					(2-math.Sqrt(math.Abs(markWeapon.CumulativeDeltaY/float64(markWeapon.nbrBullet))))/2*100)
		mapWeaponPatternReturn.MarkPattern /= 2
		weaponPatternReturn = append(weaponPatternReturn, mapWeaponPatternReturn)
	}

	helpers.SendData(c, http.StatusOK, weaponPatternReturn)
}
