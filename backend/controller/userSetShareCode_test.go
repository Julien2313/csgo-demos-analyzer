package controller_test

import (
	"encoding/json"
	"net/http"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/mock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/server"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/downloaderinfo"
)

const (
	methodeShareCode = http.MethodPost
	urlShareCode     = "/api/v1/user/sharecode"
)

func TestControllerUser_SetShareCodeMustBeConnected(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	assert.Nil(t, checkMustBeConnected(serverTest.Router, true, methodeShareCode, urlShareCode))
}

func TestControllerUser_SetShareCodeOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	userDAO := dao.NewUserDAO(localDB)

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	httpCookie := performTestAuth(serverTest.Router, *userGenerated)

	shareCodeBytes, err := json.Marshal(&tmpShareCodeSend{
		ShareCode: "CSGO-wGfVh-PWWy2-BW4Lx-UPREK-WTZfE",
	})
	require.NoError(t, err)

	resp := sendRequestWithToken(serverTest.Router,
		shareCodeBytes,
		methodeShareCode,
		urlShareCode,
		httpCookie)
	require.Equal(t, http.StatusOK, resp.Code)

	userFromDB := model.User{Mail: userGenerated.Mail}
	require.NoError(t, userDAO.GetUserByMail(&userFromDB))
	assert.Equal(t, "CSGO-wGfVh-PWWy2-BW4Lx-UPREK-WTZfE", *userFromDB.ShareCode)

	userFromDB2 := *userGenerated2
	require.NoError(t, userDAO.GetUserByMail(&userFromDB2))
	assert.Equal(t, userGenerated2.ShareCode, userFromDB2.ShareCode)
}

func TestControllerUser_SetShareCodeNOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	userDAO := dao.NewUserDAO(localDB)

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	httpCookie := performTestAuth(serverTest.Router, *userGenerated)

	shareCodeBytes, err := json.Marshal(&tmpShareCodeSend{
		ShareCode: "CSGO-wGfVh-PWWy2-BW4Lx-UPREK",
	})
	require.NoError(t, err)

	resp := sendRequestWithToken(serverTest.Router,
		shareCodeBytes,
		methodeShareCode,
		urlShareCode,
		httpCookie)
	require.Equal(t, http.StatusBadRequest, resp.Code)

	userFromDB := model.User{Mail: userGenerated.Mail}
	require.NoError(t, userDAO.GetUserByMail(&userFromDB))
	assert.Equal(t, userGenerated.ShareCode, userFromDB.ShareCode)

	shareCodeBytes, err = json.Marshal(&tmpShareCodeSend{
		ShareCode: "wGfVh-PWWy2-BW4Lx-UPREK-WTZfE",
	})
	require.NoError(t, err)

	resp = sendRequestWithToken(serverTest.Router,
		shareCodeBytes,
		methodeShareCode,
		urlShareCode,
		httpCookie)
	require.Equal(t, http.StatusBadRequest, resp.Code)

	userFromDB = model.User{Mail: userGenerated.Mail}
	require.NoError(t, userDAO.GetUserByMail(&userFromDB))
	assert.Equal(t, userGenerated.ShareCode, userFromDB.ShareCode)

	shareCodeBytes, err = json.Marshal(&tmpShareCodeSend{
		ShareCode: "CSGO-wGfVh-PWWy2-B4Lx-UPREK-WTZfE",
	})
	require.NoError(t, err)

	resp = sendRequestWithToken(serverTest.Router,
		shareCodeBytes,
		methodeShareCode,
		urlShareCode,
		httpCookie)
	require.Equal(t, http.StatusBadRequest, resp.Code)

	userFromDB = model.User{Mail: userGenerated.Mail}
	require.NoError(t, userDAO.GetUserByMail(&userFromDB))
	assert.Equal(t, userGenerated.ShareCode, userFromDB.ShareCode)

	resp = sendRequestWithToken(serverTest.Router,
		[]byte("teeeest"),
		methodeShareCode,
		urlShareCode,
		httpCookie)
	require.Equal(t, http.StatusBadRequest, resp.Code)

	userFromDB = model.User{Mail: userGenerated.Mail}
	require.NoError(t, userDAO.GetUserByMail(&userFromDB))
	assert.Equal(t, userGenerated.ShareCode, userFromDB.ShareCode)
}
