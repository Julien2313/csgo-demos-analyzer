package controller_test

import (
	"net/http"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/mock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/server"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/downloaderinfo"
)

const (
	methodeCheckAuth = http.MethodGet
	urlCheckAuth     = "/api/v1/auth"
)

func TestControllerAuth_CheckAuthOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	httpCookie := performTestAuth(serverTest.Router, *userGenerated)
	require.NotNil(t, httpCookie)

	resp := sendRequestWithToken(serverTest.Router,
		nil,
		methodeCheckAuth,
		urlCheckAuth,
		httpCookie)
	assert.Equal(t, http.StatusOK, resp.Code)
}

func TestControllerAuth_CheckAuthNOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	httpCookie := &http.Cookie{
		Name:  "bad name",
		Value: "bad value",
	}

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}

	resp := sendRequestWithToken(serverTest.Router,
		nil,
		methodeCheckAuth,
		urlCheckAuth,
		httpCookie)
	assert.Equal(t, http.StatusUnauthorized, resp.Code)
}
