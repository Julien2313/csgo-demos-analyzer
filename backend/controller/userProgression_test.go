package controller_test

import (
	"encoding/json"
	"net/http"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/controller"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/mock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/server"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/downloaderinfo"
)

const (
	methodeProgression = http.MethodGet
	urlProgression     = "/api/v1/user/progression"
)

func TestControllerUser_ProgressionMustBeConnected(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	assert.Nil(t, checkMustBeConnected(serverTest.Router, true, methodeProgression, urlProgression))
}

func TestControllerUser_ProgressionOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	httpCookie := performTestAuth(serverTest.Router, *userGenerated)

	resp := sendRequestWithToken(serverTest.Router, nil, methodeProgression, urlProgression, httpCookie)
	require.Equal(t, http.StatusOK, resp.Code)

	var response map[string]controller.DatasetsReturn

	require.NoError(t, json.Unmarshal(resp.Body.Bytes(), &response))
	dataSets := response["data"]
	progression := dataSets
	require.Len(t, progression.Data, len(demosGeneratedAnalyzed))

	for numFlashScore, flashScore := range progression.Data {
		dateFromDB := demosGeneratedAnalyzed[len(demosGeneratedAnalyzed)-numFlashScore-1].Date.Unix()

		assert.Equal(t, dateFromDB, flashScore.X)

		if numFlashScore == len(progression.Data)-1 {
			break
		}

		date := time.Unix(progression.Data[numFlashScore].X, 0)

		nextDate := time.Unix(progression.Data[numFlashScore+1].X, 0)
		assert.True(t, date.Unix() <= nextDate.Unix())
		player := demosGeneratedAnalyzed[len(demosGeneratedAnalyzed)-numFlashScore-1].FindPlayer(userGenerated.SteamID)
		assert.Equal(t, *player.ScoreFacts.FlashScore, flashScore.Y)
	}
}

func TestControllerUser_ProgressionNOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	userDAO := dao.NewUserDAO(localDB)
	localUserGenerated := model.StoreGeneratedUser(localDB)
	localUserGenerated.SteamID = helpers.StrPtr("")
	require.NoError(t, userDAO.UpdateSteamID(localUserGenerated))

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	httpCookie := performTestAuth(serverTest.Router, *localUserGenerated)

	resp := sendRequestWithToken(serverTest.Router,
		nil,
		methodeProgression,
		urlProgression,
		httpCookie)
	assert.Equal(t, http.StatusForbidden, resp.Code)
}
