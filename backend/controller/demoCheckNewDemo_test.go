package controller_test

import (
	"math/rand"
	"net/http"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/mock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/server"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/downloaderinfo"
)

const (
	methodeCheckNewDemo = http.MethodPost
	urlCheckNewDemo     = "/api/v1/demo/checknewdemo"
)

func TestControllerDemo_CheckNewDemo(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	userDAO := dao.NewUserDAO(localDB)
	userPicked := mock.UsersMocked[rand.Intn(mock.NbrUser)]

	localUserGenerated := model.GenerateRandomUser(false)
	localUserGenerated.ID = helpers.RandUIntPSQL()
	localUserGenerated.SteamID = userPicked.SteamID
	localUserGenerated.AccessTokenHistory = userPicked.SteamIDKey
	localUserGenerated.ShareCode = &userPicked.CurrentMatchSharingCode.KnownCode

	password := *localUserGenerated.Password
	require.NoError(t, localUserGenerated.HashPassword())

	require.NoError(t, userDAO.Create(localUserGenerated))

	localUserGenerated.Password = &password

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	httpCookie := performTestAuth(serverTest.Router, *localUserGenerated)
	require.NotNil(t, httpCookie)

	resp := sendRequestWithToken(serverTest.Router,
		nil,
		methodeCheckNewDemo,
		urlCheckNewDemo,
		httpCookie)
	assert.Equal(t, http.StatusOK, resp.Code)
}

func TestControllerDemo_CheckNewDemoBadToken(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	userDAO := dao.NewUserDAO(localDB)
	userPicked := mock.UsersMocked[rand.Intn(mock.NbrUser)]

	localUserGenerated := model.GenerateRandomUser(false)
	localUserGenerated.ID = helpers.RandUIntPSQL()
	localUserGenerated.SteamID = userPicked.SteamID
	localUserGenerated.AccessTokenHistory = userPicked.SteamIDKey
	localUserGenerated.ShareCode = model.GenerateRandomShareCode()

	password := *localUserGenerated.Password
	require.NoError(t, localUserGenerated.HashPassword())

	require.NoError(t, userDAO.Create(localUserGenerated))

	localUserGenerated.Password = &password

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	httpCookie := performTestAuth(serverTest.Router, *localUserGenerated)
	require.NotNil(t, httpCookie)

	resp := sendRequestWithToken(serverTest.Router,
		nil,
		methodeCheckNewDemo,
		urlCheckNewDemo,
		httpCookie)
	assert.Equal(t, http.StatusUnprocessableEntity, resp.Code)

	userPicked = mock.UsersMocked[rand.Intn(mock.NbrUser)]

	localUserGenerated = model.GenerateRandomUser(false)
	localUserGenerated.ID = helpers.RandUIntPSQL()
	localUserGenerated.SteamID = userPicked.SteamID
	localUserGenerated.AccessTokenHistory = userPicked.SteamIDKey
	localUserGenerated.ShareCode = helpers.StrPtr("")

	password = *localUserGenerated.Password
	require.NoError(t, localUserGenerated.HashPassword())

	require.NoError(t, userDAO.Create(localUserGenerated))

	localUserGenerated.Password = &password
	httpCookie = performTestAuth(serverTest.Router, *localUserGenerated)
	require.NotNil(t, httpCookie)

	resp = sendRequestWithToken(serverTest.Router,
		nil,
		methodeCheckNewDemo,
		urlCheckNewDemo,
		httpCookie)
	assert.Equal(t, http.StatusForbidden, resp.Code)
}
