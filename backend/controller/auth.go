package controller

import (
	"fmt"
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gorm.io/gorm"
)

// AuthController s'occupe de l'authentification des Utilisateurs
// pour la gestion des comptes, voir UserController
type AuthController struct {
	LoggerBack *custlogger.Logger
}

// NewAuthController Retourne une instance du controller
func NewAuthController(logBack *custlogger.Logger) AuthController {
	return AuthController{logBack}
}

// AuthReturn struct returned when auth
type AuthReturn struct {
	APIWorking *bool   `json:"apiWorking"`
	SteamID    *string `json:"steamID"`
}

// Authentication authenticate the user for futurs requests
func (ac AuthController) Authentication(c *gin.Context) {
	db := store.GetDB(c)

	type tmpCredentials struct {
		TmpUser struct {
			Mail     *string `json:"mail" binding:"required"`
			Password *string `json:"password" binding:"required"`
		} `json:"credentials" binding:"required"`
	}

	credentials := &tmpCredentials{}
	if err := c.ShouldBindJSON(credentials); err != nil {
		helpers.SendError(c, ac.LoggerBack, helpers.ErrInvalidInputJSON(), nil, err)
		return
	}

	user := model.User{
		Mail:     credentials.TmpUser.Mail,
		Password: credentials.TmpUser.Password,
	}

	if !ac.formAuth(db, &user, c) {
		return
	}

	authReturn := AuthReturn{APIWorking: user.APIValveWorking, SteamID: user.SteamID}

	helpers.SendData(c, http.StatusOK, authReturn)
}

// CheckAuth check if the user's token is known
func (ac AuthController) CheckAuth(c *gin.Context) {
	db := store.GetDB(c)
	userDao := dao.NewUserDAO(db)

	user, err := userDao.GetUserFromContext(c)
	if err != nil {
		helpers.SendError(c, ac.LoggerBack, helpers.ErrBadTokenConnection(), nil, err)
		return
	}

	helpers.SendData(c, http.StatusOK, user.SteamID)
}

//Logout disconnect the user
func (ac AuthController) Logout(c *gin.Context) {
	session := sessions.Default(c)
	session.Clear()

	if err := session.Save(); err != nil {
		helpers.SendError(c, ac.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	helpers.SendData(c, http.StatusOK, nil)
}

func (ac AuthController) formAuth(db *gorm.DB, user *model.User, c *gin.Context) bool {
	password := *user.Password
	userDao := dao.NewUserDAO(db)

	if err := userDao.GetUserByMail(user); err != nil {
		helpers.SendError(c, ac.LoggerBack, helpers.ErrBadCredentials(), nil, err)
		return false
	}

	if err := user.CheckPassword(password); err != nil {
		helpers.SendError(c, ac.LoggerBack, helpers.ErrBadCredentials(), nil, err)
		return false
	}

	if err := ac.performAuth(c, *user.ID); err != nil {
		helpers.SendError(c, ac.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return false
	}

	return true
}

func (ac AuthController) performAuth(c *gin.Context, userID uint) error {
	session := sessions.Default(c)
	session.Set(helpers.Userkey, fmt.Sprint(userID))

	return session.Save()
}
