package controller_test

import (
	"math/rand"
	"net/http"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/mock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/server"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/downloaderinfo"
)

const (
	methodeCheckTokens = http.MethodGet
	urlCheckTokens     = "/api/v1/user/checktokens"
)

func TestControllerUser_CheckTokens(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	userDAO := dao.NewUserDAO(localDB)
	userPicked := mock.UsersMocked[rand.Intn(mock.NbrUser)]
	localUserGenerated := model.StoreGeneratedUser(localDB)
	localUserGenerated.SteamID = userPicked.SteamID
	require.NoError(t, userDAO.UpdateSteamID(localUserGenerated))

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	httpCookie := performTestAuth(serverTest.Router, *localUserGenerated)

	resp := sendRequestWithToken(serverTest.Router,
		nil,
		methodeCheckTokens,
		urlCheckTokens,
		httpCookie)
	require.Equal(t, http.StatusBadRequest, resp.Code)

	localUserGenerated.AccessTokenHistory = userPicked.SteamIDKey
	require.NoError(t, userDAO.UpdateAccessTokenHistory(localUserGenerated))
	localUserGenerated.ShareCode = &userPicked.CurrentMatchSharingCode.KnownCode
	require.NoError(t, userDAO.UpdateShareCode(localUserGenerated))

	resp = sendRequestWithToken(serverTest.Router,
		nil,
		methodeCheckTokens,
		urlCheckTokens,
		httpCookie)
	assert.Equal(t, http.StatusOK, resp.Code)
}
