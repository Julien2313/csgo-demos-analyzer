package controller_test

import (
	"encoding/json"
	"net/http"
	"os"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/controller"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/mock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/server"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/downloaderinfo"
)

const (
	methodeGetLastAnalyzed = http.MethodGet
	urlGetLastAnalyzed     = "/api/v1/demo/lastAnalyzed"
)

func TestControllerDemo_GetLastAnalyzedMustBeConnected(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	assert.Nil(t, checkMustBeConnected(serverTest.Router, false, methodeGetLastAnalyzed, urlGetLastAnalyzed))
}

func TestControllerDemo_GetLastAnalyzedOK(t *testing.T) {
	if os.Getenv("GITLAB") != "" {
		t.Skip()
	}
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	httpCookie := performTestAuth(serverTest.Router, *userGeneratedVirgin)
	require.NotNil(t, httpCookie)

	resp := sendRequestWithToken(serverTest.Router,
		nil,
		methodeGetLastAnalyzed,
		urlGetLastAnalyzed,
		httpCookie)
	require.Equal(t, http.StatusOK, resp.Code)

	var response map[string]controller.GetLastAnalyzedReturn

	require.NoError(t, json.Unmarshal(resp.Body.Bytes(), &response))
	demosResponse := response["data"].Demos

	assert.Len(t, demosResponse, nbrDemosGenerated)
}
