package controller_test

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/maxatome/go-testdeep/td"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/controller"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/mock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/server"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/downloaderinfo"
)

const (
	methodeGetDemo = http.MethodGet
	urlGetDemo     = "/api/v1/stats/demo/:demoID"
)

func TestControllerDemo_GetDemoAnalyzedOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	httpCookie := performTestAuth(serverTest.Router, *userGenerated)
	require.NotNil(t, httpCookie)

	demo := demosGeneratedAnalyzed[0]

	resp := sendRequestWithToken(serverTest.Router,
		nil,
		methodeGetDemo,
		strings.Replace(urlGetDemo, ":demoID", strconv.FormatInt(*demo.ID, 10), 1),
		httpCookie)
	require.Equal(t, http.StatusOK, resp.Code)

	var response map[string]controller.DemoReturn

	require.NoError(t, json.Unmarshal(resp.Body.Bytes(), &response))

	demoResponse := response["data"]
	demoResponse.Date = helpers.TimePtr(demoResponse.Date.In(time.UTC))

	assert.Equal(t, strconv.FormatInt(*demo.ID, 10), *demoResponse.ID)
	assert.Equal(t, *demo.VersionAnalyzer, *demoResponse.VersionAnalyzer)
	assert.Equal(t, demo.Date.Unix(), demoResponse.Date.Unix())
	assert.Equal(t, *demo.MapName, *demoResponse.MapName)
	assert.Equal(t, *demo.Name, *demoResponse.Name)
	assert.Equal(t, *demo.Size, *demoResponse.Size)
	assert.Equal(t, demo.IsWin(userGenerated.SteamID), demoResponse.EndState)
	assert.Equal(t, []int{*demo.TeamsStats[0].Score, *demo.TeamsStats[1].Score}, *demoResponse.Score)

	require.Len(t, demoResponse.Teams, 2)
	assert.Len(t, demo.TeamsStats, len(demoResponse.Teams))

	for numTeam := 1; numTeam > 0; numTeam-- {
		demoGen := demo.TeamsStats[numTeam]
		teamResp := demoResponse.Teams[numTeam]
		assert.Equal(t, *demoGen.Score, *teamResp.Score)

		require.Len(t, teamResp.Players, 5)
		assert.Len(t, demoGen.PlayersStats, len(teamResp.Players))

		for numPlayer := 0; numPlayer < 5; numPlayer++ {
			playerGen := demoGen.PlayersStats[numPlayer]
			playerResp := teamResp.Players[numPlayer]
			assert.Equal(t, *playerGen.ID, *playerResp.ID)
			assert.Equal(t, *playerGen.SteamID, *playerResp.SteamID)
			assert.NotNil(t, playerResp.LinkAvatar)
			assert.Equal(t, *playerGen.Rank, *playerResp.Rank)
			assert.Equal(t, *playerGen.Username, *playerResp.Username)
			assert.Equal(t, *playerGen.Kills, *playerResp.Kills)
			assert.Equal(t, *playerGen.Deaths, *playerResp.Deaths)
			assert.Equal(t, *playerGen.Assists, *playerResp.Assists)

			require.Len(t, playerResp.Duels.Keys, 5)
			for _, xV := range playerResp.Duels.Data {
				require.Len(t, xV, 6)
				for _, duels := range playerGen.DuelsPlayer {
					if xV["matchup"].(string) == string((*duels.Matchup)[0]) {
						if *duels.NbrOccurence == 0 {
							assert.Equal(t, "None", xV[string((*duels.Matchup)[2])].(string))
						} else {
							assert.Equal(t,
								helpers.Round(float64(*duels.NbrWin)/float64(*duels.NbrOccurence), 2),
								xV[string((*duels.Matchup)[2])].(float64),
							)
						}
					}
				}
			}
		}

		require.Len(t, teamResp.Duels.Keys, 5)
		for _, xV := range teamResp.Duels.Data {
			require.Len(t, xV, 6)
			for _, duels := range demoGen.DuelsTeam {
				if xV["matchup"].(string) == string((*duels.Matchup)[0]) {
					if *duels.NbrOccurence == 0 {
						assert.Equal(t, "None", xV[string((*duels.Matchup)[2])].(string))
					} else {
						assert.Equal(t,
							helpers.Round(float64(*duels.NbrWin)/float64(*duels.NbrOccurence), 2),
							xV[string((*duels.Matchup)[2])].(float64),
						)
					}
				}
			}
		}
	}

	require.Len(t, demoResponse.MatrixFlashs.Data, 10)
	require.Len(t, demoResponse.MatrixFlashs.Keys, 10)
	require.Len(t, demoResponse.MatrixKills.Data, 10)
	require.Len(t, demoResponse.MatrixKills.Keys, 10)
	for _, team := range demo.TeamsStats {
		for _, player := range team.PlayersStats {
			for _, data := range demoResponse.MatrixFlashs.Data {
				assert.Contains(t, data, *player.Username)
			}
			for _, data := range demoResponse.MatrixKills.Data {
				assert.Contains(t, data, *player.Username)
			}
			assert.Contains(t, demoResponse.MatrixFlashs.Keys, *player.Username)
			assert.Contains(t, demoResponse.MatrixKills.Keys, *player.Username)
		}
	}

	demo = demosGeneratedNotAnalyzed[0]

	resp = sendRequestWithToken(serverTest.Router,
		nil,
		methodeGetDemo,
		strings.Replace(urlGetDemo, ":demoID", strconv.FormatInt(*demo.ID, 10), 1),
		httpCookie)
	require.Equal(t, http.StatusOK, resp.Code)

	require.NoError(t, json.Unmarshal(resp.Body.Bytes(), &response))
	demoResponse = response["data"]
	demoResponse.Date = helpers.TimePtr(demoResponse.Date.In(time.UTC))

	assert.Equal(t, strconv.FormatInt(*demo.ID, 10), *demoResponse.ID)
	assert.Equal(t, *demo.VersionAnalyzer, *demoResponse.VersionAnalyzer)
	assert.Equal(t, *demo.Date, *demoResponse.Date)
	assert.Nil(t, demoResponse.MapName)
	assert.Equal(t, *demo.Name, *demoResponse.Name)
	assert.Equal(t, *demo.Size, *demoResponse.Size)
	assert.Equal(t, model.Undefined, demoResponse.EndState)
	assert.Equal(t, []int{0, 0}, *demoResponse.Score)
	assert.Nil(t, demoResponse.Teams)

	require.Len(t, demoResponse.Rounds, len(demo.Rounds))
	for numRound, round := range demoResponse.Rounds {
		assert.Equal(t, *demo.Rounds[numRound].NumRound, *round.NumRound)
		assert.Equal(t, *demo.Rounds[numRound].SideWin, *round.SideWin)
		require.Len(t, round.Frames, len(demo.Rounds[numRound].Frames))
		require.Len(t, round.ScoreFactsPerRound, len(demo.Rounds[numRound].ScoreFactsPerPlayer))
		for _, score := range round.ScoreFactsPerRound {
			require.NotNil(t, score.SteamID)
		}
		require.Len(t, round.ChatMessage, len(demo.Rounds[numRound].ChatMessage))
		assert.True(t, len(round.ChatMessage) > 0)
		// for numChatMessage, chatMessage := range round.ChatMessage {
		// 	assert.Equal(t, *demo.Rounds[numRound].ChatMessage[numChatMessage].IsChatAll, *chatMessage.IsChatAll)
		// 	assert.Equal(t, *demo.Rounds[numRound].ChatMessage[numChatMessage].Sender, *chatMessage.Sender)
		// 	assert.Equal(t, *demo.Rounds[numRound].ChatMessage[numChatMessage].SideSender, *chatMessage.SideSender)
		// 	assert.Equal(t, *demo.Rounds[numRound].ChatMessage[numChatMessage].Message, *chatMessage.Message)
		// }

		require.Len(t, round.HeatMapKills, len(demo.Rounds[numRound].HeatMapKills))
		for numHeatMapKill, heatMapKill := range round.HeatMapKills {
			td.Cmp(t, heatMapKill, round.HeatMapKills[numHeatMapKill])
		}
		require.Len(t, round.HeatMapDmgs, len(demo.Rounds[numRound].HeatMapDmgs))
		for numHeatMapDmg, heatMapDmg := range round.HeatMapDmgs {
			td.Cmp(t, heatMapDmg, round.HeatMapDmgs[numHeatMapDmg])
		}

		require.Len(t, round.Frames, len(demo.Rounds[numRound].Frames))
		assert.True(t, len(round.Frames) > 0)
		sort.Slice(demo.Rounds[numRound].Frames, func(i, j int) bool {
			return *demo.Rounds[numRound].Frames[i].NumFrame < *demo.Rounds[numRound].Frames[j].NumFrame
		})
		for numFrame, frame := range round.Frames {
			assert.Equal(t,
				*demo.Rounds[numRound].Frames[numFrame].NumFrame,
				*frame.NumFrame,
				fmt.Sprintf("numRound %d numFrame %d", numRound, numFrame),
			)
			assert.Equal(t,
				*demo.Rounds[numRound].Frames[numFrame].PlayerCarrierSteamID,
				*frame.PlayerCarrierSteamID,
				fmt.Sprintf("numRound %d numFrame %d", numRound, numFrame),
			)
			assert.Len(t,
				demo.Rounds[numRound].Frames[numFrame].PlayersCard,
				len(frame.PlayersCard),
				fmt.Sprintf("numRound %d numFrame %d", numRound, numFrame),
			)
			assert.Len(t,
				demo.Rounds[numRound].Frames[numFrame].PlayersPositions,
				len(frame.PlayersPositions),
				fmt.Sprintf("numRound %d numFrame %d", numRound, numFrame),
			)
			assert.Len(t,
				demo.Rounds[numRound].Frames[numFrame].Grenades,
				len(frame.Grenades),
				fmt.Sprintf("numRound %d numFrame %d", numRound, numFrame),
			)
			assert.Equal(t,
				*frame.BombLastPosDownX,
				*demo.Rounds[numRound].Frames[numFrame].BombLastPosDownX,
				fmt.Sprintf("numRound %d numFrame %d", numRound, numFrame),
			)
			assert.Equal(t,
				*frame.BombLastPosDownY,
				*demo.Rounds[numRound].Frames[numFrame].BombLastPosDownY,
				fmt.Sprintf("numRound %d numFrame %d", numRound, numFrame),
			)
			assert.Equal(t,
				*frame.BombLastPosDownZ,
				*demo.Rounds[numRound].Frames[numFrame].BombLastPosDownZ,
				fmt.Sprintf("numRound %d numFrame %d", numRound, numFrame),
			)
			assert.Equal(t,
				*frame.PlayerCarrierSteamID,
				*demo.Rounds[numRound].Frames[numFrame].PlayerCarrierSteamID,
				fmt.Sprintf("numRound %d numFrame %d", numRound, numFrame),
			)
			assert.Equal(t,
				*frame.Timer,
				*demo.Rounds[numRound].Frames[numFrame].Timer,
				fmt.Sprintf("numRound %d numFrame %d", numRound, numFrame),
			)
			assert.Equal(t,
				*frame.BombPlanted,
				*demo.Rounds[numRound].Frames[numFrame].BombPlanted,
				fmt.Sprintf("numRound %d numFrame %d", numRound, numFrame),
			)
			assert.Equal(t,
				frame.Events,
				demo.Rounds[numRound].Frames[numFrame].EventsByte,
				fmt.Sprintf("numRound %d numFrame %d", numRound, numFrame),
			)
		}
	}
}

func TestControllerDemo_GetDemoAnalyzedNOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	httpCookie := performTestAuth(serverTest.Router, *userGenerated)
	require.NotNil(t, httpCookie)

	resp := sendRequestWithToken(serverTest.Router,
		nil,
		methodeGetDemo,
		strings.Replace(urlGetDemo, ":demoID", "6468736436784", 1),
		httpCookie)
	assert.Equal(t, http.StatusNotFound, resp.Code)

	resp = sendRequestWithToken(serverTest.Router,
		nil,
		methodeGetDemo,
		strings.Replace(urlGetDemo, ":demoID", "coucou", 1),
		httpCookie)
	assert.Equal(t, http.StatusBadRequest, resp.Code)
}
