package controller_test

import (
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/controller"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/mock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/server"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/downloaderinfo"
)

const (
	methodeGetAllDemo = http.MethodGet
	urlGetAllDemo     = "/api/v1/demo/user/:steamID"
)

func TestControllerDemo_GetAllMustBeConnected(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	assert.Nil(t, checkMustBeConnected(serverTest.Router, false, methodeGetAllDemo, urlGetAllDemo))
}

func TestControllerDemo_GetAllDemoOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	httpCookie := performTestAuth(serverTest.Router, *userGeneratedVirgin)
	require.NotNil(t, httpCookie)

	resp := sendRequestWithToken(serverTest.Router,
		nil,
		methodeGetAllDemo,
		strings.Replace(urlGetAllDemo, ":steamID", *userGeneratedVirgin.SteamID, 10),
		httpCookie)
	require.Equal(t, http.StatusOK, resp.Code)

	var response map[string]controller.GetAllFromUserReturn

	require.NoError(t, json.Unmarshal(resp.Body.Bytes(), &response))
	demosResponse := response["data"].Demos

	assert.Len(t, demosResponse, 0)

	resp = sendRequestWithToken(serverTest.Router,
		nil,
		methodeGetAllDemo,
		strings.Replace(urlGetAllDemo, ":steamID", *userGenerated.SteamID, 10),
		httpCookie)
	require.Equal(t, http.StatusOK, resp.Code)

	require.NoError(t, json.Unmarshal(resp.Body.Bytes(), &response))
	demosResponse = response["data"].Demos

	require.Len(t, demosResponse, nbrDemosGenerated)

	for numDemo := range demosResponse {
		assert.Equal(t, strconv.FormatInt(*demosGeneratedAnalyzed[numDemo].ID, 10), *demosResponse[numDemo].ID)
		assert.Equal(t, *demosGeneratedAnalyzed[numDemo].Name, *demosResponse[numDemo].Name)
		assert.Equal(t, *demosGeneratedAnalyzed[numDemo].Size, *demosResponse[numDemo].Size)
		assert.Equal(t, *demosGeneratedAnalyzed[numDemo].MapName, *demosResponse[numDemo].MapName)
		assert.Equal(t, demosGeneratedAnalyzed[numDemo].Date.UnixNano(), demosResponse[numDemo].Date.UnixNano())
		assert.Equal(t, *demosGeneratedAnalyzed[numDemo].VersionAnalyzer, *demosResponse[numDemo].VersionAnalyzer)
		assert.Equal(t, *demosGeneratedAnalyzed[numDemo].Downloaded, *demosResponse[numDemo].Downloaded)
		assert.Equal(t, demosGeneratedAnalyzed[numDemo].IsWin(userGenerated.SteamID), demosResponse[numDemo].EndState)

		assert.Equal(t, []int{
			*demosGeneratedAnalyzed[numDemo].TeamsStats[0].Score,
			*demosGeneratedAnalyzed[numDemo].TeamsStats[1].Score,
		},
			*demosResponse[numDemo].Score)
	}
}

func TestControllerDemo_GetAllDemoWithFilters(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	httpCookie := performTestAuth(serverTest.Router, *userGenerated)
	require.NotNil(t, httpCookie)

	filters := controller.FiltersGetDemos{
		Limit: 8,
	}
	filtersBytes, err := json.Marshal(filters)
	require.NoError(t, err)

	resp := sendRequestWithToken(serverTest.Router,
		filtersBytes,
		methodeGetAllDemo,
		strings.Replace(urlGetAllDemo, ":steamID", *userGenerated.SteamID, 10),
		httpCookie)
	require.Equal(t, http.StatusOK, resp.Code)

	var response map[string]controller.GetAllFromUserReturn
	require.NoError(t, json.Unmarshal(resp.Body.Bytes(), &response))
	demosResponse := response["data"]
	require.Len(t, demosResponse.Demos, nbrDemosGenerated)
	require.Equal(t, nbrDemosGenerated, demosResponse.NbrDemosToAnalyze)
}
