package controller

import (
	"net"
	"net/http"
	"regexp"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/downloaderinfo"
)

// UserController controller of the users
type UserController struct {
	LoggerBack      *custlogger.Logger
	di              *downloaderinfo.DownloaderInfo
	analyzerVersion *int64
}

// NewUserController Retourne une instance du controller
func NewUserController(
	log *custlogger.Logger,
	di *downloaderinfo.DownloaderInfo,
	analyzerVersion int64,
) UserController {
	return UserController{
		LoggerBack:      log,
		di:              di,
		analyzerVersion: &analyzerVersion,
	}
}

// DataCalendarReturn global struct for data calendar returned
type DataCalendarReturn struct {
	Day   *string `json:"date"`
	Value *int    `json:"count"`
}

// Register check the credentials, if the mail isn't already took, and add it to the DB
func (uc *UserController) Register(c *gin.Context) {
	db := store.GetDB(c)

	type tmpCredentials struct {
		TmpUser struct {
			Mail     *string `json:"mail" binding:"required"`
			Password *string `json:"password" binding:"required"`
			Confirm  *string `json:"confirm" binding:"required"`
		} `json:"credentials" binding:"required"`
	}

	credentials := &tmpCredentials{}
	if err := c.BindJSON(credentials); err != nil {
		helpers.SendError(c,
			uc.LoggerBack,
			helpers.ErrInvalidInputJSON(),
			helpers.MakeEasyDetail(helpers.PAYLOAD, "user"),
			err,
		)
		return
	}

	if *credentials.TmpUser.Confirm != *credentials.TmpUser.Password {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrBadCredentials(), nil, nil)
		return
	}

	// Hash du mot de passe
	var err error
	if *credentials.TmpUser.Password, err = helpers.HashPassword(*credentials.TmpUser.Password); err != nil {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	*credentials.TmpUser.Mail = strings.ToUpper(*credentials.TmpUser.Mail)

	// On peut se débarasser de la struct preUser
	user := model.User{
		Mail:     credentials.TmpUser.Mail,
		Password: credentials.TmpUser.Password,
	}

	userDao := dao.NewUserDAO(db)
	if exists, err := userDao.Exists(user); err != nil {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	} else if exists {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrDuplicate(), nil, err)
		return
	}

	if err := userDao.Create(&user); err != nil {
		helpers.CheckErrorValidator(c, uc.LoggerBack, err)
		return
	}

	helpers.SendData(c, http.StatusCreated, nil)
}

// GetCurrentUserResp type returned for GetCurrentUser
type GetCurrentUserResp struct {
	ID       *uint    `json:"id"`
	SteamID  *string  `json:"steamID"`
	Mail     *string  `json:"mail"`
	SizeUsed *float64 `json:"size_used"`
}

// GetCurrentUser retourne les informations de l'utilisateur actuellement connecté
func (uc UserController) GetCurrentUser(c *gin.Context) {
	db := store.GetDB(c)

	userID, err := helpers.GetUserIDFromContext(c)
	if err != nil {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	userDao := dao.NewUserDAO(db)
	user := &model.User{
		ID: userID,
	}

	if err := userDao.Get(user); err != nil {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	userResp := GetCurrentUserResp{
		ID:       user.ID,
		Mail:     user.Mail,
		SizeUsed: user.SizeUsed,
		SteamID:  user.SteamID,
	}

	helpers.SendData(c, http.StatusOK, userResp)
}

// CheckTokens check if the tokens provided for downloading demo are ok or not
func (uc UserController) CheckTokens(c *gin.Context) {
	db := store.GetDB(c)

	userID, err := helpers.GetUserIDFromContext(c)
	if err != nil {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	userDao := dao.NewUserDAO(db)

	user := &model.User{
		ID: userID,
	}
	if err := userDao.Get(user); err != nil {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	if user.SteamID == nil || user.AccessTokenHistory == nil || user.ShareCode == nil {
		helpers.SendData(c, http.StatusBadRequest, nil)
		return
	}

	if !uc.di.CheckIfTokenAreValids(*user.SteamID, *user.AccessTokenHistory, *user.ShareCode) {
		helpers.SendData(c, http.StatusBadRequest, nil)
	}

	helpers.SendData(c, http.StatusOK, nil)
}

// ConfigTokenGet type token got from user
type ConfigTokenGet struct {
	IP string `json:"ip" binding:"required"`
}

// CreateToken create a token
func (uc UserController) CreateToken(c *gin.Context) {
	db := store.GetDB(c)

	userID, err := helpers.GetUserIDFromContext(c)
	if err != nil {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	configTokenGet := &ConfigTokenGet{}
	if err := c.BindJSON(configTokenGet); err != nil {
		helpers.SendError(c,
			uc.LoggerBack,
			helpers.ErrInvalidInputJSON(),
			helpers.MakeEasyDetail(helpers.PAYLOAD, "configTokenGet"),
			err,
		)
		return
	}

	if net.ParseIP(configTokenGet.IP) == nil {
		helpers.SendError(c,
			uc.LoggerBack,
			helpers.ErrBadRequest(),
			helpers.MakeEasyDetail(helpers.PAYLOAD, "ip"),
			nil,
		)
		return
	}

	token := &model.TokenUploadDemo{
		UserID:    userID,
		IPAllowed: &configTokenGet.IP,
	}
	token.Generate()
	tokenDAO := dao.NewTokenUploadDemoDAO(db)
	if err := tokenDAO.Create(token); err != nil {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	helpers.SendData(c, http.StatusOK, ConfigTokenResp{
		IP:       *token.IPAllowed,
		Token:    *token.Token,
		LastUsed: token.LastUsed,
	})
}

// ConfigTokenResp type token returned to user
type ConfigTokenResp struct {
	IP       string     `json:"ip"`
	Token    string     `json:"token"`
	LastUsed *time.Time `json:"lastUsed"`
}

// GetTokens returns the tokens of the user
func (uc UserController) GetTokens(c *gin.Context) {
	db := store.GetDB(c)

	userID, err := helpers.GetUserIDFromContext(c)
	if err != nil {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	tokenDAO := dao.NewTokenUploadDemoDAO(db)
	tokens, err := tokenDAO.GetAllFromUser(*userID)
	if err != nil {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	var tokensResp []ConfigTokenResp
	for _, token := range tokens {
		tokensResp = append(tokensResp, ConfigTokenResp{
			IP:       *token.IPAllowed,
			Token:    *token.Token,
			LastUsed: token.LastUsed,
		})
	}

	helpers.SendData(c, http.StatusOK, tokensResp)
}

//PointReturn one point for a dataset
type PointReturn struct {
	X int64   `json:"x"`
	Y float64 `json:"y"`
}

// DatasetsReturn main type returned containing all the datasets
type DatasetsReturn struct {
	Data []PointReturn `json:"data"`
}

// Progression return the progression of the current user
func (uc UserController) Progression(c *gin.Context) {
	db := store.GetDB(c)
	userDao := dao.NewUserDAO(db)

	user, err := userDao.GetUserFromContext(c)
	if err != nil {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	if user.SteamID == nil || *user.SteamID == "" {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrForbidden(), nil, nil)
		return
	}

	demoDao := dao.NewDemoDAO(db, uc.LoggerBack)
	demos, err := demoDao.GetAllAnalizedWithStats(*user.SteamID, nil, *uc.analyzerVersion)
	if err != nil {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	progressionReturn := DatasetsReturn{}

	for _, demo := range demos {
		player := demo.FindPlayer(user.SteamID)
		if player == nil {
			//TODO : remove from owned demo
			continue
		}

		progressionReturn.Data = append(progressionReturn.Data, PointReturn{
			X: demo.Date.Unix(),
			Y: *player.ScoreFacts.FlashScore,
		})
	}

	helpers.SendData(c, http.StatusOK, progressionReturn)
}

// GetDateCalendar return all the date the player played
func (uc UserController) GetDateCalendar(c *gin.Context) {
	db := store.GetDB(c)

	steamID := c.Param("steamID")
	if steamID == "" {
		uc.LoggerBack.Infof("no steamID")
		helpers.SendError(c, uc.LoggerBack, helpers.ErrForbidden(), nil, nil)
		return
	}

	demoDao := dao.NewDemoDAO(db, uc.LoggerBack)
	demos, err := demoDao.GetAllFromUser(steamID, &model.AllSources, nil, nil, nil)
	if err != nil {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	dataCalendar := []DataCalendarReturn{}
	for _, demo := range demos {
		currDay := demo.Date.Format("2006-01-02")

		var found bool
		for _, date := range dataCalendar {
			if *date.Day == currDay {
				*date.Value++
				found = true
				break
			}
		}
		if !found {
			dataCalendar = append(dataCalendar, DataCalendarReturn{
				Day:   &currDay,
				Value: helpers.IntPtr(1),
			})
		}
	}

	helpers.SendData(c, http.StatusOK, dataCalendar)
}

// SetSteamID modifie le steamID de l'utilisateur
func (uc UserController) SetSteamID(c *gin.Context) {
	db := store.GetDB(c)

	userID, err := helpers.GetUserIDFromContext(c)
	if err != nil {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	userDAO := dao.NewUserDAO(db)
	user := &model.User{
		ID: userID,
	}

	type tmpUserSteamID struct {
		UserSteamID string `json:"steamid" binding:"required"`
	}

	bindUserSteamID := &tmpUserSteamID{}

	// check si pas d'erreur au bind
	if err := c.BindJSON(bindUserSteamID); err != nil {
		helpers.SendError(c,
			uc.LoggerBack,
			helpers.ErrInvalidInputJSON(),
			helpers.MakeEasyDetail(helpers.PAYLOAD, "steamID"),
			err,
		)
		return
	}

	user.SteamID = &bindUserSteamID.UserSteamID
	if len(*user.SteamID) != model.LengthSteamID {
		helpers.SendError(c,
			uc.LoggerBack,
			helpers.ErrInvalidInputJSON(),
			helpers.MakeEasyDetail(helpers.PAYLOAD, "steamID"),
			err,
		)
		return
	}

	matched, err := regexp.Match(`^\d{17}$`, []byte(*user.SteamID))
	if err != nil {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	if !matched {
		helpers.SendError(c, uc.LoggerBack,
			helpers.ErrInvalidInputJSON(), helpers.MakeEasyDetail(helpers.PAYLOAD, "steamID"), err)
		return
	}

	if err := userDAO.UpdateSteamID(user); err != nil {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	size, err := userDAO.CountMemoryUsed(*user)
	if err != nil {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	if err := userDAO.SetMemoryUsed(*user, helpers.OToGo(float64(size))); err != nil {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	helpers.SendData(c, http.StatusOK, nil)
}

// SetShareCode modifie lesharecode de l'utilisateur
func (uc UserController) SetShareCode(c *gin.Context) {
	db := store.GetDB(c)

	userID, err := helpers.GetUserIDFromContext(c)
	if err != nil {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	userDAO := dao.NewUserDAO(db)
	user := &model.User{
		ID: userID,
	}

	var UserShareCode struct {
		ShareCode string `json:"shareCode" binding:"required"`
	}

	// check si pas d'erreur au bind
	if err := c.BindJSON(&UserShareCode); err != nil {
		helpers.SendError(c,
			uc.LoggerBack,
			helpers.ErrInvalidInputJSON(),
			helpers.MakeEasyDetail(helpers.PAYLOAD, "shareCode"),
			err,
		)
		return
	}

	user.ShareCode = &UserShareCode.ShareCode

	matched, err := regexp.Match(`^CSGO(-[0-9a-zA-Z]{5}){5}$`, []byte(*user.ShareCode))
	if err != nil {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	if !matched {
		helpers.SendError(c,
			uc.LoggerBack,
			helpers.ErrInvalidInputJSON(),
			helpers.MakeEasyDetail(helpers.PAYLOAD, "shareCode"),
			err,
		)
		return
	}

	if err := userDAO.UpdateShareCode(user); err != nil {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	helpers.SendData(c, http.StatusOK, nil)
}

// SetAccessTokenHistory modifie lesharecode de l'utilisateur
func (uc UserController) SetAccessTokenHistory(c *gin.Context) {
	db := store.GetDB(c)

	userID, err := helpers.GetUserIDFromContext(c)
	if err != nil {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	userDAO := dao.NewUserDAO(db)
	user := &model.User{
		ID: userID,
	}

	var UserAccessTokenHistory struct {
		Token string `json:"token" binding:"required"`
	}

	// check si pas d'erreur au bind
	if err := c.BindJSON(&UserAccessTokenHistory); err != nil {
		helpers.SendError(c,
			uc.LoggerBack,
			helpers.ErrInvalidInputJSON(),
			helpers.MakeEasyDetail(helpers.PAYLOAD, "token"),
			err,
		)
		return
	}

	user.AccessTokenHistory = &UserAccessTokenHistory.Token

	matched, err := regexp.Match(`^[0-9A-Z]{4}-[0-9A-Z]{5}-[0-9A-Z]{4}$`, []byte(*user.AccessTokenHistory))
	if err != nil {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	if !matched {
		helpers.SendError(c,
			uc.LoggerBack,
			helpers.ErrInvalidInputJSON(),
			helpers.MakeEasyDetail(helpers.PAYLOAD, "token"),
			err,
		)
		return
	}

	if err := userDAO.UpdateAccessTokenHistory(user); err != nil {
		helpers.SendError(c, uc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	helpers.SendData(c, http.StatusOK, nil)
}
