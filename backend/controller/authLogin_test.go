package controller_test

import (
	"encoding/json"
	"net/http"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/controller"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/mock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/server"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/downloaderinfo"
)

const (
	methodeLogin = http.MethodPost
	urlLogin     = "/api/v1/user/login"
)

func TestControllerUser_LoginMustBeConnected(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	assert.Nil(t, checkMustBeConnected(serverTest.Router, false, methodeLogin, urlLogin))
}

func TestControllerAuth_LoginOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	userBytes, err := json.Marshal(&tmpCredentialsSend{
		User: tmpUserSend{
			Mail:     userGenerated.Mail,
			Password: userGenerated.Password,
		},
	})
	require.NoError(t, err)
	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}

	resp := sendRequest(serverTest.Router, userBytes, methodeLogin, urlLogin)
	assert.Equal(t, http.StatusOK, resp.Code)

	var response map[string]controller.AuthReturn

	require.NoError(t, json.Unmarshal(resp.Body.Bytes(), &response))
	authResponse := response["data"]
	assert.False(t, *authResponse.APIWorking)
}

func TestControllerAuth_LoginOKAPIOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	userDAO := dao.NewUserDAO(localDB)
	locaUserGenerated := model.GenerateRandomUser(false)
	pass := *locaUserGenerated.Password

	var err error
	*locaUserGenerated.Password, err = helpers.HashPassword(*locaUserGenerated.Password)
	require.NoError(t, err)

	locaUserGenerated.APIValveWorking = helpers.BoolPtr(true)

	require.NoError(t, userDAO.Create(locaUserGenerated))

	userBytes, err := json.Marshal(&tmpCredentialsSend{
		User: tmpUserSend{
			Mail:     locaUserGenerated.Mail,
			Password: &pass,
		},
	})
	require.NoError(t, err)

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}

	resp := sendRequest(serverTest.Router, userBytes, methodeLogin, urlLogin)
	assert.Equal(t, http.StatusOK, resp.Code)

	var response map[string]controller.AuthReturn

	require.NoError(t, json.Unmarshal(resp.Body.Bytes(), &response))
	authResponse := response["data"]
	assert.True(t, *authResponse.APIWorking)
}

func TestControllerAuth_LoginNOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	userBytes, err := json.Marshal(&tmpCredentialsSend{
		User: tmpUserSend{
			Mail:     userGeneratedNotStored.Mail,
			Password: userGeneratedNotStored.Password,
		},
	})
	require.NoError(t, err)

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}

	resp := sendRequest(serverTest.Router, userBytes, methodeLogin, urlLogin)
	assert.Equal(t, http.StatusBadRequest, resp.Code)
}

func TestControllerAuth_LoginBadJSON(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	userBytes, err := json.Marshal(&model.Demo{
		Name: helpers.StrPtr("demotestbadjson"),
	})
	require.NoError(t, err)

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}

	resp := sendRequest(serverTest.Router, userBytes, methodeLogin, urlLogin)
	assert.Equal(t, http.StatusBadRequest, resp.Code)
}

func TestControllerAuth_LoginBadPassword(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	userBytes, err := json.Marshal(&tmpCredentialsSend{
		User: tmpUserSend{
			Mail:     helpers.StrPtr("mail"),
			Password: helpers.StrPtr("pass"),
		},
	})
	require.NoError(t, err)
	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}

	resp := sendRequest(serverTest.Router, userBytes, methodeLogin, urlLogin)
	assert.Equal(t, http.StatusBadRequest, resp.Code)

	userBytes, err = json.Marshal(&tmpCredentialsSend{
		User: tmpUserSend{
			Mail:     userGenerated.Mail,
			Password: helpers.StrPtr("pass"),
		},
	})
	require.NoError(t, err)

	resp = sendRequest(serverTest.Router, userBytes, methodeLogin, urlLogin)
	assert.Equal(t, http.StatusBadRequest, resp.Code)
}
