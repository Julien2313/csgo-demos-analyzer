package controller_test

import (
	"log"
	"math/rand"
	"os"
	"sort"
	"testing"
	"time"

	"github.com/jarcoal/httpmock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/mock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var logTest *custlogger.Logger

var _testDBGlobal *gorm.DB

type tmpUserSend struct {
	Mail     *string `json:"mail"`
	Password *string `json:"password"`
	Confirm  *string `json:"confirm"`
}

type tmpSteamIDSend struct {
	SteamID string `json:"steamid"`
}

type tmpShareCodeSend struct {
	ShareCode string `json:"shareCode"`
}

type tmpAccessTokenHistorySend struct {
	AccessTokenHistory string `json:"token"`
}

type tmpCredentialsSend struct {
	User tmpUserSend `json:"credentials"`
}

var fastLogger logger.Interface

var userBanchmark *model.User
var userGeneratedNotStored *model.User
var userGenerated *model.User
var userGenerated2 *model.User
var userGeneratedVirgin *model.User

var currentVersionAnalyzer = int64(*helpers.RandUIntPSQL())

var demosGenerated []*model.Demo
var demosGeneratedOutAnalyzed []*model.Demo
var demosGeneratedAnalyzed []*model.Demo
var demosGeneratedNotAnalyzed []*model.Demo

const nbrDemosGenerated = 5

func TestMain(m *testing.M) {
	os.Exit(testMainWraper(m))
}

func testMainWraper(m *testing.M) int {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	mock.PlayersSteamID()
	mock.NextMatchSharingCode()

	rand.Seed(time.Now().Unix())

	logTest = &custlogger.Logger{}
	logTest.SetupInstanceLogggerTest()

	_testDBGlobal = store.InitDB(true, store.SchemaTestController, logTest)
	fastLogger = logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			LogLevel: logger.Silent,
		},
	)
	userBanchmark = model.StoreGeneratedUser(_testDBGlobal)
	for numDemo := 0; numDemo < 10; numDemo++ {
		demo := model.GenerateRandomDemoWithStats(userBanchmark, *userBanchmark.SteamID, 10)
		if err := _testDBGlobal.Create(&demo).Error; err != nil {
			panic(err)
		}
	}

	userGeneratedNotStored = model.GenerateRandomUser(false)
	userGenerated = model.StoreGeneratedUser(_testDBGlobal)
	userGeneratedVirgin = model.StoreGeneratedUser(_testDBGlobal)
	userGenerated2 = model.StoreGeneratedUser(_testDBGlobal)
	demoDAO := dao.NewDemoDAO(_testDBGlobal, logTest)

	demo := model.GenerateRandomDemoWithStats(userGenerated2, *userGenerated2.SteamID, 1)
	if err := demoDAO.Create(demo, &fastLogger); err != nil {
		panic(err)
	}
	demo = model.GenerateRandomDemo(userGenerated2, *userGenerated2.SteamID)
	if err := demoDAO.Create(demo, &fastLogger); err != nil {
		panic(err)
	}
	demosGeneratedNotAnalyzed = []*model.Demo{demo}

	for numDemo := 0; numDemo < nbrDemosGenerated; numDemo++ {
		demo = model.GenerateRandomDemoWithStats(userGenerated, *userGenerated.SteamID, 1)
		if err := demoDAO.Create(demo, &fastLogger); err != nil {
			panic(err)
		}

		demosGeneratedOutAnalyzed = append(demosGeneratedOutAnalyzed, demo)

		demo = model.GenerateRandomDemoWithStats(userGenerated, *userGenerated.SteamID, currentVersionAnalyzer)
		sort.Slice(demo.TeamsStats[0].PlayersStats, func(i, j int) bool {
			if *demo.TeamsStats[0].PlayersStats[i].Kills == *demo.TeamsStats[0].PlayersStats[j].Kills {
				return *demo.TeamsStats[0].PlayersStats[i].Assists > *demo.TeamsStats[0].PlayersStats[j].Assists
			}
			return *demo.TeamsStats[0].PlayersStats[i].Kills > *demo.TeamsStats[0].PlayersStats[j].Kills
		})

		sort.Slice(demo.TeamsStats[1].PlayersStats, func(i, j int) bool {
			if *demo.TeamsStats[1].PlayersStats[i].Kills == *demo.TeamsStats[1].PlayersStats[j].Kills {
				return *demo.TeamsStats[1].PlayersStats[i].Assists > *demo.TeamsStats[1].PlayersStats[j].Assists
			}
			return *demo.TeamsStats[1].PlayersStats[i].Kills > *demo.TeamsStats[1].PlayersStats[j].Kills
		})
		for _, round := range demo.Rounds {
			for _, heatmapKill := range round.HeatMapKills {
				if rand.Int()%2 == 0 {
					heatmapKill.SteamIDKiller = userGenerated.SteamID
				} else {
					heatmapKill.SteamIDVictim = userGenerated.SteamID
				}
			}

			for _, heatmapDmg := range round.HeatMapDmgs {
				if rand.Int()%2 == 0 {
					heatmapDmg.SteamIDShooter = userGenerated.SteamID
				} else {
					heatmapDmg.SteamIDVictim = userGenerated.SteamID
				}
			}
		}

		if err := demoDAO.Create(demo, &fastLogger); err != nil {
			panic(err)
		}

		demosGeneratedAnalyzed = append(demosGeneratedAnalyzed, demo)
	}

	sort.Slice(demosGeneratedOutAnalyzed, func(i, j int) bool {
		return demosGeneratedOutAnalyzed[i].Date.Before(*demosGeneratedOutAnalyzed[j].Date)
	})
	sort.Slice(demosGeneratedAnalyzed, func(i, j int) bool {
		return demosGeneratedAnalyzed[i].Date.After(*demosGeneratedAnalyzed[j].Date)
	})

	demosGenerated = append(demosGeneratedAnalyzed, demosGeneratedOutAnalyzed...)
	sort.Slice(demosGenerated, func(i, j int) bool {
		return demosGenerated[i].Date.After(*demosGenerated[j].Date)
	})

	return m.Run()
}
