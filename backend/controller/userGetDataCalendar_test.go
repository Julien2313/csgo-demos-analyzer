package controller_test

import (
	"encoding/json"
	"net/http"
	"strings"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/controller"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/mock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/server"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/downloaderinfo"
)

const (
	methodeDataCalendar = http.MethodGet
	urlDataCalendar     = "/api/v1/user/dateCalendar/:steamID"
)

func TestControllerUser_DataCalendarMustBeConnected(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	assert.NoError(t, checkMustBeConnected(serverTest.Router, false, methodeDataCalendar, urlDataCalendar))
}

func TestControllerUser_DataCalendarOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	httpCookie := performTestAuth(serverTest.Router, *userGenerated)

	resp := sendRequestWithToken(serverTest.Router,
		nil,
		methodeDataCalendar,
		strings.Replace(urlDataCalendar, ":steamID", *userGenerated.SteamID, 10),
		httpCookie)
	require.Equal(t, http.StatusOK, resp.Code)

	var response map[string][]controller.DataCalendarReturn

	require.NoError(t, json.Unmarshal(resp.Body.Bytes(), &response))
	calendar := response["data"]

	var nbrDate int
	for _, date := range calendar {
		nbrDate += *date.Value
	}
	assert.Equal(t, nbrDemosGenerated*2, nbrDate)
}
