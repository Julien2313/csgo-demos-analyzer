package controller_test

import (
	"encoding/json"
	"net/http"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/mock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/server"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/downloaderinfo"
)

const (
	methodeSteamID = http.MethodPost
	urlSteamID     = "/api/v1/user/steamid"
)

func TestControllerUser_SetSteamIDMustBeConnected(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	assert.Nil(t, checkMustBeConnected(serverTest.Router, true, methodeSteamID, urlSteamID))
}

func TestControllerUser_SetSteamIDOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	userDAO := dao.NewUserDAO(localDB)

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	httpCookie := performTestAuth(serverTest.Router, *userGenerated)

	// set steamID to first user
	steamIDBytes, err := json.Marshal(&tmpSteamIDSend{
		SteamID: "58965874589658210",
	})
	require.NoError(t, err)

	resp := sendRequestWithToken(serverTest.Router,
		steamIDBytes,
		methodeSteamID,
		urlSteamID,
		httpCookie)
	require.Equal(t, http.StatusOK, resp.Code)

	// check if steam id has changes for the first
	userFromDB := model.User{Mail: userGenerated.Mail}
	require.NoError(t, userDAO.GetUserByMail(&userFromDB))
	assert.Equal(t, "58965874589658210", *userFromDB.SteamID)

	// and check if it hasn't changed anything for the 2nd one
	userFromDB2 := model.User{Mail: userGenerated2.Mail}
	require.NoError(t, userDAO.GetUserByMail(&userFromDB2))
	assert.Equal(t, userGenerated2.SteamID, userFromDB2.SteamID)
}

func TestControllerUser_SetSteamIDNOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	userDAO := dao.NewUserDAO(localDB)

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	httpCookie := performTestAuth(serverTest.Router, *userGenerated)

	steamIDBytes, err := json.Marshal(&tmpSteamIDSend{
		SteamID: "5896587458658210",
	})
	require.NoError(t, err)

	resp := sendRequestWithToken(serverTest.Router,
		steamIDBytes,
		methodeSteamID,
		urlSteamID,
		httpCookie)
	require.Equal(t, http.StatusBadRequest, resp.Code)

	userFromDB := model.User{Mail: userGenerated.Mail}
	require.NoError(t, userDAO.GetUserByMail(&userFromDB))
	assert.Equal(t, userGenerated.SteamID, userFromDB.SteamID)

	steamIDBytes, err = json.Marshal(&tmpSteamIDSend{
		SteamID: "589658458658210",
	})
	require.NoError(t, err)

	resp = sendRequestWithToken(serverTest.Router,
		steamIDBytes,
		methodeSteamID,
		urlSteamID,
		httpCookie)
	require.Equal(t, http.StatusBadRequest, resp.Code)

	userFromDB = model.User{Mail: userGenerated.Mail}
	require.NoError(t, userDAO.GetUserByMail(&userFromDB))
	assert.Equal(t, userGenerated.SteamID, userFromDB.SteamID)

	steamIDBytes, err = json.Marshal(&tmpSteamIDSend{
		SteamID: "5896545684586585210",
	})
	require.NoError(t, err)

	resp = sendRequestWithToken(serverTest.Router,
		steamIDBytes,
		methodeSteamID,
		urlSteamID,
		httpCookie)
	require.Equal(t, http.StatusBadRequest, resp.Code)

	userFromDB = model.User{Mail: userGenerated.Mail}
	require.NoError(t, userDAO.GetUserByMail(&userFromDB))
	assert.Equal(t, userGenerated.SteamID, userFromDB.SteamID)

	steamIDBytes, err = json.Marshal(&tmpSteamIDSend{
		SteamID: "jpolkejrhbgnkjloz",
	})
	require.NoError(t, err)

	resp = sendRequestWithToken(serverTest.Router,
		steamIDBytes,
		methodeSteamID,
		urlSteamID,
		httpCookie)
	require.Equal(t, http.StatusBadRequest, resp.Code)

	userFromDB = model.User{Mail: userGenerated.Mail}
	require.NoError(t, userDAO.GetUserByMail(&userFromDB))
	assert.Equal(t, userGenerated.SteamID, userFromDB.SteamID)

	resp = sendRequestWithToken(serverTest.Router,
		[]byte("teeeest"),
		methodeSteamID,
		urlSteamID,
		httpCookie)
	require.Equal(t, http.StatusBadRequest, resp.Code)

	userFromDB = model.User{Mail: userGenerated.Mail}
	require.NoError(t, userDAO.GetUserByMail(&userFromDB))
	assert.Equal(t, userGenerated.SteamID, userFromDB.SteamID)
}
