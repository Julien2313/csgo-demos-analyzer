package controller_test

import (
	"encoding/json"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/mock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/server"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/democlassifier"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/downloaderinfo"
)

const (
	methodeUploadDemo = http.MethodPost
	urlUploadDemo     = "/api/v1/demo/upload"
)

func TestControllerDemo_Upload(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{PathStorage: "./"}
	dc := democlassifier.NewDemoClassifier(localDB, nil, nil, nil)

	serverTest.Router = gin.Default()
	require.NoError(t, serverTest.InitializeRoutes(localDB, dc, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false,
	))
	httpCookie := performTestAuth(serverTest.Router, *userGenerated)
	require.NotNil(t, httpCookie)

	params := make(map[string][]byte)
	var err error
	params["lastModified"], err = json.Marshal(time.Now().Add(-time.Hour * 24 * 365).Unix())
	require.NoError(t, err)

	resp, err := sendRequestWithFile(serverTest.Router,
		methodeUploadDemo,
		urlUploadDemo,
		httpCookie,
		params,
		"../../csgo-stock-demo/match730_003429481443976282181_0454742687.dem.bz2",
		"demo",
		"match730_003429481443976282181_0454742687.dem.bz2",
	)

	require.NoError(t, err)
	assert.Equal(t, http.StatusCreated, resp.Code)

	demoDAO := dao.NewDemoDAO(localDB, logTest)
	demos, err := demoDAO.GetAllNotClassified()
	require.NoError(t, err)
	require.Len(t, demos, 1)

	demo := demos[0]
	defer func() {
		require.NoError(t, os.Remove(*demo.Path))
	}()

	assert.Equal(t, model.UnknownGameMode, demo.GameMode)
	assert.Len(t, demo.HashFile, 0)
	assert.Equal(t, model.UserUpload, demo.Source)
	assert.True(t, *demo.Downloaded)
	assert.Equal(t, *userGenerated.ID, *demo.UserID)
	_, err = os.Stat(*demo.Path)
	assert.NoError(t, err)
}
