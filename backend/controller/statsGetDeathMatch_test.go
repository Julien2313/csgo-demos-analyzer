package controller_test

import (
	"encoding/json"
	"math/rand"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/controller"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/mock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/server"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
)

const (
	methodeGetDeathMatchDemo = http.MethodGet
	urlGetDeathMatchDemo     = "/api/v1/stats/deathmatch"
)

// @randomFailOnGitlab
func TestControllerDemo_GetDeathMatchDemoOK(t *testing.T) {
	if os.Getenv("GITLAB") != "" {
		t.Skip()
	}
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	now := time.Now()
	now = time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())

	localUserGenerated := model.StoreGeneratedUser(localDB)
	dmDemos := model.GenerateRandomsDeathMatchDemo(*localUserGenerated.ID, *localUserGenerated.SteamID, now, 10)
	dmDemos = append(dmDemos,
		model.GenerateRandomsDeathMatchDemo(
			*localUserGenerated.ID,
			*localUserGenerated.SteamID,
			now.AddDate(0, 0, -1), 10,
		)...)
	dmDemos = append(dmDemos,
		model.GenerateRandomsDeathMatchDemo(
			*localUserGenerated.ID,
			*localUserGenerated.SteamID,
			now.AddDate(0, 0, -2), 10,
		)...)
	dmDemos = append(dmDemos,
		model.GenerateRandomsDeathMatchDemo(
			*localUserGenerated.ID,
			*localUserGenerated.SteamID,
			now.AddDate(0, 0, -3), 10,
		)...)
	require.NoError(t, localDB.Create(dmDemos).Error)

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	serverTest.Router = gin.Default()
	require.NoError(t, serverTest.InitializeRoutes(localDB, nil, nil,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false),
	)
	httpCookie := performTestAuth(serverTest.Router, *localUserGenerated)
	require.NotNil(t, httpCookie)

	resp := sendRequestWithToken(serverTest.Router,
		nil,
		methodeGetDeathMatchDemo,
		urlGetDeathMatchDemo,
		httpCookie)
	require.Equal(t, http.StatusOK, resp.Code)

	var response map[string]controller.WeaponsStatsDemosReturn
	require.NoError(t, json.Unmarshal(resp.Body.Bytes(), &response))
	stats := response["data"]
	require.Len(t, stats.WeaponStatsPerDay.FirstBulletAccuracy.Data, 4)
	require.Len(t, stats.WeaponStatsPerDay.FirstBulletHS.Data, 4)
	require.Len(t, stats.WeaponStatsPerDay.BulletAccuracy.Data, 4)
	require.Len(t, stats.WeaponStatsPerDay.HSPercentage.Data, 4)
	require.Len(t, stats.WeaponStatsPerDay.NbrBulletsFired.Data, 4)
	require.Len(t, stats.WeaponStatsPerDay.AverageVelocityAttacker.Data, 4)
	for numDay := 0; numDay < 4; numDay++ {
		assert.True(t, stats.WeaponStatsPerDay.FirstBulletAccuracy.Data[numDay].Y < 100.0)
		assert.True(t, stats.WeaponStatsPerDay.FirstBulletAccuracy.Data[numDay].Y >= 0.0)
		assert.True(t, stats.WeaponStatsPerDay.BulletAccuracy.Data[numDay].Y < 100.0)
		assert.True(t, stats.WeaponStatsPerDay.BulletAccuracy.Data[numDay].Y >= 0.0)
		assert.True(t, stats.WeaponStatsPerDay.HSPercentage.Data[numDay].Y < 100.0)
		assert.True(t, stats.WeaponStatsPerDay.HSPercentage.Data[numDay].Y >= 0.0)
		assert.True(t, stats.WeaponStatsPerDay.FirstBulletHS.Data[numDay].Y < 100.0)
		assert.True(t, stats.WeaponStatsPerDay.FirstBulletHS.Data[numDay].Y >= 0.0)
		assert.True(t, stats.WeaponStatsPerDay.AverageVelocityAttacker.Data[numDay].Y < 250.0)
		assert.True(t, stats.WeaponStatsPerDay.AverageVelocityAttacker.Data[numDay].Y > 0.0)
	}
	for _, weaponStats := range stats.WeaponTypeStatsPerDay {
		for _, weaponStatsDay := range weaponStats.FirstBulletAccuracy.Data {
			assert.True(t, weaponStatsDay.Y >= 0.0)
			assert.True(t, weaponStatsDay.Y < 100.0)
		}
		for _, weaponStatsDay := range weaponStats.BulletAccuracy.Data {
			assert.True(t, weaponStatsDay.Y >= 0.0)
			assert.True(t, weaponStatsDay.Y < 100.0)
		}
		for _, weaponStatsDay := range weaponStats.HSPercentage.Data {
			assert.True(t, weaponStatsDay.Y >= 0.0)
			assert.True(t, weaponStatsDay.Y < 100.0)
		}
		for _, weaponStatsDay := range weaponStats.FirstBulletHS.Data {
			assert.True(t, weaponStatsDay.Y >= 0.0, weaponStatsDay.Y)
			assert.True(t, weaponStatsDay.Y < 100.0)
		}
		for _, weaponStatsDay := range weaponStats.AverageVelocityAttacker.Data {
			assert.True(t, weaponStatsDay.Y >= 0.0)
			assert.True(t, weaponStatsDay.Y < 250.0)
		}
	}
}

func TestControllerDemo_GetDeathMatchDemoDetails1(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	now := time.Now()
	now = time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())

	nbrBulletFired := rand.Intn(100)
	nbrBulletHit := rand.Intn(nbrBulletFired + 1)
	nbrBulletHS := rand.Intn(nbrBulletHit + 1)

	nbrFirstBulletFired := rand.Intn(nbrBulletFired + 1)
	nbrFirstBulletHit := rand.Intn(nbrFirstBulletFired + 1)
	nbrFirstBulletHS := rand.Intn(nbrFirstBulletHit + 1)
	dmDemo := model.DeathMatchDemo{
		ID:              helpers.Int64Ptr(rand.Int63()),
		HashFile:        []byte(helpers.RandomString(16)),
		UserID:          userGenerated.ID,
		VersionAnalyzer: helpers.Int64Ptr(rand.Int63()),
		DeathMatchPlayers: []*model.DeathMatchPlayer{{
			SteamID:             userGenerated.SteamID,
			FirstBulletAccuracy: helpers.FloatPtr(rand.Float64() * 100),
			FirstBulletHS:       helpers.FloatPtr(rand.Float64() * 100),
			BulletAccuracy:      helpers.FloatPtr(rand.Float64() * 100),
			HSPercentage:        helpers.FloatPtr(rand.Float64() * 100),

			WeaponsStats: nil,

			NbrBulletFired: &nbrBulletFired,
			NbrBulletHit:   &nbrBulletHit,
			NbrBulletHS:    &nbrBulletHS,

			NbrFirstBulletFired: &nbrFirstBulletFired,
			NbrFirstBulletHit:   &nbrFirstBulletHit,
			NbrFirstBulletHS:    &nbrFirstBulletHS,

			AverageVelocityAttacker: helpers.FloatPtr(rand.Float64() * 250),
		}},
		Date: &now,
		Path: helpers.StrPtr(helpers.RandomString(20)),
		Name: helpers.StrPtr(helpers.RandomString(20)),
	}
	require.NoError(t, localDB.Create(&dmDemo).Error)

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	serverTest.Router = gin.Default()
	require.NoError(t, serverTest.InitializeRoutes(localDB, nil, nil,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false),
	)
	httpCookie := performTestAuth(serverTest.Router, *userGenerated)
	require.NotNil(t, httpCookie)

	resp := sendRequestWithToken(serverTest.Router,
		nil,
		methodeGetDeathMatchDemo,
		urlGetDeathMatchDemo,
		httpCookie)
	require.Equal(t, http.StatusOK, resp.Code)

	var response map[string]controller.WeaponsStatsDemosReturn
	require.NoError(t, json.Unmarshal(resp.Body.Bytes(), &response))
}

func TestControllerDemo_GetDeathMatchDemoDetails2(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	now := time.Now()
	now = time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())

	nbrBulletFired := rand.Intn(1000) + 100
	nbrBulletHit := rand.Intn(nbrBulletFired + 1)
	nbrBulletHS := rand.Intn(nbrBulletHit + 1)
	nbrFirstBulletFired := rand.Intn(nbrBulletFired + 1)
	nbrFirstBulletHit := rand.Intn(nbrFirstBulletFired + 1)
	nbrFirstBulletHS := rand.Intn(nbrFirstBulletHit + 1)

	dmDemo := model.DeathMatchDemo{
		ID:              helpers.Int64Ptr(rand.Int63()),
		HashFile:        []byte(helpers.RandomString(16)),
		UserID:          userGenerated.ID,
		VersionAnalyzer: helpers.Int64Ptr(rand.Int63()),
		DeathMatchPlayers: []*model.DeathMatchPlayer{{
			SteamID:             userGenerated.SteamID,
			FirstBulletAccuracy: helpers.FloatPtr(rand.Float64() * 100),
			FirstBulletHS:       helpers.FloatPtr(rand.Float64() * 100),
			BulletAccuracy:      helpers.FloatPtr(rand.Float64() * 100),
			HSPercentage:        helpers.FloatPtr(rand.Float64() * 100),

			WeaponsStats: model.GenerateRandomWeaponsStats(15),

			NbrBulletFired: &nbrBulletFired,
			NbrBulletHit:   &nbrBulletHit,
			NbrBulletHS:    &nbrBulletHS,

			NbrFirstBulletFired: &nbrFirstBulletFired,
			NbrFirstBulletHit:   &nbrFirstBulletHit,
			NbrFirstBulletHS:    &nbrFirstBulletHS,

			AverageVelocityAttacker: helpers.FloatPtr(rand.Float64() * 250),
		}},
		Date: &now,
		Path: helpers.StrPtr(helpers.RandomString(20)),
		Name: helpers.StrPtr(helpers.RandomString(20)),
	}
	require.NoError(t, localDB.Create(&dmDemo).Error)

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	serverTest.Router = gin.Default()
	require.NoError(t, serverTest.InitializeRoutes(localDB, nil, nil,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false),
	)
	httpCookie := performTestAuth(serverTest.Router, *userGenerated)
	require.NotNil(t, httpCookie)

	resp := sendRequestWithToken(serverTest.Router,
		nil,
		methodeGetDeathMatchDemo,
		urlGetDeathMatchDemo,
		httpCookie)
	require.Equal(t, http.StatusOK, resp.Code)
}
