package controller

import (
	"bufio"
	"errors"
	"fmt"
	"io/ioutil"
	"math"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzerdeathmatch"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/democlassifier"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/downloaderdem"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/downloaderinfo"
	"gorm.io/gorm"
)

// ErrBadFileName bad filename
var ErrBadFileName = errors.New("bad filename")

// ErrBadGameConVars bad game con vars
var ErrBadGameConVars = errors.New("bad game con vars")

// ErrNoTokenSent no token sent
var ErrNoTokenSent = errors.New("no token sent")

// DemoController controller of the demos
type DemoController struct {
	LoggerBack      *custlogger.Logger
	dc              *democlassifier.DemoClassifier
	di              *downloaderinfo.DownloaderInfo
	adm             *analyzerdeathmatch.AnalyzerDeathMatch
	a               *analyzer.Analyzer
	analyzerVersion *int64
}

// NewDemoController Retourne une instance du controller
func NewDemoController(
	logBack *custlogger.Logger,
	dc *democlassifier.DemoClassifier,
	di *downloaderinfo.DownloaderInfo,
	adm *analyzerdeathmatch.AnalyzerDeathMatch,
	a *analyzer.Analyzer,
	analyzerVersion int64,
) DemoController {
	return DemoController{
		LoggerBack:      logBack,
		dc:              dc,
		di:              di,
		adm:             adm,
		a:               a,
		analyzerVersion: &analyzerVersion,
	}
}

// DemoReturn global struct for demo returned
type DemoReturn struct {
	ID              *string        `json:"id"`
	Downloaded      *bool          `json:"downloaded"`
	VersionAnalyzer *int64         `json:"versionAnalyzer"`
	Date            *time.Time     `json:"date"`
	MapName         *string        `json:"mapName"`
	Name            *string        `json:"name"`
	Size            *int64         `json:"size"`
	Score           *[]int         `json:"score"`
	EndState        model.EndState `json:"endState"`
	Teams           []*TeamReturn  `json:"teams"`
	Rounds          []*RoundReturn `json:"rounds"`
	BestMarks       *MarksReturn   `json:"bestMarks"`
	BaddestMarks    *MarksReturn   `json:"baddestMarks"`

	MatrixFlashs *HeatMapReturn `json:"matrixFlashs"`
	MatrixKills  *HeatMapReturn `json:"matrixKills"`

	AverageRank *int `json:"averageRank"`
}

func copyTDRFrom(demo model.Demo) *DemoReturn {
	d := &DemoReturn{
		ID:              helpers.StrPtr(strconv.FormatInt(*demo.ID, 10)),
		VersionAnalyzer: demo.VersionAnalyzer,
		Downloaded:      demo.Downloaded,
		Date:            demo.Date,
		MapName:         demo.MapName,
		Name:            demo.Name,
		Size:            demo.Size,
		Rounds:          copyTRRFrom(demo.Rounds),
	}

	var (
		cumulativeRank    int
		nbrPlayerWithTank int
	)

	for _, team := range demo.TeamsStats {
		for _, player := range team.PlayersStats {
			if player.Rank != nil {
				cumulativeRank += *player.Rank
				nbrPlayerWithTank++
			}
		}
	}

	if nbrPlayerWithTank > 0 {
		d.AverageRank = helpers.IntPtr(int(math.Round(float64(cumulativeRank) / float64(nbrPlayerWithTank))))
	}

	d.MatrixFlashs = &HeatMapReturn{}
	d.MatrixKills = &HeatMapReturn{}

	return d
}

// RoundReturn global struct for round returned
type RoundReturn struct {
	NumRound           *int                   `json:"numRound"`
	Frames             []*FrameReturn         `json:"frames"`
	ChatMessage        []*ChatMessageReturn   `json:"chatMessages"`
	SideWin            *int                   `json:"sideWin"`
	ScoreFactsPerRound []*ScoreFactsReturn    `json:"scoreFactsPerRound"`
	MarksPerRound      []*MarksPerRoundReturn `json:"marksPerRound"`

	HeatMapKills []*HeatMapKillReturn `json:"heatMapKills"`
	HeatMapDmgs  []*HeatMapDmgReturn  `json:"heatMapDmgs"`
}

func copyTRRFrom(rounds []*model.Round) []*RoundReturn {
	roundsReturn := []*RoundReturn{}

	for _, round := range rounds {
		var score []*ScoreFactsReturn
		for _, modelScore := range round.ScoreFactsPerPlayer {
			score = append(score, copyTSRFrom(modelScore))
		}
		var marksPerRounds []*MarksPerRoundReturn
		for _, marksPerRound := range round.MarksPerRound {
			marksPerRounds = append(marksPerRounds, copyTMPRRFrom(marksPerRound))
		}

		roundReturn := RoundReturn{
			NumRound:           round.NumRound,
			ChatMessage:        copyTCMRFrom(round.ChatMessage),
			SideWin:            round.SideWin,
			ScoreFactsPerRound: score,
			MarksPerRound:      marksPerRounds,
			Frames:             copyTFRFrom(round.Frames),
			HeatMapKills:       copyTHMKRFrom(round.HeatMapKills),
			HeatMapDmgs:        copyTHMDRFrom(round.HeatMapDmgs),
		}
		roundsReturn = append(roundsReturn, &roundReturn)
	}

	return roundsReturn
}

// ChatMessageReturn global struct for ChatMessage returned
type ChatMessageReturn struct {
	Sender     *string `json:"sender"`
	SideSender *int    `json:"sideSender"`
	Message    *string `json:"message"`
	IsChatAll  *bool   `json:"ischatAll"`
}

func copyTCMRFrom(chatMessages []*model.ChatMessage) []*ChatMessageReturn {
	chatMessagesReturn := []*ChatMessageReturn{}

	for _, chatMessage := range chatMessages {
		chatMessageReturn := ChatMessageReturn{
			Sender:     chatMessage.Sender,
			SideSender: chatMessage.SideSender,
			Message:    chatMessage.Message,
			IsChatAll:  chatMessage.IsChatAll,
		}
		chatMessagesReturn = append(chatMessagesReturn, &chatMessageReturn)
	}

	return chatMessagesReturn
}

// FrameReturn global struct for frame returned
type FrameReturn struct {
	NumFrame *int `json:"numFrame"`

	PlayersCard          []*PlayerCardReturn     `json:"playersCard"`
	PlayersPositions     []*PlayerPositionReturn `json:"playersPositions"`
	Grenades             []*GrenadeReturn        `json:"grenades"`
	BombLastPosDownX     *float64                `json:"bombLastPosDownX"`
	BombLastPosDownY     *float64                `json:"bombLastPosDownY"`
	BombLastPosDownZ     *float64                `json:"bombLastPosDownZ"`
	PlayerCarrierSteamID *string                 `json:"playerCarrierSteamID"`
	Timer                *string                 `json:"timer"`
	BombPlanted          *bool                   `json:"bombPlanted"`
	Events               []byte                  `json:"events"`
}

func copyTFRFrom(frames []*model.Frame) []*FrameReturn {
	framesReturn := []*FrameReturn{}

	for _, frame := range frames {
		frameReturn := FrameReturn{
			NumFrame:             frame.NumFrame,
			PlayersCard:          copyTPCRFrom(frame.PlayersCard),
			PlayersPositions:     copyTPPRFrom(frame.PlayersPositions),
			Grenades:             copyTGRFrom(frame.Grenades),
			BombLastPosDownX:     frame.BombLastPosDownX,
			BombLastPosDownY:     frame.BombLastPosDownY,
			BombLastPosDownZ:     frame.BombLastPosDownZ,
			PlayerCarrierSteamID: frame.PlayerCarrierSteamID,
			Timer:                frame.Timer,
			BombPlanted:          frame.BombPlanted,
			Events:               frame.EventsByte,
		}
		framesReturn = append(framesReturn, &frameReturn)
	}

	sort.Slice(framesReturn, func(i, j int) bool {
		return *framesReturn[i].NumFrame < *framesReturn[j].NumFrame
	})
	return framesReturn
}

// PlayerCardReturn global struct for playerCard returned
type PlayerCardReturn struct {
	IsAlive          *bool    `json:"isAlive"`
	IsConnected      *bool    `json:"isConnected"`
	IsControllingBot *bool    `json:"isControllingBot"`
	Side             *int     `json:"side"`
	PrimaryWeapon    *string  `json:"primaryWeapon"`
	Pistol           *string  `json:"pistol"`
	Grenades         []string `json:"grenades"`
	HasC4            *bool    `json:"hasC4"`
	PlayerName       *string  `json:"playerName"`
	SteamID          *string  `json:"steamID"`
	Health           *int     `json:"health"`
	Armor            *int     `json:"armor"`
	HasHelmet        *bool    `json:"hasHelmet"`
	HasDefuseKit     *bool    `json:"hasDefuseKit"`
	Money            *int     `json:"money"`
}

func copyTPCRFrom(playersCard []*model.PlayerCard) []*PlayerCardReturn {
	playersCardReturn := []*PlayerCardReturn{}

	for _, playerCard := range playersCard {
		playerCardReturn := PlayerCardReturn{
			IsAlive:          &playerCard.IsAlive,
			IsConnected:      &playerCard.IsConnected,
			IsControllingBot: &playerCard.IsControllingBot,
			Side:             &playerCard.Side,
			PrimaryWeapon:    &playerCard.PrimaryWeapon,
			Pistol:           &playerCard.Pistol,
			Grenades:         strings.Split(playerCard.Grenades, ";"),
			HasC4:            &playerCard.HasC4,
			PlayerName:       &playerCard.PlayerName,
			SteamID:          &playerCard.SteamID,
			Health:           &playerCard.Health,
			Armor:            &playerCard.Armor,
			HasHelmet:        &playerCard.HasHelmet,
			HasDefuseKit:     &playerCard.HasDefuseKit,
			Money:            &playerCard.Money,
		}
		playersCardReturn = append(playersCardReturn, &playerCardReturn)
	}

	return playersCardReturn
}

// PlayerPositionReturn global struct for playerPosition returned
type PlayerPositionReturn struct {
	IsAlive        *bool    `json:"isAlive"`
	NbrShoots      *int     `json:"nbrShoots"`
	IsDefusing     *bool    `json:"isDefusing"`
	IsPlanting     *bool    `json:"isPlanting"`
	Side           *int     `json:"side"`
	SteamID        *string  `json:"steamID"`
	PositionX      *float64 `json:"positionX"`
	PositionY      *float64 `json:"positionY"`
	PositionZ      *float64 `json:"positionZ"`
	ViewDirectionX *float64 `json:"viewDirectionX"`
}

func copyTPPRFrom(playersPositions []*model.PlayerPositions) []*PlayerPositionReturn {
	playersPositionReturn := []*PlayerPositionReturn{}

	for _, playerPositions := range playersPositions {
		playerPositionReturn := PlayerPositionReturn{
			IsAlive:        &playerPositions.IsAlive,
			NbrShoots:      &playerPositions.NbrShoots,
			IsDefusing:     &playerPositions.IsDefusing,
			IsPlanting:     &playerPositions.IsPlanting,
			Side:           &playerPositions.Side,
			SteamID:        &playerPositions.SteamID,
			PositionX:      &playerPositions.PositionX,
			PositionY:      &playerPositions.PositionY,
			PositionZ:      &playerPositions.PositionZ,
			ViewDirectionX: &playerPositions.ViewDirectionX,
		}
		playersPositionReturn = append(playersPositionReturn, &playerPositionReturn)
	}

	return playersPositionReturn
}

// GrenadeReturn  global struct for GrenadeReturn returned
type GrenadeReturn struct {
	ID          *string    `json:"id"`
	State       *int       `json:"state"`
	PositionX   *float64   `json:"positionX"`
	PositionY   *float64   `json:"positionY"`
	PositionZ   *float64   `json:"positionZ"`
	Fire        *[]float64 `json:"fire"`
	GrenadeType *int       `json:"grenadeType"`
}

func copyTGRFrom(grenades []*model.Grenade) []*GrenadeReturn {
	grenadesReturn := []*GrenadeReturn{}

	for _, grenade := range grenades {
		grenadeReturn := GrenadeReturn{
			ID:          helpers.StrPtr(strconv.FormatInt(grenade.ID, 10)),
			State:       helpers.IntPtr(int(grenade.State)),
			PositionX:   &grenade.PositionX,
			PositionY:   &grenade.PositionY,
			PositionZ:   &grenade.PositionZ,
			GrenadeType: helpers.IntPtr(int(grenade.GrenadeType)),
			Fire:        &grenade.Fire,
		}
		grenadesReturn = append(grenadesReturn, &grenadeReturn)
	}

	return grenadesReturn
}

// FiltersGetDemos filters for the resume of the demos
type FiltersGetDemos struct {
	Limit    int                 `form:"limit"`
	Offset   int                 `form:"offset"`
	Sources  *[]model.DemoSource `form:"sources[]"`
	Maps     *[]string           `form:"maps[]"`
	Won      *bool               `form:"won"`
	DateFrom *int64              `form:"selectedDateStart"`
	DateTo   *int64              `form:"selectedDateEnd"`
}

// GetAllFromUserReturn struct returned when calling GetAllFromUser
type GetAllFromUserReturn struct {
	Demos             []DemoReturn `json:"demos"`
	NbrDemosToAnalyze int          `json:"nbrDemosToAnalyze"`
}

// GetAllFromUser return all the demo of the user
func (dc DemoController) GetAllFromUser(c *gin.Context) {
	db := store.GetDB(c)

	steamID := c.Param("steamID")
	if steamID == "" {
		dc.LoggerBack.Infof("no steamID")
		helpers.SendError(c, dc.LoggerBack, helpers.ErrForbidden(), nil, nil)
		return
	}

	filters := &FiltersGetDemos{}
	if err := c.ShouldBindQuery(filters); err != nil {
		helpers.SendError(c, dc.LoggerBack, helpers.ErrInvalidInputURL(), nil, err)
		return
	}

	if filters.Limit > 50 {
		filters.Limit = 50
	}

	demoDao := dao.NewDemoDAO(db, dc.LoggerBack)
	demos, err := demoDao.GetResumeFromFilters(
		steamID,
		filters.Sources,
		filters.Limit,
		filters.Offset,
		filters.Maps,
		filters.DateFrom,
		filters.DateTo,
		*dc.analyzerVersion,
	)
	if err != nil {
		helpers.SendError(c, dc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	demosReturn := []DemoReturn{}
	for _, demo := range demos {
		if demo.VersionAnalyzer == nil || *demo.VersionAnalyzer < *dc.analyzerVersion {
			continue
		}

		demoTmp := copyTDRFrom(demo)

		if err := demoDao.GetWithScore(&demo, steamID); err != nil {
			helpers.SendError(c, dc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
			return
		}

		demoTmp.Score = &[]int{*demo.TeamsStats[0].Score, *demo.TeamsStats[1].Score}
		demoTmp.EndState = demo.IsWin(&steamID)

		demosReturn = append(demosReturn, *demoTmp)
	}

	nbrDemosToAnalyze, err := demoDao.GetNbr2BeAnalyzedFromPlayer(steamID, *dc.analyzerVersion)
	if err != nil {
		helpers.SendError(c, dc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	getAllFromUserReturn := GetAllFromUserReturn{
		Demos:             demosReturn,
		NbrDemosToAnalyze: int(nbrDemosToAnalyze),
	}

	helpers.SendData(c, http.StatusOK, getAllFromUserReturn)
}

// GetLastAnalyzedReturn struct returned when calling GetLastAnalyzed
type GetLastAnalyzedReturn struct {
	Demos []DemoReturn `json:"demos"`
}

// GetLastAnalyzed return all the last demos analyzed
func (dc DemoController) GetLastAnalyzed(c *gin.Context) {
	db := store.GetDB(c)

	demoDao := dao.NewDemoDAO(db, dc.LoggerBack)
	demos, err := demoDao.GetLastAnalyzed(*dc.analyzerVersion)
	if err != nil {
		helpers.SendError(c, dc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	demosReturn := []DemoReturn{}
	for _, demo := range demos {
		demoTmp := copyTDRFrom(demo)
		demoTmp.Score = &[]int{*demo.TeamsStats[0].Score, *demo.TeamsStats[1].Score}
		demosReturn = append(demosReturn, *demoTmp)
	}

	getLastAnalyzedReturn := GetLastAnalyzedReturn{
		Demos: demosReturn,
	}

	helpers.SendData(c, http.StatusOK, getLastAnalyzedReturn)
}

// CheckNewDemo return all the demo of the user
func (dc DemoController) CheckNewDemo(c *gin.Context) {
	db := store.GetDB(c)
	userDao := dao.NewUserDAO(db)

	user, err := userDao.GetUserFromContext(c)
	if err != nil {
		helpers.SendError(c, dc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	if user.AccessTokenHistory == nil || *user.AccessTokenHistory == "" ||
		user.ShareCode == nil || *user.ShareCode == "" ||
		user.SteamID == nil || *user.SteamID == "" {
		helpers.SendError(c, dc.LoggerBack, helpers.ErrForbidden(), nil, nil)
		return
	}

	if !dc.di.CheckIfTokenAreValids(*user.SteamID, *user.AccessTokenHistory, *user.ShareCode) {
		helpers.SendError(c, dc.LoggerBack, helpers.ErrDataMissing(), nil, nil)
		return
	}

	isNext, nextShareCode := dc.di.CheckIfNewDemo(*user)
	if !isNext {
		demoDao := dao.NewDemoDAO(db, dc.LoggerBack)
		demo, err := demoDao.GetFromShareCode(nextShareCode)
		if err != nil && err != gorm.ErrRecordNotFound {
			helpers.SendError(c, dc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
			return
		}
		if demo.ID != nil {
			isNext = true
		}
	}

	helpers.SendData(c, http.StatusOK, isNext)
}

// UploadURLGet struct to get url from payload
type UploadURLGet struct {
	URL *string `json:"url" binding:"required"`
}

// UploadByURL enable user to upload dedmo by url (from faceit/esea in example)
func (dc DemoController) UploadByURL(c *gin.Context) {
	db := store.GetDB(c)
	userDao := dao.NewUserDAO(db)

	user, err := userDao.GetUserFromContext(c)
	if err != nil {
		helpers.SendError(c, dc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	uploadURLGet := &UploadURLGet{}
	if err := c.BindJSON(uploadURLGet); err != nil {
		helpers.SendError(c,
			dc.LoggerBack,
			helpers.ErrInvalidInputJSON(),
			helpers.MakeEasyDetail(helpers.PAYLOAD, "uploadURLGet"),
			err,
		)
		return
	}

	reDirectLinkFaceit := regexp.MustCompile(`.*\.faceit-cdn.net/csgo/.*dem\.gz$`)
	reLinkFaceit := regexp.MustCompile(`faceit.com/.*/csgo/room/.*$`)

	var directURL string

	switch {
	case reDirectLinkFaceit.MatchString(*uploadURLGet.URL):
		directURL = *uploadURLGet.URL
		fmt.Println("yeeeeeeeeeeeeeees reDirectLinkFaceit")
	case reLinkFaceit.MatchString(*uploadURLGet.URL):
		fmt.Println("yeeeeeeeeeeeeeees reLinkFaceit")
		return
	default:
		helpers.SendError(c, dc.LoggerBack, helpers.ErrInvalidInputURL(), nil, nil)
		return
	}

	name := fmt.Sprintf("match730_%s", filepath.Base(directURL))
	path := fmt.Sprintf("%s/%s", dc.di.PathStorage, name)
	fmt.Println(name, directURL)
	demo := model.Demo{
		ID:              helpers.Int64Ptr(rand.Int63()),
		Date:            helpers.TimePtr(time.Now()),
		UserID:          user.ID,
		ReplayURL:       &directURL,
		Size:            helpers.Int64Ptr(0),
		Downloaded:      helpers.BoolPtr(false),
		VersionAnalyzer: helpers.Int64Ptr(0),
		Path:            &path,
		Name:            &name,
		Source:          model.UserUpload,
	}

	demoDAO := dao.NewDemoDAO(db, dc.LoggerBack)
	if err := demoDAO.Create(&demo, nil); err != nil {
		helpers.SendError(c, dc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	if err := downloaderdem.DownloadDemo(db, dc.LoggerBack, demo); err != nil {
		helpers.SendError(c, dc.LoggerBack, helpers.ErrInvalidInputURL(), nil, nil)
		return
	}

	if err := dc.a.AddDataAnalyzer([]int64{*demo.ID}); err != nil {
		helpers.SendError(c, dc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	helpers.SendData(c, http.StatusOK, nil)
}

// UploadPost data about the demo uploaded
type UploadPost struct {
	LastModified int64 `form:"lastModified" binding:"required"`
}

// Upload upload a demo
func (dc DemoController) Upload(c *gin.Context) {
	db := store.GetDB(c)
	// user must send the right token or must be connected !
	userID, err := checkToken(c, db)
	if err != nil {
		userDao := dao.NewUserDAO(db)
		user, err := userDao.GetUserFromContext(c)
		if err != nil {
			helpers.SendError(c, dc.LoggerBack, helpers.ErrForbidden(), nil, err)
			return
		}
		userID = *user.ID
	}

	demoFormFile, err := c.FormFile("demo")
	if err != nil {
		helpers.SendError(c, dc.LoggerBack, helpers.ErrInvalidInput(), nil, err)
		return
	}

	if !strings.Contains(demoFormFile.Filename, ".dem") {
		helpers.SendError(c, dc.LoggerBack, helpers.ErrInvalidInput(), nil, err)
		return
	}

	var dateDemo time.Time
	uploadPost := &UploadPost{}
	if err := c.ShouldBindQuery(uploadPost); err != nil {
		dateDemo, err = parseDate(demoFormFile.Filename)
		if err != nil {
			helpers.SendError(c, dc.LoggerBack, helpers.ErrInvalidInput(), nil, err)
			return
		}
	} else {
		dateDemo = time.Unix(0, uploadPost.LastModified*int64(time.Millisecond))
	}

	demoID := rand.Int63()
	filenameSplitted := strings.Split(demoFormFile.Filename, ".dem")
	name := fmt.Sprintf("match730_%d.dem%s", demoID, filenameSplitted[len(filenameSplitted)-1])
	path := fmt.Sprintf("%s/%s", dc.di.PathStorage, name)
	demo := model.Demo{
		ID:              helpers.Int64Ptr(demoID),
		UserID:          &userID,
		Date:            &dateDemo,
		Path:            &path,
		Name:            &name,
		VersionAnalyzer: helpers.Int64Ptr(0),
		Size:            helpers.Int64Ptr(0),
		Downloaded:      helpers.BoolPtr(true),
		Source:          model.UserUpload,
		GameMode:        model.UnknownGameMode,
	}

	demoDao := dao.NewDemoDAO(db, dc.LoggerBack)
	if exists, err := demoDao.Exists(demo); err != nil {
		helpers.SendError(c, dc.LoggerBack, helpers.ErrInvalidInput(), nil, err)
		return
	} else if exists {
		helpers.SendError(c, dc.LoggerBack, helpers.ErrDuplicate(), nil, err)
		return
	}

	if err := demoDao.Create(&demo, nil); err != nil {
		helpers.CheckErrorValidator(c, dc.LoggerBack, err)
		return
	}

	newZipFile, err := os.Create(path)
	if err != nil {
		helpers.SendError(c, dc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}
	defer func() {
		if err := newZipFile.Close(); err != nil {
			helpers.SendError(c, dc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
			return
		}
	}()

	demoFile, err := demoFormFile.Open()
	if err != nil {
		helpers.SendError(c, dc.LoggerBack, helpers.ErrInvalidInput(), nil, err)
		return
	}

	demoBytes, err := ioutil.ReadAll(bufio.NewReader(demoFile))
	if err != nil {
		helpers.SendError(c, dc.LoggerBack, helpers.ErrInvalidInput(), nil, err)
		return
	}

	if _, err := newZipFile.Write(demoBytes); err != nil {
		helpers.SendError(c, dc.LoggerBack, helpers.ErrInternalServerError(), nil, err)
		return
	}

	wQuery := c.MustGet("wQuery").(*sync.WaitGroup)
	go waitForAnalyze(dc, demo, wQuery)

	helpers.SendData(c, http.StatusCreated, strconv.FormatInt(demoID, 10))
}

func checkToken(c *gin.Context, db *gorm.DB) (uint, error) {
	tokenStr := c.Param("token")
	if tokenStr == "" {
		return 0, ErrNoTokenSent
	}

	token := model.TokenUploadDemo{
		Token:     &tokenStr,
		IPAllowed: helpers.StrPtr(c.ClientIP()),
	}

	tokenDAO := dao.NewTokenUploadDemoDAO(db)
	if err := tokenDAO.Get(&token); err != nil {
		return 0, err
	}

	dbOverride := store.GetDBOverride(c)
	tokenOverrideDAO := dao.NewTokenUploadDemoDAO(dbOverride)
	if err := tokenOverrideDAO.UpdateUse(&token); err != nil {
		return 0, err
	}

	return *token.UserID, nil
}

func waitForAnalyze(dc DemoController, demo model.Demo, wQuery *sync.WaitGroup) {
	c := make(chan struct{})
	go func() {
		defer close(c)
		wQuery.Wait()
	}()

	select {
	case <-c:
	case <-time.After(time.Second * 5):
		return
	}

	if err := dc.dc.AddDate(demo); err != nil {
		dc.LoggerBack.Error(err, "err to log : ", err)
	}
}

func parseDate(filename string) (time.Time, error) {
	re := regexp.MustCompile(`deathmatch-\d+-(\d{8})-\d+-\w+.dem`)
	rawDate := re.FindStringSubmatch(filename)
	if len(rawDate) != 2 {
		return time.Time{}, ErrBadFileName
	}

	year, err := strconv.Atoi(rawDate[1][0:4])
	if err != nil {
		return time.Time{}, ErrBadFileName
	}
	month, err := strconv.Atoi(rawDate[1][4:6])
	if err != nil {
		return time.Time{}, ErrBadFileName
	}
	day, err := strconv.Atoi(rawDate[1][6:8])
	if err != nil {
		return time.Time{}, ErrBadFileName
	}

	return time.Date(year, time.Month(month), day, 12, 0, 0, 0, time.UTC), nil
}
