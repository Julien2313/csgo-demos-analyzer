package controller_test

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

// sendRequest send a simple request
func sendRequest(router *gin.Engine, parameters []byte, methode string, url string) *httptest.ResponseRecorder {
	req, err := http.NewRequest(methode, url, bytes.NewBuffer(parameters))
	if err != nil {
		panic(err)
	}

	req.Header.Add("Content-Type", "application/json")

	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)

	return resp
}

// sendRequestWithToken send a simple request with the connection token
func sendRequestWithToken(
	router *gin.Engine,
	params []byte,
	methode, url string,
	authCookie *http.Cookie,
) *httptest.ResponseRecorder {
	req, err := http.NewRequest(methode, url, bytes.NewBuffer(params))
	if err != nil {
		panic(err)
	}

	req.Header.Add("Content-Type", "application/json")
	req.AddCookie(authCookie)

	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)

	return resp
}

// sendRequestWithFile send a request with a file
func sendRequestWithFile(
	router *gin.Engine,
	methode, url string,
	authCookie *http.Cookie,
	params map[string][]byte,
	pathFile, fieldName, fileName string,
) (*httptest.ResponseRecorder, error) {

	demoFile, err := os.Open(pathFile)
	if err != nil {
		return nil, err
	}
	defer demoFile.Close()

	var bufferDemo bytes.Buffer
	w := multipart.NewWriter(&bufferDemo)

	for k, v := range params {
		if err := w.WriteField(k, string(v)); err != nil {
			return nil, err
		}
	}

	fw, err := w.CreateFormFile(fieldName, fileName)
	if err != nil {
		return nil, err
	}

	if _, err := io.Copy(fw, demoFile); err != nil {
		return nil, err
	}
	w.Close()

	req, err := http.NewRequest(methode, url, &bufferDemo)
	if err != nil {
		return nil, err
	}

	q := req.URL.Query()
	for k, v := range params {
		q.Add(k, string(v))
	}

	req.URL.RawQuery = q.Encode()

	req.Header.Add("Content-Type", w.FormDataContentType())
	req.AddCookie(authCookie)

	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)

	return resp, nil
}

// performTestAuth return the cookie of the auth
func performTestAuth(router *gin.Engine, user model.User) *http.Cookie {
	type tmpUser struct {
		Mail     *string `json:"mail"`
		Password *string `json:"password"`
	}

	type tmpCredentials struct {
		User tmpUser `json:"credentials"`
	}

	userBytes, err := json.Marshal(&tmpCredentials{
		User: tmpUser{
			Mail:     user.Mail,
			Password: user.Password,
		},
	})
	if err != nil {
		panic(err)
	}

	resp := sendRequest(router, userBytes, http.MethodPost, "/api/v1/user/login")
	for _, cookie := range resp.Result().Cookies() {
		if cookie.Name == "csgo_demo_analyzer_session" {
			return cookie
		}
	}

	return nil
}

// checkMustBeConnected check if an user has to be connected
// for the access of a route
// mustBeConnected		errorQueryTocken		error
// true					false					true
// false				false					false
// true					true					false
// false				true					true
func checkMustBeConnected(router *gin.Engine, mustBeConnected bool, methode, url string) error {
	resp := sendRequest(router, nil, methode, url)

	var errorNewGen helpers.ErrorNewGen

	if err := json.Unmarshal(resp.Body.Bytes(), &errorNewGen); err != nil {
		panic(err)
	}

	errorQueryTocken := errorNewGen.Title == helpers.ErrBadTokenConnection().Title

	if mustBeConnected && !errorQueryTocken {
		return errors.New("Should not be possible to perform this action, " + errorNewGen.Title)
	} else if !mustBeConnected && errorQueryTocken {
		return errors.New("Should be possible to perform this action, " + errorNewGen.Title)
	}

	return nil
}
