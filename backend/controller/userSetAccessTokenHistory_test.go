package controller_test

import (
	"encoding/json"
	"net/http"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/mock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/server"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/downloaderinfo"
)

const (
	methodeAuthenticationCodeHitory = http.MethodPost
	urlAuthenticationCodeHitory     = "/api/v1/user/authenticationcodehitory"
)

func TestControllerUser_SetAccessTokenHistoryMustBeConnected(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	assert.NoError(t, checkMustBeConnected(
		serverTest.Router,
		true,
		methodeAuthenticationCodeHitory,
		urlAuthenticationCodeHitory,
	))
}

func TestControllerUser_SetAccessTokenHistoryOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	httpCookie := performTestAuth(serverTest.Router, *userGenerated)

	accessTokHistB, err := json.Marshal(&tmpAccessTokenHistorySend{
		AccessTokenHistory: "8HRT-EF3O4-K5PO",
	})
	require.NoError(t, err)

	resp := sendRequestWithToken(serverTest.Router,
		accessTokHistB,
		methodeAuthenticationCodeHitory,
		urlAuthenticationCodeHitory,
		httpCookie)
	require.Equal(t, http.StatusOK, resp.Code)

	userFromDB := model.User{Mail: userGenerated.Mail}
	userDAO := dao.NewUserDAO(localDB)
	require.NoError(t, userDAO.GetUserByMail(&userFromDB))
	assert.Equal(t, "8HRT-EF3O4-K5PO", *userFromDB.AccessTokenHistory)

	userFromDB2 := *userGenerated2
	require.NoError(t, userDAO.GetUserByMail(&userFromDB2))
	assert.Equal(t, userGenerated2.AccessTokenHistory, userFromDB2.AccessTokenHistory)
}

func TestControllerUser_SetAccessTokenHistoryNOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	userDAO := dao.NewUserDAO(localDB)

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	httpCookie := performTestAuth(serverTest.Router, *userGenerated)

	accessTokHistB, err := json.Marshal(&tmpAccessTokenHistorySend{
		AccessTokenHistory: "8hRT-EF3O4-K5PO",
	})
	require.NoError(t, err)

	resp := sendRequestWithToken(serverTest.Router,
		accessTokHistB,
		methodeAuthenticationCodeHitory,
		urlAuthenticationCodeHitory,
		httpCookie)
	require.Equal(t, http.StatusBadRequest, resp.Code)

	userFromDB := model.User{Mail: userGenerated.Mail}
	require.NoError(t, userDAO.GetUserByMail(&userFromDB))
	assert.Equal(t, userGenerated.AccessTokenHistory, userFromDB.AccessTokenHistory)

	accessTokHistB, err = json.Marshal(&tmpAccessTokenHistorySend{
		AccessTokenHistory: "8HRT-EFO4-K5PO",
	})
	require.NoError(t, err)

	resp = sendRequestWithToken(serverTest.Router,
		accessTokHistB,
		methodeAuthenticationCodeHitory,
		urlAuthenticationCodeHitory,
		httpCookie)
	require.Equal(t, http.StatusBadRequest, resp.Code)

	userFromDB = model.User{Mail: userGenerated.Mail}
	require.NoError(t, userDAO.GetUserByMail(&userFromDB))
	assert.Equal(t, userGenerated.AccessTokenHistory, userFromDB.AccessTokenHistory)

	accessTokHistB, err = json.Marshal(&tmpAccessTokenHistorySend{
		AccessTokenHistory: "8HRT-EF3O4-K5PO-K5PO",
	})
	require.NoError(t, err)

	resp = sendRequestWithToken(serverTest.Router,
		accessTokHistB,
		methodeAuthenticationCodeHitory,
		urlAuthenticationCodeHitory,
		httpCookie)
	require.Equal(t, http.StatusBadRequest, resp.Code)

	userFromDB = model.User{Mail: userGenerated.Mail}
	require.NoError(t, userDAO.GetUserByMail(&userFromDB))
	assert.Equal(t, userGenerated.AccessTokenHistory, userFromDB.AccessTokenHistory)

	resp = sendRequestWithToken(serverTest.Router,
		[]byte("teeeest"),
		methodeAuthenticationCodeHitory,
		urlAuthenticationCodeHitory,
		httpCookie)
	require.Equal(t, http.StatusBadRequest, resp.Code)

	userFromDB = model.User{Mail: userGenerated.Mail}
	require.NoError(t, userDAO.GetUserByMail(&userFromDB))
	assert.Equal(t, userGenerated.AccessTokenHistory, userFromDB.AccessTokenHistory)
}
