package controller_test

import (
	"encoding/json"
	"net/http"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/mock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/server"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/downloaderinfo"
	"gorm.io/gorm"
)

const (
	methodeRegister = http.MethodPost
	urlRegister     = "/api/v1/user/register"
)

func TestControllerUser_RegisterMustBeConnected(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	assert.Nil(t, checkMustBeConnected(serverTest.Router, false, methodeRegister, urlRegister))
}

func TestControllerUser_RegisterOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	userDAO := dao.NewUserDAO(localDB)
	userBytes, err := json.Marshal(&tmpCredentialsSend{
		User: tmpUserSend{
			Mail:     userGeneratedNotStored.Mail,
			Password: userGeneratedNotStored.Password,
			Confirm:  userGeneratedNotStored.Password,
		},
	})
	require.NoError(t, err)

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}

	resp := sendRequest(serverTest.Router, userBytes, methodeRegister, urlRegister)
	require.Equal(t, http.StatusCreated, resp.Code)

	userFromDB := &model.User{Mail: userGenerated.Mail}
	assert.NoError(t, userDAO.GetUserByMail(userFromDB))
	assert.NoError(t, helpers.CheckHashPassword(*userFromDB.Password, *userGenerated.Password))
}

func TestControllerUser_RegisterFieldMissing(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	userDAO := dao.NewUserDAO(localDB)
	localUserGenerated := model.GenerateRandomUser(false)
	userBytes, err := json.Marshal(&tmpCredentialsSend{
		User: tmpUserSend{
			Mail:     localUserGenerated.Mail,
			Password: localUserGenerated.Password,
		},
	})
	require.NoError(t, err)

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}

	resp := sendRequest(serverTest.Router, userBytes, methodeRegister, urlRegister)
	assert.Equal(t, http.StatusBadRequest, resp.Code)

	userFromDB := *localUserGenerated
	assert.EqualError(t, userDAO.GetUserByMail(&userFromDB), gorm.ErrRecordNotFound.Error())
}

func TestControllerUser_RegisterBadConfirm(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	userDAO := dao.NewUserDAO(localDB)
	localUserGenerated := model.GenerateRandomUser(false)
	userBytes, err := json.Marshal(&tmpCredentialsSend{
		User: tmpUserSend{
			Mail:     localUserGenerated.Mail,
			Password: localUserGenerated.Password,
			Confirm:  localUserGenerated.Mail,
		},
	})
	require.NoError(t, err)

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}

	resp := sendRequest(serverTest.Router, userBytes, methodeRegister, urlRegister)
	assert.Equal(t, http.StatusBadRequest, resp.Code)

	userFromDB := *localUserGenerated
	assert.EqualError(t, userDAO.GetUserByMail(&userFromDB), gorm.ErrRecordNotFound.Error())
}

func TestControllerUser_RegisterBadMail(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	userDAO := dao.NewUserDAO(localDB)
	localUserGenerated := model.GenerateRandomUser(false)
	userBytes, err := json.Marshal(&tmpCredentialsSend{
		User: tmpUserSend{
			Mail:     helpers.StrPtr("badmail"),
			Password: localUserGenerated.Password,
			Confirm:  localUserGenerated.Password,
		},
	})
	require.NoError(t, err)

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}

	resp := sendRequest(serverTest.Router, userBytes, methodeRegister, urlRegister)
	assert.Equal(t, http.StatusBadRequest, resp.Code)

	userFromDB := *localUserGenerated
	assert.EqualError(t, userDAO.GetUserByMail(&userFromDB), gorm.ErrRecordNotFound.Error())
}

func TestControllerUser_RegisterExists(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	userBytes, err := json.Marshal(&tmpCredentialsSend{
		User: tmpUserSend{
			Mail:     userGenerated.Mail,
			Password: userGenerated.Password,
			Confirm:  userGenerated.Password,
		},
	})
	require.NoError(t, err)

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}

	resp := sendRequest(serverTest.Router, userBytes, methodeRegister, urlRegister)
	assert.Equal(t, http.StatusConflict, resp.Code)
}
