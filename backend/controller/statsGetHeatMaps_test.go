package controller_test

import (
	"encoding/json"
	"net/http"
	"strings"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/mock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/server"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/downloaderinfo"
	"gorm.io/gorm"
)

const (
	methodeGetHeatMaps = http.MethodGet
	urlGetHeatMaps     = "/api/v1/stats/heatmaps/:steamID/:mapName"
)

func TestControllerDemo_GetHeatMapsOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	httpCookie := performTestAuth(serverTest.Router, *userGenerated)
	require.NotNil(t, httpCookie)

	cptHeatMapKills := 0
	cptHeatMapDmg := 0

	demos := []*model.Demo{}
	require.NoError(t, localDB.
		Preload("SteamsIDs").
		Where("? IN (?)",
			*userGenerated.SteamID,
			localDB.Model(&model.SteamID{}).Select("steam_id").Where("demo_id = demos.id")).
		Preload("Rounds.HeatMapKills", func(db *gorm.DB) *gorm.DB {
			return db.Where("steam_id_killer = ? OR steam_id_victim = ?", *userGenerated.SteamID, *userGenerated.SteamID)
		}).
		Preload("Rounds.HeatMapDmgs", func(db *gorm.DB) *gorm.DB {
			return db.Where("steam_id_shooter = ? OR steam_id_victim = ?", *userGenerated.SteamID, *userGenerated.SteamID)
		}).Where("map_name = ?", *demosGeneratedAnalyzed[0].MapName).Find(&demos).Error)
	for _, demo := range demos {
		for _, round := range demo.Rounds {
			cptHeatMapKills += len(round.HeatMapKills)
			cptHeatMapDmg += len(round.HeatMapDmgs)
		}
	}

	resp := sendRequestWithToken(serverTest.Router,
		nil,
		methodeGetHeatMaps,
		strings.Replace(
			strings.Replace(urlGetHeatMaps, ":steamID", *userGenerated.SteamID, 1),
			":mapName", *demosGeneratedAnalyzed[0].MapName,
			1,
		),
		httpCookie)
	require.Equal(t, http.StatusOK, resp.Code)

	var response map[string][]interface{}
	require.NoError(t, json.Unmarshal(resp.Body.Bytes(), &response))

	heatMaps := response["data"]
	require.Len(t, heatMaps, 2)

	assert.Len(t, heatMaps[0].([]interface{}), cptHeatMapKills, "%d:%d", len(heatMaps[0].([]interface{})), cptHeatMapKills)
	assert.Len(t, heatMaps[1].([]interface{}), cptHeatMapDmg, "%d:%d", len(heatMaps[1].([]interface{})), cptHeatMapDmg)
}
