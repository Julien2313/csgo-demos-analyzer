package controller_test

import (
	"encoding/json"
	"net/http"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/controller"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/mock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/server"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
)

const (
	methodeGetCompetitiveDemo = http.MethodGet
	urlGetCompetitiveDemo     = "/api/v1/stats/competitives"
)

func BenchmarkGetCompetitiveDemo(b *testing.B) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	serverTest.Router = gin.Default()
	require.NoError(b, serverTest.InitializeRoutes(localDB, nil, nil,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false),
	)
	httpCookie := performTestAuth(serverTest.Router, *userBanchmark)
	require.NotNil(b, httpCookie)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		resp := sendRequestWithToken(
			serverTest.Router,
			nil,
			methodeGetCompetitiveDemo,
			urlGetCompetitiveDemo,
			httpCookie,
		)
		require.Equal(b, http.StatusOK, resp.Code)
	}
}

func TestControllerDemo_GetProgressionVelocity(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	_userGenerated := model.StoreGeneratedUser(localDB)
	demo := model.GenerateRandomDemoWithStats(_userGenerated, *_userGenerated.SteamID, 10)
	for _, team := range demo.TeamsStats {
		for _, player := range team.PlayersStats {
			if *player.SteamID == *_userGenerated.SteamID {
				*player.Marks.AverageVelocityShoots = 100
			}
		}
	}

	require.NoError(t, localDB.Create(&demo).Error)

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	serverTest.Router = gin.Default()
	require.NoError(t, serverTest.InitializeRoutes(localDB, nil, nil,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false),
	)
	httpCookie := performTestAuth(serverTest.Router, *_userGenerated)
	require.NotNil(t, httpCookie)

	resp := sendRequestWithToken(serverTest.Router,
		nil,
		methodeGetCompetitiveDemo,
		urlGetCompetitiveDemo,
		httpCookie)
	require.Equal(t, http.StatusOK, resp.Code)

	var response map[string]controller.WeaponsStatsDemosReturn
	require.NoError(t, json.Unmarshal(resp.Body.Bytes(), &response))
	stats := response["data"]
	require.Len(t, stats.WeaponStatsPerDay.AverageVelocityAttacker.Data, 1)
	assert.Equal(t, 100.0, stats.WeaponStatsPerDay.AverageVelocityAttacker.Data[0].Y)
}

func TestControllerDemo_GetCompetitiveDemoOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	now := time.Now()
	now = time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())

	localUserGenerated := model.StoreGeneratedUser(localDB)
	mmDemo := model.GenerateRandomDemoWithStats(localUserGenerated, *localUserGenerated.SteamID, 10)
	require.NoError(t, localDB.Create(mmDemo).Error)
	mmDemo = model.GenerateRandomDemoWithStats(localUserGenerated, *localUserGenerated.SteamID, 10)
	require.NoError(t, localDB.Create(mmDemo).Error)
	mmDemo = model.GenerateRandomDemoWithStats(localUserGenerated, *localUserGenerated.SteamID, 10)
	require.NoError(t, localDB.Create(mmDemo).Error)

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	serverTest.Router = gin.Default()
	require.NoError(t, serverTest.InitializeRoutes(localDB, nil, nil,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false),
	)
	httpCookie := performTestAuth(serverTest.Router, *localUserGenerated)
	require.NotNil(t, httpCookie)

	resp := sendRequestWithToken(serverTest.Router,
		nil,
		methodeGetDeathMatchDemo,
		urlGetDeathMatchDemo,
		httpCookie)
	require.Equal(t, http.StatusOK, resp.Code)

	var response map[string]controller.WeaponsStatsDemosReturn
	require.NoError(t, json.Unmarshal(resp.Body.Bytes(), &response))
	weaponsStatsDemos := response["data"]
	nbrDemos := len(weaponsStatsDemos.WeaponStatsPerDay.BulletAccuracy.Data)
	for _, statsPerRoundBySide := range weaponsStatsDemos.WeaponMarksPerRoundPerDay {
		assert.Len(t, statsPerRoundBySide.FirstBulletAccuracy.Data, nbrDemos)
		assert.Len(t, statsPerRoundBySide.FirstBulletHS.Data, nbrDemos)
		assert.Len(t, statsPerRoundBySide.BulletAccuracy.Data, nbrDemos)
		assert.Len(t, statsPerRoundBySide.HSPercentage.Data, nbrDemos)
		assert.Len(t, statsPerRoundBySide.NbrBulletsFired.Data, nbrDemos)
		assert.Len(t, statsPerRoundBySide.AverageVelocityAttacker.Data, nbrDemos)
	}
}
