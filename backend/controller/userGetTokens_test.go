package controller_test

import (
	"encoding/json"
	"math/rand"
	"net/http"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/controller"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/mock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/server"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
)

const (
	methodeTokens = http.MethodGet
	urlTokens     = "/api/v1/user/tokens"
)

func TestControllerUser_Tokens(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	userDAO := dao.NewUserDAO(localDB)
	userPicked := mock.UsersMocked[rand.Intn(mock.NbrUser)]
	localUserGenerated := model.StoreGeneratedUser(localDB)
	localUserGenerated.SteamID = userPicked.SteamID
	require.NoError(t, userDAO.UpdateSteamID(localUserGenerated))

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, nil,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	httpCookie := performTestAuth(serverTest.Router, *localUserGenerated)

	resp := sendRequestWithToken(serverTest.Router,
		nil,
		methodeTokens,
		urlTokens,
		httpCookie)
	require.Equal(t, http.StatusOK, resp.Code)

	var response map[string][]controller.ConfigTokenResp

	require.NoError(t, json.Unmarshal(resp.Body.Bytes(), &response))
	tokens := response["data"]
	assert.Len(t, tokens, 0)

	tokensDB := model.GenerateRandomTokens(*localUserGenerated.ID, 10)
	require.NoError(t, localDB.Create(tokensDB).Error)

	resp = sendRequestWithToken(serverTest.Router,
		nil,
		methodeTokens,
		urlTokens,
		httpCookie)
	require.Equal(t, http.StatusOK, resp.Code)

	require.NoError(t, json.Unmarshal(resp.Body.Bytes(), &response))
	tokens = response["data"]
	assert.Len(t, tokens, 10)
}
