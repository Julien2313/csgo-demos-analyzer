package controller_test

import (
	"encoding/json"
	"net/http"
	"strings"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/controller"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/mock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/server"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/downloaderinfo"
)

const (
	methodeUserMe = http.MethodGet
	urlUserMe     = "/api/v1/user/me"
)

func TestControllerUser_GetCurrentUserMustBeConnected(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	assert.Nil(t, checkMustBeConnected(serverTest.Router, true, methodeUserMe, urlUserMe))
}

func TestControllerUser_GetCurrentUserOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	userDAO := dao.NewUserDAO(localDB)

	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}
	httpCookie := performTestAuth(serverTest.Router, *userGenerated)

	resp := sendRequestWithToken(serverTest.Router,
		nil,
		methodeUserMe,
		urlUserMe,
		httpCookie)
	require.Equal(t, http.StatusOK, resp.Code)

	var response map[string]controller.GetCurrentUserResp

	require.NoError(t, json.Unmarshal(resp.Body.Bytes(), &response))

	userResponse := response["data"]

	userFromDB := model.User{
		Mail:     helpers.StrPtr(*userGenerated.Mail),
		Password: helpers.StrPtr(*userGenerated.Password),
	}
	require.NoError(t, userDAO.GetUserByMail(&userFromDB))
	require.NoError(t, helpers.CheckHashPassword(*userFromDB.Password, *userGenerated.Password))

	assert.NotNil(t, userResponse.ID)
	assert.NotNil(t, userResponse.Mail)
	assert.NotNil(t, userResponse.SteamID)
	assert.NotNil(t, userResponse.SizeUsed)

	assert.Equal(t, *userGenerated.ID, *userResponse.ID)
	assert.Equal(t, strings.ToUpper(*userGenerated.Mail), strings.ToUpper(*userResponse.Mail))
	assert.Equal(t, *userGenerated.SteamID, *userResponse.SteamID)
	assert.Equal(t, *userGenerated.SizeUsed, *userResponse.SizeUsed)
}

func TestControllerUser_GetCurrentUserNotLogged(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	serverTest := &server.Server{SchemaDB: store.SchemaTestController}
	serverTest.SetDB(localDB)

	di := &downloaderinfo.DownloaderInfo{TokenAPIKey: mock.APIkey}
	serverTest.SetDownloaderInfo(di)

	serverTest.Router = gin.Default()
	if err := serverTest.InitializeRoutes(localDB, nil, di,
		nil,
		nil,
		"127.0.0.1", mock.APIkey,
		currentVersionAnalyzer,
		logTest, logTest,
		false); err != nil {
		panic(err)
	}

	resp := sendRequest(serverTest.Router, nil, methodeUserMe, urlUserMe)
	require.Equal(t, http.StatusUnauthorized, resp.Code)

	var response map[string]model.User

	assert.Error(t, json.Unmarshal(resp.Body.Bytes(), &response))
}
