package custlogger

import (
	"fmt"
	"io"

	"github.com/natefinch/lumberjack"
	"github.com/sirupsen/logrus"
)

const defaultLogPath = "."
const defaultLogErrorPath = "."

// Logger type for logging info
type Logger struct {
	logger        *logrus.Entry
	DefaultOutput *io.PipeWriter
	test          bool
}

var log *logrus.Logger
var logErr *logrus.Logger
var loggerErr Logger

var writer *io.PipeWriter
var writerErr *io.PipeWriter

var _ = func() error {
	log = logrus.New()
	logErr = logrus.New()

	path := defaultLogPath + "/csgo-analyzer.log"
	pathErr := defaultLogErrorPath + "/csgo-analyzer-err.log"

	log.SetLevel(logrus.TraceLevel)

	log.SetFormatter(&logrus.TextFormatter{
		DisableColors: false,
		FullTimestamp: true,
	})
	logErr.SetFormatter(&logrus.TextFormatter{
		DisableColors: false,
		FullTimestamp: true,
	})

	// Log as JSON instead of the default ASCII formatter.
	log.SetFormatter(&logrus.JSONFormatter{})
	logErr.SetFormatter(&logrus.JSONFormatter{})

	log.Out = &lumberjack.Logger{
		Filename:   path,
		MaxSize:    100, // megabytes
		MaxBackups: 10,
		MaxAge:     14, // days
		Compress:   true,
	}
	logErr.Out = &lumberjack.Logger{
		Filename:   pathErr,
		MaxSize:    100, // megabytes
		MaxBackups: 10,
		MaxAge:     31, // days
		Compress:   true,
	}

	writer = log.Writer()
	writerErr = logErr.Writer()

	loggerErr.logger = logErr.WithFields(logrus.Fields{
		"service": "ERROR",
	})
	loggerErr.DefaultOutput = writerErr

	return nil
}()

// AddField add a field to the logger
func (l *Logger) AddField(name string, value interface{}) *Logger {
	l.logger = l.logger.WithField(name, value)
	return l
}

// SetupInstanceLoggger has to be used when setupping logger when not testing
func (l *Logger) SetupInstanceLoggger(name string) {
	l.logger = log.WithFields(logrus.Fields{
		"service": name,
	})
	l.DefaultOutput = writer
	l.test = false
}

// SetupInstanceLogggerTest has to be used when setupping logger when testing
func (l *Logger) SetupInstanceLogggerTest() {
	l.logger = log.WithFields(logrus.Fields{
		"service": "test",
	})
	l.test = true
}

func (l *Logger) Error(err error, args ...interface{}) {
	if l == nil || l.test || l.logger == nil {
		fmt.Println(args...)
		fmt.Println(err)
	} else {
		l.logger.WithError(err).Error(args...)
		loggerErr.logger.WithError(err).Error(args...)
	}
}

// Errorf helpers to use Errorf
func (l *Logger) Errorf(err error, format string, args ...interface{}) {
	if l == nil || l.test || l.logger == nil {
		fmt.Printf(format, args...)
		fmt.Println(err)
	} else {
		l.logger.WithError(err).Errorf(format, args...)
		loggerErr.logger.WithError(err).Errorf(format, args...)
	}
}

// Trace helpers to use Trace
func (l *Logger) Trace(args ...interface{}) {
	if l == nil || l.test || l.logger == nil {
		fmt.Println(args...)
	} else {
		l.logger.Trace(args...)
	}
}

// Tracef helpers to use Tracef
func (l *Logger) Tracef(format string, args ...interface{}) {
	if l == nil || l.test || l.logger == nil {
		fmt.Printf(format, args...)
	} else {
		l.logger.Tracef(format, args...)
	}
}

// Info helpers to use Info
func (l *Logger) Info(args ...interface{}) {
	if l == nil || l.test || l.logger == nil {
		fmt.Println(args...)
	} else {
		l.logger.Info(args...)
	}
}

// Infof helpers to use Infof
func (l *Logger) Infof(format string, args ...interface{}) {
	if l == nil || l.test || l.logger == nil {
		fmt.Printf(format, args...)
	} else {
		l.logger.Infof(format, args...)
	}
}
