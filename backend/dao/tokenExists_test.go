package dao_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
)

func TestDaoToken_Exists(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	tokenDAO := dao.NewTokenUploadDemoDAO(localDB)

	exists, err := tokenDAO.Exists(tokenGenerated[0])
	require.NoError(t, err)
	require.True(t, exists)

	exists, err = tokenDAO.Exists(tokenGeneratedNotStored[0])
	require.NoError(t, err)
	require.False(t, exists)
}
