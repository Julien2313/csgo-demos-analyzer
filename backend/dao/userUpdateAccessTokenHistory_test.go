package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoUser_UpdateAccessTokenHistory(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	userDAO := dao.NewUserDAO(localDB)
	user := model.GenerateRandomUser(true)
	require.NoError(t, userDAO.Create(user))

	user.AccessTokenHistory = helpers.StrPtr(helpers.RandomString(10))
	assert.NoError(t, userDAO.UpdateAccessTokenHistory(user))

	userFromDB := model.User{
		ID: user.ID,
	}
	require.NoError(t, localDB.First(&userFromDB).Error)
	assert.Equal(t, *user.AccessTokenHistory, *userFromDB.AccessTokenHistory)

	user.AccessTokenHistory = nil
	assert.Error(t, userDAO.UpdateAccessTokenHistory(user))
}
