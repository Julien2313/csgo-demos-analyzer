package dao_test

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoUser_Get(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	userDAO := dao.NewUserDAO(localDB)

	userFromDB := model.User{
		ID: userGenerated.ID,
	}
	require.NoError(t, userDAO.Get(&userFromDB))
	assert.NotNil(t, userFromDB.ShareCode)
	assert.NotNil(t, userFromDB.AccessTokenHistory)
	assert.NotNil(t, userFromDB.SteamID)
	assert.Nil(t, userFromDB.Demos)

	assert.NotNil(t, userFromDB.ID)
	assert.NotNil(t, userFromDB.CreatedAt)
	assert.NotNil(t, userFromDB.UpdatedAt)
	assert.NotNil(t, userFromDB.SteamID)
	assert.NotNil(t, userFromDB.Mail)
	assert.NotNil(t, userFromDB.Password)
	assert.NotNil(t, userFromDB.SizeUsed)

	assert.Equal(t, strings.ToUpper(*userGenerated.Mail), strings.ToUpper(*userFromDB.Mail))

	userFromDB = model.User{
		Mail: userGenerated.Mail,
	}
	require.NoError(t, userDAO.Get(&userFromDB))
	assert.Equal(t, *userGenerated.ID, *userFromDB.ID)

	assert.Error(t, userDAO.Get(&model.User{}))
}
