package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoDemo_SetSizeOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	size := int64(47468464)
	demoDAO := dao.NewDemoDAO(localDB, logTest)
	require.NoError(t, demoDAO.SetSize(*demosGenerated[0].ID, size))

	demoFromDB := model.Demo{
		ID: demosGenerated[0].ID,
	}
	require.NoError(t, demoDAO.Get(&demoFromDB))
	assert.Equal(t, size, *demoFromDB.Size)
}

func TestDaoDemo_SetSizeNOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	size := int64(47468464)
	demoDAO := dao.NewDemoDAO(localDB, logTest)
	assert.Error(t, demoDAO.SetSize(-15, size))
}
