package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoDemo_Get(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	demoDAO := dao.NewDemoDAO(localDB, logTest)

	demoFromDB := &model.Demo{ID: demosGeneratedAnalyzed[0].ID}
	assert.NoError(t, demoDAO.Get(demoFromDB))
	assert.Error(t, demoDAO.Get(&model.Demo{}))
}
