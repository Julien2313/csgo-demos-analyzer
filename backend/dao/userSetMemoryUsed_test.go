package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoUser_SetMemoryUsed(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	userDAO := dao.NewUserDAO(localDB)
	user := model.GenerateRandomUser(true)
	require.NoError(t, userDAO.Create(user))
	user.SizeUsed = helpers.FloatPtr(65.41684)
	assert.NoError(t, userDAO.SetMemoryUsed(*user, *user.SizeUsed))

	userFromDB := model.User{
		SteamID: user.SteamID,
	}
	require.NoError(t, userDAO.Get(&userFromDB))
	assert.Equal(t, *user.SizeUsed, *userFromDB.SizeUsed)

	user.ID = nil
	assert.NoError(t, userDAO.SetMemoryUsed(*user, *user.SizeUsed))

	require.NoError(t, userDAO.Get(&userFromDB))
	assert.Equal(t, *user.SizeUsed, *userFromDB.SizeUsed)

	user.Mail = nil
	assert.NoError(t, userDAO.SetMemoryUsed(*user, *user.SizeUsed))

	require.NoError(t, userDAO.Get(&userFromDB))
	assert.Equal(t, *user.SizeUsed, *userFromDB.SizeUsed)

	assert.Error(t, userDAO.SetMemoryUsed(model.User{}, 2))
}
