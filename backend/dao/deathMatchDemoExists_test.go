package dao_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDeathMatchDemo_Exists(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	dmDAO := dao.NewDemoDeathMatchDAO(localDB)
	exists, err := dmDAO.Exists(deathMatchDemosGenerated[0])
	require.NoError(t, err)
	require.True(t, exists)

	exists, err = dmDAO.Exists(model.DeathMatchDemo{})
	require.Error(t, err)
	require.False(t, exists)

	exists, err = dmDAO.Exists(deathMatchDemosGeneratedNotStored[0])
	require.NoError(t, err)
	require.False(t, exists)
}
