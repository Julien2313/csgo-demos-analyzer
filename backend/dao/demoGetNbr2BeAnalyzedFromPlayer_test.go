package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
)

func TestDaoDemo_GetNbr2BeAnalyzedFromPlayer(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	demoDAO := dao.NewDemoDAO(localDB, logTest)
	nbrDemos, err := demoDAO.GetNbr2BeAnalyzedFromPlayer(*userGenerated.SteamID, currentVersionAnalyzer)
	require.NoError(t, err)

	assert.Equal(t, int64(nbrDemosGenerated), nbrDemos)
}
