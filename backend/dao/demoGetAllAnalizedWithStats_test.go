package dao_test

import (
	"sort"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
)

func TestDaoDemo_GetAllAnalizedWithStats(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	demoDAO := dao.NewDemoDAO(localDB, logTest)

	demosResp, err := demoDAO.GetAllAnalizedWithStats(*userGenerated.SteamID, nil, currentVersionAnalyzer)
	require.NoError(t, err)
	require.Len(t, demosResp, nbrDemosGenerated)

	sort.Slice(demosResp, func(i, j int) bool {
		return demosResp[i].Date.After(*demosResp[j].Date)
	})

	for numDemo := 0; numDemo < nbrDemosGenerated; numDemo++ {
		assert.Equal(t, *demosGeneratedAnalyzed[numDemo].ID, *demosResp[numDemo].ID)
		assert.Equal(t, *demosGeneratedAnalyzed[numDemo].VersionAnalyzer, *demosResp[numDemo].VersionAnalyzer)
		assert.Equal(t, *demosGeneratedAnalyzed[numDemo].UserID, *demosResp[numDemo].UserID)
		assert.Equal(t, demosGeneratedAnalyzed[numDemo].Date.UnixNano(), demosResp[numDemo].Date.UnixNano())
		assert.Equal(t, *demosGeneratedAnalyzed[numDemo].MapName, *demosResp[numDemo].MapName)
		assert.Equal(t, *demosGeneratedAnalyzed[numDemo].Path, *demosResp[numDemo].Path)
		assert.Equal(t, *demosGeneratedAnalyzed[numDemo].Name, *demosResp[numDemo].Name)
		assert.Equal(t, *demosGeneratedAnalyzed[numDemo].Size, *demosResp[numDemo].Size)
		assert.Equal(t, *demosGeneratedAnalyzed[numDemo].ReplayURL, *demosResp[numDemo].ReplayURL)

		for numTeam := range demosGeneratedAnalyzed[numDemo].TeamsStats {
			teamGen := demosGeneratedAnalyzed[numDemo].TeamsStats[numTeam]
			teamResp := demosResp[numDemo].TeamsStats[numTeam]
			assert.Equal(t, *teamGen.ID, *teamResp.ID)
			assert.Equal(t, *teamGen.Score, *teamResp.Score)

			for numPlayer := 0; numPlayer < 5; numPlayer++ {
				playerGen := teamGen.PlayersStats[numPlayer]
				playerResp := teamResp.PlayersStats[numPlayer]
				assert.Equal(t, *playerGen.ID, *playerResp.ID)
				assert.Equal(t, *playerGen.TeamStatsID, *playerResp.TeamStatsID)
				assert.Equal(t, *playerGen.SteamID, *playerResp.SteamID)
				assert.Equal(t, *playerGen.Username, *playerResp.Username)
				assert.Equal(t, *playerGen.Kills, *playerResp.Kills)
				assert.Equal(t, *playerGen.Deaths, *playerResp.Deaths)
				assert.Equal(t, *playerGen.Assists, *playerResp.Assists)
			}
		}
	}
}
