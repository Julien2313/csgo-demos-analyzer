package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
)

func TestDaoDemo_GetFromShareCode(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	demoDAO := dao.NewDemoDAO(localDB, logTest)
	_, err := demoDAO.GetFromShareCode("qsd36f5436sqdf4gsgf")
	require.Error(t, err)

	demoFromDB, err := demoDAO.GetFromShareCode(*demosGeneratedAnalyzed[0].Sharecode)
	require.NoError(t, err)
	require.NotNil(t, demoFromDB)

	assert.Equal(t, *demosGeneratedAnalyzed[0].ID, *demoFromDB.ID)
	assert.Equal(t, *demosGeneratedAnalyzed[0].Sharecode, *demoFromDB.Sharecode)
	assert.Equal(t, *demosGeneratedAnalyzed[0].VersionAnalyzer, *demoFromDB.VersionAnalyzer)
	assert.Equal(t, *demosGeneratedAnalyzed[0].UserID, *demoFromDB.UserID)
	assert.Equal(t, demosGeneratedAnalyzed[0].Date.UnixNano(), demoFromDB.Date.UnixNano())
	assert.Equal(t, *demosGeneratedAnalyzed[0].Path, *demoFromDB.Path)
	assert.Equal(t, *demosGeneratedAnalyzed[0].Name, *demoFromDB.Name)
	assert.Equal(t, *demosGeneratedAnalyzed[0].Size, *demoFromDB.Size)
	assert.Equal(t, *demosGeneratedAnalyzed[0].ReplayURL, *demoFromDB.ReplayURL)
}
