package dao_test

import (
	"math/rand"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoDemo_SetLastVersionAnalyzer(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	version := rand.Int63()

	demoDAO := dao.NewDemoDAO(localDB, logTest)
	require.NoError(t, demoDAO.SetLastVersionAnalyzer(*demosGenerated[0].ID, version))

	demoFromDB := model.Demo{
		ID: demosGenerated[0].ID,
	}
	assert.NoError(t, demoDAO.Get(&demoFromDB))
	assert.Equal(t, version, *demoFromDB.VersionAnalyzer)

	assert.Error(t, demoDAO.SetLastVersionAnalyzer(646846345, version))
}
