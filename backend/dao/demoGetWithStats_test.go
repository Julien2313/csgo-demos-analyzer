package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoDemo_GetWithStats(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	demoFromDB := model.Demo{
		ID: demosGeneratedAnalyzed[0].ID,
	}

	demoDAO := dao.NewDemoDAO(localDB, logTest)
	assert.Error(t, demoDAO.GetWithStats(&model.Demo{}))
	require.NoError(t, demoDAO.GetWithStats(&demoFromDB))
	assert.NotNil(t, demoFromDB.TeamsStats)
	require.Len(t, demoFromDB.TeamsStats, 2)

	require.Len(t, demoFromDB.TeamsStats[0].PlayersStats, 5)

	for numPlayer, player := range demoFromDB.TeamsStats[0].PlayersStats {
		assert.Equal(t,
			*demosGeneratedAnalyzed[0].TeamsStats[0].PlayersStats[numPlayer].Rank,
			*player.Rank)
		assert.Equal(t,
			*demosGeneratedAnalyzed[0].TeamsStats[0].PlayersStats[numPlayer].Kills,
			*player.Kills)
		assert.Equal(t,
			*demosGeneratedAnalyzed[0].TeamsStats[0].PlayersStats[numPlayer].Deaths,
			*player.Deaths)
		assert.Equal(t,
			*demosGeneratedAnalyzed[0].TeamsStats[0].PlayersStats[numPlayer].Assists,
			*player.Assists)
		assert.Equal(t,
			*demosGeneratedAnalyzed[0].TeamsStats[0].PlayersStats[numPlayer].SteamID,
			*player.SteamID)
	}

	require.Len(t, demoFromDB.TeamsStats[1].PlayersStats, 5)

	for numPlayer, player := range demoFromDB.TeamsStats[1].PlayersStats {
		assert.Equal(t,
			*demosGeneratedAnalyzed[0].TeamsStats[1].PlayersStats[numPlayer].Rank,
			*player.Rank)
		assert.Equal(t,
			*demosGeneratedAnalyzed[0].TeamsStats[1].PlayersStats[numPlayer].Kills,
			*player.Kills)
		assert.Equal(t,
			*demosGeneratedAnalyzed[0].TeamsStats[1].PlayersStats[numPlayer].Deaths,
			*player.Deaths)
		assert.Equal(t,
			*demosGeneratedAnalyzed[0].TeamsStats[1].PlayersStats[numPlayer].Assists,
			*player.Assists)
		assert.Equal(t,
			*demosGeneratedAnalyzed[0].TeamsStats[1].PlayersStats[numPlayer].SteamID,
			*player.SteamID)
	}
}
