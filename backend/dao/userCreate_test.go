package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoUser_Create(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	userDAO := dao.NewUserDAO(localDB)
	user := model.GenerateRandomUser(true)
	require.NoError(t, userDAO.Create(user))

	userFromDB := model.User{
		ID: user.ID,
	}

	require.NoError(t, userDAO.Get(&userFromDB))
	assert.NotNil(t, userFromDB.ID)
	assert.Equal(t, *userFromDB.ID, *user.ID)
	assert.Equal(t, *userFromDB.Mail, *user.Mail)
	assert.Len(t, user.Demos, 0)
}

func TestDaoUser_CreateExists(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	userDAO := dao.NewUserDAO(localDB)
	user := model.GenerateRandomUser(true)
	assert.NoError(t, userDAO.Create(user))
	assert.Error(t, userDAO.Create(user))
}

func TestDaoUser_CreateNOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	userDAO := dao.NewUserDAO(localDB)
	user := model.GenerateRandomUser(true)
	user.Mail = nil
	assert.Error(t, userDAO.Create(user))

	user = model.GenerateRandomUser(true)
	user.Password = nil
	assert.Error(t, userDAO.Create(user))
}
