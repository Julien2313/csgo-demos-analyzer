package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoUser_GetUsersWhoEnabledDownloadInfo(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	userDAO := dao.NewUserDAO(localDB)
	users := &[]*model.User{}
	require.NoError(t, userDAO.GetUsersWhoEnabledDownloadInfo(users))
	num := len(*users)

	user := model.GenerateRandomUser(true)
	user.AccessTokenHistory = nil
	require.NoError(t, userDAO.Create(user))
	user = model.GenerateRandomUser(true)
	user.ShareCode = nil
	require.NoError(t, userDAO.Create(user))
	user = model.GenerateRandomUser(true)
	user.SteamID = nil
	require.NoError(t, userDAO.Create(user))

	require.NoError(t, userDAO.GetUsersWhoEnabledDownloadInfo(users))
	assert.Len(t, *users, num)

	_ = model.StoreGeneratedUser(localDB)

	require.NoError(t, userDAO.GetUsersWhoEnabledDownloadInfo(users))
	assert.Len(t, *users, num+1)
}
