package dao_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDeathMatchDemo_SaveAnalyze(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	dmDAO := dao.NewDemoDeathMatchDAO(localDB)

	demoDB := model.GenerateRandomDeathMatchDemo(*userGenerated.ID, *userGenerated.SteamID, time.Now())
	demoNaked := demoDB
	demoNaked.DeathMatchPlayers = nil
	require.NoError(t, localDB.Create(&demoNaked).Error)
	demoDB.ID = demoNaked.ID
	require.NoError(t, dmDAO.SaveAnalyze(demoDB))
	exists, err := dmDAO.Exists(demoDB)
	require.NoError(t, err)
	require.True(t, exists)
}
