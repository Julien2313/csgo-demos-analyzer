package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
)

func TestDaoToken_GetAllFromUser(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	tokenDAO := dao.NewTokenUploadDemoDAO(localDB)

	tokens, err := tokenDAO.GetAllFromUser(*userGenerated.ID)
	require.NoError(t, err)
	assert.Len(t, tokens, nbrTokenGenerated)
}
