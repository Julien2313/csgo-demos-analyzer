package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
)

func TestDaoDemo_GetAllNotDownloaded(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	cptDemoNotDownloaded := 0
	for _, demo := range demosGenerated {
		if !*demo.Downloaded {
			cptDemoNotDownloaded++
		}
	}

	demoDAO := dao.NewDemoDAO(localDB, logTest)
	demos, err := demoDAO.GetAllNotDownloaded(nil)
	require.NoError(t, err)

	cptDemoGetNotDownloaded := 0
	for _, demo := range demos {
		if !*demo.Downloaded && *demo.User.ID == *userGenerated.ID {
			cptDemoGetNotDownloaded++
		}
	}

	assert.Equal(t, cptDemoNotDownloaded, cptDemoGetNotDownloaded)
}
