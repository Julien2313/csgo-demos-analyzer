package dao_test

import (
	"log"
	"math/rand"
	"os"
	"sort"
	"testing"
	"time"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var logTest *custlogger.Logger

var _testDBGlobal *gorm.DB
var fastLogger logger.Interface

var userGenerated *model.User
var userGenerated2 *model.User
var userGeneratedVirgin *model.User
var userGeneratedVirgin2 *model.User

var demosGenerated []*model.Demo
var demosGeneratedNotStored *model.Demo
var demosGeneratedOutAnalyzed []*model.Demo
var demosGeneratedNotAnalyzed *model.Demo
var demosGeneratedAnalyzed []*model.Demo

const nbrDemosGenerated = 5
const nbrDemosGeneratedOutAnalyzed = 7

var currentVersionAnalyzer = int64(*helpers.RandUIntPSQL())

var deathMatchDemosGenerated []model.DeathMatchDemo
var deathMatchDemosGenerated2 []model.DeathMatchDemo
var deathMatchDemosGenerated2OutAnalyzed []model.DeathMatchDemo
var deathMatchDemosGeneratedNotStored []model.DeathMatchDemo

const nbrDeathMatchDemosGenerated = 5
const nbrDeathMatchDemosGenerated2OutAnalyzed = 5

var tokenGenerated []model.TokenUploadDemo
var tokenGeneratedNotStored []model.TokenUploadDemo
var tokenGenerated2 []model.TokenUploadDemo

const nbrTokenGenerated = 10

func TestMain(m *testing.M) {
	rand.Seed(time.Now().Unix())

	logTest = &custlogger.Logger{}
	logTest.SetupInstanceLogggerTest()

	_testDBGlobal = store.InitDB(true, store.SchemaTestDao, logTest)
	fastLogger = logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			LogLevel: logger.Silent,
		},
	)

	userGenerated = model.StoreGeneratedUser(_testDBGlobal)
	userGenerated2 = model.StoreGeneratedUser(_testDBGlobal)
	userGeneratedVirgin = model.StoreGeneratedUser(_testDBGlobal)
	userGeneratedVirgin2 = model.StoreGeneratedUser(_testDBGlobal)

	demoDAO := dao.NewDemoDAO(_testDBGlobal, logTest)

	demo := model.GenerateRandomDemoWithStats(userGenerated2, *userGenerated2.SteamID, 1)
	if err := demoDAO.Create(demo, &fastLogger); err != nil {
		panic(err)
	}

	demosGeneratedNotStored = model.GenerateRandomDemoWithStats(userGenerated2, *userGenerated2.SteamID, 1)
	demosGeneratedNotAnalyzed = model.GenerateRandomDemo(userGenerated2, *userGenerated2.SteamID)
	if err := demoDAO.Create(demosGeneratedNotAnalyzed, &fastLogger); err != nil {
		panic(err)
	}

	for numDemo := 0; numDemo < nbrDemosGenerated; numDemo++ {
		demo = model.GenerateRandomDemoWithStats(userGenerated, *userGenerated.SteamID, 1)
		if err := demoDAO.Create(demo, &fastLogger); err != nil {
			panic(err)
		}

		demosGeneratedOutAnalyzed = append(demosGeneratedOutAnalyzed, demo)

		demo = model.GenerateRandomDemoWithStats(userGenerated, *userGenerated.SteamID, currentVersionAnalyzer)
		if err := demoDAO.Create(demo, &fastLogger); err != nil {
			panic(err)
		}

		demosGeneratedAnalyzed = append(demosGeneratedAnalyzed, demo)
	}

	deathMatchDemosGenerated = model.GenerateRandomsDeathMatchDemo(
		*userGenerated.ID,
		*userGenerated.SteamID,
		time.Now(),
		nbrDemosGenerated,
	)
	if err := _testDBGlobal.Create(deathMatchDemosGenerated).Error; err != nil {
		panic(err)
	}
	deathMatchDemosGeneratedNotStored = model.GenerateRandomsDeathMatchDemo(
		*userGenerated.ID,
		*userGenerated.SteamID,
		time.Now(),
		1,
	)
	deathMatchDemosGenerated2 = model.GenerateRandomsDeathMatchDemo(
		*userGenerated2.ID,
		*userGenerated2.SteamID,
		time.Now(),
		1,
	)
	if err := _testDBGlobal.Create(deathMatchDemosGenerated2).Error; err != nil {
		panic(err)
	}
	deathMatchDemosGenerated2OutAnalyzed = model.GenerateRandomsDeathMatchDemo(
		*userGenerated2.ID,
		*userGenerated2.SteamID,
		time.Now(),
		5,
	)
	for numDemo := range deathMatchDemosGenerated2OutAnalyzed {
		deathMatchDemosGenerated2OutAnalyzed[numDemo].VersionAnalyzer = helpers.Int64Ptr(0)
	}
	if err := _testDBGlobal.Create(deathMatchDemosGenerated2OutAnalyzed).Error; err != nil {
		panic(err)
	}

	sort.Slice(demosGeneratedOutAnalyzed, func(i, j int) bool {
		return demosGeneratedOutAnalyzed[i].Date.After(*demosGeneratedOutAnalyzed[j].Date)
	})
	sort.Slice(demosGeneratedAnalyzed, func(i, j int) bool {
		return demosGeneratedAnalyzed[i].Date.After(*demosGeneratedAnalyzed[j].Date)
	})

	demosGenerated = append(demosGeneratedAnalyzed, demosGeneratedOutAnalyzed...)
	sort.Slice(demosGenerated, func(i, j int) bool {
		return demosGenerated[i].Date.After(*demosGenerated[j].Date)
	})

	tokenGeneratedNotStored = model.GenerateRandomTokens(*userGenerated.ID, 10)
	tokenGenerated = model.GenerateRandomTokens(*userGenerated.ID, 10)
	if err := _testDBGlobal.Create(tokenGenerated).Error; err != nil {
		panic(err)
	}
	tokenGenerated2 = model.GenerateRandomTokens(*userGenerated2.ID, 10)
	if err := _testDBGlobal.Create(tokenGenerated2).Error; err != nil {
		panic(err)
	}

	os.Exit(m.Run())
}
