package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoToken_UpdateUse(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	tokenDAO := dao.NewTokenUploadDemoDAO(localDB)

	require.NoError(t, tokenDAO.UpdateUse(&tokenGenerated[0]))
	tokenFromDB := &model.TokenUploadDemo{
		ID: tokenGenerated[0].ID,
	}

	require.NoError(t, localDB.Find(tokenFromDB).Error)
	assert.NotNil(t, tokenFromDB.LastUsed)
}
