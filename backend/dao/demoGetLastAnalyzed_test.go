package dao_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
)

func TestDaoDemo_GetLastAnalyzed(t *testing.T) {
	if os.Getenv("GITLAB") != "" {
		t.Skip()
	}
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	demoDAO := dao.NewDemoDAO(localDB, logTest)
	demosResp, err := demoDAO.GetLastAnalyzed(currentVersionAnalyzer)
	require.NoError(t, err)
	require.Len(t, demosResp, nbrDemosGenerated)
}
