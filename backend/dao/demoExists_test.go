package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoDemo_Exists(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	demoDAO := dao.NewDemoDAO(localDB, logTest)

	exists, err := demoDAO.Exists(*demosGenerated[0])
	assert.True(t, exists)
	assert.NoError(t, err)

	exists, err = demoDAO.Exists(model.Demo{})
	assert.False(t, exists)
	assert.Error(t, err)
}
