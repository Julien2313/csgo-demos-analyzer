package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoDemo_Create(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	demo := model.GenerateRandomDemo(userGenerated, *userGenerated.SteamID)
	demoDAO := dao.NewDemoDAO(localDB, logTest)
	require.NoError(t, demoDAO.Create(demo, &fastLogger))

	demoFromDB := model.Demo{
		ID: demo.ID,
	}
	require.NoError(t, demoDAO.Get(&demoFromDB))
	assert.NotNil(t, demoFromDB.ID)
	assert.Equal(t, *demoFromDB.ID, *demo.ID)
	assert.Equal(t, *demoFromDB.Name, *demo.Name)
	assert.Equal(t, *demoFromDB.Size, *demo.Size)
}

func TestDaoDemo_CreateExists(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	demoDAO := dao.NewDemoDAO(localDB, logTest)

	assert.Error(t, demoDAO.Create(demosGenerated[0], &fastLogger))
}

func TestDaoDemo_CreateNOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	demoDAO := dao.NewDemoDAO(localDB, logTest)

	demo := model.GenerateRandomDemo(userGenerated, *userGenerated.SteamID)
	name := *demo.Name
	demo.Name = nil
	assert.Error(t, demoDAO.Create(demo, &fastLogger))
	demo.Name = &name

	size := *demo.Size
	demo.Size = nil
	assert.Error(t, demoDAO.Create(demo, &fastLogger))
	demo.Size = &size

	userID := *demo.UserID
	demo.UserID = nil
	assert.Error(t, demoDAO.Create(demo, &fastLogger))
	demo.UserID = &userID
}
