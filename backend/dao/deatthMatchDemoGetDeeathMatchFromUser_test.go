package dao_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
)

func TestDeathMatchDemo_GetDeathMatchFromUser(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	dmDAO := dao.NewDemoDeathMatchDAO(localDB)
	demos, err := dmDAO.GetDeathMatchFromUser(*userGenerated)
	require.NoError(t, err)
	require.Len(t, demos, nbrDeathMatchDemosGenerated)
}
