package dao

import (
	"errors"
	"time"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gorm.io/gorm"
)

// TokenUploadDemoDAO the doa of the demos deathmatch
type TokenUploadDemoDAO struct {
	dataSource *gorm.DB
}

// NewTokenUploadDemoDAO constructor for the demo DAO
func NewTokenUploadDemoDAO(dataSource *gorm.DB) TokenUploadDemoDAO {
	return TokenUploadDemoDAO{
		dataSource: dataSource,
	}
}

// Exists return true if the token exists
func (dao TokenUploadDemoDAO) Exists(token model.TokenUploadDemo) (bool, error) {
	tmpToken := model.TokenUploadDemo{}

	if token.Token == nil || token.IPAllowed == nil {
		return false, ErrNotEnoughData
	}

	if err := dao.dataSource.
		Where("token = ? AND ip_allowed = ?", *token.Token, token.IPAllowed).
		First(&tmpToken).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return false, nil
		}

		return false, err
	}

	return tmpToken.ID != nil, nil
}

// Create add the token in the DB
func (dao TokenUploadDemoDAO) Create(token *model.TokenUploadDemo) error {
	return dao.dataSource.Create(token).Error
}

// Get get the token from the DB
func (dao TokenUploadDemoDAO) Get(token *model.TokenUploadDemo) error {
	if token.Token == nil {
		return ErrNotEnoughData
	}

	return dao.dataSource.Where("token = ?", *token.Token).First(token).Error
}

// UpdateUse update the fields lastUsed
func (dao TokenUploadDemoDAO) UpdateUse(token *model.TokenUploadDemo) error {
	if token.ID == nil {
		return ErrNotEnoughData
	}

	return dao.dataSource.
		Model(&model.TokenUploadDemo{}).
		Where("id = ?", *token.ID).
		Update("last_used", time.Now()).
		Error
}

// GetAllFromUser get the tokens from the DB from an user
func (dao TokenUploadDemoDAO) GetAllFromUser(userID uint) ([]model.TokenUploadDemo, error) {
	var tokens []model.TokenUploadDemo

	return tokens, dao.dataSource.Where("user_id = ?", userID).Find(&tokens).Error
}
