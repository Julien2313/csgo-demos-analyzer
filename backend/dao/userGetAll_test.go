package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoUser_GetAll(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	userDAO := dao.NewUserDAO(localDB)
	users := []*model.User{}
	require.NoError(t, userDAO.GetAll(&users))
	nbrUserInDB := len(users)

	for nbrUser := 0; nbrUser < 10; nbrUser++ {
		_ = model.StoreGeneratedUser(localDB)
	}

	assert.NoError(t, userDAO.GetAll(&users))
	assert.Len(t, users, 10+nbrUserInDB)
}
