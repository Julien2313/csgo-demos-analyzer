package dao

import (
	"errors"
	"math"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gorm.io/gorm"
)

// UserDAO the doa of the user
type UserDAO struct {
	dataSource *gorm.DB
}

// NewUserDAO constructor for the user DAO
func NewUserDAO(dataSource *gorm.DB) UserDAO {
	return UserDAO{
		dataSource: dataSource,
	}
}

// GetUserFromContext return the user of the user's token from the DB
func (dao UserDAO) GetUserFromContext(c *gin.Context) (*model.User, error) {
	userID, err := helpers.GetUserIDFromContext(c)
	if err != nil {
		return nil, err
	}

	user := &model.User{
		ID: userID,
	}
	if err := dao.Get(user); err != nil {
		return nil, err
	}

	return user, nil
}

// GetUserByMail return the user from its mail
func (dao UserDAO) GetUserByMail(user *model.User) error {
	if user.Mail == nil {
		return ErrNotEnoughData
	}

	*user.Mail = strings.ToUpper(*user.Mail)

	return dao.dataSource.Where("upper(Mail) = upper(?)", *user.Mail).First(user).Error
}

// CountMemoryUsed count all the memory used by one user
func (dao UserDAO) CountMemoryUsed(user model.User) (size int64, err error) {
	if user.SteamID == nil {
		return math.MaxInt64, ErrNotEnoughData
	}

	type Result struct {
		Total int64
	}

	result := Result{}

	return result.Total, dao.dataSource.Table("demos").
		Where("? IN (?)", *user.SteamID,
			dao.dataSource.Model(&model.SteamID{}).Select("steam_id").Where("demo_id = demos.id")).
		Select("sum(size) as total").Scan(&result).
		Error
}

// AddMemoryUsed add the current memory used by the new demo to the user
func (dao UserDAO) AddMemoryUsed(user model.User, size float64) error {
	if user.ID != nil {
		return dao.dataSource.
			Model(&model.User{}).
			Where("id = ?", *user.ID).
			Update("size_used", gorm.Expr("size_used + ?", size)).Error
	}

	if user.Mail != nil {
		return dao.dataSource.
			Model(&model.User{}).
			Where("upper(Mail) = upper(?)", *user.Mail).
			Update("size_used", gorm.Expr("size_used + ?", size)).Error
	}

	if user.SteamID != nil {
		return dao.dataSource.
			Model(&model.User{}).
			Where("steam_id = ?", *user.SteamID).
			Update("size_used", gorm.Expr("size_used + ?", size)).Error
	}

	return ErrNotEnoughData
}

// SetMemoryUsed set the current memory used to the user
func (dao UserDAO) SetMemoryUsed(user model.User, size float64) error {
	if user.ID != nil {
		return dao.dataSource.
			Model(&model.User{}).
			Where("id = ?", *user.ID).
			Update("size_used", size).Error
	}

	if user.Mail != nil {
		return dao.dataSource.
			Model(&model.User{}).
			Where("upper(Mail) = upper(?)", *user.Mail).
			Update("size_used", size).Error
	}

	if user.SteamID != nil {
		return dao.dataSource.
			Model(&model.User{}).
			Where("steam_id = ?", *user.SteamID).
			Update("size_used", size).Error
	}

	return ErrNotEnoughData
}

// SetAPIValveWorking set if the data for the api is ok or not for the user
func (dao UserDAO) SetAPIValveWorking(user model.User, state bool) error {
	if user.ID != nil {
		return dao.dataSource.
			Model(&model.User{}).
			Where("id = ?", *user.ID).
			Update("api_valve_working", state).Error
	}

	if user.Mail != nil {
		return dao.dataSource.
			Model(&model.User{}).
			Where("upper(Mail) = upper(?)", *user.Mail).
			Update("api_valve_working", state).Error
	}

	if user.SteamID != nil {
		return dao.dataSource.
			Model(&model.User{}).
			Where("steam_id = ?", *user.SteamID).
			Update("api_valve_working", state).Error
	}

	return ErrNotEnoughData
}

// GetUsersWhoEnabledDownloadInfo return users who gave permissions to download info
func (dao UserDAO) GetUsersWhoEnabledDownloadInfo(users *[]*model.User) error {
	return dao.dataSource.Where("steam_ID != '' AND share_code != '' AND access_token_history != ''").Find(users).Error
}

// GetAll return all users
func (dao UserDAO) GetAll(users *[]*model.User) error {
	return dao.dataSource.Find(users).Error
}

// Exists return true if the user already exists
func (dao UserDAO) Exists(user model.User) (bool, error) {
	tmpUser := model.User{
		ID:      user.ID,
		Mail:    user.Mail,
		SteamID: user.SteamID,
	}

	switch {
	case tmpUser.ID != nil:
		if err := dao.dataSource.Where("id = ?", *tmpUser.ID).First(&tmpUser).Error; err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				return false, nil
			}

			return false, err
		}
	case tmpUser.SteamID != nil:
		if err := dao.dataSource.Where("steam_id = ?", *user.SteamID).First(&tmpUser).Error; err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				return false, nil
			}

			return false, err
		}
	case tmpUser.Mail != nil:
		if err := dao.dataSource.Where("upper(Mail) = upper(?)", *user.Mail).First(&tmpUser).Error; err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				return false, nil
			}

			return false, err
		}
	default:
		return false, ErrNotEnoughData
	}

	return tmpUser.ID != nil, nil
}

// Create crée un nouvel utilisateur
func (dao UserDAO) Create(user *model.User) error {
	if user.Mail != nil {
		*user.Mail = strings.ToUpper(*user.Mail)
	}

	return dao.dataSource.Create(user).Error
}

// Get Retourne l'utilisateur trouvé par son ID
func (dao *UserDAO) Get(user *model.User) error {
	if user.ID != nil {
		return dao.dataSource.
			First(&user, *user.ID).
			Error
	}

	if user.Mail != nil {
		return dao.dataSource.
			Where("upper(Mail) = upper(?)", *user.Mail).
			Find(&user).
			Error
	}

	if user.SteamID != nil {
		return dao.dataSource.
			Where("steam_id = ?", *user.SteamID).
			Find(&user).
			Error
	}

	return ErrNotEnoughData
}

// UpdateSteamID update le steamID de l'utilisateur
func (dao *UserDAO) UpdateSteamID(user *model.User) error {
	if user.SteamID == nil || user.ID == nil {
		return ErrNotEnoughData
	}

	return dao.dataSource.
		Model(&user).
		Update("steam_id", *user.SteamID).
		Error
}

// UpdateShareCode update le sharecode de l'utilisateur
func (dao *UserDAO) UpdateShareCode(user *model.User) error {
	if user.ShareCode == nil || user.ID == nil {
		return ErrNotEnoughData
	}

	return dao.dataSource.
		Model(&user).
		Update("share_code", *user.ShareCode).
		Error
}

// UpdateAccessTokenHistory update l'access token de l'utilisateur
func (dao *UserDAO) UpdateAccessTokenHistory(user *model.User) error {
	if user.AccessTokenHistory == nil || user.ID == nil {
		return ErrNotEnoughData
	}

	return dao.dataSource.
		Model(&user).
		Update("access_token_history", *user.AccessTokenHistory).
		Error
}
