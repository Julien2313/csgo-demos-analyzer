package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
)

func TestDaoDemo_GetAllFromUser(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	demoDAO := dao.NewDemoDAO(localDB, logTest)
	demosResp, err := demoDAO.GetAllFromUser(*userGenerated.SteamID, nil, nil, nil, nil)
	require.NoError(t, err)
	require.Len(t, demosResp, nbrDemosGenerated*2)

	for numDemo := 0; numDemo < nbrDemosGenerated*2; numDemo++ {
		assert.Equal(t, *demosGenerated[numDemo].ID, *demosResp[numDemo].ID)
		assert.Equal(t, *demosGenerated[numDemo].VersionAnalyzer, *demosResp[numDemo].VersionAnalyzer)
		assert.Nil(t, demosResp[numDemo].User)
		assert.Equal(t, demosGenerated[numDemo].Date.UnixNano(), demosResp[numDemo].Date.UnixNano())
		assert.Equal(t, *demosGenerated[numDemo].Path, *demosResp[numDemo].Path)
		assert.Equal(t, *demosGenerated[numDemo].Name, *demosResp[numDemo].Name)
		assert.Equal(t, *demosGenerated[numDemo].Size, *demosResp[numDemo].Size)
		assert.Equal(t, *demosGenerated[numDemo].ReplayURL, *demosResp[numDemo].ReplayURL)
	}
}
