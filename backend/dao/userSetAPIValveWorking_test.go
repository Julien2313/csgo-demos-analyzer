package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoUser_SetAPIValveWorking(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	userDAO := dao.NewUserDAO(localDB)
	user := model.GenerateRandomUser(true)
	user.APIValveWorking = helpers.BoolPtr(false)
	require.NoError(t, userDAO.Create(user))
	require.NoError(t, userDAO.SetAPIValveWorking(*user, true))

	userFromDB := model.User{
		SteamID: user.SteamID,
	}
	require.NoError(t, userDAO.Get(&userFromDB))
	assert.True(t, *userFromDB.APIValveWorking)

	user.ID = nil
	assert.NoError(t, userDAO.SetAPIValveWorking(*user, false))

	require.NoError(t, userDAO.Get(&userFromDB))
	assert.False(t, *userFromDB.APIValveWorking)

	user.Mail = nil
	assert.NoError(t, userDAO.SetAPIValveWorking(*user, true))

	require.NoError(t, userDAO.Get(&userFromDB))
	assert.True(t, *userFromDB.APIValveWorking)

	assert.Error(t, userDAO.SetAPIValveWorking(model.User{}, true))
}
