package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
)

func TestDaoDemo_IsAnalyzed(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	demoDAO := dao.NewDemoDAO(localDB, logTest)
	for numDemo := 0; numDemo < nbrDemosGenerated*2; numDemo++ {
		isAnalyzed, err := demoDAO.IsAnalyzed(*demosGenerated[numDemo].ID)
		require.NoError(t, err)
		assert.Equal(t, *demosGenerated[numDemo].VersionAnalyzer > 0, isAnalyzed)
	}
}
