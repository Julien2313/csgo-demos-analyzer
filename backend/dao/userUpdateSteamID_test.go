package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoUser_UpdateSteamID(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	userDAO := dao.NewUserDAO(localDB)
	user := model.GenerateRandomUser(true)
	require.NoError(t, userDAO.Create(user))

	require.NoError(t, userDAO.UpdateSteamID(user))

	userFromDB := model.User{
		ID: user.ID,
	}
	require.NoError(t, localDB.First(&userFromDB).Error)
	assert.Equal(t, *user.SteamID, *userFromDB.SteamID)

	user.SteamID = nil
	assert.Error(t, userDAO.UpdateSteamID(user))
}
