package dao_test

import (
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoUser_CountMemoryUsed(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	userDAO := dao.NewUserDAO(localDB)
	demoDAO := dao.NewDemoDAO(localDB, logTest)

	// no demo
	size, err := userDAO.CountMemoryUsed(*userGeneratedVirgin)
	require.NoError(t, err)
	assert.Equal(t, int64(0), size)

	// no user
	size, err = userDAO.CountMemoryUsed(model.User{})
	assert.Error(t, err)
	assert.Equal(t, int64(math.MaxInt64), size)

	// One demo
	demo := model.GenerateRandomDemo(userGeneratedVirgin, *userGeneratedVirgin.SteamID)
	demo.AddSteamsIDs(*userGeneratedVirgin.SteamID, 1)
	require.NoError(t, demoDAO.Create(demo, &fastLogger))

	size, err = userDAO.CountMemoryUsed(*userGeneratedVirgin)
	require.NoError(t, err)
	assert.Equal(t, *demo.Size, size)

	// two demos
	demo2 := model.GenerateRandomDemo(userGeneratedVirgin, *userGeneratedVirgin.SteamID)
	demo2.AddSteamsIDs(*userGeneratedVirgin.SteamID, 1)
	require.NoError(t, demoDAO.Create(demo2, &fastLogger))

	size, err = userDAO.CountMemoryUsed(*userGeneratedVirgin)

	require.NoError(t, err)
	assert.Equal(t, *demo.Size+*demo2.Size, size)

	// new user
	demo3 := model.GenerateRandomDemo(userGeneratedVirgin2, *userGeneratedVirgin2.SteamID)
	demo3.AddSteamsIDs(*userGeneratedVirgin2.SteamID, 1)
	require.NoError(t, demoDAO.Create(demo3, &fastLogger))

	// only one demo for him
	size, err = userDAO.CountMemoryUsed(*userGeneratedVirgin2)
	require.NoError(t, err)
	assert.Equal(t, *demo3.Size, size)

	size, err = userDAO.CountMemoryUsed(*userGeneratedVirgin)
	require.NoError(t, err)
	assert.Equal(t, *demo.Size+*demo2.Size, size)
}
