package dao_test

import (
	"testing"
	"time"

	"github.com/maxatome/go-testdeep/td"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDeathMatchDemo_Create(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	demo := model.GenerateRandomDeathMatchDemo(*userGenerated.ID, *userGenerated.SteamID, time.Now())
	dmDAO := dao.NewDemoDeathMatchDAO(localDB)
	require.NoError(t, dmDAO.Create(&demo))

	demosFromDB := &model.DeathMatchDemo{ID: demo.ID}
	require.NoError(t, dmDAO.Get(demosFromDB))

	demo.CreatedAt = demosFromDB.CreatedAt
	demo.UpdatedAt = demosFromDB.UpdatedAt
	demo.Date = demosFromDB.Date

	td.Cmp(t, *demosFromDB, demo)

}
