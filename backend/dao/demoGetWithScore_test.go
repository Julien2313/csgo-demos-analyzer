package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoDemo_GetWithScore(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	demoFromDB := model.Demo{
		ID: demosGeneratedAnalyzed[0].ID,
	}

	demoDAO := dao.NewDemoDAO(localDB, logTest)
	assert.Error(t, demoDAO.GetWithScore(&model.Demo{}, ""))
	require.NoError(t, demoDAO.GetWithScore(&demoFromDB, *userGenerated.SteamID))

	require.NotNil(t, demoFromDB.TeamsStats)
	require.Len(t, demoFromDB.TeamsStats, 2)

	assert.Equal(t, 1, len(demoFromDB.TeamsStats[0].PlayersStats)+len(demoFromDB.TeamsStats[1].PlayersStats))
}
