package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoDemo_GetAllNotClassified(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	cptDemoNotClassified := 0
	for _, demo := range demosGenerated {
		if demo.GameMode == model.UnknownGameMode {
			cptDemoNotClassified++
		}
	}

	demoDAO := dao.NewDemoDAO(localDB, logTest)
	demos, err := demoDAO.GetAllNotClassified()
	require.NoError(t, err)

	assert.Len(t, demos, cptDemoNotClassified)
}
