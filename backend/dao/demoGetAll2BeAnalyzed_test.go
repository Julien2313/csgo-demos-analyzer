package dao_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

// @randomFailOnGitlab
func TestDaoDemo_GetAll2BeAnalyzed(t *testing.T) {
	if os.Getenv("GITLAB") != "" {
		t.Skip()
	}
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	demoDAO := dao.NewDemoDAO(localDB, logTest)
	demos, err := demoDAO.GetAll2BeAnalyzed(currentVersionAnalyzer, model.Competitive)
	require.NoError(t, err)

	// ------------------
	nbrDemos := 0 // this is because of a bug on gitlab-ci, idk why but user created on other schema are queried here
	for _, demo := range demos {
		if *demo.UserID == *userGenerated.ID ||
			*demo.UserID == *userGenerated2.ID ||
			*demo.UserID == *userGeneratedVirgin.ID ||
			*demo.UserID == *userGeneratedVirgin2.ID {
			nbrDemos++
		}
	}

	assert.Equal(t, nbrDemos, nbrDemosGeneratedOutAnalyzed)
	// ------------------
	// assert.len(t, demo, nbrDemosGeneratedOutAnalyzed)
}
