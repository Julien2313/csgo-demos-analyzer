package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoUser_AddMemoryUsed(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	userDAO := dao.NewUserDAO(localDB)
	user := model.StoreGeneratedUser(localDB)
	sizeNewDemo := 65.41684
	require.NoError(t, userDAO.AddMemoryUsed(*user, sizeNewDemo))

	userFromDB := model.User{
		SteamID: user.SteamID,
	}
	require.NoError(t, userDAO.Get(&userFromDB))
	assert.Equal(t, helpers.Round(sizeNewDemo+*user.SizeUsed, 4), helpers.Round(*userFromDB.SizeUsed, 4))

	user.ID = nil
	require.NoError(t, userDAO.AddMemoryUsed(*user, sizeNewDemo))
	require.NoError(t, userDAO.Get(&userFromDB))
	assert.Equal(t, helpers.Round(sizeNewDemo*2+*user.SizeUsed, 4), helpers.Round(*userFromDB.SizeUsed, 4))

	user.Mail = nil
	require.NoError(t, userDAO.AddMemoryUsed(*user, sizeNewDemo))

	require.NoError(t, userDAO.Get(&userFromDB))
	assert.Equal(t, helpers.Round(sizeNewDemo*3+*user.SizeUsed, 4), helpers.Round(*userFromDB.SizeUsed, 4))

	assert.Error(t, userDAO.AddMemoryUsed(model.User{}, 2))
}
