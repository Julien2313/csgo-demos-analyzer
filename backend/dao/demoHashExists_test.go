package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoDemo_HashExists(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	demoDAO := dao.NewDemoDAO(localDB, logTest)
	exists, demoFound, err := demoDAO.HashExists(*demosGenerated[0])
	assert.True(t, exists)
	assert.NoError(t, err)
	assert.NotNil(t, demoFound)

	exists, demoFound, err = demoDAO.HashExists(*demosGeneratedNotStored)
	assert.False(t, exists)
	assert.NoError(t, err)
	assert.Nil(t, demoFound)

	exists, demoFound, err = demoDAO.HashExists(model.Demo{})
	assert.False(t, exists)
	assert.Error(t, err)
	assert.Nil(t, demoFound)
}
