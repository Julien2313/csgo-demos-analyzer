package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gorm.io/gorm"
)

func TestDaoDemo_SaveAnalyzeOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	demoDAO := dao.NewDemoDAO(localDB, logTest)
	demoFromDB := model.Demo{ID: demosGenerated[0].ID}
	require.NoError(t, demoDAO.GetWithStats(&demoFromDB))

	require.Len(t, demoFromDB.TeamsStats, 2)
	for _, team := range demoFromDB.TeamsStats {
		require.Len(t, team.PlayersStats, 5)
		for _, player := range team.PlayersStats {
			assert.NotNil(t, player.ID)
			assert.NotNil(t, player.SteamID)
			assert.NotNil(t, player.Marks)
			assert.NotNil(t, player.Username)
			assert.NotNil(t, player.Rank)
			assert.NotNil(t, player.ScoreFacts)
			assert.NotNil(t, player.MVPsFacts)
			assert.NotNil(t, player.Kills)
			assert.NotNil(t, player.Deaths)
			assert.NotNil(t, player.Assists)
			assert.Len(t, player.MatrixKills, 10)
			assert.Len(t, player.MatrixFlashs, 10)
		}
	}
}

func TestDaoDemo_SaveAnalyzeNOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	demoDAO := dao.NewDemoDAO(localDB, logTest)
	demo := model.GenerateRandomDemo(userGenerated, *userGenerated.SteamID)

	assert.Error(t, demoDAO.SaveAnalyze(*demo))
	assert.Error(t, demoDAO.SaveAnalyze(model.Demo{}))
	assert.EqualError(t,
		demoDAO.SaveAnalyze(model.Demo{ID: helpers.Int64Ptr(int64(*helpers.RandUIntPSQL()))}),
		gorm.ErrRecordNotFound.Error())
}
