package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoUser_Exists(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	userDAO := dao.NewUserDAO(localDB)
	user := model.StoreGeneratedUser(localDB)

	exists, err := userDAO.Exists(*user)
	assert.True(t, exists)
	assert.NoError(t, err)

	user.ID = nil
	exists, err = userDAO.Exists(*user)
	assert.True(t, exists)
	assert.NoError(t, err)
	assert.Nil(t, user.ID)

	user.Mail = nil
	exists, err = userDAO.Exists(*user)
	assert.True(t, exists)
	assert.NoError(t, err)

	user.SteamID = nil
	exists, err = userDAO.Exists(*user)
	assert.False(t, exists)
	assert.Error(t, err)

	user = model.GenerateRandomUser(true)
	exists, err = userDAO.Exists(*user)
	assert.False(t, exists)
	assert.NoError(t, err)

	user.ID = nil
	exists, err = userDAO.Exists(*user)
	assert.False(t, exists)
	assert.NoError(t, err)

	user.SteamID = nil
	exists, err = userDAO.Exists(*user)
	assert.False(t, exists)
	assert.NoError(t, err)

	user.Mail = nil
	exists, err = userDAO.Exists(*user)
	assert.False(t, exists)
	assert.Error(t, err)
}
