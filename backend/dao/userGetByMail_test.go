package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoUser_GetByMail(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	userFromDB := model.User{
		Mail: userGenerated.Mail,
	}
	userDAO := dao.NewUserDAO(localDB)
	require.NoError(t, userDAO.GetUserByMail(&userFromDB))
	assert.Equal(t, *userGenerated.ID, *userFromDB.ID)

	userFromDB = model.User{
		Mail: helpers.StrPtr("noMail"),
	}
	assert.Error(t, userDAO.GetUserByMail(&userFromDB))
	assert.Error(t, userDAO.GetUserByMail(&model.User{}))
}
