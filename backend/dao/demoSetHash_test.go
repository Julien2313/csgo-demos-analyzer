package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoDemo_SetHash(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	hash := []byte(helpers.RandomString(16))

	demoDAO := dao.NewDemoDAO(localDB, logTest)
	require.NoError(t, demoDAO.SetHash(*demosGenerated[0].ID, hash))

	demoFromDB := model.Demo{
		ID: demosGenerated[0].ID,
	}
	assert.NoError(t, demoDAO.Get(&demoFromDB))
	assert.Equal(t, hash, demoFromDB.HashFile)

	assert.Error(t, demoDAO.SetHash(646846345, hash))
}
