package dao_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoDemo_SwapCompetitiveToDMDemo(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	fileDemoName := *demosGeneratedNotAnalyzed.Path
	fileDemo, err := os.Create(fileDemoName)
	require.NoError(t, err)
	defer func() {
		require.NoError(t, fileDemo.Close())
		require.NoError(t, os.Remove(fileDemoName))
	}()

	demoDAO := dao.NewDemoDAO(localDB, logTest)
	require.NoError(t, demoDAO.SwapCompetitiveToDMDemo(*demosGeneratedNotAnalyzed, fileDemoName))

	exists, err := demoDAO.Exists(*demosGeneratedNotAnalyzed)
	assert.NoError(t, err)
	assert.False(t, exists)

	deathmatchDemoDAO := dao.NewDemoDeathMatchDAO(localDB)
	exists, err = deathmatchDemoDAO.Exists(model.DeathMatchDemo{HashFile: demosGeneratedNotAnalyzed.HashFile})
	assert.NoError(t, err)
	assert.True(t, exists)

	deathMatchDemo := &model.DeathMatchDemo{ID: demosGeneratedNotAnalyzed.ID}
	require.NoError(t, deathmatchDemoDAO.Get(deathMatchDemo))

	assert.Equal(t, demosGeneratedNotAnalyzed.UserID, deathMatchDemo.UserID)
	assert.Equal(t, demosGeneratedNotAnalyzed.HashFile, deathMatchDemo.HashFile)
	assert.Equal(t, demosGeneratedNotAnalyzed.Path, deathMatchDemo.Path)
	assert.Equal(t, demosGeneratedNotAnalyzed.Name, deathMatchDemo.Name)
}
