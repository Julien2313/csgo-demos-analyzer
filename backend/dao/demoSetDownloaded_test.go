package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoDemo_SetDownloaded(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	demo := model.GenerateRandomDemo(userGenerated, *userGenerated.SteamID)
	demo.Downloaded = helpers.BoolPtr(false)
	demoDAO := dao.NewDemoDAO(localDB, logTest)
	require.NoError(t, demoDAO.Create(demo, &fastLogger))

	require.NoError(t, demoDAO.SetDownloaded(*demo.ID))

	demoFromDB := model.Demo{
		ID: demo.ID,
	}
	require.NoError(t, demoDAO.Get(&demoFromDB))
	assert.True(t, *demoFromDB.Downloaded)

	assert.Error(t, demoDAO.SetDownloaded(646846345))
}
