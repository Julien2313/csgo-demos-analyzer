package dao_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
)

// @randomFailOnGitlab
func TestDeathMatchDemo_GetAll2BeAnalyzed(t *testing.T) {
	if os.Getenv("GITLAB") != "" {
		t.Skip()
	}
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	dmDAO := dao.NewDemoDeathMatchDAO(localDB)
	demo2BeAnalyzed, err := dmDAO.GetAll2BeAnalyzed(1)
	require.NoError(t, err)
	require.Len(t, demo2BeAnalyzed, nbrDeathMatchDemosGenerated2OutAnalyzed)
}
