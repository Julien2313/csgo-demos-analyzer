package dao

import (
	"errors"
	"log"
	"os"
	"time"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"gorm.io/gorm/logger"
)

// DemoDeathMatchDAO the doa of the demos deathmatch
type DemoDeathMatchDAO struct {
	dataSource *gorm.DB
}

// NewDemoDeathMatchDAO constructor for the demo DAO
func NewDemoDeathMatchDAO(dataSource *gorm.DB) DemoDeathMatchDAO {
	return DemoDeathMatchDAO{
		dataSource: dataSource,
	}
}

// Exists return true if the demo already exists
func (dao DemoDeathMatchDAO) Exists(demo model.DeathMatchDemo) (bool, error) {
	tmpDemo := model.DeathMatchDemo{}

	if demo.HashFile == nil {
		return false, ErrNotEnoughData
	}

	if err := dao.dataSource.Where("hash_file = ?", demo.HashFile).First(&tmpDemo).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return false, nil
		}

		return false, err
	}

	return tmpDemo.ID != nil, nil
}

// Create add the demo in the DB
func (dao DemoDeathMatchDAO) Create(demo *model.DeathMatchDemo) error {
	return dao.dataSource.Create(demo).Error
}

// GetAll2BeAnalyzed get all the demos from the DB not analyzed or with an analyzer outdated
func (dao DemoDeathMatchDAO) GetAll2BeAnalyzed(analyzedVersion int64) (demos []model.DeathMatchDemo, err error) {
	return demos, dao.dataSource.
		Where("version_analyzer < ?", analyzedVersion).
		Find(&demos).Error
}

// Get get the demo from the DB
func (dao DemoDeathMatchDAO) Get(demo *model.DeathMatchDemo) error {
	if demo.ID == nil {
		return ErrNotEnoughData
	}

	return dao.dataSource.Where("id = ?", *demo.ID).First(demo).Error
}

// SaveAnalyze saves the analyze of the demo
func (dao DemoDeathMatchDAO) SaveAnalyze(demo model.DeathMatchDemo) error {
	if demo.ID == nil {
		return ErrNotEnoughData
	}

	if exists, err := dao.Exists(demo); err != nil {
		return err
	} else if !exists {
		return gorm.ErrRecordNotFound
	}

	fastLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold: 1000 * time.Millisecond,
			LogLevel:      logger.Error,
			Colorful:      false,
		},
	)
	if err := dao.dataSource.
		Clauses(clause.OnConflict{DoNothing: true}).
		Session(&gorm.Session{FullSaveAssociations: true, Logger: fastLogger}).Save(&demo).Error; err != nil {
		return err
	}

	return nil
}

// GetDeathMatchFromUser returns death match stats from player
func (dao DemoDeathMatchDAO) GetDeathMatchFromUser(user model.User) (demos []model.DeathMatchDemo, err error) {
	if user.SteamID == nil {
		return nil, ErrNotEnoughData
	}

	return demos, dao.dataSource.
		Order("date desc").
		Where("? IN (?)",
			*user.SteamID,
			dao.dataSource.Model(&model.DeathMatchPlayer{}).
				Select("steam_id").
				Where("death_match_demo_id = death_match_demos.id")).
		Preload("DeathMatchPlayers").
		Preload("DeathMatchPlayers.WeaponsStats").
		Find(&demos).
		Error
}
