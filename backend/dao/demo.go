package dao

import (
	"errors"
	"log"
	"os"
	"sort"
	"time"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"gorm.io/gorm/logger"
)

// DemoDAO the doa of the demos
type DemoDAO struct {
	dataSource *gorm.DB
	logDB      *custlogger.Logger
}

// NewDemoDAO constructor for the demo DAO
func NewDemoDAO(dataSource *gorm.DB, logger *custlogger.Logger) DemoDAO {
	return DemoDAO{
		dataSource: dataSource,
		logDB:      logger,
	}
}

// ErrNotEnoughData not enough data for the query
var ErrNotEnoughData = errors.New("not enough data for the query")

// ErrAlreadyAnalyzed demo already analyzed
var ErrAlreadyAnalyzed = errors.New("demo already analyzed")

// HashExists return true if the hash already exists
func (dao DemoDAO) HashExists(demo model.Demo) (bool, *model.Demo, error) {
	tmpDemo := model.Demo{}

	if demo.ID == nil || len(demo.HashFile) == 0 {
		return false, nil, ErrNotEnoughData
	}

	if err := dao.dataSource.Where("hash_file = ?", demo.HashFile).First(&tmpDemo).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return false, nil, nil
		}

		return false, nil, err
	}

	return tmpDemo.ID != nil, &tmpDemo, nil
}

// Exists return true if the demo already exists
func (dao DemoDAO) Exists(demo model.Demo) (bool, error) {
	tmpDemo := model.Demo{}

	if demo.ID == nil {
		return false, ErrNotEnoughData
	}

	if err := dao.dataSource.Where("id = ? OR hash_file = ?", *demo.ID, demo.HashFile).First(&tmpDemo).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return false, nil
		}

		return false, err
	}

	return tmpDemo.ID != nil, nil
}

// Create add the demo in the DB
func (dao DemoDAO) Create(demo *model.Demo, log *logger.Interface) error {
	if demo.UserID == nil {
		return ErrNotEnoughData
	}

	demo.User = nil
	if log != nil {
		if err := dao.dataSource.Session(&gorm.Session{Logger: *log}).Create(demo).
			Error; err != nil {
			return err
		}
	} else {
		if err := dao.dataSource.Create(demo).Error; err != nil {
			return err
		}
	}
	return nil
}

// Get get the demo from the DB
func (dao DemoDAO) Get(demo *model.Demo) error {
	if demo.ID == nil {
		return ErrNotEnoughData
	}

	return dao.dataSource.Where("id = ?", *demo.ID).First(demo).Error
}

// DeleteAnalyzes delete all analyses
func (dao DemoDAO) DeleteAnalyzes(demo *model.Demo) error {
	if demo.ID == nil {
		return ErrNotEnoughData
	}

	if err := dao.GetWithStats(demo); err != nil {
		return err
	}

	if len(demo.TeamsStats) != 0 {
		for _, team := range demo.TeamsStats {
			for _, player := range team.PlayersStats {
				if player.Marks != nil {
					for _, weaponMarks := range player.Marks.WeaponsMarks {
						if err := dao.dataSource.Select(clause.Associations).Delete(weaponMarks).Error; err != nil {
							return err
						}
					}
				}
				for _, weaponPattern := range player.WeaponPatterns {
					for _, bulletStats := range weaponPattern.BulletStats {
						if err := dao.dataSource.Select(clause.Associations).Delete(bulletStats).Error; err != nil {
							return err
						}
					}
					if err := dao.dataSource.Select(clause.Associations).Delete(weaponPattern).Error; err != nil {
						return err
					}
				}
			}
			if len(team.PlayersStats) != 0 {
				if err := dao.dataSource.Select(clause.Associations).Delete(team.PlayersStats).Error; err != nil {
					return err
				}
			}
			for _, player := range team.PlayersStats {
				if player.Marks != nil {
					if err := dao.dataSource.Select(clause.Associations).Delete(player.Marks).Error; err != nil {
						return err
					}
				}
			}
			if len(team.DuelsTeam) != 0 {
				if err := dao.dataSource.Select(clause.Associations).Delete(team.DuelsTeam).Error; err != nil {
					return err
				}
			}
		}
		if err := dao.dataSource.Select(clause.Associations).Delete(&demo.TeamsStats).Error; err != nil {
			return err
		}
	}

	if len(demo.Rounds) != 0 {
		for _, round := range demo.Rounds {
			for _, frame := range round.Frames {
				if len(frame.Events) != 0 {
					if err := dao.dataSource.Select(clause.Associations).Delete(frame.Events).Error; err != nil {
						return err
					}
				}
			}
			if len(round.Frames) != 0 {
				if err := dao.dataSource.Select(clause.Associations).Delete(round.Frames).Error; err != nil {
					return err
				}
			}
			round.Frames = nil

			if len(round.ChatMessage) != 0 {
				if err := dao.dataSource.Select(clause.Associations).Delete(round.ChatMessage).Error; err != nil {
					return err
				}
			}
			round.ChatMessage = nil

			if len(round.HeatMapKills) != 0 {
				if err := dao.dataSource.Select(clause.Associations).Delete(round.HeatMapKills).Error; err != nil {
					return err
				}
			}
			round.HeatMapKills = nil

			if len(round.HeatMapDmgs) != 0 {
				if err := dao.dataSource.Select(clause.Associations).Delete(round.HeatMapDmgs).Error; err != nil {
					return err
				}
			}
			round.HeatMapDmgs = nil
		}
		if err := dao.dataSource.Select(clause.Associations).Delete(&demo.Rounds).Error; err != nil {
			return err
		}
	}

	if len(demo.SteamsIDs) != 0 {
		if err := dao.dataSource.Select(clause.Associations).Delete(&demo.SteamsIDs).Error; err != nil {
			return err
		}
	}

	if err := dao.dataSource.Model(&model.Demo{ID: demo.ID}).Update("version_analyzer", 1).Error; err != nil {
		return err
	}

	return nil
}

// Delete delete the demo from the DB
func (dao DemoDAO) Delete(demo model.Demo) error {
	if demo.ID == nil {
		return ErrNotEnoughData
	}

	if err := dao.DeleteAnalyzes(&demo); err != nil {
		return err
	}
	return dao.dataSource.Select(clause.Associations).Delete(demo).Error
}

// GetWithStats get the demo from the DB with the stats
func (dao DemoDAO) GetWithStats(demo *model.Demo) error {
	if demo.ID == nil {
		return ErrNotEnoughData
	}

	if err := dao.dataSource.
		Preload("Rounds", func(db *gorm.DB) *gorm.DB {
			return db.Order("Rounds.num_round ASC")
		}).
		Preload("Rounds.Frames").
		Preload("Rounds.ScoreFactsPerPlayer").
		Preload("Rounds.ChatMessage").
		Preload("Rounds.HeatMapKills").
		Preload("Rounds.HeatMapDmgs").
		Preload("Rounds.MarksPerRound").
		Preload("SteamsIDs").
		Preload("TeamsStats").
		Preload("TeamsStats.DuelsTeam").
		Preload("TeamsStats.PlayersStats").
		Preload("TeamsStats.PlayersStats.WeaponPatterns").
		Preload("TeamsStats.PlayersStats.WeaponPatterns.BulletStats").
		Preload("TeamsStats.PlayersStats.DuelsPlayer").
		Preload("TeamsStats.PlayersStats.Marks").
		Preload("TeamsStats.PlayersStats.Marks.WeaponsMarks").
		Preload("TeamsStats.PlayersStats.ScoreFacts").
		Preload("TeamsStats.PlayersStats.MatrixKills").
		Preload("TeamsStats.PlayersStats.MatrixFlashs").
		First(demo).
		Error; err != nil {
		return err
	}

	return nil
}

// GetMarks get the demo's marks from the DB with the stats
func (dao DemoDAO) GetMarks(demo *model.Demo, steamID string) error {
	if demo.ID == nil {
		return ErrNotEnoughData
	}

	if err := dao.dataSource.
		Preload("Rounds.MarksPerRound", func(db *gorm.DB) *gorm.DB {
			return db.Where("steam_id = ?", steamID)
		}).
		Preload("TeamsStats").
		Preload("TeamsStats.PlayersStats").
		Preload("TeamsStats.PlayersStats.Marks").
		Preload("TeamsStats.PlayersStats.Marks.WeaponsMarks").
		First(demo).
		Error; err != nil {
		return err
	}

	return nil
}

// GetWithScore get the demo from the DB with the score of the teams
func (dao DemoDAO) GetWithScore(demo *model.Demo, steamID string) error {
	if demo.ID == nil {
		return ErrNotEnoughData
	}

	if err := dao.dataSource.
		Preload("SteamsIDs").
		Preload("TeamsStats").
		Preload("TeamsStats.PlayersStats", "steam_id = ?", steamID).
		First(demo).
		Error; err != nil {
		return err
	}

	return nil
}

// GetAllPatternsFromUser get patterns from user
func (dao DemoDAO) GetAllPatternsFromUser(
	steamID string,
	sources *[]model.DemoSource,
) (playerStats []model.PlayerStats, err error) {
	query := dao.dataSource

	if sources != nil {
		query = query.Where("source in (?)", *sources)
	}

	return playerStats, query.
		Where("steam_id = ?", steamID).
		Preload("WeaponPatterns").
		Preload("WeaponPatterns.BulletStats").
		Find(&playerStats).
		Error
}

// GetAllHeatMapFromUserFromMap return all heatmaps of a player
func (dao DemoDAO) GetAllHeatMapFromUserFromMap(
	steamID, mapName string,
	sources *[]model.DemoSource,
) (demos []model.Demo, err error) {
	query := dao.dataSource.
		Where("map_name = ?", mapName)

	if sources != nil {
		query = query.Where("source in (?)", *sources)
	}

	return demos, query.
		Preload("SteamsIDs").
		Where("? IN (?)",
			steamID,
			dao.dataSource.Model(&model.SteamID{}).Select("steam_id").Where("demo_id = demos.id")).
		Preload("Rounds.HeatMapKills", func(db *gorm.DB) *gorm.DB {
			return db.Where("steam_id_killer = ? OR steam_id_victim = ?", steamID, steamID)
		}).
		Preload("Rounds.HeatMapDmgs", func(db *gorm.DB) *gorm.DB {
			return db.Where("steam_id_shooter = ? OR steam_id_victim = ?", steamID, steamID)
		}).
		Find(&demos).
		Error
}

// GetAllFromUser get all the demo from the DB for 1 user
func (dao DemoDAO) GetAllFromUser(
	steamID string,
	sources *[]model.DemoSource,
	maps *[]string,
	from, to *int64,
) (demos []model.Demo, err error) {
	query := dao.dataSource.
		Preload("SteamsIDs").
		Order("date desc").
		Where("? IN (?)",
			steamID,
			dao.dataSource.Model(&model.SteamID{}).Select("steam_id").Where("demo_id = demos.id"))

	if sources != nil {
		query = query.Where("source in (?)", *sources)
	}

	if maps != nil {
		query = query.Where("map_name in (?)", *maps)
	}

	if from != nil {
		query = query.Where("date >= ?", time.Unix(*from/1000, 0).Format("2006-01-02"))
	}

	if to != nil {
		query = query.Where("date <= ?", time.Unix(*to/1000, 0).Add(time.Hour*24).Format("2006-01-02"))
	}
	return demos, query.
		Find(&demos).
		Error
}

// GetAllFromUserWithMarks get all the demo from the DB for 1 user with the marks
func (dao DemoDAO) GetAllFromUserWithMarks(
	steamID string,
	sources *[]model.DemoSource,
	maps *[]string,
	from, to *int64,
) (demos []model.Demo, err error) {
	query := dao.dataSource.
		Preload("SteamsIDs").
		Order("date desc").
		Where("? IN (?)",
			steamID,
			dao.dataSource.Model(&model.SteamID{}).Select("steam_id").Where("demo_id = demos.id"))

	if sources != nil {
		query = query.Where("source in (?)", *sources)
	}

	if maps != nil {
		query = query.Where("map_name in (?)", *maps)
	}

	if from != nil {
		query = query.Where("date >= ?", time.Unix(*from/1000, 0).Format("2006-01-02"))
	}

	if to != nil {
		query = query.Where("date <= ?", time.Unix(*to/1000, 0).Add(time.Hour*24).Format("2006-01-02"))
	}
	return demos, query.
		Preload("Rounds.MarksPerRound", func(db *gorm.DB) *gorm.DB {
			return db.Where("steam_id = ?", steamID)
		}).
		Preload("TeamsStats.PlayersStats", func(db *gorm.DB) *gorm.DB {
			return db.Where("steam_id = ?", steamID)
		}).
		Preload("TeamsStats.PlayersStats.Marks").
		Preload("TeamsStats.PlayersStats.Marks.WeaponsMarks").
		Find(&demos).
		Error
}

// GetLastAnalyzed all the last demos analyzed
func (dao DemoDAO) GetLastAnalyzed(versionAnalyzer int64) (demos []model.Demo, err error) {
	return demos, dao.dataSource.
		Limit(10).
		Order("created_at desc").
		Preload("TeamsStats.PlayersStats").
		Where("version_analyzer >= ?", versionAnalyzer).
		Find(&demos).
		Error
}

// GetResumeFromFilters get all the demos by filters
func (dao DemoDAO) GetResumeFromFilters(
	steamID string,
	sources *[]model.DemoSource,
	limit, offset int,
	maps *[]string,
	from, to *int64,
	versionAnalyzer int64,
) (demos []model.Demo, err error) {
	query := dao.dataSource.
		Limit(limit).
		Offset(offset).
		Preload("SteamsIDs").
		Order("date desc").
		Where("? IN (?)",
			steamID,
			dao.dataSource.Model(&model.SteamID{}).Select("steam_id").Where("demo_id = demos.id"))

	if sources != nil {
		query = query.Where("source in (?)", *sources)
	}

	if maps != nil {
		query = query.Where("map_name in (?)", *maps)
	}

	if from != nil {
		query = query.Where("date >= ?", time.Unix(*from/1000, 0).Format("2006-01-02"))
	}

	if to != nil {
		query = query.Where("date <= ?", time.Unix(*to/1000, 0).Add(time.Hour*24).Format("2006-01-02"))
	}

	query = query.Where("version_analyzer >= ?", versionAnalyzer)

	return demos, query.
		Find(&demos).
		Error
}

// GetAllAnalizedWithStats get all demos from the DB for 1 user analyzed
func (dao DemoDAO) GetAllAnalizedWithStats(
	steamID string,
	sources *[]model.DemoSource,
	analyzerVersion int64,
) (demos []model.Demo, err error) {
	query := dao.dataSource

	if sources != nil {
		query = query.Where("source in (?)", *sources)
	}

	return demos, query.
		Preload("SteamsIDs").
		Preload("TeamsStats.PlayersStats").
		Preload("TeamsStats.PlayersStats.ScoreFacts").
		Order("date").
		Where("version_analyzer = ? AND ? IN (?)", analyzerVersion, steamID,
			dao.dataSource.Model(&model.SteamID{}).Select("steam_id").Where("demo_id = demos.id")).
		Find(&demos).
		Error
}

// GetFromShareCode get the demo linked to the sharecode
func (dao DemoDAO) GetFromShareCode(sharecode string) (*model.Demo, error) {
	var demo model.Demo
	return &demo, dao.dataSource.
		Where("sharecode = ?", sharecode).
		First(&demo).Error
}

// GetAllNotClassified get all the demos from the DB not classifier
func (dao DemoDAO) GetAllNotClassified() (demos []model.Demo, err error) {
	query := dao.dataSource

	return demos, query.
		Preload("User").
		Where("game_mode = ?", model.UnknownGameMode).
		Find(&demos).Error
}

// GetAllNotDownloaded get all the demos from the DB not downloaded
func (dao DemoDAO) GetAllNotDownloaded(sources *[]model.DemoSource) (demos []model.Demo, err error) {
	query := dao.dataSource

	if sources != nil {
		query = query.Where("source in (?)", *sources)
	}

	return demos, query.
		Preload("User").
		Where("downloaded = false").
		Find(&demos).Error
}

// GetAll2BeAnalyzed get all the demos from the DB not analyzed or with an analyzer outdated
func (dao DemoDAO) GetAll2BeAnalyzed(
	analyzedVersion int64,
	gameMode model.DemoGameMode,
) (demos []model.Demo, err error) {
	query := dao.dataSource.Where("game_mode  = ?", gameMode)

	return demos, query.
		Where("version_analyzer < ?", analyzedVersion).
		Find(&demos).Error
}

// GetNbr2BeAnalyzedFromPlayer get the number of the demos from the DB not analyzed or with an analyzer outdated
func (dao DemoDAO) GetNbr2BeAnalyzedFromPlayer(steamID string, analyzedVersion int64) (nbrDemos int64, err error) {
	return nbrDemos, dao.dataSource.Model(&model.Demo{}).
		Where("version_analyzer < ?", analyzedVersion).
		Preload("SteamsIDs").
		Where("? IN (?)",
			steamID,
			dao.dataSource.Model(&model.SteamID{}).Select("steam_id").Where("demo_id = demos.id")).
		Count(&nbrDemos).Error
}

// SetLastVersionAnalyzer set the version analyzer of the demo with the last version
func (dao DemoDAO) SetLastVersionAnalyzer(demoID int64, analyzerVersion int64) error {
	query := dao.dataSource.Model(&model.Demo{ID: &demoID}).Update("version_analyzer", analyzerVersion)
	if query.Error != nil {
		return query.Error
	}

	if query.RowsAffected == 0 {
		return gorm.ErrRecordNotFound
	}

	return nil
}

// SetDownloaded set the demo as downloaded
func (dao DemoDAO) SetDownloaded(demoID int64) error {
	query := dao.dataSource.Model(&model.Demo{ID: &demoID}).Update("downloaded", true)
	if query.Error != nil {
		return query.Error
	}

	if query.RowsAffected == 0 {
		return gorm.ErrRecordNotFound
	}

	return nil
}

// SetSize set the size of the demo
func (dao DemoDAO) SetSize(demoID int64, size int64) error {
	query := dao.dataSource.Model(&model.Demo{ID: &demoID}).Update("size", size)
	if query.Error != nil {
		return query.Error
	}

	if query.RowsAffected == 0 {
		return gorm.ErrRecordNotFound
	}

	return nil
}

// SwapCompetitiveToDMDemo swap a competitive demo to a deathmatch demo
func (dao DemoDAO) SwapCompetitiveToDMDemo(demo model.Demo, newPath string) error {
	isAnalyzed, err := dao.IsAnalyzed(*demo.ID)
	if err != nil {
		return err
	}
	if isAnalyzed {
		return ErrAlreadyAnalyzed
	}

	if err := os.Rename(*demo.Path, newPath); err != nil {
		return err
	}

	DMDemo := model.DeathMatchDemo{
		ID:              demo.ID,
		Date:            demo.Date,
		UserID:          demo.UserID,
		VersionAnalyzer: helpers.Int64Ptr(0),
		HashFile:        demo.HashFile,
		Path:            &newPath,
		Name:            demo.Name,
	}
	if err := dao.dataSource.Create(&DMDemo).Error; err != nil {
		return err
	}

	if err := dao.Delete(demo); err != nil {
		return err
	}

	return nil
}

// SetGameMode set the gamemode of the demo
func (dao DemoDAO) SetGameMode(demoID int64, mode model.DemoGameMode) error {
	query := dao.dataSource.Model(&model.Demo{ID: &demoID}).Update("game_mode", mode)
	if query.Error != nil {
		return query.Error
	}

	if query.RowsAffected == 0 {
		return gorm.ErrRecordNotFound
	}

	return nil
}

// SetSource set the source of the demo
func (dao DemoDAO) SetSource(demoID int64, source model.DemoSource) error {
	query := dao.dataSource.Model(&model.Demo{ID: &demoID}).Update("source", source)
	if query.Error != nil {
		return query.Error
	}

	if query.RowsAffected == 0 {
		return gorm.ErrRecordNotFound
	}

	return nil
}

// SetShareCode set the sharecode of the demo
func (dao DemoDAO) SetShareCode(demoID int64, sharecode *string) error {
	query := dao.dataSource.Model(&model.Demo{ID: &demoID}).Update("sharecode", *sharecode)
	if query.Error != nil {
		return query.Error
	}

	if query.RowsAffected == 0 {
		return gorm.ErrRecordNotFound
	}

	return nil
}

// SetReplayURL set the replayURL of the demo
func (dao DemoDAO) SetReplayURL(demoID int64, replayURL *string) error {
	query := dao.dataSource.Model(&model.Demo{ID: &demoID}).Update("replay_url", *replayURL)
	if query.Error != nil {
		return query.Error
	}

	if query.RowsAffected == 0 {
		return gorm.ErrRecordNotFound
	}

	return nil
}

// SetHash set the hash of the demo
func (dao DemoDAO) SetHash(demoID int64, hash []byte) error {
	query := dao.dataSource.Model(&model.Demo{ID: &demoID}).Update("hash_file", hash)
	if query.Error != nil {
		return query.Error
	}

	if query.RowsAffected == 0 {
		return gorm.ErrRecordNotFound
	}

	return nil
}

// IsAnalyzed return true if the demo is analyzed
func (dao DemoDAO) IsAnalyzed(demoID int64) (bool, error) {
	demo := model.Demo{
		ID: &demoID,
	}

	err := dao.dataSource.First(&demo).Error
	if err != nil {
		return false, err
	}

	return *demo.VersionAnalyzer > 0, nil
}

// SaveAnalyze saves the analyze of the demo
func (dao DemoDAO) SaveAnalyze(demo model.Demo) error {
	if demo.ID == nil {
		return ErrNotEnoughData
	}

	if exists, err := dao.Exists(demo); err != nil {
		return err
	} else if !exists {
		return gorm.ErrRecordNotFound
	}

	demo.User = nil

	rounds := demo.Rounds
	demo.Rounds = nil
	sort.Slice(rounds, func(i, j int) bool {
		return *rounds[i].NumRound < *rounds[j].NumRound
	})

	fastLogger := store.NewCustomLogger(
		log.New(dao.logDB.DefaultOutput, "\r\n", log.LstdFlags), // io writer
		store.Config{
			SlowThreshold: 1000 * time.Millisecond,
			LogLevel:      logger.Error,
			Colorful:      false,
		},
	)
	if err := dao.dataSource.
		Clauses(clause.OnConflict{DoNothing: true}).
		Session(&gorm.Session{FullSaveAssociations: true, Logger: fastLogger}).Save(&demo).Error; err != nil {
		return err
	}

	for _, round := range rounds {
		round.DemoID = demo.ID
		if err := dao.dataSource.
			Clauses(clause.OnConflict{DoNothing: true}).
			Session(&gorm.Session{FullSaveAssociations: true, Logger: fastLogger}).Save(round).Error; err != nil {
			return err
		}
	}

	dao.logDB.Tracef("demo %d has been saved", demo.ID)

	return nil
}
