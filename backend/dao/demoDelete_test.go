package dao_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

func TestDaoDemo_DeleteOK(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	demoDAO := dao.NewDemoDAO(localDB, logTest)

	exists, err := demoDAO.Exists(*demosGeneratedOutAnalyzed[0])
	assert.True(t, exists)
	assert.NoError(t, err)

	require.NoError(t, demoDAO.Delete(*demosGeneratedOutAnalyzed[0]))

	exists, err = demoDAO.Exists(*demosGeneratedOutAnalyzed[0])
	assert.False(t, exists)
	assert.NoError(t, err)
	assert.Error(t, demoDAO.Delete(model.Demo{}))
}

// func TesttDaoDemo_DeleteAnalyzesOK(t *testing.T) {
// 	localDB := _testDBGlobal.Begin()
// 	defer localDB.Rollback()
// 	demoDAO := dao.NewDemoDAO(localDB, logTest)

// 	demo := model.GenerateRandomDemoWithStats(userGeneratedVirgin, *userGeneratedVirgin.SteamID, 1)
// 	demo.Downloaded = helpers.BoolPtr(true)
// 	demo.VersionAnalyzer = helpers.Int64Ptr(rand.Int63())
// 	require.NoError(t, demoDAO.Create(demo, &fastLogger))
// 	var count int64
// 	assert.NoError(t, localDB.Model(&model.PlayerStats{}).Count(&count).Error)
// 	assert.True(t, count > 0)
// 	assert.NoError(t, localDB.Model(&model.TeamStats{}).Count(&count).Error)
// 	assert.True(t, count > 0)
// 	assert.NoError(t, localDB.Model(&model.DuelsPlayer{}).Count(&count).Error)
// 	assert.True(t, count > 0)
// 	assert.NoError(t, localDB.Model(&model.Frame{}).Count(&count).Error)
// 	assert.True(t, count > 0)
// 	assert.NoError(t, localDB.Model(&model.ChatMessage{}).Count(&count).Error)
// 	assert.True(t, count > 0)
// 	assert.NoError(t, localDB.Model(&model.Round{}).Count(&count).Error)
// 	assert.True(t, count > 0)
// 	assert.NoError(t, localDB.Model(&model.SteamID{}).Count(&count).Error)
// 	assert.True(t, count > 0)
// 	assert.NoError(t, localDB.Model(&model.HeatMapDmg{}).Count(&count).Error)
// 	assert.True(t, count > 0)
// 	assert.NoError(t, localDB.Model(&model.HeatMapKill{}).Count(&count).Error)
// 	assert.True(t, count > 0)
// 	assert.NoError(t, localDB.Model(&model.Marks{}).Count(&count).Error)
// 	assert.True(t, count > 0)
// 	assert.NoError(t, localDB.Model(&model.WeaponMarks{}).Count(&count).Error)
// 	assert.True(t, count > 0)

// 	require.NoError(t, demoDAO.DeleteAnalyzes(demo))

// 	demoFromDB := model.Demo{ID: demo.ID}
// 	assert.NoError(t, demoDAO.GetWithStats(&demoFromDB))

// 	assert.Len(t, demoFromDB.TeamsStats, 0)
// 	assert.Error(t, demoDAO.DeleteAnalyzes(&model.Demo{}))

// 	rounds := []model.Round{}
// 	assert.NoError(t, localDB.Where("demo_id = ?", *demo.ID).Find(&rounds).Error)
// 	assert.Len(t, rounds, 0)

// 	assert.NoError(t, localDB.Model(&model.PlayerStats{}).Count(&count).Error)
// 	assert.Equal(t, int64(0), count)
// 	assert.NoError(t, localDB.Model(&model.TeamStats{}).Count(&count).Error)
// 	assert.Equal(t, int64(0), count)
// 	assert.NoError(t, localDB.Model(&model.DuelsPlayer{}).Count(&count).Error)
// 	assert.Equal(t, int64(0), count)
// 	assert.NoError(t, localDB.Model(&model.Frame{}).Count(&count).Error)
// 	assert.Equal(t, int64(0), count)
// 	assert.NoError(t, localDB.Model(&model.ChatMessage{}).Count(&count).Error)
// 	assert.Equal(t, int64(0), count)
// 	assert.NoError(t, localDB.Model(&model.Round{}).Count(&count).Error)
// 	assert.Equal(t, int64(0), count)
// 	assert.NoError(t, localDB.Model(&model.SteamID{}).Count(&count).Error)
// 	assert.Equal(t, int64(0), count)
// 	assert.NoError(t, localDB.Model(&model.HeatMapDmg{}).Count(&count).Error)
// 	assert.Equal(t, int64(0), count)
// 	assert.NoError(t, localDB.Model(&model.HeatMapKill{}).Count(&count).Error)
// 	assert.Equal(t, int64(0), count)
// 	assert.NoError(t, localDB.Model(&model.Marks{}).Count(&count).Error)
// 	assert.Equal(t, int64(0), count)
// 	assert.NoError(t, localDB.Model(&model.WeaponMarks{}).Count(&count).Error)
// 	assert.Equal(t, int64(0), count)
// }
