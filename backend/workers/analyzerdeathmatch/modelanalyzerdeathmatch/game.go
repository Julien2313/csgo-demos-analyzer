package modelanalyzerdeathmatch

import (
	"time"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"
)

// Game struct of a game with all the events
type Game struct {
	FramesPerSecond int
	Started         bool

	Players map[uint64]*Player

	MapName string
}

// Analyze analyze a deathmatch
func (g *Game) Analyze() {
	for _, player := range g.Players {
		player.WeaponsStats = make(map[common.EquipmentType]*WeaponStats)
		for numBulletFired, bulletFired := range player.BulletsFired {
			if bulletFired.WeaponType == common.EqUnknown {
				continue
			}
			if player.WeaponsStats[bulletFired.WeaponType] == nil {
				player.WeaponsStats[bulletFired.WeaponType] = &WeaponStats{}
			}
			weaponFired := player.WeaponsStats[bulletFired.WeaponType]
			weaponFired.NbrBulletFired++
			weaponFired.CumulativeVelocityAttacker += bulletFired.VictimVelocity
			if bulletFired.Hit {
				weaponFired.NbrBulletHit++
				if bulletFired.HitGroup == events.HitGroupHead {
					weaponFired.NbrBulletHS++
				}
			}

			if numBulletFired > 0 {
				lastBullet := player.BulletsFired[numBulletFired-1]
				if bulletFired.TimeFired-lastBullet.TimeFired > time.Second*5 {
					weaponFired.NbrFirstBulletFired++
					if bulletFired.Hit {
						weaponFired.NbrFirstBulletHit++
						if bulletFired.HitGroup == events.HitGroupHead {
							weaponFired.NbrFirstBulletHS++
						}
					}
				}
			}
		}

		for _, weapon := range player.WeaponsStats {
			if weapon.NbrFirstBulletFired != 0 {
				weapon.FirstBulletAccuracy = float64(weapon.NbrFirstBulletHit) / float64(weapon.NbrFirstBulletFired) * 100.0
			}
			if weapon.NbrFirstBulletHit != 0 {
				weapon.FirstBulletHS = float64(weapon.NbrFirstBulletHS) / float64(weapon.NbrFirstBulletHit) * 100.0
			}
			if weapon.NbrBulletFired != 0 {
				weapon.BulletAccuracy = float64(weapon.NbrBulletHit) / float64(weapon.NbrBulletFired) * 100.0
				weapon.AverageVelocityAttacker = weapon.CumulativeVelocityAttacker / float64(weapon.NbrBulletFired)
			}
			if weapon.NbrBulletHit != 0 {
				weapon.HSPercentage = float64(weapon.NbrBulletHS) / float64(weapon.NbrBulletHit) * 100.0
			}
		}
	}
}
