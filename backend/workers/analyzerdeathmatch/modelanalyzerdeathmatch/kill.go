package modelanalyzerdeathmatch

// Kill struct of a kill with all the data
type Kill struct {
	HeadShot bool
	WallBang bool

	Weapon string
}
