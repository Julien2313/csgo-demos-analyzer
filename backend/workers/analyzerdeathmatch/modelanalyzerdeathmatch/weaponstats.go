package modelanalyzerdeathmatch

// WeaponStats stats of weapons
type WeaponStats struct {
	FirstBulletAccuracy float64
	FirstBulletHS       float64
	BulletAccuracy      float64
	HSPercentage        float64

	NbrBulletFired int
	NbrBulletHit   int
	NbrBulletHS    int

	NbrFirstBulletFired int
	NbrFirstBulletHit   int
	NbrFirstBulletHS    int

	CumulativeVelocityAttacker float64
	AverageVelocityAttacker    float64
}
