package modelanalyzerdeathmatch

import (
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
)

// Player struct of a player and its kills, assits, flashes...
type Player struct {
	Player *common.Player

	SteamID  uint64
	Username string

	Kills        []*Kill
	BulletsFired []*BulletFired

	WeaponsStats map[common.EquipmentType]*WeaponStats
}
