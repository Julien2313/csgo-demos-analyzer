package modelanalyzerdeathmatch

import (
	"time"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"
)

// BulletFired data about a bullet fired
type BulletFired struct {
	TimeFired time.Duration
	Hit       bool
	HitGroup  events.HitGroup

	WeaponType    common.EquipmentType
	Weapon        string
	DistanceEnemy float64

	AttackerVelocity float64
	VictimVelocity   float64
}
