package analyzerdeathmatch

import (
	"runtime/debug"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzerdeathmatch/modelanalyzerdeathmatch"
)

// ParserDeathMatch type to parse the game
type ParserDeathMatch struct {
	DemoID           int64
	ParserDemoinfocs demoinfocs.Parser

	Game *modelanalyzerdeathmatch.Game
	log  *custlogger.Logger
}

// Parse analyze one game
func (p *ParserDeathMatch) Parse() error {
	p.Game = &modelanalyzerdeathmatch.Game{
		Players: make(map[uint64]*modelanalyzerdeathmatch.Player),
	}

	header, err := p.ParserDemoinfocs.ParseHeader()
	if err != nil {
		p.log.Error(err, "error when parsing header :", err)
		return err
	}

	p.ParserDemoinfocs.RegisterEventHandler(func(e events.PlayerConnect) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(err, "Recovered in PlayerConnect of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		if _, exists := p.Game.Players[e.Player.SteamID64]; exists {
			p.Game.Players[e.Player.SteamID64].Player = e.Player
			return
		}

		player := &modelanalyzerdeathmatch.Player{
			Player:   e.Player,
			SteamID:  e.Player.SteamID64,
			Username: e.Player.Name,
		}
		p.Game.Players[player.SteamID] = player
	})

	p.WeaponFire()
	p.PlayerHurt()

	p.Game.MapName = *helpers.ParseMapName(header.MapName)

	defer func() {
		if r := recover(); r != nil {
			p.log.Errorf(err, "Recovered in Parse of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
		}
	}()

	if err := p.ParserDemoinfocs.ParseToEnd(); err != nil {
		if err.Error() == "send on closed channel" {
			p.log.Error(err, "Let's hope Markus solve this")
			return err
		}
		p.log.Error(err, "Error while parsing demo : ", err)
	}

	return nil
}

// WeaponFire event when a player fire
func (p *ParserDeathMatch) WeaponFire() {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.WeaponFire) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in WeaponFire of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		if e.Shooter == nil || e.Shooter.IsBot {
			return
		}

		attacker := p.Game.Players[e.Shooter.SteamID64]
		if attacker == nil {
			return
		}

		if len(attacker.BulletsFired) > 0 &&
			attacker.BulletsFired[len(attacker.BulletsFired)-1].TimeFired == p.ParserDemoinfocs.CurrentTime() {
			return
		}

		attacker.BulletsFired = append(attacker.BulletsFired, &modelanalyzerdeathmatch.BulletFired{
			TimeFired:        p.ParserDemoinfocs.CurrentTime(),
			Hit:              false,
			HitGroup:         99,
			WeaponType:       e.Weapon.Type,
			Weapon:           e.Weapon.String(),
			DistanceEnemy:    -1.0,
			AttackerVelocity: e.Shooter.Velocity().Norm(),
			VictimVelocity:   -1.0,
		})
	})
}

// PlayerHurt event when a player has been hurt
func (p *ParserDeathMatch) PlayerHurt() {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.PlayerHurt) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in PlayerHurt of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		if e.Attacker == nil || e.Attacker.IsBot {
			return
		}
		if e.Player == nil {
			return
		}

		attacker := p.Game.Players[e.Attacker.SteamID64]
		if attacker == nil {
			return
		}
		if len(attacker.BulletsFired) > 0 &&
			attacker.BulletsFired[len(attacker.BulletsFired)-1].TimeFired == p.ParserDemoinfocs.CurrentTime() {
			attacker.BulletsFired = attacker.BulletsFired[:len(attacker.BulletsFired)-1]
		}

		attacker.BulletsFired = append(attacker.BulletsFired, &modelanalyzerdeathmatch.BulletFired{
			TimeFired:        p.ParserDemoinfocs.CurrentTime(),
			Hit:              true,
			HitGroup:         e.HitGroup,
			WeaponType:       e.Weapon.Type,
			Weapon:           e.Weapon.String(),
			DistanceEnemy:    e.Attacker.LastAlivePosition.Distance(e.Player.LastAlivePosition),
			AttackerVelocity: e.Attacker.Velocity().Norm(),
			VictimVelocity:   e.Player.Velocity().Norm(),
		})
	})
}
