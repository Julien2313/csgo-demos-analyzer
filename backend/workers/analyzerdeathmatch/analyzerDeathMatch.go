package analyzerdeathmatch

import (
	"errors"
	"os"
	"runtime/debug"
	"strconv"
	"time"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gorm.io/gorm"
)

// DataAnalyzerDeathMatch data send to be analyzed
type DataAnalyzerDeathMatch struct {
	demoID int64
}

// AnalyzerDeathMatch stores the games it has to analyze
type AnalyzerDeathMatch struct {
	version     int64
	data        chan DataAnalyzerDeathMatch
	shutdown    chan interface{}
	db          *gorm.DB
	log         *custlogger.Logger
	portServer  int
	PathStorage string
}

const maxChan = 10000

// ErrChanFull chan is full
var ErrChanFull = errors.New("chan analyzer is full")

// ErrCouldNotShutdown could not shutdown analyzer deathmatch
var ErrCouldNotShutdown = errors.New("could not shutdown analyzerdeathmatch analyzer deathmatch")

// Start starts this worker
func Start(db *gorm.DB, logAnalyzer *custlogger.Logger, version int64, schema string,
	PathStorage string, portServer int) (*AnalyzerDeathMatch, error) {
	analyzer := &AnalyzerDeathMatch{
		data:        make(chan DataAnalyzerDeathMatch, maxChan),
		shutdown:    make(chan interface{}),
		log:         &custlogger.Logger{},
		version:     version,
		db:          db,
		portServer:  portServer,
		PathStorage: PathStorage,
	}
	analyzer.log.SetupInstanceLoggger("analyzer")

	go analyzer.CheckForEver()

	return analyzer, analyzer.CheckOutAnalyzed()
}

// CheckForEver this check if new demo need to be analyzed
func (a *AnalyzerDeathMatch) CheckForEver() {
	defer func() {
		if r := recover(); r != nil {
			a.log.Errorf(nil, "Recovered in Analyzer.CheckForEver %v %s", r, string(debug.Stack()))
			time.Sleep(time.Second * 10)
			go a.CheckForEver()
		}
	}()

	for {
		// to avoid 1/2 chance to analyze even if we want to shut down
		select {
		case <-a.shutdown:
			a.log.Info("analyzer shutdown")
			return
		default:
		}
		select {
		case <-a.shutdown:
			a.log.Info("analyzer shutdown")
			return
		case data := <-a.data:
			a.ParseOneGame(data)
		}
	}
}

// CheckOutAnalyzed at the start of the analyzer, check if their is demo to (re)analyze or not
func (a *AnalyzerDeathMatch) CheckOutAnalyzed() error {
	demoDAO := dao.NewDemoDeathMatchDAO(a.db)

	demos, err := demoDAO.GetAll2BeAnalyzed(a.version)
	if err != nil {
		a.log.Error(err, "Can't get all dem to (re)analyze:", err)
		return err
	}

	for _, demo := range demos {
		if err := a.AddDataAnalyzer([]int64{*demo.ID}); err != nil {
			a.log.Error(err, "Can't add to analyzed:", err)
			return err
		}
	}

	return nil
}

// AddDataAnalyzer add data to be analyzed
func (a *AnalyzerDeathMatch) AddDataAnalyzer(demoIDs []int64) error {
	for _, ID := range demoIDs {
		select {
		case a.data <- DataAnalyzerDeathMatch{
			demoID: ID,
		}:
		default:
			return ErrChanFull
		}
	}

	return nil
}

// Shutdown shutdown AnalyzerDeathMatch
func (a *AnalyzerDeathMatch) Shutdown() {
	a.shutdown <- struct{}{}
}

// ParseOneGame start to analyze one game
func (a *AnalyzerDeathMatch) ParseOneGame(data DataAnalyzerDeathMatch) {
	demoDAO := dao.NewDemoDeathMatchDAO(a.db)
	demo := model.DeathMatchDemo{
		ID: &data.demoID,
	}

	if err := demoDAO.Get(&demo); err != nil {
		a.log.Error(err, "error while getting the demo from DAO :", err)
		return
	}

	demoZipped, err := os.Open(*demo.Path)
	if err != nil {
		a.log.Error(err, "error while opening the demo from its path :", err)
		return
	}
	defer func() {
		if err := demoZipped.Close(); err != nil {
			a.log.Error(err, "error while closing the demo :", err.Error())
		}
	}()

	parser := ParserDeathMatch{
		ParserDemoinfocs: demoinfocs.NewParserWithConfig(demoZipped, demoinfocs.DefaultParserConfig),
		DemoID:           *demo.ID,
		log:              a.log,
	}
	if err := parser.Parse(); err != nil {
		return
	}
	parser.Game.Analyze()
	a.Transcript(parser, &demo)

	demo.VersionAnalyzer = helpers.Int64Ptr(a.version)

	if err := demoDAO.SaveAnalyze(demo); err != nil {
		a.log.Error(err, "error while trying to save the analyzed deathmatch demo : ", err)
		return
	}
}

// Transcript convert a demo to be stored in the DB
func (a *AnalyzerDeathMatch) Transcript(parser ParserDeathMatch, demo *model.DeathMatchDemo) {
	for _, player := range parser.Game.Players {
		weaponStats := []*model.DeathMatchWeaponStats{}
		var (
			nbrBulletFired      int
			nbrBulletHit        int
			nbrBulletHS         int
			nbrFirstBulletFired int
			nbrFirstBulletHit   int
			nbrFirstBulletHS    int

			cumulativeVelocityAttacker float64
		)

		for weaponID, weapon := range player.WeaponsStats {
			weaponStats = append(weaponStats, &model.DeathMatchWeaponStats{
				FirstBulletAccuracy:     &weapon.FirstBulletAccuracy,
				FirstBulletHS:           &weapon.FirstBulletHS,
				BulletAccuracy:          &weapon.BulletAccuracy,
				HSPercentage:            &weapon.HSPercentage,
				NbrBulletFired:          &weapon.NbrBulletFired,
				NbrBulletHit:            &weapon.NbrBulletHit,
				NbrBulletHS:             &weapon.NbrBulletHS,
				NbrFirstBulletFired:     &weapon.NbrFirstBulletFired,
				NbrFirstBulletHit:       &weapon.NbrFirstBulletHit,
				NbrFirstBulletHS:        &weapon.NbrFirstBulletHS,
				AverageVelocityAttacker: &weapon.AverageVelocityAttacker,
				WeaponID:                func(e common.EquipmentType) *common.EquipmentType { return &e }(weaponID),
			})
			nbrBulletFired += weapon.NbrBulletFired
			nbrBulletHit += weapon.NbrBulletHit
			nbrBulletHS += weapon.NbrBulletHS
			nbrFirstBulletFired += weapon.NbrFirstBulletFired
			nbrFirstBulletHit += weapon.NbrFirstBulletHit
			nbrFirstBulletHS += weapon.NbrFirstBulletHS
			cumulativeVelocityAttacker += weapon.CumulativeVelocityAttacker
		}

		var (
			firstBulletAccuracy     float64
			firstBulletHS           float64
			bulletAccuracy          float64
			hsPercentage            float64
			averageVelocityAttacker float64
		)

		if nbrFirstBulletFired != 0 {
			firstBulletAccuracy = float64(nbrFirstBulletHit) / float64(nbrFirstBulletFired) * 100.0
		}
		if nbrFirstBulletHit != 0 {
			firstBulletHS = float64(nbrFirstBulletHS) / float64(nbrFirstBulletHit) * 100.0
		}
		if nbrBulletFired != 0 {
			averageVelocityAttacker = cumulativeVelocityAttacker / float64(nbrBulletFired)
			bulletAccuracy = float64(nbrBulletHit) / float64(nbrBulletFired) * 100.0
		}
		if nbrBulletHit != 0 {
			hsPercentage = float64(nbrBulletHS) / float64(nbrBulletHit) * 100.0
		}
		demo.DeathMatchPlayers = append(demo.DeathMatchPlayers, &model.DeathMatchPlayer{
			SteamID:                 helpers.StrPtr(strconv.FormatUint(player.SteamID, 10)),
			WeaponsStats:            weaponStats,
			FirstBulletAccuracy:     &firstBulletAccuracy,
			FirstBulletHS:           &firstBulletHS,
			BulletAccuracy:          &bulletAccuracy,
			HSPercentage:            &hsPercentage,
			NbrBulletFired:          &nbrBulletFired,
			NbrBulletHit:            &nbrBulletHit,
			NbrBulletHS:             &nbrBulletHS,
			NbrFirstBulletFired:     &nbrFirstBulletFired,
			NbrFirstBulletHit:       &nbrFirstBulletHit,
			NbrFirstBulletHS:        &nbrFirstBulletHS,
			AverageVelocityAttacker: &averageVelocityAttacker,
		})
	}
}
