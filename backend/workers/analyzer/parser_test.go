package analyzer_test

import (
	"math/rand"
	"testing"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
)

func TestParser_LastFrameShootPlayer(t *testing.T) {
	steamIDAttacker := rand.Uint64()
	steamIDAttackerennemies := rand.Uint64()

	parser := analyzer.Parser{}
	assert.Equal(t, -1, parser.LastFrameShootPlayer(steamIDAttacker))

	parser.Game = &modelanalyzer.Game{
		CurrentRound: &modelanalyzer.Round{},
	}
	assert.Equal(t, -1, parser.LastFrameShootPlayer(steamIDAttacker))

	weaponFireEvent := modelanalyzer.WeaponFireEvent{
		WeaponFire: events.WeaponFire{
			Shooter: &common.Player{SteamID64: steamIDAttacker},
			Weapon: &common.Equipment{
				Type: common.EqAK47,
			},
		},
	}
	weaponFireEventEnnemie := modelanalyzer.WeaponFireEvent{
		WeaponFire: events.WeaponFire{
			Shooter: &common.Player{SteamID64: steamIDAttackerennemies},
			Weapon: &common.Equipment{
				Type: common.EqAK47,
			},
		},
	}

	playerHurtEvent := modelanalyzer.PlayerHurtEvent{
		PlayerHurt: events.PlayerHurt{
			Attacker: &common.Player{SteamID64: steamIDAttacker},
			Weapon: &common.Equipment{
				Type: common.EqAK47,
			},
		},
	}
	playerHurtEventEnnemie := modelanalyzer.PlayerHurtEvent{
		PlayerHurt: events.PlayerHurt{
			Attacker: &common.Player{SteamID64: steamIDAttackerennemies},
			Weapon: &common.Equipment{
				Type: common.EqAK47,
			},
		},
	}

	parser.Game.CurrentRound.Frames = []*modelanalyzer.Frame{
		{NumFrame: 0},
		{
			NumFrame: 1,
			Events: []modelanalyzer.EventAnalyzer{
				&weaponFireEvent,
				&weaponFireEventEnnemie,
			},
		},
		{
			NumFrame: 2,
			Events: []modelanalyzer.EventAnalyzer{
				&weaponFireEvent,
			},
		},
		{NumFrame: 3},
	}
	assert.Equal(t, 2, parser.LastFrameShootPlayer(steamIDAttacker))
	assert.Equal(t, 1, parser.LastFrameShootPlayer(steamIDAttackerennemies))

	parser.Game.CurrentRound.Frames = []*modelanalyzer.Frame{
		{NumFrame: 0},
		{
			NumFrame: 1,
			Events: []modelanalyzer.EventAnalyzer{
				&playerHurtEvent,
				&playerHurtEventEnnemie,
			},
		},
		{
			NumFrame: 2,
			Events: []modelanalyzer.EventAnalyzer{
				&playerHurtEvent,
			},
		},
		{NumFrame: 3},
	}
	assert.Equal(t, 2, parser.LastFrameShootPlayer(steamIDAttacker))
	assert.Equal(t, 1, parser.LastFrameShootPlayer(steamIDAttackerennemies))
}
