package analyzer

import (
	"errors"
	"fmt"
	"os"
	"runtime/debug"
	"strconv"
	"strings"
	"time"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzerhelper"
	"gorm.io/gorm"
)

// DataAnalyzer data send to be analyzed
type DataAnalyzer struct {
	demoID int64
}

// Analyzer stores the games it has to analyze
type Analyzer struct {
	version  int64
	data     chan DataAnalyzer
	shutdown chan interface{}
	db       *gorm.DB
	log      *custlogger.Logger
}

const maxChan = 10000

// ErrChanFull chan is full
var ErrChanFull = errors.New("chan analyzer is full")

// ErrCouldNotShutdown could not shutdown analyzer
var ErrCouldNotShutdown = errors.New("could not shutdown analyzer")

// NewAnalyzer constructor of Analyzer
func NewAnalyzer(db *gorm.DB, logAnalyzer *custlogger.Logger, version int64) *Analyzer {
	return &Analyzer{
		data:     make(chan DataAnalyzer, maxChan),
		shutdown: make(chan interface{}),
		log:      &custlogger.Logger{},
		version:  version,
		db:       db,
	}
}

// Start starts this worker
func Start(db *gorm.DB, logAnalyzer *custlogger.Logger, version int64) (*Analyzer, error) {
	analyzer := NewAnalyzer(db, logAnalyzer, version)
	analyzer.log.SetupInstanceLoggger("analyzer")

	go analyzer.CheckForEver()

	return analyzer, analyzer.CheckOutAnalyzed()
}

// CheckOutAnalyzed at the start of the analyzer, check if their is demo to (re)analyze or not
func (a *Analyzer) CheckOutAnalyzed() error {
	demoDAO := dao.NewDemoDAO(a.db, a.log)

	demos, err := demoDAO.GetAll2BeAnalyzed(a.version, model.Competitive)
	if err != nil {
		a.log.Error(err, "Can't get all dem to (re)analyze:", err)
		return err
	}

	for _, demo := range demos {
		if !*demo.Downloaded {
			continue
		}

		if *demo.VersionAnalyzer == 202011261355 {
			if *demo.MapName == "ancient" {
				if err := a.AddDataAnalyzer([]int64{*demo.ID}); err != nil {
					a.log.Error(err, "Can't add to analyzed:", err)
					return err
				}
			} else {
				if err := demoDAO.SetLastVersionAnalyzer(*demo.ID, a.version); err != nil {
					a.log.Error(err, "can't update new version:", err)
					return err
				}
			}
			continue
		}

		if err := a.AddDataAnalyzer([]int64{*demo.ID}); err != nil {
			a.log.Error(err, "Can't add to analyzed:", err)
			return err
		}
	}

	return nil
}

// CheckForEver this check if new demo need to be analyzed
func (a *Analyzer) CheckForEver() {
	defer func() {
		if r := recover(); r != nil {
			a.log.Errorf(nil, "Recovered in Analyzer.CheckForEver %v %s", r, string(debug.Stack()))
			time.Sleep(time.Second * 10)
			go a.CheckForEver()
		}
	}()

	for {
		// to avoid 1/2 chance to analyze even if we want to shut down
		select {
		case <-a.shutdown:
			a.log.Info("analyzer shutdown")
			return
		default:
		}
		select {
		case <-a.shutdown:
			a.log.Info("analyzer shutdown")
			return
		case data := <-a.data:
			a.ParseOneGame(data)
		}
	}
}

// Shutdown shutdown analyzer
func (a *Analyzer) Shutdown() {
	a.shutdown <- struct{}{}
}

// AddDataAnalyzer add data to be analyzed
func (a *Analyzer) AddDataAnalyzer(demoIDs []int64) error {
	for _, ID := range demoIDs {
		select {
		case a.data <- DataAnalyzer{
			demoID: ID,
		}:
		default:
			return ErrChanFull
		}
	}

	return nil
}

// ConvertRounds convert the rounds of the game to fit in the DB
func (a *Analyzer) ConvertRounds(game *modelanalyzer.Game) []*model.Round {
	defer func() {
		if r := recover(); r != nil {
			a.log.Errorf(nil, "Recovered in Analyze %v %s", r, string(debug.Stack()))
		}
	}()

	for steamID := range game.Players {
		for numRound := range game.Rounds {
			score := &modelanalyzer.ScoreFacts{NumRound: numRound + 1}
			game.Players[steamID].ScoreFactsPerRound = append(game.Players[steamID].ScoreFactsPerRound, score)
		}
	}

	for _, player := range game.Players {
		player.Mark.WeaponMark = make(map[common.EquipmentType]*modelanalyzer.WeaponMark)
	}

	modelRounds := []*model.Round{}
	for _, round := range game.Rounds {
		scoreFactsPerPlayer := []*model.ScoreFacts{}
		for _, player := range game.Players {
			score := player.ScoreFactsPerRound[round.NumRound].ConvertForDB(strconv.FormatUint(player.SteamID, 10))
			scoreFactsPerPlayer = append(scoreFactsPerPlayer, score)
		}

		modelFrames := []*model.Frame{}
		for numFrame, frame := range round.Frames {
			modelFrame := &model.Frame{
				NumFrame:             &frame.NumFrame,
				BombPlanted:          &frame.BombPlanted,
				Timer:                &frame.Timer,
				PlayerCarrierSteamID: &frame.PlayerCarrierSteamID,
				BombLastPosDownX:     &frame.BombLastPosDown.X,
				BombLastPosDownY:     &frame.BombLastPosDown.Y,
				BombLastPosDownZ:     &frame.BombLastPosDown.Z,
			}

			if !frame.AddPositions && len(modelFrames) > 0 {
				modelFrame = modelFrames[len(modelFrames)-1]
			}

			for _, event := range frame.Events {
				if event == nil {
					continue
				}

				a.HandleShoots(event, round.Frames, numFrame)

				modelEvent := event.HandleEvent(game, *frame, round)
				if modelEvent == nil {
					continue
				}

				modelEvent.SetFrame(modelFrame)
				modelFrame.Events = append(modelFrame.Events, modelEvent)
			}

			a.SaveStatesGrenades(round.Frames, numFrame)
			modelFrame.Grenades = a.ComputeGrenadesCurrentFrame(frame.Grenades, game.MapName)

			if !frame.AddPositions {
				continue
			}

			if frame.AddPlayersCard {
				modelFrame.PlayersCard = a.ComputePlayersCardCurrentFrame(frame.Players, game.MapName)
			}
			modelFrame.PlayersPositions = a.ComputePlayersPositionsCurrentFrame(frame.Players, game.MapName)
			modelFrames = append(modelFrames, modelFrame)
		}

		var marksPerRoundPlayers []*model.MarksPerRound
		for steamIDPlayer, player := range game.Players {
			if round.MarksPerRound[steamIDPlayer] == nil {
				round.MarksPerRound[steamIDPlayer] = &modelanalyzer.MarksPerRound{
					SteamID:   strconv.FormatUint(steamIDPlayer, 10),
					Side:      modelanalyzer.GetSideFromTeam(player.Team.NumTeam, round.NumRound),
					ConstTeam: player.Team.NumTeam,
				}
			}
			marksPerRound := round.MarksPerRound[steamIDPlayer].ConvertDB()
			marksPerRoundPlayers = append(marksPerRoundPlayers, marksPerRound)
		}

		modelRounds = append(modelRounds, &model.Round{
			NumRound:            &round.NumRound,
			ChatMessage:         round.ConvertDBChatMessage(),
			Frames:              modelFrames,
			SideWin:             &round.SideWin,
			ScoreFactsPerPlayer: scoreFactsPerPlayer,
			MarksPerRound:       marksPerRoundPlayers,
		})
	}

	return modelRounds
}

// SaveStatesGrenades save the highest state of a grenade for the next frame if this one isn't displayed
func (a *Analyzer) SaveStatesGrenades(frames []*modelanalyzer.Frame, numFrame int) {
	newGrenades := frames[numFrame].Grenades
	if numFrame > 0 && !frames[numFrame-1].AddPositions {
		oldGrenades := frames[numFrame-1].Grenades
		for _, oldGrenade := range oldGrenades {
			var newG *modelanalyzer.Grenade
			for _, newGrenade := range frames[numFrame].Grenades {
				if oldGrenade.ID == newGrenade.ID {
					newG = newGrenade
					break
				}
			}
			if newG == nil {
				newGrenades = append(newGrenades, oldGrenade)
				continue
			}
			if int(oldGrenade.State) > int(newG.State) {
				newG.State = oldGrenade.State
				newG.Position = oldGrenade.Position
			}
		}
	}
	frames[numFrame].Grenades = newGrenades
}

// HandleShoots to register the number of shoots in this frame or the next
func (a *Analyzer) HandleShoots(event modelanalyzer.EventAnalyzer, frames []*modelanalyzer.Frame, numFrame int) {
	if event.GetType() == modelanalyzer.WeaponFireEvt {
		if !event.(*modelanalyzer.WeaponFireEvent).WeaponFire.Shooter.IsBot &&
			frames[numFrame].Players[event.(*modelanalyzer.WeaponFireEvent).WeaponFire.Shooter.SteamID64] != nil {
			if frames[numFrame].AddPositions {
				frames[numFrame].Players[event.(*modelanalyzer.WeaponFireEvent).WeaponFire.Shooter.SteamID64].NbrShoots++
			} else {
				if numFrame < len(frames)-1 {
					frames[numFrame+1].Players[event.(*modelanalyzer.WeaponFireEvent).WeaponFire.Shooter.SteamID64].NbrShoots++
				}
			}
		}
	}
}

// ParseOneGame start to analyze one game
func (a *Analyzer) ParseOneGame(data DataAnalyzer) {
	demoDAO := dao.NewDemoDAO(a.db, a.log)
	demo := model.Demo{
		ID: &data.demoID,
	}

	localLogger := a.log.AddField("demoID", data.demoID)

	if err := demoDAO.Get(&demo); err != nil {
		localLogger.Error(err, "error while getting the demo from DAO :", err)
		return
	}

	fileDemo, err := os.Open(*demo.Path)
	if err != nil {
		localLogger.Error(err, "error while opening the demo from its path :", err)
		if strings.Contains(err.Error(), "no such file or directory") {
			if errDelete := demoDAO.Delete(demo); errDelete != nil {
				localLogger.Error(err, "error while deleting the demo :", err)
			}
		}
		return
	}

	defer func() {
		if err := fileDemo.Close(); err != nil {
			localLogger.Error(err, "error while closing the demo :", err.Error())
		}
	}()

	demoUnzippedReader, err := helpers.DemoFileToReader(fileDemo)
	if err != nil {
		localLogger.Error(err, "could not read the demo file : ", err)
		return
	}

	parser := Parser{
		ParserDemoinfocs: demoinfocs.NewParserWithConfig(demoUnzippedReader, demoinfocs.DefaultParserConfig),
		DemoID:           *demo.ID,
		log:              localLogger,
	}

	analyzed, err := demoDAO.IsAnalyzed(*demo.ID)
	if err != nil {
		localLogger.Error(err, "error appended while checking if demo is analyzed :", err)
		return
	}

	if analyzed {
		if *demo.VersionAnalyzer >= a.version {
			return
		}

		if err := demoDAO.DeleteAnalyzes(&demo); err != nil {
			localLogger.Error(err, "error while deletting the demo :", err)
			return
		}
	}
	parser.Parse()
	rounds := a.ConvertRounds(parser.Game)

	parser.Game.Transcript(localLogger, rounds, &demo)
	demo.VersionAnalyzer = helpers.Int64Ptr(a.version)

	if err := demoDAO.SaveAnalyze(demo); err != nil {
		localLogger.Error(err, "error while trying to save the analyzed competitive demo : ", err)
		return
	}
}

// ComputePlayersCardCurrentFrame register every players data for the current frame
func (a *Analyzer) ComputePlayersCardCurrentFrame(
	playersEvent map[uint64]*modelanalyzer.PlayerEvent,
	mapName string,
) []*model.PlayerCard {
	playersCard := []*model.PlayerCard{}

	for _, playerEvent := range playersEvent {
		var grenades string
		for _, grenade := range playerEvent.Grenades {
			grenades += fmt.Sprintf("%s;", grenade)
		}

		playersCard = append(playersCard, &model.PlayerCard{
			IsAlive:          playerEvent.IsAlive,
			IsConnected:      playerEvent.IsConnected,
			IsControllingBot: playerEvent.IsControllingBot,
			Side:             playerEvent.Side,
			PrimaryWeapon:    playerEvent.PrimaryWeapon,
			Pistol:           playerEvent.Pistol,
			Grenades:         grenades,
			HasC4:            playerEvent.HasC4,
			PlayerName:       playerEvent.PlayerName,
			SteamID:          strconv.FormatUint(playerEvent.SteamID, 10),
			Health:           playerEvent.Health,
			Armor:            playerEvent.Armor,
			HasHelmet:        playerEvent.HasHelmet,
			HasDefuseKit:     playerEvent.HasDefuseKit,
			Money:            playerEvent.Money,
		})
	}
	return playersCard
}

// ComputePlayersPositionsCurrentFrame register every players positions for the current frame
func (a *Analyzer) ComputePlayersPositionsCurrentFrame(
	playersEvent map[uint64]*modelanalyzer.PlayerEvent,
	mapName string,
) []*model.PlayerPositions {
	playersPosition := []*model.PlayerPositions{}

	for _, playerEvent := range playersEvent {
		position := analyzerhelper.ConvertMapPos(mapName, playerEvent.Position)

		playersPosition = append(playersPosition, &model.PlayerPositions{
			IsAlive:        playerEvent.IsAlive,
			NbrShoots:      playerEvent.NbrShoots,
			IsDefusing:     playerEvent.IsDefusing,
			IsPlanting:     playerEvent.IsPlanting,
			Side:           playerEvent.Side,
			SteamID:        strconv.FormatUint(playerEvent.SteamID, 10),
			PositionX:      position.X,
			PositionY:      position.Y,
			PositionZ:      position.Z,
			ViewDirectionX: playerEvent.ViewDirectionX,
		})
	}
	return playersPosition
}

// ComputeGrenadesCurrentFrame register every grenades positions for the current frame
func (a *Analyzer) ComputeGrenadesCurrentFrame(
	grenadesAnalyzer []*modelanalyzer.Grenade,
	mapName string,
) []*model.Grenade {
	grenades := []*model.Grenade{}

	for _, grenade := range grenadesAnalyzer {
		var found bool // TODO: fix this bug, it shouldn't happend
		for _, g := range grenades {
			if g.ID == grenade.ID {
				found = true
				break
			}
		}
		if found {
			continue
		}

		position := analyzerhelper.ConvertMapPos(mapName, grenade.Position)

		var fire []float64
		if grenade.PositionFire != nil {
			for _, f := range *grenade.PositionFire {
				pos := analyzerhelper.ConvertMapPos2D(mapName, f)
				fire = append(fire, pos.X, pos.Y)
			}
		}

		grenades = append(grenades, &model.Grenade{
			ID:          grenade.ID,
			PositionX:   position.X,
			PositionY:   position.Y,
			PositionZ:   position.Z,
			State:       model.GrenadeState(grenade.State),
			GrenadeType: grenade.GrenadeType,
			Fire:        fire,
		})
	}

	return grenades
}
