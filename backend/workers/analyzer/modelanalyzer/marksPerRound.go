package modelanalyzer

import (
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

// MarksPerRound represents the marks of a player for 1 round
type MarksPerRound struct {
	SteamID   string
	Side      int
	ConstTeam int

	NbrBulletsFired      int
	NbrBulletsHit        int
	NbrBulletsHS         int
	NbrFirstBulletsFired int
	NbrFirstBulletsHit   int
	NbrFirstBulletsHS    int

	Damage                    int
	UtilityDamage             int
	CumulativeVelocityShoots  int
	CumulativeDeltaXCrossHair float64
	CumulativeDeltaYCrossHair float64
	NbrDeaths                 int
	NbrKills                  int
}

// AddKill add a kill
func (mpr *MarksPerRound) AddKill() {
	mpr.NbrKills++
}

// AddDeath add a death
func (mpr *MarksPerRound) AddDeath() {
	mpr.NbrDeaths++
}

// AddShoot add a shoot
func (mpr *MarksPerRound) AddShoot(
	weapon common.EquipmentType,
	velocity int,
	isFirstBullet bool,
) {
	if weapon.Class() == common.EqClassEquipment || weapon.Class() == common.EqClassGrenade {
		return
	}
	mpr.NbrBulletsFired++
	mpr.CumulativeVelocityShoots += velocity

	if isFirstBullet {
		mpr.NbrFirstBulletsFired++
	}
}

// AddShootHit add a shoot hit
func (mpr *MarksPerRound) AddShootHit(
	isHS bool, healthDamage int,
	weapon common.EquipmentType,
	velocity int,
	deltaX, deltaY float64,
	isFirstBullet bool,
) {
	mpr.Damage += healthDamage

	if weapon.Class() == common.EqClassGrenade {
		mpr.UtilityDamage += healthDamage
	}
	if weapon.Class() == common.EqClassEquipment || weapon.Class() == common.EqClassGrenade {
		return
	}

	mpr.NbrBulletsFired++
	mpr.NbrBulletsHit++
	mpr.CumulativeVelocityShoots += velocity

	if isHS {
		mpr.NbrBulletsHS++
	}

	if isFirstBullet {
		mpr.NbrFirstBulletsFired++
		mpr.NbrFirstBulletsHit++
		mpr.CumulativeDeltaXCrossHair += deltaX
		mpr.CumulativeDeltaYCrossHair += deltaY
		if isHS {
			mpr.NbrFirstBulletsHS++
		}
	}
}

// ConvertDB convert for the DB
func (mpr *MarksPerRound) ConvertDB() *model.MarksPerRound {
	var (
		accuracy               float64
		hs                     float64
		firstBulletAccuracy    float64
		firstBulletHS          float64
		averageVelocityShoots  int
		averageDeltaXCrossHair int
		averageDeltaYCrossHair int
	)

	if mpr.NbrBulletsFired > 0 {
		accuracy = float64(mpr.NbrBulletsHit) / float64(mpr.NbrBulletsFired)
	}

	if mpr.NbrBulletsHit > 0 {
		hs = float64(mpr.NbrBulletsHS) / float64(mpr.NbrBulletsHit)
	}

	if mpr.NbrFirstBulletsFired > 0 {
		firstBulletAccuracy = float64(mpr.NbrFirstBulletsHit) / float64(mpr.NbrFirstBulletsFired)
	}

	if mpr.NbrFirstBulletsHit > 0 {
		firstBulletHS = float64(mpr.NbrFirstBulletsHS) / float64(mpr.NbrFirstBulletsHit)
		averageDeltaXCrossHair = int(mpr.CumulativeDeltaXCrossHair / float64(mpr.NbrFirstBulletsHit))
		averageDeltaYCrossHair = int(mpr.CumulativeDeltaYCrossHair / float64(mpr.NbrFirstBulletsHit))
	}

	if mpr.NbrBulletsFired > 0 {
		averageVelocityShoots = int(float64(mpr.CumulativeVelocityShoots) / float64(mpr.NbrBulletsFired))
	}

	return &model.MarksPerRound{
		SteamID:                &mpr.SteamID,
		Side:                   &mpr.Side,
		ConstTeam:              &mpr.ConstTeam,
		NbrBulletsFired:        &mpr.NbrBulletsFired,
		NbrBulletsHit:          &mpr.NbrBulletsHit,
		NbrBulletsHS:           &mpr.NbrBulletsHS,
		NbrFirstBulletsFired:   &mpr.NbrFirstBulletsFired,
		NbrFirstBulletsHit:     &mpr.NbrFirstBulletsHit,
		NbrFirstBulletsHS:      &mpr.NbrFirstBulletsHS,
		Accuracy:               &accuracy,
		HS:                     &hs,
		FirstBulletAccuracy:    &firstBulletAccuracy,
		FirstBulletHS:          &firstBulletHS,
		AverageVelocityShoots:  &averageVelocityShoots,
		AverageDeltaXCrossHair: &averageDeltaXCrossHair,
		AverageDeltaYCrossHair: &averageDeltaYCrossHair,
		Damage:                 &mpr.Damage,
		UtilityDamage:          &mpr.UtilityDamage,
		NbrDeaths:              &mpr.NbrDeaths,
		NbrKills:               &mpr.NbrKills,
	}
}
