package modelanalyzer

import (
	"math/rand"
	"strconv"

	"github.com/golang/geo/r3"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

// GenerateRandomPlayerEvent generate a random PlayerEvent
func GenerateRandomPlayerEvent() *PlayerEvent {
	steamID := rand.Uint64()
	return &PlayerEvent{
		IsAlive:          helpers.RandBool(),
		IsConnected:      helpers.RandBool(),
		IsControllingBot: helpers.RandBool(),
		Side:             rand.Int()%2 + 1,
		PrimaryWeapon:    helpers.RandomString(4),
		Pistol:           helpers.RandomString(4),
		Grenades:         []string{helpers.RandomString(4), helpers.RandomString(4)},
		HasC4:            helpers.RandBool(),
		PlayerName:       helpers.RandomString(10),
		SteamID:          steamID,
		PlayerSteamID:    strconv.FormatUint(steamID, 10),
		Position:         r3.Vector{X: 1, Y: 1, Z: 1},
		Health:           rand.Int() % 101,
		Armor:            rand.Int() % 101,
		HasHelmet:        helpers.RandBool(),
		HasDefuseKit:     helpers.RandBool(),
		Money:            rand.Int() % 16001,
		EquipmentValue:   rand.Int() % 7001,
		ViewDirectionX:   rand.Float64() * 360,
		ViewDirectionY:   rand.Float64() * 360,
	}
}
