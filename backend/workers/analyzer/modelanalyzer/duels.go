package modelanalyzer

// DuelIndex index used for map duels
type DuelIndex struct {
	NbrAliveAllies   int
	NbrAliveEnnemies int
}

// Duel store if the duel has been won or not, and the players alives on the same team
type Duel struct {
	Won          bool
	PlayersAlive []uint64
}
