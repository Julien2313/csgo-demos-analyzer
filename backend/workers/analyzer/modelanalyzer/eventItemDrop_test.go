package modelanalyzer_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
)

func TestGetType_ItemDropEvent(t *testing.T) {
	assert.Equal(t, modelanalyzer.ItemDropEvt, (&modelanalyzer.ItemDropEvent{}).GetType())
}
func TestHandleEvent_ItemDropEvent(t *testing.T) {
	itemDropEvent := modelanalyzer.ItemDropEvent{}

	assert.Nil(t, itemDropEvent.HandleEvent(
		&modelanalyzer.Game{},
		modelanalyzer.Frame{},
		&modelanalyzer.Round{}))
}
