package modelanalyzer

import (
	"time"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
)

// HeatMapKill type to store kills
type HeatMapKill struct {
	DurationSinceRoundBegan *time.Duration

	WeaponKiller  *common.EquipmentType
	SteamIDKiller *string
	SideKiller    *int
	KillerPosX    *float64
	KillerPosY    *float64

	ActiveWeaponVictim *common.EquipmentType
	SteamIDVictim      *string
	SideVictim         *int
	VictimPosX         *float64
	VictimPosY         *float64

	HitGroup *int
}

// HeatMapDmg type to store dmgs
type HeatMapDmg struct {
	DurationSinceRoundBegan *time.Duration

	WeaponShooter  *common.EquipmentType
	SteamIDShooter *string
	SideShooter    *int
	ShooterPosX    *float64
	ShooterPosY    *float64

	ActiveWeaponVictim *common.EquipmentType
	SteamIDVictim      *string
	SideVictim         *int
	VictimPosX         *float64
	VictimPosY         *float64

	Dmg      *int
	HitGroup *int
}
