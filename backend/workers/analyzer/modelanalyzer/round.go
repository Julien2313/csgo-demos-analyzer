package modelanalyzer

import (
	"math"
	"time"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gonum.org/v1/gonum/mat"
)

// Round a specific round with events during the game
type Round struct {
	NumRound            int
	Frames              []*Frame
	SideWin             int
	ScoreFactsPerPlayer map[uint64]*ScoreFacts
	MarksPerRound       map[uint64]*MarksPerRound
	Grenades            map[int]*Grenade

	HeatMapKills []*HeatMapKill
	HeatMapDmgs  []*HeatMapDmg

	ChatMessages []ChatMessage

	TimeBombPlanted   *time.Duration
	TimeRoundStarted  time.Duration
	TimeFreezeTimeEnd *time.Duration
}

// ConvertDBChatMessage convert a list of messages for the DB
func (r *Round) ConvertDBChatMessage() []*model.ChatMessage {
	chatMessages := []*model.ChatMessage{}
	for _, chatMessage := range r.ChatMessages {
		chatMessages = append(chatMessages, chatMessage.ConvertForDB())
	}

	return chatMessages
}

// GetDeltasCrossHairPlacement return the absolute differene of angle between
// the current frame and the frame alst second ago
func (r *Round) GetDeltasCrossHairPlacement(currentFrame Frame, steamIDToFind uint64) (float64, float64) {
	oldFrame := r.GetOldFrame(currentFrame, time.Millisecond*500)
	if oldFrame == nil {
		return 0, 0
	}

	oldPlayerEvent := oldFrame.GetPlayer(steamIDToFind)
	playerEvent := currentFrame.GetPlayer(steamIDToFind)
	if oldPlayerEvent == nil || playerEvent == nil {
		return 0, 0
	}

	deltaX := math.Abs(helpers.PMod(playerEvent.ViewDirectionX-oldPlayerEvent.ViewDirectionX+180.0, 360.0) - 180)
	deltaY := math.Abs(helpers.PMod(playerEvent.ViewDirectionY-oldPlayerEvent.ViewDirectionY+180.0, 360.0) - 180)

	return deltaX, deltaY
}

// GetOldFrame return the frame a second ago
func (r *Round) GetOldFrame(currentFrame Frame, duration time.Duration) *Frame {
	for numFrame := len(r.Frames) - 1; numFrame >= 0; numFrame-- {
		f := r.Frames[numFrame]
		if currentFrame.IGTime-f.IGTime > duration {
			return f
		}
	}

	return nil
}

// ComputeMatrixSightWithLag compute the matrix of a player used to calculate the distance
func (r *Round) ComputeMatrixSightWithLag(frame Frame, steamIDPlayer uint64) *mat.Dense {
	p := frame.GetPlayer(steamIDPlayer)
	oldFrame := r.GetOldFrame(frame, time.Millisecond*time.Duration(p.Ping*2))
	if oldFrame == nil {
		oldFrame = &frame
	}
	p = oldFrame.GetPlayer(steamIDPlayer)
	if p == nil {
		return nil
	}

	viewX := p.ViewDirectionX
	viewY := p.ViewDirectionY
	posX := p.Position.X
	posY := p.Position.Y
	posZ := p.Position.Z

	theta := float64(viewX) * math.Pi / 180
	phi := float64(viewY) * math.Pi / 180

	cosTheta := math.Cos(theta)
	cosPhi := math.Cos(phi)
	sinTheta := math.Sin(theta)
	sinPhi := math.Sin(phi)

	pRw := mat.NewDense(3, 3,
		[]float64{
			cosTheta * cosPhi, sinTheta * cosPhi, -sinPhi,
			-sinTheta, cosTheta, 0,
			cosTheta * sinPhi, sinTheta * sinPhi, cosPhi,
		})

	var npRw mat.Dense
	npRw.Mul(pRw, mat.NewDiagDense(3, []float64{-1, -1, -1}))

	wt0w := mat.NewDense(3, 1,
		[]float64{
			posX,
			posY,
			posZ,
		})
	var vpRw mat.Dense
	vpRw.Mul(&npRw, wt0w)
	pTw := mat.NewDense(4, 4,
		[]float64{
			pRw.At(0, 0), pRw.At(0, 1), pRw.At(0, 2), vpRw.At(0, 0),
			pRw.At(1, 0), pRw.At(1, 1), pRw.At(1, 2), vpRw.At(1, 0),
			pRw.At(2, 0), pRw.At(2, 1), pRw.At(2, 2), vpRw.At(2, 0),
			0, 0, 0, 1,
		})

	return pTw

}

// IsDistancePlayersFromSightAtAMinimum returns true if the distance is less than the minimum sent
func (r *Round) IsDistancePlayersFromSightAtAMinimum(
	frame Frame,
	steamIDPlayer uint64,
	distanceMinimum float64,
) (bool, []*PlayerEvent) {
	if !frame.Players[steamIDPlayer].IsAlive {
		return false, nil
	}

	pTw := r.ComputeMatrixSightWithLag(frame, steamIDPlayer)
	if pTw == nil {
		return false, nil
	}
	var (
		playerFound    bool
		closestPlayers []*PlayerEvent
	)

	for victimSteamID, playerAimed := range frame.Players {
		if playerAimed.Side == frame.Players[steamIDPlayer].Side || !playerAimed.IsAlive {
			continue
		}
		v := frame.Players[victimSteamID]
		oldFrame := r.GetOldFrame(frame, time.Millisecond*time.Duration(v.Ping*2))
		if oldFrame == nil {
			oldFrame = &frame
		}

		oldPlayerAimed := oldFrame.Players[victimSteamID]

		pEn := mat.NewDense(4, 1,
			[]float64{
				oldPlayerAimed.Position.X,
				oldPlayerAimed.Position.Y,
				oldPlayerAimed.Position.Z,
				1,
			})

		var res mat.Dense
		res.Mul(pTw, pEn)

		x := res.At(0, 0)
		y := res.At(1, 0)
		z := res.At(2, 0)

		playerDistance := math.Sqrt(y*y + z*z)
		if playerDistance <= distanceMinimum && x >= 0 {
			closestPlayers = append(closestPlayers, playerAimed)
			playerFound = true
		}
	}

	return playerFound, closestPlayers
}
