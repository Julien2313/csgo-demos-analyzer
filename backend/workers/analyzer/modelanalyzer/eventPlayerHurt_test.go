package modelanalyzer_test

import (
	"math/rand"
	"testing"
	"time"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
)

func TestGetType_PlayerHurtEvent(t *testing.T) {
	assert.Equal(t, modelanalyzer.PlayerHurtEvt, (&modelanalyzer.PlayerHurtEvent{}).GetType())
}

func TestHandleEvent_PlayerHurt(t *testing.T) {
	playerHurtEvent := modelanalyzer.PlayerHurtEvent{
		PlayerHurt: events.PlayerHurt{},
		TimeEvent:  time.Millisecond * 16871,
	}
	playerHurtEventFront := playerHurtEvent.HandleEvent(
		&modelanalyzer.Game{},
		modelanalyzer.Frame{},
		&modelanalyzer.Round{},
	)
	assert.Nil(t, playerHurtEventFront)

	steamIDAttacker := rand.Uint64()
	steamIDVictim := rand.Uint64()

	playerHurtEvent = modelanalyzer.PlayerHurtEvent{
		PlayerHurt: events.PlayerHurt{
			Attacker:     &common.Player{Name: helpers.RandomString(30), Team: common.Team(2), SteamID64: steamIDAttacker},
			Player:       &common.Player{Name: helpers.RandomString(30), Team: common.Team(3), SteamID64: steamIDVictim},
			Weapon:       &common.Equipment{Type: common.EqAK47},
			HealthDamage: 10,
			HitGroup:     events.HitGroupHead,
		},
		TimeEvent: time.Millisecond * 16871,
	}

	team1 := &modelanalyzer.Team{NumTeam: 2}
	team2 := &modelanalyzer.Team{NumTeam: 3}
	players := map[uint64]*modelanalyzer.Player{
		steamIDAttacker: {
			SteamID:            steamIDAttacker,
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
			Team:               team1,
		},
		steamIDVictim: {
			SteamID:            steamIDVictim,
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
			Team:               team2,
		},
	}

	round := modelanalyzer.Round{
		MarksPerRound:     make(map[uint64]*modelanalyzer.MarksPerRound),
		NumRound:          1,
		TimeFreezeTimeEnd: helpers.DurationPtr(time.Second * 15),
		SideWin:           2,
	}
	frame := modelanalyzer.Frame{
		Players: map[uint64]*modelanalyzer.PlayerEvent{
			steamIDAttacker: {
				Side:    2,
				IsAlive: true,
			},
			steamIDVictim: {
				Side:         3,
				IsAlive:      true,
				Health:       10,
				ActiveWeapon: common.EqAK47,
			},
		},
	}

	playerHurtEventFront = playerHurtEvent.HandleEvent(&modelanalyzer.Game{Players: players}, frame, &round)
	assert.Nil(t, playerHurtEventFront)

	assert.Equal(t, 15.0, players[steamIDAttacker].ScoreFactsPerRound[1].AttackScore.DamageScore)
	assert.Equal(t, 0, players[steamIDAttacker].Mark.UtilityDamage)
	assert.Equal(t, 10, players[steamIDAttacker].Mark.NbrDamageGave)
	assert.Equal(t, 1, players[steamIDAttacker].Mark.NbrShoots)
	assert.Equal(t, 1, players[steamIDAttacker].Mark.NbrShootsHit)
	assert.Equal(t, 1, players[steamIDAttacker].Mark.NbrShootsHS)

	team1 = &modelanalyzer.Team{NumTeam: 2}
	team2 = &modelanalyzer.Team{NumTeam: 3}
	players = map[uint64]*modelanalyzer.Player{
		steamIDAttacker: {
			SteamID:            steamIDAttacker,
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
			Team:               team1,
		},
		steamIDVictim: {
			SteamID:            steamIDVictim,
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
			Team:               team2,
		},
	}

	round = modelanalyzer.Round{
		MarksPerRound:     make(map[uint64]*modelanalyzer.MarksPerRound),
		NumRound:          1,
		TimeFreezeTimeEnd: helpers.DurationPtr(time.Second * 15),
		SideWin:           2,
	}
	frame = modelanalyzer.Frame{
		Players: map[uint64]*modelanalyzer.PlayerEvent{
			steamIDAttacker: {
				Side:    2,
				IsAlive: true,
			},
			steamIDVictim: {
				Side:    3,
				IsAlive: true,
				Health:  10,
			},
		},
	}

	playerHurtEvent = modelanalyzer.PlayerHurtEvent{
		PlayerHurt: events.PlayerHurt{
			Attacker:     &common.Player{Name: helpers.RandomString(30), Team: common.Team(2), SteamID64: steamIDAttacker},
			Player:       &common.Player{Name: helpers.RandomString(30), Team: common.Team(3), SteamID64: steamIDVictim},
			Weapon:       &common.Equipment{Type: common.EqHE},
			HealthDamage: 10,
		},
		TimeEvent: time.Millisecond * 16871,
	}
	playerHurtEventFront = playerHurtEvent.HandleEvent(&modelanalyzer.Game{Players: players}, frame, &round)
	assert.Nil(t, playerHurtEventFront)

	assert.Equal(t, 15.0, players[steamIDAttacker].ScoreFactsPerRound[1].AttackScore.DamageScore)
	assert.Equal(t, 10, players[steamIDAttacker].Mark.UtilityDamage)
	assert.Equal(t, 10, players[steamIDAttacker].Mark.NbrDamageGave)
	assert.Equal(t, 0, players[steamIDAttacker].Mark.NbrShoots)
	assert.Equal(t, 0, players[steamIDAttacker].Mark.NbrShootsHit)
	assert.Equal(t, 0, players[steamIDAttacker].Mark.NbrShootsHS)
}
