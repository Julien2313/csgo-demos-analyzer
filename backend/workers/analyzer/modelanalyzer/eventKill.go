package modelanalyzer

import (
	"strconv"
	"time"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzerhelper"
)

// KillEvent event of a kill
type KillEvent struct {
	Kill      events.Kill
	TimeEvent time.Duration
}

// GetType type of event
func (ke *KillEvent) GetType() EventType {
	return KillEvt
}

// ComputeMultiplicatorAttack return a coef for an attack event
func ComputeMultiplicatorAttack(players map[uint64]*PlayerEvent,
	equipmentValueAttacker, equipmentValueVictim int,
	teamCurrentPlayer int,
	won, teamKill bool) float64 {
	coefAlive := ComputeCoefAlive(players, teamCurrentPlayer)

	var coefWon float64
	if won {
		coefWon = 1.5
	} else {
		coefWon = 0.66
	}

	coefEco := 1.0
	if equipmentValueAttacker != 0.0 {
		coefEco = float64(equipmentValueVictim) / float64(equipmentValueAttacker)
		if coefEco > 2 {
			coefEco = 2
		} else if coefEco < 0.5 {
			coefEco = 0.5
		}
	}

	if teamKill {
		return -coefAlive * coefWon * coefEco
	}
	return coefAlive * coefWon * coefEco
}

func (ke *KillEvent) manageMatrixDuels(game *Game, frame Frame, round *Round, killerSide int) {
	alliesAlive, ennemiesAlive := returnSteamsIDAlive(frame.Players, killerSide)
	if len(alliesAlive) != 0 && len(ennemiesAlive) != 0 {
		counterTeam := 5 - killerSide
		duelKillerTeam := Duel{PlayersAlive: alliesAlive}
		duelOtherTeam := Duel{PlayersAlive: ennemiesAlive}

		if round.SideWin == killerSide {
			duelKillerTeam.Won = true
		} else {
			duelOtherTeam.Won = true
		}

		duelsKillerTeam := game.Teams[killerSide].DuelsMatrix[DuelIndex{len(alliesAlive), len(ennemiesAlive)}]
		duelsOtherTeam := game.Teams[counterTeam].DuelsMatrix[DuelIndex{len(ennemiesAlive), len(alliesAlive)}]

		if round.NumRound >= 15 {
			duelsKillerTeam = game.Teams[counterTeam].DuelsMatrix[DuelIndex{len(alliesAlive), len(ennemiesAlive)}]
			duelsOtherTeam = game.Teams[killerSide].DuelsMatrix[DuelIndex{len(ennemiesAlive), len(alliesAlive)}]
		}

		*duelsKillerTeam = append(*duelsKillerTeam, duelKillerTeam)
		*duelsOtherTeam = append(*duelsOtherTeam, duelOtherTeam)
	}
}

// HandleEvent return a kill event, addind score
func (ke *KillEvent) HandleEvent(game *Game, frame Frame, round *Round) model.Event {
	killerName, killerSteamID, killerSide, killerEquipmentValue := GetGeneralDataFromPlayer(ke.Kill.Killer, frame.Players)
	_, assisterSteamID, assisterSide, _ := GetGeneralDataFromPlayer(ke.Kill.Assister, frame.Players)
	victimName, victimSteamID, victimSide, victimEquipmentValue := GetGeneralDataFromPlayer(ke.Kill.Victim, frame.Players)

	ke.manageMatrixDuels(game, frame, round, killerSide)

	var weaponKillType *common.EquipmentType
	var weaponName string
	if ke.Kill.Weapon != nil {
		weaponKillType = &ke.Kill.Weapon.Type
		weaponName = ke.Kill.Weapon.String()
	}

	killer := game.Players[killerSteamID]
	assister := game.Players[assisterSteamID]
	victim := game.Players[victimSteamID]
	if killer != nil {
		killer.Kills = append(killer.Kills, &Kill{
			HeadShot: ke.Kill.IsHeadshot,
			WallBang: ke.Kill.IsWallBang(),
			Frame:    frame.NumFrame,
			TeamKill: killerSide == victimSide,
			Killer:   killer,
			Assist:   assister,
			Victim:   victim,
			Weapon:   weaponName,
		})
		score := ComputeMultiplicatorAttack(frame.Players,
			killerEquipmentValue, victimEquipmentValue,
			killerSide,
			round.SideWin == killerSide, victimSide == killerSide)

		killer.ScoreFactsPerRound[round.NumRound].AttackScore.KillScore += (killScore * score)
		killer.Mark.AddKill(*weaponKillType)
	}

	if assister != nil {
		assister.Assists = append(assister.Assists, &Assist{
			TeamKill: assisterSide == victimSide,
		})

		score := ComputeMultiplicatorAttack(frame.Players,
			1, 1,
			assisterSide,
			round.SideWin == assisterSide, victimSide == assisterSide)
		assister.ScoreFactsPerRound[round.NumRound].AttackScore.AssistKillScore += (assistKillScore * score)
	}

	var weaponVictimType *common.EquipmentType
	if victim != nil {
		weaponVictimType = &frame.Players[victim.SteamID].ActiveWeapon
		stuffValue := ComputeStuffValue(frame.Players[victim.SteamID].Grenades)
		victim.Mark.AddStuffLost(stuffValue, *weaponKillType)

		victim.Deaths = append(victim.Deaths, &Death{})

		score := ComputeMultiplicatorAttack(frame.Players,
			victimEquipmentValue, killerEquipmentValue,
			victimSide,
			round.SideWin == victimSide, false)
		victim.ScoreFactsPerRound[round.NumRound].AttackScore.DeathScore -= (deathScore * score)
	}

	var timeKill time.Duration
	if round.TimeBombPlanted != nil && *round.TimeBombPlanted < ke.TimeEvent {
		timeKill = time.Second*40 - (ke.TimeEvent - *round.TimeBombPlanted)
	} else {
		timeKill = ke.TimeEvent - *round.TimeFreezeTimeEnd
	}

	killerPosX, killerPosY := ke.computePositionPlayer(game.MapName, ke.Kill.Killer)
	victimPosX, victimPosY := ke.computePositionPlayer(game.MapName, ke.Kill.Victim)

	round.HeatMapKills = append(round.HeatMapKills, &HeatMapKill{
		DurationSinceRoundBegan: helpers.DurationPtr(ke.TimeEvent - *round.TimeFreezeTimeEnd),
		WeaponKiller:            weaponKillType,
		SteamIDKiller:           helpers.StrPtr(strconv.FormatUint(killerSteamID, 10)),
		SideKiller:              &killerSide,
		KillerPosX:              killerPosX,
		KillerPosY:              killerPosY,
		ActiveWeaponVictim:      weaponVictimType,
		SteamIDVictim:           helpers.StrPtr(strconv.FormatUint(victimSteamID, 10)),
		SideVictim:              &victimSide,
		VictimPosX:              victimPosX,
		VictimPosY:              victimPosY,
	})

	return &model.KillEvent{
		TimeKill:   &timeKill,
		KillerSide: &killerSide,
		VictimSide: &victimSide,
		IsHeadShot: &ke.Kill.IsHeadshot,
		IsWallBang: helpers.BoolPtr(ke.Kill.IsWallBang()),
		KillerName: &killerName,
		VictimName: &victimName,
		Weapon:     &weaponName,
	}

}

func (ke *KillEvent) computePositionPlayer(mapName string, player *common.Player) (
	killerPosX *float64,
	killerPosY *float64,
) {
	if player != nil {
		killerPos := analyzerhelper.ConvertMapPos(mapName, player.LastAlivePosition)
		killerPosX = &killerPos.X
		killerPosY = &killerPos.Y
	}
	return killerPosX, killerPosY
}
