package modelanalyzer

const (
	// bombDefuseScore score for defusing the bomb
	bombDefuseScore = 10
	// bombDefuseScore score for planting the bomb
	bombPlantedScore = 10
	// killScore score for killing a player
	killScore = 20
	// assistKillScore score for assisting a kill
	assistKillScore = 10
	// deathScore score for dying
	deathScore = 10
	// flashScore score flashing a player per second
	flashScore = 3
	// ninjaDefuseScore score added when a ninja defuse occured
	ninjaDefuseScore = 100
)
