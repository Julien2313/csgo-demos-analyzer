package modelanalyzer

// Assist type of an assist occurred during a match
type Assist struct {
	Kill     *Kill
	TeamKill bool
}
