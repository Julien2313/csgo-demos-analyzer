package modelanalyzer

import (
	"time"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

// GrenadeProjectileThrowEvent event of a flashed player
type GrenadeProjectileThrowEvent struct {
	GrenadeProjectileThrow events.GrenadeProjectileThrow
	TimeEvent              time.Duration
}

// GetType type of event
func (gpt *GrenadeProjectileThrowEvent) GetType() EventType {
	return GrenadeProjectileThrowEvt
}

// HandleEvent return a player flashed event, addind score
func (gpt *GrenadeProjectileThrowEvent) HandleEvent(game *Game, frame Frame, round *Round) model.Event {
	return nil
}
