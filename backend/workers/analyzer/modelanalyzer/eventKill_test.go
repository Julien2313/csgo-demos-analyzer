package modelanalyzer_test

import (
	"math/rand"
	"testing"
	"time"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
)

func TestGetType_KillEvent(t *testing.T) {
	assert.Equal(t, modelanalyzer.KillEvt, (&modelanalyzer.KillEvent{}).GetType())
}

func TestComputeMultiplicatorAttack(t *testing.T) {
	assert.Equal(t, 1.5,
		modelanalyzer.ComputeMultiplicatorAttack(map[uint64]*modelanalyzer.PlayerEvent{}, 1, 1, 1, true, false))
	assert.Equal(t, 0.66,
		modelanalyzer.ComputeMultiplicatorAttack(map[uint64]*modelanalyzer.PlayerEvent{}, 1, 1, 1, false, false))
	assert.Equal(t, 1.5,
		modelanalyzer.ComputeMultiplicatorAttack(map[uint64]*modelanalyzer.PlayerEvent{}, 1, 1, 1, true, false))

	players := map[uint64]*modelanalyzer.PlayerEvent{
		rand.Uint64(): {
			IsAlive: true,
			Side:    2,
		},
		rand.Uint64(): {
			IsAlive: true,
			Side:    2,
		},
		rand.Uint64(): {
			IsAlive: true,
			Side:    2,
		},
		rand.Uint64(): {
			IsAlive: true,
			Side:    1,
		},
		rand.Uint64(): {
			IsAlive: true,
			Side:    1,
		},
		rand.Uint64(): {
			IsAlive: false,
			Side:    1,
		},
	}
	assert.Equal(t, 1.5*1.1, helpers.Round(modelanalyzer.ComputeMultiplicatorAttack(players, 1, 1, 1, true, false), 2))
}

func TestHandleEvent_Kill(t *testing.T) {
	steamIDKiller := rand.Uint64()
	steamIDAssister := rand.Uint64()
	steamIDVictim := rand.Uint64()

	killEvent := modelanalyzer.KillEvent{
		Kill: events.Kill{
			Killer:   &common.Player{Name: helpers.RandomString(30), Team: common.Team(2), SteamID64: steamIDKiller},
			Assister: &common.Player{Name: helpers.RandomString(30), Team: common.Team(2), SteamID64: steamIDAssister},
			Victim:   &common.Player{Name: helpers.RandomString(30), Team: common.Team(3), SteamID64: steamIDVictim},
			Weapon:   &common.Equipment{Type: common.EqAK47},
		},
		TimeEvent: time.Millisecond * 16871,
	}

	team1 := &modelanalyzer.Team{NumTeam: 2}
	team2 := &modelanalyzer.Team{NumTeam: 3}
	players := map[uint64]*modelanalyzer.Player{
		steamIDKiller: {
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
			Team:               team1,
			SteamID:            steamIDKiller,
		},
		steamIDAssister: {
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
			Team:               team1,
			SteamID:            steamIDAssister,
		},
		steamIDVictim: {
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
			Team:               team2,
			SteamID:            steamIDVictim,
		},
	}

	round := modelanalyzer.Round{
		NumRound:          1,
		TimeFreezeTimeEnd: helpers.DurationPtr(time.Second * 15),
		SideWin:           2,
	}
	frame := modelanalyzer.Frame{
		Players: map[uint64]*modelanalyzer.PlayerEvent{
			steamIDKiller: {
				Side:     2,
				IsAlive:  true,
				Grenades: []string{},
			},
			steamIDAssister: {
				Side:     2,
				IsAlive:  true,
				Grenades: []string{},
			},
			steamIDVictim: {
				Side:     3,
				IsAlive:  true,
				Grenades: []string{"Flashbang"},
			},
		},
	}
	teams := map[int]*modelanalyzer.Team{
		2: {
			DuelsMatrix: make(map[modelanalyzer.DuelIndex]*[]modelanalyzer.Duel),
		},
		3: {
			DuelsMatrix: make(map[modelanalyzer.DuelIndex]*[]modelanalyzer.Duel),
		},
	}

	for a := 1; a <= 5; a++ {
		for e := 1; e <= 5; e++ {
			index := modelanalyzer.DuelIndex{
				NbrAliveAllies:   a,
				NbrAliveEnnemies: e,
			}
			teams[2].DuelsMatrix[index] = &[]modelanalyzer.Duel{}
			index = modelanalyzer.DuelIndex{
				NbrAliveAllies:   a,
				NbrAliveEnnemies: e,
			}
			teams[3].DuelsMatrix[index] = &[]modelanalyzer.Duel{}
		}
	}

	game := &modelanalyzer.Game{Players: players, Teams: teams}

	killEventFront := killEvent.HandleEvent(game, frame, &round).(*model.KillEvent)

	assert.Equal(t, 27.0, players[steamIDKiller].ScoreFactsPerRound[1].AttackScore.KillScore)
	assert.Equal(t, 13.5, players[steamIDAssister].ScoreFactsPerRound[1].AttackScore.AssistKillScore)
	assert.Equal(t, -7.26, helpers.Round(players[steamIDVictim].ScoreFactsPerRound[1].AttackScore.DeathScore, 2))

	assert.Equal(t, time.Millisecond*1871, *killEventFront.TimeKill)
	assert.Equal(t, 2, *killEventFront.KillerSide)
	assert.Equal(t, 3, *killEventFront.VictimSide)
	assert.Equal(t, false, *killEventFront.IsHeadShot)
	assert.Equal(t, false, *killEventFront.IsWallBang)
	assert.Equal(t, killEvent.Kill.Killer.Name, *killEventFront.KillerName)
	assert.Equal(t, killEvent.Kill.Victim.Name, *killEventFront.VictimName)
	assert.Equal(t, "AK-47", *killEventFront.Weapon)

	assert.Equal(t, 200, players[steamIDVictim].Mark.DollarStuffLost)
}
