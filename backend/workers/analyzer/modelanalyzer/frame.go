package modelanalyzer

import (
	"time"

	"github.com/golang/geo/r3"
)

// Frame a frame IG
type Frame struct {
	NumFrame             int
	BombLastPosDown      r3.Vector
	BombPlanted          bool
	PlayerCarrierSteamID string
	IGTime               time.Duration
	Timer                string
	Events               []EventAnalyzer
	Players              map[uint64]*PlayerEvent
	AddPlayersCard       bool
	AddPositions         bool
	Grenades             []*Grenade
}

// GetPlayer return the player
func (f *Frame) GetPlayer(steamIDToFind uint64) *PlayerEvent {
	for steamID, player := range f.Players {
		if steamIDToFind == steamID {
			return player
		}
	}

	return nil
}
