package modelanalyzer_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
)

func TestMarksPerRound_AddShoot(t *testing.T) {
	m := &modelanalyzer.MarksPerRound{}
	m.AddShoot(0, 100, true)
	assert.Equal(t, 1, m.NbrBulletsFired)
	assert.Equal(t, 1, m.NbrFirstBulletsFired)
	assert.Equal(t, 100, m.CumulativeVelocityShoots)

	m.AddShoot(0, 50, false)
	assert.Equal(t, 2, m.NbrBulletsFired)
	assert.Equal(t, 1, m.NbrFirstBulletsFired)
	assert.Equal(t, 150, m.CumulativeVelocityShoots)
}

func TestMarksPerRound_AddShootHit(t *testing.T) {
	m := &modelanalyzer.MarksPerRound{}
	m.AddShootHit(false, 100, 0, 66, 10, 20, true)
	assert.Equal(t, 1, m.NbrFirstBulletsFired)
	assert.Equal(t, 1, m.NbrFirstBulletsHit)
	assert.Equal(t, 0, m.NbrFirstBulletsHS)
	assert.Equal(t, 1, m.NbrBulletsFired)
	assert.Equal(t, 1, m.NbrBulletsHit)
	assert.Equal(t, 0, m.NbrBulletsHS)
	assert.Equal(t, 100, m.Damage)
	assert.Equal(t, 66, m.CumulativeVelocityShoots)
	assert.Equal(t, 10.0, m.CumulativeDeltaXCrossHair)
	assert.Equal(t, 20.0, m.CumulativeDeltaYCrossHair)

	m.AddShootHit(true, 100, 0, 66, 10, 20, true)
	assert.Equal(t, 2, m.NbrFirstBulletsFired)
	assert.Equal(t, 2, m.NbrFirstBulletsHit)
	assert.Equal(t, 1, m.NbrFirstBulletsHS)
	assert.Equal(t, 2, m.NbrBulletsFired)
	assert.Equal(t, 2, m.NbrBulletsHit)
	assert.Equal(t, 1, m.NbrBulletsHS)
	assert.Equal(t, 200, m.Damage)
	assert.Equal(t, 132, m.CumulativeVelocityShoots)
	assert.Equal(t, 20.0, m.CumulativeDeltaXCrossHair)
	assert.Equal(t, 40.0, m.CumulativeDeltaYCrossHair)

	m.AddShootHit(false, 100, 0, 66, 10, 20, false)
	assert.Equal(t, 2, m.NbrFirstBulletsFired)
	assert.Equal(t, 2, m.NbrFirstBulletsHit)
	assert.Equal(t, 1, m.NbrFirstBulletsHS)
	assert.Equal(t, 3, m.NbrBulletsFired)
	assert.Equal(t, 3, m.NbrBulletsHit)
	assert.Equal(t, 1, m.NbrBulletsHS)
	assert.Equal(t, 300, m.Damage)
	assert.Equal(t, 198, m.CumulativeVelocityShoots)
	assert.Equal(t, 20.0, m.CumulativeDeltaXCrossHair)
	assert.Equal(t, 40.0, m.CumulativeDeltaYCrossHair)
}

func TestMarksPerRound_ConvertDB(t *testing.T) {
	mpr := &modelanalyzer.MarksPerRound{
		SteamID:                   *model.GenerateRandomSteamIDStr(),
		Side:                      2,
		ConstTeam:                 3,
		NbrBulletsFired:           500,
		NbrBulletsHit:             250,
		NbrBulletsHS:              100,
		NbrFirstBulletsFired:      300,
		NbrFirstBulletsHit:        132,
		NbrFirstBulletsHS:         33,
		Damage:                    1000,
		UtilityDamage:             200,
		CumulativeVelocityShoots:  10000,
		CumulativeDeltaXCrossHair: 600,
		CumulativeDeltaYCrossHair: 750,
		NbrDeaths:                 1,
		NbrKills:                  4,
	}
	modelMarkPerRound := mpr.ConvertDB()

	assert.Equal(t, mpr.SteamID, *modelMarkPerRound.SteamID)
	assert.Equal(t, mpr.Side, *modelMarkPerRound.Side)
	assert.Equal(t, mpr.ConstTeam, *modelMarkPerRound.ConstTeam)
	assert.Equal(t, mpr.NbrBulletsFired, *modelMarkPerRound.NbrBulletsFired)
	assert.Equal(t, mpr.NbrBulletsHit, *modelMarkPerRound.NbrBulletsHit)
	assert.Equal(t, mpr.NbrBulletsHS, *modelMarkPerRound.NbrBulletsHS)
	assert.Equal(t, mpr.NbrFirstBulletsFired, *modelMarkPerRound.NbrFirstBulletsFired)
	assert.Equal(t, mpr.NbrFirstBulletsHit, *modelMarkPerRound.NbrFirstBulletsHit)
	assert.Equal(t, mpr.NbrFirstBulletsHS, *modelMarkPerRound.NbrFirstBulletsHS)
	assert.Equal(t, 0.5, *modelMarkPerRound.Accuracy)
	assert.Equal(t, 0.4, *modelMarkPerRound.HS)
	assert.Equal(t, 0.44, *modelMarkPerRound.FirstBulletAccuracy)
	assert.Equal(t, 0.25, *modelMarkPerRound.FirstBulletHS)
	assert.Equal(t, mpr.Damage, *modelMarkPerRound.Damage)
	assert.Equal(t, mpr.UtilityDamage, *modelMarkPerRound.UtilityDamage)
	assert.Equal(t, 20, *modelMarkPerRound.AverageVelocityShoots)
	assert.Equal(t, 4, *modelMarkPerRound.AverageDeltaXCrossHair)
	assert.Equal(t, 5, *modelMarkPerRound.AverageDeltaYCrossHair)
	assert.Equal(t, mpr.NbrDeaths, *modelMarkPerRound.NbrDeaths)
	assert.Equal(t, mpr.NbrKills, *modelMarkPerRound.NbrKills)
}
