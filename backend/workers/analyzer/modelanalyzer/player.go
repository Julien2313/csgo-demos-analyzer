package modelanalyzer

import (
	"math"
	"strconv"

	"github.com/golang/geo/r3"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

// Player struct of a player and its kills, assits, flashes...
type Player struct {
	Player *common.Player
	Team   *Team

	SteamID  uint64
	Username string
	Rank     int
	Color    common.Color

	ScoreFacts         ScoreFacts
	ScoreFactsPerRound []*ScoreFacts
	MVPsFacts          int

	Mark Mark

	Kills   []*Kill
	Deaths  []*Death
	Assists []*Assist

	Flashs      []*Flash
	StatsFlashs StatsFlashs

	WeaponPatterns []*WeaponPattern
	resetRecoil    bool
}

// StatsFlashs stats of a specific Player
type StatsFlashs struct {
	Bought        int
	Throwed       int
	Score         float64
	ScoreAllies   float64
	ScoreEnnemies float64
}

// ComputeFlashScore compute the flash score of the player
func (p *Player) ComputeFlashScore(nbrRounds int) {
	p.StatsFlashs = StatsFlashs{}
	for _, flash := range p.Flashs {
		flash.ComputeScore()
		p.StatsFlashs.Score += flash.Score
	}

	p.StatsFlashs.Score /= float64(nbrRounds)
}

// ComputeMatrixKills returns the MatrixKills of the current player
func (p *Player) ComputeMatrixKills(teams map[int]*Team) []*model.MatrixKills {
	matrixKills := []*model.MatrixKills{}

	for _, team := range teams {
		for _, enemy := range team.Players {
			matrixKills = append(matrixKills, &model.MatrixKills{
				KilledSteamID:  helpers.StrPtr(strconv.FormatUint(enemy.SteamID, 10)),
				KilledUsername: &enemy.Username,
				NbrKills:       helpers.IntPtr(0),
			})
		}
	}

	for _, kill := range p.Kills {
		// might be a bot, or by bombe, out of bounds...
		if kill.Victim == nil || kill.Killer == nil {
			continue
		}

		for numMK := range matrixKills {
			if *matrixKills[numMK].KilledSteamID == strconv.FormatUint(kill.Victim.SteamID, 10) {
				*matrixKills[numMK].NbrKills++
				break
			}
		}
	}

	return matrixKills
}

// ComputeMatrixFlashs returns the MatrixFlashs of the current player
func (p *Player) ComputeMatrixFlashs(teams map[int]*Team) []*model.MatrixFlashs {
	matrixFlashs := []*model.MatrixFlashs{}

	for _, team := range teams {
		for _, player := range team.Players {
			matrixFlashs = append(matrixFlashs, &model.MatrixFlashs{
				FlashedSteamID:  helpers.StrPtr(strconv.FormatUint(player.SteamID, 10)),
				FlashedUsername: &player.Username,
				NbrSecFlashs:    helpers.FloatPtr(0),
			})
		}
	}

	for _, flash := range p.Flashs {
		for _, flashed := range flash.PlayersFlashed {
			if flashed.FlashedParser.IsBot {
				continue
			}

			for numMF := range matrixFlashs {
				if *matrixFlashs[numMF].FlashedSteamID == strconv.FormatUint(flashed.FlashedParser.SteamID64, 10) {
					*matrixFlashs[numMF].NbrSecFlashs += flashed.Duration.Seconds()
					break
				}
			}
		}
	}

	return matrixFlashs
}

// ComputeWeaponPatterns compute the weapon patterns of a player
func (p *Player) ComputeWeaponPatterns(idTest int64) []*model.WeaponPattern {
	mapWeaponPatterns := make(map[common.EquipmentType]*model.WeaponPattern)
	for _, weaponPattern := range p.WeaponPatterns {
		if _, exists := mapWeaponPatterns[weaponPattern.WeaponType]; !exists {
			mapWeaponPatterns[weaponPattern.WeaponType] = &model.WeaponPattern{WeaponType: weaponPattern.WeaponType}
		}

		mapWeaponPattern := mapWeaponPatterns[weaponPattern.WeaponType]
		for _, bulletStats := range weaponPattern.BulletStats {
			for len(mapWeaponPattern.BulletStats) < bulletStats.BulletNumber+1 {
				mapWeaponPattern.BulletStats = append(mapWeaponPattern.BulletStats, &model.BulletStats{
					BulletNumber: len(mapWeaponPattern.BulletStats),
				})
			}

			mapWeaponPattern.BulletStats[bulletStats.BulletNumber].NbrBulletFired++
			if bulletStats.Hit {
				mapWeaponPattern.BulletStats[bulletStats.BulletNumber].NbrBulletHit++
			}
			if bulletStats.HitHS {
				mapWeaponPattern.BulletStats[bulletStats.BulletNumber].NbrBulletHitHS++
			}
			if bulletStats.Kill {
				mapWeaponPattern.BulletStats[bulletStats.BulletNumber].NbrBulletKill++
			}
			mapWeaponPattern.BulletStats[bulletStats.BulletNumber].CumulativeDeltaX += bulletStats.DeltaX
			mapWeaponPattern.BulletStats[bulletStats.BulletNumber].CumulativeDeltaY += bulletStats.DeltaY

		}
	}

	weaponPatterns := []*model.WeaponPattern{}
	for _, weaponPattern := range mapWeaponPatterns {
		if weaponPattern.WeaponType.Class() == common.EqClassUnknown ||
			weaponPattern.WeaponType.Class() == common.EqClassPistols ||
			weaponPattern.WeaponType.Class() == common.EqClassHeavy ||
			weaponPattern.WeaponType.Class() == common.EqClassEquipment ||
			weaponPattern.WeaponType.Class() == common.EqClassGrenade {
			continue
		}
		if len(weaponPattern.BulletStats) < 3 {
			continue
		}
		weaponPatterns = append(weaponPatterns, weaponPattern)
	}

	return weaponPatterns
}

// CountNbrEnnemiesKilled ignores team kill
func (p *Player) CountNbrEnnemiesKilled() *int {
	var nbrKills int

	for _, kill := range p.Kills {
		if !kill.TeamKill {
			nbrKills++
		}
	}

	return &nbrKills
}

// CountNbrEnnemiesKillAssisted ignores team assist
func (p *Player) CountNbrEnnemiesKillAssisted() *int {
	var nbrAssists int

	for _, assist := range p.Assists {
		if !assist.TeamKill {
			nbrAssists++
		}
	}

	return &nbrAssists
}

// GetZ I'm not good enough to explain this, good luck
func GetZ(a, b r3.Vector) (r3.Vector, r3.Vector, r3.Vector) {
	vec1 := a.Sub(b)
	Y := vec1.Mul(1.0 / vec1.Norm())
	vecCrossX := Y.Cross(r3.Vector{X: 0, Y: 0, Z: 1})
	X := vecCrossX.Mul(1.0 / vecCrossX.Norm())

	return X, Y, X.Cross(Y)
}

// AnglePlayerMovedFromPlayer return the angle from both player in 2 differents frames
func AnglePlayerMovedFromPlayer(
	frame, oldFrame Frame,
	steamIDAimer uint64,
	steamIDvictim uint64,
) (float64, float64) {
	aimer := frame.GetPlayer(steamIDAimer)
	if aimer == nil {
		return 0.0, 0.0
	}
	victim := frame.GetPlayer(steamIDvictim)
	if victim == nil {
		return 0.0, 0.0
	}

	oldAimer := oldFrame.GetPlayer(steamIDAimer)
	if oldAimer == nil {
		return 0.0, 0.0
	}
	oldVictim := oldFrame.GetPlayer(steamIDvictim)
	if oldVictim == nil {
		return 0.0, 0.0
	}

	X1, Y1, Z1 := GetZ(victim.Position, aimer.Position)
	X2, _, Z2 := GetZ(oldVictim.Position, oldAimer.Position)
	angleZ := math.Atan2(X2.Dot(Y1), X1.Dot(X2))
	dProductZ := Z2.Dot(Z1)
	if dProductZ > 1 {
		dProductZ = 1
	}
	if dProductZ < -1 {
		dProductZ = -1
	}
	angleX := math.Acos(dProductZ)

	sign := X2.Dot(Z1.Cross(Z2))
	if sign < 0 {
		angleX *= -1
	}

	return angleX * 180 / math.Pi, angleZ * 180 / math.Pi
}

// AddWeaponFirePattern append the current bullet to the last pattern ot to a new one
func (p *Player) AddWeaponFirePattern(
	weaponType common.EquipmentType,
	frame *Frame,
	playrAimer *PlayerEvent,
	numBullet int,
	hit, hitHS, kill bool,
	playerAimed *PlayerEvent,
) {
	if weaponType.Class() == common.EqClassEquipment || weaponType.Class() == common.EqClassGrenade {
		return
	}
	if playerAimed == nil {
		return
	}

	var (
		deltaX float64
		deltaY float64
	)

	var lastPattern *WeaponPattern
	if len(p.WeaponPatterns) > 0 {
		lastPattern = p.WeaponPatterns[len(p.WeaponPatterns)-1]
	}

	if lastPattern != nil && lastPattern.WeaponType != weaponType && numBullet != 0 {
		p.resetRecoil = false
		return
	}

	var currentPattern *WeaponPattern
	if lastPattern == nil || lastPattern.WeaponType != weaponType || numBullet == 0 {
		p.resetRecoil = true
		currentPattern = &WeaponPattern{
			WeaponType: weaponType,
		}
		p.WeaponPatterns = append(p.WeaponPatterns, currentPattern)
	} else {
		lastBulletShoot := lastPattern.BulletStats[len(lastPattern.BulletStats)-1]

		oldPlayerAimer := lastBulletShoot.FrameShoot.GetPlayer(playrAimer.SteamID)
		oldPlayerAimed := lastBulletShoot.FrameShoot.GetPlayer(playerAimed.SteamID)
		if oldPlayerAimer == nil || oldPlayerAimed == nil {
			p.resetRecoil = false
			return
		}

		if oldPlayerAimer.Position.Distance(playrAimer.Position) > 50/2.5 ||
			oldPlayerAimed.Position.Distance(playerAimed.Position) > 100/2.5 {
			p.resetRecoil = false
			return
		}
		// both player must be at 5 meters or more at least
		if playrAimer.Position.Distance(playerAimed.Position) < 500/2.5 {
			p.resetRecoil = false
			return
		}

		if lastBulletShoot.BulletNumber+1 == numBullet &&
			p.resetRecoil &&
			lastPattern.BulletStats[0].PlayerAimed.SteamID == playerAimed.SteamID {
			currentPattern = lastPattern

			angleX, angleZ := AnglePlayerMovedFromPlayer(
				*frame, *lastBulletShoot.FrameShoot,
				playrAimer.SteamID, playerAimed.SteamID,
			)

			// directionX + angleZ because Z is the rotation of the player horizontally
			// directionY + angleX because X is the rotation of the player vertically
			deltaX = helpers.PMod(lastPattern.BulletStats[0].ViewDirectionX-playrAimer.ViewDirectionX-angleZ+180.0, 360.0) - 180
			deltaY = helpers.PMod(lastPattern.BulletStats[0].ViewDirectionY-playrAimer.ViewDirectionY-angleX+180.0, 360.0) - 180
			if math.Abs(deltaX) > 5 || math.Abs(deltaY) > 10 {
				p.resetRecoil = false
				return
			}

			deltaXLastBullet := helpers.PMod(lastBulletShoot.ViewDirectionX-playrAimer.ViewDirectionX-angleZ+180.0, 360.0) - 180
			deltaYLastBullet := helpers.PMod(lastBulletShoot.ViewDirectionY-playrAimer.ViewDirectionY-angleX+180.0, 360.0) - 180
			if math.Abs(deltaXLastBullet) > 3 || math.Abs(deltaYLastBullet) > 3 {
				p.resetRecoil = false
				return
			}
		} else {
			p.resetRecoil = false
			return
		}
	}

	if currentPattern != nil {
		currentPattern.BulletStats = append(currentPattern.BulletStats, &BulletStats{
			BulletNumber:   numBullet,
			FrameShoot:     frame,
			PlayerAimed:    playerAimed,
			Hit:            hit,
			HitHS:          hitHS,
			Kill:           kill,
			ViewDirectionX: playrAimer.ViewDirectionX,
			ViewDirectionY: playrAimer.ViewDirectionY,
			DeltaX:         deltaX,
			DeltaY:         deltaY,
		})
	}
}
