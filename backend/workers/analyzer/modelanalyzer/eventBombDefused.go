package modelanalyzer

import (
	"time"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

// BombDefusedEvent event of a bomb defused
type BombDefusedEvent struct {
	BombDefused   events.BombDefused
	TimeRemainded time.Duration
}

// GetType type of event
func (bde *BombDefusedEvent) GetType() EventType {
	return BombDefusedEvt
}

// HandleEvent return a bomb defused event, addind score
func (bde *BombDefusedEvent) HandleEvent(game *Game, frame Frame, round *Round) model.Event {
	defuser := game.Players[bde.BombDefused.Player.SteamID64]
	if defuser != nil {
		if ennemiesAlive := CountAliveEnnemies(frame.Players, bde.BombDefused.Player.Team); ennemiesAlive > 0 {
			aliveAlly := CountAliveAllies(frame.Players, bde.BombDefused.Player.Team)
			for _, player := range frame.Players {
				if !player.IsCT || !player.IsAlive {
					continue
				}
				game.Players[player.SteamID].ScoreFactsPerRound[round.NumRound].AssistsScore.BombDefuseScore +=
					float64(ninjaDefuseScore*ennemiesAlive) / float64(aliveAlly)
			}
		}
		defuser.ScoreFactsPerRound[round.NumRound].AssistsScore.BombDefuseScore += bombDefuseScore
	}

	return &model.BombDefusedEvent{
		Defuser:       &bde.BombDefused.Player.Name,
		TimeRemainded: &bde.TimeRemainded,
	}
}
