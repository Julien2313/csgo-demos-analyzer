package modelanalyzer_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
)

func TestGetType_RoundFreezetimeEndEvent(t *testing.T) {
	assert.Equal(t, modelanalyzer.RoundFreezetimeEndEvt, (&modelanalyzer.RoundFreezetimeEndEvent{}).GetType())
}
func TestHandleEvent_RoundFreezetimeEndEvent(t *testing.T) {
	roundFreezetimeEndEvent := modelanalyzer.RoundFreezetimeEndEvent{}

	roundFreezetimeEndEventFront := roundFreezetimeEndEvent.HandleEvent(
		&modelanalyzer.Game{},
		modelanalyzer.Frame{},
		&modelanalyzer.Round{}).(*model.RoundFreezetimeEndEvent)
	assert.Equal(t, model.RoundFreezetimeEndEvent{}, *roundFreezetimeEndEventFront)
}
