package modelanalyzer

import (
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

// ItemDropEvent event when an item has been dropped
type ItemDropEvent struct {
}

// GetType type of event
func (ide *ItemDropEvent) GetType() EventType {
	return ItemDropEvt
}

// HandleEvent return a model.Event, add score etc
func (ide *ItemDropEvent) HandleEvent(game *Game, frame Frame, round *Round) model.Event {
	return nil
}
