package modelanalyzer_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
)

func TestScoreFacts_ConvertForDB(t *testing.T) {
	score := modelanalyzer.GenerateRandomScoreFacts()

	modelScore := score.ConvertForDB("0")
	assert.Equal(t, score.AttackScore.DamageScore, *modelScore.DamageScore)
	assert.Equal(t, score.AttackScore.KillScore, *modelScore.KillScore)
	assert.Equal(t, score.AttackScore.DeathScore, *modelScore.DeathScore)
	assert.Equal(t, score.AttackScore.AssistKillScore, *modelScore.AssistKillScore)
	assert.Equal(t, score.AssistsScore.DropScore, *modelScore.DropScore)
	assert.Equal(t, score.AssistsScore.FlashScore, *modelScore.FlashScore)
	assert.Equal(t, score.AssistsScore.RevangedScore, *modelScore.RevangedScore)
	assert.Equal(t, score.AssistsScore.BombDefuseScore, *modelScore.BombDefuseScore)
	assert.Equal(t, score.AssistsScore.BombPlantedScore, *modelScore.BombPlantedScore)
}
