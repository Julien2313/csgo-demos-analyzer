package modelanalyzer

import "math/rand"

// GenerateRandomScoreFacts helper to generate score
func GenerateRandomScoreFacts() *ScoreFacts {
	score := &ScoreFacts{
		AttackScore: AttackScore{
			DamageScore: rand.Float64()*120 - 10,
		},
		AssistsScore: AssistsScore{
			DropScore: rand.Float64() * 40,
		},
	}
	score.ComputeTotalScore()

	return score
}

// GenerateRandomScoresFactsPerRound helper to generate score per round
func GenerateRandomScoresFactsPerRound(nbrRound int) []*ScoreFacts {
	ScoresFacts := []*ScoreFacts{}
	for numRound := 0; numRound < nbrRound; numRound++ {
		ScoresFacts = append(ScoresFacts, GenerateRandomScoreFacts())
	}

	return ScoresFacts
}
