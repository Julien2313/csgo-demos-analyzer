package modelanalyzer_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
)

func TestMark_AddStuffLost(t *testing.T) {
	m := &modelanalyzer.Mark{}
	m.AddStuffLost(1000, 0)
	assert.Equal(t, 1000, m.DollarStuffLost)
	assert.Equal(t, 1, m.NbrDeaths)
}

func TestMark_AddShoot(t *testing.T) {
	m := &modelanalyzer.Mark{}
	m.AddShoot(0, 0, true)
	assert.Equal(t, 1, m.NbrShoots)
}

func TestMark_AddShootHit(t *testing.T) {
	m := &modelanalyzer.Mark{}
	m.AddShootHit(false, 100, 0, 0, 0, 0, 0, true)
	assert.Equal(t, 1, m.NbrShootsHit)
	assert.Equal(t, 0, m.NbrShootsHS)
	m.AddShootHit(true, 100, 0, 0, 0, 0, 0, true)
	assert.Equal(t, 2, m.NbrShootsHit)
	assert.Equal(t, 1, m.NbrShootsHS)
}

func TestMark_ConvertDB(t *testing.T) {
	m := &modelanalyzer.Mark{
		UtilityDamage:   245,
		DollarStuffLost: 6542,
		NbrDeaths:       18,
		NbrShoots:       268,
		NbrShootsHit:    68,
		NbrShootsHS:     20,
		NbrDamageGave:   3458,
	}
	demoID := helpers.Int64Ptr(0)
	modelMark := m.ConvertDB(demoID)

	assert.Equal(t, 0.2537313432835821, *modelMark.Accuracy)
	assert.Equal(t, 0.29411764705882354, *modelMark.HS)
	assert.Equal(t, 363.44444444444446, *modelMark.GrenadesValueDeath)
	assert.Equal(t, 3458, *modelMark.Damage)
	assert.Equal(t, 245, *modelMark.UtilityDamage)

	m = &modelanalyzer.Mark{
		UtilityDamage:   245,
		DollarStuffLost: 6542,
		NbrDeaths:       0,
		NbrShoots:       268,
		NbrShootsHit:    0,
		NbrShootsHS:     20,
		NbrDamageGave:   3458,
	}
	modelMark = m.ConvertDB(demoID)

	assert.Equal(t, 0.0, *modelMark.Accuracy)
	assert.Equal(t, 0.0, *modelMark.HS)
	assert.Equal(t, 0.0, *modelMark.GrenadesValueDeath)
	assert.Equal(t, 3458, *modelMark.Damage)
	assert.Equal(t, 245, *modelMark.UtilityDamage)
}
