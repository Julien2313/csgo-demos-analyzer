package modelanalyzer

import (
	"time"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

// ChatMessageEvent event when an item has been pickup
type ChatMessageEvent struct {
	ChatMessage events.ChatMessage
	TimeEvent   time.Duration
}

// GetType type of event
func (cme *ChatMessageEvent) GetType() EventType {
	return ChatMessageEvt
}

// HandleEvent return a model.Event, add score etc
func (cme *ChatMessageEvent) HandleEvent(game *Game, frame Frame, round *Round) model.Event {
	round.ChatMessages = append(round.ChatMessages, ChatMessage{
		Sender:     cme.ChatMessage.Sender.Name,
		SideSender: int(cme.ChatMessage.Sender.Team),
		Message:    cme.ChatMessage.Text,
		IsChatAll:  cme.ChatMessage.IsChatAll,
	})

	return nil
}
