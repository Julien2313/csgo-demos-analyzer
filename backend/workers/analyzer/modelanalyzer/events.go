package modelanalyzer

import (
	"fmt"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

// EventType list all type of events during the game
type EventType int

// list of all evt registered
const (
	KillEvt EventType = iota + 1
	PlayerHurtEvt
	BombPlantedEvt
	BombDefusedEvt
	BombExplodeEvt
	RoundFreezetimeEndEvt
	PlayerFlashedEvt

	PickUpItemEvt
	ItemDropEvt
	WeaponFireEvt
	GrenadeProjectileThrowEvt

	ChatMessageEvt
)

// EventAnalyzer interface for all events
type EventAnalyzer interface {
	GetType() EventType
	HandleEvent(*Game, Frame, *Round) model.Event
}

// GetGeneralDataFromPlayer return informations about player sent
// changes name to magic, bot or add bot if the player is controlling a bot
func GetGeneralDataFromPlayer(playerCommon *common.Player,
	playersEvent map[uint64]*PlayerEvent) (string, uint64, int, int) {
	var (
		playerName           string
		playerSteamID        uint64
		playerSide           int
		playerEquipmentValue int
	)

	switch {
	case playerCommon == nil:
		playerName = "magic"
	case playerCommon.IsBot:
		playerName = "bot"
		playerSide = int(playerCommon.Team)
	default:
		playerSteamID = playerCommon.SteamID64
		playerName = playerCommon.Name

		if playerCommon.IsControllingBot() {
			playerName = fmt.Sprintf("Bot %s", playerName)
		}

		playerSide = int(playerCommon.Team)
		if _, exists := playersEvent[playerSteamID]; exists {
			playerEquipmentValue = playersEvent[playerSteamID].EquipmentValue
		}
	}

	return playerName, playerSteamID, playerSide, playerEquipmentValue
}

func getPlayerVelocity(
	playerCommon *common.Player,
	playersEvent map[uint64]*PlayerEvent,
) int {

	if playerCommon == nil || playerCommon.IsBot {
		return 0
	}

	var velocity int
	playerSteamID := playerCommon.SteamID64
	if _, exists := playersEvent[playerSteamID]; exists {
		velocity = playersEvent[playerSteamID].Velocity
	}

	return velocity
}

// CountAliveAllies return number of allies
func CountAliveAllies(players map[uint64]*PlayerEvent, teamCurrentPlayer common.Team) int {
	var nbrAlliesAlive int

	for _, player := range players {
		if player.IsAlive {
			if common.TeamCounterTerrorists == teamCurrentPlayer && player.IsCT ||
				common.TeamCounterTerrorists != teamCurrentPlayer && !player.IsCT {
				nbrAlliesAlive++
			}
		}
	}

	return nbrAlliesAlive
}

// CountAliveEnnemies return number of ennemies
func CountAliveEnnemies(players map[uint64]*PlayerEvent, teamCurrentPlayer common.Team) int {
	var nbrEnnemiesAlive int

	for _, player := range players {
		if player.IsAlive {
			if common.TeamCounterTerrorists != teamCurrentPlayer && player.IsCT ||
				common.TeamCounterTerrorists == teamCurrentPlayer && !player.IsCT {
				nbrEnnemiesAlive++
			}
		}
	}

	return nbrEnnemiesAlive
}
func returnSteamsIDAlive(players map[uint64]*PlayerEvent, teamCurrentPlayer int) ([]uint64, []uint64) {
	var (
		alliesAlive   []uint64
		ennemiesAlive []uint64
	)

	for _, player := range players {
		if player.IsAlive {
			if player.Side == teamCurrentPlayer {
				alliesAlive = append(alliesAlive, player.SteamID)
			} else {
				ennemiesAlive = append(ennemiesAlive, player.SteamID)
			}
		}
	}
	return alliesAlive, ennemiesAlive
}

//ComputeTeamsAlive return number of alive and ennemies alives
func ComputeTeamsAlive(players map[uint64]*PlayerEvent, teamCurrentPlayer int) (int, int) {
	var (
		nbrAlliesAlive   int
		nbrEnnemiesAlive int
	)

	for _, player := range players {
		if player.IsAlive {
			if player.Side == teamCurrentPlayer {
				nbrAlliesAlive++
			} else {
				nbrEnnemiesAlive++
			}
		}
	}
	return nbrAlliesAlive, nbrEnnemiesAlive
}

// ComputeCoefAlive return the coef 1 + (nbrEnnemiesAlive-nbrAlliesAlive)/10
func ComputeCoefAlive(players map[uint64]*PlayerEvent, teamCurrentPlayer int) float64 {
	nbrAlliesAlive, nbrEnnemiesAlive := ComputeTeamsAlive(players, teamCurrentPlayer)
	return 1 + float64(nbrEnnemiesAlive-nbrAlliesAlive)/10
}

// ComputeStuffValue return in $ the value of the current stuff
func ComputeStuffValue(grenades []string) int {
	stuffValue := 0
	for _, grenade := range grenades {
		switch grenade {
		case "Smoke Grenade":
			stuffValue += 300
		case "HE Grenade":
			stuffValue += 300
		case "Incendiary Grenade":
			stuffValue += 600
		case "Molotov":
			stuffValue += 400
		case "Flashbang":
			stuffValue += 200
		case "Decoy Grenade":
			stuffValue += 50
		}
	}
	return stuffValue
}
