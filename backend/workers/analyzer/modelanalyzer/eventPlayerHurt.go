package modelanalyzer

import (
	"strconv"
	"time"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzerhelper"
)

// PlayerHurtEvent event of a player hurt
type PlayerHurtEvent struct {
	PlayerHurt events.PlayerHurt
	TimeEvent  time.Duration
}

// GetType type of event
func (phe *PlayerHurtEvent) GetType() EventType {
	return PlayerHurtEvt
}

// HandleEvent return a player hurt event, addind score
func (phe *PlayerHurtEvent) HandleEvent(game *Game, frame Frame, round *Round) model.Event {
	if phe.PlayerHurt.Attacker == nil || phe.PlayerHurt.Attacker.IsBot {
		return nil
	}

	_, attackerSteamID, attackerSide, attackerEquipmentValue :=
		GetGeneralDataFromPlayer(phe.PlayerHurt.Attacker, frame.Players)
	_, victimSteamID, victimSide, victimEquipmentValue :=
		GetGeneralDataFromPlayer(phe.PlayerHurt.Player, frame.Players)

	// this is for the front, to know during the frame how many bullets have been fired
	if _, exists := frame.Players[phe.PlayerHurt.Attacker.SteamID64]; exists {
		frame.Players[phe.PlayerHurt.Attacker.SteamID64].NbrShoots++
	}

	healthDamage := phe.PlayerHurt.HealthDamage
	var isKill bool
	if victim := frame.Players[victimSteamID]; victim != nil {
		if phe.PlayerHurt.Health == 0 {
			healthDamage = victim.Health
			isKill = true
		}
	}

	attacker := game.Players[attackerSteamID]
	if attacker != nil {
		if attackerSide != victimSide {
			isHS := phe.PlayerHurt.HitGroup == events.HitGroupHead
			recoil := frame.GetPlayer(attackerSteamID).Recoil
			isFirstBullet := recoil == 0

			deltaX, deltaY := round.GetDeltasCrossHairPlacement(frame, attackerSteamID)
			velocity := getPlayerVelocity(phe.PlayerHurt.Attacker, frame.Players)
			attacker.Mark.AddShootHit(isHS,
				healthDamage,
				phe.PlayerHurt.Weapon.Type,
				velocity,
				deltaX, deltaY,
				attackerSteamID,
				isFirstBullet,
			)

			if shooter, exists := frame.Players[phe.PlayerHurt.Attacker.SteamID64]; exists {
				var playerHurt *PlayerEvent
				if _, exists := frame.Players[phe.PlayerHurt.Player.SteamID64]; exists {
					playerHurt = frame.Players[phe.PlayerHurt.Player.SteamID64]
				}
				if !shooter.IsAirborne && shooter.HasSeen(victimSteamID) {
					attacker.AddWeaponFirePattern(phe.PlayerHurt.Weapon.Type, &frame, shooter, recoil, true, isHS, isKill, playerHurt)
				}
			}

			if round.MarksPerRound[attackerSteamID] == nil {
				round.MarksPerRound[attackerSteamID] = &MarksPerRound{
					SteamID:   strconv.FormatUint(attackerSteamID, 10),
					Side:      int(phe.PlayerHurt.Attacker.Team),
					ConstTeam: attacker.Team.NumTeam,
				}
			}
			round.MarksPerRound[attackerSteamID].AddShootHit(isHS,
				healthDamage,
				phe.PlayerHurt.Weapon.Type,
				velocity,
				deltaX, deltaY,
				isFirstBullet,
			)

			score := ComputeMultiplicatorAttack(frame.Players,
				attackerEquipmentValue, victimEquipmentValue,
				attackerSide,
				round.SideWin == attackerSide, attackerSide == victimSide,
			)
			attacker.ScoreFactsPerRound[round.NumRound].AttackScore.DamageScore += (float64(healthDamage) * score)
		}
	}

	var weaponShooterType *common.EquipmentType
	if phe.PlayerHurt.Weapon != nil {
		weaponShooterType = &phe.PlayerHurt.Weapon.Type
	}

	var weaponvictimType *common.EquipmentType
	victim := game.Players[victimSteamID]
	if victim != nil {
		weaponvictimType = &frame.Players[victim.SteamID].ActiveWeapon
	}

	var killerPosX *float64
	var killerPosY *float64
	if phe.PlayerHurt.Attacker != nil {
		killerPos := analyzerhelper.ConvertMapPos(game.MapName, phe.PlayerHurt.Attacker.LastAlivePosition)
		killerPosX = &killerPos.X
		killerPosY = &killerPos.Y
	}

	var victimPosX *float64
	var victimPosY *float64
	if phe.PlayerHurt.Player != nil {
		victimPos := analyzerhelper.ConvertMapPos(game.MapName, phe.PlayerHurt.Player.LastAlivePosition)
		victimPosX = &victimPos.X
		victimPosY = &victimPos.Y
	}

	round.HeatMapDmgs = append(round.HeatMapDmgs, &HeatMapDmg{
		DurationSinceRoundBegan: helpers.DurationPtr(phe.TimeEvent - *round.TimeFreezeTimeEnd),
		WeaponShooter:           weaponShooterType,
		SteamIDShooter:          helpers.StrPtr(strconv.FormatUint(attackerSteamID, 10)),
		SideShooter:             &attackerSide,
		ShooterPosX:             killerPosX,
		ShooterPosY:             killerPosY,
		ActiveWeaponVictim:      weaponvictimType,
		SteamIDVictim:           helpers.StrPtr(strconv.FormatUint(victimSteamID, 10)),
		SideVictim:              &victimSide,
		VictimPosX:              victimPosX,
		VictimPosY:              victimPosY,
		Dmg:                     helpers.IntPtr(phe.PlayerHurt.HealthDamage),
	})

	return nil
}
