package modelanalyzer

import (
	"github.com/golang/geo/r2"
	"github.com/golang/geo/r3"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
)

// GrenadeState represente the states of the grandes
type GrenadeState int

// consts of the GrenadeState
const (
	Nil GrenadeState = iota
	Destroyed
	Expired
	Stop
	Thrown
	Start
	Explode
)

// Grenade struct of grenade used for the analyzer
type Grenade struct {
	ID           int64
	Grenade      *common.GrenadeProjectile
	GrenadeType  common.EquipmentType
	State        GrenadeState
	Position     r3.Vector
	PositionFire *[]r2.Point
}
