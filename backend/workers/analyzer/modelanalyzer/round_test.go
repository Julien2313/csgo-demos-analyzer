package modelanalyzer_test

import (
	"math/rand"
	"testing"
	"time"

	"github.com/golang/geo/r3"
	"github.com/maxatome/go-testdeep/td"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
	"gonum.org/v1/gonum/mat"
)

func TestRound_ConvertDBChatMessage(t *testing.T) {
	round := modelanalyzer.Round{
		ChatMessages: modelanalyzer.GenerateRandomChatMessages(rand.Intn(200) + 50),
	}
	modelChatMessages := round.ConvertDBChatMessage()

	assert.Len(t, modelChatMessages, len(round.ChatMessages))
}

func TestRound_GetDeltasCrossHairPlacement(t *testing.T) {
	round := modelanalyzer.Round{}

	dX, dY := round.GetDeltasCrossHairPlacement(modelanalyzer.Frame{}, 0)
	td.Cmp(t, dX, 0.0)
	td.Cmp(t, dY, 0.0)
	steamIDToFind := rand.Uint64()
	round.Frames = []*modelanalyzer.Frame{
		{
			IGTime: time.Millisecond * 700,
			Players: map[uint64]*modelanalyzer.PlayerEvent{
				steamIDToFind: {},
			},
		},
		{
			IGTime: time.Millisecond * 1750,
			Players: map[uint64]*modelanalyzer.PlayerEvent{
				rand.Uint64(): {
					ViewDirectionX: 10.0,
					ViewDirectionY: 20.0,
				},
			},
		},
	}

	dX, dY = round.GetDeltasCrossHairPlacement(*round.Frames[1], steamIDToFind)
	td.Cmp(t, dX, 0.0)
	td.Cmp(t, dY, 0.0)

	round.Frames[1].Players[steamIDToFind] = &modelanalyzer.PlayerEvent{
		ViewDirectionX: -10.0,
		ViewDirectionY: -20.0,
	}
	dX, dY = round.GetDeltasCrossHairPlacement(*round.Frames[1], steamIDToFind)
	td.Cmp(t, dX, 10.0)
	td.Cmp(t, dY, 20.0)
}

func TestRound_GetOldFrame(t *testing.T) {
	round := modelanalyzer.Round{}
	assert.Nil(t, round.GetOldFrame(modelanalyzer.Frame{}, time.Second))

	round.Frames = []*modelanalyzer.Frame{
		{IGTime: time.Millisecond * 700},
		{IGTime: time.Millisecond * 800},
		{IGTime: time.Millisecond * 1750},
		{IGTime: time.Millisecond * 3000},
	}
	oldFrame := round.GetOldFrame(*round.Frames[2], time.Second)
	td.Cmp(t, *oldFrame, *round.Frames[0])

	assert.Nil(t, round.GetOldFrame(*round.Frames[2], time.Second*10))
}

func TestRound_ComputeMatrixSightWithLag(t *testing.T) {
	steamIDToFind := rand.Uint64()
	round := modelanalyzer.Round{
		Frames: []*modelanalyzer.Frame{
			{
				Players: map[uint64]*modelanalyzer.PlayerEvent{
					steamIDToFind: {
						ViewDirectionX: 10.0,
						ViewDirectionY: 20.0,
						Position: r3.Vector{
							X: 122.0,
							Y: -645.8,
							Z: 343.9,
						},
					},
				},
			},
		},
	}

	matrix := round.ComputeMatrixSightWithLag(*round.Frames[0], steamIDToFind)
	td.Cmp(t, *matrix, *mat.NewDense(4, 4,
		[]float64{
			0.9254165783983235, 0.16317591116653482, -0.3420201433256687, 110.09890815645018,
			-0.17364817766693033, 0.9848077530122081, 0, 657.1739245706494,
			0.33682408883346515, 0.0593911746138847, 0.9396926207859084, -325.8980105603099,
			0, 0, 0, 1,
		}))
}

func TestRound_IsDistancePlayersFromSightAtAMinimum(t *testing.T) {
	steamIDToFind := rand.Uint64()
	round := modelanalyzer.Round{
		Frames: []*modelanalyzer.Frame{
			{
				Players: map[uint64]*modelanalyzer.PlayerEvent{
					steamIDToFind: {
						Side:           5,
						IsAlive:        true,
						ViewDirectionX: 10.0,
						ViewDirectionY: 20.0,
						Position: r3.Vector{
							X: 122.0,
							Y: -645.8,
							Z: 343.9,
						},
					},
				},
			},
		},
	}
	playerFound, playerAimed := round.IsDistancePlayersFromSightAtAMinimum(*round.Frames[0], steamIDToFind, 300)
	assert.False(t, playerFound)
	assert.Len(t, playerAimed, 0)

	steamIDEnnemie := rand.Uint64()
	round.Frames[0].Players[steamIDEnnemie] = &modelanalyzer.PlayerEvent{
		Side:           3,
		IsAlive:        true,
		ViewDirectionX: 10.0,
		ViewDirectionY: 20.0,
		Position: r3.Vector{
			X: 242.0,
			Y: -645.8,
			Z: 343.9,
		},
	}
	playerFound, playerAimed = round.IsDistancePlayersFromSightAtAMinimum(*round.Frames[0], steamIDToFind, 300)
	assert.True(t, playerFound)
	assert.Len(t, playerAimed, 1)
	round.Frames[0].Players[steamIDEnnemie] = &modelanalyzer.PlayerEvent{
		Side:           3,
		IsAlive:        true,
		ViewDirectionX: 10.0,
		ViewDirectionY: 20.0,
		Position: r3.Vector{
			X: 242.0,
			Y: -245.8,
			Z: 343.9,
		},
	}
	playerFound, playerAimed = round.IsDistancePlayersFromSightAtAMinimum(*round.Frames[0], steamIDToFind, 300)
	assert.False(t, playerFound)
	assert.Len(t, playerAimed, 0)

	round.Frames[0].Players[steamIDEnnemie] = &modelanalyzer.PlayerEvent{
		Side:           3,
		IsAlive:        true,
		ViewDirectionX: 10.0,
		ViewDirectionY: 20.0,
		Position: r3.Vector{
			X: 242.0,
			Y: -645.8,
			Z: 343.9,
		},
	}
	round.Frames[0].Players[steamIDToFind] = &modelanalyzer.PlayerEvent{
		Side:           5,
		IsAlive:        true,
		ViewDirectionX: 180.0,
		ViewDirectionY: 20.0,
		Position: r3.Vector{
			X: 122.0,
			Y: -645.8,
			Z: 343.9,
		},
	}
	playerFound, playerAimed = round.IsDistancePlayersFromSightAtAMinimum(*round.Frames[0], steamIDToFind, 300)
	assert.False(t, playerFound)
	assert.Len(t, playerAimed, 0)
}
