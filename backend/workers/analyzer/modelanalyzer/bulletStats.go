package modelanalyzer

// BulletStats type with stats about bullet
type BulletStats struct {
	BulletNumber int

	PlayerAimed *PlayerEvent
	FrameShoot  *Frame

	Hit            bool
	HitHS          bool
	Kill           bool
	ViewDirectionX float64
	ViewDirectionY float64
	DeltaX         float64
	DeltaY         float64
}
