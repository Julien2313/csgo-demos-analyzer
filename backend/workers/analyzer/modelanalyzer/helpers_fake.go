package modelanalyzer

import (
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	st "github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/sendtables"
	stfake "github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/sendtables/fake"
)

// DemoInfoProviderMock mock struct demoinfo
type DemoInfoProviderMock struct {
	tickRate             float64
	ingameTick           int
	playersByHandle      map[int]*common.Player
	playerResourceEntity st.Entity
	equipment            *common.Equipment
}

// TickRate return the tickRate
func (p DemoInfoProviderMock) TickRate() float64 {
	return p.tickRate
}

// IngameTick return the ingameTick
func (p DemoInfoProviderMock) IngameTick() int {
	return p.ingameTick
}

// FindPlayerByHandle return the player by handle
func (p DemoInfoProviderMock) FindPlayerByHandle(handle int) *common.Player {
	return p.playersByHandle[handle]
}

// PlayerResourceEntity return the entity of the player
func (p DemoInfoProviderMock) PlayerResourceEntity() st.Entity {
	return p.playerResourceEntity
}

// FindWeaponByEntityID return the weapon by ID
func (p DemoInfoProviderMock) FindWeaponByEntityID(id int) *common.Equipment {
	return p.equipment
}

// MockDemoInfoProvider create a mock for demoInfoProvider
func MockDemoInfoProvider(tickRate float64, tick int) DemoInfoProviderMock {
	return DemoInfoProviderMock{
		tickRate:   tickRate,
		ingameTick: tick,
	}
}

// EntityWithProperty create an new entity with a property
func EntityWithProperty(id int64, propName string, value st.PropertyValue) *stfake.Entity {
	entity := new(stfake.Entity)
	entity.On("ID").Return(id)

	prop := new(stfake.Property)
	prop.On("Value").Return(value)

	entity.On("Property", propName).Return(prop)
	entity.On("PropertyValue", propName).Return(value, true)
	entity.On("PropertyValueMust", propName).Return(value)

	return entity
}

// AddEntityProperty add a property to an entity
func AddEntityProperty(entity *stfake.Entity, propName string, value st.PropertyValue) *stfake.Entity {
	prop := new(stfake.Property)
	prop.On("Value").Return(value)

	entity.On("Property", propName).Return(prop)
	entity.On("PropertyValue", propName).Return(value, true)
	entity.On("PropertyValueMust", propName).Return(value)

	return entity
}
