package modelanalyzer

// Kill struct of a kill with all the data
type Kill struct {
	HeadShot bool
	WallBang bool
	Frame    int
	TeamKill bool

	Victim *Player
	Killer *Player
	Assist *Player

	Weapon string
}
