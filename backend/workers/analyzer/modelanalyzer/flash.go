package modelanalyzer

import (
	"time"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
)

// PlayerFlashed player flash and the duration
type PlayerFlashed struct {
	Flashed       *Player
	FlashedParser *common.Player
	Duration      time.Duration
}

// Flash type of a flash with all data
type Flash struct {
	Score          float64
	ScoreEnnemies  float64
	ScoreAllies    float64
	Projectile     *common.GrenadeProjectile
	Thrower        *Player
	ThrowerParser  *common.Player
	PlayersFlashed []PlayerFlashed
}

// ComputeScore compute the score of the flash
func (f *Flash) ComputeScore() {
	flasherTeam := f.Thrower.Team.NumTeam

	f.Score = 0.0
	for _, playersFlashed := range f.PlayersFlashed {
		if flasherTeam == playersFlashed.Flashed.Team.NumTeam {
			f.Score -= playersFlashed.Duration.Seconds()
			f.ScoreAllies += playersFlashed.Duration.Seconds()
		} else {
			f.Score += playersFlashed.Duration.Seconds()
			f.ScoreEnnemies += playersFlashed.Duration.Seconds()
		}
	}
}
