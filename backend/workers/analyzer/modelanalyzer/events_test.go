package modelanalyzer_test

import (
	"testing"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
)

func TestCountAliveEnnemies(t *testing.T) {
	players := make(map[uint64]*modelanalyzer.PlayerEvent)
	players[0] = &modelanalyzer.PlayerEvent{IsAlive: true, IsCT: true}
	players[1] = &modelanalyzer.PlayerEvent{IsAlive: false, IsCT: true}
	players[2] = &modelanalyzer.PlayerEvent{IsAlive: false, IsCT: false}
	players[3] = &modelanalyzer.PlayerEvent{IsAlive: true, IsCT: false}
	players[4] = &modelanalyzer.PlayerEvent{IsAlive: true, IsCT: false}

	assert.Equal(t, 2, modelanalyzer.CountAliveEnnemies(players, common.TeamCounterTerrorists))

	players = make(map[uint64]*modelanalyzer.PlayerEvent)
	players[0] = &modelanalyzer.PlayerEvent{IsAlive: true, IsCT: true}
	players[1] = &modelanalyzer.PlayerEvent{IsAlive: false, IsCT: true}
	players[2] = &modelanalyzer.PlayerEvent{IsAlive: false, IsCT: false}
	players[3] = &modelanalyzer.PlayerEvent{IsAlive: true, IsCT: false}
	players[4] = &modelanalyzer.PlayerEvent{IsAlive: true, IsCT: false}

	assert.Equal(t, 1, modelanalyzer.CountAliveEnnemies(players, common.TeamTerrorists))
}

func TestCountAliveAllies(t *testing.T) {
	players := make(map[uint64]*modelanalyzer.PlayerEvent)
	players[0] = &modelanalyzer.PlayerEvent{IsAlive: true, IsCT: true}
	players[1] = &modelanalyzer.PlayerEvent{IsAlive: false, IsCT: true}
	players[2] = &modelanalyzer.PlayerEvent{IsAlive: false, IsCT: false}
	players[3] = &modelanalyzer.PlayerEvent{IsAlive: true, IsCT: false}
	players[4] = &modelanalyzer.PlayerEvent{IsAlive: true, IsCT: false}

	assert.Equal(t, 1, modelanalyzer.CountAliveAllies(players, common.TeamCounterTerrorists))

	players = make(map[uint64]*modelanalyzer.PlayerEvent)
	players[0] = &modelanalyzer.PlayerEvent{IsAlive: true, IsCT: true}
	players[1] = &modelanalyzer.PlayerEvent{IsAlive: false, IsCT: true}
	players[2] = &modelanalyzer.PlayerEvent{IsAlive: false, IsCT: false}
	players[3] = &modelanalyzer.PlayerEvent{IsAlive: true, IsCT: false}
	players[4] = &modelanalyzer.PlayerEvent{IsAlive: true, IsCT: false}

	assert.Equal(t, 2, modelanalyzer.CountAliveAllies(players, common.TeamTerrorists))
}

func TestComputeStuffValue(t *testing.T) {
	assert.Equal(t, 300+300+600+400+200*2+50,
		modelanalyzer.ComputeStuffValue([]string{
			"Smoke Grenade",
			"HE Grenade",
			"Incendiary Grenade",
			"Molotov",
			"Flashbang",
			"Flashbang",
			"Decoy Grenade",
		}))
}
