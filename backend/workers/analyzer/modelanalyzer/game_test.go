package modelanalyzer_test

import (
	"errors"
	"fmt"
	"math/rand"
	"strconv"
	"testing"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzerhelper"
)

func TestComputeComputePlayersCardCurrentFrame(t *testing.T) {
	a := analyzer.Analyzer{}
	playersEvent := make(map[uint64]*modelanalyzer.PlayerEvent)

	var mapName string
	for key := range analyzerhelper.Deltas {
		mapName = key
	}

	for numPlayer := 0; numPlayer < 10; numPlayer++ {
		player := modelanalyzer.GenerateRandomPlayerEvent()
		playersEvent[player.SteamID] = player
	}

	playersCard := a.ComputePlayersCardCurrentFrame(playersEvent, mapName)
	require.Len(t, playersCard, len(playersEvent))
	for steamID, playerEvent := range playersEvent {
		require.Equal(t, steamID, playerEvent.SteamID)
		require.Equal(t, strconv.FormatUint(steamID, 10), playerEvent.PlayerSteamID)
		playerCard := &model.PlayerCard{}
		for _, player := range playersCard {
			if player.SteamID == playerEvent.PlayerSteamID {
				playerCard = player
				break
			}
		}
		if playerCard == nil {
			require.NoError(t, errors.New("player not found"))
			return
		}

		assert.Equal(t, playerEvent.IsAlive, playerCard.IsAlive)
		assert.Equal(t, playerEvent.IsConnected, playerCard.IsConnected)
		assert.Equal(t, playerEvent.IsControllingBot, playerCard.IsControllingBot)
		assert.Equal(t, playerEvent.Side, playerCard.Side)
		assert.Equal(t, playerEvent.PrimaryWeapon, playerCard.PrimaryWeapon)
		assert.Equal(t, playerEvent.Pistol, playerCard.Pistol)
		var grenades string
		for _, grenade := range playerEvent.Grenades {
			grenades += fmt.Sprintf("%s;", grenade)
		}
		assert.Equal(t, grenades, playerCard.Grenades)
		assert.Equal(t, playerEvent.HasC4, playerCard.HasC4)
		assert.Equal(t, playerEvent.PlayerName, playerCard.PlayerName)
		assert.Equal(t, playerEvent.PlayerSteamID, playerCard.SteamID)
		assert.Equal(t, playerEvent.Health, playerCard.Health)
		assert.Equal(t, playerEvent.Armor, playerCard.Armor)
		assert.Equal(t, playerEvent.HasHelmet, playerCard.HasHelmet)
		assert.Equal(t, playerEvent.HasDefuseKit, playerCard.HasDefuseKit)
		assert.Equal(t, playerEvent.Money, playerCard.Money)
	}
}

func TestFindFlash(t *testing.T) {
	game := modelanalyzer.Game{
		Flashs: []*modelanalyzer.Flash{},
	}

	for numFlash := 0; numFlash < 10; numFlash++ {
		game.Flashs = append(game.Flashs, &modelanalyzer.Flash{
			Score:      rand.Float64() * 5,
			Projectile: common.NewGrenadeProjectile(),
		})
	}
	flashPicked := game.Flashs[rand.Intn(10)]
	flashFound := game.FindFlash(flashPicked.Projectile.UniqueID())
	assert.Equal(t, flashPicked.Projectile.UniqueID(), flashFound.Projectile.UniqueID())
	assert.Equal(t, flashPicked.Score, flashFound.Score)

	assert.Nil(t, game.FindFlash(rand.Int63()))
}

func TestComputeScores(t *testing.T) {
	game := modelanalyzer.Game{
		Players: make(map[uint64]*modelanalyzer.Player),
	}

	for steamID := uint64(0); steamID < 10; steamID++ {
		game.Players[steamID] = &modelanalyzer.Player{ScoreFactsPerRound: modelanalyzer.GenerateRandomScoresFactsPerRound(3)}
	}

	game.ComputeScores()

	for steamID := uint64(0); steamID < 5; steamID++ {
		assert.NotEqual(t, 0, game.Players[steamID].ScoreFacts.RatioScore)
	}
}

func TestComputeMVPs(t *testing.T) {
	game := modelanalyzer.Game{
		NbrRounds: 2,
		Players:   make(map[uint64]*modelanalyzer.Player),
		Rounds:    []*modelanalyzer.Round{{SideWin: 2}, {SideWin: 2}, {SideWin: 3}},
	}

	team1 := &modelanalyzer.Team{NumTeamParser: common.Team(2)}
	for steamID := uint64(0); steamID < 5; steamID++ {
		game.Players[steamID] = &modelanalyzer.Player{
			SteamID:            rand.Uint64(),
			Team:               team1,
			ScoreFactsPerRound: modelanalyzer.GenerateRandomScoresFactsPerRound(3),
		}
	}
	team2 := &modelanalyzer.Team{NumTeamParser: common.Team(3)}
	for steamID := uint64(5); steamID < 10; steamID++ {
		game.Players[steamID] = &modelanalyzer.Player{
			SteamID:            rand.Uint64(),
			Team:               team2,
			ScoreFactsPerRound: modelanalyzer.GenerateRandomScoresFactsPerRound(3)}
	}

	game.Players[0].ScoreFactsPerRound[0].TotalScore = 10000
	game.Players[2].ScoreFactsPerRound[1].TotalScore = 10000
	game.Players[9].ScoreFactsPerRound[2].TotalScore = 10000

	game.ComputeMVPs()
	assert.Equal(t, 1, game.Players[0].MVPsFacts)
	assert.Equal(t, 0, game.Players[1].MVPsFacts)
	assert.Equal(t, 1, game.Players[2].MVPsFacts)
	assert.Equal(t, 0, game.Players[3].MVPsFacts)
	assert.Equal(t, 0, game.Players[4].MVPsFacts)

	assert.Equal(t, 0, game.Players[5].MVPsFacts)
	assert.Equal(t, 0, game.Players[6].MVPsFacts)
	assert.Equal(t, 0, game.Players[7].MVPsFacts)
	assert.Equal(t, 0, game.Players[8].MVPsFacts)
	assert.Equal(t, 1, game.Players[9].MVPsFacts)
}
