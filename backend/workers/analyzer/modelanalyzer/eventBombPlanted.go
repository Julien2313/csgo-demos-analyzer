package modelanalyzer

import (
	"time"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

// BombPlantedEvent event of a bomb planted
type BombPlantedEvent struct {
	BombPlanted events.BombPlanted
	TimeEvent   time.Duration
}

// GetType type of event
func (bpe *BombPlantedEvent) GetType() EventType {
	return BombPlantedEvt
}

// HandleEvent return a bomb planted event, addind score
func (bpe *BombPlantedEvent) HandleEvent(game *Game, frame Frame, round *Round) model.Event {
	planter := game.Players[bpe.BombPlanted.Player.SteamID64]
	if planter != nil {
		planter.ScoreFactsPerRound[round.NumRound].AssistsScore.BombPlantedScore += bombPlantedScore
	}

	return &model.BombPlantedEvent{
		TimePlanted: helpers.DurationPtr(bpe.TimeEvent - *round.TimeFreezeTimeEnd),
		Planter:     &bpe.BombPlanted.Player.Name,
		Bombsite:    helpers.StrPtr(string(bpe.BombPlanted.Site)),
	}
}
