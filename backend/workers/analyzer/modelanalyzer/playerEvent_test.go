package modelanalyzer_test

import (
	"strconv"
	"testing"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	st "github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/sendtables"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
)

func TestPlayerCommonToEvent(t *testing.T) {
	commonPlayer := common.NewPlayer(modelanalyzer.MockDemoInfoProvider(0, 128))
	commonPlayer.Team = common.Team(2)
	commonPlayer.Inventory[0] = common.NewEquipment(common.EqKnife)
	commonPlayer.Inventory[1] = common.NewEquipment(common.EqGlock)
	commonPlayer.Inventory[2] = common.NewEquipment(common.EqAK47)
	commonPlayer.Inventory[3] = common.NewEquipment(common.EqBomb)
	commonPlayer.Inventory[4] = common.NewEquipment(common.EqFlash)
	commonPlayer.Inventory[5] = common.NewEquipment(common.EqDecoy)
	commonPlayer.Inventory[6] = common.NewEquipment(common.EqMolotov)
	commonPlayer.Inventory[7] = common.NewEquipment(common.EqIncendiary)
	commonPlayer.Inventory[8] = common.NewEquipment(common.EqSmoke)
	commonPlayer.Inventory[9] = common.NewEquipment(common.EqHE)

	entity := modelanalyzer.EntityWithProperty(1, "m_hActiveWeapon", st.PropertyValue{IntVal: 3})
	modelanalyzer.AddEntityProperty(entity, "m_bIsControllingBot", st.PropertyValue{IntVal: 0})
	modelanalyzer.AddEntityProperty(entity, "m_iHealth", st.PropertyValue{IntVal: 100})
	modelanalyzer.AddEntityProperty(entity, "m_ArmorValue", st.PropertyValue{IntVal: 100})
	modelanalyzer.AddEntityProperty(entity, "m_bHasHelmet", st.PropertyValue{IntVal: 1})
	modelanalyzer.AddEntityProperty(entity, "m_hGroundEntity", st.PropertyValue{IntVal: 1})
	modelanalyzer.AddEntityProperty(entity, "m_bHasDefuser", st.PropertyValue{IntVal: 1})
	modelanalyzer.AddEntityProperty(entity, "m_iAccount", st.PropertyValue{IntVal: 1000})
	modelanalyzer.AddEntityProperty(entity, "m_angEyeAngles[1]", st.PropertyValue{FloatVal: 56.5})
	modelanalyzer.AddEntityProperty(entity, "m_angEyeAngles[0]", st.PropertyValue{FloatVal: 24.5})
	modelanalyzer.AddEntityProperty(entity, "localdata.m_vecVelocity[0]", st.PropertyValue{FloatVal: 100.0})
	modelanalyzer.AddEntityProperty(entity, "localdata.m_vecVelocity[1]", st.PropertyValue{FloatVal: 45.0})
	modelanalyzer.AddEntityProperty(entity, "localdata.m_vecVelocity[2]", st.PropertyValue{FloatVal: 5.0})
	modelanalyzer.AddEntityProperty(entity,
		"m_fFlags",
		st.PropertyValue{ArrayVal: []st.PropertyValue{
			{IntVal: 0},
			{IntVal: 0},
			{IntVal: 0},
			{IntVal: 0},
			{IntVal: 0},
		},
		})
	modelanalyzer.AddEntityProperty(entity, "m_unCurrentEquipmentValue", st.PropertyValue{FloatVal: 0.0})
	modelanalyzer.AddEntityProperty(entity, "m_iPing.1", st.PropertyValue{IntVal: 32})
	modelanalyzer.AddEntityProperty(entity, "m_flRecoilIndex", st.PropertyValue{IntVal: 13})

	grenades := []string([]string{
		"Decoy Grenade",
		"Flashbang",
		"HE Grenade",
		"Incendiary Grenade",
		"Molotov",
		"Smoke Grenade",
	})
	commonPlayer.Entity = entity

	playerEvent := modelanalyzer.PlayerCommonToEvent(commonPlayer, nil, 0)
	assert.Equal(t, commonPlayer.IsAlive(), playerEvent.IsAlive)
	assert.Equal(t, commonPlayer.IsConnected, playerEvent.IsConnected)
	assert.Equal(t, commonPlayer.IsDefusing, playerEvent.IsDefusing)
	assert.Equal(t, commonPlayer.IsPlanting, playerEvent.IsPlanting)
	assert.Equal(t, commonPlayer.IsControllingBot(), playerEvent.IsControllingBot)
	assert.Equal(t, int(commonPlayer.Team), playerEvent.Side)
	assert.Equal(t, commonPlayer.Team == common.TeamCounterTerrorists, playerEvent.IsCT)
	// assert.Equal(t, common.EqBomb, playerEvent.ActiveWeapon)
	assert.Equal(t, common.EqAK47.String(), playerEvent.PrimaryWeapon)
	assert.Equal(t, common.EqGlock.String(), playerEvent.Pistol)
	assert.Equal(t, grenades, playerEvent.Grenades)
	assert.True(t, playerEvent.HasC4)
	assert.Equal(t, commonPlayer.Name, playerEvent.PlayerName)
	assert.Equal(t, commonPlayer.SteamID64, playerEvent.SteamID)
	assert.Equal(t, commonPlayer.EquipmentValueCurrent(), playerEvent.EquipmentValue)
	assert.Equal(t, strconv.FormatUint(commonPlayer.SteamID64, 10), playerEvent.PlayerSteamID)
	assert.Equal(t, commonPlayer.LastAlivePosition, playerEvent.Position)
	assert.Equal(t, commonPlayer.Health(), playerEvent.Health)
	assert.Equal(t, commonPlayer.Armor(), playerEvent.Armor)
	assert.Equal(t, commonPlayer.HasHelmet(), playerEvent.HasHelmet)
	assert.Equal(t, commonPlayer.HasDefuseKit(), playerEvent.HasDefuseKit)
	assert.Equal(t, commonPlayer.Money(), playerEvent.Money)
	assert.Equal(t, float64(commonPlayer.ViewDirectionX()), playerEvent.ViewDirectionX)
	assert.Equal(t, float64(commonPlayer.ViewDirectionY()), playerEvent.ViewDirectionY)

	assert.Equal(t, commonPlayer.IsDucking(), playerEvent.IsCrouching)
	assert.Equal(t, 0, playerEvent.NbrShoots)
	assert.Equal(t, int(commonPlayer.Velocity().Norm()), playerEvent.Velocity)
	assert.Equal(t, commonPlayer.Ping(), playerEvent.Ping)
	// assert.Equal(t, 13, playerEvent.Recoil)
	assert.Equal(t, 0.0, playerEvent.AccuracyPenalty)
}
