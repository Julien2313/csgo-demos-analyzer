package modelanalyzer_test

import (
	"testing"
	"time"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
)

func TestGetType_GrenadeProjectileThrowEvent(t *testing.T) {
	assert.Equal(t, modelanalyzer.GrenadeProjectileThrowEvt, (&modelanalyzer.GrenadeProjectileThrowEvent{}).GetType())
}

func TestHandleEvent_GrenadeProjectileThrowEvent(t *testing.T) {
	grenadeProjectileThrowEvent := modelanalyzer.GrenadeProjectileThrowEvent{
		GrenadeProjectileThrow: events.GrenadeProjectileThrow{},
		TimeEvent:              time.Millisecond * 16871,
	}

	grenadeProjectileThrowEventFront := grenadeProjectileThrowEvent.HandleEvent(
		&modelanalyzer.Game{},
		modelanalyzer.Frame{},
		&modelanalyzer.Round{})
	assert.Nil(t, grenadeProjectileThrowEventFront)
}
