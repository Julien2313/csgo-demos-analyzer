package modelanalyzer

import (
	"math/rand"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

// ChatMessage represents a message sent
type ChatMessage struct {
	Sender     string
	SideSender int
	Message    string
	IsChatAll  bool
}

// ConvertForDB convert the message for the DB
func (m *ChatMessage) ConvertForDB() *model.ChatMessage {
	return &model.ChatMessage{
		Sender:     &m.Sender,
		SideSender: &m.SideSender,
		Message:    &m.Message,
		IsChatAll:  &m.IsChatAll,
	}
}

// GenerateRandomChatMessage generate a random ChatMessage
func GenerateRandomChatMessage() ChatMessage {
	return ChatMessage{
		Sender:     helpers.RandomString(5),
		SideSender: rand.Int()%2 + 2,
		Message:    helpers.RandomString(50),
		IsChatAll:  (rand.Int() % 2) == 0,
	}
}

// GenerateRandomChatMessages generate randoms ChatMessages
func GenerateRandomChatMessages(nbrMessage int) []ChatMessage {
	chatMessages := []ChatMessage{}

	for numMessage := 0; numMessage < nbrMessage; numMessage++ {
		chatMessages = append(chatMessages, GenerateRandomChatMessage())
	}

	return chatMessages
}
