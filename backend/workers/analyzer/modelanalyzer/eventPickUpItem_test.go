package modelanalyzer_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
)

func TestGetType_PickUpItemEvent(t *testing.T) {
	assert.Equal(t, modelanalyzer.PickUpItemEvt, (&modelanalyzer.PickUpItemEvent{}).GetType())
}
func TestHandleEvent_PickUpItemEvent(t *testing.T) {
	pickUpItemEvent := modelanalyzer.PickUpItemEvent{}

	assert.Nil(t, pickUpItemEvent.HandleEvent(
		&modelanalyzer.Game{},
		modelanalyzer.Frame{},
		&modelanalyzer.Round{}))
}
