package modelanalyzer_test

import (
	"testing"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"github.com/maxatome/go-testdeep/td"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
)

func TestWeaponMark_AddKill(t *testing.T) {
	weaponMark := modelanalyzer.GenerateRandomWeaponMark()

	weaponMarkToAddKill := weaponMark
	weaponMarkToAddKill.AddKill()

	weaponMark.NbrKills++

	td.Cmp(t, weaponMarkToAddKill, weaponMark)
}

func TestWeaponMark_AddDeath(t *testing.T) {
	weaponMark := modelanalyzer.GenerateRandomWeaponMark()

	weaponMarkToAddDeath := weaponMark
	weaponMarkToAddDeath.AddDeath()

	weaponMark.NbrDeaths++

	td.Cmp(t, weaponMarkToAddDeath, weaponMark)
}

func TestWeaponMark_AddShoot(t *testing.T) {
	weaponMark := modelanalyzer.GenerateRandomWeaponMark()

	weaponMarkToAddShoot := weaponMark
	weaponMarkToAddShoot.AddShoot(true, 12)
	weaponMark.NbrShoots++
	weaponMark.CumulativeVelocityShoots += 12
	weaponMark.NbrFirstBulletsFired++
	td.Cmp(t, weaponMarkToAddShoot, weaponMark)

	weaponMarkToAddShoot.AddShoot(false, 12)
	weaponMark.NbrShoots++
	weaponMark.CumulativeVelocityShoots += 12
	td.Cmp(t, weaponMarkToAddShoot, weaponMark)
}

func TestWeaponMark_AddShootHit(t *testing.T) {
	weaponMark := modelanalyzer.GenerateRandomWeaponMark()

	weaponMarkToAddShootHit := weaponMark
	weaponMarkToAddShootHit.AddShootHit(true, false, 12, 5.3, 8.4)

	weaponMark.NbrShoots++
	weaponMark.NbrShootsHit++
	weaponMark.CumulativeVelocityShoots += 12
	weaponMark.NbrShootsHS++
	td.Cmp(t, weaponMarkToAddShootHit, weaponMark)

	weaponMarkToAddShootHit.AddShootHit(false, true, 12, 5.3, 8.4)
	weaponMark.NbrShoots++
	weaponMark.NbrShootsHit++
	weaponMark.CumulativeVelocityShoots += 12
	weaponMark.NbrFirstBulletsFired++
	weaponMark.NbrFirstBulletsHit++
	weaponMark.CumulativeDeltaXCrossHair += 5.3
	weaponMark.CumulativeDeltaYCrossHair += 8.4
	td.Cmp(t, weaponMarkToAddShootHit, weaponMark)

	weaponMarkToAddShootHit.AddShootHit(false, true, 12, 5.3, 8.4)
	weaponMark.NbrShoots++
	weaponMark.NbrShootsHit++
	weaponMark.CumulativeVelocityShoots += 12
	weaponMark.NbrFirstBulletsFired++
	weaponMark.NbrFirstBulletsHit++
	weaponMark.CumulativeDeltaXCrossHair += 5.3
	weaponMark.CumulativeDeltaYCrossHair += 8.4
	td.Cmp(t, weaponMarkToAddShootHit, weaponMark)

	weaponMarkToAddShootHit.AddShootHit(true, true, 12, 5.3, 8.4)
	weaponMark.NbrShoots++
	weaponMark.NbrShootsHit++
	weaponMark.CumulativeVelocityShoots += 12
	weaponMark.NbrShootsHS++
	weaponMark.NbrFirstBulletsFired++
	weaponMark.NbrFirstBulletsHit++
	weaponMark.CumulativeDeltaXCrossHair += 5.3
	weaponMark.CumulativeDeltaYCrossHair += 8.4
	weaponMark.NbrFirstBulletsHS++
	td.Cmp(t, weaponMarkToAddShootHit, weaponMark)
}

func TestWeaponMark_AddKillConvertDB(t *testing.T) {
	m := &modelanalyzer.WeaponMark{
		NbrKills:                  37,
		NbrDeaths:                 12,
		NbrShoots:                 1745,
		NbrShootsHit:              500,
		NbrShootsHS:               100,
		NbrFirstBulletsFired:      753,
		NbrFirstBulletsHit:        542,
		NbrFirstBulletsHS:         200,
		NbrDamageGave:             3700,
		CumulativeVelocityShoots:  43625,
		CumulativeDeltaXCrossHair: 3794,
		CumulativeDeltaYCrossHair: 1084,
	}

	modelWeaponMark := m.ConvertDB(common.EqAK47)

	assert.Equal(t, common.EqAK47, *modelWeaponMark.WeaponType)
	assert.Equal(t, 1745, *modelWeaponMark.NbrBulletsFired)
	assert.Equal(t, 500, *modelWeaponMark.NbrBulletsHit)
	assert.Equal(t, 100, *modelWeaponMark.NbrBulletsHS)
	assert.Equal(t, 753, *modelWeaponMark.NbrFirstBulletsFired)
	assert.Equal(t, 542, *modelWeaponMark.NbrFirstBulletsHit)
	assert.Equal(t, 200, *modelWeaponMark.NbrFirstBulletsHS)
	assert.Equal(t, 0.28653295128939826, *modelWeaponMark.Accuracy)
	assert.Equal(t, 0.2, *modelWeaponMark.HS)
	assert.Equal(t, 0.7197875166002656, *modelWeaponMark.FirstBulletAccuracy)
	assert.Equal(t, 0.36900369003690037, *modelWeaponMark.FirstBulletHS)
	assert.Equal(t, 25, *modelWeaponMark.AverageVelocityShoots)
	assert.Equal(t, 7, *modelWeaponMark.AverageDeltaXCrossHair)
	assert.Equal(t, 2, *modelWeaponMark.AverageDeltaYCrossHair)
	assert.Equal(t, 3700, *modelWeaponMark.Damage)
	assert.Equal(t, 12, *modelWeaponMark.NbrDeaths)
	assert.Equal(t, 37, *modelWeaponMark.NbrKills)
}
