package modelanalyzer

import (
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

// PickUpItemEvent event when an item has been pickup
type PickUpItemEvent struct {
}

// GetType type of event
func (puie *PickUpItemEvent) GetType() EventType {
	return PickUpItemEvt
}

// HandleEvent return a model.Event, add score etc
func (puie *PickUpItemEvent) HandleEvent(game *Game, frame Frame, round *Round) model.Event {
	return nil
}
