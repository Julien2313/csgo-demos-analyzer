package modelanalyzer

import (
	"fmt"
	"strconv"

	"github.com/thoas/go-funk"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

// Game struct of a game with all the events
type Game struct {
	FramesPerSecond int
	Started         bool
	Ended           bool
	Switched        bool

	Players map[uint64]*Player

	RoundCancelled bool
	Rounds         []*Round
	CurrentRound   *Round
	NbrRounds      int

	Teams   map[int]*Team
	MapName string

	Kills   []*Kill
	Deaths  []*Death
	Assists []*Assist
	Flashs  []*Flash
}

// FindFlash return a flash from its uniqueID
func (g Game) FindFlash(uniqueID int64) *Flash {
	for iFlash, flash := range g.Flashs {
		if flash.Projectile.UniqueID() == uniqueID {
			return g.Flashs[iFlash]
		}
	}

	return nil
}

// ComputeScores compute the facts score
func (g *Game) ComputeScores() {
	var totalScore float64

	for _, player := range g.Players {
		player.ScoreFacts.TotalScore = 0

		for _, score := range player.ScoreFactsPerRound {
			score.ComputeTotalScore()
			player.ScoreFacts.AddScoreFacts(*score)
		}

		player.ScoreFacts.ComputeTotalScore()
		totalScore += player.ScoreFacts.TotalScore
	}

	totalScore /= 10

	for _, player := range g.Players {
		player.ScoreFacts.RatioScore = player.ScoreFacts.TotalScore / totalScore
	}
}

// ComputeMVPs compute and count the number of MVPs
// !!! must always be call AFTER ComputeScores !!!
func (g *Game) ComputeMVPs() {
	for numRound := 0; numRound < g.NbrRounds+1; numRound++ {
		var (
			bestScore         float64
			bestPlayerSteamID uint64
		)

		winnerRound := g.Rounds[numRound].SideWin

		for _, player := range g.Players {
			if player == nil || player.Team == nil {
				continue
			}
			if int(player.Team.NumTeamParser) == winnerRound {
				if numRound >= 15 {
					continue
				}
			} else {
				if numRound < 15 {
					continue
				}
			}

			if player.ScoreFactsPerRound[numRound].TotalScore > bestScore {
				bestScore = player.ScoreFactsPerRound[numRound].TotalScore
				bestPlayerSteamID = player.SteamID
			}
		}

		for numPlayer, player := range g.Players {
			if player.SteamID == bestPlayerSteamID {
				g.Players[numPlayer].MVPsFacts++
			}
		}
	}
}

// CopyHeatMaps copy HeatMaps from analyzer to DB
func (g *Game) CopyHeatMaps(demo *model.Demo) {
	for numRound, round := range g.Rounds {
		if len(demo.Rounds) <= numRound {
			panic("should not go here !")
		}

		modelRound := demo.Rounds[numRound]
		modelRound.HeatMapKills = nil
		modelRound.HeatMapDmgs = nil
		for _, heatMapKill := range round.HeatMapKills {
			modelRound.HeatMapKills = append(modelRound.HeatMapKills, &model.HeatMapKill{
				DurationSinceRoundBegan: heatMapKill.DurationSinceRoundBegan,
				WeaponKiller:            heatMapKill.WeaponKiller,
				SteamIDKiller:           heatMapKill.SteamIDKiller,
				SideKiller:              heatMapKill.SideKiller,
				KillerPosX:              heatMapKill.KillerPosX,
				KillerPosY:              heatMapKill.KillerPosY,
				ActiveWeaponVictim:      heatMapKill.ActiveWeaponVictim,
				SideVictim:              heatMapKill.SideVictim,
				SteamIDVictim:           heatMapKill.SteamIDVictim,
				VictimPosX:              heatMapKill.VictimPosX,
				VictimPosY:              heatMapKill.VictimPosY,
			})
		}
		for _, heatMapDmg := range round.HeatMapDmgs {
			modelRound.HeatMapDmgs = append(modelRound.HeatMapDmgs, &model.HeatMapDmg{
				DurationSinceRoundBegan: heatMapDmg.DurationSinceRoundBegan,
				WeaponShooter:           heatMapDmg.WeaponShooter,
				SteamIDShooter:          heatMapDmg.SteamIDShooter,
				SideShooter:             heatMapDmg.SideShooter,
				ShooterPosX:             heatMapDmg.ShooterPosX,
				ShooterPosY:             heatMapDmg.ShooterPosY,
				ActiveWeaponVictim:      heatMapDmg.ActiveWeaponVictim,
				SteamIDVictim:           heatMapDmg.SteamIDVictim,
				SideVictim:              heatMapDmg.SideVictim,
				VictimPosX:              heatMapDmg.VictimPosX,
				VictimPosY:              heatMapDmg.VictimPosY,
				Dmg:                     heatMapDmg.Dmg,
			})
		}
	}
}

// Transcript stored data anlyzed in the demo
func (g *Game) Transcript(logger *custlogger.Logger, rounds []*model.Round, demo *model.Demo) {
	teams := []*model.TeamStats{}
	steamsIDs := []*model.SteamID{}

	g.ComputeScores()
	g.ComputeMVPs()

	for _, team := range g.Teams {
		if len(team.Players) == 0 {
			continue
		}

		players := []*model.PlayerStats{}

		for _, player := range team.Players {
			player.ComputeFlashScore(g.NbrRounds)

			totalScore := player.ScoreFacts.ConvertForDB(strconv.FormatUint(player.SteamID, 10))
			duelsPlayer := []*model.DuelsPlayer{}

			for index, duels := range team.DuelsMatrix {
				matchup := fmt.Sprintf("%dv%d", index.NbrAliveAllies, index.NbrAliveEnnemies)
				modelDuels := &model.DuelsPlayer{
					Matchup:      &matchup,
					NbrOccurence: helpers.IntPtr(0),
					NbrWin:       helpers.IntPtr(0),
				}

				for _, duel := range *duels {
					if !funk.ContainsUInt64(duel.PlayersAlive, player.SteamID) {
						continue
					}
					*modelDuels.NbrOccurence++
					if duel.Won {
						*modelDuels.NbrWin++
					}
				}
				duelsPlayer = append(duelsPlayer, modelDuels)
			}

			playerStats := &model.PlayerStats{
				SteamID:        helpers.StrPtr(strconv.FormatUint(player.SteamID, 10)),
				Username:       &player.Username,
				MVPsFacts:      &player.MVPsFacts,
				Color:          &player.Color,
				ScoreFacts:     totalScore,
				Marks:          player.Mark.ConvertDB(demo.ID),
				Rank:           &player.Rank,
				Kills:          player.CountNbrEnnemiesKilled(),
				Deaths:         helpers.IntPtr(len(player.Deaths)),
				Assists:        player.CountNbrEnnemiesKillAssisted(),
				MatrixKills:    player.ComputeMatrixKills(g.Teams),
				MatrixFlashs:   player.ComputeMatrixFlashs(g.Teams),
				DuelsPlayer:    duelsPlayer,
				WeaponPatterns: player.ComputeWeaponPatterns(*demo.ID),
			}

			steamsIDs = append(steamsIDs, &model.SteamID{SteamID: playerStats.SteamID})
			players = append(players, playerStats)
		}

		duelsTeam := []*model.DuelsTeam{}

		for index, duels := range team.DuelsMatrix {
			matchup := fmt.Sprintf("%dv%d", index.NbrAliveAllies, index.NbrAliveEnnemies)
			modelDuels := &model.DuelsTeam{
				Matchup:      &matchup,
				NbrOccurence: helpers.IntPtr(len(*duels)),
				NbrWin:       helpers.IntPtr(0),
			}

			for _, duel := range *duels {
				if duel.Won {
					*modelDuels.NbrWin++
				}
			}
			duelsTeam = append(duelsTeam, modelDuels)
		}

		teamStats := &model.TeamStats{
			Score:        &team.Score,
			PlayersStats: players,
			DuelsTeam:    duelsTeam,
		}
		teams = append(teams, teamStats)
	}

	demo.SteamsIDs = steamsIDs
	demo.TeamsStats = teams
	demo.MapName = &g.MapName
	demo.Rounds = rounds

	g.CopyHeatMaps(demo)
}
