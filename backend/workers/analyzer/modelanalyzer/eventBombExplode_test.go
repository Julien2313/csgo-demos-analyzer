package modelanalyzer_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
)

func TestGetType_BombExplodeEvent(t *testing.T) {
	assert.Equal(t, modelanalyzer.BombExplodeEvt, (&modelanalyzer.BombExplodeEvent{}).GetType())
}
func TestHandleEvent_BombExplodeEvent(t *testing.T) {
	bombExplodeEvt := modelanalyzer.BombExplodeEvent{}

	bombExplodeEvtFront := bombExplodeEvt.HandleEvent(
		&modelanalyzer.Game{},
		modelanalyzer.Frame{},
		&modelanalyzer.Round{}).(*model.BombExplodeEvent)
	assert.Equal(t, model.BombExplodeEvent{}, *bombExplodeEvtFront)
}
