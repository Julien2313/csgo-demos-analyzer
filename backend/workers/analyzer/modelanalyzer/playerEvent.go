package modelanalyzer

import (
	"fmt"
	"sort"
	"strconv"

	"github.com/golang/geo/r3"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
)

// PlayerEvent store all data of a player from the parser in order to analyze it later.
// can't just use the common.Event as it doesn't store all the players
type PlayerEvent struct {
	SteamID          uint64
	EnnemiesSeen     []uint64
	IsAlive          bool
	IsAirborne       bool
	IsConnected      bool
	IsControllingBot bool
	IsDefusing       bool
	IsPlanting       bool
	IsCrouching      bool
	NbrShoots        int
	Side             int
	IsCT             bool
	ActiveWeapon     common.EquipmentType
	PrimaryWeapon    string
	Pistol           string
	Grenades         []string
	HasC4            bool
	PlayerName       string
	PlayerSteamID    string
	Position         r3.Vector
	Health           int
	Armor            int
	HasHelmet        bool
	HasDefuseKit     bool
	Money            int
	EquipmentValue   int
	ViewDirectionX   float64
	ViewDirectionY   float64
	Velocity         int
	Ping             int
	Recoil           int
	AccuracyPenalty  float64
}

// PlayerCommonToEvent convert a commonPlayer to a playerEvent
func PlayerCommonToEvent(player *common.Player, players map[uint64]*Player, nbrRounds int) *PlayerEvent {
	var (
		primaryWeapon    string
		pistols          string
		hasC4            bool
		grenades         []string
		isAlive          bool
		isControllingBot bool
		username         string
		activeWeapon     common.EquipmentType
		recoil           int
		accuracyPenalty  float64
	)

	if player != nil && player.ActiveWeapon() != nil && player.ActiveWeapon().Entity != nil {
		recoil = int(player.ActiveWeapon().Entity.PropertyValueMust("m_flRecoilIndex").FloatVal)
	}

	for _, weapon := range player.Weapons() {
		switch {
		case weapon.Type.Class() == common.EqClassPistols:
			pistols = weapon.String()
		case weapon.Type.Class() == common.EqClassGrenade:
			grenades = append(grenades, weapon.String())
		case weapon.Type.Class() == common.EqClassSMG ||
			weapon.Type.Class() == common.EqClassHeavy ||
			weapon.Type.Class() == common.EqClassRifle:
			primaryWeapon = weapon.String()
		default:
			if weapon.String() == "C4" {
				hasC4 = true
			}
		}
	}
	sort.Strings(grenades)

	isControllingBot = player.IsControllingBot()
	if isControllingBot {
		isAlive = false
		username = fmt.Sprintf("Bot %s", player.Name)
	} else {
		isAlive = player.IsAlive()
		username = player.Name
	}

	var side int
	//TODO change that, pls
	if int(player.Team) != 0 {
		side = int(player.Team)
	} else if players[player.SteamID64].Team != nil {
		side = players[player.SteamID64].Team.GetConstTeamNum(nbrRounds)
	}

	if player.ActiveWeapon() != nil {
		activeWeapon = player.ActiveWeapon().Type
	}

	var ennemiesSeen []uint64
	if player.TeamState != nil {
		for _, ennemy := range player.TeamState.Opponent.Members() {
			if !ennemy.IsAlive() || !ennemy.IsConnected {
				continue
			}
			if player.HasSpotted(ennemy) {
				ennemiesSeen = append(ennemiesSeen, ennemy.SteamID64)
			}
		}

	}

	return &PlayerEvent{
		IsAlive:          isAlive,
		EnnemiesSeen:     ennemiesSeen,
		IsAirborne:       player.IsAirborne(),
		IsConnected:      player.IsConnected,
		IsDefusing:       player.IsDefusing,
		IsPlanting:       player.IsPlanting,
		IsCrouching:      player.IsDucking(),
		IsControllingBot: isControllingBot,
		Side:             side,
		IsCT:             player.Team == common.TeamCounterTerrorists,
		ActiveWeapon:     activeWeapon,
		PrimaryWeapon:    primaryWeapon,
		Pistol:           pistols,
		Grenades:         grenades,
		HasC4:            hasC4,
		PlayerName:       username,
		SteamID:          player.SteamID64,
		EquipmentValue:   player.EquipmentValueCurrent(),
		PlayerSteamID:    strconv.FormatUint(player.SteamID64, 10),
		Position:         player.LastAlivePosition,
		Health:           player.Health(),
		Armor:            player.Armor(),
		HasHelmet:        player.HasHelmet(),
		HasDefuseKit:     player.HasDefuseKit(),
		Money:            player.Money(),
		ViewDirectionX:   float64(player.ViewDirectionX()),
		ViewDirectionY:   float64(player.ViewDirectionY()),
		Velocity:         int(player.Velocity().Norm()),
		Ping:             player.Ping(),
		Recoil:           recoil,
		AccuracyPenalty:  accuracyPenalty,
	}
}

// HasSeen return true if p see ennemy
func (p *PlayerEvent) HasSeen(steamIDEnnemy uint64) bool {
	for _, steamIDEnnemySeen := range p.EnnemiesSeen {
		if steamIDEnnemySeen == steamIDEnnemy {
			return true
		}
	}

	return false
}
