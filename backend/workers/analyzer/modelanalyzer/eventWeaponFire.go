package modelanalyzer

import (
	"strconv"
	"time"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

// WeaponFireEvent event of a fire
type WeaponFireEvent struct {
	WeaponFire events.WeaponFire
	TimeEvent  time.Duration
}

// GetType type of event
func (wfe *WeaponFireEvent) GetType() EventType {
	return WeaponFireEvt
}

// HandleEvent return a fire event
func (wfe *WeaponFireEvent) HandleEvent(game *Game, frame Frame, round *Round) model.Event {
	_, playerFiringSteamID, sidedShooter, _ := GetGeneralDataFromPlayer(wfe.WeaponFire.Shooter, frame.Players)
	playerFiring := game.Players[playerFiringSteamID]
	if playerFiring == nil {
		return nil
	}

	// this is for the front, to know during the frame how many bullets have been fired
	shooter, shooterExists := frame.Players[wfe.WeaponFire.Shooter.SteamID64]
	if shooterExists {
		frame.Players[wfe.WeaponFire.Shooter.SteamID64].NbrShoots++
	}

	if _, nbrEnnemiesAlive := ComputeTeamsAlive(frame.Players, sidedShooter); nbrEnnemiesAlive == 0 {
		return nil
	}

	// less than 3 meters
	playerFound, playersAimed := round.IsDistancePlayersFromSightAtAMinimum(frame, playerFiringSteamID, 300/2.5)
	if !playerFound {
		return nil
	}
	recoil := frame.GetPlayer(playerFiringSteamID).Recoil
	isFirstBullet := recoil == 0
	velocity := getPlayerVelocity(wfe.WeaponFire.Shooter, frame.Players)
	playerFiring.Mark.AddShoot(
		wfe.WeaponFire.Weapon.Type,
		velocity,
		isFirstBullet,
	)

	if len(playersAimed) == 1 {
		if shooterExists {
			if !shooter.IsAirborne && shooter.HasSeen(playersAimed[0].SteamID) {
				playerFiring.AddWeaponFirePattern(
					wfe.WeaponFire.Weapon.Type,
					&frame, shooter, recoil,
					false, false, false,
					playersAimed[0],
				)
			}
		}
	}

	if round.MarksPerRound[playerFiringSteamID] == nil {
		round.MarksPerRound[playerFiringSteamID] = &MarksPerRound{
			SteamID:   strconv.FormatUint(playerFiringSteamID, 10),
			Side:      int(wfe.WeaponFire.Shooter.Team),
			ConstTeam: playerFiring.Team.NumTeam,
		}
	}
	round.MarksPerRound[playerFiringSteamID].AddShoot(wfe.WeaponFire.Weapon.Type,
		getPlayerVelocity(wfe.WeaponFire.Shooter, frame.Players),
		isFirstBullet,
	)

	return nil
}
