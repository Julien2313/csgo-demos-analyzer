package modelanalyzer

import (
	"time"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

// PlayerFlashedEvent event of a flashed player
type PlayerFlashedEvent struct {
	PlayerFlashed events.PlayerFlashed
	TimeEvent     time.Duration
}

// GetType type of event
func (pfe *PlayerFlashedEvent) GetType() EventType {
	return PlayerFlashedEvt
}

// HandleEvent return a player flashed event, addind score
func (pfe *PlayerFlashedEvent) HandleEvent(game *Game, frame Frame, round *Round) model.Event {
	var flasherSteamID uint64
	if pfe.PlayerFlashed.Attacker != nil && !pfe.PlayerFlashed.Attacker.IsBot {
		flasherSteamID = pfe.PlayerFlashed.Attacker.SteamID64
	}

	var flashedSteamID uint64
	if pfe.PlayerFlashed.Player != nil && !pfe.PlayerFlashed.Player.IsBot {
		flashedSteamID = pfe.PlayerFlashed.Player.SteamID64
	}

	playerflashed := PlayerFlashed{
		Flashed:       game.Players[flasherSteamID],
		FlashedParser: pfe.PlayerFlashed.Player,
		Duration:      time.Duration(float32(time.Second) * pfe.PlayerFlashed.Player.FlashDuration),
	}
	playerflashed.Duration = playerflashed.Duration - time.Second
	if playerflashed.Duration.Seconds() < 0 {
		playerflashed.Duration = 0
	}

	flash := game.FindFlash(pfe.PlayerFlashed.Projectile.UniqueID())
	if flash != nil {
		flash.PlayersFlashed = append(flash.PlayersFlashed, playerflashed)
	} else {
		flash = &Flash{
			Projectile:     pfe.PlayerFlashed.Projectile,
			PlayersFlashed: []PlayerFlashed{playerflashed},
			Thrower:        game.Players[flasherSteamID],
			ThrowerParser:  pfe.PlayerFlashed.Attacker,
		}

		game.Flashs = append(game.Flashs, flash)
		if _, exists := game.Players[flasherSteamID]; exists {
			game.Players[flasherSteamID].StatsFlashs.Throwed++
			game.Players[flasherSteamID].Flashs = append(game.Players[flasherSteamID].Flashs, flash)
		}
	}

	flasher, existsflasher := game.Players[flasherSteamID]
	flashed, existsFlashed := game.Players[flashedSteamID]

	if !existsflasher || !existsFlashed {
		return nil
	}

	score := playerflashed.Duration.Seconds() * flashScore
	if flasher.Team == nil {
		return nil
	}
	if flasher.Team.NumTeam == flashed.Team.NumTeam {
		score *= -1
	}
	flasher.ScoreFactsPerRound[round.NumRound].AssistsScore.FlashScore += score

	return nil
}
