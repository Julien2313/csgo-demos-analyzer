package modelanalyzer

import (
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

// Mark reflect how well a player played (not how much he has an impact)
type Mark struct {
	UtilityDamage int

	NbrDeaths                 int
	DollarStuffLost           int
	NbrKills                  int
	CumulativeVelocityShoots  int
	CumulativeDeltaXCrossHair float64
	CumulativeDeltaYCrossHair float64

	NbrShoots    int
	NbrShootsHit int
	NbrShootsHS  int

	NbrFirstBulletsFired int
	NbrFirstBulletsHit   int
	NbrFirstBulletsHS    int

	NbrDamageGave int

	WeaponMark map[common.EquipmentType]*WeaponMark
}

// AddKill add a kill
func (m *Mark) AddKill(weapon common.EquipmentType) {
	m.NbrKills++
	if m.WeaponMark == nil {
		m.WeaponMark = make(map[common.EquipmentType]*WeaponMark)
	}
	if m.WeaponMark[weapon] == nil {
		m.WeaponMark[weapon] = &WeaponMark{}
	}
	m.WeaponMark[weapon].AddKill()
}

// AddStuffLost add the sutff lost when dead
func (m *Mark) AddStuffLost(stuffValue int, weapon common.EquipmentType) {
	m.DollarStuffLost += stuffValue
	m.NbrDeaths++

	if m.WeaponMark == nil {
		m.WeaponMark = make(map[common.EquipmentType]*WeaponMark)
	}
	if m.WeaponMark[weapon] == nil {
		m.WeaponMark[weapon] = &WeaponMark{}
	}
	m.WeaponMark[weapon].AddDeath()
}

// AddShoot add a shoot
func (m *Mark) AddShoot(
	weapon common.EquipmentType,
	velocity int,
	isFirstBullet bool,
) {
	if weapon.Class() == common.EqClassEquipment || weapon.Class() == common.EqClassGrenade {
		return
	}
	m.NbrShoots++
	m.CumulativeVelocityShoots += velocity

	if isFirstBullet {
		m.NbrFirstBulletsFired++
	}

	if m.WeaponMark == nil {
		m.WeaponMark = make(map[common.EquipmentType]*WeaponMark)
	}
	if m.WeaponMark[weapon] == nil {
		m.WeaponMark[weapon] = &WeaponMark{}
	}
	m.WeaponMark[weapon].AddShoot(isFirstBullet, velocity)
}

// AddShootHit add a shoot hit
func (m *Mark) AddShootHit(
	isHS bool, healthDamage int,
	weapon common.EquipmentType,
	velocity int,
	deltaX, deltaY float64,
	steamIDToFind uint64,
	isFirstBullet bool,
) {
	m.NbrDamageGave += healthDamage

	if weapon.Class() == common.EqClassGrenade {
		m.UtilityDamage += healthDamage
	}
	if weapon.Class() == common.EqClassEquipment || weapon.Class() == common.EqClassGrenade {
		return
	}
	m.NbrShoots++
	m.NbrShootsHit++
	m.CumulativeVelocityShoots += velocity
	if isHS {
		m.NbrShootsHS++
	}

	if isFirstBullet {
		m.NbrFirstBulletsFired++
		m.NbrFirstBulletsHit++
		m.CumulativeDeltaXCrossHair += deltaX
		m.CumulativeDeltaYCrossHair += deltaY
		if isHS {
			m.NbrFirstBulletsHS++
		}
	}

	if m.WeaponMark == nil {
		m.WeaponMark = make(map[common.EquipmentType]*WeaponMark)
	}
	if m.WeaponMark[weapon] == nil {
		m.WeaponMark[weapon] = &WeaponMark{}
	}
	m.WeaponMark[weapon].AddShootHit(isHS, isFirstBullet, velocity, deltaX, deltaY)
}

// ConvertDB convert for the DB
func (m *Mark) ConvertDB(demoID *int64) *model.Marks {
	var (
		accuracy               float64
		hs                     float64
		firstbulletAccuracy    float64
		firstbulletHS          float64
		utilityDamage          int
		grenadesValueDeath     float64
		nbrDamageGave          int
		averageVelocityShoots  int
		averageDeltaXCrossHair int
		averageDeltaYCrossHair int
	)

	utilityDamage = m.UtilityDamage
	nbrDamageGave = m.NbrDamageGave

	if m.NbrShoots > 0 {
		accuracy = float64(m.NbrShootsHit) / float64(m.NbrShoots)
	}

	if m.NbrShootsHit > 0 {
		hs = float64(m.NbrShootsHS) / float64(m.NbrShootsHit)
	}

	if m.NbrFirstBulletsFired > 0 {
		firstbulletAccuracy = float64(m.NbrFirstBulletsHit) / float64(m.NbrFirstBulletsFired)
	}

	if m.NbrFirstBulletsHit > 0 {
		firstbulletHS = float64(m.NbrFirstBulletsHS) / float64(m.NbrFirstBulletsHit)
		averageDeltaXCrossHair = int(m.CumulativeDeltaXCrossHair / float64(m.NbrFirstBulletsHit))
		averageDeltaYCrossHair = int(m.CumulativeDeltaYCrossHair / float64(m.NbrFirstBulletsHit))
	}

	if m.NbrDeaths > 0 {
		grenadesValueDeath = float64(m.DollarStuffLost) / float64(m.NbrDeaths)
	}
	if m.NbrShoots > 0 {
		averageVelocityShoots = int(float64(m.CumulativeVelocityShoots) / float64(m.NbrShoots))
	}

	var weaponsMarks []*model.WeaponMarks
	for weaponType, weaponMarks := range m.WeaponMark {
		weaponsMarks = append(weaponsMarks, weaponMarks.ConvertDB(weaponType))
	}

	return &model.Marks{
		Accuracy:               &accuracy,
		HS:                     &hs,
		FirstBulletAccuracy:    &firstbulletAccuracy,
		FirstBulletHS:          &firstbulletHS,
		NbrBulletsFired:        &m.NbrShoots,
		NbrBulletsHit:          &m.NbrShootsHit,
		NbrBulletsHS:           &m.NbrShootsHS,
		NbrFirstBulletsFired:   &m.NbrFirstBulletsFired,
		NbrFirstBulletsHit:     &m.NbrFirstBulletsHit,
		NbrFirstBulletsHS:      &m.NbrFirstBulletsHS,
		UtilityDamage:          &utilityDamage,
		GrenadesValueDeath:     &grenadesValueDeath,
		Damage:                 &nbrDamageGave,
		WeaponsMarks:           weaponsMarks,
		AverageVelocityShoots:  &averageVelocityShoots,
		AverageDeltaXCrossHair: &averageDeltaXCrossHair,
		AverageDeltaYCrossHair: &averageDeltaYCrossHair,
		NbrDeaths:              &m.NbrDeaths,
		NbrKills:               &m.NbrKills,
	}
}
