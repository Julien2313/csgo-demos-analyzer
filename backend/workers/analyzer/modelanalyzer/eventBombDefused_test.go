package modelanalyzer_test

import (
	"math/rand"
	"testing"
	"time"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
)

func TestGetType_BombDefusedEvent(t *testing.T) {
	assert.Equal(t, modelanalyzer.BombDefusedEvt, (&modelanalyzer.BombDefusedEvent{}).GetType())
}

func TestHandleEvent_BombDefusedEvent(t *testing.T) {
	steamID := rand.Uint64()
	bombDefusedEvent := modelanalyzer.BombDefusedEvent{
		BombDefused: events.BombDefused{
			BombEvent: events.BombEvent{
				Player: &common.Player{Name: helpers.RandomString(30), SteamID64: steamID, Team: common.TeamCounterTerrorists},
			},
		},
		TimeRemainded: time.Millisecond * 16871,
	}

	players := map[uint64]*modelanalyzer.Player{
		steamID: {
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
		},
		1: {
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
		},
		2: {
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
		},
		3: {
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
		},
		4: {
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
		},
	}
	playersEvent := make(map[uint64]*modelanalyzer.PlayerEvent)
	playersEvent[steamID] = &modelanalyzer.PlayerEvent{IsAlive: true, IsCT: true, SteamID: steamID}
	playersEvent[1] = &modelanalyzer.PlayerEvent{IsAlive: false, IsCT: true, SteamID: 1}
	playersEvent[2] = &modelanalyzer.PlayerEvent{IsAlive: false, IsCT: false, SteamID: 2}
	playersEvent[3] = &modelanalyzer.PlayerEvent{IsAlive: true, IsCT: false, SteamID: 3}
	playersEvent[4] = &modelanalyzer.PlayerEvent{IsAlive: true, IsCT: false, SteamID: 4}

	bombDefusedEventFront := bombDefusedEvent.HandleEvent(
		&modelanalyzer.Game{Players: players},
		modelanalyzer.Frame{Players: playersEvent},
		&modelanalyzer.Round{NumRound: 1}).(*model.BombDefusedEvent)

	assert.Equal(t, bombDefusedEvent.TimeRemainded, *bombDefusedEventFront.TimeRemainded)
	assert.Equal(t, bombDefusedEvent.BombDefused.Player.Name, *bombDefusedEventFront.Defuser)
	assert.Equal(t, 0.0, players[steamID].ScoreFactsPerRound[0].AssistsScore.BombDefuseScore)
	assert.Equal(t, 210.0, players[steamID].ScoreFactsPerRound[1].AssistsScore.BombDefuseScore)
	assert.Equal(t, 0.0, players[steamID].ScoreFactsPerRound[2].AssistsScore.BombDefuseScore)
}
