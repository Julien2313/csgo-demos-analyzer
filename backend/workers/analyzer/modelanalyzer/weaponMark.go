package modelanalyzer

import (
	"math/rand"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

// WeaponMark reflect how well a player played (not how much he has an impact)
type WeaponMark struct {
	NbrKills  int
	NbrDeaths int

	NbrShoots    int
	NbrShootsHit int
	NbrShootsHS  int

	NbrFirstBulletsFired int
	NbrFirstBulletsHit   int
	NbrFirstBulletsHS    int

	NbrDamageGave             int
	CumulativeVelocityShoots  int
	CumulativeDeltaXCrossHair float64
	CumulativeDeltaYCrossHair float64
}

// GenerateRandomWeaponMark generate a random WeaponMark
func GenerateRandomWeaponMark() WeaponMark {
	return WeaponMark{
		NbrKills:                  rand.Intn(40),
		NbrDeaths:                 rand.Intn(30),
		NbrShoots:                 rand.Intn(2000),
		NbrShootsHit:              rand.Intn(1000),
		NbrShootsHS:               rand.Intn(500),
		NbrFirstBulletsFired:      rand.Intn(500),
		NbrFirstBulletsHit:        rand.Intn(250),
		NbrFirstBulletsHS:         rand.Intn(100),
		NbrDamageGave:             rand.Intn(3500),
		CumulativeVelocityShoots:  rand.Intn(4567),
		CumulativeDeltaXCrossHair: rand.Float64() * 30,
		CumulativeDeltaYCrossHair: rand.Float64() * 30,
	}
}

// AddKill add a kill
func (wm *WeaponMark) AddKill() {
	wm.NbrKills++
}

// AddDeath add a death
func (wm *WeaponMark) AddDeath() {
	wm.NbrDeaths++
}

// AddShoot add a shoot
func (wm *WeaponMark) AddShoot(isFirstBullet bool, velocity int) {
	wm.NbrShoots++
	wm.CumulativeVelocityShoots += velocity
	if isFirstBullet {
		wm.NbrFirstBulletsFired++
	}
}

// AddShootHit add a shoot hit
func (wm *WeaponMark) AddShootHit(isHS bool, isFirstBullet bool, velocity int, deltaX, deltaY float64) {
	wm.NbrShoots++
	wm.NbrShootsHit++
	wm.CumulativeVelocityShoots += velocity
	if isHS {
		wm.NbrShootsHS++
	}
	if isFirstBullet {
		wm.NbrFirstBulletsFired++
		wm.NbrFirstBulletsHit++
		wm.CumulativeDeltaXCrossHair += deltaX
		wm.CumulativeDeltaYCrossHair += deltaY
		if isHS {
			wm.NbrFirstBulletsHS++
		}
	}
}

// ConvertDB convert for the DB
func (wm *WeaponMark) ConvertDB(weaponType common.EquipmentType) *model.WeaponMarks {
	var (
		accuracy               float64
		hs                     float64
		firstBulletAccuracy    float64
		firstBulletHS          float64
		averageVelocityShoots  int
		averageDeltaXCrossHair int
		averageDeltaYCrossHair int
	)

	if wm.NbrShoots > 0 {
		accuracy = float64(wm.NbrShootsHit) / float64(wm.NbrShoots)
	}

	if wm.NbrShootsHit > 0 {
		hs = float64(wm.NbrShootsHS) / float64(wm.NbrShootsHit)
	}

	if wm.NbrFirstBulletsFired > 0 {
		firstBulletAccuracy = float64(wm.NbrFirstBulletsHit) / float64(wm.NbrFirstBulletsFired)
	}

	if wm.NbrFirstBulletsHit > 0 {
		firstBulletHS = float64(wm.NbrFirstBulletsHS) / float64(wm.NbrFirstBulletsHit)
		averageDeltaXCrossHair = int(wm.CumulativeDeltaXCrossHair / float64(wm.NbrFirstBulletsHit))
		averageDeltaYCrossHair = int(wm.CumulativeDeltaYCrossHair / float64(wm.NbrFirstBulletsHit))
	}

	if wm.NbrShoots > 0 {
		averageVelocityShoots = int(float64(wm.CumulativeVelocityShoots) / float64(wm.NbrShoots))
	}

	return &model.WeaponMarks{
		WeaponType:             &weaponType,
		NbrBulletsFired:        &wm.NbrShoots,
		NbrBulletsHit:          &wm.NbrShootsHit,
		NbrBulletsHS:           &wm.NbrShootsHS,
		NbrFirstBulletsFired:   &wm.NbrFirstBulletsFired,
		NbrFirstBulletsHit:     &wm.NbrFirstBulletsHit,
		NbrFirstBulletsHS:      &wm.NbrFirstBulletsHS,
		Accuracy:               &accuracy,
		HS:                     &hs,
		FirstBulletAccuracy:    &firstBulletAccuracy,
		FirstBulletHS:          &firstBulletHS,
		AverageVelocityShoots:  &averageVelocityShoots,
		AverageDeltaXCrossHair: &averageDeltaXCrossHair,
		AverageDeltaYCrossHair: &averageDeltaYCrossHair,
		Damage:                 &wm.NbrDamageGave,
		NbrDeaths:              &wm.NbrDeaths,
		NbrKills:               &wm.NbrKills,
	}
}
