package modelanalyzer_test

import (
	"math/rand"
	"testing"

	"github.com/golang/geo/r3"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
)

func TestGetType_WeaponFireEvent(t *testing.T) {
	assert.Equal(t, modelanalyzer.WeaponFireEvt, (&modelanalyzer.WeaponFireEvent{}).GetType())
}
func TestHandleEvent_WeaponFireEvent(t *testing.T) {
	weaponFireEvent := modelanalyzer.WeaponFireEvent{}

	weaponFireEventFront := weaponFireEvent.HandleEvent(
		&modelanalyzer.Game{},
		modelanalyzer.Frame{},
		&modelanalyzer.Round{})
	assert.Nil(t, weaponFireEventFront)

	steamIDAttacker := rand.Uint64()
	steamIDEnnemie := rand.Uint64()
	players := map[uint64]*modelanalyzer.Player{
		steamIDAttacker: {
			Team:               &modelanalyzer.Team{NumTeam: 2},
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
		},
	}

	weaponFireEvent = modelanalyzer.WeaponFireEvent{
		WeaponFire: events.WeaponFire{
			Shooter: &common.Player{SteamID64: steamIDAttacker, Team: common.TeamCounterTerrorists},
			Weapon: &common.Equipment{
				Type: common.EqAK47,
			},
		},
	}
	frames := modelanalyzer.Frame{
		Players: map[uint64]*modelanalyzer.PlayerEvent{
			steamIDAttacker: {
				Side:     int(common.TeamCounterTerrorists),
				IsAlive:  true,
				Recoil:   2,
				Velocity: 100,
			},
			steamIDEnnemie: {
				Side:    int(common.TeamTerrorists),
				IsAlive: true,
			},
		},
	}
	round := &modelanalyzer.Round{
		MarksPerRound: make(map[uint64]*modelanalyzer.MarksPerRound),
	}
	weaponFireEventFront = weaponFireEvent.HandleEvent(
		&modelanalyzer.Game{Players: players},
		frames,
		round)
	assert.Nil(t, weaponFireEventFront)

	assert.Equal(t, 1, players[steamIDAttacker].Mark.NbrShoots)
	assert.Equal(t, 0, players[steamIDAttacker].Mark.NbrFirstBulletsFired)
	assert.Equal(t, 0, players[steamIDAttacker].Mark.NbrShootsHS)
	assert.Equal(t, 0, players[steamIDAttacker].Mark.NbrShootsHit)
	assert.Equal(t, 100, players[steamIDAttacker].Mark.CumulativeVelocityShoots)
	assert.Equal(t, 1, frames.Players[steamIDAttacker].NbrShoots)
}

func TestHandleEvent_WeaponFireEventNoEnnemies(t *testing.T) {
	steamIDAttacker := rand.Uint64()
	steamIDEnnemie := rand.Uint64()
	players := map[uint64]*modelanalyzer.Player{
		steamIDAttacker: {
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
		},
	}

	weaponFireEvent := modelanalyzer.WeaponFireEvent{
		WeaponFire: events.WeaponFire{
			Shooter: &common.Player{SteamID64: steamIDAttacker, Team: common.TeamCounterTerrorists},
			Weapon: &common.Equipment{
				Type: common.EqAK47,
			},
		},
	}
	frames := modelanalyzer.Frame{
		Players: map[uint64]*modelanalyzer.PlayerEvent{
			steamIDAttacker: {
				Side:    int(common.TeamCounterTerrorists),
				IsAlive: true,
			},
			steamIDEnnemie: {
				Side:    int(common.TeamTerrorists),
				IsAlive: false,
			},
		},
	}
	weaponFireEventFront := weaponFireEvent.HandleEvent(
		&modelanalyzer.Game{Players: players},
		frames,
		&modelanalyzer.Round{})
	assert.Nil(t, weaponFireEventFront)

	assert.Equal(t, 0, players[steamIDAttacker].Mark.NbrShoots)
	assert.Equal(t, 0, players[steamIDAttacker].Mark.NbrShootsHS)
	assert.Equal(t, 0, players[steamIDAttacker].Mark.NbrShootsHit)
	assert.Equal(t, 1, frames.Players[steamIDAttacker].NbrShoots)
}

func TestHandleEvent_WeaponFireEventEnnemyNotOnSight(t *testing.T) {
	steamIDAttacker := rand.Uint64()
	steamIDEnnemie := rand.Uint64()
	players := map[uint64]*modelanalyzer.Player{
		steamIDAttacker: {
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
		},
	}

	weaponFireEvent := modelanalyzer.WeaponFireEvent{
		WeaponFire: events.WeaponFire{
			Shooter: &common.Player{SteamID64: steamIDAttacker, Team: common.TeamCounterTerrorists},
			Weapon: &common.Equipment{
				Type: common.EqAK47,
			},
		},
	}
	frames := modelanalyzer.Frame{
		Players: map[uint64]*modelanalyzer.PlayerEvent{
			steamIDAttacker: {
				Side:     int(common.TeamCounterTerrorists),
				IsAlive:  true,
				Position: r3.Vector{X: 0, Y: 0, Z: 0},
			},
			steamIDEnnemie: {
				Side:     int(common.TeamTerrorists),
				IsAlive:  true,
				Position: r3.Vector{X: -10, Y: -10, Z: 0},
			},
		},
	}
	weaponFireEventFront := weaponFireEvent.HandleEvent(
		&modelanalyzer.Game{Players: players},
		frames,
		&modelanalyzer.Round{})
	assert.Nil(t, weaponFireEventFront)

	assert.Equal(t, 0, players[steamIDAttacker].Mark.NbrShoots)
	assert.Equal(t, 0, players[steamIDAttacker].Mark.NbrShootsHS)
	assert.Equal(t, 0, players[steamIDAttacker].Mark.NbrShootsHit)
	assert.Equal(t, 1, frames.Players[steamIDAttacker].NbrShoots)
}
