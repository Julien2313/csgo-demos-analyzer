package modelanalyzer_test

import (
	"math/rand"
	"testing"
	"time"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
)

func TestGetType_PlayerFlashedEvent(t *testing.T) {
	assert.Equal(t, modelanalyzer.PlayerFlashedEvt, (&modelanalyzer.PlayerFlashedEvent{}).GetType())
}

func TestHandleEvent_PlayerFlashed(t *testing.T) {
	steamIDFlasher := rand.Uint64()
	steamIDFlashed1 := rand.Uint64()
	steamIDFlashed2 := rand.Uint64()

	playerFlashedEvent := modelanalyzer.PlayerFlashedEvent{
		TimeEvent: time.Millisecond * 16871,
		PlayerFlashed: events.PlayerFlashed{
			Player:     &common.Player{SteamID64: steamIDFlashed1, FlashDuration: float32(3)},
			Attacker:   &common.Player{SteamID64: steamIDFlasher},
			Projectile: common.NewGrenadeProjectile(),
		},
	}

	team1 := &modelanalyzer.Team{NumTeam: 2}
	team2 := &modelanalyzer.Team{NumTeam: 3}
	players := map[uint64]*modelanalyzer.Player{
		steamIDFlasher: {
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
			Team:               team1,
		},
		steamIDFlashed1: {
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
			Team:               team1,
		},
		steamIDFlashed2: {
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
			Team:               team2,
		},
	}
	game := modelanalyzer.Game{
		Players: players,
	}

	round := modelanalyzer.Round{
		NumRound:          1,
		TimeFreezeTimeEnd: helpers.DurationPtr(time.Second * 15),
		SideWin:           2,
	}

	playerFlashedEventFront := playerFlashedEvent.HandleEvent(&game, modelanalyzer.Frame{}, &round)
	assert.Nil(t, playerFlashedEventFront)

	assert.Len(t, players[steamIDFlasher].Flashs, 1)
	assert.Equal(t, -6.0, helpers.Round(players[steamIDFlasher].ScoreFactsPerRound[1].AssistsScore.FlashScore, 2))

	playerFlashedEvent2 := modelanalyzer.PlayerFlashedEvent{
		TimeEvent: time.Millisecond * 16871,
		PlayerFlashed: events.PlayerFlashed{
			Player:     &common.Player{SteamID64: steamIDFlashed2, FlashDuration: float32(5)},
			Attacker:   &common.Player{SteamID64: steamIDFlasher},
			Projectile: common.NewGrenadeProjectile(),
		},
	}
	playerFlashedEventFront2 := playerFlashedEvent2.HandleEvent(&game, modelanalyzer.Frame{}, &round)
	assert.Nil(t, playerFlashedEventFront2)
	assert.Len(t, players[steamIDFlasher].Flashs, 2)
	assert.Equal(t, 6.0, helpers.Round(players[steamIDFlasher].ScoreFactsPerRound[1].AssistsScore.FlashScore, 2))
	assert.Len(t, game.Flashs, 2)
}

func TestHandleEvent_PlayerFlashedSecondsRemoved(t *testing.T) {
	steamIDFlasher := rand.Uint64()
	steamIDFlashed1 := rand.Uint64()
	steamIDFlashed2 := rand.Uint64()

	playerFlashedEvent := modelanalyzer.PlayerFlashedEvent{
		TimeEvent: time.Millisecond * 16871,
		PlayerFlashed: events.PlayerFlashed{
			Player:     &common.Player{SteamID64: steamIDFlashed1, FlashDuration: 0.5},
			Attacker:   &common.Player{SteamID64: steamIDFlasher},
			Projectile: common.NewGrenadeProjectile(),
		},
	}

	team1 := &modelanalyzer.Team{NumTeam: 2}
	team2 := &modelanalyzer.Team{NumTeam: 3}
	players := map[uint64]*modelanalyzer.Player{
		steamIDFlasher: {
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
			Team:               team1,
		},
		steamIDFlashed1: {
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
			Team:               team1,
		},
		steamIDFlashed2: {
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
			Team:               team2,
		},
	}
	game := modelanalyzer.Game{
		Players: players,
	}

	round := modelanalyzer.Round{
		NumRound:          1,
		TimeFreezeTimeEnd: helpers.DurationPtr(time.Second * 15),
		SideWin:           2,
	}

	playerFlashedEventFront := playerFlashedEvent.HandleEvent(&game, modelanalyzer.Frame{}, &round)
	assert.Nil(t, playerFlashedEventFront)

	assert.Len(t, players[steamIDFlasher].Flashs, 1)
	assert.Equal(t, 0.0, helpers.Round(players[steamIDFlasher].ScoreFactsPerRound[1].AssistsScore.FlashScore, 2))

	playerFlashedEvent2 := modelanalyzer.PlayerFlashedEvent{
		TimeEvent: time.Millisecond * 16871,
		PlayerFlashed: events.PlayerFlashed{
			Player:     &common.Player{SteamID64: steamIDFlashed2, FlashDuration: 0.5},
			Attacker:   &common.Player{SteamID64: steamIDFlasher},
			Projectile: common.NewGrenadeProjectile(),
		},
	}
	playerFlashedEventFront2 := playerFlashedEvent2.HandleEvent(&game, modelanalyzer.Frame{}, &round)
	assert.Nil(t, playerFlashedEventFront2)
	assert.Len(t, players[steamIDFlasher].Flashs, 2)
	assert.Equal(t, 0.0, helpers.Round(players[steamIDFlasher].ScoreFactsPerRound[1].AssistsScore.FlashScore, 2))
	assert.Len(t, game.Flashs, 2)
}

func TestHandleEvent_PlayerFlashedMultiple(t *testing.T) {
	steamIDFlasher := rand.Uint64()
	steamIDFlashed1 := rand.Uint64()
	steamIDFlashed2 := rand.Uint64()
	projectile := common.NewGrenadeProjectile()

	playerFlashedEvent := modelanalyzer.PlayerFlashedEvent{
		TimeEvent: time.Millisecond * 16871,
		PlayerFlashed: events.PlayerFlashed{
			Player:     &common.Player{SteamID64: steamIDFlashed1, FlashDuration: 2.5},
			Attacker:   &common.Player{SteamID64: steamIDFlasher},
			Projectile: projectile,
		},
	}

	team1 := &modelanalyzer.Team{NumTeam: 2}
	team2 := &modelanalyzer.Team{NumTeam: 3}
	players := map[uint64]*modelanalyzer.Player{
		steamIDFlasher: {
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
			Team:               team1,
		},
		steamIDFlashed1: {
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
			Team:               team1,
		},
		steamIDFlashed2: {
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
			Team:               team2,
		},
	}
	game := modelanalyzer.Game{
		Players: players,
	}

	round := modelanalyzer.Round{
		NumRound:          1,
		TimeFreezeTimeEnd: helpers.DurationPtr(time.Second * 15),
		SideWin:           2,
	}

	playerFlashedEventFront := playerFlashedEvent.HandleEvent(&game, modelanalyzer.Frame{}, &round)
	assert.Nil(t, playerFlashedEventFront)

	assert.Len(t, players[steamIDFlasher].Flashs, 1)
	assert.Equal(t, -4.5, helpers.Round(players[steamIDFlasher].ScoreFactsPerRound[1].AssistsScore.FlashScore, 2))

	playerFlashedEvent2 := modelanalyzer.PlayerFlashedEvent{
		TimeEvent: time.Millisecond * 16871,
		PlayerFlashed: events.PlayerFlashed{
			Player:     &common.Player{SteamID64: steamIDFlashed2, FlashDuration: 0.5},
			Attacker:   &common.Player{SteamID64: steamIDFlasher},
			Projectile: projectile,
		},
	}
	playerFlashedEventFront2 := playerFlashedEvent2.HandleEvent(&game, modelanalyzer.Frame{}, &round)
	assert.Nil(t, playerFlashedEventFront2)
	assert.Len(t, players[steamIDFlasher].Flashs, 1)
	assert.Len(t, players[steamIDFlasher].Flashs[0].PlayersFlashed, 2)
	assert.Equal(t, -4.5, helpers.Round(players[steamIDFlasher].ScoreFactsPerRound[1].AssistsScore.FlashScore, 2))

	assert.Len(t, game.Flashs, 1)
}
