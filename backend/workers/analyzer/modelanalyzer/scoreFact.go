package modelanalyzer

import "gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"

// ScoreFacts show how much the player made an impact during the round
// in example, accuracy would me in a mark, not here, because the player could have a very bad accuracy,
// but still have a bug impact
type ScoreFacts struct {
	NumRound int

	RatioScore   float64
	TotalScore   float64
	AttackScore  AttackScore
	AssistsScore AssistsScore
}

// AttackScore reflects how a player was useful at BIG WARIOR
type AttackScore struct {
	DamageScore     float64
	KillScore       float64
	DeathScore      float64
	AssistKillScore float64
}

// AssistsScore reflects how a player assists his team
type AssistsScore struct {
	DropScore     float64
	FlashScore    float64
	RevangedScore float64

	BombDefuseScore  float64
	BombPlantedScore float64
}

// ComputeTotalScore calculate the total of score from everything
func (sf *ScoreFacts) ComputeTotalScore() {
	sf.TotalScore = sf.AttackScore.ComputeTotalScore() + sf.AssistsScore.ComputeTotalScore()
}

// AddScoreFacts add a score this score
func (sf *ScoreFacts) AddScoreFacts(scoretoAdd ScoreFacts) {
	sf.TotalScore += scoretoAdd.TotalScore
	sf.AttackScore.DamageScore += scoretoAdd.AttackScore.DamageScore
	sf.AttackScore.KillScore += scoretoAdd.AttackScore.KillScore
	sf.AttackScore.DeathScore += scoretoAdd.AttackScore.DeathScore
	sf.AttackScore.AssistKillScore += scoretoAdd.AttackScore.AssistKillScore
	sf.AssistsScore.DropScore += scoretoAdd.AssistsScore.DropScore
	sf.AssistsScore.FlashScore += scoretoAdd.AssistsScore.FlashScore
	sf.AssistsScore.RevangedScore += scoretoAdd.AssistsScore.RevangedScore
	sf.AssistsScore.BombDefuseScore += scoretoAdd.AssistsScore.BombDefuseScore
	sf.AssistsScore.BombPlantedScore += scoretoAdd.AssistsScore.BombPlantedScore
}

// ConvertForDB convert a analyzer.scoreFact to a model for DB
func (sf *ScoreFacts) ConvertForDB(steamID string) *model.ScoreFacts {
	return &model.ScoreFacts{
		SteamID: &steamID,

		RatioScore: &sf.RatioScore,
		TotalScore: &sf.TotalScore,

		DamageScore:     &sf.AttackScore.DamageScore,
		KillScore:       &sf.AttackScore.KillScore,
		DeathScore:      &sf.AttackScore.DeathScore,
		AssistKillScore: &sf.AttackScore.AssistKillScore,

		DropScore:        &sf.AssistsScore.DropScore,
		FlashScore:       &sf.AssistsScore.FlashScore,
		RevangedScore:    &sf.AssistsScore.RevangedScore,
		BombDefuseScore:  &sf.AssistsScore.BombDefuseScore,
		BombPlantedScore: &sf.AssistsScore.BombPlantedScore,
	}
}

// ComputeTotalScore calculate the total attack score
func (as AttackScore) ComputeTotalScore() float64 {
	return as.DamageScore + as.KillScore + as.DeathScore + as.AssistKillScore
}

// ComputeTotalScore calculate the total assist score
func (as AssistsScore) ComputeTotalScore() float64 {
	return as.DropScore + as.FlashScore + as.RevangedScore + as.BombDefuseScore + as.BombPlantedScore
}
