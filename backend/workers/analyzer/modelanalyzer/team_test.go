package modelanalyzer_test

import (
	"testing"

	"github.com/maxatome/go-testdeep/td"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
)

func TestTeam_GetConstTeamNum(t *testing.T) {
	team := modelanalyzer.Team{NumTeam: 2}

	td.Cmp(t, team.GetConstTeamNum(0), team.NumTeam)
	td.Cmp(t, team.GetConstTeamNum(7), team.NumTeam)
	td.Cmp(t, team.GetConstTeamNum(8), team.NumTeam)
	td.Cmp(t, team.GetConstTeamNum(14), team.NumTeam)
	td.Cmp(t, team.GetConstTeamNum(15), 5-team.NumTeam)
	td.Cmp(t, team.GetConstTeamNum(17), 5-team.NumTeam)
	td.Cmp(t, team.GetConstTeamNum(20), 5-team.NumTeam)
	td.Cmp(t, team.GetConstTeamNum(23), 5-team.NumTeam)
	td.Cmp(t, team.GetConstTeamNum(28), 5-team.NumTeam)
	td.Cmp(t, team.GetConstTeamNum(29), 5-team.NumTeam)
	td.Cmp(t, team.GetConstTeamNum(30), 5-team.NumTeam)
	td.Cmp(t, team.GetConstTeamNum(31), 5-team.NumTeam)
	td.Cmp(t, team.GetConstTeamNum(32), 5-team.NumTeam)
	td.Cmp(t, team.GetConstTeamNum(33), team.NumTeam)
	td.Cmp(t, team.GetConstTeamNum(34), team.NumTeam)
	td.Cmp(t, team.GetConstTeamNum(35), team.NumTeam)
	td.Cmp(t, team.GetConstTeamNum(36), team.NumTeam)
	td.Cmp(t, team.GetConstTeamNum(37), team.NumTeam)
	td.Cmp(t, team.GetConstTeamNum(38), team.NumTeam)
	td.Cmp(t, team.GetConstTeamNum(39), 5-team.NumTeam)
}
