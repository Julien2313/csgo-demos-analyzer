package modelanalyzer_test

import (
	"math/rand"
	"testing"
	"time"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
)

func TestGetType_BombPlantedEvent(t *testing.T) {
	assert.Equal(t, modelanalyzer.BombPlantedEvt, (&modelanalyzer.BombPlantedEvent{}).GetType())
}

func TestHandleEvent_BombPlantedEvent(t *testing.T) {
	steamID := rand.Uint64()
	bombPlantedEvent := modelanalyzer.BombPlantedEvent{
		BombPlanted: events.BombPlanted{
			BombEvent: events.BombEvent{
				Player: &common.Player{Name: helpers.RandomString(30), SteamID64: steamID},
				Site:   'A',
			},
		},
		TimeEvent: time.Millisecond * 16871,
	}

	players := map[uint64]*modelanalyzer.Player{
		steamID: {
			ScoreFactsPerRound: []*modelanalyzer.ScoreFacts{{}, {}, {}},
		},
	}

	round := modelanalyzer.Round{
		NumRound:          1,
		TimeFreezeTimeEnd: helpers.DurationPtr(time.Second * 60),
	}

	bombPlantedEventFront := bombPlantedEvent.HandleEvent(
		&modelanalyzer.Game{Players: players},
		modelanalyzer.Frame{},
		&round).(*model.BombPlantedEvent)

	assert.Equal(t, bombPlantedEvent.TimeEvent-*round.TimeFreezeTimeEnd, *bombPlantedEventFront.TimePlanted)
	assert.Equal(t, bombPlantedEvent.BombPlanted.Player.Name, *bombPlantedEventFront.Planter)
	assert.Equal(t, string(bombPlantedEvent.BombPlanted.Site), *bombPlantedEventFront.Bombsite)

	assert.Equal(t, 0.0, players[steamID].ScoreFactsPerRound[0].AssistsScore.BombPlantedScore)
	assert.Equal(t, 10.0, players[steamID].ScoreFactsPerRound[1].AssistsScore.BombPlantedScore)
	assert.Equal(t, 0.0, players[steamID].ScoreFactsPerRound[2].AssistsScore.BombPlantedScore)
}
