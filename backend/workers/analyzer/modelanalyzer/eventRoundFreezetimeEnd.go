package modelanalyzer

import (
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

// RoundFreezetimeEndEvent event when freeze time is over
type RoundFreezetimeEndEvent struct {
}

// GetType type of event
func (rfe *RoundFreezetimeEndEvent) GetType() EventType {
	return RoundFreezetimeEndEvt
}

// HandleEvent return a model.Event, add score etc
func (rfe *RoundFreezetimeEndEvent) HandleEvent(game *Game, frame Frame, round *Round) model.Event {
	return &model.RoundFreezetimeEndEvent{}
}
