package modelanalyzer

import "github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"

// Team struct of team
type Team struct {
	Players map[uint64]*Player
	Score   int

	DuelsMatrix map[DuelIndex]*[]Duel

	NumTeam       int
	NumTeamParser common.Team
}

// GetConstTeamNum return the side from the first round
func (t *Team) GetConstTeamNum(numRound int) int {
	return GetConstTeamNum(t.NumTeam, numRound)
}

// GetConstTeamNum returns the team from the side and the round number
func GetConstTeamNum(side, numRound int) int {
	// OMG, i really want to throw up
	if numRound < 15 {
		return side
	} else if numRound < 33 {
		// 5 - 2 == 3, 5 - 3 == 2
		return 5 - side
	} else if numRound < 39 {
		return side
	} else if numRound < 45 {
		return 5 - side
	} else if numRound < 51 {
		return side
	} else if numRound < 57 {
		return 5 - side
	} else if numRound < 63 {
		return side
	} else if numRound < 69 {
		return 5 - side
	} else if numRound < 75 {
		return side
	} else if numRound < 81 {
		return 5 - side
	} else if numRound < 87 {
		return side
	} else if numRound < 93 {
		return 5 - side
	}

	return -1
}

// GetSideFromTeam returns the side from the team and the round number
func GetSideFromTeam(team, numRound int) int {
	// OMG, i really want to throw up
	if numRound < 15 {
		return team
	} else if numRound < 33 {
		// 5 - 2 == 3, 5 - 3 == 2
		return 5 - team
	} else if numRound < 39 {
		return team
	} else if numRound < 45 {
		return 5 - team
	} else if numRound < 51 {
		return team
	} else if numRound < 57 {
		return 5 - team
	} else if numRound < 63 {
		return team
	} else if numRound < 69 {
		return 5 - team
	} else if numRound < 75 {
		return team
	} else if numRound < 81 {
		return 5 - team
	} else if numRound < 87 {
		return team
	} else if numRound < 93 {
		return 5 - team
	}

	return -1
}
