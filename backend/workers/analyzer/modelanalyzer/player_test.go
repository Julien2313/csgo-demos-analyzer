package modelanalyzer_test

import (
	"math/rand"
	"strconv"
	"testing"
	"time"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
)

func TestPlayer_ComputeFlashScore(t *testing.T) {
	team1 := &modelanalyzer.Team{NumTeam: 2}
	team2 := &modelanalyzer.Team{NumTeam: 3}

	player1 := modelanalyzer.PlayerFlashed{Flashed: &modelanalyzer.Player{Team: team1}, Duration: time.Second * 2}
	player2 := modelanalyzer.PlayerFlashed{Flashed: &modelanalyzer.Player{Team: team2}, Duration: time.Second * 2}
	player3 := modelanalyzer.PlayerFlashed{Flashed: &modelanalyzer.Player{Team: team2}, Duration: time.Second * 1}
	player4 := modelanalyzer.PlayerFlashed{Flashed: &modelanalyzer.Player{Team: team1}, Duration: time.Second * 4}

	flash1 := &modelanalyzer.Flash{
		Thrower:        &modelanalyzer.Player{Team: team1},
		PlayersFlashed: []modelanalyzer.PlayerFlashed{player1, player2, player3, player4},
	}

	player5 := modelanalyzer.PlayerFlashed{Flashed: &modelanalyzer.Player{Team: team1}, Duration: time.Second * 2}
	player6 := modelanalyzer.PlayerFlashed{Flashed: &modelanalyzer.Player{Team: team2}, Duration: time.Second * 2}
	player7 := modelanalyzer.PlayerFlashed{Flashed: &modelanalyzer.Player{Team: team2}, Duration: time.Second * 1}
	player8 := modelanalyzer.PlayerFlashed{Flashed: &modelanalyzer.Player{Team: team1}, Duration: time.Second * 4}

	flash2 := &modelanalyzer.Flash{
		Thrower:        &modelanalyzer.Player{Team: team1},
		PlayersFlashed: []modelanalyzer.PlayerFlashed{player5, player6, player7, player8},
	}

	player := modelanalyzer.Player{Flashs: []*modelanalyzer.Flash{flash1, flash2}}

	player.ComputeFlashScore(10)
	assert.Equal(t, -0.6, player.StatsFlashs.Score)
}

func TestPlayer_ComputeMatrixKills(t *testing.T) {
	teams := make(map[int]*modelanalyzer.Team)
	teams[0] = &modelanalyzer.Team{NumTeam: 2}
	teams[1] = &modelanalyzer.Team{NumTeam: 3, Players: make(map[uint64]*modelanalyzer.Player)}
	teams[1].Players[0] = &modelanalyzer.Player{SteamID: rand.Uint64(), Username: helpers.RandomString(15)}
	teams[1].Players[1] = &modelanalyzer.Player{SteamID: rand.Uint64(), Username: helpers.RandomString(15)}
	teams[1].Players[2] = &modelanalyzer.Player{SteamID: rand.Uint64(), Username: helpers.RandomString(15)}
	teams[1].Players[3] = &modelanalyzer.Player{SteamID: rand.Uint64(), Username: helpers.RandomString(15)}
	teams[1].Players[4] = &modelanalyzer.Player{SteamID: rand.Uint64(), Username: helpers.RandomString(15)}

	player := &modelanalyzer.Player{Team: teams[0]}

	kills := []*modelanalyzer.Kill{}
	kills = append(kills, &modelanalyzer.Kill{})
	kills = append(kills, &modelanalyzer.Kill{Killer: player})
	kills = append(kills, &modelanalyzer.Kill{Killer: player, Victim: teams[1].Players[0]})
	kills = append(kills, &modelanalyzer.Kill{Killer: player, Victim: teams[1].Players[0]})
	kills = append(kills, &modelanalyzer.Kill{Killer: player, Victim: teams[1].Players[1]})
	kills = append(kills, &modelanalyzer.Kill{Killer: player, Victim: teams[1].Players[1]})
	kills = append(kills, &modelanalyzer.Kill{Killer: player, Victim: teams[1].Players[1]})
	kills = append(kills, &modelanalyzer.Kill{Killer: player, Victim: teams[1].Players[2]})
	kills = append(kills, &modelanalyzer.Kill{Killer: player, Victim: teams[1].Players[4]})
	kills = append(kills, &modelanalyzer.Kill{Killer: player, Victim: teams[1].Players[4]})
	kills = append(kills, &modelanalyzer.Kill{Killer: player, Victim: teams[1].Players[4]})
	kills = append(kills, &modelanalyzer.Kill{Killer: player, Victim: teams[1].Players[4]})
	kills = append(kills, &modelanalyzer.Kill{Killer: player, Victim: teams[1].Players[4]})

	player.Kills = kills

	matrixKills := player.ComputeMatrixKills(teams)
	assert.Len(t, matrixKills, 5)

	for _, matrixKill := range matrixKills {
		if *matrixKill.KilledSteamID == strconv.FormatUint(teams[1].Players[0].SteamID, 10) {
			assert.Equal(t, 2, *matrixKill.NbrKills)
		} else if *matrixKill.KilledSteamID == strconv.FormatUint(teams[1].Players[1].SteamID, 10) {
			assert.Equal(t, 3, *matrixKill.NbrKills)
		} else if *matrixKill.KilledSteamID == strconv.FormatUint(teams[1].Players[2].SteamID, 10) {
			assert.Equal(t, 1, *matrixKill.NbrKills)
		} else if *matrixKill.KilledSteamID == strconv.FormatUint(teams[1].Players[3].SteamID, 10) {
			assert.Equal(t, 0, *matrixKill.NbrKills)
		} else if *matrixKill.KilledSteamID == strconv.FormatUint(teams[1].Players[4].SteamID, 10) {
			assert.Equal(t, 5, *matrixKill.NbrKills)
		} else {
			assert.Equal(t, 0, *matrixKill.NbrKills)
		}
	}
}

func TestPlayer_ComputeMatrixFlashs(t *testing.T) {
	player := modelanalyzer.Player{SteamID: rand.Uint64(), Username: helpers.RandomString(15)}
	teams := make(map[int]*modelanalyzer.Team)
	teams[0] = &modelanalyzer.Team{NumTeam: 2, Players: make(map[uint64]*modelanalyzer.Player)}
	player.Team = teams[0]
	teams[0].Players[0] = &player
	teams[0].Players[1] = &modelanalyzer.Player{Team: teams[0], SteamID: rand.Uint64(), Username: helpers.RandomString(15)}
	teams[0].Players[2] = &modelanalyzer.Player{Team: teams[0], SteamID: rand.Uint64(), Username: helpers.RandomString(15)}
	teams[0].Players[3] = &modelanalyzer.Player{Team: teams[0], SteamID: rand.Uint64(), Username: helpers.RandomString(15)}
	teams[0].Players[4] = &modelanalyzer.Player{Team: teams[0], SteamID: rand.Uint64(), Username: helpers.RandomString(15)}
	teams[1] = &modelanalyzer.Team{NumTeam: 3, Players: make(map[uint64]*modelanalyzer.Player)}
	teams[1].Players[0] = &modelanalyzer.Player{Team: teams[1], SteamID: rand.Uint64(), Username: helpers.RandomString(15)}
	teams[1].Players[1] = &modelanalyzer.Player{Team: teams[1], SteamID: rand.Uint64(), Username: helpers.RandomString(15)}
	teams[1].Players[2] = &modelanalyzer.Player{Team: teams[1], SteamID: rand.Uint64(), Username: helpers.RandomString(15)}
	teams[1].Players[3] = &modelanalyzer.Player{Team: teams[1], SteamID: rand.Uint64(), Username: helpers.RandomString(15)}
	teams[1].Players[4] = &modelanalyzer.Player{Team: teams[1], SteamID: rand.Uint64(), Username: helpers.RandomString(15)}

	flash1 := modelanalyzer.Flash{
		PlayersFlashed: []modelanalyzer.PlayerFlashed{{
			FlashedParser: &common.Player{SteamID64: teams[0].Players[1].SteamID},
			Flashed:       teams[0].Players[1],
			Duration:      time.Millisecond * 3745,
		}},
		Thrower: &player,
	}
	flash1.ComputeScore()
	flash2 := modelanalyzer.Flash{
		PlayersFlashed: []modelanalyzer.PlayerFlashed{{
			FlashedParser: &common.Player{SteamID64: teams[0].Players[1].SteamID},
			Flashed:       teams[0].Players[1],
			Duration:      time.Millisecond * 1000,
		}},
		Thrower: &player,
	}
	flash2.ComputeScore()
	player.Flashs = append(player.Flashs, &flash1, &flash2)

	matrixFlashs := player.ComputeMatrixFlashs(teams)
	require.Len(t, matrixFlashs, 10)

	for _, matrixFlash := range matrixFlashs {
		if *matrixFlash.FlashedSteamID == strconv.FormatUint(teams[0].Players[1].SteamID, 10) {
			assert.Equal(t, 4.745, *matrixFlash.NbrSecFlashs)
		} else {
			assert.Equal(t, 0.0, *matrixFlash.NbrSecFlashs)
		}
	}
}

func TestPlayer_CountNbrEnnemiesKilled(t *testing.T) {
	player := modelanalyzer.Player{
		Kills: []*modelanalyzer.Kill{
			{TeamKill: true},
			{TeamKill: false},
			{TeamKill: true},
			{TeamKill: false},
			{TeamKill: false},
			{TeamKill: true},
		},
	}

	assert.Equal(t, 3, *player.CountNbrEnnemiesKilled())
}

func TestPlayer_CountNbrEnnemiesKillAssisted(t *testing.T) {
	player := modelanalyzer.Player{
		Assists: []*modelanalyzer.Assist{
			{TeamKill: true},
			{TeamKill: false},
			{TeamKill: true},
			{TeamKill: false},
			{TeamKill: false},
			{TeamKill: true},
		},
	}

	assert.Equal(t, 3, *player.CountNbrEnnemiesKillAssisted())
}
