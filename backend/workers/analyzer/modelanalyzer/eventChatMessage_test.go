package modelanalyzer_test

import (
	"math/rand"
	"testing"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
)

func TestGetType_ChatMessageEvent(t *testing.T) {
	assert.Equal(t, modelanalyzer.ChatMessageEvt, (&modelanalyzer.ChatMessageEvent{}).GetType())
}
func TestHandleEvent_ChatMessageEvent(t *testing.T) {
	chatMessageEvent := modelanalyzer.ChatMessageEvent{
		ChatMessage: events.ChatMessage{
			Sender:    &common.Player{Name: helpers.RandomString(15), Team: common.Team(rand.Intn(2) + 2)},
			IsChatAll: rand.Intn(2) == 0,
			Text:      helpers.RandomString(50),
		},
	}

	round := &modelanalyzer.Round{}
	assert.Nil(t, chatMessageEvent.HandleEvent(
		&modelanalyzer.Game{},
		modelanalyzer.Frame{},
		round,
	))
	require.Len(t, round.ChatMessages, 1)
	assert.Equal(t, chatMessageEvent.ChatMessage.Sender.Name, round.ChatMessages[0].Sender)
	assert.Equal(t, int(chatMessageEvent.ChatMessage.Sender.Team), round.ChatMessages[0].SideSender)
	assert.Equal(t, chatMessageEvent.ChatMessage.IsChatAll, round.ChatMessages[0].IsChatAll)
	assert.Equal(t, chatMessageEvent.ChatMessage.Text, round.ChatMessages[0].Message)
}
