package modelanalyzer

import (
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

// BombExplodeEvent event when bomb explode
type BombExplodeEvent struct {
	BombExplode events.BombExplode
}

// GetType type of event
func (bee *BombExplodeEvent) GetType() EventType {
	return BombExplodeEvt
}

// HandleEvent return a bomb exploded event, addind score
func (bee *BombExplodeEvent) HandleEvent(game *Game, frame Frame, round *Round) model.Event {
	return &model.BombExplodeEvent{}
}
