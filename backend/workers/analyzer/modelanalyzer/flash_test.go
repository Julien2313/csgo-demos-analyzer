package modelanalyzer_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
)

func TestFlash_ComputeScore(t *testing.T) {
	team1 := &modelanalyzer.Team{NumTeam: 2}
	team2 := &modelanalyzer.Team{NumTeam: 3}

	player1 := modelanalyzer.PlayerFlashed{Flashed: &modelanalyzer.Player{Team: team1}, Duration: time.Second * 2}
	player2 := modelanalyzer.PlayerFlashed{Flashed: &modelanalyzer.Player{Team: team2}, Duration: time.Second * 2}
	player3 := modelanalyzer.PlayerFlashed{Flashed: &modelanalyzer.Player{Team: team2}, Duration: time.Second * 1}
	player4 := modelanalyzer.PlayerFlashed{Flashed: &modelanalyzer.Player{Team: team1}, Duration: time.Second * 4}

	flash := &modelanalyzer.Flash{
		Thrower:        &modelanalyzer.Player{Team: team1},
		PlayersFlashed: []modelanalyzer.PlayerFlashed{player1, player2, player3, player4},
	}

	flash.ComputeScore()
	assert.Equal(t, -3.0, flash.Score)
}
