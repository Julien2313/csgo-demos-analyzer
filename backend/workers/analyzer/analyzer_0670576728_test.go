// +build !race

package analyzer

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gorm.io/gorm/logger"
)

func TestAnalyzer_ParseOneGame(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	demoDAO := dao.NewDemoDAO(localDB, logTest)
	userDAO := dao.NewUserDAO(localDB)
	fastLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			LogLevel: logger.Silent,
		},
	)

	user := model.GenerateRandomUser(true)
	user.SteamID = helpers.StrPtr(strconv.FormatInt(76561198018574562, 10))
	require.NoError(t, userDAO.Create(user))

	demo := model.Demo{
		ID:              helpers.Int64Ptr(3428576404172701964),
		User:            user,
		UserID:          user.ID,
		Name:            helpers.StrPtr("match730_003428582232443322407_0670576728.dem.bz2"),
		Path:            helpers.StrPtr("../../../csgo-stock-demo/match730_003428582232443322407_0670576728.dem.bz2"),
		Size:            helpers.Int64Ptr(0),
		Downloaded:      helpers.BoolPtr(true),
		VersionAnalyzer: helpers.Int64Ptr(0),
		Date:            &time.Time{},
	}
	require.NoError(t, demoDAO.Create(&demo, &fastLogger))
	data := DataAnalyzer{
		demoID: *demo.ID,
	}
	a := Analyzer{db: localDB, log: logTest}
	a.ParseOneGame(data)

	demoFromDB := model.Demo{ID: demo.ID}
	require.NoError(t, demoDAO.GetWithStats(&demoFromDB))
	require.Len(t, demoFromDB.TeamsStats, 2)

	var otherTeam int

	var isPlayerIn bool
	for numTeam, team := range demoFromDB.TeamsStats {
		for _, player := range team.PlayersStats {
			if *player.SteamID == "76561198018574562" {
				isPlayerIn = true
				assert.Equal(t, 16, *team.Score)

				otherTeam = 1 - numTeam
			}
		}
	}

	require.True(t, isPlayerIn)
	assert.Equal(t, 13, *demoFromDB.TeamsStats[otherTeam].Score)
	assert.Len(t, demoFromDB.SteamsIDs, 10)

	for _, team := range demoFromDB.TeamsStats {
		for _, player := range team.PlayersStats {
			assert.Len(t, player.MatrixKills, 10)
			assert.Len(t, player.MatrixFlashs, 10)
		}
	}

	type TestFrame struct {
		PlayersCard map[uint64]model.PlayerCard `json:"playersCard"`
	}

	allFrames := []*TestFrame{
		{
			PlayersCard: map[uint64]model.PlayerCard{
				76561198069969016: {
					IsAlive: true,
				},
				76561199048787213: {
					IsAlive: true,
				},
				76561197999609504: {
					IsAlive: true,
				},
				76561198018574562: {
					IsAlive: true,
				},
				76561198081237162: {
					IsAlive: true,
				},
				76561199050350107: {
					IsAlive: true,
				},
				76561198337653352: {
					IsAlive: true,
				},
				76561198276844793: {
					IsAlive: true,
				},
				76561198416130173: {
					IsAlive: true,
				},
				76561198289556066: {
					IsAlive: true,
				},
			},
		},
		{
			PlayersCard: map[uint64]model.PlayerCard{
				76561198069969016: {
					IsAlive: true,
				},
				76561199048787213: {
					IsAlive: true,
				},
				76561197999609504: {
					IsAlive: true,
				},
				76561198018574562: {
					IsAlive: true,
				},
				76561198081237162: {
					IsAlive: true,
				},
				76561199050350107: {
					IsAlive: true,
				},
				76561198337653352: {
					IsAlive: true,
				},
				76561198276844793: {
					IsAlive: true,
				},
				76561198416130173: {
					IsAlive: true,
				},
				76561198289556066: {
					IsAlive: false,
				},
			},
		},
		{
			PlayersCard: map[uint64]model.PlayerCard{
				76561198069969016: {
					IsAlive: true,
				},
				76561199048787213: {
					IsAlive: true,
				},
				76561197999609504: {
					IsAlive: true,
				},
				76561198018574562: {
					IsAlive: true,
				},
				76561198081237162: {
					IsAlive: true,
				},
				76561199050350107: {
					IsAlive: true,
				},
				76561198337653352: {
					IsAlive: true,
				},
				76561198276844793: {
					IsAlive: false,
				},
				76561198416130173: {
					IsAlive: true,
				},
				76561198289556066: {
					IsAlive: false,
				},
			},
		},
		{
			PlayersCard: map[uint64]model.PlayerCard{
				76561198069969016: {
					IsAlive: false,
				},
				76561199048787213: {
					IsAlive: true,
				},
				76561197999609504: {
					IsAlive: true,
				},
				76561198018574562: {
					IsAlive: true,
				},
				76561198081237162: {
					IsAlive: true,
				},
				76561199050350107: {
					IsAlive: true,
				},
				76561198337653352: {
					IsAlive: true,
				},
				76561198276844793: {
					IsAlive: false,
				},
				76561198416130173: {
					IsAlive: true,
				},
				76561198289556066: {
					IsAlive: false,
				},
			},
		},
		{
			PlayersCard: map[uint64]model.PlayerCard{
				76561198069969016: {
					IsAlive: false,
				},
				76561199048787213: {
					IsAlive: true,
				},
				76561197999609504: {
					IsAlive: true,
				},
				76561198018574562: {
					IsAlive: true,
				},
				76561198081237162: {
					IsAlive: false,
				},
				76561199050350107: {
					IsAlive: true,
				},
				76561198337653352: {
					IsAlive: true,
				},
				76561198276844793: {
					IsAlive: false,
				},
				76561198416130173: {
					IsAlive: true,
				},
				76561198289556066: {
					IsAlive: false,
				},
			},
		},
		{
			PlayersCard: map[uint64]model.PlayerCard{
				76561198069969016: {
					IsAlive: false,
				},
				76561199048787213: {
					IsAlive: false,
				},
				76561197999609504: {
					IsAlive: true,
				},
				76561198018574562: {
					IsAlive: true,
				},
				76561198081237162: {
					IsAlive: false,
				},
				76561199050350107: {
					IsAlive: true,
				},
				76561198337653352: {
					IsAlive: true,
				},
				76561198276844793: {
					IsAlive: false,
				},
				76561198416130173: {
					IsAlive: true,
				},
				76561198289556066: {
					IsAlive: false,
				},
			},
		},
		{
			PlayersCard: map[uint64]model.PlayerCard{
				76561198069969016: {
					IsAlive: false,
				},
				76561199048787213: {
					IsAlive: false,
				},
				76561197999609504: {
					IsAlive: true,
				},
				76561198018574562: {
					IsAlive: true,
				},
				76561198081237162: {
					IsAlive: false,
				},
				76561199050350107: {
					IsAlive: true,
				},
				76561198337653352: {
					IsAlive: true,
				},
				76561198276844793: {
					IsAlive: false,
				},
				76561198416130173: {
					IsAlive: false,
				},
				76561198289556066: {
					IsAlive: false,
				},
			},
		},
		{
			PlayersCard: map[uint64]model.PlayerCard{
				76561198069969016: {
					IsAlive: false,
				},
				76561199048787213: {
					IsAlive: false,
				},
				76561197999609504: {
					IsAlive: false,
				},
				76561198018574562: {
					IsAlive: true,
				},
				76561198081237162: {
					IsAlive: false,
				},
				76561199050350107: {
					IsAlive: true,
				},
				76561198337653352: {
					IsAlive: true,
				},
				76561198276844793: {
					IsAlive: false,
				},
				76561198416130173: {
					IsAlive: false,
				},
				76561198289556066: {
					IsAlive: false,
				},
			},
		},
		{
			PlayersCard: map[uint64]model.PlayerCard{
				76561198069969016: {
					IsAlive: false,
				},
				76561199048787213: {
					IsAlive: false,
				},
				76561197999609504: {
					IsAlive: false,
				},
				76561198018574562: {
					IsAlive: true,
				},
				76561198081237162: {
					IsAlive: false,
				},
				76561199050350107: {
					IsAlive: true,
				},
				76561198337653352: {
					IsAlive: false,
				},
				76561198276844793: {
					IsAlive: false,
				},
				76561198416130173: {
					IsAlive: false,
				},
				76561198289556066: {
					IsAlive: false,
				},
			},
		},
		{
			PlayersCard: map[uint64]model.PlayerCard{
				76561198069969016: {
					IsAlive: false,
				},
				76561199048787213: {
					IsAlive: false,
				},
				76561197999609504: {
					IsAlive: false,
				},
				76561198018574562: {
					IsAlive: true,
				},
				76561198081237162: {
					IsAlive: false,
				},
				76561199050350107: {
					IsAlive: false,
				},
				76561198337653352: {
					IsAlive: false,
				},
				76561198276844793: {
					IsAlive: false,
				},
				76561198416130173: {
					IsAlive: false,
				},
				76561198289556066: {
					IsAlive: false,
				},
			},
		},
	}

	round := demoFromDB.Rounds[len(demoFromDB.Rounds)-1-0]
	sort.Slice(round.Frames, func(i, j int) bool {
		return *round.Frames[i].NumFrame < *round.Frames[j].NumFrame
	})

	require.NotEmpty(t, round.Frames)
	numEvent := 0
	for numFrame, frame := range round.Frames {
		for _, player := range frame.PlayersCard {
			steamID, err := helpers.ToUint64(player.SteamID)
			require.NoError(t, err)
			assert.Equal(t, allFrames[numEvent].PlayersCard[steamID].IsAlive, player.IsAlive,
				fmt.Sprintf("num round %d num event %d numFrame %d playersteamID %s", 0, numEvent, numFrame, player.SteamID))
		}
		events := make([]map[string]interface{}, 100)
		require.NoError(t, json.Unmarshal(frame.EventsByte, &events))
		for _, event := range events {
			if model.EventTypeBDD(event["type"].(float64)) == model.KillEvt {
				numEvent++
			}
		}
	}

	matrixKills := map[string]map[string]model.MatrixKills{
		"76561198289556066": {
			"76561198069969016": {
				KilledSteamID:  helpers.StrPtr("76561198069969016"),
				KilledUsername: helpers.StrPtr("Eya"),
				NbrKills:       helpers.IntPtr(3),
			},
			"76561199048787213": {
				KilledSteamID:  helpers.StrPtr("76561199048787213"),
				KilledUsername: helpers.StrPtr("Yonizo"),
				NbrKills:       helpers.IntPtr(6),
			},
			"76561197999609504": {
				KilledSteamID:  helpers.StrPtr("76561197999609504"),
				KilledUsername: helpers.StrPtr("socio* linux <3"),
				NbrKills:       helpers.IntPtr(1),
			},
			"76561198018574562": {
				KilledSteamID:  helpers.StrPtr("76561198018574562"),
				KilledUsername: helpers.StrPtr("Black Fart"),
				NbrKills:       helpers.IntPtr(4),
			},
			"76561198081237162": {
				KilledSteamID:  helpers.StrPtr("76561198081237162"),
				KilledUsername: helpers.StrPtr("Uaw"),
				NbrKills:       helpers.IntPtr(3),
			},
		},
		"76561198416130173": {
			"76561198069969016": {
				KilledSteamID:  helpers.StrPtr("76561198069969016"),
				KilledUsername: helpers.StrPtr("Eya"),
				NbrKills:       helpers.IntPtr(3),
			},
			"76561199048787213": {
				KilledSteamID:  helpers.StrPtr("76561199048787213"),
				KilledUsername: helpers.StrPtr("Yonizo"),
				NbrKills:       helpers.IntPtr(4),
			},
			"76561197999609504": {
				KilledSteamID:  helpers.StrPtr("76561197999609504"),
				KilledUsername: helpers.StrPtr("socio* linux <3"),
				NbrKills:       helpers.IntPtr(2),
			},
			"76561198018574562": {
				KilledSteamID:  helpers.StrPtr("76561198018574562"),
				KilledUsername: helpers.StrPtr("Black Fart"),
				NbrKills:       helpers.IntPtr(4),
			},
			"76561198081237162": {
				KilledSteamID:  helpers.StrPtr("76561198081237162"),
				KilledUsername: helpers.StrPtr("Uaw"),
				NbrKills:       helpers.IntPtr(1),
			},
		},
		"76561198276844793": {
			"76561198069969016": {
				KilledSteamID:  helpers.StrPtr("76561198069969016"),
				KilledUsername: helpers.StrPtr("Eya"),
				NbrKills:       helpers.IntPtr(3),
			},
			"76561199048787213": {
				KilledSteamID:  helpers.StrPtr("76561199048787213"),
				KilledUsername: helpers.StrPtr("Yonizo"),
				NbrKills:       helpers.IntPtr(1),
			},
			"76561197999609504": {
				KilledSteamID:  helpers.StrPtr("76561197999609504"),
				KilledUsername: helpers.StrPtr("socio* linux <3"),
				NbrKills:       helpers.IntPtr(3),
			},
			"76561198018574562": {
				KilledSteamID:  helpers.StrPtr("76561198018574562"),
				KilledUsername: helpers.StrPtr("Black Fart"),
				NbrKills:       helpers.IntPtr(3),
			},
			"76561198081237162": {
				KilledSteamID:  helpers.StrPtr("76561198081237162"),
				KilledUsername: helpers.StrPtr("Uaw"),
				NbrKills:       helpers.IntPtr(3),
			},
		},
		"76561198337653352": {
			"76561198069969016": {
				KilledSteamID:  helpers.StrPtr("76561198069969016"),
				KilledUsername: helpers.StrPtr("Eya"),
				NbrKills:       helpers.IntPtr(4),
			},
			"76561199048787213": {
				KilledSteamID:  helpers.StrPtr("76561199048787213"),
				KilledUsername: helpers.StrPtr("Yonizo"),
				NbrKills:       helpers.IntPtr(3),
			},
			"76561197999609504": {
				KilledSteamID:  helpers.StrPtr("76561197999609504"),
				KilledUsername: helpers.StrPtr("socio* linux <3"),
				NbrKills:       helpers.IntPtr(5),
			},
			"76561198018574562": {
				KilledSteamID:  helpers.StrPtr("76561198018574562"),
				KilledUsername: helpers.StrPtr("Black Fart"),
				NbrKills:       helpers.IntPtr(0),
			},
			"76561198081237162": {
				KilledSteamID:  helpers.StrPtr("76561198081237162"),
				KilledUsername: helpers.StrPtr("Uaw"),
				NbrKills:       helpers.IntPtr(7),
			},
		},
		"76561199050350107": {
			"76561198069969016": {
				KilledSteamID:  helpers.StrPtr("76561198069969016"),
				KilledUsername: helpers.StrPtr("Eya"),
				NbrKills:       helpers.IntPtr(5),
			},
			"76561199048787213": {
				KilledSteamID:  helpers.StrPtr("76561199048787213"),
				KilledUsername: helpers.StrPtr("Yonizo"),
				NbrKills:       helpers.IntPtr(6),
			},
			"76561197999609504": {
				KilledSteamID:  helpers.StrPtr("76561197999609504"),
				KilledUsername: helpers.StrPtr("socio* linux <3"),
				NbrKills:       helpers.IntPtr(11),
			},
			"76561198018574562": {
				KilledSteamID:  helpers.StrPtr("76561198018574562"),
				KilledUsername: helpers.StrPtr("Black Fart"),
				NbrKills:       helpers.IntPtr(8),
			},
			"76561198081237162": {
				KilledSteamID:  helpers.StrPtr("76561198081237162"),
				KilledUsername: helpers.StrPtr("Uaw"),
				NbrKills:       helpers.IntPtr(7),
			},
		},
		//------------------------------------------------------------------
		"76561198069969016": {
			"76561199050350107": {
				KilledSteamID:  helpers.StrPtr("76561199050350107"),
				KilledUsername: helpers.StrPtr("Tetrahydrokannabinol"),
				NbrKills:       helpers.IntPtr(6),
			},
			"76561198337653352": {
				KilledSteamID:  helpers.StrPtr("76561198337653352"),
				KilledUsername: helpers.StrPtr("w0lf ❤"),
				NbrKills:       helpers.IntPtr(3),
			},
			"76561198276844793": {
				KilledSteamID:  helpers.StrPtr("76561198276844793"),
				KilledUsername: helpers.StrPtr("matish"),
				NbrKills:       helpers.IntPtr(6),
			},
			"76561198416130173": {
				KilledSteamID:  helpers.StrPtr("76561198416130173"),
				KilledUsername: helpers.StrPtr("kickflip"),
				NbrKills:       helpers.IntPtr(3),
			},
			"76561198289556066": {
				KilledSteamID:  helpers.StrPtr("76561198289556066"),
				KilledUsername: helpers.StrPtr("Jaqeee"),
				NbrKills:       helpers.IntPtr(3),
			},
		},
		"76561199048787213": {
			"76561199050350107": {
				KilledSteamID:  helpers.StrPtr("76561199050350107"),
				KilledUsername: helpers.StrPtr("Tetrahydrokannabinol"),
				NbrKills:       helpers.IntPtr(4),
			},
			"76561198337653352": {
				KilledSteamID:  helpers.StrPtr("76561198337653352"),
				KilledUsername: helpers.StrPtr("w0lf ❤"),
				NbrKills:       helpers.IntPtr(0),
			},
			"76561198276844793": {
				KilledSteamID:  helpers.StrPtr("76561198276844793"),
				KilledUsername: helpers.StrPtr("matish"),
				NbrKills:       helpers.IntPtr(0),
			},
			"76561198416130173": {
				KilledSteamID:  helpers.StrPtr("76561198416130173"),
				KilledUsername: helpers.StrPtr("kickflip"),
				NbrKills:       helpers.IntPtr(1),
			},
			"76561198289556066": {
				KilledSteamID:  helpers.StrPtr("76561198289556066"),
				KilledUsername: helpers.StrPtr("Jaqeee"),
				NbrKills:       helpers.IntPtr(3),
			},
		},
		"76561197999609504": {
			"76561199050350107": {
				KilledSteamID:  helpers.StrPtr("76561199050350107"),
				KilledUsername: helpers.StrPtr("Tetrahydrokannabinol"),
				NbrKills:       helpers.IntPtr(2),
			},
			"76561198337653352": {
				KilledSteamID:  helpers.StrPtr("76561198337653352"),
				KilledUsername: helpers.StrPtr("w0lf ❤"),
				NbrKills:       helpers.IntPtr(8),
			},
			"76561198276844793": {
				KilledSteamID:  helpers.StrPtr("76561198276844793"),
				KilledUsername: helpers.StrPtr("matish"),
				NbrKills:       helpers.IntPtr(7),
			},
			"76561198416130173": {
				KilledSteamID:  helpers.StrPtr("76561198416130173"),
				KilledUsername: helpers.StrPtr("kickflip"),
				NbrKills:       helpers.IntPtr(6),
			},
			"76561198289556066": {
				KilledSteamID:  helpers.StrPtr("76561198289556066"),
				KilledUsername: helpers.StrPtr("Jaqeee"),
				NbrKills:       helpers.IntPtr(9),
			},
		},
		"76561198018574562": {
			"76561199050350107": {
				KilledSteamID:  helpers.StrPtr("76561199050350107"),
				KilledUsername: helpers.StrPtr("Tetrahydrokannabinol"),
				NbrKills:       helpers.IntPtr(4),
			},
			"76561198337653352": {
				KilledSteamID:  helpers.StrPtr("76561198337653352"),
				KilledUsername: helpers.StrPtr("w0lf ❤"),
				NbrKills:       helpers.IntPtr(3),
			},
			"76561198276844793": {
				KilledSteamID:  helpers.StrPtr("76561198276844793"),
				KilledUsername: helpers.StrPtr("matish"),
				NbrKills:       helpers.IntPtr(5),
			},
			"76561198416130173": {
				KilledSteamID:  helpers.StrPtr("76561198416130173"),
				KilledUsername: helpers.StrPtr("kickflip"),
				NbrKills:       helpers.IntPtr(6),
			},
			"76561198289556066": {
				KilledSteamID:  helpers.StrPtr("76561198289556066"),
				KilledUsername: helpers.StrPtr("Jaqeee"),
				NbrKills:       helpers.IntPtr(3),
			},
		},
		"76561198081237162": {
			"76561199050350107": {
				KilledSteamID:  helpers.StrPtr("76561199050350107"),
				KilledUsername: helpers.StrPtr("Tetrahydrokannabinol"),
				NbrKills:       helpers.IntPtr(2),
			},
			"76561198337653352": {
				KilledSteamID:  helpers.StrPtr("76561198337653352"),
				KilledUsername: helpers.StrPtr("w0lf ❤"),
				NbrKills:       helpers.IntPtr(7),
			},
			"76561198276844793": {
				KilledSteamID:  helpers.StrPtr("76561198276844793"),
				KilledUsername: helpers.StrPtr("matish"),
				NbrKills:       helpers.IntPtr(4),
			},
			"76561198416130173": {
				KilledSteamID:  helpers.StrPtr("76561198416130173"),
				KilledUsername: helpers.StrPtr("kickflip"),
				NbrKills:       helpers.IntPtr(3),
			},
			"76561198289556066": {
				KilledSteamID:  helpers.StrPtr("76561198289556066"),
				KilledUsername: helpers.StrPtr("Jaqeee"),
				NbrKills:       helpers.IntPtr(4),
			},
		},
	}

	for _, teamResp := range demoFromDB.TeamsStats {
		for _, playerResp := range teamResp.PlayersStats {
			for _, matrixKillsResp := range playerResp.MatrixKills {
				if matrixKills[*playerResp.SteamID] == nil {
					continue
				}
				matrixKillsExp := matrixKills[*playerResp.SteamID][*matrixKillsResp.KilledSteamID]
				if _, exists := matrixKills[*playerResp.SteamID][*matrixKillsResp.KilledSteamID]; !exists {
					continue
				}
				assert.Equal(t, *matrixKillsExp.KilledSteamID, *matrixKillsResp.KilledSteamID)
				assert.Equal(t, *matrixKillsExp.KilledUsername, *matrixKillsResp.KilledUsername)
				assert.Equal(t, *matrixKillsExp.NbrKills, *matrixKillsResp.NbrKills)
			}
		}
	}

	MatrixFlashs := map[string]map[string]model.MatrixFlashs{
		"76561198289556066": {
			"76561198069969016": {
				FlashedSteamID:  helpers.StrPtr("76561198069969016"),
				FlashedUsername: helpers.StrPtr("Eya"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561199048787213": {
				FlashedSteamID:  helpers.StrPtr("76561199048787213"),
				FlashedUsername: helpers.StrPtr("Yonizo"),
				NbrSecFlashs:    helpers.FloatPtr(1.987765504),
			},
			"76561197999609504": {
				FlashedSteamID:  helpers.StrPtr("76561197999609504"),
				FlashedUsername: helpers.StrPtr("socio* linux <3"),
				NbrSecFlashs:    helpers.FloatPtr(3.15798272),
			},
			"76561198018574562": {
				FlashedSteamID:  helpers.StrPtr("76561198018574562"),
				FlashedUsername: helpers.StrPtr("Black Fart"),
				NbrSecFlashs:    helpers.FloatPtr(2.91013504),
			},
			"76561198081237162": {
				FlashedSteamID:  helpers.StrPtr("76561198081237162"),
				FlashedUsername: helpers.StrPtr("Uaw"),
				NbrSecFlashs:    helpers.FloatPtr(2.754450688),
			},
			"76561199050350107": {
				FlashedSteamID:  helpers.StrPtr("76561199050350107"),
				FlashedUsername: helpers.StrPtr("Tetrahydrokannabinol"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198337653352": {
				FlashedSteamID:  helpers.StrPtr("76561198337653352"),
				FlashedUsername: helpers.StrPtr("w0lf ❤"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198276844793": {
				FlashedSteamID:  helpers.StrPtr("76561198276844793"),
				FlashedUsername: helpers.StrPtr("matish"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198416130173": {
				FlashedSteamID:  helpers.StrPtr("76561198416130173"),
				FlashedUsername: helpers.StrPtr("kickflip"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198289556066": {
				FlashedSteamID:  helpers.StrPtr("76561198289556066"),
				FlashedUsername: helpers.StrPtr("Jaqeee"),
				NbrSecFlashs:    helpers.FloatPtr(0.9369792),
			},
		},
		"76561198416130173": {
			"76561198069969016": {
				FlashedSteamID:  helpers.StrPtr("76561198069969016"),
				FlashedUsername: helpers.StrPtr("Eya"),
				NbrSecFlashs:    helpers.FloatPtr(2.873506816),
			},
			"76561199048787213": {
				FlashedSteamID:  helpers.StrPtr("76561199048787213"),
				FlashedUsername: helpers.StrPtr("Yonizo"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561197999609504": {
				FlashedSteamID:  helpers.StrPtr("76561197999609504"),
				FlashedUsername: helpers.StrPtr("socio* linux <3"),
				NbrSecFlashs:    helpers.FloatPtr(11.023789568),
			},
			"76561198018574562": {
				FlashedSteamID:  helpers.StrPtr("76561198018574562"),
				FlashedUsername: helpers.StrPtr("Black Fart"),
				NbrSecFlashs:    helpers.FloatPtr(2.110456576),
			},
			"76561198081237162": {
				FlashedSteamID:  helpers.StrPtr("76561198081237162"),
				FlashedUsername: helpers.StrPtr("Uaw"),
				NbrSecFlashs:    helpers.FloatPtr(2.563034624),
			},
			"76561199050350107": {
				FlashedSteamID:  helpers.StrPtr("76561199050350107"),
				FlashedUsername: helpers.StrPtr("Tetrahydrokannabinol"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198337653352": {
				FlashedSteamID:  helpers.StrPtr("76561198337653352"),
				FlashedUsername: helpers.StrPtr("w0lf ❤"),
				NbrSecFlashs:    helpers.FloatPtr(4.664078976),
			},
			"76561198276844793": {
				FlashedSteamID:  helpers.StrPtr("76561198276844793"),
				FlashedUsername: helpers.StrPtr("matish"),
				NbrSecFlashs:    helpers.FloatPtr(4.4847802240000005),
			},
			"76561198416130173": {
				FlashedSteamID:  helpers.StrPtr("76561198416130173"),
				FlashedUsername: helpers.StrPtr("kickflip"),
				NbrSecFlashs:    helpers.FloatPtr(2.795672064),
			},
			"76561198289556066": {
				FlashedSteamID:  helpers.StrPtr("76561198289556066"),
				FlashedUsername: helpers.StrPtr("Jaqeee"),
				NbrSecFlashs:    helpers.FloatPtr(0.927314944),
			},
		},
		"76561198276844793": {
			"76561198069969016": {
				FlashedSteamID:  helpers.StrPtr("76561198069969016"),
				FlashedUsername: helpers.StrPtr("Eya"),
				NbrSecFlashs:    helpers.FloatPtr(3.45590272),
			},
			"76561199048787213": {
				FlashedSteamID:  helpers.StrPtr("76561199048787213"),
				FlashedUsername: helpers.StrPtr("Yonizo"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561197999609504": {
				FlashedSteamID:  helpers.StrPtr("76561197999609504"),
				FlashedUsername: helpers.StrPtr("socio* linux <3"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198018574562": {
				FlashedSteamID:  helpers.StrPtr("76561198018574562"),
				FlashedUsername: helpers.StrPtr("Black Fart"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198081237162": {
				FlashedSteamID:  helpers.StrPtr("76561198081237162"),
				FlashedUsername: helpers.StrPtr("Uaw"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561199050350107": {
				FlashedSteamID:  helpers.StrPtr("76561199050350107"),
				FlashedUsername: helpers.StrPtr("Tetrahydrokannabinol"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198337653352": {
				FlashedSteamID:  helpers.StrPtr("76561198337653352"),
				FlashedUsername: helpers.StrPtr("w0lf ❤"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198276844793": {
				FlashedSteamID:  helpers.StrPtr("76561198276844793"),
				FlashedUsername: helpers.StrPtr("matish"),
				NbrSecFlashs:    helpers.FloatPtr(3.438342144),
			},
			"76561198416130173": {
				FlashedSteamID:  helpers.StrPtr("76561198416130173"),
				FlashedUsername: helpers.StrPtr("kickflip"),
				NbrSecFlashs:    helpers.FloatPtr(0.562069504),
			},
			"76561198289556066": {
				FlashedSteamID:  helpers.StrPtr("76561198289556066"),
				FlashedUsername: helpers.StrPtr("Jaqeee"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
		},
		"76561198337653352": {
			"76561198069969016": {
				FlashedSteamID:  helpers.StrPtr("76561198069969016"),
				FlashedUsername: helpers.StrPtr("Eya"),
				NbrSecFlashs:    helpers.FloatPtr(3.40734464),
			},
			"76561199048787213": {
				FlashedSteamID:  helpers.StrPtr("76561199048787213"),
				FlashedUsername: helpers.StrPtr("Yonizo"),
				NbrSecFlashs:    helpers.FloatPtr(4.544702208),
			},
			"76561197999609504": {
				FlashedSteamID:  helpers.StrPtr("76561197999609504"),
				FlashedUsername: helpers.StrPtr("socio* linux <3"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198018574562": {
				FlashedSteamID:  helpers.StrPtr("76561198018574562"),
				FlashedUsername: helpers.StrPtr("Black Fart"),
				NbrSecFlashs:    helpers.FloatPtr(6.005109248),
			},
			"76561198081237162": {
				FlashedSteamID:  helpers.StrPtr("76561198081237162"),
				FlashedUsername: helpers.StrPtr("Uaw"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561199050350107": {
				FlashedSteamID:  helpers.StrPtr("76561199050350107"),
				FlashedUsername: helpers.StrPtr("Tetrahydrokannabinol"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198337653352": {
				FlashedSteamID:  helpers.StrPtr("76561198337653352"),
				FlashedUsername: helpers.StrPtr("w0lf ❤"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198276844793": {
				FlashedSteamID:  helpers.StrPtr("76561198276844793"),
				FlashedUsername: helpers.StrPtr("matish"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198416130173": {
				FlashedSteamID:  helpers.StrPtr("76561198416130173"),
				FlashedUsername: helpers.StrPtr("kickflip"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198289556066": {
				FlashedSteamID:  helpers.StrPtr("76561198289556066"),
				FlashedUsername: helpers.StrPtr("Jaqeee"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
		},
		"76561199050350107": {
			"76561198069969016": {
				FlashedSteamID:  helpers.StrPtr("76561198069969016"),
				FlashedUsername: helpers.StrPtr("Eya"),
				NbrSecFlashs:    helpers.FloatPtr(0.200141056),
			},
			"76561199048787213": {
				FlashedSteamID:  helpers.StrPtr("76561199048787213"),
				FlashedUsername: helpers.StrPtr("Yonizo"),
				NbrSecFlashs:    helpers.FloatPtr(3.301697024),
			},
			"76561197999609504": {
				FlashedSteamID:  helpers.StrPtr("76561197999609504"),
				FlashedUsername: helpers.StrPtr("socio* linux <3"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198018574562": {
				FlashedSteamID:  helpers.StrPtr("76561198018574562"),
				FlashedUsername: helpers.StrPtr("Black Fart"),
				NbrSecFlashs:    helpers.FloatPtr(0.952998016),
			},
			"76561198081237162": {
				FlashedSteamID:  helpers.StrPtr("76561198081237162"),
				FlashedUsername: helpers.StrPtr("Uaw"),
				NbrSecFlashs:    helpers.FloatPtr(0.952532864),
			},
			"76561199050350107": {
				FlashedSteamID:  helpers.StrPtr("76561199050350107"),
				FlashedUsername: helpers.StrPtr("Tetrahydrokannabinol"),
				NbrSecFlashs:    helpers.FloatPtr(4.01882368),
			},
			"76561198337653352": {
				FlashedSteamID:  helpers.StrPtr("76561198337653352"),
				FlashedUsername: helpers.StrPtr("w0lf ❤"),
				NbrSecFlashs:    helpers.FloatPtr(4.01882368),
			},
			"76561198276844793": {
				FlashedSteamID:  helpers.StrPtr("76561198276844793"),
				FlashedUsername: helpers.StrPtr("matish"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198416130173": {
				FlashedSteamID:  helpers.StrPtr("76561198416130173"),
				FlashedUsername: helpers.StrPtr("kickflip"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198289556066": {
				FlashedSteamID:  helpers.StrPtr("76561198289556066"),
				FlashedUsername: helpers.StrPtr("Jaqeee"),
				NbrSecFlashs:    helpers.FloatPtr(6.587011584),
			},
		},
		//------------------------------------------------------------------
		"76561198069969016": {
			"76561198069969016": {
				FlashedSteamID:  helpers.StrPtr("76561198069969016"),
				FlashedUsername: helpers.StrPtr("Eya"),
				NbrSecFlashs:    helpers.FloatPtr(2.184670336),
			},
			"76561199048787213": {
				FlashedSteamID:  helpers.StrPtr("76561199048787213"),
				FlashedUsername: helpers.StrPtr("Yonizo"),
				NbrSecFlashs:    helpers.FloatPtr(1.08863872),
			},
			"76561197999609504": {
				FlashedSteamID:  helpers.StrPtr("76561197999609504"),
				FlashedUsername: helpers.StrPtr("socio* linux <3"),
				NbrSecFlashs:    helpers.FloatPtr(1.089645184),
			},
			"76561198018574562": {
				FlashedSteamID:  helpers.StrPtr("76561198018574562"),
				FlashedUsername: helpers.StrPtr("Black Fart"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198081237162": {
				FlashedSteamID:  helpers.StrPtr("76561198081237162"),
				FlashedUsername: helpers.StrPtr("Uaw"),
				NbrSecFlashs:    helpers.FloatPtr(1.08863872),
			},
			"76561199050350107": {
				FlashedSteamID:  helpers.StrPtr("76561199050350107"),
				FlashedUsername: helpers.StrPtr("Tetrahydrokannabinol"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198337653352": {
				FlashedSteamID:  helpers.StrPtr("76561198337653352"),
				FlashedUsername: helpers.StrPtr("w0lf ❤"),
				NbrSecFlashs:    helpers.FloatPtr(2.202040832),
			},
			"76561198276844793": {
				FlashedSteamID:  helpers.StrPtr("76561198276844793"),
				FlashedUsername: helpers.StrPtr("matish"),
				NbrSecFlashs:    helpers.FloatPtr(2.554068224),
			},
			"76561198416130173": {
				FlashedSteamID:  helpers.StrPtr("76561198416130173"),
				FlashedUsername: helpers.StrPtr("kickflip"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198289556066": {
				FlashedSteamID:  helpers.StrPtr("76561198289556066"),
				FlashedUsername: helpers.StrPtr("Jaqeee"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
		},
		"76561199048787213": {
			"76561198069969016": {
				FlashedSteamID:  helpers.StrPtr("76561198069969016"),
				FlashedUsername: helpers.StrPtr("Eya"),
				NbrSecFlashs:    helpers.FloatPtr(0.025878528),
			},
			"76561199048787213": {
				FlashedSteamID:  helpers.StrPtr("76561199048787213"),
				FlashedUsername: helpers.StrPtr("Yonizo"),
				NbrSecFlashs:    helpers.FloatPtr(0.024704832),
			},
			"76561197999609504": {
				FlashedSteamID:  helpers.StrPtr("76561197999609504"),
				FlashedUsername: helpers.StrPtr("socio* linux <3"),
				NbrSecFlashs:    helpers.FloatPtr(2.588530688),
			},
			"76561198018574562": {
				FlashedSteamID:  helpers.StrPtr("76561198018574562"),
				FlashedUsername: helpers.StrPtr("Black Fart"),
				NbrSecFlashs:    helpers.FloatPtr(0.025294528),
			},
			"76561198081237162": {
				FlashedSteamID:  helpers.StrPtr("76561198081237162"),
				FlashedUsername: helpers.StrPtr("Uaw"),
				NbrSecFlashs:    helpers.FloatPtr(0.025294528),
			},
			"76561199050350107": {
				FlashedSteamID:  helpers.StrPtr("76561199050350107"),
				FlashedUsername: helpers.StrPtr("Tetrahydrokannabinol"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198337653352": {
				FlashedSteamID:  helpers.StrPtr("76561198337653352"),
				FlashedUsername: helpers.StrPtr("w0lf ❤"),
				NbrSecFlashs:    helpers.FloatPtr(4.126647296),
			},
			"76561198276844793": {
				FlashedSteamID:  helpers.StrPtr("76561198276844793"),
				FlashedUsername: helpers.StrPtr("matish"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198416130173": {
				FlashedSteamID:  helpers.StrPtr("76561198416130173"),
				FlashedUsername: helpers.StrPtr("kickflip"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198289556066": {
				FlashedSteamID:  helpers.StrPtr("76561198289556066"),
				FlashedUsername: helpers.StrPtr("Jaqeee"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
		},
		"76561197999609504": {
			"76561198069969016": {
				FlashedSteamID:  helpers.StrPtr("76561198069969016"),
				FlashedUsername: helpers.StrPtr("Eya"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561199048787213": {
				FlashedSteamID:  helpers.StrPtr("76561199048787213"),
				FlashedUsername: helpers.StrPtr("Yonizo"),
				NbrSecFlashs:    helpers.FloatPtr(1.615959552),
			},
			"76561197999609504": {
				FlashedSteamID:  helpers.StrPtr("76561197999609504"),
				FlashedUsername: helpers.StrPtr("socio* linux <3"),
				NbrSecFlashs:    helpers.FloatPtr(0.011231296),
			},
			"76561198018574562": {
				FlashedSteamID:  helpers.StrPtr("76561198018574562"),
				FlashedUsername: helpers.StrPtr("Black Fart"),
				NbrSecFlashs:    helpers.FloatPtr(4.003380864),
			},
			"76561198081237162": {
				FlashedSteamID:  helpers.StrPtr("76561198081237162"),
				FlashedUsername: helpers.StrPtr("Uaw"),
				NbrSecFlashs:    helpers.FloatPtr(1.262919936),
			},
			"76561199050350107": {
				FlashedSteamID:  helpers.StrPtr("76561199050350107"),
				FlashedUsername: helpers.StrPtr("Tetrahydrokannabinol"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198337653352": {
				FlashedSteamID:  helpers.StrPtr("76561198337653352"),
				FlashedUsername: helpers.StrPtr("w0lf ❤"),
				NbrSecFlashs:    helpers.FloatPtr(3.45060352),
			},
			"76561198276844793": {
				FlashedSteamID:  helpers.StrPtr("76561198276844793"),
				FlashedUsername: helpers.StrPtr("matish"),
				NbrSecFlashs:    helpers.FloatPtr(6.452546048),
			},
			"76561198416130173": {
				FlashedSteamID:  helpers.StrPtr("76561198416130173"),
				FlashedUsername: helpers.StrPtr("kickflip"),
				NbrSecFlashs:    helpers.FloatPtr(6.713260288000001),
			},
			"76561198289556066": {
				FlashedSteamID:  helpers.StrPtr("76561198289556066"),
				FlashedUsername: helpers.StrPtr("Jaqeee"),
				NbrSecFlashs:    helpers.FloatPtr(3.488606208),
			},
		},
		"76561198018574562": {
			"76561198069969016": {
				FlashedSteamID:  helpers.StrPtr("76561198069969016"),
				FlashedUsername: helpers.StrPtr("Eya"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561199048787213": {
				FlashedSteamID:  helpers.StrPtr("76561199048787213"),
				FlashedUsername: helpers.StrPtr("Yonizo"),
				NbrSecFlashs:    helpers.FloatPtr(4.975454976),
			},
			"76561197999609504": {
				FlashedSteamID:  helpers.StrPtr("76561197999609504"),
				FlashedUsername: helpers.StrPtr("socio* linux <3"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198018574562": {
				FlashedSteamID:  helpers.StrPtr("76561198018574562"),
				FlashedUsername: helpers.StrPtr("Black Fart"),
				NbrSecFlashs:    helpers.FloatPtr(0.580049408),
			},
			"76561198081237162": {
				FlashedSteamID:  helpers.StrPtr("76561198081237162"),
				FlashedUsername: helpers.StrPtr("Uaw"),
				NbrSecFlashs:    helpers.FloatPtr(5.3202444799999995),
			},
			"76561199050350107": {
				FlashedSteamID:  helpers.StrPtr("76561199050350107"),
				FlashedUsername: helpers.StrPtr("Tetrahydrokannabinol"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198337653352": {
				FlashedSteamID:  helpers.StrPtr("76561198337653352"),
				FlashedUsername: helpers.StrPtr("w0lf ❤"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198276844793": {
				FlashedSteamID:  helpers.StrPtr("76561198276844793"),
				FlashedUsername: helpers.StrPtr("matish"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198416130173": {
				FlashedSteamID:  helpers.StrPtr("76561198416130173"),
				FlashedUsername: helpers.StrPtr("kickflip"),
				NbrSecFlashs:    helpers.FloatPtr(4.649216512),
			},
			"76561198289556066": {
				FlashedSteamID:  helpers.StrPtr("76561198289556066"),
				FlashedUsername: helpers.StrPtr("Jaqeee"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
		},
		"76561198081237162": {
			"76561198069969016": {
				FlashedSteamID:  helpers.StrPtr("76561198069969016"),
				FlashedUsername: helpers.StrPtr("Eya"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561199048787213": {
				FlashedSteamID:  helpers.StrPtr("76561199048787213"),
				FlashedUsername: helpers.StrPtr("Yonizo"),
				NbrSecFlashs:    helpers.FloatPtr(0.),
			},
			"76561197999609504": {
				FlashedSteamID:  helpers.StrPtr("76561197999609504"),
				FlashedUsername: helpers.StrPtr("socio* linux <3"),
				NbrSecFlashs:    helpers.FloatPtr(1.013463552),
			},
			"76561198018574562": {
				FlashedSteamID:  helpers.StrPtr("76561198018574562"),
				FlashedUsername: helpers.StrPtr("Black Fart"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198081237162": {
				FlashedSteamID:  helpers.StrPtr("76561198081237162"),
				FlashedUsername: helpers.StrPtr("Uaw"),
				NbrSecFlashs:    helpers.FloatPtr(4.16916992),
			},
			"76561199050350107": {
				FlashedSteamID:  helpers.StrPtr("76561199050350107"),
				FlashedUsername: helpers.StrPtr("Tetrahydrokannabinol"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198337653352": {
				FlashedSteamID:  helpers.StrPtr("76561198337653352"),
				FlashedUsername: helpers.StrPtr("w0lf ❤"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198276844793": {
				FlashedSteamID:  helpers.StrPtr("76561198276844793"),
				FlashedUsername: helpers.StrPtr("matish"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
			"76561198416130173": {
				FlashedSteamID:  helpers.StrPtr("76561198416130173"),
				FlashedUsername: helpers.StrPtr("kickflip"),
				NbrSecFlashs:    helpers.FloatPtr(0.792644608),
			},
			"76561198289556066": {
				FlashedSteamID:  helpers.StrPtr("76561198289556066"),
				FlashedUsername: helpers.StrPtr("Jaqeee"),
				NbrSecFlashs:    helpers.FloatPtr(0),
			},
		},
	}

	for _, teamResp := range demoFromDB.TeamsStats {
		for _, playerResp := range teamResp.PlayersStats {
			for _, matrixFlashsResp := range playerResp.MatrixFlashs {
				matrixFlashsExp := MatrixFlashs[*playerResp.SteamID][*matrixFlashsResp.FlashedSteamID]
				assert.Equal(t, *matrixFlashsExp.FlashedSteamID, *matrixFlashsResp.FlashedSteamID)
				assert.Equal(t, *matrixFlashsExp.FlashedUsername, *matrixFlashsResp.FlashedUsername)
				assert.Equal(t, *matrixFlashsExp.NbrSecFlashs, *matrixFlashsResp.NbrSecFlashs,
					*playerResp.SteamID, *matrixFlashsExp.FlashedSteamID)
			}
		}
	}

	scoreflash := map[string]float64{
		"76561198289556066": 29.62006,
		"76561198416130173": 17.09682,
		"76561198276844793": -1.63353,
		"76561198337653352": 41.87146,
		"76561199050350107": -27.65187,
		"76561198069969016": -2.08646,
		"76561199048787213": 4.31083,
		"76561197999609504": 39.63457,
		"76561198018574562": -18.6796,
		"76561198081237162": -13.16997,
	}

	for _, teamResp := range demoFromDB.TeamsStats {
		for _, playerResp := range teamResp.PlayersStats {
			assert.Equal(t, scoreflash[*playerResp.SteamID], helpers.Floor(*playerResp.ScoreFacts.FlashScore, 5))
		}
	}

	scoreFlashPerRound := []float64{
		0,
		-16.78,
		0,
		0,
		0,
		30.66,
		-4.81,
		0,
		0,
		0,
		17.71,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		1.59,
		0,
		-8.58,
		-2.73,
	}

	for _, round := range demoFromDB.Rounds {
		for _, score := range round.ScoreFactsPerPlayer {
			if *score.SteamID != "76561198416130173" {
				continue
			}

			assert.Equal(t, scoreFlashPerRound[*round.NumRound], helpers.Floor(*score.FlashScore, 2))
		}
	}

	totalScore := map[string]float64{
		"76561198289556066": 3153.5,
		"76561198416130173": 2220.22,
		"76561198276844793": 1849.64,
		"76561198337653352": 3526.41,
		"76561199050350107": 4516.75,
		"76561198069969016": 3952.86,
		"76561199048787213": 1011.13,
		"76561197999609504": 4346.83,
		"76561198018574562": 3013.97,
		"76561198081237162": 2826.56,
	}

	ratioScore := map[string]float64{
		"76561198289556066": 1.03672,
		"76561198416130173": 0.7299,
		"76561198276844793": 0.60807,
		"76561198337653352": 1.15932,
		"76561199050350107": 1.4849,
		"76561198069969016": 1.29951,
		"76561199048787213": 0.33241,
		"76561197999609504": 1.42903,
		"76561198018574562": 0.99085,
		"76561198081237162": 0.92924,
	}

	for _, teamResp := range demoFromDB.TeamsStats {
		for _, playerResp := range teamResp.PlayersStats {
			assert.Equal(t, totalScore[*playerResp.SteamID],
				helpers.Floor(*playerResp.ScoreFacts.TotalScore, 2),
				*playerResp.SteamID)
			assert.Equal(t, ratioScore[*playerResp.SteamID],
				helpers.Floor(*playerResp.ScoreFacts.RatioScore, 5),
				*playerResp.SteamID)
		}
	}
	MVPs := map[string]int{
		"76561198289556066": 1,
		"76561198416130173": 1,
		"76561198276844793": 0,
		"76561198337653352": 6,
		"76561199050350107": 5,
		"76561198069969016": 4,
		"76561199048787213": 0,
		"76561197999609504": 5,
		"76561198018574562": 2,
		"76561198081237162": 5,
	}

	for _, teamResp := range demoFromDB.TeamsStats {
		for _, playerResp := range teamResp.PlayersStats {
			assert.Equal(t, MVPs[*playerResp.SteamID], *playerResp.MVPsFacts, *playerResp.SteamID)
		}
	}

	kills := make(map[string]int)

	for _, round := range demoFromDB.Rounds {
		for _, frame := range round.Frames {
			events := make([]map[string]interface{}, 100)
			require.NoError(t, json.Unmarshal(frame.EventsByte, &events))
			for _, event := range events {
				if model.EventTypeBDD(event["type"].(float64)) == model.KillEvt {
					killer := event["killer"].(string)
					kills[killer]++
				}
			}
		}
	}

	assert.Equal(t, 21, kills["Black Fart"])
	assert.Equal(t, 21, kills["Eya"])
	assert.Equal(t, 37, kills["Tetrahydrokannabinol"])
	assert.Equal(t, 21, kills["Uaw"])
	assert.Equal(t, 8, kills["Yonizo"])
	assert.Equal(t, 16, kills["kickflip"])
	assert.Equal(t, 13, kills["matish"])
	assert.Equal(t, 33, kills["socio* linux <3"])
	assert.Equal(t, 19, kills["w0lf ❤"])

	marks := map[string]model.Marks{
		"76561198289556066": {
			HS:                 helpers.FloatPtr(0.14772727272727273),
			UtilityDamage:      helpers.IntPtr(0),
			Accuracy:           helpers.FloatPtr(0.31654676258992803),
			Damage:             helpers.IntPtr(2301),
			GrenadesValueDeath: helpers.FloatPtr(700),
		},
		"76561198416130173": {
			HS:                 helpers.FloatPtr(0.21052631578947367),
			UtilityDamage:      helpers.IntPtr(234),
			Accuracy:           helpers.FloatPtr(0.17351598173515982),
			Damage:             helpers.IntPtr(1856),
			GrenadesValueDeath: helpers.FloatPtr(397.36842105263156),
		},
		"76561198276844793": {
			HS:                 helpers.FloatPtr(0.05357142857142857),
			UtilityDamage:      helpers.IntPtr(33),
			Accuracy:           helpers.FloatPtr(0.28865979381443296),
			Damage:             helpers.IntPtr(1556),
			GrenadesValueDeath: helpers.FloatPtr(481.8181818181818),
		},
		"76561198337653352": {
			HS:                 helpers.FloatPtr(0.16666666666666666),
			UtilityDamage:      helpers.IntPtr(22),
			Accuracy:           helpers.FloatPtr(0.27906976744186046),
			Damage:             helpers.IntPtr(2094),
			GrenadesValueDeath: helpers.FloatPtr(339.1304347826087),
		},
		"76561199050350107": {
			HS:                 helpers.FloatPtr(0.22),
			UtilityDamage:      helpers.IntPtr(26),
			Accuracy:           helpers.FloatPtr(0.28169014084507044),
			Damage:             helpers.IntPtr(3484),
			GrenadesValueDeath: helpers.FloatPtr(557.8947368421053),
		},
		"76561198069969016": {
			HS:                 helpers.FloatPtr(0.10843373493975904),
			UtilityDamage:      helpers.IntPtr(194),
			Accuracy:           helpers.FloatPtr(0.27483443708609273),
			Damage:             helpers.IntPtr(2582),
			GrenadesValueDeath: helpers.FloatPtr(408.3333333333333),
		},
		"76561199048787213": {
			HS:                 helpers.FloatPtr(0.13793103448275862),
			UtilityDamage:      helpers.IntPtr(21),
			Accuracy:           helpers.FloatPtr(0.14356435643564355),
			Damage:             helpers.IntPtr(826),
			GrenadesValueDeath: helpers.FloatPtr(225),
		},
		"76561197999609504": {
			HS:                 helpers.FloatPtr(0.122137404580152672),
			UtilityDamage:      helpers.IntPtr(275),
			Accuracy:           helpers.FloatPtr(0.255859375),
			Damage:             helpers.IntPtr(3846),
			GrenadesValueDeath: helpers.FloatPtr(272.72727272727275),
		},
		"76561198018574562": {
			HS:                 helpers.FloatPtr(0.1016949152542373),
			UtilityDamage:      helpers.IntPtr(49),
			Accuracy:           helpers.FloatPtr(0.30412371134020616),
			Damage:             helpers.IntPtr(2188),
			GrenadesValueDeath: helpers.FloatPtr(505.2631578947368),
		},
		"76561198081237162": {
			HS:                 helpers.FloatPtr(0.12162162162162163),
			UtilityDamage:      helpers.IntPtr(13),
			Accuracy:           helpers.FloatPtr(0.24749163879598662),
			Damage:             helpers.IntPtr(2002),
			GrenadesValueDeath: helpers.FloatPtr(438.0952380952381),
		},
	}

	for _, team := range demoFromDB.TeamsStats {
		for _, player := range team.PlayersStats {
			assert.Equal(t, *marks[*player.SteamID].HS, *player.Marks.HS, *player.SteamID)
			assert.Equal(t, *marks[*player.SteamID].UtilityDamage, *player.Marks.UtilityDamage, *player.SteamID)
			assert.Equal(t, *marks[*player.SteamID].Accuracy, *player.Marks.Accuracy, *player.SteamID)
			assert.Equal(t, *marks[*player.SteamID].Damage, *player.Marks.Damage, *player.SteamID)
			assert.Equal(t, *marks[*player.SteamID].GrenadesValueDeath, *player.Marks.GrenadesValueDeath, *player.SteamID)
		}
	}

	duelsTeams := map[int]map[string]model.DuelsTeam{
		2: {
			"5v5": {NbrOccurence: helpers.IntPtr(25), NbrWin: helpers.IntPtr(11)},
			"5v4": {NbrOccurence: helpers.IntPtr(9), NbrWin: helpers.IntPtr(5)},
			"5v3": {NbrOccurence: helpers.IntPtr(3), NbrWin: helpers.IntPtr(2)},
			"5v2": {NbrOccurence: helpers.IntPtr(1), NbrWin: helpers.IntPtr(0)},
			"5v1": {NbrOccurence: helpers.IntPtr(0), NbrWin: helpers.IntPtr(0)},
			"4v5": {NbrOccurence: helpers.IntPtr(22), NbrWin: helpers.IntPtr(10)},
			"4v4": {NbrOccurence: helpers.IntPtr(16), NbrWin: helpers.IntPtr(9)},
			"4v3": {NbrOccurence: helpers.IntPtr(14), NbrWin: helpers.IntPtr(11)},
			"4v2": {NbrOccurence: helpers.IntPtr(10), NbrWin: helpers.IntPtr(9)},
			"4v1": {NbrOccurence: helpers.IntPtr(7), NbrWin: helpers.IntPtr(7)},
			"3v5": {NbrOccurence: helpers.IntPtr(10), NbrWin: helpers.IntPtr(2)},
			"3v4": {NbrOccurence: helpers.IntPtr(9), NbrWin: helpers.IntPtr(2)},
			"3v3": {NbrOccurence: helpers.IntPtr(9), NbrWin: helpers.IntPtr(3)},
			"3v2": {NbrOccurence: helpers.IntPtr(6), NbrWin: helpers.IntPtr(3)},
			"3v1": {NbrOccurence: helpers.IntPtr(3), NbrWin: helpers.IntPtr(3)},
			"2v5": {NbrOccurence: helpers.IntPtr(5), NbrWin: helpers.IntPtr(0)},
			"2v4": {NbrOccurence: helpers.IntPtr(7), NbrWin: helpers.IntPtr(1)},
			"2v3": {NbrOccurence: helpers.IntPtr(9), NbrWin: helpers.IntPtr(3)},
			"2v2": {NbrOccurence: helpers.IntPtr(9), NbrWin: helpers.IntPtr(4)},
			"2v1": {NbrOccurence: helpers.IntPtr(5), NbrWin: helpers.IntPtr(4)},
			"1v5": {NbrOccurence: helpers.IntPtr(4), NbrWin: helpers.IntPtr(0)},
			"1v4": {NbrOccurence: helpers.IntPtr(8), NbrWin: helpers.IntPtr(0)},
			"1v3": {NbrOccurence: helpers.IntPtr(6), NbrWin: helpers.IntPtr(0)},
			"1v2": {NbrOccurence: helpers.IntPtr(6), NbrWin: helpers.IntPtr(0)},
			"1v1": {NbrOccurence: helpers.IntPtr(3), NbrWin: helpers.IntPtr(1)},
		},
		3: {
			"5v5": {NbrOccurence: helpers.IntPtr(25), NbrWin: helpers.IntPtr(14)},
			"5v4": {NbrOccurence: helpers.IntPtr(22), NbrWin: helpers.IntPtr(12)},
			"5v3": {NbrOccurence: helpers.IntPtr(10), NbrWin: helpers.IntPtr(8)},
			"5v2": {NbrOccurence: helpers.IntPtr(5), NbrWin: helpers.IntPtr(5)},
			"5v1": {NbrOccurence: helpers.IntPtr(4), NbrWin: helpers.IntPtr(4)},
			"4v5": {NbrOccurence: helpers.IntPtr(9), NbrWin: helpers.IntPtr(4)},
			"4v4": {NbrOccurence: helpers.IntPtr(16), NbrWin: helpers.IntPtr(7)},
			"4v3": {NbrOccurence: helpers.IntPtr(9), NbrWin: helpers.IntPtr(7)},
			"4v2": {NbrOccurence: helpers.IntPtr(7), NbrWin: helpers.IntPtr(6)},
			"4v1": {NbrOccurence: helpers.IntPtr(8), NbrWin: helpers.IntPtr(8)},
			"3v5": {NbrOccurence: helpers.IntPtr(3), NbrWin: helpers.IntPtr(1)},
			"3v4": {NbrOccurence: helpers.IntPtr(14), NbrWin: helpers.IntPtr(3)},
			"3v3": {NbrOccurence: helpers.IntPtr(9), NbrWin: helpers.IntPtr(6)},
			"3v2": {NbrOccurence: helpers.IntPtr(9), NbrWin: helpers.IntPtr(6)},
			"3v1": {NbrOccurence: helpers.IntPtr(6), NbrWin: helpers.IntPtr(6)},
			"2v5": {NbrOccurence: helpers.IntPtr(1), NbrWin: helpers.IntPtr(1)},
			"2v4": {NbrOccurence: helpers.IntPtr(10), NbrWin: helpers.IntPtr(1)},
			"2v3": {NbrOccurence: helpers.IntPtr(6), NbrWin: helpers.IntPtr(3)},
			"2v2": {NbrOccurence: helpers.IntPtr(9), NbrWin: helpers.IntPtr(5)},
			"2v1": {NbrOccurence: helpers.IntPtr(6), NbrWin: helpers.IntPtr(6)},
			"1v5": {NbrOccurence: helpers.IntPtr(0), NbrWin: helpers.IntPtr(0)},
			"1v4": {NbrOccurence: helpers.IntPtr(7), NbrWin: helpers.IntPtr(0)},
			"1v3": {NbrOccurence: helpers.IntPtr(3), NbrWin: helpers.IntPtr(0)},
			"1v2": {NbrOccurence: helpers.IntPtr(5), NbrWin: helpers.IntPtr(1)},
			"1v1": {NbrOccurence: helpers.IntPtr(3), NbrWin: helpers.IntPtr(2)},
		},
	}

	for _, team := range demoFromDB.TeamsStats {
		numTeam := 2
		for _, player := range team.PlayersStats {
			if *player.SteamID == "76561198018574562" {
				numTeam = 3
				break
			}
		}
		require.Len(t, team.DuelsTeam, len(duelsTeams[numTeam]))

		for _, duels := range team.DuelsTeam {
			require.Contains(t, duelsTeams[numTeam], *duels.Matchup, fmt.Sprintf("%d: %s", numTeam, *duels.Matchup))
			assert.Equal(t,
				*duelsTeams[numTeam][*duels.Matchup].NbrOccurence,
				*duels.NbrOccurence,
				fmt.Sprintf("%d: %s", numTeam, *duels.Matchup),
			)
			assert.Equal(t,
				*duelsTeams[numTeam][*duels.Matchup].NbrWin,
				*duels.NbrWin,
				fmt.Sprintf("%d: %s", numTeam, *duels.Matchup),
			)
		}
	}

	sidesJulien := []int{
		2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
		3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	}
	require.Len(t, demoFromDB.Rounds, len(sidesJulien))

	for numRound, round := range demoFromDB.Rounds {
		require.Len(t, round.MarksPerRound, 10)
		for _, marks := range round.MarksPerRound {
			if *marks.SteamID != "76561198018574562" {
				continue
			}
			assert.Equal(t, sidesJulien[numRound], *marks.Side)
			break
		}
	}

	sidesUaw := []int{
		2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
		3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	}
	require.Len(t, demoFromDB.Rounds, len(sidesUaw))
	for numRound, round := range demoFromDB.Rounds {
		require.Len(t, round.MarksPerRound, 10)
		for _, marks := range round.MarksPerRound {
			if *marks.SteamID != "76561198081237162" {
				continue
			}
			assert.Equal(t, sidesUaw[numRound], *marks.Side)
			break
		}
	}
}
