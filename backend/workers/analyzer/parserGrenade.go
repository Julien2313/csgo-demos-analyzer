package analyzer

import (
	"runtime/debug"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
)

func getRegisteredGrenade(p *Parser, ID int) *modelanalyzer.Grenade {
	return p.Game.CurrentRound.Grenades[ID]
}

func addRegisteredGrenade(p *Parser, projectile *common.GrenadeProjectile) *modelanalyzer.Grenade {
	if projectile == nil {
		return nil
	}

	grenade := &modelanalyzer.Grenade{
		ID:          projectile.UniqueID(),
		Grenade:     projectile,
		GrenadeType: projectile.WeaponInstance.Type,
	}
	p.Game.CurrentRound.Grenades[projectile.Entity.ID()] = grenade

	return grenade
}

func registerHeExplode(p *Parser) {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.HeExplode) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in ComputeGrenades.HeExplode of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		if !p.Game.Started {
			return
		}

		grenade := getRegisteredGrenade(p, e.GrenadeEvent.GrenadeEntityID)
		if grenade == nil {
			return
		}
		grenade.State = modelanalyzer.Explode
		p.AddFrame(nil, false, false)
	})
}

func registerFlashExplode(p *Parser) {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.FlashExplode) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil,
					"Recovered in ComputeGrenades.FlashExplode of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		if !p.Game.Started {
			return
		}

		grenade := getRegisteredGrenade(p, e.GrenadeEvent.GrenadeEntityID)
		if grenade == nil {
			return
		}
		grenade.State = modelanalyzer.Explode
		p.AddFrame(nil, false, false)
	})
}

func registerDecoyStart(p *Parser) {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.DecoyStart) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil,
					"Recovered in ComputeGrenades.DecoyStart of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		if !p.Game.Started {
			return
		}

		grenade := getRegisteredGrenade(p, e.GrenadeEvent.GrenadeEntityID)
		if grenade == nil {
			return
		}
		grenade.State = modelanalyzer.Start
		p.AddFrame(nil, false, false)
	})
}

func registerSmokeStart(p *Parser) {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.SmokeStart) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil,
					"Recovered in ComputeGrenades.SmokeStart of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		if !p.Game.Started {
			return
		}

		grenade := getRegisteredGrenade(p, e.GrenadeEvent.GrenadeEntityID)
		if grenade == nil {
			return
		}
		grenade.State = modelanalyzer.Start
		p.AddFrame(nil, false, false)
	})
}

func registerFireGrenadeStart(p *Parser) {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.FireGrenadeStart) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in ComputeGrenades.FireGrenadeStart"+
					" of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		if !p.Game.Started {
			return
		}

		grenade := getRegisteredGrenade(p, e.GrenadeEvent.GrenadeEntityID)
		if grenade == nil {
			return
		}
		grenade.State = modelanalyzer.Start
		p.AddFrame(nil, false, false)
	})
}

func registerGrenadeProjectileThrow(p *Parser) {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.GrenadeProjectileThrow) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in ComputeGrenades.GrenadeProjectileThrow "+
					" of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		if !p.Game.Started {
			return
		}

		grenade := addRegisteredGrenade(p, e.Projectile)
		grenade.State = modelanalyzer.Thrown
		p.AddFrame(nil, false, false)
	})
}
