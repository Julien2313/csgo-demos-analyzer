package analyzer

import (
	"log"
	"math/rand"
	"os"
	"strconv"
	"testing"
	"time"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var logTest *custlogger.Logger
var _testDBGlobal *gorm.DB

func TestMain(m *testing.M) {
	rand.Seed(time.Now().Unix())

	logTest = &custlogger.Logger{}
	logTest.SetupInstanceLogggerTest()

	_testDBGlobal = store.InitDB(true, store.SchemaTestAnalyzer, logTest)

	os.Exit(m.Run())
}

func TestAnalyzer_Start(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	a, err := Start(localDB, logTest, 0)
	defer a.Shutdown()
	require.NoError(t, err)

	assert.NotNil(t, a.data)
	assert.NotNil(t, a.db)
	assert.NotNil(t, a.log)
}

func TestAnalyzer_Shutdown(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	a, err := Start(localDB, logTest, 0)
	require.NoError(t, err)
	a.Shutdown()

	chanBusy := false
	select {
	case a.data <- DataAnalyzer{}:
	default:
		chanBusy = true
	}
	// because even if analyzer is shutdown, chan isn't close
	assert.False(t, chanBusy)
}

// @randomFailOnGitlab
func TestAnalyzer_CheckNotDownloadedOutAnalyzed(t *testing.T) {
	if os.Getenv("GITLAB") != "" {
		t.Skip()
	}
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	demoDAO := dao.NewDemoDAO(localDB, logTest)
	userDAO := dao.NewUserDAO(localDB)
	fastLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			LogLevel: logger.Silent,
		},
	)

	user := model.GenerateRandomUser(true)
	require.NoError(t, userDAO.Create(user))

	demo := model.GenerateRandomDemo(user, *user.SteamID)
	demo.Downloaded = helpers.BoolPtr(true)
	demo.VersionAnalyzer = helpers.Int64Ptr(0)
	require.NoError(t, demoDAO.Create(demo, &fastLogger))

	demo = model.GenerateRandomDemo(user, *user.SteamID)
	demo.Downloaded = helpers.BoolPtr(false)
	demo.VersionAnalyzer = helpers.Int64Ptr(0)
	require.NoError(t, demoDAO.Create(demo, &fastLogger))

	demo = model.GenerateRandomDemo(user, *user.SteamID)
	demo.Downloaded = helpers.BoolPtr(true)
	demo.VersionAnalyzer = helpers.Int64Ptr(0)
	require.NoError(t, demoDAO.Create(demo, &fastLogger))

	demo = model.GenerateRandomDemo(user, *user.SteamID)
	demo.Downloaded = helpers.BoolPtr(true)
	demo.VersionAnalyzer = helpers.Int64Ptr(0)
	require.NoError(t, demoDAO.Create(demo, &fastLogger))

	demo = model.GenerateRandomDemo(user, *user.SteamID)
	demo.Downloaded = helpers.BoolPtr(true)

	version := int64(100)
	demo.VersionAnalyzer = helpers.Int64Ptr(version + 1)
	require.NoError(t, demoDAO.Create(demo, &fastLogger))

	a := &Analyzer{
		data:    make(chan DataAnalyzer, maxChan),
		log:     &custlogger.Logger{},
		db:      localDB,
		version: version,
	}
	assert.NotNil(t, a.data)
	assert.NotNil(t, a.db)
	assert.NotNil(t, a.log)

	require.NoError(t, a.CheckOutAnalyzed())

	cptToAnalyzer := 0

	func() {
		for {
			select {
			case <-a.data:
				cptToAnalyzer++
			default:
				return
			}
		}
	}()
	assert.Equal(t, 3, cptToAnalyzer)
}

func TestAnalyzer_AddDataAnalyzer(t *testing.T) {
	a := &Analyzer{
		data: make(chan DataAnalyzer, maxChan),
	}

	IDs := []int64{int64(*helpers.RandUIntPSQL())}
	assert.NoError(t, a.AddDataAnalyzer(IDs))

	dataFromChan := <-a.data

	assert.Equal(t, IDs[0], dataFromChan.demoID)
}

func TestAnalyzer_AddDataAnalyzerFull(t *testing.T) {
	a := &Analyzer{
		data: make(chan DataAnalyzer, 2),
	}

	IDs := []int64{int64(rand.Int31())}
	assert.NoError(t, a.AddDataAnalyzer(IDs))
	assert.NoError(t, a.AddDataAnalyzer(IDs))
	assert.Error(t, a.AddDataAnalyzer(IDs))
	assert.Error(t, a.AddDataAnalyzer(IDs))
	<-a.data
	assert.NoError(t, a.AddDataAnalyzer(IDs))
}

func TestAnalyzer_AlreadyAnalyzed(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	demoDAO := dao.NewDemoDAO(localDB, logTest)
	userDAO := dao.NewUserDAO(localDB)
	fastLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			LogLevel: logger.Silent,
		},
	)

	user := model.GenerateRandomUser(true)
	user.SteamID = helpers.StrPtr(strconv.FormatInt(76561198018574562, 10))
	require.NoError(t, userDAO.Create(user))

	version := int64(100)
	demo := model.Demo{
		ID:              helpers.Int64Ptr(3428576404172701964),
		User:            user,
		UserID:          user.ID,
		Name:            helpers.StrPtr("match730_003428582232443322407_0670576728.dem.bz2"),
		Path:            helpers.StrPtr("../../../csgo-stock-demo/match730_003428582232443322407_0670576728.dem.bz2"),
		Size:            helpers.Int64Ptr(0),
		Downloaded:      helpers.BoolPtr(true),
		VersionAnalyzer: helpers.Int64Ptr(version + 1),
		Date:            &time.Time{},
	}
	require.NoError(t, demoDAO.Create(&demo, &fastLogger))

	data := DataAnalyzer{
		demoID: *demo.ID,
	}
	a := Analyzer{
		db:      localDB,
		log:     logTest,
		version: version,
	}
	a.ParseOneGame(data)

	assert.NoError(t, demoDAO.GetWithStats(&demo))
	assert.Equal(t, version+1, *demo.VersionAnalyzer)
}

func TestAnalyzer_Outdated(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()
	demoDAO := dao.NewDemoDAO(localDB, logTest)
	userDAO := dao.NewUserDAO(localDB)
	fastLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			LogLevel: logger.Silent,
		},
	)

	user := model.GenerateRandomUser(true)
	require.NoError(t, userDAO.Create(user))

	version := int64(100)
	demo := model.Demo{
		ID:              helpers.Int64Ptr(3428576404172701964),
		UserID:          user.ID,
		Name:            helpers.StrPtr("match730_003428582232443322407_0670576728.dem.bz2"),
		Path:            helpers.StrPtr("../../../csgo-stock-demo/match730_003428582232443322407_0670576728.dem.bz2"),
		Size:            helpers.Int64Ptr(0),
		Downloaded:      helpers.BoolPtr(true),
		VersionAnalyzer: helpers.Int64Ptr(version - 1),
		Date:            &time.Time{},
	}
	require.NoError(t, demoDAO.Create(&demo, &fastLogger))

	data := DataAnalyzer{
		demoID: *demo.ID,
	}
	a := Analyzer{
		db:      localDB,
		log:     logTest,
		version: version,
	}

	a.ParseOneGame(data)

	assert.NoError(t, demoDAO.GetWithStats(&demo))
	assert.Equal(t, version, *demo.VersionAnalyzer)
}

func TestAnalyzer_SaveStatesGrenades(t *testing.T) {
	frames := []*modelanalyzer.Frame{}
	frames = append(frames, &modelanalyzer.Frame{
		AddPositions: false,
		Grenades:     []*modelanalyzer.Grenade{{ID: 2}, {ID: 20, State: modelanalyzer.Explode}},
	})
	frames = append(frames, &modelanalyzer.Frame{
		AddPositions: true,
		Grenades:     []*modelanalyzer.Grenade{{ID: 20, State: modelanalyzer.Destroyed}},
	})
	frames = append(frames, &modelanalyzer.Frame{
		AddPositions: false,
	})
	a := &Analyzer{}
	a.SaveStatesGrenades(frames, 1)

	require.Len(t, frames[1].Grenades, 2)
	assert.Equal(t, frames[0].Grenades[1].ID, frames[1].Grenades[0].ID)
	assert.Equal(t, modelanalyzer.Explode, frames[1].Grenades[0].State)
}

func TestAnalyzer_HandleShoots(t *testing.T) {
	shooter := common.NewPlayer(nil)
	shooter.SteamID64 = rand.Uint64()

	frames := []*modelanalyzer.Frame{}
	frames = append(frames, &modelanalyzer.Frame{
		AddPositions: false,
		Players: map[uint64]*modelanalyzer.PlayerEvent{
			shooter.SteamID64: {},
		},
	})
	frames = append(frames, &modelanalyzer.Frame{
		AddPositions: true,
		Players: map[uint64]*modelanalyzer.PlayerEvent{
			shooter.SteamID64: {},
		},
	})
	a := &Analyzer{}
	eventWeaponFire := modelanalyzer.WeaponFireEvent{
		WeaponFire: events.WeaponFire{
			Shooter: shooter,
		},
	}
	a.HandleShoots(&eventWeaponFire, frames, 0)
	assert.Equal(t, 1, frames[1].Players[shooter.SteamID64].NbrShoots)

	a.HandleShoots(&eventWeaponFire, frames, 1)
	assert.Equal(t, 2, frames[1].Players[shooter.SteamID64].NbrShoots)
}
