package analyzer

import (
	"math"
	"runtime/debug"
	"strconv"
	"time"

	"github.com/jinzhu/copier"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/common"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"
	"github.com/thoas/go-funk"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer/modelanalyzer"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzerhelper"
)

// Parser type to parse the game
type Parser struct {
	DemoID           int64
	ParserDemoinfocs demoinfocs.Parser

	Game *modelanalyzer.Game
	log  *custlogger.Logger
}

// Parse analyze one game
func (p *Parser) Parse() {
	p.Game = &modelanalyzer.Game{
		Players: make(map[uint64]*modelanalyzer.Player),
		Teams:   make(map[int]*modelanalyzer.Team),
	}

	header, err := p.ParserDemoinfocs.ParseHeader()
	if err != nil {
		p.log.Error(err, "error when parsing header :", err)

		return
	}
	p.Game.MapName = *helpers.ParseMapName(header.MapName)

	defer func() {
		if r := recover(); r != nil {
			p.log.Errorf(nil, "Recovered in Parse of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
		}
	}()

	p.ParserDemoinfocs.RegisterEventHandler(func(e events.ItemPickup) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in ItemPickup of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		if !p.Game.Started {
			return
		}
		if e.Player.IsBot {
			return
		}
		if e.Weapon.Type == common.EqFlash {
			if e.Weapon.Owner == nil {
				return
			}
			// TODO on ne peut plus récup l'id du joueur acheteur
			if e.Weapon.Owner.SteamID64 == e.Player.SteamID64 {
				p.Game.Players[e.Player.SteamID64].StatsFlashs.Bought++
			}
		}
	})

	p.StorePositionsPlayers()
	p.RegisterGrenadesEvents()
	p.ComputeRoundStart()
	p.ComputePlayerConnect()
	p.ComputeMatchStarted()
	p.ComputeMatchEnded()
	p.ComputeTeamSideSwitch()
	p.ComputeTeamScore()
	p.ComputePlayerHurt()
	p.ComputePlayerFlashed()
	p.EventWeaponFire()
	p.GetRank()
	p.EventsRoundKills()
	p.EventsRoundBombPlanted()
	p.EventsRoundBombeDefused()
	p.EventsRoundBombeExplode()
	p.EventsRoundEndedTimeOut()
	p.EventsRoundFreezetimeEnd()
	p.EventsItemPickup()
	p.EventsItemDrop()
	p.GetMessages()

	if err := p.ParserDemoinfocs.ParseToEnd(); err != nil {
		if err.Error() == "send on closed channel" {
			p.log.Error(err, "Let's hope Markus solve this")
			return
		}
		p.log.Error(err, "Error while parsing demo : ", err)
	}
}

// GetMessages register every messages
func (p *Parser) GetMessages() {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.ChatMessage) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in GetMessages of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		ChatMessageEvent := events.ChatMessage{}
		if err := copier.Copy(&ChatMessageEvent, &e); err != nil {
			p.log.Errorf(nil, "Error while copying event ChatMessage : %v", err)
			return
		}

		p.AddFrame(&modelanalyzer.ChatMessageEvent{
			ChatMessage: ChatMessageEvent,
			TimeEvent:   p.ParserDemoinfocs.CurrentTime(),
		}, false, false)
	})
}

// EventsItemPickup register when player buy
func (p *Parser) EventsItemPickup() {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.ItemPickup) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in ItemPickup of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		p.AddFrame(nil, false, false)
	})
}

// EventsItemDrop register when player drop something
func (p *Parser) EventsItemDrop() {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.ItemDrop) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in ItemDrop of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		p.AddFrame(nil, false, false)
	})
}

// RegisterGrenadesEvents register all the grenades events we need
func (p *Parser) RegisterGrenadesEvents() {
	registerHeExplode(p)
	registerFlashExplode(p)
	registerDecoyStart(p)
	registerSmokeStart(p)
	registerFireGrenadeStart(p)
	registerGrenadeProjectileThrow(p)
}

// ComputeRoundStart event on RoundStart
func (p *Parser) ComputeRoundStart() {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.RoundStart) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in RoundStart of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		if !p.Game.Started {
			return
		}

		if e.TimeLimit == 115 {
			p.Game.NbrRounds++

			newRound := modelanalyzer.Round{
				NumRound:         p.Game.NbrRounds,
				Grenades:         make(map[int]*modelanalyzer.Grenade),
				TimeRoundStarted: p.ParserDemoinfocs.CurrentTime(),
				MarksPerRound:    make(map[uint64]*modelanalyzer.MarksPerRound),
			}
			p.Game.CurrentRound = &newRound
			p.Game.Rounds = append(p.Game.Rounds, &newRound)
		}
	})
}

// EventWeaponFire event on WeaponFire
func (p *Parser) EventWeaponFire() {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.WeaponFire) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in RoundStart of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		if !p.Game.Started {
			return
		}

		if p.LastFrameShootPlayer(e.Shooter.SteamID64) == p.ParserDemoinfocs.CurrentFrame() {
			return
		}

		WeaponFireEvent := events.WeaponFire{}
		if err := copier.Copy(&WeaponFireEvent, &e); err != nil {
			p.log.Errorf(nil, "Error while copying event WeaponFire : %v", err)
			return
		}

		p.AddFrame(&modelanalyzer.WeaponFireEvent{
			WeaponFire: WeaponFireEvent,
			TimeEvent:  p.ParserDemoinfocs.CurrentTime(),
		}, false, false)
	})
}

// ComputePlayerConnect event on PlayerConnect
func (p *Parser) ComputePlayerConnect() {
	// killer and flasher used as key for heatmap
	var forbidenNames = []string{"killer", "flasher"}

	p.ParserDemoinfocs.RegisterEventHandler(func(e events.PlayerConnect) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in PlayerConnect of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		if _, exists := p.Game.Players[e.Player.SteamID64]; exists {
			p.Game.Players[e.Player.SteamID64].Player = e.Player
			return
		}
		var username = e.Player.Name
		if funk.Contains(forbidenNames, username) {
			username = "_" + username
		}

		player := &modelanalyzer.Player{
			Player:   e.Player,
			SteamID:  e.Player.SteamID64,
			Username: username,
		}
		p.Game.Players[player.SteamID] = player
	})

	p.ParserDemoinfocs.RegisterEventHandler(func(e events.PlayerDisconnected) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in PlayerDisconnected of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		steamID := e.Player.SteamID64
		if _, exists := p.Game.Players[steamID]; exists && p.Game.Players[steamID].Team == nil {
			p.Game.Players[steamID].Player = e.Player
			p.Game.Players[steamID].Color = e.Player.Color()
			if p.Game.Players[steamID] != nil &&
				p.Game.Players[steamID].Player != nil &&
				p.Game.Players[steamID].Player.TeamState != nil {
				p.Game.Players[steamID].Team = p.Game.Teams[int(p.Game.Players[steamID].Player.TeamState.Team())]
			}
		}
	})
}

// ComputeMatchStarted event on MatchStarted
func (p *Parser) ComputeMatchStarted() {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.MatchStartedChanged) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in MatchStartedChanged of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		if !(!e.OldIsStarted && e.NewIsStarted) && p.Game.Ended {
			return
		}

		// a watchdog in case p.Game.Ended isn't set to true ?
		if !(!e.OldIsStarted && e.NewIsStarted) && p.Game.NbrRounds > 10 {
			return
		}

		p.Game.Started = true
		p.Game.NbrRounds = 0
		p.Game.FramesPerSecond = int(math.Round(float64(p.ParserDemoinfocs.Header().PlaybackFrames) /
			p.ParserDemoinfocs.Header().PlaybackTime.Seconds()))

		newRound := modelanalyzer.Round{
			NumRound:         0,
			Grenades:         make(map[int]*modelanalyzer.Grenade),
			TimeRoundStarted: p.ParserDemoinfocs.CurrentTime(),
			MarksPerRound:    make(map[uint64]*modelanalyzer.MarksPerRound),
		}
		p.Game.CurrentRound = &newRound
		p.Game.Rounds = []*modelanalyzer.Round{&newRound}

		for _, team := range p.Game.Teams {
			team.Players = make(map[uint64]*modelanalyzer.Player)
		}

		for steamID := range p.Game.Players {
			p.Game.Players[steamID].Color = p.Game.Players[steamID].Player.Color()
			if p.Game.Players[steamID].Player.TeamState == nil {
				for _, player := range p.ParserDemoinfocs.GameState().Participants().All() {
					if player.SteamID64 == steamID && player.TeamState != nil &&
						(player.TeamState.Team() == common.TeamCounterTerrorists || player.TeamState.Team() == common.TeamTerrorists) {
						p.Game.Players[steamID].Player.TeamState = player.TeamState
					}
				}
			}

			if p.Game.Players[steamID].Player == nil || p.Game.Players[steamID].Player.TeamState == nil {
				continue
			}

			teamParser := p.Game.Players[steamID].Player.TeamState.Team()
			if teamParser != common.TeamTerrorists && teamParser != common.TeamCounterTerrorists {
				continue
			}

			localTeam := int(teamParser)
			if p.Game.Teams[localTeam] == nil {
				p.Game.Teams[localTeam] = &modelanalyzer.Team{
					Players:       make(map[uint64]*modelanalyzer.Player),
					DuelsMatrix:   make(map[modelanalyzer.DuelIndex]*[]modelanalyzer.Duel),
					NumTeam:       localTeam,
					NumTeamParser: teamParser,
				}
				for a := 1; a <= 5; a++ {
					for e := 1; e <= 5; e++ {
						index := modelanalyzer.DuelIndex{
							NbrAliveAllies:   a,
							NbrAliveEnnemies: e,
						}
						p.Game.Teams[localTeam].DuelsMatrix[index] = &[]modelanalyzer.Duel{}
					}
				}
			}
			p.Game.Players[steamID].Team = p.Game.Teams[localTeam]
			p.Game.Teams[localTeam].Players[steamID] = p.Game.Players[steamID]
		}
	})
}

// ComputeMatchEnded catch the event when the match ended
func (p *Parser) ComputeMatchEnded() {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.AnnouncementWinPanelMatch) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in AnnouncementWinPanelMatch of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		p.Game.Ended = true
	})
}

// ComputeTeamSideSwitch event on TeamSideSwitch
func (p *Parser) ComputeTeamSideSwitch() {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.TeamSideSwitch) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in TeamSideSwitch of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		if !p.Game.Started {
			return
		}
		p.Game.Switched = !p.Game.Switched
	})
}

// ComputePlayerHurt event on PlayerHurt
func (p *Parser) ComputePlayerHurt() {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.PlayerHurt) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in ComputeScorePlayerHurt of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()
		if !p.Game.Started {
			return
		}

		// we want to remove the event weaponFire if the player hit someone
		nbrFrames := len(p.Game.CurrentRound.Frames)
		if nbrFrames > 0 &&
			p.Game.CurrentRound.Frames[nbrFrames-1].NumFrame == p.ParserDemoinfocs.CurrentFrame() {
			frame := p.Game.CurrentRound.Frames[nbrFrames-1]
			for numEvent := range frame.Events {
				if frame.Events[numEvent] != nil {
					if frame.Events[numEvent].GetType() == modelanalyzer.WeaponFireEvt {
						evt := frame.Events[numEvent].(*modelanalyzer.WeaponFireEvent)
						if evt.WeaponFire.Shooter != nil &&
							e.Attacker != nil &&
							evt.WeaponFire.Shooter.SteamID64 == e.Attacker.SteamID64 {
							frame.Events[len(frame.Events)-1], frame.Events[numEvent] =
								frame.Events[numEvent], frame.Events[len(frame.Events)-1]
							frame.Events = frame.Events[:len(frame.Events)-1]
							break
						}
					}
				}
			}
		}

		PlayerHurtEvent := events.PlayerHurt{}
		if err := copier.Copy(&PlayerHurtEvent, &e); err != nil {
			p.log.Errorf(err, "Error while copying event PlayerHurt : %v", err)
			return
		}

		p.AddFrame(&modelanalyzer.PlayerHurtEvent{
			PlayerHurt: PlayerHurtEvent,
			TimeEvent:  p.ParserDemoinfocs.CurrentTime(),
		}, false, false)
	})
}

// ComputePlayerFlashed event on PlayerFlashed
func (p *Parser) ComputePlayerFlashed() {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.PlayerFlashed) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in ComputeScorePlayerFlashed of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()
		if !p.Game.Started {
			return
		}

		PlayerFlashedEvent := events.PlayerFlashed{}
		if err := copier.Copy(&PlayerFlashedEvent, &e); err != nil {
			p.log.Errorf(nil, "Error while copying event PlayerFlashed : %v", err)
			return
		}

		p.AddFrame(&modelanalyzer.PlayerFlashedEvent{
			PlayerFlashed: PlayerFlashedEvent,
			TimeEvent:     p.ParserDemoinfocs.CurrentTime(),
		}, false, false)
	})
}

// ComputeTeamScore changes the score when it changes
func (p *Parser) ComputeTeamScore() {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.ScoreUpdated) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in ComputeTeamScore of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		if e.NewScore == e.OldScore {
			return
		}
		// I guess this appends when the warmup starts ?
		if len(p.Game.Teams) != 2 {
			return
		}

		for numTeam, team := range p.Game.Teams {
			if team.NumTeamParser == e.TeamState.Team() {
				if p.Game.Switched {
					continue
				}
				p.Game.Teams[numTeam].Score = e.NewScore

				break
			} else {
				if !p.Game.Switched {
					continue
				}
				p.Game.Teams[numTeam].Score = e.NewScore

				break
			}
		}
	})
}

// GetRank get the ranks if the players
func (p *Parser) GetRank() {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.RankUpdate) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in GetRank of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		if _, exists := p.Game.Players[e.SteamID64()]; exists {
			p.Game.Players[e.SteamID64()].Rank = e.RankNew
		}
	})
}

// StorePositionsPlayers every second, store the position of all the players
func (p *Parser) StorePositionsPlayers() {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.FrameDone) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in StorePositionsPlayers of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		if !p.Game.Started || p.Game.Ended {
			return
		}

		p.AddFrame(nil,
			p.ParserDemoinfocs.CurrentFrame()%(p.Game.FramesPerSecond/2) == 0,
			p.ParserDemoinfocs.CurrentFrame()%(p.Game.FramesPerSecond) == 0,
		)
	})
}

// EventsRoundKills store the event of a kill, with the weapon, killer, killed, time, position
func (p *Parser) EventsRoundKills() {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.Kill) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in EventsRoundKills of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		if !p.Game.Started {
			return
		}

		killEvent := events.Kill{}
		if err := copier.Copy(&killEvent, &e); err != nil {
			p.log.Errorf(nil, "Error while copying event Kill : %v", err)
		}
		p.AddFrame(&modelanalyzer.KillEvent{
			Kill:      killEvent,
			TimeEvent: p.ParserDemoinfocs.CurrentTime(),
		}, false, false)
	})
}

// EventsRoundBombPlanted register the event when the bombe has been planted
func (p *Parser) EventsRoundBombPlanted() {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.BombPlanted) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in EventsRoundBombPlanted of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		if !p.Game.Started {
			return
		}

		BombPlantedEvent := events.BombPlanted{}
		if err := copier.Copy(&BombPlantedEvent, &e); err != nil {
			p.log.Errorf(nil, "Error while copying event BombPlanted : %v", err)
		}
		// bug on grind !
		if BombPlantedEvent.Site == 0 {
			BombPlantedEvent.Site = 66
		}
		p.AddFrame(&modelanalyzer.BombPlantedEvent{
			BombPlanted: BombPlantedEvent,
			TimeEvent:   p.ParserDemoinfocs.CurrentTime(),
		}, false, false)
		p.Game.CurrentRound.TimeBombPlanted = helpers.DurationPtr(p.ParserDemoinfocs.CurrentTime())
	})
}

// EventsRoundBombeDefused register the event when the round ends with the bomb defused
func (p *Parser) EventsRoundBombeDefused() {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.BombDefused) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in EventsRoundBombeDefused of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		if !p.Game.Started {
			return
		}

		bombPlantedSince := p.ParserDemoinfocs.CurrentTime() - *p.Game.CurrentRound.TimeBombPlanted

		BombDefusedEvent := events.BombDefused{}
		if err := copier.Copy(&BombDefusedEvent, &e); err != nil {
			p.log.Errorf(nil, "Error while copying event BombDefused : %v", err)
		}
		p.AddFrame(&modelanalyzer.BombDefusedEvent{
			BombDefused: BombDefusedEvent,
			// bomb explode after 40 secs
			TimeRemainded: time.Second*40 - bombPlantedSince,
		}, false, false)
	})
}

// EventsRoundBombeExplode register the event when the round ends with the bomb explode
func (p *Parser) EventsRoundBombeExplode() {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.BombExplode) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in EventsRoundBombeExplode of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		if !p.Game.Started {
			return
		}

		p.AddFrame(&modelanalyzer.BombExplodeEvent{}, false, false)
	})
}

// EventsRoundEndedTimeOut register the event when the round ends with timeout
func (p *Parser) EventsRoundEndedTimeOut() {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.ScoreUpdated) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in EventsRoundEndedTimeOut of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		if !p.Game.Started {
			return
		}

		p.Game.CurrentRound.SideWin = int(e.TeamState.Team())
	})
}

// EventsRoundFreezetimeEnd register the event when the freeze time ends
func (p *Parser) EventsRoundFreezetimeEnd() {
	p.ParserDemoinfocs.RegisterEventHandler(func(e events.RoundFreezetimeEnd) {
		defer func() {
			if r := recover(); r != nil {
				p.log.Errorf(nil, "Recovered in EventsRoundFreezetimeEnd of demo ID %d, %v %s", p.DemoID, r, string(debug.Stack()))
			}
		}()

		if !p.Game.Started {
			return
		}

		p.AddFrame(&modelanalyzer.RoundFreezetimeEndEvent{}, true, false)
	})
}

// GetAllPlayerPlaying store in a map all players for an instant
func (p *Parser) GetAllPlayerPlaying() map[uint64]*modelanalyzer.PlayerEvent {
	playersEvent := make(map[uint64]*modelanalyzer.PlayerEvent)
	players := []*common.Player{}

	for _, player := range p.ParserDemoinfocs.GameState().Participants().All() {
		if _, exists := p.Game.Players[player.SteamID64]; !exists {
			continue
		}

		var isPlayerRegistered bool
		for numP, p := range players {
			if p.SteamID64 == player.SteamID64 {
				isPlayerRegistered = true

				if player.IsConnected {
					players[numP] = player
				}
				break
			}
		}

		if !isPlayerRegistered {
			players = append(players, player)
			continue
		}
	}

	for _, player := range players {
		playersEvent[player.SteamID64] = modelanalyzer.PlayerCommonToEvent(player, p.Game.Players, p.Game.NbrRounds)
	}

	return playersEvent
}

// GetAllGrenadesIG return all the grandes of the game
func (p *Parser) GetAllGrenadesIG() []*modelanalyzer.Grenade {
	grenades := []*modelanalyzer.Grenade{}
	for _, grenade := range p.ParserDemoinfocs.GameState().GrenadeProjectiles() {
		g := &modelanalyzer.Grenade{
			ID:          grenade.UniqueID(),
			GrenadeType: grenade.WeaponInstance.Type,
			Position:    grenade.Position(),
		}
		if _, exists := p.Game.CurrentRound.Grenades[grenade.Entity.ID()]; !exists {
			continue
		}

		g.State = p.Game.CurrentRound.Grenades[grenade.Entity.ID()].State
		// because the state Explode doesn't change for HE need to delete it manually :(
		if p.Game.CurrentRound.Grenades[grenade.Entity.ID()].State == modelanalyzer.Explode &&
			p.Game.CurrentRound.Grenades[grenade.Entity.ID()].GrenadeType == common.EqHE {
			delete(p.Game.CurrentRound.Grenades, grenade.Entity.ID())
		}

		grenades = append(grenades, g)
	}
	for _, grenade := range p.ParserDemoinfocs.GameState().Infernos() {
		fire := grenade.Fires().ConvexHull2D()
		g := &modelanalyzer.Grenade{
			ID:           grenade.UniqueID(),
			GrenadeType:  common.EqIncendiary,
			PositionFire: &fire,
		}

		grenades = append(grenades, g)
	}

	return grenades
}

// GetPlayerCarrierSteamIDBomb return the steamID of the carrier of the bomb
// if no one is carrying the bomb, return ""
func (p *Parser) GetPlayerCarrierSteamIDBomb() string {
	var playerCarrierSteamID string
	if p.ParserDemoinfocs.GameState().Bomb().Carrier != nil {
		playerCarrierSteamID = strconv.FormatUint(p.ParserDemoinfocs.GameState().Bomb().Carrier.SteamID64, 10)
	}

	return playerCarrierSteamID
}

// AddFrame add a frame to the current round
func (p *Parser) AddFrame(event modelanalyzer.EventAnalyzer, addPositions, addPlayersCard bool) {
	if !p.Game.Started {
		return
	}

	currentTime := p.ParserDemoinfocs.CurrentTime()

	// if round is still freeze, we don't want to store anything
	if event != nil &&
		event.GetType() == modelanalyzer.RoundFreezetimeEndEvt &&
		p.Game.CurrentRound.TimeFreezeTimeEnd == nil {
		p.Game.CurrentRound.TimeFreezeTimeEnd = helpers.DurationPtr(currentTime)
		addPositions = true
		addPlayersCard = true
	}

	if p.Game.CurrentRound.TimeFreezeTimeEnd == nil {
		return
	}

	nbrFrames := len(p.Game.CurrentRound.Frames)
	if nbrFrames != 0 {
		if p.Game.CurrentRound.Frames[nbrFrames-1].NumFrame == p.ParserDemoinfocs.CurrentFrame() {
			p.Game.CurrentRound.Frames[nbrFrames-1].Events = append(p.Game.CurrentRound.Frames[nbrFrames-1].Events, event)
			return
		}
	}

	var strTimer string
	var timer time.Duration

	if p.Game.CurrentRound.TimeBombPlanted != nil {
		timer = time.Second*40 - (currentTime - *p.Game.CurrentRound.TimeBombPlanted)
	} else {
		timer = time.Second*115 - (currentTime - *p.Game.CurrentRound.TimeFreezeTimeEnd)
	}

	if timer.Seconds() < 0 {
		timer = 0
	}
	strTimer = helpers.FmtDuration(timer)

	p.Game.CurrentRound.Frames = append(p.Game.CurrentRound.Frames, &modelanalyzer.Frame{
		NumFrame:             p.ParserDemoinfocs.CurrentFrame(),
		IGTime:               currentTime,
		Timer:                strTimer,
		BombLastPosDown:      analyzerhelper.ConvertMapPos(p.Game.MapName, p.ParserDemoinfocs.GameState().Bomb().Position()),
		BombPlanted:          p.Game.CurrentRound.TimeBombPlanted != nil,
		PlayerCarrierSteamID: p.GetPlayerCarrierSteamIDBomb(),
		Players:              p.GetAllPlayerPlaying(),
		AddPlayersCard:       addPlayersCard,
		AddPositions:         addPositions,
		Grenades:             p.GetAllGrenadesIG(),
		Events:               []modelanalyzer.EventAnalyzer{event},
	})
}

// LastFrameShootPlayer return the last frame the player shot
func (p *Parser) LastFrameShootPlayer(attackerSteamID uint64) int {
	if p.Game == nil || p.Game.CurrentRound == nil {
		return -1
	}
	nbrFrames := len(p.Game.CurrentRound.Frames)
	if nbrFrames == 0 {
		return -1
	}

	for numFrame := nbrFrames - 1; numFrame > 0; numFrame-- {
		currFrame := p.Game.CurrentRound.Frames[numFrame]
		for _, event := range currFrame.Events {
			if event == nil {
				continue
			}
			if event.GetType() == modelanalyzer.PlayerHurtEvt {
				playerHurt := event.(*modelanalyzer.PlayerHurtEvent).PlayerHurt
				if playerHurt.Attacker != nil {
					if event.(*modelanalyzer.PlayerHurtEvent).PlayerHurt.Attacker.SteamID64 == attackerSteamID {
						return currFrame.NumFrame
					}
				}
			}
			if event.GetType() == modelanalyzer.WeaponFireEvt {
				weaponFire := event.(*modelanalyzer.WeaponFireEvent).WeaponFire
				if weaponFire.Shooter != nil {
					if event.(*modelanalyzer.WeaponFireEvent).WeaponFire.Shooter.SteamID64 == attackerSteamID {
						return currFrame.NumFrame
					}
				}
			}
		}
	}

	return -1
}
