// +build !race

package analyzer

import (
	"log"
	"os"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gorm.io/gorm/logger"
)

func TestAnalyzer_ParseGameOT(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	demoDAO := dao.NewDemoDAO(localDB, logTest)
	userDAO := dao.NewUserDAO(localDB)
	fastLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			LogLevel: logger.Silent,
		},
	)

	user := model.GenerateRandomUser(true)
	user.SteamID = helpers.StrPtr(strconv.FormatInt(76561198018574562, 10))
	require.NoError(t, userDAO.Create(user))

	demo := model.Demo{
		ID:              helpers.Int64Ptr(3428576404172701965),
		User:            user,
		UserID:          user.ID,
		Name:            helpers.StrPtr("match730_4ac5bc37-0f2a-4326-81ad-9d284da36cf4.dem.gz"),
		Path:            helpers.StrPtr("../../../csgo-stock-demo/match730_4ac5bc37-0f2a-4326-81ad-9d284da36cf4.dem.gz"),
		Size:            helpers.Int64Ptr(0),
		Downloaded:      helpers.BoolPtr(true),
		VersionAnalyzer: helpers.Int64Ptr(0),
		Date:            &time.Time{},
	}
	require.NoError(t, demoDAO.Create(&demo, &fastLogger))
	data := DataAnalyzer{
		demoID: *demo.ID,
	}
	a := Analyzer{db: localDB, log: logTest}
	a.ParseOneGame(data)

	demoFromDB := model.Demo{ID: demo.ID}
	require.NoError(t, demoDAO.GetWithStats(&demoFromDB))
	require.Len(t, demoFromDB.TeamsStats, 2)

	require.Len(t, demoFromDB.Rounds, 52)
	sidesJulien := []int{
		2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
		3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
		3, 3, 3, 2, 2, 2,
		2, 2, 2, 3, 3, 3,
		3, 3, 3, 2, 2, 2,
		2, 2, 2, 3,
	}
	for numRound, round := range demoFromDB.Rounds {
		var mark *model.MarksPerRound
		for _, m := range round.MarksPerRound {
			if *m.SteamID == "76561198018574562" {
				mark = m
				break
			}
		}
		require.NotNil(t, mark, len(round.MarksPerRound))
		assert.Equal(t, sidesJulien[numRound], *mark.Side, numRound)
	}

	require.Len(t, demoFromDB.TeamsStats, 2)
	require.Len(t, demoFromDB.TeamsStats[0].PlayersStats, 5)
	require.Len(t, demoFromDB.TeamsStats[1].PlayersStats, 5)
}
