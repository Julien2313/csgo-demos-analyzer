package democlassifier

import (
	"math/rand"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gorm.io/gorm"
)

var logTest *custlogger.Logger

var _testDBGlobal *gorm.DB

func TestMain(m *testing.M) {
	rand.Seed(time.Now().Unix())

	logTest = &custlogger.Logger{}
	logTest.SetupInstanceLogggerTest()

	_testDBGlobal = store.InitDB(true, store.SchemaTestDemoClassifier, logTest)

	os.Exit(testMainWraper(m))
}

func testMainWraper(m *testing.M) int {
	return m.Run()
}

func TestStart(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	_, err := Start(localDB, nil, nil, logTest)
	require.NoError(t, err)
}

func TestDemoClassifier_AddDate(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	dc := NewDemoClassifier(localDB, nil, nil, logTest)

	demoSent := model.Demo{
		ID: helpers.Int64Ptr(rand.Int63()),
	}
	require.NoError(t, dc.AddDate(demoSent))

	dataFromChan := <-dc.data
	assert.Equal(t, *demoSent.ID, *dataFromChan.demo.ID)
}

func TestDemoClassifier_Shutdown(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	dc := NewDemoClassifier(localDB, nil, nil, logTest)

	go func() {
		require.NoError(t, dc.Shutdown())
	}()

	chanBusy := false
	select {
	case <-dc.shutdown:
	case <-time.After(time.Second):
		chanBusy = true
	}
	assert.False(t, chanBusy)
}

func TestClassifyDemo(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	user := model.StoreGeneratedUser(localDB)
	demoToClassify := model.GenerateRandomDemo(user, *user.SteamID)
	demoToClassify.Path = helpers.StrPtr("../../../csgo-stock-demo/match730_003429481443976282181_0454742687.dem.bz2")
	demoToClassify.Name = helpers.StrPtr("match730_003429481443976282181_0454742687.dem.bz2")
	demoToClassify.HashFile = nil
	require.NoError(t, localDB.Create(demoToClassify).Error)
	demoToClassify.Size = nil

	gameMode, err := ClassifyDemo(localDB, logTest, demoToClassify)
	require.NoError(t, err)
	require.Equal(t, model.Competitive, gameMode)

	demoToClassify = model.GenerateRandomDemo(user, *user.SteamID)
	demoToClassify.Path = helpers.StrPtr("../../../csgo-stock-demo/match730_825009241640.dem")
	demoToClassify.Name = helpers.StrPtr("match730_825009241640.dem")
	require.NoError(t, localDB.Create(demoToClassify).Error)

	gameMode, err = ClassifyDemo(localDB, logTest, demoToClassify)
	require.NoError(t, err)
	require.Equal(t, model.DM, gameMode)

	assert.NotNil(t, demoToClassify.HashFile)
	assert.NotNil(t, demoToClassify.Size)
}
