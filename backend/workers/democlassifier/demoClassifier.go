package democlassifier

import (
	"bytes"
	"crypto/sha256"
	"errors"
	"fmt"
	"os"
	"runtime/debug"
	"time"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzerdeathmatch"
	"gorm.io/gorm"
)

// DataDemoClassifier data send to classify the demo
type DataDemoClassifier struct {
	demo model.Demo
}

// DemoClassifier workers who classify the demo
type DemoClassifier struct {
	data     chan DataDemoClassifier
	shutdown chan interface{}
	db       *gorm.DB
	log      *custlogger.Logger
	Anal     *analyzer.Analyzer
	AnalDM   *analyzerdeathmatch.AnalyzerDeathMatch
}

const maxChan = 10000

// ErrChanFull chan is full
var ErrChanFull = errors.New("chan DemoClassifier is full")

// ErrCouldNotShutdown could not shutdown demoClassifier
var ErrCouldNotShutdown = errors.New("could not shutdown demoClassifier")

// NewDemoClassifier constructor of DemoClassifier
func NewDemoClassifier(
	db *gorm.DB,
	anal *analyzer.Analyzer,
	analDM *analyzerdeathmatch.AnalyzerDeathMatch,
	logDemoClassifier *custlogger.Logger,
) *DemoClassifier {
	return &DemoClassifier{
		data:     make(chan DataDemoClassifier, maxChan),
		shutdown: make(chan interface{}),
		log:      logDemoClassifier,
		Anal:     anal,
		AnalDM:   analDM,
		db:       db,
	}
}

// Start starts this worker
func Start(
	db *gorm.DB,
	anal *analyzer.Analyzer,
	analDM *analyzerdeathmatch.AnalyzerDeathMatch,
	logDemoClassifier *custlogger.Logger,
) (
	*DemoClassifier,
	error,
) {
	dc := NewDemoClassifier(db, anal, analDM, logDemoClassifier)
	go dc.CheckForEver()

	demoDAO := dao.NewDemoDAO(dc.db, dc.log)

	demosUnanalized, err := demoDAO.GetAllNotClassified()
	if err != nil {
		dc.log.Error(err, "Can't get all demos not classified:", err)
		return nil, err
	}

	for _, demo := range demosUnanalized {
		if err := dc.AddDate(demo); err != nil {
			dc.log.Error(err, "Can't add to demo to classify:", err)
			return nil, err
		}
	}

	return dc, nil
}

// AddDate add data to be downloaded
func (dc *DemoClassifier) AddDate(demo model.Demo) error {
	select {
	case dc.data <- DataDemoClassifier{
		demo: demo,
	}:
	default:
		return ErrChanFull
	}

	return nil
}

// Shutdown shutdown DemoClassifier
func (dc *DemoClassifier) Shutdown() error {
	shutdownDC := make(chan interface{})

	go func() {
		dc.shutdown <- struct{}{}
		shutdownDC <- struct{}{}
	}()

	select {
	case <-shutdownDC:
	case <-time.After(time.Minute):
		return ErrCouldNotShutdown
	}

	return nil
}

// CheckForEver checks if their is a new demo to download
func (dc *DemoClassifier) CheckForEver() {
	defer func() {
		if r := recover(); r != nil {
			dc.log.Errorf(nil, "Recovered in DemoClassifier.checkForEver %v %s", r, string(debug.Stack()))
			time.Sleep(time.Second * 10)
			go dc.CheckForEver()
		}
	}()
	daoDemo := dao.NewDemoDAO(dc.db, dc.log)

	for {
		select {
		case <-dc.shutdown:
			dc.log.Info("DemoClassifier shutdown")
			return
		default:
		}
		select {
		case <-dc.shutdown:
			dc.log.Info("DemoClassifier shutdown")
			return
		case data := <-dc.data:
			gameMode, err := ClassifyDemo(dc.db, dc.log, &data.demo)
			if err != nil || gameMode == model.UnknownGameMode {
				if err != nil {
					dc.log.Error(err, "error while trying to get the gamemode : ", err)
				}
				if err := daoDemo.Delete(data.demo); err != nil {
					dc.log.Error(err, "error while trying to delete demo in DB : ", err)
					continue
				}
				if err := os.Remove(*data.demo.Path); err != nil {
					dc.log.Error(err, "error while trying to delete demo file : ", err)
					continue
				}
				continue
			}

			switch gameMode {
			case model.Competitive:
				if err := dc.Anal.AddDataAnalyzer([]int64{*data.demo.ID}); err != nil {
					dc.log.Error(err, "error while trying to analyze competitive game : ", err)
					continue
				}
			case model.DM:
				if err := daoDemo.SwapCompetitiveToDMDemo(
					data.demo,
					fmt.Sprintf("%s/%s", dc.AnalDM.PathStorage, *data.demo.Name),
				); err != nil {
					dc.log.Error(err, "error while trying to convert competitive to deathmatch: ", err)
					continue
				}
				if err := dc.AnalDM.AddDataAnalyzer([]int64{*data.demo.ID}); err != nil {
					dc.log.Error(err, "error while trying to analyze DM game: ", err)
					continue
				}
			}
		}
	}
}

// ClassifyDemo check if a demo is a DM, mm etc
func ClassifyDemo(db *gorm.DB, log *custlogger.Logger, demo *model.Demo) (model.DemoGameMode, error) {
	fileDemo, err := os.Open(*demo.Path)
	if err != nil {
		return model.UnknownGameMode, err
	}
	defer func() {
		if err := fileDemo.Close(); err != nil {
			log.Error(err, "error while closing the demo :", err.Error())
		}
	}()

	demoUnzippedBytes, err := helpers.DemoFileToByte(fileDemo)
	if err != nil {
		log.Error(err, "could not read the demo file : ", err)
		return model.UnknownGameMode, err
	}

	hasher := sha256.New()
	if _, err := hasher.Write(demoUnzippedBytes); err != nil {
		log.Error(err, "error while trying to write in hash : ", err)
		return model.UnknownGameMode, err
	}

	demoDAO := dao.NewDemoDAO(db, log)
	demo.HashFile = hasher.Sum(nil)
	if err := demoDAO.SetHash(*demo.ID, demo.HashFile); err != nil {
		log.Error(err, "error while trying to set hash : ", err)
		return model.UnknownGameMode, err
	}

	stats, err := fileDemo.Stat()
	if err != nil {
		log.Error(err, "error while trying to get the stats of the demo : ", err)
		return model.UnknownGameMode, err
	}

	demo.Size = helpers.Int64Ptr(stats.Size())
	if err := demoDAO.SetSize(*demo.ID, *demo.Size); err != nil {
		log.Error(err, "error while trying to set size : ", err)
		return model.UnknownGameMode, err
	}

	var errorFromParser error
	demoData := helpers.NewDemoInfoParserWithoutPanic(bytes.NewReader(demoUnzippedBytes), &errorFromParser)
	if errorFromParser != nil {
		log.Error(err, "error while trying to create new parser : ", errorFromParser)
		return model.UnknownGameMode, errorFromParser
	}
	defer demoData.Close()

	helpers.ParseDemoWithoutPanic(demoData, &errorFromParser)
	if errorFromParser != nil {
		log.Error(err, "error while trying to parse the demo : ", errorFromParser)
		return model.UnknownGameMode, errorFromParser
	}
	gameMode := demoData.GameState().ConVars()["game_mode"]
	gameType := demoData.GameState().ConVars()["game_type"]

	var mode model.DemoGameMode
	if gameType == "1" && gameMode == "2" {
		mode = model.DM
	} else if (gameType == "0" || gameType == "") && gameMode == "1" {
		mode = model.Competitive
	}

	if err := demoDAO.SetGameMode(*demo.ID, mode); err != nil {
		log.Error(err, "error while trying to set game_mode : ", err)
		return model.UnknownGameMode, err
	}

	return mode, nil
}
