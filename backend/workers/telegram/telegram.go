package telegram

import (
	"errors"
	"runtime/debug"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
)

// DataTelegramBot data send for telegram
type DataTelegramBot struct {
	message string
}

// Bot workers who receive and send message to telegram
type Bot struct {
	data     chan DataTelegramBot
	shutdown chan interface{}
	chatID   int64
	log      custlogger.Logger
	bot      *tgbotapi.BotAPI
}

var telegramBot Bot

const maxChan = 10000

// ErrChanFull chan is full
var ErrChanFull = errors.New("chan downloaderDem is full")

// Start starts this worker
func Start(keyTelegram string, chatID int64, debug bool) {
	var err error

	telegramBot = Bot{
		data:     make(chan DataTelegramBot, maxChan),
		shutdown: make(chan interface{}),
		log:      custlogger.Logger{},
		chatID:   chatID,
	}
	telegramBot.log.SetupInstanceLoggger("telegram")
	telegramBot.bot, err = tgbotapi.NewBotAPI(keyTelegram)

	if err != nil {
		panic(err)
	}

	telegramBot.bot.Debug = debug
	go telegramBot.checkForEver()
}

// AddDataTelegramBot add data to be messaged
func AddDataTelegramBot(message string) error {
	if telegramBot.data == nil {
		return nil
	}
	select {
	case telegramBot.data <- DataTelegramBot{
		message: message,
	}:
	default:
		return ErrChanFull
	}

	return nil
}

func (tb *Bot) checkForEver() {
	defer func() {
		if r := recover(); r != nil {
			tb.log.Errorf(nil, "Recovered in TelegramBot.checkForEver %v %s", r, string(debug.Stack()))
			time.Sleep(time.Second * 10)
			go tb.checkForEver()
		}
	}()

	for {
		select {
		case <-tb.shutdown:
			return
		case data := <-tb.data:
			tb.log.Error(nil, "message to send to telegram :", data.message)
			msg := tgbotapi.NewMessage(tb.chatID, data.message)

			if _, err := tb.bot.Send(msg); err != nil {
				tb.log.Error(err, "error while trying to send message :", err)
				continue
			}
		}
	}
}
