package downloaderinfo

import (
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math/big"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"runtime/debug"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/thoas/go-funk"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/downloaderdem"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/telegram"
	"gorm.io/gorm"
)

// DataDownloaderInfo data send for download
type DataDownloaderInfo struct {
	userID     uint
	steamID    string
	shareCode  string
	steamIDKey string
}

// DownloaderInfo workers who download .info
type DownloaderInfo struct {
	data        chan DataDownloaderInfo
	shutdown    chan interface{}
	db          *gorm.DB
	TokenAPIKey string
	boilerBin   string
	PathStorage string
	log         custlogger.Logger
	DD          *downloaderdem.DownloaderDem
}

// ErrInvalideShareCode sharecode isn't good
var ErrInvalideShareCode = errors.New("sharecode isn't good")

// ErrInvalideInput token and/or steam id isn't good
var ErrInvalideInput = errors.New("token and/or steam id isn't good")

// ErrTooManyTryBoiler too many try for boiler
var ErrTooManyTryBoiler = errors.New("too many try for boiler")

// ErrChanFull chan is full
var ErrChanFull = errors.New("chan downloaderInfo is full")

const maxChan = 10000

// Start starts this worker
func Start(db *gorm.DB, dd *downloaderdem.DownloaderDem,
	logDownloaderInfo *custlogger.Logger,
	apiKey, boilerBin, PathStorage string) *DownloaderInfo {
	downloaderInfo := &DownloaderInfo{
		data:        make(chan DataDownloaderInfo, maxChan),
		shutdown:    make(chan interface{}),
		db:          db,
		TokenAPIKey: apiKey,
		log:         custlogger.Logger{},
		boilerBin:   boilerBin,
		PathStorage: PathStorage,
		DD:          dd,
	}

	downloaderInfo.log.SetupInstanceLoggger("downloaderInfo")

	go downloaderInfo.checkForEver()
	go downloaderInfo.addCheckForEver(time.Hour)

	return downloaderInfo
}

// CheckIfTokenAreValids returns true if the token are valids
func (di *DownloaderInfo) CheckIfTokenAreValids(steamID, steamIDKey, sharecode string) bool {
	_, _, err := di.checkNextShareCode(steamID, steamIDKey, sharecode)

	return err == nil
}

// CheckIfNewDemo checks if their is a new ddemo
func (di *DownloaderInfo) CheckIfNewDemo(user model.User) (bool, string) {
	if user.AccessTokenHistory == nil || *user.AccessTokenHistory == "" ||
		user.ShareCode == nil || *user.ShareCode == "" ||
		user.SteamID == nil || *user.SteamID == "" {
		return false, ""
	}

	if user.ID == nil {
		di.log.Error(nil, "should not be appening, user.ID is nil")
		return false, ""
	}

	isNext, nextShareCode, err := di.checkNextShareCode(*user.SteamID, *user.AccessTokenHistory, *user.ShareCode)
	if err != nil {
		di.log.Error(err, "error while trying to check if there is a new demo", err)
		return false, ""
	}

	if isNext {
		if err := di.AddDataDownloaderInfo(*user.ID, *user.SteamID, *user.AccessTokenHistory, *user.ShareCode); err != nil {
			di.log.Error(err, "error while addind data to downloader info after checking", err)
			return false, ""
		}
	}
	return isNext, nextShareCode
}

func (di *DownloaderInfo) checkNextShareCode(steamID, steamIDKey, shareCode string) (bool, string, error) {
	var data struct {
		Key        string
		SteamID    string
		SteamIDKey string
		KnownCode  string
	}

	data.Key = di.TokenAPIKey
	data.SteamID = steamID
	data.SteamIDKey = steamIDKey
	data.KnownCode = shareCode

	req, err := http.NewRequest("GET", "https://api.steampowered.com/ICSGOPlayers_730/GetNextMatchSharingCode/v1", nil)
	if err != nil {
		di.log.Error(err, "error while setuping request for steam api for next share code", err)

		return false, "", err
	}

	q := req.URL.Query()

	q.Add("key", data.Key)
	q.Add("steamid", data.SteamID)
	q.Add("steamidkey", data.SteamIDKey)
	q.Add("knowncode", data.KnownCode)
	req.URL.RawQuery = q.Encode()

	client := http.Client{}

	dataGot, err := client.Do(req)
	if err != nil {
		di.log.Error(err, "error while requesting steam api for next share code", err)

		return false, "", err
	}

	defer func() {
		if err := dataGot.Body.Close(); err != nil {
			di.log.Error(err, "error while closing body response", err)
		}
	}()

	bodyBytes, err := ioutil.ReadAll(dataGot.Body)
	if err != nil {
		di.log.Error(err, "error while reading request response", err)

		return false, "", err
	}

	if dataGot.StatusCode == http.StatusOK {
		nextShareCode := struct {
			Result struct {
				NextCode string `json:"nextcode"`
			} `json:"result"`
		}{}

		if err := json.Unmarshal(bodyBytes, &nextShareCode); err != nil {
			return false, "", err
		}

		return true, nextShareCode.Result.NextCode, nil
	}

	if dataGot.StatusCode == http.StatusAccepted {
		return false, "", nil
	}

	if dataGot.StatusCode == http.StatusRequestEntityTooLarge {
		return false, "", ErrInvalideShareCode
	}

	return false, "", ErrInvalideInput
}

func (di *DownloaderInfo) shareCodeToID(shareCode string) (string, string, string) {
	dico := "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefhijkmnopqrstuvwxyz23456789"

	shareCode = strings.ReplaceAll(shareCode[5:], "-", "")

	bigID := big.NewInt(0)
	lenDico := big.NewInt(int64(len(dico)))

	for _, c := range funk.ReverseString(shareCode) {
		cBig := big.NewInt(int64(funk.IndexOf(dico, string(c))))
		bigID = bigID.Add(bigID.Mul(bigID, lenDico), cBig)
	}

	all := bigID.Bytes()
	if len(all) != 2*8+2 {
		all = append([]byte{0}, all...)
	}

	matchIDBytes := strconv.FormatUint(binary.LittleEndian.Uint64(all[0:8]), 10)
	outcomeIDBytes := strconv.FormatUint(binary.LittleEndian.Uint64(all[8:16]), 10)
	tokenIDByte := strconv.FormatUint(uint64(binary.LittleEndian.Uint16(all[16:18])), 10)

	return matchIDBytes, outcomeIDBytes, tokenIDByte
}

// AddDataDownloaderInfo add data to be downloaded
func (di *DownloaderInfo) AddDataDownloaderInfo(userID uint, steamID, steamIDKey, shareCode string) error {
	select {
	case di.data <- DataDownloaderInfo{
		userID:     userID,
		steamID:    steamID,
		steamIDKey: steamIDKey,
		shareCode:  shareCode,
	}:
	default:
		return ErrChanFull
	}

	return nil
}

// Shutdown shutdown analyzer
func (di *DownloaderInfo) Shutdown() {
	di.shutdown <- struct{}{}
	di.shutdown <- struct{}{}
}

// downloadInfoBoiler download info with boiler
func (di *DownloaderInfo) downloadInfoBoiler(path, matchID, outcomeID, tokenID string, nbrTry int) error {
	cmd := exec.Command(di.boilerBin, path, matchID, outcomeID, tokenID)
	di.log.Info("exec: `" + strings.Join(cmd.Args[:], " ") + "`")

	waitCmd := sync.WaitGroup{}
	waitCmd.Add(1)

	done := make(chan error)

	go func() {
		var (
			output []byte
			errCmd error
		)

		if output, errCmd = cmd.Output(); errCmd != nil {
			errtoLog := fmt.Sprintf("error with boiler : %s:%s", string(output), errCmd)

			if errCmd.Error() != "signal: interrupt" {
				if errTelegram := telegram.AddDataTelegramBot(errtoLog); errTelegram != nil {
					di.log.Error(errTelegram, "error while trying to send message to telegram", errTelegram)
				}
			}

			di.log.Error(errCmd, errtoLog)
		}
		done <- errCmd
	}()

	select {
	case <-time.After(time.Minute):
		_ = cmd.Process.Kill()

		if nbrTry > 5 {
			return ErrTooManyTryBoiler
		}

		return di.downloadInfoBoiler(path, matchID, outcomeID, tokenID, nbrTry+1)
	case err := <-done:
		return err
	}
}

// CheckForEver checks if their is a new demo info to download
func (di *DownloaderInfo) checkForEver() {
	defer func() {
		if r := recover(); r != nil {
			di.log.Errorf(nil, "Recovered in DownloaderInfo.checkForEver %v %s", r, string(debug.Stack()))
			time.Sleep(time.Second * 10)
			go di.checkForEver()
		}
	}()

	for {
		select {
		case <-di.shutdown:
			di.log.Info("downloaderinfo shutdown")
			return
		default:
		}
		select {
		case <-di.shutdown:
			di.log.Info("downloaderinfo shutdown")
			return
		case data := <-di.data:
			func() {
				matchID, outcomeID, tokenID := di.shareCodeToID(data.shareCode)

				userDAO := dao.NewUserDAO(di.db)
				user := model.User{SteamID: &data.steamID}

				next, nextShareCode, err := di.checkNextShareCode(data.steamID, data.steamIDKey, data.shareCode)
				if err != nil {
					di.log.Error(err, "error while trying to get next share code :", err)

					if err := userDAO.SetAPIValveWorking(user, false); err != nil {
						di.log.Error(err, "error while trying to change to false the var. APIValve of the user", err)
						return
					}
				} else if err := userDAO.SetAPIValveWorking(user, true); err != nil {
					di.log.Error(err, "error while trying to change to true the var. APIValve of the user", err)
					return
				}

				fileInfoPath := "./tmpDemoInfo" + strconv.FormatInt(time.Now().UnixNano(), 10)
				if err := di.downloadInfoBoiler(fileInfoPath, matchID, outcomeID, tokenID, 0); err != nil {
					di.log.Error(err, "error while trying to download demo info through boiler", err)
					return
				}

				defer func() {
					if err := os.Remove(fileInfoPath); err != nil {
						di.log.Error(err, "error while trying to remove demo info file", err)
						return
					}
				}()

				demo, err := model.ParseDemoInfoFromFile(fileInfoPath)
				if err != nil {
					di.log.Error(err, "error while parsing demo info", err)
					return
				}

				demoDAO := dao.NewDemoDAO(di.db, &di.log)
				exists, err := demoDAO.Exists(model.Demo{ID: demo.ID})
				if err != nil {
					di.log.Error(err, "error while trying to know if demo exists or not", err)
					return
				}

				if !exists {
					name := fmt.Sprintf("match730_%s", filepath.Base(*demo.ReplayURL))
					path := fmt.Sprintf("%s/%s", di.PathStorage, name)
					demo.Path = &path
					demo.Name = &name
					demo.Size = helpers.Int64Ptr(0)
					demo.Sharecode = &data.shareCode
					demo.Source = model.Valve
					demo.GameMode = model.Competitive
					demo.UserID = &data.userID
					demo.Downloaded = helpers.BoolPtr(false)
					demo.VersionAnalyzer = helpers.Int64Ptr(0)

					if err := demoDAO.Create(demo, nil); err != nil {
						di.log.Error(err, "error while creating demo", err)
						return
					}
					if err := di.DD.AddDataDownloaderDem(*demo); err != nil {
						di.log.Error(err, "error while adding data to downloader demo", err)
						return
					}
				}

				if next {
					if err := userDAO.UpdateShareCode(&model.User{ID: &data.userID, ShareCode: &nextShareCode}); err != nil {
						di.log.Error(err, "error while updating sharecode", err)
						return
					}
					if err := di.AddDataDownloaderInfo(data.userID, data.steamID, data.steamIDKey, nextShareCode); err != nil {
						di.log.Error(err, "error while trying to add data to downloader info", err)
						return
					}
				}
			}()
		}
	}
}

// addCheckForEver add users to be checked
func (di *DownloaderInfo) addCheckForEver(waitInMinutes time.Duration) {
	defer func() {
		if r := recover(); r != nil {
			di.log.Errorf(nil, "Recovered in DownloaderInfo.addCheckForEver %v %s", r, string(debug.Stack()))
			time.Sleep(time.Second * 10)
			go di.addCheckForEver(waitInMinutes)
		}
	}()

	userDao := dao.NewUserDAO(di.db)

	for {
		func() {
			users := []*model.User{}
			if err := userDao.GetUsersWhoEnabledDownloadInfo(&users); err != nil {
				di.log.Error(err, "error while trying to get users with all informations to download their demos", err)
				return
			}

			di.log.Tracef("found %d users", len(users))

			for _, user := range users {
				if err := di.AddDataDownloaderInfo(*user.ID, *user.SteamID, *user.AccessTokenHistory, *user.ShareCode); err != nil {
					di.log.Error(err, "error while addind data to downloader info", err)
					return
				}
			}
		}()

		waitTimer := make(chan interface{})

		go func() {
			time.Sleep(waitInMinutes)
			waitTimer <- struct{}{}
		}()

		select {
		case <-di.shutdown:
			di.log.Info("downloaderinfo adder shutdown")
			return
		case <-waitTimer:
		}
	}
}
