package downloaderinfo

import (
	"math/rand"
	"os"
	"strconv"
	"testing"
	"time"

	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/mock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
	"gorm.io/gorm"
)

var logTest *custlogger.Logger

var _testDBGlobal *gorm.DB

func TestMain(m *testing.M) {
	rand.Seed(time.Now().Unix())

	logTest = &custlogger.Logger{}
	logTest.SetupInstanceLogggerTest()

	_testDBGlobal = store.InitDB(true, store.SchemaTestAnalyzerDownload, logTest)

	os.Exit(testMainWraper(m))
}

func testMainWraper(m *testing.M) int {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	mock.NextMatchSharingCode()

	return m.Run()
}

func TestCheckIfTokenAreValids(t *testing.T) {
	di := &DownloaderInfo{
		TokenAPIKey: mock.APIkey,
	}

	userPicked := mock.UsersMocked[rand.Intn(mock.NbrUser)]

	assert.True(t, di.CheckIfTokenAreValids(*userPicked.SteamID,
		*userPicked.SteamIDKey,
		userPicked.CurrentMatchSharingCode.KnownCode))
}

func TestShareCodeToID(t *testing.T) {
	di := &DownloaderInfo{
		TokenAPIKey: mock.APIkey,
	}
	matchIDBytes, outcomeIDBytes, tokenIDByte := di.shareCodeToID("CSGO-3nlu1-Qnzkq-nqkdH-mWT4q-ONwmn")
	assert.Equal(t, "1245127347456508416", matchIDBytes)
	assert.Equal(t, "10570892690995028261", outcomeIDBytes)
	assert.Equal(t, "10393", tokenIDByte)
}

func TestAddDataDownloaderInfo(t *testing.T) {
	di := &DownloaderInfo{
		data: make(chan DataDownloaderInfo, maxChan),
	}

	userPicked := mock.UsersMocked[rand.Intn(mock.NbrUser)]
	userSent := model.User{
		ID:                 helpers.RandUIntPSQL(),
		SteamID:            userPicked.SteamID,
		AccessTokenHistory: userPicked.Key,
		ShareCode:          &userPicked.CurrentMatchSharingCode.KnownCode,
	}
	assert.NoError(t, di.AddDataDownloaderInfo(*userSent.ID,
		*userSent.SteamID, *userSent.AccessTokenHistory,
		*userSent.ShareCode))

	dataFromChan := <-di.data

	assert.Equal(t, *userSent.ID, dataFromChan.userID)
	assert.Equal(t, *userSent.SteamID, dataFromChan.steamID)
	assert.Equal(t, *userSent.AccessTokenHistory, dataFromChan.steamIDKey)
	assert.Equal(t, *userSent.ShareCode, dataFromChan.shareCode)
}

func TestDownloadInfoBoiler(t *testing.T) {
	di := &DownloaderInfo{
		data: make(chan DataDownloaderInfo, maxChan),
	}
	// Can't test more because steam must be launched

	// boiler isn't found
	assert.Error(t, di.downloadInfoBoiler("", "", "", "", 0))
}

// @randomFailOnGitlab
func TestAddCheckForEver(t *testing.T) {
	if os.Getenv("GITLAB") != "" {
		t.Skip()
	}
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	userDAO := dao.NewUserDAO(localDB)

	di := &DownloaderInfo{
		data:     make(chan DataDownloaderInfo, maxChan),
		shutdown: make(chan interface{}),
		log:      *logTest,
		db:       localDB,
	}

	numGoodUser := 0
	for numUser := 0; !(numUser >= 10 && numGoodUser > 7); numUser++ {
		goodUser := true
		user := model.GenerateRandomUser(true)
		switch rand.Intn(2) {
		case 0:
			goodUser = false
			switch rand.Intn(2) {
			case 0:
				user.AccessTokenHistory = nil
			case 1:
				user.AccessTokenHistory = helpers.StrPtr("")
			}
		case 1:
			user.AccessTokenHistory = helpers.StrPtr(helpers.RandomString(10))
		}
		switch rand.Intn(2) {
		case 0:
			goodUser = false
			switch rand.Intn(2) {
			case 0:
				user.ShareCode = nil
			case 1:
				user.ShareCode = helpers.StrPtr("")
			}
		case 1:
			user.ShareCode = helpers.StrPtr(helpers.RandomString(10))
		}
		switch rand.Intn(2) {
		case 0:
			goodUser = false
			switch rand.Intn(2) {
			case 0:
				user.SteamID = nil
			case 1:
				user.SteamID = helpers.StrPtr("")
			}
		case 1:
			user.SteamID = helpers.StrPtr(strconv.FormatInt(76561198018574562, 10))
		}

		if goodUser {
			numGoodUser++
		}

		require.NoError(t, userDAO.Create(user))
	}

	go func() {
		di.shutdown <- struct{}{}
	}()
	go di.addCheckForEver(time.Minute * 10)

	numGoodUserGot := 0

	<-di.data
	numGoodUserGot++

	func() {
		for {
			select {
			case <-di.data:
				numGoodUserGot++
			default:
				return
			}
		}
	}()
	require.NotEqual(t, 0, numGoodUser)
	assert.True(t, numGoodUserGot%numGoodUser == 0, "%d:%d", numGoodUserGot, numGoodUser)
}

func TestStartsShutDown(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	logTest := &custlogger.Logger{}
	logTest.SetupInstanceLogggerTest()

	di := Start(localDB, nil, logTest, "apiKEY", "", "./storage")
	di.Shutdown()
	assert.Equal(t, "apiKEY", di.TokenAPIKey)
	assert.Equal(t, "", di.boilerBin)
	assert.NotNil(t, di.data)
	assert.NotNil(t, di.shutdown)
	assert.NotNil(t, di.db)
	assert.NotNil(t, di.log)
}

// @randomFailOnGitlab
func TestCheckIfNewDemo(t *testing.T) {
	if os.Getenv("GITLAB") != "" {
		t.Skip()
	}
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	logTest := &custlogger.Logger{}
	logTest.SetupInstanceLogggerTest()

	di := &DownloaderInfo{
		TokenAPIKey: mock.APIkey,
		data:        make(chan DataDownloaderInfo, maxChan),
		shutdown:    make(chan interface{}),
		log:         *logTest,
		db:          localDB,
	}

	userPicked := mock.UsersMocked[rand.Intn(mock.NbrUser)]
	userToCheck := model.User{
		ID:                 helpers.RandUIntPSQL(),
		AccessTokenHistory: userPicked.SteamIDKey,
		ShareCode:          &userPicked.CurrentMatchSharingCode.KnownCode,
		SteamID:            userPicked.SteamID,
	}
	di.CheckIfNewDemo(userToCheck)

	found := false
	select {
	case <-di.data:
		found = true
	default:
	}
	assert.True(t, found)

	userToCheck = model.User{
		ID:        helpers.RandUIntPSQL(),
		ShareCode: &userPicked.CurrentMatchSharingCode.KnownCode,
		SteamID:   userPicked.SteamID,
	}
	di.CheckIfNewDemo(userToCheck)

	notFound := false
	select {
	case <-di.data:
	default:
		notFound = true
	}
	assert.True(t, notFound)

	userToCheck = model.User{
		ShareCode: &userPicked.CurrentMatchSharingCode.KnownCode,
		SteamID:   userPicked.SteamID,
	}
	di.CheckIfNewDemo(userToCheck)

	notFound = false
	select {
	case <-di.data:
	default:
		notFound = true
	}
	assert.True(t, notFound)

	userToCheck = model.User{
		ID:                 helpers.RandUIntPSQL(),
		AccessTokenHistory: helpers.StrPtr("SDFG-RGHJF-YUTR"),
		ShareCode:          &userPicked.CurrentMatchSharingCode.KnownCode,
		SteamID:            userPicked.SteamID,
	}
	di.CheckIfNewDemo(userToCheck)

	notFound = false
	select {
	case <-di.data:
	default:
		notFound = true
	}
	assert.True(t, notFound)
}

func TestCheckForEver(t *testing.T) {
	localDB := _testDBGlobal.Begin()
	defer localDB.Rollback()

	logTest := &custlogger.Logger{}
	logTest.SetupInstanceLogggerTest()

	userDAO := dao.NewUserDAO(localDB)

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	di := &DownloaderInfo{
		data:        make(chan DataDownloaderInfo, maxChan),
		shutdown:    make(chan interface{}),
		log:         *logTest,
		db:          localDB,
		boilerBin:   "../../../bin-boiler-mock/boiler-writter", // todo mock this shit
		TokenAPIKey: mock.APIkey,
	}
	go di.checkForEver()

	userPicked := mock.UsersMocked[rand.Intn(mock.NbrUser)]
	user := model.GenerateRandomUser(true)
	user.SteamID = helpers.StrPtr(strconv.FormatInt(76561198018574562, 10))
	require.NoError(t, userDAO.Create(user))

	assert.NoError(t, di.AddDataDownloaderInfo(*user.ID,
		*userPicked.SteamID, *userPicked.SteamIDKey,
		userPicked.CurrentMatchSharingCode.KnownCode))
	di.shutdown <- struct{}{}
}
