package downloaderdem

import (
	"bufio"
	"bytes"
	"compress/bzip2"
	"compress/gzip"
	"crypto/sha256"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"runtime/debug"
	"time"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/dao"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/workers/analyzer"
	"gorm.io/gorm"
)

// DataDownloaderDem data send for download
type DataDownloaderDem struct {
	demo model.Demo
}

// DownloaderDem workers who download .dem
type DownloaderDem struct {
	data     chan DataDownloaderDem
	shutdown chan interface{}
	db       *gorm.DB
	log      *custlogger.Logger
	Anal     *analyzer.Analyzer
}

const maxChan = 10000

// ErrChanFull chan is full
var ErrChanFull = errors.New("chan downloaderDem is full")

// ErrBadHTPPStatusDownload bad http status download demo
var ErrBadHTPPStatusDownload = errors.New("bad http status download demo")

// ErrDemoExtensionNotTakenIntoAccount extension of the demo is not taken into account
var ErrDemoExtensionNotTakenIntoAccount = errors.New("extension of the demo is not taken into account")

// ErrCouldNotShutdown could not shutdown downloader demo
var ErrCouldNotShutdown = errors.New("could not shutdown downloader demo")

// Start starts this worker
func Start(db *gorm.DB, anal *analyzer.Analyzer, logDownloaderDem *custlogger.Logger) (*DownloaderDem, error) {
	dd := &DownloaderDem{
		data:     make(chan DataDownloaderDem, maxChan),
		shutdown: make(chan interface{}),
		log:      logDownloaderDem,
		Anal:     anal,
		db:       db,
	}
	go dd.CheckForEver()

	if err := dd.CheckNotDownloaded(); err != nil {
		return nil, err
	}

	go dd.CheckForEverNotDownloaded()

	return dd, nil
}

// CheckForEverNotDownloaded at the start of the downloaderDem, check if there are demos to download
func (dd *DownloaderDem) CheckForEverNotDownloaded() {
	defer func() {
		if r := recover(); r != nil {
			dd.log.Errorf(nil, "Recovered in DownloaderDem.CheckForEver %v %s", r, string(debug.Stack()))
			time.Sleep(time.Second * 10)
			go dd.CheckForEverNotDownloaded()
		}
	}()

	for {
		// to avoid 1/2 chance to analyze even if we want to shut down
		select {
		case <-dd.shutdown:
			dd.log.Info("DownloaderDem shutdown")
			return
		default:
		}
		select {
		case <-dd.shutdown:
			dd.log.Info("DownloaderDem shutdown")
			return
		case <-time.After(time.Hour):
			if err := dd.CheckNotDownloaded(); err != nil {
				dd.log.Error(err, "Can't get all not downloaded dem:", err)
			}
		}
	}
}

// CheckNotDownloaded check if there are demos to download
func (dd *DownloaderDem) CheckNotDownloaded() error {
	demoDAO := dao.NewDemoDAO(dd.db, dd.log)

	demosNotDownloaded, err := demoDAO.GetAllNotDownloaded(&model.SafeSources)
	if err != nil {
		dd.log.Error(err, "Can't get all not downloaded dem:", err)
		return err
	}

	for _, demo := range demosNotDownloaded {
		if err := dd.AddDataDownloaderDem(demo); err != nil {
			dd.log.Error(err, "Can't add to downloaded dem:", err)
			return err
		}
	}

	return nil
}

// AddDataDownloaderDem add data to be downloaded
func (dd *DownloaderDem) AddDataDownloaderDem(demo model.Demo) error {
	select {
	case dd.data <- DataDownloaderDem{
		demo: demo,
	}:
	default:
		return ErrChanFull
	}

	return nil
}

// Shutdown shutdown downloaderdem
func (dd *DownloaderDem) Shutdown() error {
	shutdownDD := make(chan interface{})

	go func() {
		dd.shutdown <- struct{}{}
		dd.shutdown <- struct{}{}
		shutdownDD <- struct{}{}
	}()

	select {
	case <-shutdownDD:
	case <-time.After(time.Minute):
		return ErrCouldNotShutdown
	}

	shutdownA := make(chan interface{})

	go func() {
		dd.Anal.Shutdown()
		shutdownA <- struct{}{}
	}()

	select {
	case <-shutdownA:
	case <-time.After(time.Minute):
		return analyzer.ErrCouldNotShutdown
	}

	return nil
}

// CheckForEver checks if their is a new demo to download
func (dd *DownloaderDem) CheckForEver() {
	defer func() {
		if r := recover(); r != nil {
			dd.log.Errorf(nil, "Recovered in DownloaderDem.checkForEver %v %s", r, string(debug.Stack()))
			time.Sleep(time.Second * 10)
			go dd.CheckForEver()
		}
	}()

	for {
		select {
		case <-dd.shutdown:
			dd.log.Info("downloaderdem shutdown")
			return
		default:
		}
		select {
		case <-dd.shutdown:
			dd.log.Info("downloaderdem shutdown")
			return
		case data := <-dd.data:
			if err := DownloadDemo(dd.db, dd.log, data.demo); err != nil {
				dd.log.Error(err, "error while trying to download demo : ", err)

				if data.demo.Date != nil && time.Since(*data.demo.Date) > time.Hour*24*31 {
					demoDAO := dao.NewDemoDAO(dd.db, dd.log)
					if err := demoDAO.Delete(data.demo); err != nil {
						dd.log.Error(err, "error while trying to delete undownloaded demo : ", err)
					}
				}
				continue
			}

			if err := dd.Anal.AddDataAnalyzer([]int64{*data.demo.ID}); err != nil {
				dd.log.Error(err, "error while trying to add demo to be analyzed : ", err)
				continue
			}
		}
	}
}

// DownloadDemo download a the demo in parameters
func DownloadDemo(db *gorm.DB, log *custlogger.Logger, demo model.Demo) error {
	resp, err := http.Get(*demo.ReplayURL)
	if err != nil {
		log.Error(err, "error while requesting zipped demo : ", err)
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return ErrBadHTPPStatusDownload
	}

	zippedDemo, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Error(err, "error while  reading ziped demo : ", err)
		return err
	}

	if err := ioutil.WriteFile(*demo.Path, zippedDemo, 0664); err != nil {
		log.Error(err, "error while saving unziped file : ", err)
		return err
	}

	var demoUnzippedReader io.Reader
	switch filepath.Ext(*demo.Name) {
	case ".bz2":
		demoUnzippedReader = bzip2.NewReader(bytes.NewReader(zippedDemo))
	case ".gz":
		demoZippedReader, err := gzip.NewReader(bytes.NewReader(zippedDemo))
		if err != nil {
			return err
		}

		t, err := ioutil.ReadAll(demoZippedReader)
		if err != nil {
			return err
		}
		demoUnzippedReader = bytes.NewReader(t)
	case ".dem":
		demoUnzippedReader = bufio.NewReader(bytes.NewReader(zippedDemo))
	default:
		return ErrDemoExtensionNotTakenIntoAccount
	}

	demoUnzippedBytes, err := ioutil.ReadAll(demoUnzippedReader)
	if err != nil {
		return err
	}

	hasher := sha256.New()
	if _, err := hasher.Write(demoUnzippedBytes); err != nil {
		log.Error(err, "error while trying to write in hash : ", err)
		return err
	}

	demo.HashFile = hasher.Sum(nil)
	demoDAO := dao.NewDemoDAO(db, log)
	exists, demoFound, err := demoDAO.HashExists(demo)
	if err != nil {
		log.Error(err, "error while trying to check if demo exists : ", err)
		return err
	}

	if exists {
		if err := demoDAO.Delete(demo); err != nil {
			log.Error(err, "error while trying to set replayURL : ", err)
			return err
		}

		if err := demoDAO.SetSource(*demoFound.ID, model.Valve); err != nil {
			log.Error(err, "error while trying to set source : ", err)
		}
		if err := demoDAO.SetShareCode(*demoFound.ID, demo.Sharecode); err != nil {
			log.Error(err, "error while trying to set sharecode : ", err)
		}
		if err := demoDAO.SetReplayURL(*demoFound.ID, demo.ReplayURL); err != nil {
			log.Error(err, "error while trying to set replayURL : ", err)
		}

		return nil
	}

	if err := demoDAO.SetHash(*demo.ID, demo.HashFile); err != nil {
		log.Error(err, "error while trying to set hash : ", err)
		return err
	}

	demo.Size = helpers.Int64Ptr(int64(len(zippedDemo)))
	if err := demoDAO.SetSize(*demo.ID, *demo.Size); err != nil {
		log.Error(err, "error while trying to set size : ", err)
		return err
	}

	if err := demoDAO.SetDownloaded(*demo.ID); err != nil {
		log.Error(err, "error while trying to set downloaded : ", err)
		return err
	}

	return nil
}
