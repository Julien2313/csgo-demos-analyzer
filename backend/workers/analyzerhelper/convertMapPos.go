package analyzerhelper

import (
	"github.com/golang/geo/r2"
	"github.com/golang/geo/r3"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

// Deltas positions of the bounds of the maps
var Deltas = map[string][2]r3.Vector{
	"inferno":  {{X: -1756.0, Y: -788.0, Z: -0.0}, {X: 2672.0, Y: 3592.0, Z: 1.0}},
	"dust2":    {{X: -2204.0, Y: -1164.0, Z: -0.0}, {X: 1788.0, Y: 3118.0, Z: 1.0}},
	"train":    {{X: -2246.0, Y: -1785.0, Z: -0.0}, {X: 2030.0, Y: 1776.0, Z: 1.0}},
	"mirage":   {{X: -2656.0, Y: -2603.0, Z: -0.0}, {X: 1456.0, Y: 864.0, Z: 1.0}},
	"nuke":     {{X: -2992.0, Y: -2480.0, Z: -0.0}, {X: 3498.0, Y: 935.0, Z: 1.0}},
	"overpass": {{X: -3960.0, Y: -3496.0, Z: -0.0}, {X: 16.0, Y: 1672.0, Z: 1.0}},
	"vertigo":  {{X: -2634.0, Y: -1511.0, Z: -0.0}, {X: -33.0, Y: 1098.0, Z: 1.0}},
	"cache":    {{X: -1795.0, Y: -1465.0, Z: -0.0}, {X: 3307.0, Y: 2313.0, Z: 1.0}},
	"cbble":    {{X: -0.0, Y: -0.0, Z: -0.0}, {X: 1.0, Y: 1.0, Z: 1.0}},
	"canals":   {{X: -0.0, Y: -0.0, Z: -0.0}, {X: 1.0, Y: 1.0, Z: 1.0}},
	"zoo":      {{X: -0.0, Y: -0.0, Z: -0.0}, {X: 1.0, Y: 1.0, Z: 1.0}},
	"abbey":    {{X: -0.0, Y: -0.0, Z: -0.0}, {X: 1.0, Y: 1.0, Z: 1.0}},
	"biome":    {{X: -0.0, Y: -0.0, Z: -0.0}, {X: 1.0, Y: 1.0, Z: 1.0}},
	"militia":  {{X: -0.0, Y: -0.0, Z: -0.0}, {X: 1.0, Y: 1.0, Z: 1.0}},
	"agency":   {{X: -0.0, Y: -0.0, Z: -0.0}, {X: 1.0, Y: 1.0, Z: 1.0}},
	"office":   {{X: -0.0, Y: -0.0, Z: -0.0}, {X: 1.0, Y: 1.0, Z: 1.0}},
	"italy":    {{X: -0.0, Y: -0.0, Z: -0.0}, {X: 1.0, Y: 1.0, Z: 1.0}},
	"anubis":   {{X: -1972.0, Y: -1804.0, Z: -0.0}, {X: 1804.0, Y: 3162.0, Z: 1.0}},
	"chlorine": {{X: -0.0, Y: -0.0, Z: -0.0}, {X: 1.0, Y: 1.0, Z: 1.0}},
	"breach":   {{X: -0.0, Y: -0.0, Z: -0.0}, {X: 1.0, Y: 1.0, Z: 1.0}},
	"workout":  {{X: -0.0, Y: -0.0, Z: -0.0}, {X: 1.0, Y: 1.0, Z: 1.0}},
	"swamp":    {{X: -1600.0, Y: -2098.0, Z: -0.0}, {X: 3252.0, Y: 3021.0, Z: 1.0}},
	"mutiny":   {{X: -7482.0, Y: -6200.0, Z: -0.0}, {X: -4083.0, Y: -1467.0, Z: 1.0}},
	"assault":  {{X: -0.0, Y: -0.0, Z: -0.0}, {X: 1.0, Y: 1.0, Z: 1.0}},
	"engage":   {{X: -2466.0, Y: -2781.0, Z: -0.0}, {X: 1423.0, Y: 2826.0, Z: 1.0}},
	"apollo":   {{X: -1374.0, Y: -6605.0, Z: -0.0}, {X: 2536.0, Y: -1760.0, Z: 1.0}},
	"ancient":  {{X: -2269.0, Y: -2500.0, Z: -0.0}, {X: 1396.0, Y: 1516.0, Z: 1.0}},
	"mocha":    {{X: 450.0, Y: -1333.0, Z: -0.0}, {X: 3604.0, Y: 2694.0, Z: 1.0}},
	"grind":    {{X: -3328.0, Y: -1679.0, Z: -0.0}, {X: 1100.0, Y: 2544.0, Z: 1.0}},
}

// ConvertMapPos convert the position of the map in %
func ConvertMapPos(mapName string, pos r3.Vector) r3.Vector {
	d, exists := Deltas[mapName]
	if !exists {
		return r3.Vector{
			X: 0,
			Y: 0,
			Z: 0,
		}
	}
	return r3.Vector{
		X: helpers.Round((pos.X-d[0].X)/(d[1].X-d[0].X), 5),
		Y: helpers.Round((pos.Y-d[0].Y)/(d[1].Y-d[0].Y), 5),
		Z: helpers.Round((pos.Z-d[0].Z)/(d[1].Z-d[0].Z), 5),
	}
}

// ConvertMapPos2D convert the position of the map in %
func ConvertMapPos2D(mapName string, pos r2.Point) r2.Point {
	d, exists := Deltas[mapName]
	if !exists {
		return r2.Point{
			X: 0,
			Y: 0,
		}
	}
	return r2.Point{
		X: helpers.Round((pos.X-d[0].X)/(d[1].X-d[0].X), 5),
		Y: helpers.Round((pos.Y-d[0].Y)/(d[1].Y-d[0].Y), 5),
	}
}
