package helpers

import (
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gopkg.in/h2non/gock.v1"
)

func TestSendRequest(t *testing.T) {
	payload := "payload test"
	require.NoError(t, gock.New("66.66.66.66:6666").
		Post("/test/").
		MatchType("json").
		Reply(200).
		JSON(payload).
		Error)

	resp, err := SendRequest(nil, "POST", "http://66.66.66.66:6666/test/")
	require.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	require.NoError(t, err)
	assert.Equal(t, payload, string(bodyBytes))

	_, err = SendRequest(nil, "GET", "http://66.66.66.66:6666/test/")
	assert.Error(t, err)
}
