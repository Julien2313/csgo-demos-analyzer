package helpers

import (
	"bufio"
	"bytes"
	"compress/bzip2"
	"compress/gzip"
	"errors"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

// ErrDemoExtensionNotTakenIntoAccount extension of the demo is not taken into account
var ErrDemoExtensionNotTakenIntoAccount = errors.New("extension of the demo is not taken into account")

// ErrFileNil file is nil
var ErrFileNil = errors.New("file is nil")

// DemoFileToByte take a demo file in parameter to open it and return it in a decrypted slice of bytes
func DemoFileToByte(fileDemo *os.File) ([]byte, error) {
	if fileDemo == nil {
		return nil, ErrFileNil
	}

	if !strings.Contains(fileDemo.Name(), ".dem") {
		return nil, ErrDemoExtensionNotTakenIntoAccount
	}

	demoUnzippedReader, err := DemoFileToReader(fileDemo)
	if err != nil {
		return nil, err
	}

	demoUnzippedBytes, err := ioutil.ReadAll(demoUnzippedReader)
	if err != nil {
		return nil, err
	}

	return demoUnzippedBytes, nil
}

// DemoFileToReader take a demo file in parameter to open it and return it in a decrypted reader
func DemoFileToReader(fileDemo *os.File) (io.Reader, error) {
	if fileDemo == nil {
		return nil, ErrFileNil
	}

	if !strings.Contains(fileDemo.Name(), ".dem") {
		return nil, ErrDemoExtensionNotTakenIntoAccount
	}

	var demoUnzippedReader io.Reader
	switch filepath.Ext(fileDemo.Name()) {
	case ".bz2":
		demoUnzippedReader = bzip2.NewReader(bufio.NewReader(fileDemo))
	case ".gz":
		demoZippedReader, err := gzip.NewReader(bufio.NewReader(fileDemo))
		if err != nil {
			return nil, err
		}

		t, err := ioutil.ReadAll(demoZippedReader)
		if err != nil {
			return nil, err
		}

		demoUnzippedReader = bytes.NewReader(t)
	case ".dem":
		demoUnzippedReader = bufio.NewReader(bufio.NewReader(fileDemo))
	default:
		return nil, ErrDemoExtensionNotTakenIntoAccount
	}

	return demoUnzippedReader, nil
}
