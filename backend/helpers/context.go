package helpers

import (
	"errors"
	"strconv"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

// ErrSessionNotLinked session isn't linked to any user connected
var ErrSessionNotLinked = errors.New("session isn't linked to any user connected")

// GetUserIDFromContext Extrait l'id utilisateur du gin.Context
func GetUserIDFromContext(c *gin.Context) (*uint, error) {
	s := sessions.Default(c)
	uidC := s.Get(Userkey)
	if uidC == nil {
		return nil, ErrSessionNotLinked
	}

	var (
		uid uint64
		err error
	)

	if uid, err = strconv.ParseUint(uidC.(string), 10, 64); err != nil {
		return nil, err
	}

	return UintPtr(uint(uid)), nil
}
