package helpers

import (
	"errors"
	"fmt"
	"io"
	"runtime/debug"

	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs"
	"github.com/markus-wa/demoinfocs-golang/v2/pkg/demoinfocs/events"
)

// NewDemoInfoParserWithoutPanic without this, if demostream isn't a demo, it panics
func NewDemoInfoParserWithoutPanic(demostream io.Reader, err *error) demoinfocs.Parser {
	defer func() {
		if r := recover(); r != nil {
			*err = errors.New("error while creating parser :" + fmt.Sprint(r) + fmt.Sprint(string(debug.Stack())))
		}
	}()
	return demoinfocs.NewParser(demostream)
}

// ParseHeaderWithoutPanic without this, if demostream isn't a demo, it panics
func ParseHeaderWithoutPanic(demoData demoinfocs.Parser, err *error) {
	defer func() {
		if r := recover(); r != nil {
			*err = errors.New("error while parsing :" + fmt.Sprint(r) + fmt.Sprint(string(debug.Stack())))
		}
	}()
	_, *err = demoData.ParseHeader()
}

// ParseDemoWithoutPanic without this, if demostream isn't a demo, it panics
func ParseDemoWithoutPanic(demoData demoinfocs.Parser, err *error) {
	defer func() {
		if r := recover(); r != nil {
			*err = errors.New("error while parsing :" + fmt.Sprint(r) + fmt.Sprint(string(debug.Stack())))
		}
	}()

	var cancelled bool

	demoData.RegisterEventHandler(func(e events.FrameDone) {
		if cancelled {
			return
		}
		if _, exists := demoData.GameState().ConVars()["game_mode"]; !exists {
			return
		}
		// if _, exists := demoData.GameState().ConVars()["game_type"]; !exists {
		// 	return
		// }

		cancelled = true
		demoData.Cancel()
	})

	localErr := demoData.ParseToEnd()
	if localErr != demoinfocs.ErrCancelled {
		*err = localErr
	}
}
