package helpers

import (
	"crypto/rand"
	"strings"
)

//RandomString Créée une chaine de caractère aléatoire de longueur n constituée de lettres et de nombres
func RandomString(n int) string {
	const letters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

	bytes := make([]byte, n)

	if _, err := rand.Read(bytes); err != nil {
		panic(err)
	}

	for i, b := range bytes {
		bytes[i] = letters[b%byte(len(letters))]
	}

	return string(bytes)
}

//RandomStringNumber Créée une chaine de caractère aléatoire de longueur n constituée uniquement de nombres
func RandomStringNumber(n int) string {
	const letters = "0123456789"

	bytes := make([]byte, n)

	if _, err := rand.Read(bytes); err != nil {
		panic(err)
	}

	for i, b := range bytes {
		bytes[i] = letters[b%byte(len(letters))]
	}

	return string(bytes)
}

//RandomStringChar Créée une chaine de caractère aléatoire de longueur n constituée de lettres
func RandomStringChar(n int) string {
	const letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-"

	bytes := make([]byte, n)

	if _, err := rand.Read(bytes); err != nil {
		panic(err)
	}

	for i, b := range bytes {
		bytes[i] = letters[b%byte(len(letters))]
	}

	return string(bytes)
}

//RandomStringCharUpperAndDigits create a random string with uppercase letters and digits
func RandomStringCharUpperAndDigits(n int) string {
	const letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

	bytes := make([]byte, n)

	if _, err := rand.Read(bytes); err != nil {
		panic(err)
	}

	for i, b := range bytes {
		bytes[i] = letters[b%byte(len(letters))]
	}

	return string(bytes)
}

// MapNames slice of all map registered
var MapNames = []string{"inferno",
	"dust2",
	"train",
	"mirage",
	"nuke",
	"overpass",
	"vertigo",
	"cache",
	"cbble",
	"canals",
	"zoo",
	"abbey",
	"biome",
	"militia",
	"agency",
	"office",
	"italy",
	"anubis",
	"chlorine",
	"breach",
	"workout",
	"swamp",
	"mutiny",
	"assault",
	"engage",
	"ancient",
	"apollo",
	"grind",
	"mocha",
}

// ParseMapName parse the raw name to a sub name (ex faceit_inferno to inferno)
func ParseMapName(rawName string) *string {
	rawName = strings.ToLower(rawName)

	for _, mapName := range MapNames {
		if strings.Contains(rawName, mapName) {
			return &mapName
		}
	}

	return &rawName
}

// TruncatString truncate the string to a certain length
func TruncatString(s string, l int) string {
	if len(s) > l {
		return s[:l]
	}
	return s
}
