package helpers

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestSendError(t *testing.T) {
	w := httptest.NewRecorder()

	gin.SetMode(gin.TestMode)
	c, _ := gin.CreateTestContext(w)

	assert.Equal(t, 0, len(c.Errors))

	detail := make(map[ErrorKey]interface{})
	detail["test"] = "ceci est un test"

	SendError(c, logTest, ErrInvalidInputJSON(), detail, nil)

	assert.Equal(t, 1, len(c.Errors))

	const strErr = `{"title":"ErrInvalidInputJSON","HTTPStatus":400,` +
		`"descriptions":{"fr":"Les donnees entrées dans le JSON sont incorrectes","eng":""},` +
		`"details":{"test":"ceci est un test"}}`

	assert.Equal(t, strErr, c.Errors.Errors()[0])
}

// bodyLogWriter obligatoire sinon importation cyclique...
type bodyLogWriter struct {
	gin.ResponseWriter
	Body *bytes.Buffer
}

func (w bodyLogWriter) Write(b []byte) (int, error) {
	w.Body.Write(b)

	return w.ResponseWriter.Write(b)
}

func TestSendData(t *testing.T) {
	w := httptest.NewRecorder()

	gin.SetMode(gin.TestMode)
	c, _ := gin.CreateTestContext(w)

	blw := &bodyLogWriter{Body: bytes.NewBufferString(""), ResponseWriter: c.Writer}
	c.Writer = blw
	SendData(c, http.StatusOK, "data is here")

	data := `{"data":"data is here"}`

	assert.Equal(t, http.StatusOK, c.Writer.Status())
	assert.Equal(t, data, blw.Body.String())
}
