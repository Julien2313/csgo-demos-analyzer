package helpers

import "golang.org/x/crypto/bcrypt"

// HashPassword Retourne le hash d'un mot de passe passé en param
// Attention utilise la lib bcrypt : hasher 2 fois le même mdp
// ne donnera pas le même hash, utiliser la fonction CheckHashPassword
func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 10)

	return string(bytes), err
}

// CheckHashPassword Permet de comparer un password hashé avec un password clear
// Ne pas utiliser pour comparer 2 hashs
func CheckHashPassword(passwordStored, passwordSent string) error {
	return bcrypt.CompareHashAndPassword([]byte(passwordStored), []byte(passwordSent))
}
