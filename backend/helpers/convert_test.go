package helpers

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestToUint(t *testing.T) {
	value := "654364"
	valueConverted, err := ToUint(value)
	require.NoError(t, err)
	assert.Equal(t, valueConverted, uint(654364))

	value = RandomStringChar(5)
	valueConverted, err = ToUint(value)
	assert.Error(t, err)
	assert.Equal(t, valueConverted, uint(0))
}

func TestToInt64(t *testing.T) {
	value := "654364"
	valueConverted, err := ToInt64(value)
	require.NoError(t, err)
	assert.Equal(t, valueConverted, int64(654364))

	value = RandomStringChar(5)
	valueConverted, err = ToInt64(value)
	assert.Error(t, err)
	assert.Equal(t, valueConverted, int64(0))
}

func TestToUint64(t *testing.T) {
	value := "654364"
	valueConverted, err := ToUint64(value)
	require.NoError(t, err)
	assert.Equal(t, valueConverted, uint64(654364))

	value = RandomStringChar(5)
	valueConverted, err = ToUint64(value)
	assert.Error(t, err)
	assert.Equal(t, valueConverted, uint64(0))
}

func TestOToGo(t *testing.T) {
	value := 654364.0
	valueConverted := OToGo(value)
	assert.Equal(t, valueConverted, value/1024/1024/1024)
}
