package helpers

import "math/rand"

// RandUIntPSQL 0-maxint for psql
func RandUIntPSQL() *uint {
	value := uint(rand.Uint32() % 2147483647)
	return &value
}

// RandBool return a random bool
func RandBool() bool {
	return rand.Int()%2 == 0
}
