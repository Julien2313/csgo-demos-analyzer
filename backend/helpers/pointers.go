package helpers

import "time"

// IntPtr permet de récupérer l'adresse d'un int
func IntPtr(value int) *int {
	return &value
}

// Int64Ptr permet de récupérer l'adresse d'un int64
func Int64Ptr(value int64) *int64 {
	return &value
}

// UInt64Ptr permet de récupérer l'adresse d'un uint64
func UInt64Ptr(value uint64) *uint64 {
	return &value
}

// UintPtr permet de récupérer l'adresse d'un uint
func UintPtr(value uint) *uint {
	return &value
}

// FloatPtr permet de récupérer l'adresse d'un float64
func FloatPtr(value float64) *float64 {
	return &value
}

// StrPtr permet de récupérer l'adresse d'un string
func StrPtr(value string) *string {
	return &value
}

// BoolPtr permet de récupérer l'adresse d'un bool
func BoolPtr(value bool) *bool {
	return &value
}

// TimePtr permet de récupérer l'adresse d'un bool
func TimePtr(value time.Time) *time.Time {
	return &value
}

// DurationPtr permet de récupérer l'adresse d'une duration
func DurationPtr(value time.Duration) *time.Duration {
	return &value
}
