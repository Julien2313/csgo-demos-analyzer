package helpers

import "strconv"

// ToUint convert string to uint
func ToUint(s string) (uint, error) {
	var (
		err error
		ret int
	)

	if ret, err = strconv.Atoi(s); err != nil {
		return 0, err
	}

	return uint(ret), nil
}

// ToInt64 convert string to int64
func ToInt64(s string) (int64, error) {
	var (
		err error
		ret int64
	)

	if ret, err = strconv.ParseInt(s, 10, 64); err != nil {
		return 0, err
	}

	return ret, nil
}

// ToUint64 convert string to int64
func ToUint64(s string) (uint64, error) {
	var (
		err error
		ret uint64
	)

	if ret, err = strconv.ParseUint(s, 10, 64); err != nil {
		return 0, err
	}

	return ret, nil
}

// OToGo convert octets to gigaoctets
func OToGo(size float64) float64 {
	return size / 1024 / 1024 / 1024
}
