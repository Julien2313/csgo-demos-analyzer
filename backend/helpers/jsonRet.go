package helpers

import (
	"encoding/json"
	"errors"

	"github.com/gin-gonic/gin"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
)

func prepareData(data interface{}) gin.H {
	return gin.H{"data": data}
}

// SendData a
func SendData(c *gin.Context, status int, data interface{}) {
	c.JSON(status, prepareData(data))
}

// SendError Permet de renvoyer au front une erreur
func SendError(c *gin.Context, logBack *custlogger.Logger, errorNewGen ErrorNewGen,
	details map[ErrorKey]interface{}, errorToLog error) {
	logBack.Error(errorToLog, "err to log : ", errorToLog)

	errorNewGen.Details = details

	strErr, err := json.Marshal(errorNewGen)
	if err != nil {
		if cErr := c.Error(err); cErr != nil {
			panic(cErr)
		}

		panic(err)
	}

	c.AbortWithStatusJSON(errorNewGen.HTTPStatus, errorNewGen)

	// c'est l'erreur parsé qui est renvoyé, donc inutile de la tester
	_ = c.Error(errors.New(string(strErr)))
}
