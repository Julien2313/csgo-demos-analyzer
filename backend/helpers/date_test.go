package helpers

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestRandDate(t *testing.T) {
	date := RandDate()
	assert.Equal(t, date.Location(), time.UTC)
	assert.True(t, date.After(time.Date(1970, 1, 0, 0, 0, 0, 0, time.UTC)))
	assert.True(t, date.Before(time.Date(2070, 1, 0, 0, 0, 0, 0, time.UTC)))
}

func TestFmtDuration(t *testing.T) {
	d := time.Second*97 + time.Microsecond*540
	assert.Equal(t, "01:37", FmtDuration(d))
}
