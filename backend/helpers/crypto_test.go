package helpers

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestHashPassword(t *testing.T) {
	passwordToHash := "csgo"
	hashed, err := HashPassword(passwordToHash)

	require.NoError(t, err)
	assert.NotEqual(t, passwordToHash, hashed)

	err = CheckHashPassword(hashed, passwordToHash)
	assert.NoError(t, err)

	err = CheckHashPassword(hashed, "passwordToHash")
	assert.Error(t, err)
}
