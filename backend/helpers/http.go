package helpers

import (
	"bytes"
	"errors"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
)

var strErrorValidator = "Error validator : "

// GenerateErrorValidator concat strErrorValidator with the param
func GenerateErrorValidator(str string) error {
	return errors.New(strErrorValidator + str)
}

// CheckErrorNotFound permet de vérifier s'il s'agit d'une erreur "not found" ou non
// func CheckErrorNotFound(c *gin.Context, details map[ErrorKey]interface{}, err error) {
// 	if gorm.IsRecordNotFoundError(err) {
// 		SendError(c, ErrNotFound(), details, err)
// 		return
// 	}
// 	SendError(c, ErrInternalServerError(), nil, err)
// }

// CheckErrorValidator permet de vérifier s'il s'agit d'une du validator ou non
func CheckErrorValidator(c *gin.Context, logBack *custlogger.Logger, err error) {
	if strings.HasPrefix(err.Error(), strErrorValidator) {
		errStr := strings.Trim(err.Error(), strErrorValidator)
		SendError(c, logBack, ErrBadRequest(), MakeEasyDetail(PAYLOAD, errStr), err)

		return
	}

	SendError(c, logBack, ErrInternalServerError(), nil, err)
}

// SendRequest permet d'envoyer une requête
func SendRequest(parameters []byte, methode string, url string) (*http.Response, error) {
	req, err := http.NewRequest(methode, url, bytes.NewBuffer(parameters))
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")

	client := http.Client{}

	return client.Do(req)
}
