package helpers

import "math"

// Round round a float to a certain precision
func Round(nbr float64, precision int) float64 {
	return math.Round(nbr*math.Pow10(precision)) / math.Pow10(precision)
}

// Floor round floor a float to a certain precision
func Floor(nbr float64, precision int) float64 {
	return math.Floor(nbr*math.Pow10(precision)) / math.Pow10(precision)
}

// PMod positive modulo
func PMod(a, b float64) float64 {
	return math.Mod((math.Mod(a, b) + b), b)
}
