package helpers

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRandomString(t *testing.T) {
	n := 0
	assert.Equal(t, n, len(RandomString(n)))
	assert.Panics(t, func() {
		RandomString(-1)
	})

	n = 10
	assert.Equal(t, n, len(RandomString(n)))
}

func TestRandomStringChar(t *testing.T) {
	n := 0
	assert.Equal(t, n, len(RandomStringChar(n)))
	assert.Panics(t, func() {
		RandomString(-1)
	})

	n = 10
	assert.Equal(t, n, len(RandomStringChar(n)))
}

func TestRandomStringNumber(t *testing.T) {
	n := 0
	assert.Equal(t, n, len(RandomStringNumber(n)))
	assert.Panics(t, func() {
		RandomString(-1)
	})

	n = 10
	assert.Equal(t, n, len(RandomStringNumber(n)))
}

func TestRandomStringCharUpperAndDigits(t *testing.T) {
	n := 0
	assert.Equal(t, n, len(RandomStringCharUpperAndDigits(n)))
	assert.Panics(t, func() {
		RandomString(-1)
	})

	n = 10
	assert.Equal(t, n, len(RandomStringCharUpperAndDigits(n)))
}

func TestParseMapName(t *testing.T) {
	assert.Equal(t, "testmapnam unknown", *ParseMapName("testmapnam unknown"))
	assert.Equal(t, "italy", *ParseMapName("testmapnam not unknown italy"))
	assert.Equal(t, "italy", *ParseMapName("aaaitalyaaa"))
}

func TestTruncatString(t *testing.T) {
	oldS := ""
	newS := TruncatString(oldS, 5)
	assert.Equal(t, "", newS)
	assert.Len(t, newS, 0)

	oldS = "abc"
	newS = TruncatString(oldS, 5)
	assert.Equal(t, "abc", newS)
	assert.Len(t, newS, 3)

	oldS = "abcdefghijklm"
	newS = TruncatString(oldS, 5)
	assert.Equal(t, "abcde", newS)
	assert.Len(t, newS, 5)
}
