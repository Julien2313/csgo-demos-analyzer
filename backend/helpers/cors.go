package helpers

import (
	"fmt"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

// ReturnCORSSetup return the CORS setup
func ReturnCORSSetup(ip string) gin.HandlerFunc {
	const maxAge = 12 * time.Hour

	return cors.New(cors.Config{
		AllowOrigins:     []string{fmt.Sprintf("http://%s:3000", ip)},
		AllowMethods:     []string{"GET", "PUT", "POST", "DELETE", "UPDATE", "OPTIONS"},
		AllowHeaders:     []string{"Content-Type"},
		AllowCredentials: true,
		MaxAge:           maxAge,
	})
}
