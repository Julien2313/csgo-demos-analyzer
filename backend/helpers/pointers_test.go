package helpers

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestIntPtr(t *testing.T) {
	varInt := 5
	assert.Equal(t, &varInt, IntPtr(varInt))
	assert.Equal(t, varInt, *IntPtr(varInt))
}

func TestInt64Ptr(t *testing.T) {
	varInt64 := int64(5)
	assert.Equal(t, &varInt64, Int64Ptr(varInt64))
	assert.Equal(t, varInt64, *Int64Ptr(varInt64))
}

func TestUInt64Ptr(t *testing.T) {
	varUInt64 := uint64(5)
	assert.Equal(t, &varUInt64, UInt64Ptr(varUInt64))
	assert.Equal(t, varUInt64, *UInt64Ptr(varUInt64))
}

func TestUintPtr(t *testing.T) {
	varUInt := uint(5)
	assert.Equal(t, &varUInt, UintPtr(varUInt))
	assert.Equal(t, varUInt, *UintPtr(varUInt))
}

func TestFloatPtr(t *testing.T) {
	varFloat := 5.0
	assert.Equal(t, &varFloat, FloatPtr(varFloat))
	assert.Equal(t, varFloat, *FloatPtr(varFloat))
}

func TestStrPtr(t *testing.T) {
	varStr := "5"
	assert.Equal(t, &varStr, StrPtr(varStr))
	assert.Equal(t, varStr, *StrPtr(varStr))
}

func TestTimePtr(t *testing.T) {
	varTime := time.Now()
	assert.Equal(t, &varTime, TimePtr(varTime))
	assert.Equal(t, varTime, *TimePtr(varTime))
}

func TestDurationPtr(t *testing.T) {
	varDuration := time.Duration(6543645368)
	assert.Equal(t, &varDuration, DurationPtr(varDuration))
	assert.Equal(t, varDuration, *DurationPtr(varDuration))
}

func TestBoolPtr(t *testing.T) {
	varBool := true
	assert.Equal(t, &varBool, BoolPtr(varBool))
	assert.Equal(t, varBool, *BoolPtr(varBool))
}
