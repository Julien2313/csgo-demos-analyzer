package helpers

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMakeEasyDetail(t *testing.T) {
	mapa := MakeEasyDetail("key", "value")
	assert.Equal(t, "value", mapa["key"])

	slice := []interface{}{"value", 3}
	mapa = MakeEasyDetail("key", slice)
	assert.Equal(t, slice, mapa["key"])
}

func TestNewGenErrorToError(t *testing.T) {
	const strErr = `{"title":"ErrInvalidInputJSON",` +
		`"HTTPStatus":400,"descriptions":{"fr":"Les donnees entrées dans le JSON sont incorrectes","eng":""},"details":null}`

	assert.Equal(t, strErr, NewGenErrorToError(ErrInvalidInputJSON()).Error())
}

func TestStatusCode(t *testing.T) {
	assert.Equal(t, http.StatusBadRequest, ErrAuth().HTTPStatus)
	assert.Equal(t, http.StatusBadRequest, ErrBadCredentials().HTTPStatus)
	assert.Equal(t, http.StatusInternalServerError, ErrInternalServerError().HTTPStatus)
	assert.Equal(t, http.StatusConflict, ErrDuplicate().HTTPStatus)
	assert.Equal(t, http.StatusBadRequest, ErrInvalidInput().HTTPStatus)
	assert.Equal(t, http.StatusBadRequest, ErrInvalidInputJSON().HTTPStatus)
	assert.Equal(t, http.StatusBadRequest, ErrInvalidInputURL().HTTPStatus)
	assert.Equal(t, http.StatusBadRequest, ErrInvalidInputCookies().HTTPStatus)
	assert.Equal(t, http.StatusBadRequest, ErrBadRequest().HTTPStatus)
	assert.Equal(t, http.StatusUnauthorized, ErrBadTokenConnection().HTTPStatus)
	assert.Equal(t, http.StatusNotFound, ErrNotFound().HTTPStatus)
	assert.Equal(t, http.StatusForbidden, ErrForbidden().HTTPStatus)
	assert.Equal(t, http.StatusBadRequest, ErrPasswordStrength().HTTPStatus)
	assert.Equal(t, http.StatusBadRequest, ErrBadRequest().HTTPStatus)
	assert.Equal(t, http.StatusBadRequest, ErrBadRequest().HTTPStatus)
	assert.Equal(t, http.StatusBadRequest, ErrBadRequest().HTTPStatus)
}
