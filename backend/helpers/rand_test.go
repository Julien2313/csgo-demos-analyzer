package helpers_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

func TestRandUIntPSQL(t *testing.T) {
	for nbrTRy := 0; nbrTRy < 100; nbrTRy++ {
		assert.True(t, *helpers.RandUIntPSQL() < 2147483647)
	}
}
