package helpers_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

func TestRound(t *testing.T) {
	value := 45.891651
	valueRounded := 45.89
	assert.Equal(t, valueRounded, helpers.Round(value, 2))
}

func TestFloor(t *testing.T) {
	value := 45.891651
	valueRounded := 45.8
	assert.Equal(t, valueRounded, helpers.Floor(value, 1))
}

func TestMod(t *testing.T) {
	a := 10.0
	b := 3.0
	assert.Equal(t, 1.0, helpers.PMod(a, b))
	a = -10.0
	b = 3.0
	assert.Equal(t, 2.0, helpers.PMod(a, b))
}
