package helpers

import (
	"fmt"
	"math/rand"
	"time"
)

// RandDate return a random time.Time
func RandDate() *time.Time {
	min := time.Date(1970, 1, 0, 0, 0, 0, 0, time.UTC).Unix()
	max := time.Date(2070, 1, 0, 0, 0, 0, 0, time.UTC).Unix()
	delta := max - min
	sec := rand.Int63n(delta) + min
	date := time.Unix(sec, 0)
	date = date.In(time.UTC)

	return &date
}

// FmtDuration convert duration to string mm:ss
func FmtDuration(d time.Duration) string {
	d = d.Round(time.Second)
	m := d / time.Minute
	d -= m * time.Minute
	s := d / time.Second

	return fmt.Sprintf("%02d:%02d", m, s)
}
