package helpers

import (
	"encoding/json"
	"errors"
	"net/http"
)

// Descriptions représente une description d'erreur stockée dans le json
type Descriptions struct {
	FR  string `json:"fr"`
	ENG string `json:"eng"`
}

// ErrorKey represents an error
type ErrorKey string

const (
	// URL : l'erreur concerne l'url
	URL ErrorKey = "URL"
	// PAYLOAD : l'erreur concerne le json
	PAYLOAD ErrorKey = "PAYLOAD"
	// HEADER : l'erreur concerne le header
	HEADER ErrorKey = "HEADER"
	// DATA : plus d'informations sur l'erreur
	DATA ErrorKey = "DATA"
)

// ErrorNewGen représente une erreur stockée dans le json
type ErrorNewGen struct {
	Title      string       `json:"title"`
	HTTPStatus int          `json:"HTTPStatus"`
	Descr      Descriptions `json:"descriptions"`

	// Variable plus spécifique au problème
	Details map[ErrorKey]interface{} `json:"details"`
}

// MakeEasyDetail créé une map detail avec une clef unique
func MakeEasyDetail(key ErrorKey, value interface{}) map[ErrorKey]interface{} {
	return map[ErrorKey]interface{}{
		key: value,
	}
}

// NewGenErrorToError permet de vérifier s'il s'agit d'une du validator ou non
func NewGenErrorToError(errorNewGen ErrorNewGen) error {
	strErr, err := json.Marshal(errorNewGen)
	if err != nil {
		panic(err)
	}

	return errors.New(string(strErr))
}

// ErrAuth Authentication error
func ErrAuth() ErrorNewGen {
	return ErrorNewGen{
		Title:      "ErrAuth",
		HTTPStatus: http.StatusBadRequest,
		Descr: Descriptions{
			FR:  "Impossible de se connecter",
			ENG: "Cannot login",
		},
	}
}

// ErrBadCredentials Bad login
func ErrBadCredentials() ErrorNewGen {
	return ErrorNewGen{
		Title:      "ErrBadCredentials",
		HTTPStatus: http.StatusBadRequest,
		Descr: Descriptions{
			FR:  "Mail ou mot de passe incorrect",
			ENG: "",
		},
	}
}

// ErrInternalServerError report error from server
func ErrInternalServerError() ErrorNewGen {
	return ErrorNewGen{
		Title:      "ErrInternalServerError",
		HTTPStatus: http.StatusInternalServerError,
		Descr: Descriptions{
			FR:  "Erreur interne du serveur",
			ENG: "Internal serveur error",
		},
	}
}

// ErrDuplicate duplicate ressource
func ErrDuplicate() ErrorNewGen {
	return ErrorNewGen{
		Title:      "ErrDuplicate",
		HTTPStatus: http.StatusConflict,
		Descr: Descriptions{
			FR:  "La ressource existe déjà avec un ou plusieurs attributs sensés être uniques",
			ENG: "",
		},
	}
}

// ErrInvalidInput bad user input
func ErrInvalidInput() ErrorNewGen {
	return ErrorNewGen{
		Title:      "ErrInvalidInput",
		HTTPStatus: http.StatusBadRequest,
		Descr: Descriptions{
			FR:  "Les donnees entrées sont incorrectes",
			ENG: "",
		},
	}
}

// ErrInvalidInputJSON bad json parsing
func ErrInvalidInputJSON() ErrorNewGen {
	return ErrorNewGen{
		Title:      "ErrInvalidInputJSON",
		HTTPStatus: http.StatusBadRequest,
		Descr: Descriptions{
			FR:  "Les donnees entrées dans le JSON sont incorrectes",
			ENG: "",
		},
	}
}

// ErrInvalidInputURL bad GET data
func ErrInvalidInputURL() ErrorNewGen {
	return ErrorNewGen{
		Title:      "ErrInvalidInputURL",
		HTTPStatus: http.StatusBadRequest,
		Descr: Descriptions{
			FR:  "Les donnees entrées dans l'url sont incorrectes",
			ENG: "",
		},
	}
}

// ErrInvalidInputCookies bad cookie
func ErrInvalidInputCookies() ErrorNewGen {
	return ErrorNewGen{
		Title:      "ErrInvalidInputCookies",
		HTTPStatus: http.StatusBadRequest,
		Descr: Descriptions{
			FR:  "Le cookie est incorrect",
			ENG: "",
		},
	}
}

// ErrBadRequest bad request
func ErrBadRequest() ErrorNewGen {
	return ErrorNewGen{
		Title:      "ErrBadRequest",
		HTTPStatus: http.StatusBadRequest,
		Descr: Descriptions{
			FR:  "La requête n'est pas correcte",
			ENG: "",
		},
	}
}

// ErrBadTokenConnection bad auth token
func ErrBadTokenConnection() ErrorNewGen {
	return ErrorNewGen{
		Title:      "ErrBadTokenConnection",
		HTTPStatus: http.StatusUnauthorized,
		Descr: Descriptions{
			FR:  "Le token de connexion est incorrect",
			ENG: "",
		},
	}
}

// ErrNotFound 404
func ErrNotFound() ErrorNewGen {
	return ErrorNewGen{
		Title:      "ErrNotFound",
		HTTPStatus: http.StatusNotFound,
		Descr: Descriptions{
			FR:  "Impossible de trouver la ressource spécifiquement demandé",
			ENG: "",
		},
	}
}

// ErrForbidden 403
func ErrForbidden() ErrorNewGen {
	return ErrorNewGen{
		Title:      "ErrForbidden",
		HTTPStatus: http.StatusForbidden,
		Descr: Descriptions{
			FR:  "Vous n'êtes pas autorisés à faire cette requête",
			ENG: "",
		},
	}
}

// ErrDataMissing 422
func ErrDataMissing() ErrorNewGen {
	return ErrorNewGen{
		Title:      "ErrDataMissing",
		HTTPStatus: http.StatusUnprocessableEntity,
		Descr: Descriptions{
			FR:  "Il manque des informations vous concernant en base de données",
			ENG: "",
		},
	}
}

// ErrPasswordStrength security
func ErrPasswordStrength() ErrorNewGen {
	return ErrorNewGen{
		Title:      "ErrPasswordStrength",
		HTTPStatus: http.StatusBadRequest,
		Descr: Descriptions{
			FR:  "Le mot de passe renseigné ne respect pas les règles de sécurité minimales",
			ENG: "",
		},
	}
}

// ErrTooManyRequest there are to many requests performed
func ErrTooManyRequest() ErrorNewGen {
	return ErrorNewGen{
		Title:      "ErrTooManyRequest",
		HTTPStatus: http.StatusTooManyRequests,
		Descr: Descriptions{
			FR:  "",
			ENG: "there are to many requests performed",
		},
	}
}
