package helpers

import (
	"bufio"
	"compress/gzip"
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestDemoFileToReader(t *testing.T) {
	_, err := DemoFileToReader(nil)
	require.Error(t, err)

	fileBz2Name := fmt.Sprintf("%s.dem.bz2", RandomStringChar(10))
	fileBz2, err := os.Create(fileBz2Name)
	require.NoError(t, err)
	defer func() {
		require.NoError(t, fileBz2.Close())
		require.NoError(t, os.Remove(fileBz2Name))
	}()

	_, err = DemoFileToReader(fileBz2)
	require.NoError(t, err)

	//--------------

	fileDemName := fmt.Sprintf("%s.dem", RandomStringChar(10))
	fileDem, err := os.Create(fileDemName)
	require.NoError(t, err)
	defer func() {
		require.NoError(t, fileDem.Close())
		require.NoError(t, os.Remove(fileDemName))
	}()

	_, err = DemoFileToReader(fileDem)
	require.NoError(t, err)

	//--------------

	fileGzName := fmt.Sprintf("%s.dem.gz", RandomStringChar(10))
	fileGz, err := os.Create(fileGzName)
	require.NoError(t, err)
	defer func() {
		require.NoError(t, fileGz.Close())
		require.NoError(t, os.Remove(fileGzName))
	}()

	gf := gzip.NewWriter(fileGz)
	fw := bufio.NewWriter(gf)
	demoFiletextGz := RandomString(4097)
	_, err = fw.WriteString(demoFiletextGz)
	require.NoError(t, err)
	require.NoError(t, gf.Close())

	fileGz.Close()
	fileGz, err = os.Open(fileGzName)
	require.NoError(t, err)

	bytes, err := DemoFileToByte(fileGz)
	require.NoError(t, err)
	assert.Equal(t, demoFiletextGz[:len(demoFiletextGz)-1], string(bytes))

	//--------------

	fileTestName := fmt.Sprintf("%s.test", RandomStringChar(10))
	fileTest, err := os.Create(fileTestName)
	require.NoError(t, err)
	defer func() {
		require.NoError(t, fileTest.Close())
		require.NoError(t, os.Remove(fileTestName))
	}()

	_, err = DemoFileToReader(fileTest)
	require.Error(t, err)
}
