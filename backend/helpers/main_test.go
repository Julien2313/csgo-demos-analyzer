package helpers

import (
	"math/rand"
	"os"
	"testing"
	"time"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/custlogger"
)

var logTest *custlogger.Logger

func TestMain(m *testing.M) {
	rand.Seed(time.Now().Unix())

	logTest = &custlogger.Logger{}
	logTest.SetupInstanceLogggerTest()

	os.Exit(m.Run())
}
