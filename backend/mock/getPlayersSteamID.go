package mock

import (
	"net/http"
	"strings"

	"github.com/jarcoal/httpmock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
)

// PlayersSteamID mocking steam api steamID
func PlayersSteamID() {
	httpmock.RegisterResponder("GET", "https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v2",
		func(req *http.Request) (*http.Response, error) {
			urlValues := req.URL.Query()

			linksAvatar := struct {
				Response struct {
					Players []struct {
						SteamID string `json:"steamid"`
						Avatar  string `json:"avatar"`
					} `json:"players"`
				} `json:"response"`
			}{}

			steamsID := strings.Split(urlValues.Get("steamids"), ", ")
			for _, steamID := range steamsID {
				avatar := struct {
					SteamID string `json:"steamid"`
					Avatar  string `json:"avatar"`
				}{SteamID: steamID, Avatar: helpers.RandomString(30)}
				linksAvatar.Response.Players = append(linksAvatar.Response.Players, avatar)
			}
			resp, err := httpmock.NewJsonResponse(http.StatusOK, linksAvatar)
			if err != nil {
				return httpmock.NewStringResponse(http.StatusInternalServerError, ""), err
			}

			return resp, nil
		},
	)
}
