package mock

import (
	"math/rand"
	"net/http"
	"strconv"

	"github.com/jarcoal/httpmock"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/model"
)

// MatchSharingCode struct with current sharecode and the nextone
type MatchSharingCode struct {
	KnownCode            string
	nextMatchSharingCode *MatchSharingCode
}

type userAPI struct {
	Key        *string `json:"key"`
	SteamID    *string `json:"steamid"`
	SteamIDKey *string `json:"steamidkey"`

	// only for mock
	KnownCode *string `json:"knowncode"`

	CurrentMatchSharingCode *MatchSharingCode
}

//UsersMocked slice of users
var UsersMocked []userAPI

// NbrUser number of users
var NbrUser int

// NextMatchSharingCode mocking steam api next share code
func NextMatchSharingCode() {
	NbrUser = 10
	for numUser := 0; numUser < NbrUser; numUser++ {
		newUser := userAPI{
			Key:        &APIkey,
			SteamID:    helpers.StrPtr(strconv.FormatUint(rand.Uint64(), 10)),
			SteamIDKey: model.GenerateRandomAccessTokenHistory(),
		}
		newUser.CurrentMatchSharingCode = &MatchSharingCode{
			KnownCode: *model.GenerateRandomShareCode(),
		}
		lastShareCode := newUser.CurrentMatchSharingCode

		for numMatchSharingCode := 0; numMatchSharingCode < 98; numMatchSharingCode++ {
			nexShareCode := &MatchSharingCode{
				KnownCode: *model.GenerateRandomShareCode(),
			}
			lastShareCode.nextMatchSharingCode = nexShareCode
			lastShareCode = nexShareCode
		}

		UsersMocked = append(UsersMocked, newUser)
	}

	mockResponseNextMatchSharingCode()
}

func mockResponseNextMatchSharingCode() {
	httpmock.RegisterResponder("GET", "https://api.steampowered.com/ICSGOPlayers_730/GetNextMatchSharingCode/v1",
		func(req *http.Request) (*http.Response, error) {
			requestUserAPI := &userAPI{}
			urlValues := req.URL.Query()

			requestUserAPI.SteamID = helpers.StrPtr(urlValues.Get("steamid"))
			if *requestUserAPI.SteamID == "" {
				return httpmock.NewStringResponse(http.StatusBadRequest,
					"<html><head><title>Bad Request</title></head><body><h1>Bad Request</h1>"+
						"Required parameter 'steamid' is missing</body></html>"), nil
			}

			requestUserAPI.Key = helpers.StrPtr(urlValues.Get("key"))
			if *requestUserAPI.Key == "" {
				return httpmock.NewStringResponse(http.StatusForbidden,
					"<html><head><title>Forbidden</title></head><body><h1>Forbidden</h1>Access is denied. "+
						"Retrying will not help. Please verify your <pre>key=</pre> parameter.</body></html>"), nil
			}

			if *requestUserAPI.Key != APIkey {
				return httpmock.NewStringResponse(http.StatusForbidden,
					"<html><head><title>Forbidden</title></head><body><h1>Forbidden</h1>Access is denied. "+
						"Retrying will not help. Please verify your <pre>key=</pre> parameter.</body></html>"), nil
			}

			requestUserAPI.SteamIDKey = helpers.StrPtr(urlValues.Get("steamidkey"))
			if *requestUserAPI.SteamIDKey == "" {
				return httpmock.NewStringResponse(http.StatusBadRequest,
					"<html><head><title>Bad Request</title></head><body><h1>Bad Request</h1>"+
						"Required parameter 'steamidkey' is missing</body></html>"), nil
			}

			requestUserAPI.KnownCode = helpers.StrPtr(urlValues.Get("knowncode"))
			if *requestUserAPI.KnownCode == "" {
				return httpmock.NewStringResponse(http.StatusBadRequest,
					"<html><head><title>Bad Request</title></head><body><h1>Bad Request</h1>"+
						"Required parameter 'knowncode' is missing</body></html>"), nil
			}

			found := false
			userFound := &userAPI{}
			for _, user := range UsersMocked {
				if *user.SteamID == *requestUserAPI.SteamID && *user.SteamIDKey == *requestUserAPI.SteamIDKey {
					userFound = &user
					found = true
					break
				}
			}
			if !found {
				return httpmock.NewStringResponse(http.StatusForbidden, ""), nil
			}

			found = false
			currentShareCode := userFound.CurrentMatchSharingCode
			for {
				if *requestUserAPI.KnownCode == currentShareCode.KnownCode {
					found = true
					break
				}
				currentShareCode = currentShareCode.nextMatchSharingCode
				if currentShareCode == nil {
					break
				}
			}
			if !found {
				return httpmock.NewStringResponse(http.StatusForbidden, ""), nil
			}

			nextShareCode := struct {
				Result struct {
					NextCode string `json:"nextcode"`
				} `json:"result"`
			}{}

			if currentShareCode.nextMatchSharingCode == nil {
				nextShareCode.Result.NextCode = "n/a"
				resp, err := httpmock.NewJsonResponse(http.StatusAccepted, nextShareCode)
				if err != nil {
					return httpmock.NewStringResponse(http.StatusInternalServerError, ""), err
				}
				return resp, nil
			}

			nextShareCode.Result.NextCode = currentShareCode.nextMatchSharingCode.KnownCode
			resp, err := httpmock.NewJsonResponse(http.StatusOK, nextShareCode)
			if err != nil {
				return httpmock.NewStringResponse(http.StatusInternalServerError, ""), err
			}
			return resp, nil
		},
	)
}
