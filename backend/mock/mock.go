package mock

import "gitlab.com/Julien2313/CSGO-demos-analyzer/backend/helpers"

// APIkey api key for test
var APIkey string = helpers.RandomStringCharUpperAndDigits(32)
