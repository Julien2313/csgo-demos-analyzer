import apiClient from '../consts/apiClient';

function login(credentials) {
  return apiClient.post('/user/login', { credentials });
}

function logout() {
  return apiClient.post('/logout');
}

function register(credentials) {
  return apiClient.post('/user/register', { credentials });
}

function setSteamID(steamid) {
  return apiClient.post('/user/steamid', { steamid });
}

function getDateCalendar(steamid) {
  return apiClient.get(`/user/dateCalendar/${steamid}`);
}

function setShareCode(shareCode) {
  return apiClient.post('/user/sharecode', { shareCode });
}

function setAuthenticationCodeHitory(token) {
  return apiClient.post('/user/authenticationcodehitory', { token });
}

function checkTokens() {
  return apiClient.get('/user/checktokens');
}

function me() {
  return apiClient.get('/user/me');
}

function checkAuth() {
  return apiClient.get('/auth');
}

function getProgression() {
  return apiClient.get('/user/progression');
}

function getAllTokens() {
  return apiClient.get('/user/tokens');
}

function createToken(token) {
  return apiClient.post('/user/createToken', token);
}

export default {
  login,
  logout,
  register,
  checkAuth,
  me,
  setSteamID,
  setShareCode,
  setAuthenticationCodeHitory,
  getProgression,
  checkTokens,
  getDateCalendar,
  getAllTokens,
  createToken,
};
