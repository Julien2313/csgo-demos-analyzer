import apiClient from '../consts/apiClient';

function uploadDemo(demo, lastModified, progressSet) {
  return apiClient.post('/demo/upload', demo, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    params: {
      lastModified,
    },
    onUploadProgress: (progressEvent) => progressSet((progressEvent.loaded / progressEvent.total) * 100),
  });
}

function getLastAnalyzed() {
  return apiClient.get('/demo/lastAnalyzed');
}

function getAllDemos(steamID, payload) {
  return apiClient.get(`/demo/user/${steamID}`, {
    params: {
      ...payload,
    },
  });
}

function getAllDeathMatchDemos() {
  return apiClient.get('/stats/deathmatch');
}

function getAllCompetitivesDemos(payload) {
  return apiClient.get('/stats/competitives', {
    params: {
      ...payload,
    },
  });
}

function getStats(demoID) {
  return apiClient.get(`/stats/demo/${demoID}`);
}

function getHeatMaps(steamID, mapName) {
  return apiClient.get(`/stats/heatmaps/${steamID}/${mapName}`);
}

function getPatterns(steamID) {
  return apiClient.get(`/stats/pattern/${steamID}`);
}

function checkNewDemo() {
  return apiClient.post('/demo/checknewdemo');
}

function exists(demo) {
  return apiClient.post('/demo/', demo, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
}

function uploadByURL(url) {
  return apiClient.post('/demo/uploadbyurl', url);
}

export default {
  uploadDemo,
  getAllDemos,
  getStats,
  exists,
  checkNewDemo,
  getAllDeathMatchDemos,
  getAllCompetitivesDemos,
  getHeatMaps,
  getPatterns,
  uploadByURL,
  getLastAnalyzed,
};
