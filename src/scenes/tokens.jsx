import React, {
  useState, useEffect,
} from 'react';
import { useHistory } from 'react-router-dom';

import { toast } from 'react-toastify';
import {
  Container,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  TextField,
  Button,
} from '@material-ui/core';

import apiUser from '../api/user';

function Tokens() {
  const history = useHistory();
  const [tokens, setTokens] = useState([]);
  const [ip, setIP] = useState('');

  const onSubmit = async (event) => {
    event.preventDefault();

    try {
      const response = await apiUser.createToken({ ip });
      setTokens([...tokens, response.data.data]);
      setIP('');

      toast.success('successful creation', {
        position: 'top-center',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    } catch (err) {
      if (err.response === undefined) {
        toast.error('An error occurred', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        return;
      }
      if (err.response.status === 429) {
        toast.error('Too many request !', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        return;
      }
      if (err.response.status === 403 && err.response.data.title === 'ErrBadTokenConnection') {
        toast.info('You have been disconnected', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        history.push({
          pathname: '/',
        });
        return;
      }

      toast.error('An error occurred', {
        position: 'top-center',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  };

  const getTokens = async () => {
    try {
      const response = await apiUser.getAllTokens();
      console.log(response);
      if (response.data.data !== null) {
        setTokens(response.data.data);
      }
    } catch (err) {
      if (err.response === undefined) {
        toast.error('An error occurred', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        return;
      }
      if (err.response.status === 429) {
        toast.error('Too many request !', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        return;
      }
      if (err.response.status === 403 && err.response.data.title === 'ErrBadTokenConnection') {
        toast.info('You have been disconnected', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        history.push({
          pathname: '/',
        });
        return;
      }

      toast.error('An error occurred', {
        position: 'top-center',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  };

  useEffect(() => {
    getTokens();
  }, []);

  return (
    <Container>
      <form onSubmit={onSubmit} noValidate>
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="ip"
          label="ip of your server"
          value={ip}
          onInput={(e) => setIP(e.target.value)}
          autoComplete="ip of your server"
          autoFocus
        />
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
        >
          Generate
        </Button>
      </form>
      <TableContainer component={Paper}>
        <Table size="small" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell align="left">IP</TableCell>
              <TableCell>Tokens</TableCell>
              <TableCell align="left">Last Used</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {tokens.map((t) => (
              <TableRow
                key={t.token}
              >
                <TableCell align="left">{t.ip}</TableCell>
                <TableCell align="left">{t.token}</TableCell>
                <TableCell align="left">{t.lastUsed === null ? 'Never' : t.lastUsed}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Container>
  );
}

export default Tokens;
