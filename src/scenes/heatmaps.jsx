import React, { useState, useEffect } from 'react';

import { useHistory } from 'react-router-dom';
import h337 from 'heatmap.js';
import { toast } from 'react-toastify';
import {
  Container,
  Box,
  Slider,
  Paper,
  FormControl,
  Select,
  MenuItem,
  FormGroup,
  FormControlLabel,
  Checkbox,
  Typography,
} from '@material-ui/core';

import { useCookies } from 'react-cookie';

import apiDemo from '../api/demo';

import map from '../components/utils/map';
import WeaponsPicker from '../components/dialog/weaponsPicker';

function ConvertX(x, chiftLeft, chiftRight, currentSizeImg) {
  return x * (currentSizeImg - chiftLeft - chiftRight) + chiftLeft;
}
function ConvertY(y, chiftTop, chiftBottom, currentSizeImg) {
  return currentSizeImg - (y * (currentSizeImg - chiftBottom - chiftTop) + chiftBottom);
}

function HeatMap() {
  const history = useHistory();
  const [cookies] = useCookies(['steamID']);
  const [mapName, setMapName] = useState('overpass');
  const [heatMaps, setHeatMaps] = useState([]);

  const mapsName = [
    { label: 'Abbey', value: 'abbey' },
    { label: 'Agency', value: 'agency' },
    { label: 'Ancient', value: 'ancient' },
    { label: 'Anubis', value: 'anubis' },
    { label: 'Apollo', value: 'apollo' },
    { label: 'Assault', value: 'assault' },
    { label: 'Biome', value: 'biome' },
    { label: 'Breach', value: 'breach' },
    { label: 'Cache', value: 'cache' },
    { label: 'Chlorine', value: 'chlorine' },
    { label: 'Cobblestone', value: 'cbble' },
    { label: 'Dust2', value: 'dust2' },
    { label: 'Engage', value: 'engage' },
    { label: 'Grind', value: 'grind' },
    { label: 'Inferno', value: 'inferno' },
    { label: 'Italy', value: 'italy' },
    { label: 'Militia', value: 'militia' },
    { label: 'Mirage', value: 'mirage' },
    { label: 'Mocha', value: 'mocha' },
    { label: 'Mutiny', value: 'mutiny' },
    { label: 'Nuke', value: 'nuke' },
    { label: 'Office', value: 'office' },
    { label: 'Overpass', value: 'overpass' },
    { label: 'Swamp', value: 'swamp' },
    { label: 'Train', value: 'train' },
    { label: 'Workout', value: 'workout' },
    { label: 'Vertigo', value: 'vertigo' },
    { label: 'Zoo', value: 'zoo' },
  ];

  const [weaponsKiller, setWeaponsKiller] = useState([]);
  const [activeWeaponsVictim, setActiveWeaponsVictim] = useState([]);
  const [filters, setFilters] = useState({
    durationMin: 0,
    durationMax: 170,
    steamIDKillers: ['*'],
    steamIDVictims: ['*'],
    dmgnMin: 0,
    dmgMax: 170,
  });

  const [heatmapInstanceKills, setHeatmapInstanceKills] = useState();
  const [heatmapInstanceDeaths, setHeatmapInstanceDeaths] = useState();
  const [heatmapInstanceDmg, setHeatmapInstanceDmg] = useState();
  const [heatmapInstanceDmgTook, setHeatmapInstanceDmgTook] = useState();

  const [sidesSelected, setSidesSelected] = useState([{
    side: '2',
    label: 'T',
    checked: true,
  }, {
    side: '3',
    label: 'CT',
    checked: true,
  }]);
  const [sidePlayer, setSidePlayer] = useState(['2', '3']);

  const currentSizeImg = 400;

  useEffect(() => {
    const getHeatMaps = async () => {
      try {
        const steamIDURL = window.location.pathname.split('/')[2];
        const response = await apiDemo.getHeatMaps(steamIDURL, mapName);
        setHeatMaps(response.data.data);
        if (response.data.data.length === 0) {
          toast.info('You first need to register tokens in your profile and to play one game', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
      } catch (err) {
        if (err.response === undefined) {
          toast.error('An error occurred', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          return;
        }
        if (err.response.status === 429) {
          toast.error('Too many request !', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          return;
        }
        if (err.response.status === 403 && err.response.data.title === 'ErrBadTokenConnection') {
          toast.info('You have been disconnected', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          history.push({
            pathname: '/',
          });
          return;
        }

        toast.error('An error occurred', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    };
    getHeatMaps();
  }, [mapName]);

  useEffect(() => {
    const steamIDURL = window.location.pathname.split('/')[2];

    if (steamIDURL === 'undefined') {
      history.push('/home');
    }

    if (!steamIDURL && cookies.steamID !== undefined) {
      history.push(`/home/${cookies.steamID}`);
    }

    setHeatmapInstanceKills(h337.create({
      container: document.querySelector('.heatmapKills'),
    }));
    setHeatmapInstanceDeaths(h337.create({
      container: document.querySelector('.heatmapDeaths'),
    }));
    setHeatmapInstanceDmg(h337.create({
      container: document.querySelector('.heatmapDmg'),
    }));
    setHeatmapInstanceDmgTook(h337.create({
      container: document.querySelector('.heatmapDmgTook'),
    }));
  }, []);

  const updateHeatMap = async () => {
    const chifts = map.returnSettingsByMap(mapName, currentSizeImg);

    const pointsKills = [];
    const pointsDeaths = [];
    const pointsDmg = [];
    const pointsDmgTook = [];

    heatMaps[0].forEach((heatMapKill) => {
      if (filters.durationMin < heatMapKill.durationSinceRoundBegan
          && heatMapKill.durationSinceRoundBegan < filters.durationMax) {
        if (heatMapKill.weaponKiller !== null
              && weaponsKiller.includes(heatMapKill.weaponKiller.toString())) {
          if (heatMapKill.activeWeaponVictim !== null
                && activeWeaponsVictim.includes(heatMapKill.activeWeaponVictim.toString())) {
            if (heatMapKill.sideKiller !== null
                    && sidePlayer.includes(heatMapKill.sideKiller.toString())) {
              if (heatMapKill.killerPosX !== null && heatMapKill.killerPosY !== null) {
                const x = ConvertX(heatMapKill.killerPosX, chifts[0], chifts[1], currentSizeImg);
                const y = ConvertY(heatMapKill.killerPosY, chifts[2], chifts[3], currentSizeImg);
                const point = {
                  x: Math.round(x),
                  y: Math.round(y),
                  value: 1,
                  radius: 4,
                };
                pointsKills.push(point);
              }
            }

            if (heatMapKill.sideVictim !== null
              && sidePlayer.includes(heatMapKill.sideVictim.toString())) {
              if (heatMapKill.victimPosX !== null && heatMapKill.victimPosY !== null) {
                const x = ConvertX(heatMapKill.victimPosX, chifts[0], chifts[1], currentSizeImg);
                const y = ConvertY(heatMapKill.victimPosY, chifts[2], chifts[3], currentSizeImg);
                const point = {
                  x: Math.round(x),
                  y: Math.round(y),
                  value: 1,
                  radius: 4,
                };
                pointsDeaths.push(point);
              }
            }
          }
        }
      }
    });

    heatMaps[1].forEach((heatMapDmg) => {
      if (filters.durationMin < heatMapDmg.durationSinceRoundBegan
        && heatMapDmg.durationSinceRoundBegan < filters.durationMax) {
        if (heatMapDmg.weaponShooter !== null
          && weaponsKiller.includes(heatMapDmg.weaponShooter.toString())) {
          if (heatMapDmg.activeWeaponVictim !== null
            && activeWeaponsVictim.includes(heatMapDmg.activeWeaponVictim.toString())) {
            if (heatMapDmg.sideShooter !== null
                      && sidePlayer.includes(heatMapDmg.sideShooter.toString())) {
              if (heatMapDmg.shooterPosX !== null && heatMapDmg.shooterPosY !== null) {
                const x = ConvertX(heatMapDmg.shooterPosX, chifts[0], chifts[1], currentSizeImg);
                const y = ConvertY(heatMapDmg.shooterPosY, chifts[2], chifts[3], currentSizeImg);
                const point = {
                  x: Math.round(x),
                  y: Math.round(y),
                  value: heatMapDmg.dmg,
                  radius: 3,
                };
                pointsDmg.push(point);
              }
            }

            if (heatMapDmg.sideVictim !== null
              && sidePlayer.includes(heatMapDmg.sideVictim.toString())) {
              if (heatMapDmg.victimPosX !== null && heatMapDmg.victimPosY !== null) {
                const x = ConvertX(heatMapDmg.victimPosX, chifts[0], chifts[1], currentSizeImg);
                const y = ConvertY(heatMapDmg.victimPosY, chifts[2], chifts[3], currentSizeImg);
                const point = {
                  x: Math.round(x),
                  y: Math.round(y),
                  value: heatMapDmg.dmg,
                  radius: 3,
                };
                pointsDmgTook.push(point);
              }
            }
          }
        }
      }
    });

    heatmapInstanceKills.setData({
      max: Math.ceil(Math.log(pointsKills.length) / 2),
      data: pointsKills,
    });

    heatmapInstanceDeaths.setData({
      max: Math.ceil(Math.log(pointsDeaths.length) / 2),
      data: pointsDeaths,
    });

    heatmapInstanceDmg.setData({
      max: 100 + Math.ceil(pointsDmg.length / 10),
      data: pointsDmg,
    });

    heatmapInstanceDmgTook.setData({
      max: 100 + Math.ceil(pointsDmgTook.length / 10),
      data: pointsDmgTook,
    });
  };

  useEffect(() => {
    if (heatmapInstanceKills === undefined
        || heatmapInstanceDeaths === undefined
        || heatmapInstanceDmg === undefined
        || heatmapInstanceDmgTook === undefined) {
      return;
    }
    if (heatMaps.length === 0) {
      return;
    }
    updateHeatMap();
  }, [heatmapInstanceKills,
    heatmapInstanceDeaths,
    heatmapInstanceDmg,
    heatmapInstanceDmgTook,
    filters,
    weaponsKiller,
    activeWeaponsVictim,
    heatMaps,
    sidePlayer,
  ]);

  function handleChangeSideSelected(event) {
    const newSides = [];
    const sidesPicked = [];

    sidesSelected.forEach((sidePicked, numSidePicked) => {
      newSides.push(sidePicked);
      if (sidePicked.side.toString() === event.target.name) {
        newSides[numSidePicked].checked = event.target.checked;
      }
      if (newSides[numSidePicked].checked) {
        sidesPicked.push(sidePicked.side);
      }
    });

    setSidesSelected(newSides);
    setSidePlayer(sidesPicked);
  }

  return (
    <div
      style={{
        backgroundAttachment: 'fixed',
        backgroundImage: `url(${process.env.PUBLIC_URL}/background/${mapName}.jpg)`,
        // backgroundSize: 'cover',
        minHeight: '100vh',
      }}
    >
      <Container>
        <Box alignSelf="stretch" display="flex" flexDirection="row" justifyContent="center">
          <Box>
            <Box alignSelf="stretch" display="flex" flexDirection="row" justifyContent="center">
              <Box
                component={Paper}
                alignSelf="stretch"
                display="flex"
                flexDirection="column"
                justifyContent="center"
              >
                <Typography align="center">
                  Killers positions
                </Typography>
                <div
                  className="heatmapKills"
                  width={currentSizeImg}
                  height={currentSizeImg}
                  style={{
                    backgroundImage: `url(${process.env.PUBLIC_URL}/radar/${mapName}.png)`,
                    height: currentSizeImg,
                    width: currentSizeImg,
                    backgroundSize: 'cover',
                  }}
                />
              </Box>
              <Box
                component={Paper}
                alignSelf="stretch"
                display="flex"
                flexDirection="column"
                justifyContent="center"
              >
                <Typography align="center">
                  Killed positions
                </Typography>
                <div
                  className="heatmapDeaths"
                  width={currentSizeImg}
                  height={currentSizeImg}
                  style={{
                    backgroundImage: `url(${process.env.PUBLIC_URL}/radar/${mapName}.png)`,
                    height: currentSizeImg,
                    width: currentSizeImg,
                    backgroundSize: 'cover',
                  }}
                />
              </Box>
            </Box>
            <Box
              component={Paper}
              margin={{
                top: 40, right: 50, left: 50, bottom: 40,
              }}
              m={1}
            >
              <Box alignSelf="stretch" display="flex" justifyContent="center">
                <FormControl>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={mapName}
                    onChange={(event) => setMapName(event.target.value)}
                  >
                    {mapsName.map((mName) => (
                      <MenuItem key={mName.value} value={mName.value}>{mName.label}</MenuItem>
                    ))}
                  </Select>
                </FormControl>
                <WeaponsPicker
                  weaponsKiller={weaponsKiller}
                  setWeaponsKiller={setWeaponsKiller}
                  label="Pick the weapons of the shooters"
                />
                <WeaponsPicker
                  weaponsKiller={activeWeaponsVictim}
                  setWeaponsKiller={setActiveWeaponsVictim}
                  label="Pick the weapons of the victims"
                />
              </Box>
              <Box alignSelf="stretch" display="flex" justifyContent="center">
                <FormControl component="fieldset">
                  <FormGroup>
                    {sidesSelected.map((sideSelected) => (
                      <FormControlLabel
                        key={sideSelected.side}
                        control={<Checkbox checked={sideSelected.checked} onChange={(e) => handleChangeSideSelected(e)} />}
                        name={sideSelected.side}
                        label={sideSelected.label}
                      />
                    ))}
                  </FormGroup>
                </FormControl>
              </Box>
              <Slider
                value={[filters.durationMin, filters.durationMax]}
                onChange={(event, newValues) => setFilters({
                  ...filters,
                  durationMin: newValues[0],
                  durationMax: newValues[1],
                })}
                valueLabelDisplay="auto"
                aria-labelledby="range-slider"
                max={170}
              />
            </Box>
            <Box
              alignSelf="stretch"
              display="flex"
              flexDirection="row"
              justifyContent="center"
            >
              <Box
                component={Paper}
                alignSelf="stretch"
                display="flex"
                flexDirection="column"
                justifyContent="center"
              >
                <Typography align="center">
                  Shooters positions
                </Typography>
                <div
                  className="heatmapDmg"
                  width={currentSizeImg}
                  height={currentSizeImg}
                  style={{
                    backgroundImage: `url(${process.env.PUBLIC_URL}/radar/${mapName}.png)`,
                    height: currentSizeImg,
                    width: currentSizeImg,
                    backgroundSize: 'cover',
                  }}
                />
              </Box>
              <Box
                component={Paper}
                alignSelf="stretch"
                display="flex"
                flexDirection="column"
                justifyContent="center"
              >
                <Typography align="center">
                  Victims positions
                </Typography>
                <div
                  className="heatmapDmgTook"
                  width={currentSizeImg}
                  height={currentSizeImg}
                  style={{
                    backgroundImage: `url(${process.env.PUBLIC_URL}/radar/${mapName}.png)`,
                    height: currentSizeImg,
                    width: currentSizeImg,
                    backgroundSize: 'cover',
                  }}
                />
              </Box>
            </Box>
          </Box>
        </Box>
      </Container>
    </div>
  );
}

export default HeatMap;
