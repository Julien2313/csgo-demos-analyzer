import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Box, AppBar, Tabs, Tab, Typography,
} from '@material-ui/core';

import SwipeableViews from 'react-swipeable-views';
import { toast } from 'react-toastify';

import apiDemo from '../api/demo';

import DM from '../components/progression/dm';
import MM from '../components/progression/mm';

function TabPanel({
  children, value, index,
}) {
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
    >
      {value === index && (
        <Box p={3}>
          <Typography component="span">{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function Progression() {
  const history = useHistory();
  const [dmDemos, setDMDemos] = useState();
  const [value, setValue] = useState(0);

  const getAllDeathMatchDemos = async () => {
    try {
      const response = await apiDemo.getAllDeathMatchDemos();
      setDMDemos(response.data.data);
    } catch (err) {
      if (err.response === undefined) {
        toast.error('An error occurred', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        return;
      }
      if (err.response.status === 429) {
        toast.error('Too many request !', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        return;
      }
      if (err.response.status === 403 && err.response.data.title === 'ErrBadTokenConnection') {
        toast.info('You have been disconnected', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        history.push({
          pathname: '/',
        });
        return;
      }
      if (err.response.status === 400) {
        toast.info('You must set your steamID in your profile to be able to see your progression !', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        return;
      }

      toast.error('An error occurred', {
        position: 'top-center',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  };

  useEffect(() => {
    // getAllDeathMatchDemos();
  }, []);

  return (
    <div>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={(event, newValue) => setValue(newValue)}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          aria-label="full width tabs example"
        >
          <Tab label="Competitives" />
          <Tab label="Death matchs" />
        </Tabs>
      </AppBar>
      <SwipeableViews
        index={value}
        onChangeIndex={(index) => setValue(index)}
      >
        <TabPanel value={value} index={0}>
          <MM />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <DM demos={dmDemos} />
        </TabPanel>
      </SwipeableViews>
    </div>
  );
}

export default Progression;
