import React from 'react';

import { Container } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Login from '../components/login';
import Register from '../components/register';
import GetLastAnalyzed from '../components/demo/getLastAnalyzed';

function Landingpage() {
  return (
    <Container>
      <Grid
        container
        justify="center"
        alignItems="center"
      >
        <img alt="poulet" src={`${process.env.PUBLIC_URL}/img/julienlepoulet.png`} />
      </Grid>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
      >
        <Login />
        <Register />
      </Grid>
      <GetLastAnalyzed />
    </Container>
  );
}

export default Landingpage;
