import React, {
  useContext, useEffect, useState,
} from 'react';

import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import {
  Container, FormControl, InputLabel, Select, MenuItem, Box,
} from '@material-ui/core';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';

import {
  Refresh as RefreshIcon,
  SkipPrevious as SkipPreviousIcon,
  SkipNext as SkipNextIcon,
  Clear as ClearIcon,
} from '@material-ui/icons';
import IconButton from '@material-ui/core/IconButton';
import { useCookies } from 'react-cookie';

import { ContextDemos } from '../context';
import CalendarGames from '../components/user/calendar';
import GetAll from '../components/demo/getAll';

import CheckNewDemo from '../components/buttons/checkNewDemo';

import apiDemo from '../api/demo';

function Home() {
  const [nextDisabled, setNextDisabled] = useState(false);
  const [isNewDemo, setIsNewDemo] = useState(false);
  const history = useHistory();
  const [cookies] = useCookies(['steamID']);
  const {
    demosDisplay, setDemosDisplay,
    limit, setLimit,
    offset, setOffset,
    selectedDateStart, setSelectedDateStart,
    selectedDateEnd, setSelectedDateEnd,
  } = useContext(ContextDemos);

  const getAllDemos = async () => {
    let demos = [];

    try {
      const steamIDURL = window.location.pathname.split('/')[2];
      setDemosDisplay({ demos: [], isGettingDemos: true });
      const sources = [1, 2, 3, 4];
      const payload = {
        limit,
        offset,
        sources,
        selectedDateStart,
        selectedDateEnd,
      };
      const response = await apiDemo.getAllDemos(steamIDURL, payload);
      demos = response.data.data.demos;
      const { nbrDemosToAnalyze } = response.data.data;
      if (nbrDemosToAnalyze > 0) {
        toast.info(`You have ${nbrDemosToAnalyze} game${nbrDemosToAnalyze > 1 ? 's' : ''} being analyzed`, {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      } else if (response.data.data.length === 0) {
        toast.info('You first need to register tokens in your profile and to play one game', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    } catch (err) {
      if (err.response === undefined) {
        toast.error('An error occurred', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        return;
      }
      if (err.response.status === 403 && err.response.data.title === 'ErrBadTokenConnection') {
        toast.info('You have been disconnected', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        history.push({
          pathname: '/',
        });
        return;
      }

      switch (err.response.status) {
        case 403:
          toast.info('You first need to register tokens in your profile and wait or hit "check new demo"', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          break;
        case 429:
          toast.error('Too many request !', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          break;
        default:
          toast.error('An error occurred', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          break;
      }
    } finally {
      if (demos.length < limit) {
        setNextDisabled(true);
      } else {
        setNextDisabled(false);
      }
      setDemosDisplay({ demos, isGettingDemos: false });
    }
  };

  useEffect(() => {
    getAllDemos();
  }, [limit, offset, selectedDateStart, selectedDateEnd]);

  useEffect(() => {
    const steamIDURL = window.location.pathname.split('/')[2];

    if (steamIDURL === 'undefined') {
      history.push('/home');
    }

    if (!steamIDURL && cookies.steamID !== undefined) {
      history.push(`/home/${cookies.steamID}`);
    }
  }, []);

  return (
    <Container align="right">
      <CalendarGames />
      <Box
        display="flex"
        alignItems="flex-end"
      >
        <CheckNewDemo setIsNewDemo={setIsNewDemo} />
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardDatePicker
            disableToolbar
            variant="inline"
            format="MM/dd/yyyy"
            margin="normal"
            id="date-picker-inline"
            label="From"
            value={selectedDateStart}
            onChange={(date) => { setSelectedDateStart(date.setHours(0, 0, 0, 0)); setOffset(0); }}
            KeyboardButtonProps={{
              'aria-label': 'change date',
            }}
          />
          <IconButton
            // style={{ padding: 0 }}
            edge="end"
            size="small"
            disabled={!selectedDateStart}
            onClick={() => { setSelectedDateStart(null); setOffset(0); }}
          >
            <ClearIcon />
          </IconButton>
          <KeyboardDatePicker
            disableToolbar
            variant="inline"
            format="MM/dd/yyyy"
            margin="normal"
            id="date-picker-inline"
            label="To"
            value={selectedDateEnd}
            onChange={(date) => { setSelectedDateEnd(date.setHours(0, 0, 0, 0)); setOffset(0); }}
            KeyboardButtonProps={{
              'aria-label': 'change date',
            }}
          />
          <IconButton
            // style={{ padding: 0 }}
            edge="end"
            size="small"
            disabled={!selectedDateEnd}
            onClick={() => { setSelectedDateEnd(null); setOffset(0); }}
          >
            <ClearIcon />
          </IconButton>
        </MuiPickersUtilsProvider>
        <IconButton onClick={() => setOffset(Math.max(0, offset - limit))}>
          <SkipPreviousIcon />
        </IconButton>
        <IconButton onClick={getAllDemos}>
          <RefreshIcon />
        </IconButton>
        <IconButton onClick={() => setOffset(offset + limit)} disabled={nextDisabled}>
          <SkipNextIcon />
        </IconButton>
        <FormControl variant="outlined">
          <InputLabel id="demo-simple-select-outlined-label">Limit</InputLabel>
          <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            value={limit}
            onChange={(event) => setLimit(event.target.value)}
            label="Limit"
          >
            <MenuItem value={10}>10</MenuItem>
            <MenuItem value={25}>25</MenuItem>
            <MenuItem value={50}>50</MenuItem>
          </Select>
        </FormControl>
      </Box>
      <GetAll demos={demosDisplay} isNewDemo={isNewDemo} />
    </Container>
  );
}

export default Home;
