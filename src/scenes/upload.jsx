/* eslint-disable no-continue */
import React, { useState } from 'react';

import { useHistory } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import LinearProgress from '@material-ui/core/LinearProgress';
import { toast } from 'react-toastify';
import {
  Container, Paper, Box,
} from '@material-ui/core';
import { map } from 'lodash';

import apiDemo from '../api/demo';

const uploadDemo = async (demo, setProgress, setStatus, setDemoID) => {
  if (demo === undefined) {
    return;
  }

  try {
    setStatus(1);
    const data = new FormData();
    data.append('demo', demo);
    const response = await apiDemo.uploadDemo(data, demo.lastModified, setProgress);
    data.delete('demo');
    const demoID = response.data.data;
    setDemoID(demoID);
    setStatus(2);
  } catch (err) {
    setStatus(3);
    if (err.response === undefined) {
      toast.error('An error occurred', {
        position: 'top-center',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      return;
    }
    switch (err.response.status) {
      case 429:
        toast.error('Too many request !', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        break;
      default:
        toast.error('An error occurred', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        break;
    }
  }
};

function DisplayDemoUploading(values) {
  const { demo } = values;
  const history = useHistory();
  const [progress, setProgress] = useState(0);
  const [status, setStatus] = useState(0);
  const [demoID, setDemoID] = useState();

  const statusUpload = () => {
    switch (status) {
      case 0:
        return '#000000';
      case 1:
        return '#FF7F00';
      case 2:
        return '#00FF00';
      case 3:
        return '#FF0000';
      default:
        return '#FF0000';
    }
  };

  let linkDemo;

  if (status === 2) {
    linkDemo = (
      <Box
        onClick={() => history.push({
          pathname: `/demo/stats/${demoID}`,
        })}
      >
        Click on the button to get the demo !
        <Button
          variant="contained"
          color="primary"
          onClick={() => history.push({
            pathname: `/demo/stats/${demoID}`,
          })}
          size="large"
        >
          to the demo
        </Button>
        {' '}
        It should soon be available if no error occured during pasrsing or if the demo doesn&apos;t already exists
      </Box>
    );
  }

  return (
    <Box marginBottom="10px">
      <Box display="flex" flexDirection="row">
        <Box>
          <Button
            variant="contained"
            color="default"
            startIcon={<CloudUploadIcon />}
            onClick={() => uploadDemo(demo, setProgress, setStatus, setDemoID)}
          >
            Upload
          </Button>
        </Box>
        <Box>
          <Paper style={{
            backgroundColor: statusUpload(), height: '18px', width: '18px', borderRadius: '50%',
          }}
          />
        </Box>
        <Box>
          {`${progress}%`}
        </Box>
        {linkDemo}
      </Box>
      <LinearProgress variant="determinate" value={progress} />
    </Box>
  );
}

function DisplayDemosUploading(values) {
  const { demos } = values;
  return map(demos, (demo, idx) => <DisplayDemoUploading key={idx} demo={demo} />);
}

function Upload() {
  const [demos, setDemos] = useState([]);

  return (
    <Container>
      You can easily upload demos here !
      <br />
      If you don&apos;t have error while uploading the demo but can&apos;t
      see it in the next after 5 minutes, feel free to contact us on
      {' '}
      <a href="https://gitter.im/csgo-demos-analyzer/community">gitter</a>
      .
      <br />
      <input
        type="file"
        multiple
        onChange={(newDemos) => { setDemos([...newDemos.target.files]); }}
      />
      <br />
      <DisplayDemosUploading demos={demos} />
    </Container>
  );
}

export default Upload;
