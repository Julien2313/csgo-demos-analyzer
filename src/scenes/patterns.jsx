import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useCookies } from 'react-cookie';
import { useForm } from 'react-hook-form';

import {
  Container,
  Box,
  Paper,
} from '@material-ui/core';
import { Line } from 'react-chartjs-2';

import apiDemo from '../api/demo';

function PatternUser(values) {
  const { weaponsPatterns } = values;
  return (
    <Box
      display="flex"
      flexWrap="wrap"
    >
      {weaponsPatterns.map((pattern) => (
        <Pattern
          key={pattern[0].label}
          pattern={pattern}
        />
      ))}
    </Box>
  );
}

function Pattern(values) {
  const { pattern } = values;

  return (
    <Box
      display="flex"
      height="465px"
      width="300px"
      component={Paper}
      marginRight="10px"
      marginLeft="10px"
      marginTop="10px"
      marginBottom="10px"
    >
      <Line
        data={{
          datasets: pattern,
        }}
        options={{
          tooltips: {
            enabled: false,
          },
          maintainAspectRatio: false,
          scales: {
            yAxes: [{
              ticks: {
                min: -10,
                max: 1,
                display: false,
              },
              type: 'linear',
              position: 'left',
              id: 'y-axis-linear',
            }],
            xAxes: [{
              ticks: {
                min: -4,
                max: 4,
                display: false,
              },
              type: 'linear',
              position: 'bottom',
              id: 'x-axis-linear',
            }],
          },
          title: {
            display: true,
            text: pattern[0].label,
            fontSize: 20,
          },
          legend: {
            display: false,
          },
        }}
      />
    </Box>
  );
}

function Patterns() {
  const history = useHistory();
  const { register, handleSubmit, errors } = useForm();
  const [cookies] = useCookies(['steamID']);

  const [weaponsPatterns, setWeaponsPatterns] = useState([]);
  const [rawPatternsEnnemy, setRawPatternsEnnemy] = useState([]);

  const [rawPatterns, setRawPatterns] = useState([]);

  const setSteamID = async (steamIDForm) => {
    try {
      const response = await apiDemo.getPatterns(steamIDForm.ID);
      setRawPatternsEnnemy(response.data.data);
    } catch (err) {
      if (err.response === undefined) {
        toast.error('An error occurred', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        return;
      }

      switch (err.response.status) {
        case 400:
          toast.error('Not a good steamID', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          break;
        case 429:
          toast.error('Too many request !', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          break;
        default:
          toast.error('An error occurred', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          break;
      }
    }
  };

  useEffect(() => {
    const userSpray = [];

    rawPatterns.forEach((rawPattern) => {
      const patterns = [];
      rawPattern.bulletStats.forEach((bulletStats) => {
        if (bulletStats.nbrBulletFired === 0) {
          bulletStats.nbrBulletFired = 1;
        }
        patterns.push({
          x: bulletStats.cumulativeDeltaX / bulletStats.nbrBulletFired,
          y: bulletStats.cumulativeDeltaY / bulletStats.nbrBulletFired,
        });
      });
      const newPatternWeapon = [{
        label: `${rawPattern.weaponType} - Accuracy ${Math.round(rawPattern.markPattern)}%`,
        data: patterns,
        backgroundColor: '#00000000',
        borderColor: '#00FF00',
        borderWidth: 1,
        pointBackgroundColor: '#00FF00',
        pointHoverBorderColor: '#00FF00',
        pointHoverBorderWidth: 1,
        pointHoverRadius: 1,
        pointRadius: 2,
        yAxisID: 'y-axis-linear',
        xAxisID: 'x-axis-linear',
      }];
      if (rawPattern.perfectPattern !== null) {
        const perfectPatterns = [];
        rawPattern.perfectPattern.forEach((perfectPattern) => {
          perfectPatterns.push({
            x: perfectPattern.cumulativeDeltaX,
            y: perfectPattern.cumulativeDeltaY,
          });
        });
        newPatternWeapon.push({
          label: `${rawPattern.weaponType} - perfect`,
          data: perfectPatterns,
          backgroundColor: '#00000000',
          borderColor: '#FF0000',
          borderWidth: 1,
          pointBackgroundColor: '#FF0000',
          pointHoverBorderColor: '#FF0000',
          pointHoverBorderWidth: 1,
          pointHoverRadius: 1,
          pointRadius: 2,
          yAxisID: 'y-axis-linear',
          xAxisID: 'x-axis-linear',
        });
      }

      rawPatternsEnnemy.forEach((rawPatternEnnemy) => {
        if (rawPattern.weaponType === rawPatternEnnemy.weaponType) {
          const patternsEnnemy = [];
          rawPatternEnnemy.bulletStats.forEach((bulletStatsEnnemy) => {
            if (bulletStatsEnnemy.nbrBulletFired === 0) {
              bulletStatsEnnemy.nbrBulletFired = 1;
            }
            patternsEnnemy.push({
              x: bulletStatsEnnemy.cumulativeDeltaX / bulletStatsEnnemy.nbrBulletFired,
              y: bulletStatsEnnemy.cumulativeDeltaY / bulletStatsEnnemy.nbrBulletFired,
            });
          });

          newPatternWeapon.push({
            label: 'Other player',
            data: patternsEnnemy,
            backgroundColor: '#00000000',
            borderColor: '#0000FF',
            borderWidth: 1,
            pointBackgroundColor: '#0000FF',
            pointHoverBorderColor: '#0000FF',
            pointHoverBorderWidth: 1,
            pointHoverRadius: 1,
            pointRadius: 2,
            yAxisID: 'y-axis-linear',
            xAxisID: 'x-axis-linear',
          });
        }
      });
      userSpray.push(newPatternWeapon);
    });
    setWeaponsPatterns(userSpray);
  }, [rawPatterns, rawPatternsEnnemy]);

  useEffect(() => {
    const getPatterns = async () => {
      try {
        const steamIDURL = window.location.pathname.split('/')[2];
        const response = await apiDemo.getPatterns(steamIDURL);
        setRawPatterns(response.data.data);
        if (response.data.data.length === 0) {
          toast.info('You first need to register tokens in your profile and to play one game', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
      } catch (err) {
        if (err.response === undefined) {
          toast.error('An error occurred', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          return;
        }
        if (err.response.status === 429) {
          toast.error('Too many request !', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          return;
        }
        if (err.response.status === 403 && err.response.data.title === 'ErrBadTokenConnection') {
          toast.info('You have been disconnected', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          history.push({
            pathname: '/',
          });
          return;
        }

        toast.error('An error occurred', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    };
    getPatterns();
  }, []);

  useEffect(() => {
    const steamIDURL = window.location.pathname.split('/')[2];

    if (steamIDURL === 'undefined') {
      history.push('/home');
    }

    if (!steamIDURL && cookies.steamID !== undefined) {
      history.push(`/home/${cookies.steamID}`);
    }
  }, []);

  if (weaponsPatterns.length === 0) {
    return (
      <Box display="flex" justifyContent="center">
        Compare your pattern with another player :
        <form style={{
          marginLeft: 20,
        }}
        >
          <input name="ID" placeholder="76685423698547563" ref={register({ required: true })} />
          {errors.ID && <span>This field is required</span>}
          <input type="submit" value="set steamID" />
        </form>
      </Box>
    );
  }

  return (
    <Container>
      <Box display="flex" flexDirection="column">
        <Box display="flex" justifyContent="center" flexDirection="row">
          <Box>
            Compare your pattern with another player :
          </Box>
          <Box>
            <form
              onSubmit={handleSubmit(setSteamID)}
            >
              <input name="ID" placeholder="76685423698547563" ref={register({ required: true })} />
              {errors.ID && <span>This field is required</span>}
              <input type="submit" value="set steamID" />
            </form>
          </Box>
        </Box>
        <Box display="flex" justifyContent="center">
          <PatternUser weaponsPatterns={weaponsPatterns} />
        </Box>
      </Box>
    </Container>
  );
}

export default Patterns;
