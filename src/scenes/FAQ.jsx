import React from 'react';

import {
  Container,
  Box,
  Typography,
} from '@material-ui/core';

function FAQ() {
  return (
    <Container>
      <Typography variant="h3">
        Frequently Asked Questions
      </Typography>
      <br />
      <Typography variant="h6">
        I&apos;ve seen a bug, where and how can I report it ?!
      </Typography>
      <Box marginLeft="20px">
        <Typography variant="body1" paragraph>
          You can let me a message on my
          {' '}
          <a href="https://gitter.im/csgo-demos-analyzer/community">gitter</a>
          .
          <br />
          <img alt="honk" src={`${process.env.PUBLIC_URL}/img/honk.jpg`} />
        </Typography>
      </Box>

      <Typography variant="h6">
        Can I believe in CSGOfacts ?
      </Typography>
      <Box marginLeft="20px">
        <Typography variant="body1">
          For what it&apos;s worth, your email address will never be sold.
          <br />
          Your password is stored hashed using
          {' '}
          <a href="https://en.wikipedia.org/wiki/Bcrypt">bcrypt</a>
          {' '}
          with a cost of 10. It&apos;s never written in the logs.
          <br />
          Nevertheless, you are free to use an unique password for this website.
          <br />
          <br />
          We ask you, to automatically download demos, 3 information: SteamID, Sharecode and Authentication Code.
          <br />
          Your steamID is public and can be found very easily with your steam name, we just can&apos;t guess it if don&apos;t have it.
          <br />
          A Sharecode isn&apos;t really private as it is the same for all the players in 1 game, but it is not public either, that&apos;s why we are asking you to provide it one time.
          <br />
          Afterwards, we will be able to automatically retrive the next ones.
          <br />
          The Authentication Code allow us to require your match history to valve. As Valve wrote
          {' '}
          <a href="https://help.steampowered.com/en/wizard/HelpWithGameIssue/?appid=730&issueid=128">here</a>
          :
        </Typography>
        <br />
        <Box marginLeft="40px">
          <Typography variant="caption" display="block" gutterBottom paragraph>
            You can create game authentication codes to allow third-party websites and applications to manage your game without running the actual game client.
            <br />
            Steam Support doesn&apos;t review or endorse third-party websites or applications that request access to your Game Authentication Codes, and can take no responsibility for those websites or applications.
            If you don&apos;t trust the website or application that is requesting your information, you should not approve the request.
            <br />
            Third-party websites and applications can use this authentication code to access your match history, your overall performance in those matches, download replays of your matches, and analyze your gameplay.
          </Typography>
        </Box>
        <Typography variant="body1">
          This does NOT mean we will be able to manage your market/inventory, or even be able to connect to your account.
          <br />
          <b>We will only use this code to automatically retrieve your matches so that we can analyze them. We undertake not to commit any malicious action towards you.</b>
          <br />
          At any time, you&apos;ll be able de revoke it using the same link
        </Typography>
      </Box>
    </Container>
  );
}

export default FAQ;
