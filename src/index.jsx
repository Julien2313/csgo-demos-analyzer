import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { ToastContainer } from 'react-toastify';
import Navigator from './navigator';


ReactDOM.render(
  <div>
    {/* <React.StrictMode> */}
    <ToastContainer
      position="top-center"
      autoClose={5000}
      hideProgressBar={false}
      newestOnTop={false}
      closeOnClick
      rtl={false}
      pauseOnFocusLoss
      draggable
      pauseOnHover
    />
    <Navigator />
    {/* </React.StrictMode>, */}
  </div>,
  document.getElementById('root'),
);
