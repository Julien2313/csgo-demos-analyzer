import Axios from 'axios';

const apiClient = Axios.create({
  baseURL: process.env.REACT_APP_BASEURL,
  withCredentials: true,
});


export default apiClient;
