import React from 'react';
import { Radar } from 'react-chartjs-2';

import {
  Box, Container,
} from '@material-ui/core';

function ComputeMarksPlayer(marks) {
  const marksPlayer = marks.marks;
  const { bestMarks, baddestMarks } = marks;
  const radarHS = (marksPlayer.hs - baddestMarks.hs) / (bestMarks.hs - baddestMarks.hs);
  const radarUtilityDamage = (marksPlayer.utilityDamage - baddestMarks.utilityDamage) / (bestMarks.utilityDamage - baddestMarks.utilityDamage);
  const radarGrenadesValueDeath = 1 - (marksPlayer.grenadesValueDeath - baddestMarks.grenadesValueDeath) / (bestMarks.grenadesValueDeath - baddestMarks.grenadesValueDeath);
  const radarAccuracy = (marksPlayer.accuracy - baddestMarks.accuracy) / (bestMarks.accuracy - baddestMarks.accuracy);
  const radardamage = (marksPlayer.damage - baddestMarks.damage) / (bestMarks.damage - baddestMarks.damage);

  return {
    radarHS, radarUtilityDamage, radarGrenadesValueDeath, radarAccuracy, radardamage,
  };
}

function ComputeMarksTeam(values) {
  const { bestMarks } = values;
  const { baddestMarks } = values;
  const { team } = values;

  const data = {
    labels: ['Damage', 'Util. dmg', 'HS', 'Accuracy', 'Stuff lost'],
    datasets: [],
  };

  const colors = ['#808080', '#e6f13d', '#803ca1', '#109856', '#68a3e5', '#eda338'];

  team.players.sort((a, b) => ((a.steamID > b.steamID) ? -1 : 1));
  team.players.forEach((player) => {
    const {
      radarHS, radarUtilityDamage, radarGrenadesValueDeath, radarAccuracy, radardamage,
    } = ComputeMarksPlayer({
      bestMarks, baddestMarks, marks: player.marks,
    });

    const colorIdx = player.color + 1;

    const playerDataset = {
      label: player.username,
      data: [
        radardamage * 100,
        radarUtilityDamage * 100,
        radarHS * 100,
        radarAccuracy * 100,
        radarGrenadesValueDeath * 100,
      ],
      backgroundColor: `${colors[colorIdx]}20`,
      borderColor: colors[colorIdx],
      borderWidth: 2,
      pointBackgroundColor: colors[colorIdx],
    };
    data.datasets.push(playerDataset);
  });


  const options = {
    responsive: true,
    maintainAspectRatio: true,
    scale: {
      ticks: { beginAtZero: true, max: 100, display: false },
      gridLines: {
        color: '#000000',
        lineWidth: 2,
        drawOnChartArea: true,
        borderDash: [10],
      },
    },
    tooltips: {
      enabled: false,
    },
  };

  return (
    <Radar
      data={data}
      options={options}
      width={600}
      height={600}
    />
  );
}

function Marks(values) {
  const { teams } = values;
  const { bestMarks, baddestMarks } = values;

  const renderTeams = teams.map((team, idx) => (
    <Box key={idx} display="flex">
      <ComputeMarksTeam team={team} bestMarks={bestMarks} baddestMarks={baddestMarks} />
    </Box>
  ));

  return (
    <Container style={{ backgroundColor: '#2d374d' }}>
      <Box display="flex" flexDirection="row" justifyContent="space-around">
        {renderTeams}
      </Box>
    </Container>
  );
}

export default Marks;
