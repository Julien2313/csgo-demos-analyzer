import React, { useState, useEffect } from 'react';
import h337 from 'heatmap.js';

import {
  Container, Box, Slider, FormControl, FormGroup, FormControlLabel, Checkbox, Paper, Typography,
} from '@material-ui/core';

import map from '../utils/map';
import WeaponsPicker from '../dialog/weaponsPicker';

function ConvertX(x, chiftLeft, chiftRight, currentSizeImg) {
  return x * (currentSizeImg - chiftLeft - chiftRight) + chiftLeft;
}
function ConvertY(y, chiftTop, chiftBottom, currentSizeImg) {
  return currentSizeImg - (y * (currentSizeImg - chiftBottom - chiftTop) + chiftBottom);
}

function Heatmap(values) {
  const { rawRounds } = values;
  const { mapName } = values;
  const { teams } = values;

  const [weaponsKiller, setWeaponsKiller] = useState([]);
  const [activeWeaponsVictim, setActiveWeaponsVictim] = useState([]);
  const [filters, setFilters] = useState({
    durationMin: 0,
    durationMax: 170,
    steamIDKillers: ['*'],
    steamIDVictims: ['*'],
    dmgnMin: 0,
    dmgMax: 170,
  });

  const [playersSelected, setPlayersSelected] = useState([]);

  const currentSizeImg = 400;

  const [heatmapInstanceKills, setHeatmapInstanceKills] = useState();
  const [heatmapInstanceDeaths, setHeatmapInstanceDeaths] = useState();
  const [heatmapInstanceDmg, setHeatmapInstanceDmg] = useState();
  const [heatmapInstanceDmgTook, setHeatmapInstanceDmgTook] = useState();

  useEffect(() => {
    setHeatmapInstanceKills(h337.create({
      container: document.querySelector('.heatmapKills'),
    }));
    setHeatmapInstanceDeaths(h337.create({
      container: document.querySelector('.heatmapDeaths'),
    }));
    setHeatmapInstanceDmg(h337.create({
      container: document.querySelector('.heatmapDmg'),
    }));
    setHeatmapInstanceDmgTook(h337.create({
      container: document.querySelector('.heatmapDmgTook'),
    }));
  }, []);

  useEffect(() => {
    const players = [];
    teams.forEach((team) => {
      team.players.forEach((player) => (
        players.push({
          steamID: player.steamID,
          label: player.username.slice(0, 10),
          checked: true,
        })));
    });
    setPlayersSelected(players);
  }, [teams]);

  useEffect(() => {
    if (heatmapInstanceKills === undefined
        || heatmapInstanceDeaths === undefined
        || heatmapInstanceDmg === undefined
        || heatmapInstanceDmgTook === undefined) {
      return;
    }
    const chifts = map.returnSettingsByMap(mapName, currentSizeImg);

    const pointsKills = [];
    const pointsDeaths = [];
    const pointsDmg = [];
    const pointsDmgTook = [];

    rawRounds.forEach((round) => {
      round.heatMapKills.forEach((heatMapKill) => {
        if (filters.durationMin < heatMapKill.durationSinceRoundBegan
          && heatMapKill.durationSinceRoundBegan < filters.durationMax) {
          if (weaponsKiller.includes(heatMapKill.weaponKiller.toString())) {
            if (activeWeaponsVictim.includes(heatMapKill.activeWeaponVictim.toString())) {
              if (filters.steamIDKillers.includes('*') || filters.steamIDKillers.includes(heatMapKill.steamIDKiller)) {
                if (heatMapKill.killerPosX !== null && heatMapKill.killerPosY !== null) {
                  const x = ConvertX(heatMapKill.killerPosX, chifts[0], chifts[1], currentSizeImg);
                  const y = ConvertY(heatMapKill.killerPosY, chifts[2], chifts[3], currentSizeImg);
                  const point = {
                    x: Math.floor(x),
                    y: Math.floor(y),
                    value: 10,
                    radius: 7,
                  };
                  pointsKills.push(point);
                }
              }

              if (filters.steamIDVictims.includes('*') || filters.steamIDVictims.includes(heatMapKill.steamIDVictim)) {
                if (heatMapKill.victimPosX !== null && heatMapKill.victimPosY !== null) {
                  const x = ConvertX(heatMapKill.victimPosX, chifts[0], chifts[1], currentSizeImg);
                  const y = ConvertY(heatMapKill.victimPosY, chifts[2], chifts[3], currentSizeImg);
                  const point = {
                    x: Math.floor(x),
                    y: Math.floor(y),
                    value: 10,
                    radius: 7,
                  };
                  pointsDeaths.push(point);
                }
              }
            }
          }
        }
      });

      round.heatMapDmgs.forEach((heatMapDmg) => {
        if (filters.durationMin < heatMapDmg.durationSinceRoundBegan
          && heatMapDmg.durationSinceRoundBegan < filters.durationMax) {
          if (weaponsKiller.includes(heatMapDmg.weaponShooter.toString())) {
            if (activeWeaponsVictim.includes(heatMapDmg.activeWeaponVictim.toString())) {
              if (filters.steamIDKillers.includes('*') || filters.steamIDKillers.includes(heatMapDmg.steamIDShooter)) {
                if (heatMapDmg.shooterPosX !== null && heatMapDmg.shooterPosY !== null) {
                  const x = ConvertX(heatMapDmg.shooterPosX, chifts[0], chifts[1], currentSizeImg);
                  const y = ConvertY(heatMapDmg.shooterPosY, chifts[2], chifts[3], currentSizeImg);
                  const point = {
                    x: Math.floor(x),
                    y: Math.floor(y),
                    value: heatMapDmg.dmg,
                    radius: 4,
                  };
                  pointsDmg.push(point);
                }
              }

              if (filters.steamIDVictims.includes('*') || filters.steamIDVictims.includes(heatMapDmg.steamIDVictim)) {
                if (heatMapDmg.victimPosX !== null && heatMapDmg.victimPosY !== null) {
                  const x = ConvertX(heatMapDmg.victimPosX, chifts[0], chifts[1], currentSizeImg);
                  const y = ConvertY(heatMapDmg.victimPosY, chifts[2], chifts[3], currentSizeImg);
                  const point = {
                    x: Math.floor(x),
                    y: Math.floor(y),
                    value: heatMapDmg.dmg,
                    radius: 4,
                  };
                  pointsDmgTook.push(point);
                }
              }
            }
          }
        }
      });
    });

    heatmapInstanceKills.setData({
      max: 20,
      data: pointsKills,
    });

    heatmapInstanceDeaths.setData({
      max: 20,
      data: pointsDeaths,
    });

    heatmapInstanceDmg.setData({
      max: 100,
      data: pointsDmg,
    });

    heatmapInstanceDmgTook.setData({
      max: 100,
      data: pointsDmgTook,
    });
  }, [heatmapInstanceKills,
    heatmapInstanceDeaths,
    heatmapInstanceDmg,
    heatmapInstanceDmgTook,
    filters,
    weaponsKiller,
    activeWeaponsVictim,
  ]);

  const handleChangePlayers = (event) => {
    const playersFilter = [];
    playersSelected.forEach((player, numPlayer) => {
      if (player.steamID === event.target.name) {
        playersSelected[numPlayer].checked = event.target.checked;
      }
      if (playersSelected[numPlayer].checked) {
        playersFilter.push(playersSelected[numPlayer].steamID);
      }
    });

    setFilters({
      ...filters,
      steamIDKillers: playersFilter,
      steamIDVictims: playersFilter,
    });
    setPlayersSelected(playersSelected);
  };

  return (
    <Container>
      <Box alignSelf="stretch" display="flex" flexDirection="row" justifyContent="center">
        <Box>
          <Box
            alignSelf="stretch"
            display="flex"
            flexDirection="row"
            justifyContent="center"
          >
            <Box
              component={Paper}
              alignSelf="stretch"
              display="flex"
              flexDirection="column"
              justifyContent="center"
            >
              <Typography align="center">
                Killers positions
              </Typography>
              <div
                className="heatmapKills"
                width={currentSizeImg}
                height={currentSizeImg}
                style={{
                  backgroundImage: `url(${process.env.PUBLIC_URL}/radar/${mapName}.png)`,
                  height: currentSizeImg,
                  width: currentSizeImg,
                  backgroundSize: 'cover',
                }}
              />
            </Box>
            <Box
              component={Paper}
              alignSelf="stretch"
              display="flex"
              flexDirection="column"
              justifyContent="center"
            >
              <Typography align="center">
                Killed positions
              </Typography>
              <div
                className="heatmapDeaths"
                width={currentSizeImg}
                height={currentSizeImg}
                style={{
                  backgroundImage: `url(${process.env.PUBLIC_URL}/radar/${mapName}.png)`,
                  height: currentSizeImg,
                  width: currentSizeImg,
                  backgroundSize: 'cover',
                }}
              />
            </Box>
          </Box>
          <Box
            component={Paper}
            margin={{
              top: 40, right: 50, left: 50, bottom: 40,
            }}
            m={1}
          >
            <FormControl component="fieldset">
              <FormGroup>
                <Box alignSelf="stretch" display="flex" flexDirection="row" justifyContent="center">
                  {playersSelected.map((playerSelected, num) => (
                    <FormControlLabel
                      key={playerSelected.steamID}
                      control={<Checkbox checked={playerSelected.checked} onChange={handleChangePlayers} color={num < 5 ? 'primary' : 'secondary'} />}
                      name={playerSelected.steamID}
                      label={playerSelected.label.slice(0, 20)}
                    />
                  ))}
                </Box>
              </FormGroup>
            </FormControl>
            <Box alignSelf="stretch" display="flex" justifyContent="center">
              <WeaponsPicker
                weaponsKiller={weaponsKiller}
                setWeaponsKiller={setWeaponsKiller}
                label="Pick the weapons of the shooters"
              />
              <WeaponsPicker
                weaponsKiller={activeWeaponsVictim}
                setWeaponsKiller={setActiveWeaponsVictim}
                label="Pick the weapons of the victims"
              />
            </Box>
            <Slider
              value={[filters.durationMin, filters.durationMax]}
              onChange={(event, newValues) => setFilters({
                ...filters,
                durationMin: newValues[0],
                durationMax: newValues[1],
              })}
              valueLabelDisplay="auto"
              aria-labelledby="range-slider"
              max={170}
            />
          </Box>
          <Box
            alignSelf="stretch"
            display="flex"
            flexDirection="row"
            justifyContent="center"
          >
            <Box
              component={Paper}
              alignSelf="stretch"
              display="flex"
              flexDirection="column"
              justifyContent="center"
            >
              <Typography align="center">
                Shooters positions
              </Typography>
              <div
                className="heatmapDmg"
                width={currentSizeImg}
                height={currentSizeImg}
                style={{
                  backgroundImage: `url(${process.env.PUBLIC_URL}/radar/${mapName}.png)`,
                  height: currentSizeImg,
                  width: currentSizeImg,
                  backgroundSize: 'cover',
                }}
              />
            </Box>
            <Box
              component={Paper}
              alignSelf="stretch"
              display="flex"
              flexDirection="column"
              justifyContent="center"
            >
              <Typography align="center">
                Victims positions
              </Typography>
              <div
                className="heatmapDmgTook"
                width={currentSizeImg}
                height={currentSizeImg}
                style={{
                  backgroundImage: `url(${process.env.PUBLIC_URL}/radar/${mapName}.png)`,
                  height: currentSizeImg,
                  width: currentSizeImg,
                  backgroundSize: 'cover',
                }}
              />
            </Box>
          </Box>
        </Box>
      </Box>
    </Container>
  );
}

export default Heatmap;
