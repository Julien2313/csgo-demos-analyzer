import React, {
  useEffect, useState,
} from 'react';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';

import { makeStyles } from '@material-ui/core/styles';
import {
  Chip,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from '@material-ui/core';

import Skeleton from '@material-ui/lab/Skeleton';

import TimeAgo from 'react-timeago';
import Moment from 'moment';
import { padStart, map } from 'lodash';

import apiDemo from '../../api/demo';

function GetLastAnalyzed() {
  const [demos, setDemos] = useState([]);
  const [isGettingDemos, setIsGettingDemos] = useState(false);
  const history = useHistory();

  const useStyles = makeStyles(() => ({
    root: {
      '&:hover': {
        color: 'black',
        background: '#EEEEEE',

      },
    },
  }));

  const classes = useStyles();
  const redirectStats = (demoID) => {
    history.push({
      pathname: `/demo/stats/${demoID}`,
      // state: demoID,
    });
  };

  const getLastAnalyzed = async () => {
    try {
      setIsGettingDemos(true);
      const response = await apiDemo.getLastAnalyzed();
      setDemos(response.data.data.demos);
    } catch (err) {
      if (err.response === undefined) {
        toast.error('An error occurred', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        return;
      }

      switch (err.response.status) {
        case 429:
          toast.error('Too many request !', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          break;
        default:
          toast.error('An error occurred', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          break;
      }
    } finally {
      setIsGettingDemos(false);
    }
  };

  useEffect(() => {
    getLastAnalyzed();
  }, []);

  if (isGettingDemos) {
    return (
      <TableContainer component={Paper}>
        <Table classes={classes} size="small" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell width="72">Demo</TableCell>
              <TableCell align="left">Map name</TableCell>
              <TableCell align="left">Score</TableCell>
              <TableCell align="left">Date</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {map([null, null, null, null], (_, idx) => (
              <TableRow key={idx}>
                <TableCell width="72" component="th" scope="row"><Skeleton variant="rect" width={72} height={72} /></TableCell>
                <TableCell width="150" align="left"><Skeleton variant="text" /></TableCell>
                <TableCell width="150" align="left"><Skeleton variant="text" /></TableCell>
                <TableCell width="450" align="left"><Skeleton variant="text" /></TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell width="72">Demo</TableCell>
            <TableCell align="left">Map name</TableCell>
            <TableCell align="left">Score</TableCell>
            <TableCell align="left">Date</TableCell>
            <TableCell align="left">Average Rank</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {demos.map((row) => (
            <TableRow
              key={row.id}
              classes={classes}
              onClick={() => redirectStats(row.id)}
              style={{ cursor: 'pointer' }}
            >
              <TableCell width="72" component="th" scope="row"><img height="50" width="50" alt="avatar" src={`${process.env.PUBLIC_URL}/mapbadge/${row.mapName}.png`} /></TableCell>
              <TableCell align="left">{row.mapName}</TableCell>
              <TableCell align="left">
                <Chip
                  style={{ backgroundColor: '#000000', color: 'white' }}
                  label={`${padStart(row.score[0], 2, 0)} : ${padStart(row.score[1], 2, 0)}`}
                />
              </TableCell>
              <TableCell align="left">
                <TimeAgo date={row.date} />
                {', '}
                {Moment(row.date).format('DD/MM/YYYY HH:mm', true)}
              </TableCell>
              <TableCell>
                <img height="30" width="75" alt="avatar" src={`${process.env.PUBLIC_URL}/ranks/${row.averageRank}.png`} />
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default GetLastAnalyzed;
