import React from 'react';
import {
  Paper,
} from '@material-ui/core';

import marksSliderKill from './marksSliderKill';

export default function switchMarksSlider(event, numE) {
  if (event.type === 1) {
    return marksSliderKill(event.victimSide, numE);
  }
  if (event.type === 2) {
    return (
      <Paper key={numE}>
        <img
          height={10}
          width={10}
          alt=""
          src={`${process.env.PUBLIC_URL}/utils/bombBlack.png`}
        />
      </Paper>
    );
  }
  if (event.type === 3) {
    return (
      <Paper key={numE}>
        <img
          height={10}
          width={10}
          alt=""
          src={`${process.env.PUBLIC_URL}/utils/defuserBlack.png`}
        />
      </Paper>
    );
  }
  if (event.type === 4) {
    return (
      <Paper key={numE}>
        <img
          height={10}
          width={10}
          alt=""
          src={`${process.env.PUBLIC_URL}/utils/C4.png`}
        />
      </Paper>
    );
  }
  return false;
}
