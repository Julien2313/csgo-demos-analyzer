import React from 'react';

import {
  Paper,
} from '@material-ui/core';

export default function marksSliderKill(
  victimSide, numE,
) {
  return (
    <Paper key={numE} variant="outlined">
      <img
        height={10}
        width={10}
        alt=""
        src={victimSide === 3 ? `${process.env.PUBLIC_URL}/utils/skullCT.png` : `${process.env.PUBLIC_URL}/utils/skullT.png`}
      />
    </Paper>
  );
}
