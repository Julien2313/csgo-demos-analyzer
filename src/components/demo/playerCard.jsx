import React, { useMemo } from 'react';
import Truncate from 'react-truncate';

import { Box, Grid, Paper } from '@material-ui/core';
import SignalCellularConnectedNoInternet0BarIcon from '@material-ui/icons/SignalCellularConnectedNoInternet0Bar';
import ComputerIcon from '@material-ui/icons/Computer';
import PropTypes from 'prop-types';

import Weapon from '../icons/weapon';

function PlayerCard({
  name,
  color,
  isAlive,
  isConnected,
  isControllingBot,
  health,
  armor,
  hasHelmet,
  hasDefuseKit,
  money,
  primaryWeapon,
  pistol,
  grenades,
  hasC4,
  colorSide,
}) {
  const playerCard = useMemo(() => (
    <Box marginBottom="2px">
      <Box flexDirection="column">
        <Box
          display="flex"
          flexDirection="row"
          height="32px"
          width="400px"
          style={{ backgroundColor: isAlive || isControllingBot ? colorSide : '#00000050' }}
        >
          <Box width="400px" display="flex" flexDirection="row">
            <Box marginLeft="10px" width="32px" style={{ color: 'white' }}>
              {!isConnected ? <SignalCellularConnectedNoInternet0BarIcon />
                : isAlive || isControllingBot ? `${health}` : <img alt="" src={`${process.env.PUBLIC_URL}/utils/skull.png`} />}
            </Box>
            <Box width="18px" marginRight="10px" style={{ color: 'white' }}>
              {isControllingBot ? <ComputerIcon />
                : (
                  <Grid style={{ height: '32px', width: '32px' }} size="small">
                    <Paper style={{
                      backgroundColor: color, height: '18px', width: '18px', borderRadius: '50%',
                    }}
                    />
                  </Grid>
                )}
            </Box>

            <Box style={{ color: 'white' }}>
              <Truncate
                lines={1}
                width={120}
              >
                {name}
              </Truncate>
            </Box>
          </Box>
          <Grid container justify="flex-end">
            <Box width="130px" marginRight="5px">
              {isConnected ? <Weapon weapon={primaryWeapon} /> : false}
            </Box>
          </Grid>
        </Box>
        <Box
          display="flex"
          flexDirection="row"
          height="32px"
          width="400px"
          style={{ backgroundColor: isAlive || isControllingBot ? '#808080' : '#00000050' }}
        >
          <Grid container justify="flex-start">
            <Box marginLeft="10px" width="32px">
              {hasHelmet
                ? <img alt="" src={`${process.env.PUBLIC_URL}/utils/helmet.png`} />
                : (armor > 0 ? <img alt="" src={`${process.env.PUBLIC_URL}/utils/armor.png`} /> : false)}
            </Box>
            <Box marginLeft="2px" marginRight="4px" width="32px">
              {hasDefuseKit
                ? <img alt="" src={`${process.env.PUBLIC_URL}/utils/defuser.png`} />
                : (hasC4 ? <img height={30} width={30} alt="" src={`${process.env.PUBLIC_URL}/utils/bomb.png`} /> : false)}
            </Box>
            <Grid style={{ color: 'white' }}>
              {`$${money}`}
            </Grid>
          </Grid>
          <Grid container justify="flex-end">
            {grenades !== null ? grenades.map((grenade, idx) => (
              <img key={idx} alt={grenade} src={`${process.env.PUBLIC_URL}/grenades/${grenade}.png`} />
            )) : false }
          </Grid>
          <Grid container justify="flex-end">
            <Box marginRight="10px">
              {isConnected ? <Weapon weapon={pistol} /> : false}
            </Box>
          </Grid>
        </Box>
      </Box>
    </Box>
  ), [
    name,
    color,
    isAlive,
    isConnected,
    isControllingBot,
    health,
    armor,
    hasHelmet,
    hasDefuseKit,
    money,
    primaryWeapon,
    pistol,
    grenades,
    hasC4,
    colorSide]);
  return playerCard;
}

PlayerCard.propTypes = {
  name: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  isAlive: PropTypes.bool.isRequired,
  isConnected: PropTypes.bool.isRequired,
  isControllingBot: PropTypes.bool.isRequired,
  health: PropTypes.number.isRequired,
  armor: PropTypes.number.isRequired,
  hasHelmet: PropTypes.bool.isRequired,
  hasDefuseKit: PropTypes.bool.isRequired,
  money: PropTypes.number.isRequired,
  primaryWeapon: PropTypes.string.isRequired,
  pistol: PropTypes.string.isRequired,
  grenades: PropTypes.arrayOf(PropTypes.string).isRequired,
  hasC4: PropTypes.bool.isRequired,
  colorSide: PropTypes.string.isRequired,
};

// PlayerCard.defaultProps = {
//   isAlive: true,
// };

export default PlayerCard;
