import React, { useState, useEffect } from 'react';
import {
  TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Paper, Container, TableSortLabel, ButtonGroup, Button,
} from '@material-ui/core';

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function tableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  {
    id: 'picture', numeric: false, disablePadding: false, label: '',
  },
  {
    id: 'username', numeric: false, disablePadding: false, label: 'Username',
  },
  {
    id: 'team', numeric: false, disablePadding: false, label: 'Team',
  },
  {
    id: 'adr', numeric: true, disablePadding: false, label: 'ADR',
  },
  {
    id: 'accuracy', numeric: true, disablePadding: false, label: 'Accuracy',
  },
  {
    id: 'firstBulletAccuracy', numeric: true, disablePadding: false, label: 'First bullet accuracy',
  },
  {
    id: 'hs', numeric: true, disablePadding: false, label: 'HS',
  },
  {
    id: 'firstBulletHS', numeric: true, disablePadding: false, label: 'First bullet HS',
  },
  {
    id: 'utilityDamage', numeric: true, disablePadding: false, label: 'Utility damage',
  },
  {
    id: 'averageVelocityShoots', numeric: true, disablePadding: false, label: 'Average velocity per shoot (max 250)',
  },
  {
    id: 'averageDeltaXCrossHair', numeric: true, disablePadding: false, label: 'Average delta crosshair X',
  },
  {
    id: 'averageDeltaYCrossHair', numeric: true, disablePadding: false, label: 'Average delta crosshair Y',
  },
];

function EnhancedTableHead(props) {
  const { order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'desc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

function WeaponsMM(values) {
  const { teams } = values;
  const { rounds } = values;
  const [rows, setRows] = useState();
  const [marks, setMarks] = useState({});

  const [order, setOrder] = React.useState('desc');
  const [orderBy, setOrderBy] = React.useState('adr');

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'desc';
    setOrder(isAsc ? 'asc' : 'desc');
    setOrderBy(property);
  };

  useEffect(() => {
    const players = [];
    const mapPlayersCT = {};
    const mapPlayersT = {};
    teams.forEach((team, numTeam) => {
      team.players.forEach((player) => {
        players.push({
          linkAvatar: player.linkAvatar,
          steamID: player.steamID,
          username: player.username,
          team: (numTeam === 0 ? 'A' : 'B'),
          accuracy: player.marks.accuracy,
          hs: player.marks.hs,
          firstBulletAccuracy: player.marks.firstBulletAccuracy,
          firstBulletHS: player.marks.firstBulletHS,
          utilityDamage: player.marks.utilityDamage,
          adr: player.marks.damage / rounds.length,
          averageVelocityShoots: player.marks.averageVelocityShoots,
          averageDeltaXCrossHair: player.marks.averageDeltaXCrossHair,
          averageDeltaYCrossHair: player.marks.averageDeltaYCrossHair,
        });
        mapPlayersCT[player.steamID] = {
          linkAvatar: player.linkAvatar,
          steamID: player.steamID,
          username: player.username,
          side: 3,
          team: (numTeam === 0 ? 'A' : 'B'),
          nbrBulletsFired: 0,
          nbrBulletsHit: 0,
          nbrBulletsHS: 0,
          nbrFirstBulletsFired: 0,
          nbrFirstBulletsHit: 0,
          nbrFirstBulletsHS: 0,
          utilityDamage: 0,
          adr: 0,
          averageVelocityShoots: 0,
          averageDeltaXCrossHair: 0,
          averageDeltaYCrossHair: 0,
          nbrRounds: 0,
        };
        mapPlayersT[player.steamID] = {
          linkAvatar: player.linkAvatar,
          steamID: player.steamID,
          username: player.username,
          side: 2,
          team: (numTeam === 0 ? 'A' : 'B'),
          nbrBulletsFired: 0,
          nbrBulletsHit: 0,
          nbrBulletsHS: 0,
          nbrFirstBulletsFired: 0,
          nbrFirstBulletsHit: 0,
          nbrFirstBulletsHS: 0,
          utilityDamage: 0,
          adr: 0,
          averageVelocityShoots: 0,
          averageDeltaXCrossHair: 0,
          averageDeltaYCrossHair: 0,
          nbrRounds: 0,
        };
      });
    });

    rounds.forEach((round) => {
      round.marksPerRound.forEach((markPerRound) => {
        if (markPerRound.side === 3) {
          mapPlayersCT[markPerRound.steamID].nbrBulletsFired += markPerRound.nbrBulletsFired;
          mapPlayersCT[markPerRound.steamID].nbrBulletsHit += markPerRound.nbrBulletsHit;
          mapPlayersCT[markPerRound.steamID].nbrBulletsHS += markPerRound.nbrBulletsHS;
          mapPlayersCT[markPerRound.steamID].nbrFirstBulletsFired += markPerRound.nbrFirstBulletsFired;
          mapPlayersCT[markPerRound.steamID].nbrFirstBulletsHit += markPerRound.nbrFirstBulletsHit;
          mapPlayersCT[markPerRound.steamID].nbrFirstBulletsHS += markPerRound.nbrFirstBulletsHS;
          mapPlayersCT[markPerRound.steamID].utilityDamage += markPerRound.utilityDamage;
          mapPlayersCT[markPerRound.steamID].adr += markPerRound.damage;
          mapPlayersCT[markPerRound.steamID].averageVelocityShoots += markPerRound.averageVelocityShoots * markPerRound.nbrBulletsFired;
          mapPlayersCT[markPerRound.steamID].averageDeltaXCrossHair += markPerRound.averageDeltaXCrossHair * markPerRound.nbrFirstBulletsHit;
          mapPlayersCT[markPerRound.steamID].averageDeltaYCrossHair += markPerRound.averageDeltaYCrossHair * markPerRound.nbrFirstBulletsHit;
          mapPlayersCT[markPerRound.steamID].nbrRounds += 1;
        } else if (markPerRound.side === 2) {
          mapPlayersT[markPerRound.steamID].nbrBulletsFired += markPerRound.nbrBulletsFired;
          mapPlayersT[markPerRound.steamID].nbrBulletsHit += markPerRound.nbrBulletsHit;
          mapPlayersT[markPerRound.steamID].nbrBulletsHS += markPerRound.nbrBulletsHS;
          mapPlayersT[markPerRound.steamID].nbrFirstBulletsFired += markPerRound.nbrFirstBulletsFired;
          mapPlayersT[markPerRound.steamID].nbrFirstBulletsHit += markPerRound.nbrFirstBulletsHit;
          mapPlayersT[markPerRound.steamID].nbrFirstBulletsHS += markPerRound.nbrFirstBulletsHS;
          mapPlayersT[markPerRound.steamID].utilityDamage += markPerRound.utilityDamage;
          mapPlayersT[markPerRound.steamID].adr += markPerRound.damage;
          mapPlayersT[markPerRound.steamID].averageVelocityShoots += markPerRound.averageVelocityShoots * markPerRound.nbrBulletsFired;
          mapPlayersT[markPerRound.steamID].averageDeltaXCrossHair += markPerRound.averageDeltaXCrossHair * markPerRound.nbrFirstBulletsHit;
          mapPlayersT[markPerRound.steamID].averageDeltaYCrossHair += markPerRound.averageDeltaYCrossHair * markPerRound.nbrFirstBulletsHit;
          mapPlayersT[markPerRound.steamID].nbrRounds += 1;
        }
      });
    });

    const playersCT = [];
    Object.keys(mapPlayersCT).forEach((steamID) => {
      const p = mapPlayersCT[steamID];
      if (p.nbrRounds > 0) {
        mapPlayersCT[steamID].adr = p.adr / p.nbrRounds;
      }
      mapPlayersCT[steamID].utilityDamage = p.utilityDamage;
      if (p.nbrBulletsFired > 0) {
        mapPlayersCT[steamID].accuracy = p.nbrBulletsHit / p.nbrBulletsFired;
        mapPlayersCT[steamID].averageVelocityShoots = p.averageVelocityShoots / p.nbrBulletsFired;
      }
      if (p.nbrBulletsHit > 0) {
        mapPlayersCT[p.steamID].hs = p.nbrBulletsHS / p.nbrBulletsHit;
      }
      if (p.nbrFirstBulletsFired > 0) {
        mapPlayersCT[steamID].firstBulletAccuracy = p.nbrFirstBulletsHit / p.nbrFirstBulletsFired;
      }
      if (p.nbrFirstBulletsHit > 0) {
        mapPlayersCT[steamID].firstBulletHS = p.nbrFirstBulletsHS / p.nbrFirstBulletsHit;
        mapPlayersCT[steamID].averageDeltaXCrossHair = p.averageDeltaXCrossHair / p.nbrFirstBulletsHit;
        mapPlayersCT[steamID].averageDeltaYCrossHair = p.averageDeltaYCrossHair / p.nbrFirstBulletsHit;
      }
      playersCT.push(mapPlayersCT[steamID]);
    });

    const playersT = [];
    Object.keys(mapPlayersT).forEach((steamID) => {
      const p = mapPlayersT[steamID];
      if (p.nbrRounds > 0) {
        mapPlayersT[steamID].adr = p.adr / p.nbrRounds;
      }
      mapPlayersT[steamID].utilityDamage = p.utilityDamage;
      if (p.nbrBulletsFired > 0) {
        mapPlayersT[steamID].accuracy = p.nbrBulletsHit / p.nbrBulletsFired;
        mapPlayersT[steamID].averageVelocityShoots = p.averageVelocityShoots / p.nbrBulletsFired;
      }
      if (p.nbrBulletsHit > 0) {
        mapPlayersT[steamID].hs = p.nbrBulletsHS / p.nbrBulletsHit;
      }
      if (p.nbrFirstBulletsFired > 0) {
        mapPlayersT[steamID].firstBulletAccuracy = p.nbrFirstBulletsHit / p.nbrFirstBulletsFired;
      }
      if (p.nbrFirstBulletsHit > 0) {
        mapPlayersT[steamID].firstBulletHS = p.nbrFirstBulletsHS / p.nbrFirstBulletsHit;
        mapPlayersT[steamID].averageDeltaXCrossHair = p.averageDeltaXCrossHair / p.nbrFirstBulletsHit;
        mapPlayersT[steamID].averageDeltaYCrossHair = p.averageDeltaYCrossHair / p.nbrFirstBulletsHit;
      }
      playersT.push(mapPlayersT[steamID]);
    });

    setRows(players);
    setMarks({
      global: players,
      CT: playersCT,
      T: playersT,
    });
  }, []);

  if (rows === undefined) {
    return false;
  }

  return (
    <Container maxWidth={false}>
      <ButtonGroup variant="contained" color="primary">
        <Button
          onClick={() => setRows(marks.global)}
          style={{
            backgroundColor: '#000000',
          }}
        >
          Global
        </Button>
        <Button
          onClick={() => setRows(marks.CT)}
          style={{
            backgroundColor: '#4374b0',
          }}
        >
          CT
        </Button>
        <Button
          onClick={() => setRows(marks.T)}
          style={{
            backgroundColor: '#C7A247',
          }}
        >
          T
        </Button>
      </ButtonGroup>
      <TableContainer component={Paper}>
        <Table
          size="small"
          aria-label="enhanced table"
        >
          <EnhancedTableHead
            order={order}
            orderBy={orderBy}
            onRequestSort={handleRequestSort}
          />
          <TableBody>
            {tableSort(rows, getComparator(order, orderBy)).map(
              (row, index) => (
                <TableRow
                  hover
                  role="checkbox"
                  tabIndex={-1}
                  key={row.steamID}
                  style={{ backgroundColor: row.team === 'A' ? '#FF000070' : '#0000FF70' }}
                >
                  <TableCell component="th" scope="row">
                    <img height="32" width="32" alt="" src={row.linkAvatar} />
                  </TableCell>
                  <TableCell align="left">{row.username}</TableCell>
                  <TableCell align="left">{row.team}</TableCell>
                  <TableCell align="right">{Math.round(row.adr)}</TableCell>
                  <TableCell align="right">
                    {Number((row.accuracy * 100).toFixed(1))}
                    %
                  </TableCell>
                  <TableCell align="right">
                    {Number((row.firstBulletAccuracy * 100).toFixed(1))}
                    %
                  </TableCell>
                  <TableCell align="right">
                    {Number((row.hs * 100).toFixed(1))}
                    %
                  </TableCell>
                  <TableCell align="right">
                    {Number((row.firstBulletHS * 100).toFixed(1))}
                    %
                  </TableCell>
                  <TableCell align="right">{Math.round(row.utilityDamage)}</TableCell>
                  <TableCell align="right">{Math.round(row.averageVelocityShoots)}</TableCell>
                  <TableCell align="right">{Math.round(row.averageDeltaXCrossHair)}</TableCell>
                  <TableCell align="right">{Math.round(row.averageDeltaYCrossHair)}</TableCell>
                </TableRow>
              ),
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </Container>
  );
}

export default WeaponsMM;
