import React from 'react';
import { Bar } from 'react-chartjs-2';

import {
  Box,
} from '@material-ui/core';

function ScoreTeam(values) {
  const { players } = values;
  if (players.length === 0) {
    return false;
  }

  const { maxScore } = values;
  const { scores } = values;
  const { side } = values;

  const colors = ['#ef476f', '#06d6a0', '#ffd166', '#8338ec', '#0466c8'];
  const labels = [];

  const dataKills = [];
  const dataDamageScore = [];
  const dataAssistKillScore = [];
  //   const dataDeathScore = [];
  const dataFlashScore = [];
  const dataBombs = [];

  for (let numPlayer = 0; numPlayer < players.length; numPlayer += 1) {
    if (players[numPlayer].side === side) {
      let scorePlayer = {};
      for (let numScore = 0; numScore < scores.length; numScore += 1) {
        if (scores[numScore].steamID === players[numPlayer].steamID) {
          labels.push(players[numPlayer].playerName);
          scorePlayer = scores[numScore];
          break;
        }
      }
      dataKills.push(Math.max(0, scorePlayer.killScore));
      dataDamageScore.push(Math.max(0, scorePlayer.damageScore));
      dataAssistKillScore.push(Math.max(0, scorePlayer.assistKillScore));
      //   dataDeathScore.push(scorePlayer.deathScore);
      dataFlashScore.push(Math.max(0, scorePlayer.flashScore));
      dataBombs.push(Math.max(0, scorePlayer.bombDefuseScore + scorePlayer.bombPlantedScore));
    }
  }

  const data = {
    labels,
    datasets: [
      {
        label: 'Kills',
        backgroundColor: colors[0],
        data: dataKills,
      },
      {
        label: 'Dmgs',
        backgroundColor: colors[1],
        data: dataDamageScore,
      },
      {
        label: 'Ass.',
        backgroundColor: colors[2],
        data: dataAssistKillScore,
      },
      //   {
      //     label: 'Deaths',
      //     backgroundColor: colors[3],
      //     data: dataDeathScore,
      //   },
      {
        label: 'Flash',
        backgroundColor: colors[3],
        data: dataFlashScore,
      },
      {
        label: 'Bombs',
        backgroundColor: colors[4],
        data: dataBombs,
      },
    ],
  };

  const options = {
    scales: {
      yAxes: [
        {
          stacked: true,
          ticks: {
            beginAtZero: true,
            suggestedMax: maxScore,
          },
        },
      ],
      xAxes: [
        {
          stacked: true,
        },
      ],
    },
  };

  return (
    <Box alignSelf="stretch" display="flex" style={{ backgroundColor: '#ffffff' }}>
      <Bar data={data} options={options} height={150} />
    </Box>
  );
}

export default ScoreTeam;
