import React, { useMemo } from 'react';

import {
  Box,
} from '@material-ui/core';

import PlayerCard from './playerCard';

function PlayersCard({ playersCard, sizeImg, playersColor }) {
  const playersCardSlice = useMemo(() => {
    if (playersCard === undefined) {
      return [];
    }

    playersCard.sort((a, b) => {
      if (a.side !== b.side) {
        return a.side > b.side ? 1 : -1;
      }
      return a.steamID > b.steamID ? 1 : -1;
    });

    return playersCard;
  }, [playersCard]);

  const playersCardHTML = useMemo(() => playersCardSlice.map((player, numPlayer) => (
    <PlayerCard
      key={player.steamID}
      name={`${numPlayer}. ${player.playerName}`}
      color={playersColor[player.steamID]}
      isAlive={player.isAlive}
      isControllingBot={player.isControllingBot}
      isConnected={player.isConnected}
      isBot={player.isBot}
      health={player.health}
      armor={player.armor}
      hasHelmet={player.hasHelmet}
      hasDefuseKit={player.hasDefuseKit}
      money={player.money}
      primaryWeapon={player.primaryWeapon}
      pistol={player.pistol}
      grenades={player.grenades}
      hasC4={player.hasC4}
      colorSide={player.side === 2 ? '#C7A247' : '#4374b0'}
    />
  )), [playersCardSlice]);

  return (
    <Box alignSelf="stretch" display="flex" flexDirection="column" justifyContent="center" height={sizeImg}>
      {playersCardHTML}
    </Box>
  );
}

export default PlayersCard;
