import React, { useMemo } from 'react';

import {
  Radio,
  RadioGroup,
  Box,
  FormControl,
  FormLabel,
  Paper,
  FormControlLabel,
} from '@material-ui/core';
import { map } from 'lodash';

const Round = ({
  numRound, winner, selectedValue, handleChangeNumRound,
}) => {
  const label = useMemo(() => (numRound + 1), [numRound]);
  const color = useMemo(() => (winner === 3 ? '#4374b0' : '#C7A247'), [winner]);
  const checked = useMemo(() => (selectedValue === numRound), [selectedValue, numRound]);
  const value = useMemo(() => (numRound), [numRound]);

  return (
    <FormControlLabel
      labelPlacement="top"
      label={label}
      control={(
        <Radio
          style={{ color }}
          size="small"
          checked={checked}
          onChange={handleChangeNumRound}
          value={value}
        />
      )}
    />
  );
};

function MatrixWinRounds(
  {
    rawRounds,
    numRoundSelected,
    setNumRoundSelected,
    setNumFramePlayer,
    setPositions,
    roundsParsed,
  },
) {
  const matrixWin = useMemo(() => {
    const localMatrixWin = [];

    rawRounds.forEach((round, numRound) => {
      localMatrixWin.push(round.sideWin);
    });
    return localMatrixWin;
  }, [rawRounds, setNumRoundSelected]);

  const matrixWin1Half = useMemo(() => matrixWin.slice(0, 15), [matrixWin]);
  const matrixWin2Half = useMemo(() => matrixWin.slice(15, 30), [matrixWin]);
  const matrixWin1OT = useMemo(() => matrixWin.slice(30, 33), [matrixWin]);
  const matrixWin1OT2Half = useMemo(() => matrixWin.slice(33, 36), [matrixWin]);
  const matrixWin2OT = useMemo(() => matrixWin.slice(36, 39), [matrixWin]);
  const matrixWin2OT2Half = useMemo(() => matrixWin.slice(39, 42), [matrixWin]);
  const matrixWin3OT = useMemo(() => matrixWin.slice(42, 45), [matrixWin]);
  const matrixWin3OT2Half = useMemo(() => matrixWin.slice(45, 48), [matrixWin]);
  const matrixWin4OT = useMemo(() => matrixWin.slice(48, 51), [matrixWin]);
  const matrixWin4OT2Half = useMemo(() => matrixWin.slice(51, 54), [matrixWin]);
  const matrixWin5OT = useMemo(() => matrixWin.slice(54, 57), [matrixWin]);
  const matrixWin5OT2Half = useMemo(() => matrixWin.slice(57, 60), [matrixWin]);
  const matrixWin6OT = useMemo(() => matrixWin.slice(60, 63), [matrixWin]);
  const matrixWin6OT2Half = useMemo(() => matrixWin.slice(63, 66), [matrixWin]);
  const matrixWin7OT = useMemo(() => matrixWin.slice(66, 69), [matrixWin]);
  const matrixWin7OT2Half = useMemo(() => matrixWin.slice(69, 72), [matrixWin]);
  const matrixWin8OT = useMemo(() => matrixWin.slice(72, 75), [matrixWin]);
  const matrixWin8OT2Half = useMemo(() => matrixWin.slice(75, 78), [matrixWin]);
  const matrixWin9OT = useMemo(() => matrixWin.slice(78, 81), [matrixWin]);
  const matrixWin9OT2Half = useMemo(() => matrixWin.slice(81, 84), [matrixWin]);
  const matrixWin10OT = useMemo(() => matrixWin.slice(84, 87), [matrixWin]);
  const matrixWin10OT2Half = useMemo(() => matrixWin.slice(87, 90), [matrixWin]);

  const handleChangeNumRound = (event) => {
    const numRound = parseInt(event.target.value, 10);
    setNumRoundSelected(numRound);
    setNumFramePlayer(0);
    setPositions(roundsParsed[numRound][0]);
  };

  const renderMatrixWin = useMemo(() => (
    <>
      <RadioGroup row>
        {map(matrixWin1Half, (_, idx) => <Round key={idx} numRound={idx} winner={matrixWin[idx]} selectedValue={numRoundSelected} handleChangeNumRound={handleChangeNumRound} />)}
      </RadioGroup>
      <RadioGroup row>
        {map(matrixWin2Half, (_, idx) => <Round key={idx + 15} numRound={idx + 15} winner={matrixWin[idx + 15]} selectedValue={numRoundSelected} handleChangeNumRound={handleChangeNumRound} />)}
      </RadioGroup>
      <RadioGroup row>
        {map(matrixWin1OT, (_, idx) => <Round key={idx + 30} numRound={idx + 30} winner={matrixWin[idx + 30]} selectedValue={numRoundSelected} handleChangeNumRound={handleChangeNumRound} />)}
        {map(matrixWin1OT2Half, (_, idx) => <Round key={idx + 33} numRound={idx + 33} winner={matrixWin[idx + 33]} selectedValue={numRoundSelected} handleChangeNumRound={handleChangeNumRound} />)}
      </RadioGroup>
      <RadioGroup row>
        {map(matrixWin2OT, (_, idx) => <Round key={idx + 36} numRound={idx + 36} winner={matrixWin[idx + 36]} selectedValue={numRoundSelected} handleChangeNumRound={handleChangeNumRound} />)}
        {map(matrixWin2OT2Half, (_, idx) => <Round key={idx + 39} numRound={idx + 39} winner={matrixWin[idx + 39]} selectedValue={numRoundSelected} handleChangeNumRound={handleChangeNumRound} />)}
      </RadioGroup>
      <RadioGroup row>
        {map(matrixWin3OT, (_, idx) => <Round key={idx + 42} numRound={idx + 42} winner={matrixWin[idx + 42]} selectedValue={numRoundSelected} handleChangeNumRound={handleChangeNumRound} />)}
        {map(matrixWin3OT2Half, (_, idx) => <Round key={idx + 45} numRound={idx + 45} winner={matrixWin[idx + 45]} selectedValue={numRoundSelected} handleChangeNumRound={handleChangeNumRound} />)}
      </RadioGroup>
      <RadioGroup row>
        {map(matrixWin4OT, (_, idx) => <Round key={idx + 48} numRound={idx + 48} winner={matrixWin[idx + 48]} selectedValue={numRoundSelected} handleChangeNumRound={handleChangeNumRound} />)}
        {map(matrixWin4OT2Half, (_, idx) => <Round key={idx + 51} numRound={idx + 51} winner={matrixWin[idx + 51]} selectedValue={numRoundSelected} handleChangeNumRound={handleChangeNumRound} />)}
      </RadioGroup>
      <RadioGroup row>
        {map(matrixWin5OT, (_, idx) => <Round key={idx + 54} numRound={idx + 54} winner={matrixWin[idx + 54]} selectedValue={numRoundSelected} handleChangeNumRound={handleChangeNumRound} />)}
        {map(matrixWin5OT2Half, (_, idx) => <Round key={idx + 57} numRound={idx + 57} winner={matrixWin[idx + 57]} selectedValue={numRoundSelected} handleChangeNumRound={handleChangeNumRound} />)}
      </RadioGroup>
      <RadioGroup row>
        {map(matrixWin6OT, (_, idx) => <Round key={idx + 60} numRound={idx + 60} winner={matrixWin[idx + 60]} selectedValue={numRoundSelected} handleChangeNumRound={handleChangeNumRound} />)}
        {map(matrixWin6OT2Half, (_, idx) => <Round key={idx + 63} numRound={idx + 63} winner={matrixWin[idx + 63]} selectedValue={numRoundSelected} handleChangeNumRound={handleChangeNumRound} />)}
      </RadioGroup>
      <RadioGroup row>
        {map(matrixWin7OT, (_, idx) => <Round key={idx + 66} numRound={idx + 66} winner={matrixWin[idx + 66]} selectedValue={numRoundSelected} handleChangeNumRound={handleChangeNumRound} />)}
        {map(matrixWin7OT2Half, (_, idx) => <Round key={idx + 69} numRound={idx + 69} winner={matrixWin[idx + 69]} selectedValue={numRoundSelected} handleChangeNumRound={handleChangeNumRound} />)}
      </RadioGroup>
      <RadioGroup row>
        {map(matrixWin8OT, (_, idx) => <Round key={idx + 72} numRound={idx + 72} winner={matrixWin[idx + 72]} selectedValue={numRoundSelected} handleChangeNumRound={handleChangeNumRound} />)}
        {map(matrixWin8OT2Half, (_, idx) => <Round key={idx + 75} numRound={idx + 75} winner={matrixWin[idx + 75]} selectedValue={numRoundSelected} handleChangeNumRound={handleChangeNumRound} />)}
      </RadioGroup>
      <RadioGroup row>
        {map(matrixWin9OT, (_, idx) => <Round key={idx + 78} numRound={idx + 78} winner={matrixWin[idx + 78]} selectedValue={numRoundSelected} handleChangeNumRound={handleChangeNumRound} />)}
        {map(matrixWin9OT2Half, (_, idx) => <Round key={idx + 81} numRound={idx + 81} winner={matrixWin[idx + 81]} selectedValue={numRoundSelected} handleChangeNumRound={handleChangeNumRound} />)}
      </RadioGroup>
      <RadioGroup row>
        {map(matrixWin10OT, (_, idx) => <Round key={idx + 86} numRound={idx + 86} winner={matrixWin[idx + 86]} selectedValue={numRoundSelected} handleChangeNumRound={handleChangeNumRound} />)}
        {map(matrixWin10OT2Half, (_, idx) => <Round key={idx + 89} numRound={idx + 89} winner={matrixWin[idx + 89]} selectedValue={numRoundSelected} handleChangeNumRound={handleChangeNumRound} />)}
      </RadioGroup>
    </>
  ), [matrixWin, numRoundSelected, matrixWin1Half, matrixWin2Half, roundsParsed]);

  return (
    <Box display="flex" alignSelf="stretch" justifyContent="center">
      <Paper variant="outlined" square>
        <FormControl component="fieldset" size="small" fullWidth>
          <FormLabel component="legend">Rounds</FormLabel>
          {renderMatrixWin}
        </FormControl>
      </Paper>
    </Box>
  );
}

export default MatrixWinRounds;
