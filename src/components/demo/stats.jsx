import React, { useState, useEffect } from 'react';
import {
  Box, Container, AppBar, Tabs, Tab, Typography, Select, FormControl, MenuItem,
  TableContainer, TableCell, Table, TableHead, TableRow, TableBody, Paper,
} from '@material-ui/core';

import SwipeableViews from 'react-swipeable-views';
import { toast } from 'react-toastify';
import Skeleton from '@material-ui/lab/Skeleton';
import { map } from 'lodash';

import ArrayKills from '../array/arrayKills';
import ArrayFlashs from '../array/arrayFlashs';
import HeatMapDuels from '../array/heatMapDuels';
import Rounds from './rounds';
import apiDemo from '../../api/demo';
import Resume from './resume';
import Marks from './marks';
import Heatmap from './heatmap';
import WeaponsMM from './weaponsMM';

function TabPanel({
  children, value, index,
}) {
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
    >
      {value === index && (
        <Box p={3}>
          <Typography component="span">{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function Stats() {
  const [demo, setDemo] = useState();
  const [value, setValue] = useState(0);

  const [duelsT1, setDuelsT1] = useState('');
  const [duelsT2, setDuelsT2] = useState('');

  const [playersColor, setPlayersColor] = useState({});

  useEffect(() => {
    const colors = ['#808080', '#e6f13d', '#803ca1', '#109856', '#68a3e5', '#eda338'];
    const demoID = window.location.pathname.split('/')[3];
    const getDemoStats = async () => {
      try {
        let demoReturned = await apiDemo.getStats(demoID);
        demoReturned = demoReturned.data.data;
        if (demoReturned.versionAnalyzer <= 1) {
          toast.info('The demo is not yet analyzed, reload the page later', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          return;
        }
        setDemo(demoReturned);

        const localColors = {};
        demoReturned.teams.forEach((team) => {
          team.players.forEach((player) => {
            localColors[player.steamID] = colors[player.color + 1];
          });
        });
        setPlayersColor(localColors);
        setDuelsT1(demoReturned.teams[0].duels);
        setDuelsT2(demoReturned.teams[1].duels);
      } catch (err) {
        if (err.response === undefined) {
          toast.error('An error occurred', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          return;
        }
        switch (err.response.status) {
          case 404:
            toast.error("This demo doesn't exists", {
              position: 'top-center',
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
            return;
          case 429:
            toast.error('Too many request !', {
              position: 'top-center',
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
            break;
          default:
            toast.error('An error occurred', {
              position: 'top-center',
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
            break;
        }
      }
    };

    getDemoStats();
  }, []);

  if (!demo || duelsT1 === '' || duelsT2 === '') {
    return (
      <div>
        <Skeleton variant="rect" width="100%" height={40} />
        <Container maxWidth={false}>
          <Box height="20px" />
          <Box display="flex" flexDirection="column" alignItems="center">
            <Box display="flex" justifyContent="space-around" flexDirection="row">
              <Skeleton variant="circle" width={40} height={40} />
              <Skeleton variant="circle" width={40} height={40} />
            </Box>
          </Box>
          <Box alignSelf="stretch" display="flex" flexDirection="row" marginTop="20px">
            <TableContainer component={Paper}>
              <Table size="small">
                <TableHead>
                  <TableRow>
                    <TableCell width="32" align="left" />
                    <TableCell width="75" align="left">Ranks</TableCell>
                    <TableCell>Username</TableCell>
                    <TableCell align="right">Kills</TableCell>
                    <TableCell align="right">Assists</TableCell>
                    <TableCell align="right">Deaths</TableCell>
                    <TableCell align="right">Score</TableCell>
                    <TableCell align="right">MVP</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {map([null, null, null, null, null], (_, idx) => (
                    <TableRow key={idx}>
                      <TableCell component="th" scope="row"><Skeleton variant="circle" width={32} height={32} /></TableCell>
                      <TableCell component="th" scope="row"><Skeleton variant="rect" width={75} height={30} /></TableCell>
                      <TableCell><Skeleton variant="text" /></TableCell>
                      <TableCell align="right"><Skeleton variant="text" /></TableCell>
                      <TableCell align="right"><Skeleton variant="text" /></TableCell>
                      <TableCell align="right"><Skeleton variant="text" /></TableCell>
                      <TableCell align="right"><Skeleton variant="text" /></TableCell>
                      <TableCell align="right"><Skeleton variant="text" /></TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
            <Box width="20px" />
            <TableContainer component={Paper}>
              <Table size="small">
                <TableHead>
                  <TableRow>
                    <TableCell width="32" align="left" />
                    <TableCell width="75" align="left">Ranks</TableCell>
                    <TableCell>Username</TableCell>
                    <TableCell align="right">Kills</TableCell>
                    <TableCell align="right">Assists</TableCell>
                    <TableCell align="right">Deaths</TableCell>
                    <TableCell align="right">Score</TableCell>
                    <TableCell align="right">MVP</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {map([null, null, null, null, null], (_, idx) => (
                    <TableRow key={idx}>
                      <TableCell component="th" scope="row"><Skeleton variant="circle" width={32} height={32} /></TableCell>
                      <TableCell component="th" scope="row"><Skeleton variant="rect" width={75} height={30} /></TableCell>
                      <TableCell><Skeleton variant="text" /></TableCell>
                      <TableCell align="right"><Skeleton variant="text" /></TableCell>
                      <TableCell align="right"><Skeleton variant="text" /></TableCell>
                      <TableCell align="right"><Skeleton variant="text" /></TableCell>
                      <TableCell align="right"><Skeleton variant="text" /></TableCell>
                      <TableCell align="right"><Skeleton variant="text" /></TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Box>
        </Container>
      </div>
    );
  }

  return (
    <div
      style={{
        backgroundAttachment: 'fixed',
        backgroundImage: `url(${process.env.PUBLIC_URL}/background/${demo.mapName}.jpg)`,
        // backgroundSize: 'cover',
        minHeight: '100vh',
      }}
    >
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={(event, newValue) => setValue(newValue)}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          aria-label="full width tabs example"
        >
          <Tab label="Generals stats" />
          <Tab label="Round by round" />
          <Tab label="Matrix" />
          <Tab label="Marks" />
          <Tab label="Heat maps" />
          <Tab label="Weapons" />
        </Tabs>
      </AppBar>
      <SwipeableViews
        index={value}
        onChangeIndex={(index) => setValue(index)}
      >
        <TabPanel value={value} index={0}>
          <Resume teams={demo.teams} />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <Container>
            <Rounds mapName={demo.mapName} rawRounds={demo.rounds} playersColor={playersColor} />
          </Container>
        </TabPanel>
        <TabPanel value={value} index={2}>
          <Container maxWidth="lg" style={{ backgroundColor: '#ffffff' }}>
            <Box alignSelf="stretch" display="flex" flexDirection="row" justifyContent="center">
              <Box display="flex" marginTop="20px">
                <ArrayKills data={demo.matrixKills.data} keys={demo.matrixKills.keys} />
              </Box>
              <Box display="flex" marginTop="20px">
                <ArrayFlashs data={demo.matrixFlashs.data} keys={demo.matrixFlashs.keys} />
              </Box>
            </Box>
            <Box alignSelf="stretch" display="flex" flexDirection="row">
              <Box style={{ width: '100%' }} alignSelf="stretch" display="flex" justifyContent="center">
                <FormControl>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={duelsT1}
                    onChange={(event) => setDuelsT1(event.target.value)}
                  >
                    <MenuItem value={demo.teams[0].duels}>Team1</MenuItem>
                    {demo.teams[0].players.map((player) => (
                      <MenuItem
                        key={player.steamID}
                        value={player.duels}
                      >
                        {player.username}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Box>
              <Box style={{ width: '100%' }} alignSelf="stretch" display="flex" justifyContent="center">
                <FormControl>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={duelsT2}
                    onChange={(event) => setDuelsT2(event.target.value)}
                  >
                    <MenuItem value={demo.teams[1].duels}>Team2</MenuItem>
                    {demo.teams[1].players.map((player) => (
                      <MenuItem
                        key={player.steamID}
                        value={player.duels}
                      >
                        {player.username}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Box>
            </Box>
            <Box alignSelf="stretch" display="flex" flexDirection="row" justifyContent="center">
              <Box display="flex" marginTop="20px">
                <HeatMapDuels data={duelsT1.data} keys={duelsT1.keys} />
              </Box>
              <Box display="flex" marginTop="20px">
                <HeatMapDuels data={duelsT2.data} keys={duelsT2.keys} />
              </Box>
            </Box>
          </Container>
        </TabPanel>
        <TabPanel value={value} index={3}>
          <Marks teams={demo.teams} bestMarks={demo.bestMarks} baddestMarks={demo.baddestMarks} />
        </TabPanel>
        <TabPanel value={value} index={4}>
          <Heatmap teams={demo.teams} mapName={demo.mapName} rawRounds={demo.rounds} />
        </TabPanel>
        <TabPanel value={value} index={5}>
          <WeaponsMM teams={demo.teams} rounds={demo.rounds} />
        </TabPanel>
      </SwipeableViews>
    </div>
  );
}

export default Stats;
