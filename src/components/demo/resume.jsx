import React from 'react';
import { useHistory } from 'react-router-dom';

import {
  Box, Chip, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Button, Paper, Container,
} from '@material-ui/core';

function Resume(values) {
  const history = useHistory();
  const { teams } = values;

  const renderTeam = (team) => {
    team.players.sort((a, b) => ((a.scoreFacts.totalScore > b.scoreFacts.totalScore) ? -1 : 1));
    return (
      <TableContainer component={Paper}>
        <Table
            size={'small'}
            >
          <TableHead>
            <TableRow>
              <TableCell width="32" align="left" />
              <TableCell width="75" align="left">Ranks</TableCell>
              <TableCell>Username</TableCell>
              <TableCell align="right">Kills</TableCell>
              <TableCell align="right">Assists</TableCell>
              <TableCell align="right">Deaths</TableCell>
              <TableCell align="right">Score</TableCell>
              <TableCell align="right">MVP</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {team.players.map((player) => (
              <TableRow key={player.steamID}>
                <TableCell component="th" scope="row">
                  <Button onClick={() => history.push(`/home/${player.steamID}`)}>
                    <img height="32" width="32" alt="" src={player.linkAvatar} />
                  </Button>
                </TableCell>
                <TableCell component="th" scope="row">
                  <img height="30" width="75" alt="avatar" src={`${process.env.PUBLIC_URL}/ranks/${player.rank}.png`} />
                </TableCell>
                <TableCell>{player.username}</TableCell>
                <TableCell align="right">{player.kills}</TableCell>
                <TableCell align="right">{player.assists}</TableCell>
                <TableCell align="right">{player.deaths}</TableCell>
                <TableCell align="right">{Number((player.scoreFacts.ratioScore).toFixed(2))}</TableCell>
                <TableCell align="right">{player.MVPsFacts}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    );
  };

  const colorScore = (score, enemyScore) => {
    if (score > enemyScore) {
      return '#2a9d8f';
    }
    if (score < enemyScore) {
      return '#e76f51';
    }
    return '#e9c46a';
  };

  return (
    <Container maxWidth={false}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Box display="flex" justifyContent="space-around" flexDirection="row">
          <Chip
            label={teams[0].score}
            style={{ backgroundColor: colorScore(teams[0].score, teams[1].score) }}
          />
          <Chip
            label={teams[1].score}
            style={{ backgroundColor: colorScore(teams[1].score, teams[0].score) }}
          />
        </Box>
        <Box alignSelf="stretch" display="flex" flexDirection="row" marginTop="20px">
          {renderTeam(teams[0])}
          <Box width="20px" />
          {renderTeam(teams[1])}
        </Box>
      </Box>
    </Container>
  );
}

export default Resume;
