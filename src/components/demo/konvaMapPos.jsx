import React from 'react';
import {
  Layer, Image, Circle, Line, Group, Text,
} from 'react-konva';
import { Spring, animated } from 'react-spring/renderprops-konva';
import useImage from 'use-image';

const colorT = '#C7A247';
const colorCT = '#4374b0';

function ConvertX(x, chiftLeft, chiftRight, currentSizeImg) {
  return x * (currentSizeImg - chiftLeft - chiftRight) + chiftLeft;
}
function ConvertY(y, chiftTop, chiftBottom, currentSizeImg) {
  return currentSizeImg - (y * (currentSizeImg - chiftBottom - chiftTop) + chiftBottom);
}

function KonvaPlayer(values) {
  const { player } = values;
  const { numframe } = values;
  const { color } = values;
  const { delay } = values;
  const { chiftLeft } = values;
  const { chiftRight } = values;
  const { chiftTop } = values;
  const { chiftBottom } = values;
  const { currentSizeImg } = values;

  const x = ConvertX(player.positionX, chiftLeft, chiftRight, currentSizeImg);
  const y = ConvertY(player.positionY, chiftTop, chiftBottom, currentSizeImg);

  const [defuse] = useImage(`${process.env.PUBLIC_URL}/utils/defuser.png`);
  const [plant] = useImage(`${process.env.PUBLIC_URL}/utils/bombPlant.png`);

  if (!player.isAlive) {
    const crossRadius = 5;
    return (
      <Group>
        <Line
          id={player.steamID}
          x={x}
          y={y}
          points={[
            crossRadius, crossRadius,
            -crossRadius, -crossRadius,
            0, 0,
            -crossRadius, crossRadius,
            0, 0,
            crossRadius, -crossRadius,
          ]}
          stroke={player.side === 2 ? colorT : colorCT}
          strokeWidth={4}
        />
        <Circle
          id={player.steamID}
          x={x}
          y={y}
          radius={2}
          stroke={color}
          fill={color}
        />
      </Group>
    );
  }

  return (
    <Group>
      <Spring
        to={{ x, y }}
        config={{ duration: delay }}
      >
        {(props) => (
          <>
            <animated.Circle {...props} id={player.steamID} radius={6} fill={player.side === 2 ? colorT : colorCT} stroke="#000000" strokeWidth={1} />
            <animated.Line {...props} id={player.steamID} points={[0, 0, 10, 0]} stroke={color} rotation={360 - player.viewDirectionX} />
          </>
        )}

      </Spring>
      {player.isDefusing ? <Image image={defuse} width={30} height={30} x={x} y={y} rotation={270 - (numframe % 4) * 30} /> : false}
      {player.isPlanting ? <Image image={plant} width={30} height={27} x={x} y={y} rotation={270 - (numframe % 4) * 30} /> : false}
    </Group>
  );
}

function KonvaBomb(values) {
  const { bombPos } = values;
  const { delay } = values;
  const [bomb] = useImage(`${process.env.PUBLIC_URL}/utils/bombRed.png`);
  if (bombPos[0] === undefined) {
    return false;
  }


  const { playerCarrierSteamID } = values;

  const { chiftLeft } = values;
  const { chiftRight } = values;
  const { chiftTop } = values;
  const { chiftBottom } = values;
  const { currentSizeImg } = values;

  let chiftXBomb = 5;
  let chiftYBomb = 5;

  if (playerCarrierSteamID !== '') {
    chiftXBomb = 0;
    chiftYBomb = 0;
  }

  const x = ConvertX(bombPos[0], chiftLeft, chiftRight, currentSizeImg) - chiftXBomb;
  const y = ConvertY(bombPos[1], chiftTop, chiftBottom, currentSizeImg) - chiftYBomb;

  return (
    <Spring
      to={{ x, y }}
      config={{ duration: delay }}
    >
      {(props) => (
        <>
          <animated.Image {...props} image={bomb} width={16} height={16} />
        </>
      )}
    </Spring>
  );
}

function KonvaGrenade(values) {
  const { ID } = values;
  const { grenade } = values;
  const { grenadeImg } = values;
  const { delay } = values;
  const { state } = values;

  const { chiftLeft } = values;
  const { chiftRight } = values;
  const { chiftTop } = values;
  const { chiftBottom } = values;
  const { currentSizeImg } = values;

  const x = ConvertX(grenade.positionX, chiftLeft, chiftRight, currentSizeImg) - grenadeImg.naturalWidth / 2;
  const y = ConvertY(grenade.positionY, chiftTop, chiftBottom, currentSizeImg) - grenadeImg.naturalHeight / 2;

  if (state !== 4) {
    return <Image image={grenadeImg} x={x} y={y} />;
  }

  return (
    <Spring
      id={ID}
      to={{ x, y }}
      config={{ duration: delay }}
    >
      {(props) => (
        <>
          <animated.Image {...props} id={ID} image={grenadeImg} />
        </>
      )}
    </Spring>
  );
}

function KonvaMapPos(values) {
  const { positions } = values;
  const { timer } = values;
  const { playersColor } = values;
  const { chiftLeft } = values;
  const { chiftRight } = values;
  const { chiftTop } = values;
  const { chiftBottom } = values;
  const { currentSizeImg } = values;
  const { numframe } = values;
  let { delay } = values;
  if (delay === 0) {
    delay = 500;
  }

  const [smokeGre] = useImage(`${process.env.PUBLIC_URL}/grenades/smokemap.png`);
  const [molotovGre] = useImage(`${process.env.PUBLIC_URL}/grenades/molotovmap.png`);
  const [incendiaryGre] = useImage(`${process.env.PUBLIC_URL}/grenades/incendiarymap.png`);
  const [HEGre] = useImage(`${process.env.PUBLIC_URL}/grenades/hemap.png`);
  const [flashGre] = useImage(`${process.env.PUBLIC_URL}/grenades/flashmap.png`);
  const [decoyGre] = useImage(`${process.env.PUBLIC_URL}/grenades/decoymap.png`);

  const [smoke] = useImage(`${process.env.PUBLIC_URL}/grenades/smoke.png`);
  const [explosion] = useImage(`${process.env.PUBLIC_URL}/grenades/explosion.png`);
  const [flash] = useImage(`${process.env.PUBLIC_URL}/grenades/flash.png`);
  const [bomb] = useImage(`${process.env.PUBLIC_URL}/utils/bombRed.png`);

  if (positions.grenades === undefined) {
    positions.grenades = [];
  }

  if (positions.playersPositions === undefined) {
    positions.playersPositions = [];
  }

  return (
    <Layer>
      {positions.bombPlanted
        ? (
          <>
            <Text fontSize={40} x={10} y={10} align="left" text={timer} fill="red" />
            <Image image={bomb} x={120} y={10} />
          </>
        )
        : <Text fontSize={40} x={10} y={10} align="left" text={timer} fill="white" />}
      {positions.grenades.map((grenade, idx) => {
        let grenadeImg;

        switch (grenade.grenadeType) {
          case 501:
            grenadeImg = decoyGre;
            break;
          case 502:
            if (grenade.state === 4) {
              grenadeImg = molotovGre;
            }
            break;
          case 503:
            if (grenade.state === 4) {
              grenadeImg = incendiaryGre;
            } else {
              const fire = [];
              for (let numPos = 0; numPos < grenade.fire.length; numPos += 2) {
                fire.push(ConvertX(grenade.fire[numPos], chiftLeft, chiftRight, currentSizeImg));
                fire.push(ConvertY(grenade.fire[numPos + 1], chiftTop, chiftBottom, currentSizeImg));
              }
              return (
                <Group key={idx}>
                  <Line
                    id={idx}
                    points={fire}
                    fill="#FF000080"
                    stroke="red"
                    strokeWidth={1}
                    closed
                  />
                </Group>
              );
            }
            break;
          case 504:
            if (grenade.state === 4) {
              grenadeImg = flashGre;
            } else {
              grenadeImg = flash;
            }
            break;
          case 505:
            if (grenade.state === 4) {
              grenadeImg = smokeGre;
            } else {
              grenadeImg = smoke;
            }
            break;
          case 506:
            if (grenade.state === 4) {
              grenadeImg = HEGre;
            } else {
              grenadeImg = explosion;
            }
            break;
          default:
            return false;
        }

        if (grenadeImg === undefined) {
          return false;
        }

        return (
          <KonvaGrenade
            key={grenade.id}
            ID={grenade.id}
            grenade={grenade}
            delay={delay}
            state={grenade.state}
            grenadeImg={grenadeImg}
            chiftLeft={chiftLeft}
            chiftRight={chiftRight}
            chiftTop={chiftTop}
            chiftBottom={chiftBottom}
            currentSizeImg={currentSizeImg}
          />
        );
      })}
      {positions.playersPositions.map((player, idx) => (
        <KonvaPlayer
          key={player.steamID}
          numframe={numframe}
          color={playersColor[player.steamID]}
          player={player}
          delay={delay}
          chiftLeft={chiftLeft}
          chiftRight={chiftRight}
          chiftTop={chiftTop}
          chiftBottom={chiftBottom}
          currentSizeImg={currentSizeImg}
        />
      ))}
      <KonvaBomb
        bombPos={[positions.bombLastPosDownX, positions.bombLastPosDownY, positions.bombLastPosDownZ]}
        playerCarrierSteamID={positions.playerCarrierSteamID}
        delay={delay}
        chiftLeft={chiftLeft}
        chiftRight={chiftRight}
        chiftTop={chiftTop}
        chiftBottom={chiftBottom}
        currentSizeImg={currentSizeImg}
      />
    </Layer>
  );
}

export default KonvaMapPos;
