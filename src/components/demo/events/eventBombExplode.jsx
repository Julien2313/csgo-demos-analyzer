import React from 'react';

import {
  Box,
  Paper,
  Typography,
} from '@material-ui/core';

export default function eventBombExplode() {
  const label = 'Bombe exploded';
  return (
    <Paper variant="outlined" style={{ backgroundColor: '#00000080' }}>
      <Typography variant="h6" style={{ display: 'flex' }}>
        <Box fontFamily="Rajdhani" m={1}>
          {label}
        </Box>
      </Typography>
    </Paper>
  );
}
