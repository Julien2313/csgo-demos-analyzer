import React from 'react';

import {
  Box,
  Paper,
  Typography,
} from '@material-ui/core';
import Time from '../../box/time';

export default function eventBombPlant(
  planter, bombsite, timeBombPlanted,
) {
  const label = `${planter} planted bombe on ${bombsite}`;
  return (
    <Paper variant="outlined" style={{ backgroundColor: '#00000080' }}>
      <Typography variant="h6" style={{ display: 'flex' }}>
        <Time time={timeBombPlanted} />
        <Box
          fontFamily="Rajdhani"
          m={1}
          style={{
            lineHeight: 0.8,
            fontSize: '1.0rem',
            letterSpacing: '0.0em',
          }}
        >
          {label}
        </Box>
      </Typography>
    </Paper>
  );
}
