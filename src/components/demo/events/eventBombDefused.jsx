import React from 'react';

import {
  Box,
  Paper,
  Typography,
} from '@material-ui/core';
import Time from '../../box/time';

export default function eventBombDefused(
  defuser, timeRemainded,
) {
  const label = `${defuser} defused bombe`;
  return (
    <Paper variant="outlined" style={{ backgroundColor: '#00000080' }}>
      <Typography variant="h6" style={{ display: 'flex' }}>
        <Time time={timeRemainded} />
        <Box fontFamily="Rajdhani" m={1}>
          {label}
        </Box>
      </Typography>
    </Paper>
  );
}
