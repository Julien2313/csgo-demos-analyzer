import eventKill from './eventKill';
import eventBombPlant from './eventBombPlant';
import eventBombDefused from './eventBombDefused';
import eventBombExplode from './eventBombExplode';

export default function switchEvent(event) {
  if (event.type === 1) {
    return eventKill(event.timeKill, event.killerSide, event.victimSide,
      event.killer, event.weaponName, event.victimName, event.isHeadShot, event.isWallBang);
  }
  if (event.type === 2) {
    return eventBombPlant(event.planter, event.bombsite, event.timePlanted);
  }
  if (event.type === 3) {
    return eventBombDefused(event.defuser, event.timeRemainded);
  }
  if (event.type === 4) {
    return eventBombExplode();
  }
  return false;
}
