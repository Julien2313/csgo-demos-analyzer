import React from 'react';

import {
  Box,
  Paper,
  Typography,
} from '@material-ui/core';

import Time from '../../box/time';
import Weapon from '../../icons/weapon';
import NameKillFeed from '../../box/nameKillFeed';

const CTColor = '#216aff';
const TColor = '#C7A247';

export default function eventKill(
  timeKill, killerSide, victimSide,
  killer, weaponName, victim, isHeadShot, isWallBang,
) {
  let colorKiller = '#FFFFFF';
  if (killerSide === 3) {
    colorKiller = CTColor;
  } else if (killerSide === 2) {
    colorKiller = TColor;
  }

  let colorVictim = '#FFFFFF';
  if (victimSide === 3) {
    colorVictim = CTColor;
  } else if (victimSide === 2) {
    colorVictim = TColor;
  }

  return (
    <Paper variant="outlined" style={{ backgroundColor: '#00000080' }}>
      <Typography variant="h6" style={{ display: 'flex' }}>
        <Time time={timeKill} />
        <NameKillFeed name={killer} color={colorKiller} />
        <Weapon weapon={weaponName} />

        { isWallBang ? (
          <Box style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <img height="30" width="30" alt="HS" src={`${process.env.PUBLIC_URL}/killfeed/wallbang.png`} />
          </Box>
        ) : false }
        { isHeadShot ? (
          <Box style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <img height="30" width="30" alt="HS" src={`${process.env.PUBLIC_URL}/killfeed/headshot.png`} />
          </Box>
        ) : false }

        <NameKillFeed name={victim} color={colorVictim} />
      </Typography>
    </Paper>
  );
}
