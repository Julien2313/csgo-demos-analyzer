import React from 'react';

import {
  Box,
  Paper,
  Typography,
} from '@material-ui/core';
import Time from '../../box/time';

export default function eventRoundFreezetimeEnd() {
  const label = 'Freeze time end';
  return (
    <Paper variant="outlined" style={{ backgroundColor: '#00000080' }}>
      <Typography variant="h6" style={{ display: 'flex' }}>
        <Time time="00:00" />
        <Box fontFamily="Rajdhani" m={1}>
          {label}
        </Box>
      </Typography>
    </Paper>
  );
}
