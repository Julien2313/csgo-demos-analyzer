/* eslint-disable no-continue */
import React, { useState } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import LinearProgress from '@material-ui/core/LinearProgress';
import { toast } from 'react-toastify';
import { Container } from '@material-ui/core';

import apiDemo from '../../api/demo';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  button: {},
});

function Upload() {
  const [demos, setDemos] = useState();
  const [cptUpload, setCptUpload] = useState(0);
  const [nbrTotalUpload, setNbrTotalUpload] = useState(0);
  const [progress, setProgress] = useState(0);

  const classes = useStyles();

  const findDemoInfo = (demosToSearch, nameDemoToFind) => {
    for (let numFile = 0; numFile < demosToSearch.length; numFile += 1) {
      if (demosToSearch[numFile].name.slice(0, -9) === nameDemoToFind) {
        return demosToSearch[numFile];
      }
    }
    return null;
  };

  const uploadDemo = async () => {
    if (demos === undefined) {
      return;
    }
    let nbrDemos = 0;
    let error = false;
    setNbrTotalUpload(0);
    for (let numFile = 0; numFile < demos.length; numFile += 1) {
      if (demos[numFile].name.slice(-9) === ('.dem.info')) {
        continue;
      }
      if (demos[numFile].name.slice(-4) !== ('.dem')) {
        toast.error(`${demos[numFile].name} isn't a demo`, {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        return;
      }
      nbrDemos += 1;
    }
    setNbrTotalUpload(nbrDemos);
    nbrDemos = 0;
    for (let numFile = 0; numFile < demos.length; numFile += 1) {
      const demo = demos[numFile];
      if (demo.name.slice(-9) === ('.dem.info')) {
        continue;
      }
      const demoInfo = findDemoInfo(demos, demo.name.slice(0, -4));

      if (demoInfo === null) {
        toast.error('Some .info are missing', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        return;
      }

      try {
        const data = new FormData();
        data.append('demoInfo', demoInfo);
        const res = await apiDemo.exists(data);
        if (res.status === 200) {
          continue;
        }
      } catch (err) {
        if (err.response === undefined) {
          toast.error('An error occurred', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          return;
        }
        switch (err.response.status) {
          case 404:
            break;
          case 429:
            toast.error('Too many request !', {
              position: 'top-center',
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
            break;
          default:
            toast.error('An error occurred', {
              position: 'top-center',
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
            continue;
        }
      }

      try {
        const data = new FormData();
        data.append('demo', demo);
        data.append('demoInfo', demoInfo);
        await apiDemo.uploadDemo(data, setProgress);
        data.delete('demo');
      } catch (err) {
        error = true;
        if (err.response === undefined) {
          toast.error('An error occurred', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          return;
        }
        switch (err.response.status) {
          case 403:
            toast.error('SteamID not set', {
              position: 'top-center',
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
            return;
          case 404:
            toast.error("You are not in a demo you're trying to upload", {
              position: 'top-center',
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
            break;
          case 400:
            toast.error("The demo isn't valid or check if the .info is linked to the .dem", {
              position: 'top-center',
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
            break;
          case 409:
            toast.error('The demo has already been uploaded', {
              position: 'top-center',
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
            break;
          case 429:
            toast.error('Too many request !', {
              position: 'top-center',
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
            break;
          default:
            toast.error('An error occurred', {
              position: 'top-center',
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
            break;
        }
      }
      nbrDemos += 1;
      setCptUpload(nbrDemos);
    }
    setDemos([]);
    if (error) {
      toast.info("One or more error occurred. Some demos couldn't be uploaded", {
        position: 'top-center',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    } else {
      toast.success('All the demos have been uploaded', {
        position: 'top-center',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  };

  return (
    <Container>
      <input type="file" multiple onChange={(newDemos) => setDemos(newDemos.target.files)} />
      <br />
      <Button
        variant="contained"
        color="default"
        className={classes.button}
        startIcon={<CloudUploadIcon />}
        onClick={uploadDemo}
      >
        Upload
      </Button>
      You can upload as many demos as you want. But check that for every .dem you have the .dem.info
      <LinearProgress variant="determinate" value={progress} />
      {cptUpload}
      /
      {nbrTotalUpload}
    </Container>
  );
}

export default Upload;
