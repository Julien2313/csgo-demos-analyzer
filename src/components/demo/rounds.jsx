/* eslint-disable no-continue */
import React, { useState, useEffect, useMemo } from 'react';
import {
  Stage,
} from 'react-konva';

import {
  Container,
  Slider,
  IconButton,
  Box,
  Paper,
  Button,
} from '@material-ui/core';

import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';
import FastForwardIcon from '@material-ui/icons/FastForward';
import Replay5Icon from '@material-ui/icons/Replay5';
import SlowMotionVideoIcon from '@material-ui/icons/SlowMotionVideo';
import SkipNextIcon from '@material-ui/icons/SkipNext';
import SkipPreviousIcon from '@material-ui/icons/SkipPrevious';

import KonvaMapPos from './konvaMapPos';
import MatrixWinRounds from './matrixWinRounds';
import PlayersCard from './playersCard';
import map from '../utils/map';
import useInterval from '../utils/useInterval';
import switchEvent from './events/event';
import switchMarksSlider from './marksSlider/marksSlider';

function Rounds(values) {
  const [numRoundSelected, setNumRoundSelected] = useState(0);
  const [nbrFrames, setNbrFrames] = useState([1]);

  const [positions, setPositions] = useState({});
  const [roundsParsed, setRoundsParsed] = useState({});

  const { rawRounds } = values;
  const { mapName } = values;
  const { playersColor } = values;

  const [numFramePlayer, setNumFramePlayer] = useState(0);
  const [delay, setDelay] = useState(0);

  const [chiftLeft, setChiftLeft] = useState(0);
  const [chiftRight, setChiftRight] = useState(0);
  const [chiftTop, setChiftTop] = useState(0);
  const [chiftBottom, setChiftBottom] = useState(0);

  const [events, setEvents] = useState([]);
  const [marksSlider, setMarksSlider] = useState([]);

  const currentSizeImg = 650;

  function updateFrame(nf, nr, r) {
    let numFrame = nf;
    let numRound = nr;
    const rounds = r;
    if (numFrame < 0) {
      numFrame = 0;
    }
    if (numFrame >= rounds[numRound].length) {
      if (numRound >= rounds.length) {
        setDelay(0);
        return;
      }
      numRound += 1;
      setNumRoundSelected(numRound);
      numFrame = 0;
    }

    let playersCard;
    for (let lastNumFrameCards = numFrame; lastNumFrameCards >= 0; lastNumFrameCards -= 1) {
      if (rounds[numRound][lastNumFrameCards].playersCard !== undefined) {
        if (rounds[numRound][lastNumFrameCards].playersCard.length > 0) {
          playersCard = rounds[numRound][lastNumFrameCards].playersCard;
          break;
        }
      }
    }

    rounds[numRound][numFrame].playersCard = playersCard;
    setPositions(rounds[numRound][numFrame]);
    setNumFramePlayer(numFrame);
  }

  const handleChangeSlider = (event, value) => {
    updateFrame(value, numRoundSelected, roundsParsed);
  };

  useEffect(() => {
    function buildRounds() {
      const framesRounds = [];
      const localNumFrames = [];
      const eventsRounds = [];
      const marksSliderRounds = [];

      rawRounds.forEach((round, numRound) => {
        if (rawRounds[numRound].frames.length !== 0) {
          const framesRound = rawRounds[numRound].frames;
          framesRounds.push(framesRound);
          localNumFrames.push(rawRounds[numRound].frames.length);

          const eventsRound = [];
          const marksSliderRound = [];

          for (let numFrame = 0; numFrame < framesRound.length; numFrame += 1) {
            const eventParsed = JSON.parse(atob(framesRound[numFrame].events));
            if (eventParsed === null) {
              continue;
            }

            eventParsed.forEach((e, numE) => {
              const eventButton = switchEvent(e);
              if (eventButton === false) {
                return;
              }
              eventsRound.push(
                <Box
                  key={numRound * 10000 + numFrame * 1000 + numE * 100 + 1}
                  display="flex"
                  justifyContent="flex-start"
                  flexDirection="row"
                  size="small"
                >
                  <Button
                    size="small"
                    variant="contained"
                    onClick={() => { setPositions(framesRound[numFrame]); updateFrame(numFrame, numRound, framesRounds); }}
                  >
                    {switchEvent(e)}
                  </Button>
                </Box>,
              );
              if (marksSliderRound.length > 0) {
                if (marksSliderRound[marksSliderRound.length - 1].value === numFrame) {
                  // marksSliderRound[marksSliderRound.length - 1].label.div.push(switchMarksSlider(e));
                  return;
                }
              }
              marksSliderRound.push({
                value: numFrame,
                label: (
                  <div
                    key={numRound * 10000 + numFrame * 1000 + numE * 100 + 1}
                  >
                    {switchMarksSlider(e)}
                  </div>
                ),
              });
            });
          }
          eventsRounds.push(eventsRound);
          marksSliderRounds.push(marksSliderRound);
        }
      });

      setEvents(eventsRounds);
      setMarksSlider(marksSliderRounds);
      setNbrFrames(localNumFrames);
      setRoundsParsed(framesRounds);
      setPositions(framesRounds[0][0]);
    }

    const chifts = map.returnSettingsByMap(mapName, currentSizeImg);
    setChiftLeft(chifts[0]);
    setChiftRight(chifts[1]);
    setChiftTop(chifts[2]);
    setChiftBottom(chifts[3]);

    buildRounds();
  }, []);

  const matrixRounds = useMemo(() => (
    <MatrixWinRounds
      rawRounds={rawRounds}
      setNumRoundSelected={setNumRoundSelected}
      numRoundSelected={numRoundSelected}
      setNumFramePlayer={setNumFramePlayer}
      setPositions={setPositions}
      roundsParsed={roundsParsed}
    />
  ), [rawRounds, setNumRoundSelected, numRoundSelected, setNumFramePlayer, setPositions, roundsParsed]);

  function tester() {
    handleChangeSlider(null, numFramePlayer + 1);
  }

  useInterval(tester, delay);

  return (
    <>
      <Box alignSelf="stretch" display="flex" flexDirection="row" justifyContent="center">
        <Box>
          <Stage
            width={currentSizeImg}
            height={currentSizeImg}
            style={{
              backgroundImage: `url(${process.env.PUBLIC_URL}/radar/${mapName}.png)`,
              height: currentSizeImg,
              width: currentSizeImg,
              backgroundSize: 'cover',
            }}
          >
            <KonvaMapPos
              positions={positions}
              timer={positions.timer}
              numframe={numFramePlayer}
              playersColor={playersColor}
              delay={delay}
              chiftLeft={chiftLeft}
              chiftRight={chiftRight}
              chiftTop={chiftTop}
              chiftBottom={chiftBottom}
              currentSizeImg={currentSizeImg}
            />
          </Stage>
        </Box>
        <Box>
          <PlayersCard playersCard={positions.playersCard} playersColor={playersColor} sizeImg={currentSizeImg} />
        </Box>
      </Box>
      <Paper>
        <Slider
          defaultValue={0}
          aria-labelledby="continuous-slider"
          step={1}
          min={0}
          max={nbrFrames[numRoundSelected] - 1}
          valueLabelDisplay="auto"
          onChange={handleChangeSlider}
          value={numFramePlayer}
          marks={marksSlider[numRoundSelected]}
        />
      </Paper>
      <Paper>
        <IconButton
          size="small"
          onClick={() => {
            if (numFramePlayer < 30 && numRoundSelected - 1 > 0) {
              setNumRoundSelected(numRoundSelected - 1);
            }
            handleChangeSlider(undefined, 0);
          }}
        >
          <SkipPreviousIcon fontSize="inherit" />
        </IconButton>
        <IconButton
          size="small"
          onClick={() => {
            handleChangeSlider(undefined, Math.max(0, numFramePlayer - 4 * 5));
          }}
        >
          <Replay5Icon fontSize="inherit" />
        </IconButton>
        <IconButton
          size="small"
          onClick={() => { setDelay(0); }}
        >
          <PauseIcon fontSize="inherit" />
        </IconButton>
        <IconButton
          size="small"
          onClick={() => { setDelay(500); }}
        >
          <PlayArrowIcon fontSize="inherit" />
        </IconButton>
        <IconButton
          size="small"
          onClick={() => { setDelay(250); }}
        >
          <FastForwardIcon fontSize="inherit" />
        </IconButton>
        <IconButton
          size="small"
          onClick={() => { tester(); }}
        >
          <SlowMotionVideoIcon fontSize="inherit" />
        </IconButton>
        <IconButton
          size="small"
          onClick={() => {
            if (numRoundSelected + 1 >= roundsParsed.length) {
              return;
            }
            setNumRoundSelected(numRoundSelected + 1);
            handleChangeSlider(undefined, 0);
          }}
        >
          <SkipNextIcon fontSize="inherit" />
        </IconButton>
      </Paper>
      <Container>
        {matrixRounds}
        {events[numRoundSelected]}
      </Container>
    </>
  );
}

export default Rounds;
