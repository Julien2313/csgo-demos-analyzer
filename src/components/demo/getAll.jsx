import React from 'react';
import { useHistory } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import {
  Chip,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from '@material-ui/core';

import Skeleton from '@material-ui/lab/Skeleton';

import TimeAgo from 'react-timeago';
import Moment from 'moment';
import { padStart, map } from 'lodash';

function GetAll(values) {
  const { demos } = values.demos;
  const { isGettingDemos } = values.demos;
  const { isNewDemo } = values;
  const history = useHistory();

  const useStyles = makeStyles(() => ({
    root: {
      '&:hover': {
        color: 'black',
        background: '#EEEEEE',

      },
    },
  }));

  const classes = useStyles();
  const redirectStats = (demoID) => {
    history.push({
      pathname: `/demo/stats/${demoID}`,
      // state: demoID,
    });
  };

  const displayWin = (endState) => {
    if (endState === null) {
      return 'Undefined';
    }
    if (endState === 1) {
      return 'Won';
    }
    if (endState === 2) {
      return 'Lost';
    }
    if (endState === 3) {
      return 'Draw';
    }

    return 'Undefined';
  };

  const colorLine = (endState) => {
    const endStateComputed = displayWin(endState);
    if (endStateComputed === 'Won') {
      return '#2a9d8f80';
    }
    if (endStateComputed === 'Lost') {
      return '#e76f5180';
    }
    return '#e9c46a80';
  };

  if (isGettingDemos) {
    return (
      <TableContainer component={Paper}>
        <Table classes={classes} size="small" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell width="72">Demo</TableCell>
              <TableCell align="left">Map name</TableCell>
              <TableCell align="left">Score</TableCell>
              <TableCell align="left">Date</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {map([null, null, null, null], (_, idx) => (
              <TableRow key={idx}>
                <TableCell width="72" component="th" scope="row"><Skeleton variant="rect" width={72} height={72} /></TableCell>
                <TableCell width="150" align="left"><Skeleton variant="text" /></TableCell>
                <TableCell width="150" align="left"><Skeleton variant="text" /></TableCell>
                <TableCell width="450" align="left"><Skeleton variant="text" /></TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell width="72">Demo</TableCell>
            <TableCell align="left">Map name</TableCell>
            <TableCell align="left">Score</TableCell>
            <TableCell align="left">Date</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {isNewDemo ? (
            <TableRow>
              <TableCell width="72" component="th" scope="row"><Skeleton variant="rect" width={72} height={72} /></TableCell>
              <TableCell width="150" align="left"><Skeleton variant="text" /></TableCell>
              <TableCell width="150" align="left"><Skeleton variant="text" /></TableCell>
              <TableCell width="450" align="left"><Skeleton variant="text" /></TableCell>
            </TableRow>
          ) : false}
          {demos.map((row) => (
            <TableRow
              key={row.id}
              classes={classes}
              onClick={() => redirectStats(row.id)}
              style={{ cursor: 'pointer' }}
            >
              <TableCell width="72" component="th" scope="row"><img height="50" width="50" alt="avatar" src={`${process.env.PUBLIC_URL}/mapbadge/${row.mapName}.png`} /></TableCell>
              <TableCell align="left">{row.mapName}</TableCell>
              <TableCell align="left">
                <Chip
                  style={{ backgroundColor: colorLine(row.endState) }}
                  label={`${padStart(row.score[0], 2, 0)} : ${padStart(row.score[1], 2, 0)}`}
                />
              </TableCell>
              <TableCell align="left">
                <TimeAgo date={row.date} />
                {', '}
                {Moment(row.date).format('DD/MM/YYYY HH:mm', true)}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default GetAll;
