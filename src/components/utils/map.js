function returnSettingsByMap(mapName, currentSizeImg) {
  let chiftLeft;
  let chiftRight;
  let chiftTop;
  let chiftBottom;

  switch (mapName) {
    case 'dust2':
      chiftLeft = 30;
      chiftRight = 26;
      chiftTop = 12;
      chiftBottom = 10;
      break;
    case 'anubis':
      chiftLeft = 76;
      chiftRight = 71;
      chiftTop = 14;
      chiftBottom = 19;
      break;
    case 'cache':
      chiftLeft = 16;
      chiftRight = 30;
      chiftTop = 83;
      chiftBottom = 83;
      break;
    case 'inferno':
      chiftLeft = 32;
      chiftRight = 25;
      chiftTop = 27;
      chiftBottom = 37;
      break;
    case 'mirage':
      chiftLeft = 55;
      chiftRight = 42;
      chiftTop = 81;
      chiftBottom = 77;
      break;
    case 'mutiny':
      chiftLeft = 78;
      chiftRight = 79;
      chiftTop = 13;
      chiftBottom = 13;
      break;
    case 'nuke':
      chiftLeft = 32;
      chiftRight = 16;
      chiftTop = 141;
      chiftBottom = 128;
      break;
    case 'overpass':
      chiftLeft = 80;
      chiftRight = 45;
      chiftTop = 8;
      chiftBottom = 2;
      break;
    case 'swamp':
      chiftLeft = 83;
      chiftRight = 100;
      chiftTop = 75;
      chiftBottom = 10;
      break;
    case 'train':
      chiftLeft = 19;
      chiftRight = 30;
      chiftTop = 64;
      chiftBottom = 64;
      break;
    case 'vertigo':
      chiftLeft = 65;
      chiftRight = 118;
      chiftTop = 82;
      chiftBottom = 100;
      break;
    case 'ancient':
      chiftLeft = 66;
      chiftRight = 79;
      chiftTop = 63;
      chiftBottom = 45;
      break;
    case 'engage':
      chiftLeft = 90;
      chiftRight = 82;
      chiftTop = 13;
      chiftBottom = 12;
      break;
    case 'apollo':
      chiftLeft = 61;
      chiftRight = 61;
      chiftTop = 13;
      chiftBottom = 14;
      break;
    case 'mocha':
      chiftLeft = 71;
      chiftRight = 89;
      chiftTop = 51;
      chiftBottom = 61;
      break;
    case 'grind':
      chiftLeft = 39;
      chiftRight = 23;
      chiftTop = 46;
      chiftBottom = 49;
      break;
    default:
      chiftLeft = 0;
      chiftRight = 0;
      chiftTop = 0;
      chiftBottom = 0;
  }

  chiftLeft *= currentSizeImg / 512;
  chiftRight *= currentSizeImg / 512;
  chiftTop *= currentSizeImg / 512;
  chiftBottom *= currentSizeImg / 512;

  return [chiftLeft, chiftRight, chiftTop, chiftBottom];
}

const mapsName = [
  {
    label: 'Ancient', value: 'ancient', activeMap: true, officialMapPol: true,
  },
  {
    label: 'Dust2', value: 'dust2', activeMap: true, officialMapPol: true,
  },
  {
    label: 'Inferno', value: 'inferno', activeMap: true, officialMapPol: true,
  },
  {
    label: 'Mirage', value: 'mirage', activeMap: true, officialMapPol: true,
  },
  {
    label: 'Nuke', value: 'nuke', activeMap: true, officialMapPol: true,
  },
  {
    label: 'Overpass', value: 'overpass', activeMap: true, officialMapPol: true,
  },
  {
    label: 'Vertigo', value: 'vertigo', activeMap: true, officialMapPol: true,
  },
  {
    label: 'Abbey', value: 'abbey', activeMap: false, officialMapPol: false,
  },
  {
    label: 'Agency', value: 'agency', activeMap: true, officialMapPol: false,
  },
  {
    label: 'Anubis', value: 'anubis', activeMap: false, officialMapPol: false,
  },
  {
    label: 'Apollo', value: 'apollo', activeMap: false, officialMapPol: false,
  },
  {
    label: 'Assault', value: 'assault', activeMap: false, officialMapPol: false,
  },
  {
    label: 'Biome', value: 'biome', activeMap: false, officialMapPol: false,
  },
  {
    label: 'Breach', value: 'breach', activeMap: false, officialMapPol: false,
  },
  {
    label: 'Cache', value: 'cache', activeMap: true, officialMapPol: false,
  },
  {
    label: 'Chlorine', value: 'chlorine', activeMap: false, officialMapPol: false,
  },
  {
    label: 'Cobblestone', value: 'cbble', activeMap: false, officialMapPol: false,
  },
  {
    label: 'Engage', value: 'engage', activeMap: true, officialMapPol: false,
  },
  {
    label: 'Grind', value: 'grind', activeMap: true, officialMapPol: false,
  },
  {
    label: 'Italy', value: 'italy', activeMap: false, officialMapPol: false,
  },
  {
    label: 'Militia', value: 'militia', activeMap: false, officialMapPol: false,
  },
  {
    label: 'Mocha', value: 'mocha', activeMap: true, officialMapPol: false,
  },
  {
    label: 'Mutiny', value: 'mutiny', activeMap: false, officialMapPol: false,
  },
  {
    label: 'Office', value: 'office', activeMap: true, officialMapPol: false,
  },
  {
    label: 'Swamp', value: 'swamp', activeMap: false, officialMapPol: false,
  },
  {
    label: 'Train', value: 'train', activeMap: true, officialMapPol: false,
  },
  {
    label: 'Workout', value: 'workout', activeMap: false, officialMapPol: false,
  },
  {
    label: 'Zoo', value: 'zoo', activeMap: false, officialMapPol: false,
  },
];

export default { mapsName, returnSettingsByMap };
