const pistols = [
  { label: 'P2000', weaponID: 1 },
  { label: 'Glock-18', weaponID: 2 },
  { label: 'p250', weaponID: 3 },
  { label: 'Desert Eagle', weaponID: 4 },
  { label: 'Five-SeveN', weaponID: 5 },
  { label: 'Dual Berettas', weaponID: 6 },
  { label: 'Tec-9', weaponID: 7 },
  { label: 'CZ75 Auto', weaponID: 8 },
  { label: 'USP-S', weaponID: 9 },
  { label: 'R8 Revolver', weaponID: 10 },
];

const smgs = [
  { label: 'MP7', weaponID: 101 },
  { label: 'MP9', weaponID: 102 },
  { label: 'PP-Bizon', weaponID: 103 },
  { label: 'MAC-10"', weaponID: 104 },
  { label: 'UMP-45', weaponID: 105 },
  { label: 'P90', weaponID: 106 },
  { label: 'MP5-SD', weaponID: 107 },
];

const heavies = [
  { label: 'Sawed-Off', weaponID: 201 },
  { label: 'Nova', weaponID: 202 },
  { label: 'SWAG-7', weaponID: 203 },
  { label: 'XM1014', weaponID: 204 },
  { label: 'M249', weaponID: 205 },
  { label: 'Negev', weaponID: 206 },
];

const rifles = [
  { label: 'Galil AR', weaponID: 301 },
  { label: 'FAMAS', weaponID: 302 },
  { label: 'AK-47', weaponID: 303 },
  { label: 'M4A4', weaponID: 304 },
  { label: 'M4A1', weaponID: 305 },
  { label: 'SSG 08', weaponID: 306 },
  { label: 'SG 553', weaponID: 307 },
  { label: 'AUG', weaponID: 308 },
  { label: 'AWP', weaponID: 309 },
  { label: 'SCAR-20', weaponID: 310 },
  { label: 'G3SG1', weaponID: 311 },
];

const equipments = [
  { label: 'Zeus x27', weaponID: 401 },
  { label: 'C4', weaponID: 404 },
  { label: 'Knife', weaponID: 405 },
  { label: 'World', weaponID: 407 },
];

const grenades = [
  { label: 'Molotov', weaponID: 502 },
  { label: 'Incendiary Grenade', weaponID: 503 },
  { label: 'HE Grenade', weaponID: 506 },
];

export default {
  pistols,
  smgs,
  heavies,
  rifles,
  equipments,
  grenades,
};
