/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';

import {
  DialogTitle, Dialog, Button, Checkbox, FormControl, Box, FormGroup, FormControlLabel, Paper,
} from '@material-ui/core';
import Draggable from 'react-draggable';

import weapons from '../utils/weapons';

function WeaponsPicker({
  weaponsKiller, setWeaponsKiller, label,
}) {
  const [openWeaponSelected, setOpenWeaponSelected] = useState(false);
  const [pos, setPos] = useState({ x: 0, y: 0 });

  const [pistolsSelected, setPistolsSelected] = useState([]);
  const [smgsSelected, setSmgsSelected] = useState([]);
  const [heaviesSelected, setHeaviesSelected] = useState([]);
  const [riflesSelected, setRiflesSelected] = useState([]);
  const [equipmentsSelected, setEquipmentsSelected] = useState([]);
  const [grenadesSelected, setGrenadesSelected] = useState([]);

  useEffect(() => {
    const weaponsFilters = [];

    const pistolsFilter = [];
    weapons.pistols.forEach((pistol) => {
      pistolsFilter.push({
        weaponID: pistol.weaponID.toString(),
        label: pistol.label,
        checked: true,
      });
      weaponsFilters.push(pistol.weaponID.toString());
    });
    setPistolsSelected(pistolsFilter);

    const smgsFilter = [];
    weapons.smgs.forEach((smg) => {
      smgsFilter.push({
        weaponID: smg.weaponID.toString(),
        label: smg.label,
        checked: true,
      });
      weaponsFilters.push(smg.weaponID.toString());
    });
    setSmgsSelected(smgsFilter);

    const heaviesFilter = [];
    weapons.heavies.forEach((heavy) => {
      heaviesFilter.push({
        weaponID: heavy.weaponID.toString(),
        label: heavy.label,
        checked: true,
      });
      weaponsFilters.push(heavy.weaponID.toString());
    });
    setHeaviesSelected(heaviesFilter);

    const riflesFilter = [];
    weapons.rifles.forEach((rifle) => {
      riflesFilter.push({
        weaponID: rifle.weaponID.toString(),
        label: rifle.label,
        checked: true,
      });
      weaponsFilters.push(rifle.weaponID.toString());
    });
    setRiflesSelected(riflesFilter);

    const equipmentsFilter = [];
    weapons.equipments.forEach((equipment) => {
      equipmentsFilter.push({
        weaponID: equipment.weaponID.toString(),
        label: equipment.label,
        checked: true,
      });
      weaponsFilters.push(equipment.weaponID.toString());
    });
    setEquipmentsSelected(equipmentsFilter);

    const grenadesFilter = [];
    weapons.grenades.forEach((grenade) => {
      grenadesFilter.push({
        weaponID: grenade.weaponID.toString(),
        label: grenade.label,
        checked: true,
      });
      weaponsFilters.push(grenade.weaponID.toString());
    });
    setGrenadesSelected(grenadesFilter);

    setWeaponsKiller(weaponsFilters);
  }, []);

  function handleChangeSelected(event, weaponsClass, setWeaponsClass) {
    const p = [];
    const w = weaponsKiller.slice();
    weaponsClass.forEach((pistol, numPistol) => {
      p.push(pistol);
      if (pistol.weaponID.toString() === event.target.name) {
        p[numPistol].checked = event.target.checked;
      }
      if (p[numPistol].checked) {
        if (!w.includes(p[numPistol].weaponID)) {
          w.push(p[numPistol].weaponID);
        }
      } else if (w.includes(p[numPistol].weaponID)) {
        w.splice(w.indexOf(p[numPistol].weaponID), 1);
      }
    });

    setWeaponsKiller(w);
    setWeaponsClass(p);
  }

  function handleResetPistols(weaponsClass, setWeaponsClass) {
    const p = [];
    const w = weaponsKiller.slice();

    const nbrWeapons = weaponsClass.length;
    let nbrWeaponsSelected = 0;
    let toBeChecked = false;

    weaponsClass.forEach((pistol) => {
      if (pistol.checked) {
        nbrWeaponsSelected += 1;
      }
    });

    if (nbrWeaponsSelected > nbrWeapons / 2) {
      toBeChecked = true;
    }
    if (nbrWeaponsSelected === nbrWeapons) {
      toBeChecked = false;
    }
    if (nbrWeaponsSelected === 0) {
      toBeChecked = true;
    }

    weaponsClass.forEach((pistol, numPistol) => {
      p.push(pistol);
      p[numPistol].checked = toBeChecked;
      if (p[numPistol].checked) {
        if (!w.includes(p[numPistol].weaponID)) {
          w.push(p[numPistol].weaponID);
        }
      } else if (w.includes(p[numPistol].weaponID)) {
        w.splice(w.indexOf(p[numPistol].weaponID), 1);
      }
    });

    setWeaponsKiller(w);
    setWeaponsClass(p);
  }

  function PaperComponent(props) {
    return (
      <Draggable
        handle="#draggable-dialog-title"
        cancel={'[class*="MuiDialogContent-root"]'}
        onStop={(e, data) => { setPos({ x: data.x, y: data.y }); return false; }}
        defaultPosition={pos}
      >
        <Paper {...props} />
      </Draggable>
    );
  }

  return (
    <>
      <Button variant="contained" color="primary" onClick={() => setOpenWeaponSelected(true)}>
        {label}
      </Button>
      <Dialog
        onClose={() => setOpenWeaponSelected(false)}
        aria-labelledby="simple-dialog-title"
        open={openWeaponSelected}
        PaperComponent={PaperComponent}
        fullWidth
        maxWidth="md"
      >
        <DialogTitle id="draggable-dialog-title" style={{ cursor: 'move' }}>{label}</DialogTitle>
        <Box alignSelf="stretch" display="flex" flexDirection="row" justifyContent="center">
          <Box>
            <Button variant="contained" onClick={() => handleResetPistols(pistolsSelected, setPistolsSelected)}>
              swap
            </Button>
            <FormControl component="fieldset">
              <FormGroup>
                {pistolsSelected.map((weaponSelected) => (
                  <FormControlLabel
                    key={weaponSelected.weaponID}
                    control={<Checkbox checked={weaponSelected.checked} onChange={(e) => handleChangeSelected(e, pistolsSelected, setPistolsSelected)} />}
                    name={weaponSelected.weaponID}
                    label={weaponSelected.label}
                  />
                ))}
              </FormGroup>
            </FormControl>
          </Box>
          <Box>
            <Button variant="contained" onClick={() => handleResetPistols(smgsSelected, setSmgsSelected)}>
              swap
            </Button>
            <FormControl component="fieldset">
              <FormGroup>
                {smgsSelected.map((weaponSelected) => (
                  <FormControlLabel
                    key={weaponSelected.weaponID}
                    control={<Checkbox checked={weaponSelected.checked} onChange={(e) => handleChangeSelected(e, smgsSelected, setSmgsSelected)} />}
                    name={weaponSelected.weaponID}
                    label={weaponSelected.label}
                  />
                ))}
              </FormGroup>
            </FormControl>
          </Box>
          <Box>
            <Button variant="contained" onClick={() => handleResetPistols(heaviesSelected, setHeaviesSelected)}>
              swap
            </Button>
            <FormControl component="fieldset">
              <FormGroup>
                {heaviesSelected.map((weaponSelected) => (
                  <FormControlLabel
                    key={weaponSelected.weaponID}
                    control={<Checkbox checked={weaponSelected.checked} onChange={(e) => handleChangeSelected(e, heaviesSelected, setHeaviesSelected)} />}
                    name={weaponSelected.weaponID}
                    label={weaponSelected.label}
                  />
                ))}
              </FormGroup>
            </FormControl>
          </Box>
          <Box>
            <Button variant="contained" onClick={() => handleResetPistols(riflesSelected, setRiflesSelected)}>
              swap
            </Button>
            <FormControl component="fieldset">
              <FormGroup>
                {riflesSelected.map((weaponSelected) => (
                  <FormControlLabel
                    key={weaponSelected.weaponID}
                    control={<Checkbox checked={weaponSelected.checked} onChange={(e) => handleChangeSelected(e, riflesSelected, setRiflesSelected)} />}
                    name={weaponSelected.weaponID}
                    label={weaponSelected.label}
                  />
                ))}
              </FormGroup>
            </FormControl>
          </Box>
          <Box>
            <Button variant="contained" onClick={() => handleResetPistols(equipmentsSelected, setEquipmentsSelected)}>
              swap
            </Button>
            <FormControl component="fieldset">
              <FormGroup>
                {equipmentsSelected.map((weaponSelected) => (
                  <FormControlLabel
                    key={weaponSelected.weaponID}
                    control={<Checkbox checked={weaponSelected.checked} onChange={(e) => handleChangeSelected(e, equipmentsSelected, setEquipmentsSelected)} />}
                    name={weaponSelected.weaponID}
                    label={weaponSelected.label}
                  />
                ))}
              </FormGroup>
            </FormControl>
          </Box>
          <Box>
            <Button variant="contained" onClick={() => handleResetPistols(grenadesSelected, setGrenadesSelected)}>
              swap
            </Button>
            <FormControl component="fieldset">
              <FormGroup>
                {grenadesSelected.map((weaponSelected) => (
                  <FormControlLabel
                    key={weaponSelected.weaponID}
                    control={<Checkbox checked={weaponSelected.checked} onChange={(e) => handleChangeSelected(e, grenadesSelected, setGrenadesSelected)} />}
                    name={weaponSelected.weaponID}
                    label={weaponSelected.label}
                  />
                ))}
              </FormGroup>
            </FormControl>
          </Box>
        </Box>
      </Dialog>
    </>
  );
}

export default WeaponsPicker;
