/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';

import {
  DialogTitle, Dialog, Button, Checkbox, FormControl, Box, FormGroup, FormControlLabel, Paper,
} from '@material-ui/core';
import Draggable from 'react-draggable';

import mapsName from '../utils/map';

function MapsPicker({
  maps, setMaps, label,
}) {
  const [openMapsSelected, setOpenMapsSelected] = useState(false);
  const [mapsSelected, setMapsSelected] = useState([]);
  const [pos, setPos] = useState({ x: 0, y: 0 });

  useEffect(() => {
    const MapsSelected = [];
    mapsName.mapsName.forEach((map) => {
      const checked = map.activeMap;
      MapsSelected.push({
        value: map.value,
        label: map.label,
        checked,
      });
    });
    setMapsSelected(MapsSelected);
  }, []);

  function handleChangeSelected(event) {
    const m = [];
    const ms = [];
    mapsSelected.forEach((map, numMap) => {
      m.push(map);
      if (map.value === event.target.name) {
        m[numMap].checked = event.target.checked;
      }
      if (m[numMap].checked) {
        if (!ms.includes(m[numMap].value)) {
          ms.push(m[numMap].value);
        }
      } else if (ms.includes(m[numMap].value)) {
        ms.splice(ms.indexOf(m[numMap].value), 1);
      }
    });

    setMapsSelected(m);
    setMaps(ms);
  }

  function onlyOfficialMapPool() {
    const m = [];
    const ms = [];

    mapsName.mapsName.forEach((map, numMap) => {
      m.push(map);
      if (m[numMap].officialMapPol) {
        ms.push(map.value);
        m[numMap].checked = true;
      } else {
        m[numMap].checked = false;
      }
    });

    setMapsSelected(m);
    setMaps(ms);
  }

  function onlyActiveMaps() {
    const m = [];
    const ms = [];

    mapsName.mapsName.forEach((map, numMap) => {
      m.push(map);
      if (m[numMap].activeMap) {
        ms.push(map.value);
        m[numMap].checked = true;
      } else {
        m[numMap].checked = false;
      }
    });

    setMapsSelected(m);
    setMaps(ms);
  }

  function PaperComponent(props) {
    return (
      <Draggable
        handle="#draggable-dialog-title"
        cancel={'[class*="MuiDialogContent-root"]'}
        onStop={(e, data) => { setPos({ x: data.x, y: data.y }); return false; }}
        defaultPosition={pos}
      >
        <Paper {...props} />
      </Draggable>
    );
  }

  return (
    <>
      <Button variant="contained" color="primary" onClick={() => setOpenMapsSelected(true)}>
        {label}
      </Button>
      <Dialog
        onClose={() => setOpenMapsSelected(false)}
        aria-labelledby="simple-dialog-title"
        open={openMapsSelected}
        PaperComponent={PaperComponent}
        fullWidth
        maxWidth="md"
      >
        <DialogTitle id="draggable-dialog-title" style={{ cursor: 'move' }}>{label}</DialogTitle>
        <Box alignSelf="stretch" display="flex" flexDirection="row" justifyContent="center">
          <Box>
            <Button variant="contained" onClick={() => onlyOfficialMapPool()}>
              Only official map pool
            </Button>
            <FormControl component="fieldset">
              <FormGroup>
                {mapsSelected.slice(0, 13).map((map) => (
                  <FormControlLabel
                    key={map.value}
                    control={<Checkbox checked={map.checked} onChange={(e) => handleChangeSelected(e)} />}
                    name={map.value}
                    label={map.label}
                  />
                ))}
              </FormGroup>
            </FormControl>
          </Box>
          <Box>
            <Button variant="contained" onClick={() => onlyActiveMaps()}>
              Only active maps
            </Button>
            <FormControl component="fieldset">
              <FormGroup>
                {mapsSelected.slice(13, 26).map((map) => (
                  <FormControlLabel
                    key={map.value}
                    control={<Checkbox checked={map.checked} onChange={(e) => handleChangeSelected(e)} />}
                    name={map.value}
                    label={map.label}
                  />
                ))}
              </FormGroup>
            </FormControl>
          </Box>
        </Box>
      </Dialog>
    </>
  );
}

export default MapsPicker;
