/* eslint-disable prefer-destructuring */
import React, { useEffect, useState } from 'react';

import {
  Container,
  MenuItem,
  FormControl,
  Select,
} from '@material-ui/core';
import { Line } from 'react-chartjs-2';
import moment from 'moment';

function DM(values) {
  const { demos } = values;

  const [displayedStatsDM, setDisplayedStatsDM] = useState();
  const [generalsStatsDM, setGeneralsStatsDM] = useState([]);

  const [weaponsStatsDM, setWeaponsStatsDM] = useState();

  const colors = [
    ['#db3421', '#d95e50'],
    ['#4eeb1a', '#7be359'],
    ['#2549d9', '#4964d1'],
    ['#f5f10c', '#d4d13b'],
    ['#995928', '#7a5436'],
    ['#eb9b23', '#e3af62'],
    ['#22e3cc', '#64d9cb'],
    ['#691ee3', '#7f50cc'],
    ['#cf1ed9', '#cb60d1'],
    ['#0ee898', '#47d6a2'],
  ];

  const [weaponClassSelectedDM, setWeaponClassSelectedDM] = useState('0');
  const weaponsTypes = [
    { label: 'All', value: '0' },
    { label: 'Pistols', value: '1' },
    { label: 'Smg', value: '2' },
    { label: 'Heavies', value: '3' },
    { label: 'Rifles', value: '4' },
    { label: 'Snipers', value: '7' },
  ];

  const [statsTypesSelectedDM, setStatsTypesSelectedDM] = useState(0);
  const statsTypes = [
    { label: 'Bullet Accuracy', value: 0 },
    { label: 'First Bullet Accuracy', value: 1 },
    { label: 'HS Percentage', value: 2 },
    { label: 'First Bullet HS', value: 3 },
  ];

  useEffect(() => {
    if (demos === undefined) {
      return;
    }
    const weaponsDM = [];

    Object.keys(demos.weaponTypeStats).forEach((weaponType) => {
      const w = demos.weaponTypeStats[weaponType];
      if (w.bulletAccuracy.data !== null) {
        weaponsDM.push({
          statsType: 0,
          weaponName: w.weaponName,
          weaponType: w.weaponType,
          weaponClass: w.weaponClass,
          data: w.bulletAccuracy.data.sort((a, b) => (a.x > b.x ? 1 : -1)),
        });
      }
      if (w.firstBulletAccuracy.data !== null) {
        weaponsDM.push({
          statsType: 1,
          weaponName: w.weaponName,
          weaponType: w.weaponType,
          weaponClass: w.weaponClass,
          data: w.firstBulletAccuracy.data.sort((a, b) => (a.x > b.x ? 1 : -1)),
        });
      }
      if (w.hSPercentage.data !== null) {
        weaponsDM.push({
          statsType: 2,
          weaponName: w.weaponName,
          weaponType: w.weaponType,
          weaponClass: w.weaponClass,
          data: w.hSPercentage.data.sort((a, b) => (a.x > b.x ? 1 : -1)),
        });
      }
      if (w.firstBulletHS.data !== null) {
        weaponsDM.push({
          statsType: 3,
          weaponName: w.weaponName,
          weaponType: w.weaponType,
          weaponClass: w.weaponClass,
          data: w.firstBulletHS.data.sort((a, b) => (a.x > b.x ? 1 : -1)),
        });
      }
    });

    const generalDM = [];
    const BADM = demos.weaponStatsPerDay.bulletAccuracy.data;
    if (BADM !== null) {
      generalDM.push({
        label: 'Bullet Accuracy',
        data: BADM.sort((a, b) => (a.x > b.x ? 1 : -1)),
        backgroundColor: '#00000000',
        borderColor: '#eb705e',
        borderWidth: 2,
        pointBackgroundColor: '#eb4026',
        pointHoverBorderColor: '#eb705e',
        pointHoverBorderWidth: 1,
        pointHoverRadius: 4,
        pointRadius: 4,
        yAxisID: 'y-axis-linear',
      });
    }
    const FBHSDM = demos.weaponStatsPerDay.firstBulletHS.data;
    if (FBHSDM !== null) {
      generalDM.push({
        label: 'First Bullet HS',
        data: FBHSDM.sort((a, b) => (a.x > b.x ? 1 : -1)),
        backgroundColor: '#00000000',
        borderColor: '#4ab55e',
        borderWidth: 2,
        pointBackgroundColor: '#27c243',
        pointHoverBorderColor: '#4ab55e',
        pointHoverBorderWidth: 1,
        pointHoverRadius: 4,
        pointRadius: 4,
        yAxisID: 'y-axis-linear',
      });
    }
    const FBADM = demos.weaponStatsPerDay.firstBulletAccuracy.data;
    if (FBADM !== null) {
      generalDM.push({
        label: 'First Bullet Accuracy',
        data: FBADM.sort((a, b) => (a.x > b.x ? 1 : -1)),
        backgroundColor: '#00000000',
        borderColor: '#daae45',
        borderWidth: 2,
        pointBackgroundColor: '#f9c74f',
        pointHoverBorderColor: '#daae45',
        pointHoverBorderWidth: 2,
        pointHoverRadius: 4,
        pointRadius: 4,
        yAxisID: 'y-axis-linear',
      });
    }
    const HSPDM = demos.weaponStatsPerDay.hSPercentage.data;
    if (HSPDM !== null) {
      generalDM.push({
        label: 'HS Percentage',
        data: HSPDM.sort((a, b) => (a.x > b.x ? 1 : -1)),
        backgroundColor: '#00000000',
        borderColor: '#348cc9',
        borderWidth: 2,
        pointBackgroundColor: '#4b95c9',
        pointHoverBorderColor: '#348cc9',
        pointHoverBorderWidth: 1,
        pointHoverRadius: 4,
        pointRadius: 4,
        yAxisID: 'y-axis-linear',
      });
    }
    const NBRFBDM = demos.weaponStatsPerDay.nbrBulletsFired.data;
    if (NBRFBDM !== null) {
      generalDM.push({
        label: 'Nbr Bullets Fired',
        data: NBRFBDM.sort((a, b) => (a.x > b.x ? 1 : -1)),
        backgroundColor: '#00000000',
        borderColor: '#000000',
        borderWidth: 2,
        pointBackgroundColor: '#000000',
        pointHoverBorderColor: '#000000',
        pointHoverBorderWidth: 1,
        pointHoverRadius: 4,
        pointRadius: 4,
        yAxisID: 'y-axis-log',
      });
    }
    const AVADM = demos.weaponStatsPerDay.averageVelocityAttacker.data;
    if (AVADM !== null) {
      generalDM.push({
        label: 'Average velocity',
        data: AVADM.sort((a, b) => (a.x > b.x ? 1 : -1)),
        backgroundColor: '#00000000',
        borderColor: '#AAAAAA',
        borderWidth: 2,
        pointBackgroundColor: '#AAAAAA',
        pointHoverBorderColor: '#AAAAAA',
        pointHoverBorderWidth: 1,
        pointHoverRadius: 4,
        pointRadius: 4,
        yAxisID: 'y-axis-velocity',
      });
    }

    setWeaponsStatsDM(weaponsDM);
    setGeneralsStatsDM(generalDM);
    setDisplayedStatsDM(generalDM);
  }, [demos]);

  function weaponDisplaySelectorDM(weaponClass, statsType) {
    setWeaponClassSelectedDM(weaponClass);
    setStatsTypesSelectedDM(statsType);
    if (weaponClass === '0') {
      setDisplayedStatsDM(generalsStatsDM);
      return;
    }

    const wDisplayed = [];

    weaponsStatsDM.forEach((weapon) => {
      if (weapon.statsType === statsType && weapon.weaponClass.toString() === weaponClass) {
        wDisplayed.push({
          label: weapon.weaponName,
          data: [...weapon.data],
          backgroundColor: '#00000000',
          borderColor: colors[wDisplayed.length][1],
          borderWidth: 2,
          pointBackgroundColor: colors[wDisplayed.length][0],
          pointHoverBorderColor: colors[wDisplayed.length][1],
          pointHoverBorderWidth: 1,
          pointHoverRadius: 4,
          pointRadius: 4,
        });
      }
    });

    setDisplayedStatsDM(wDisplayed);
  }

  if (displayedStatsDM === undefined) {
    return false;
  }

  return (
    <Container maxWidth={false}>
      <p>
        Want more data ? Check our CSGO serveur ! Type in the console &rsquo;connect 91.121.97.27&rsquo; and play some games !
        <br />
        Be aware that, to avoid displaying irrelevant data, we are displaying the moving average over 7 days.
      </p>
      <br />
      <FormControl>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={weaponClassSelectedDM}
          onChange={(event) => weaponDisplaySelectorDM(event.target.value, statsTypesSelectedDM)}
        >
          {weaponsTypes.map((wTypes) => (
            <MenuItem key={wTypes.label} value={wTypes.value}>{wTypes.label}</MenuItem>
          ))}
        </Select>
      </FormControl>

      {weaponClassSelectedDM !== '0' ? (
        <FormControl>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={statsTypesSelectedDM}
            onChange={(event) => weaponDisplaySelectorDM(weaponClassSelectedDM, event.target.value)}
          >
            {statsTypes.map((sTypes) => (
              <MenuItem key={sTypes.label} value={sTypes.value}>{sTypes.label}</MenuItem>
            ))}
          </Select>
        </FormControl>
      ) : false}

      <div style={{
        position: 'relative', margin: 'auto', width: '80vw', height: '30vh',
      }}
      >
        <Line
          data={{
            datasets: displayedStatsDM,
          }}
          options={{
            height: 50,
            maintainAspectRatio: false,
            scales: {
              xAxes: [{
                type: 'time',
                time: {
                  displayFormats: {
                    day: 'YYYY MMM DD',
                  },
                  // parse unix timestamp seconds
                  parser: (timestamp) => moment(timestamp, 'X'),
                  unit: 'day',
                  unitStepSize: 1,
                },
              }],
              yAxes: [{
                ticks: {
                  beginAtZero: true,
                  min: 0,
                  max: 100,
                },
                type: 'linear',
                position: 'left',
                id: 'y-axis-linear',
              }, {
                ticks: {
                  beginAtZero: true,
                },
                type: 'logarithmic',
                position: 'right',
                id: 'y-axis-log',
              }, {
                ticks: {
                  beginAtZero: true,
                  min: 0,
                  max: 250,
                },
                type: 'linear',
                // position: 'right',
                id: 'y-axis-velocity',
              }],
            },
            title: {
              display: true,
              text: 'Progression',
              fontSize: 20,
            },
            legend: {
              display: true,
              position: 'right',
            },
            tooltips: {
              callbacks: {
                label: (tooltipItem) => `${tooltipItem.xLabel} | ${Math.round(tooltipItem.yLabel * 1000) / 1000}`,
              },
            },
          }}
        />
      </div>
    </Container>
  );
}

export default DM;
