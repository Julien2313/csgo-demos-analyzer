/* eslint-disable prefer-destructuring */
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

import {
  Container,
  MenuItem,
  FormControl,
  Select,
  Box,
  ButtonGroup,
  Button,
} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import {
  Clear as ClearIcon,
} from '@material-ui/icons';

import Skeleton from '@material-ui/lab/Skeleton';

import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

import { Line } from 'react-chartjs-2';
import { toast } from 'react-toastify';
import moment from 'moment';
import mapsName from '../utils/map';

import MapsPicker from '../dialog/mapsPicker';
import apiDemo from '../../api/demo';

function MM() {
  const history = useHistory();
  const [selectedDateStart, setSelectedDateStart] = useState(null);
  const [selectedDateEnd, setSelectedDateEnd] = useState(null);
  const [sideDisplay, setSideDisplay] = useState(0);
  const [mergedBy, setMergedBy] = useState(0);

  const [displayedStatsMM, setDisplayedStatsMM] = useState();

  const [generalsStatsMM, setGeneralsStatsMM] = useState({});
  const [weaponsStatsMM, setWeaponsStatsMM] = useState();

  const [generalsStatsMMPerWeek, setGeneralsStatsMMPerWeek] = useState({});
  const [weaponsStatsMMPerWeek, setWeaponsStatsMMPerWeek] = useState();

  const [generalsStatsMMPerMonth, setGeneralsStatsMMPerMonth] = useState({});
  const [weaponsStatsMMPerMonth, setWeaponsStatsMMPerMonth] = useState();

  const [demos, setDemos] = useState();
  const [maps, setMaps] = useState([]);

  const colors = [
    ['#db3421', '#d95e50'],
    ['#4eeb1a', '#7be359'],
    ['#2549d9', '#4964d1'],
    ['#f5f10c', '#d4d13b'],
    ['#995928', '#7a5436'],
    ['#eb9b23', '#e3af62'],
    ['#22e3cc', '#64d9cb'],
    ['#691ee3', '#7f50cc'],
    ['#cf1ed9', '#cb60d1'],
    ['#0ee898', '#47d6a2'],
  ];

  const [weaponClassSelectedMM, setWeaponClassSelectedMM] = useState('0');
  const weaponsTypes = [
    { label: 'All', value: '0' },
    { label: 'Pistols', value: '1' },
    { label: 'Smg', value: '2' },
    { label: 'Heavies', value: '3' },
    { label: 'Rifles', value: '4' },
    { label: 'Snipers', value: '7' },
  ];

  const [statsTypesSelectedMM, setStatsTypesSelectedMM] = useState(0);
  const statsTypes = [
    { label: 'Bullet Accuracy', value: 0 },
    { label: 'First Bullet Accuracy', value: 1 },
    { label: 'HS Percentage', value: 2 },
    { label: 'First Bullet HS', value: 3 },
  ];

  useEffect(() => {
    const mapsFilters = [];
    mapsName.mapsName.forEach((map) => {
      if (map.activeMap) {
        mapsFilters.push(map.value);
      }
    });
    setMaps(mapsFilters);
  }, []);

  useEffect(() => {
    const getAllCompetitivesDemos = async () => {
      if (maps.length === 0) {
        return;
      }
      try {
        const payload = {
          maps,
          selectedDateStart,
          selectedDateEnd,
        };
        const response = await apiDemo.getAllCompetitivesDemos(payload);
        setDemos(response.data.data);
      } catch (err) {
        if (err.response === undefined) {
          toast.error('An error occurred', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          return;
        }
        if (err.response.status === 403 && err.response.data.title === 'ErrBadTokenConnection') {
          toast.info('You have been disconnected', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          history.push({
            pathname: '/',
          });
          return;
        }

        if (err.response.status === 429) {
          toast.error('Too many request !', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          return;
        }

        toast.error('An error occurred', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    };

    getAllCompetitivesDemos();
  }, [maps, selectedDateStart, selectedDateEnd]);

  useEffect(() => {
    if (demos === undefined) {
      return;
    }

    const weaponsMM = {};
    const weaponsMMPerSide = {};
    const generalMM = {};
    demos.forEach((timeFrame) => {
      const index = timeFrame.index;
      weaponsMM[index] = [];
      Object.keys(timeFrame.weaponTypeStats).forEach((weaponType) => {
        const w = timeFrame.weaponTypeStats[weaponType];
        if (w.bulletAccuracy.data !== null) {
          weaponsMM[index].push({
            statsType: 0,
            weaponName: w.weaponName,
            weaponType: w.weaponType,
            weaponClass: w.weaponClass,
            data: w.bulletAccuracy.data.sort((a, b) => (a.x > b.x ? 1 : -1)),
          });
        }
        if (w.firstBulletAccuracy.data !== null) {
          weaponsMM[index].push({
            statsType: 1,
            weaponName: w.weaponName,
            weaponType: w.weaponType,
            weaponClass: w.weaponClass,
            data: w.firstBulletAccuracy.data.sort((a, b) => (a.x > b.x ? 1 : -1)),
          });
        }
        if (w.hSPercentage.data !== null) {
          weaponsMM[index].push({
            statsType: 2,
            weaponName: w.weaponName,
            weaponType: w.weaponType,
            weaponClass: w.weaponClass,
            data: w.hSPercentage.data.sort((a, b) => (a.x > b.x ? 1 : -1)),
          });
        }
        if (w.firstBulletHS.data !== null) {
          weaponsMM[index].push({
            statsType: 3,
            weaponName: w.weaponName,
            weaponType: w.weaponType,
            weaponClass: w.weaponClass,
            data: w.firstBulletHS.data.sort((a, b) => (a.x > b.x ? 1 : -1)),
          });
        }
      });

      generalMM[index] = [];
      const BAMM = timeFrame.weaponStats.bulletAccuracy.data;
      if (BAMM !== null) {
        generalMM[index].push({
          label: 'Bullet Accuracy',
          data: BAMM.sort((a, b) => (a.x > b.x ? 1 : -1)),
          backgroundColor: '#00000000',
          borderColor: '#eb705e',
          borderWidth: 2,
          pointBackgroundColor: '#eb4026',
          pointHoverBorderColor: '#eb705e',
          pointHoverBorderWidth: 1,
          pointHoverRadius: 4,
          pointRadius: 4,
          yAxisID: 'y-axis-linear',
        });
      }

      const FBHSMM = timeFrame.weaponStats.firstBulletHS.data;
      if (FBHSMM !== null) {
        generalMM[index].push({
          label: 'First Bullet HS',
          data: FBHSMM.sort((a, b) => (a.x > b.x ? 1 : -1)),
          backgroundColor: '#00000000',
          borderColor: '#4ab55e',
          borderWidth: 2,
          pointBackgroundColor: '#27c243',
          pointHoverBorderColor: '#4ab55e',
          pointHoverBorderWidth: 1,
          pointHoverRadius: 4,
          pointRadius: 4,
          yAxisID: 'y-axis-linear',
        });
      }

      const FBAMM = timeFrame.weaponStats.firstBulletAccuracy.data;
      if (FBAMM !== null) {
        generalMM[index].push({
          label: 'First Bullet Accuracy',
          data: FBAMM.sort((a, b) => (a.x > b.x ? 1 : -1)),
          backgroundColor: '#00000000',
          borderColor: '#daae45',
          borderWidth: 2,
          pointBackgroundColor: '#f9c74f',
          pointHoverBorderColor: '#daae45',
          pointHoverBorderWidth: 2,
          pointHoverRadius: 4,
          pointRadius: 4,
          yAxisID: 'y-axis-linear',
        });
      }

      const HSPMM = timeFrame.weaponStats.hSPercentage.data;
      if (HSPMM !== null) {
        generalMM[index].push({
          label: 'HS Percentage',
          data: HSPMM.sort((a, b) => (a.x > b.x ? 1 : -1)),
          backgroundColor: '#00000000',
          borderColor: '#348cc9',
          borderWidth: 2,
          pointBackgroundColor: '#4b95c9',
          pointHoverBorderColor: '#348cc9',
          pointHoverBorderWidth: 1,
          pointHoverRadius: 4,
          pointRadius: 4,
          yAxisID: 'y-axis-linear',
        });
      }

      const AVAMM = timeFrame.weaponStats.averageVelocityAttacker.data;
      if (AVAMM !== null) {
        generalMM[index].push({
          label: 'Average velocity',
          data: AVAMM.sort((a, b) => (a.x > b.x ? 1 : -1)),
          backgroundColor: '#00000000',
          borderColor: '#AAAAAA',
          borderWidth: 2,
          pointBackgroundColor: '#AAAAAA',
          pointHoverBorderColor: '#AAAAAA',
          pointHoverBorderWidth: 1,
          pointHoverRadius: 4,
          pointRadius: 4,
          yAxisID: 'y-axis-velocity',
        });
      }

      weaponsMMPerSide[index] = {};
      Object.keys(timeFrame.weaponstatsPerSide).forEach((weaponStatsPerSide) => {
        weaponsMMPerSide[index][weaponStatsPerSide] = [];
        if (timeFrame.weaponstatsPerSide[weaponStatsPerSide] !== undefined) {
          const BAMMCT = timeFrame.weaponstatsPerSide[weaponStatsPerSide].bulletAccuracy.data;
          if (BAMMCT !== null) {
            weaponsMMPerSide[index][weaponStatsPerSide].push({
              label: 'Bullet Accuracy',
              data: BAMMCT.sort((a, b) => (a.x > b.x ? 1 : -1)),
              backgroundColor: '#00000000',
              borderColor: '#eb705e',
              borderWidth: 2,
              pointBackgroundColor: '#eb4026',
              pointHoverBorderColor: '#eb705e',
              pointHoverBorderWidth: 1,
              pointHoverRadius: 4,
              pointRadius: 4,
              yAxisID: 'y-axis-linear',
            });
          }
          const FBHSMMCT = timeFrame.weaponstatsPerSide[weaponStatsPerSide].firstBulletHS.data;
          if (FBHSMMCT !== null) {
            weaponsMMPerSide[index][weaponStatsPerSide].push({
              label: 'First Bullet HS',
              data: FBHSMMCT.sort((a, b) => (a.x > b.x ? 1 : -1)),
              backgroundColor: '#00000000',
              borderColor: '#4ab55e',
              borderWidth: 2,
              pointBackgroundColor: '#27c243',
              pointHoverBorderColor: '#4ab55e',
              pointHoverBorderWidth: 1,
              pointHoverRadius: 4,
              pointRadius: 4,
              yAxisID: 'y-axis-linear',
            });
          }
          const FBAMMCT = timeFrame.weaponstatsPerSide[weaponStatsPerSide].firstBulletAccuracy.data;
          if (FBAMMCT !== null) {
            weaponsMMPerSide[index][weaponStatsPerSide].push({
              label: 'First Bullet Accuracy',
              data: FBAMMCT.sort((a, b) => (a.x > b.x ? 1 : -1)),
              backgroundColor: '#00000000',
              borderColor: '#daae45',
              borderWidth: 2,
              pointBackgroundColor: '#f9c74f',
              pointHoverBorderColor: '#daae45',
              pointHoverBorderWidth: 2,
              pointHoverRadius: 4,
              pointRadius: 4,
              yAxisID: 'y-axis-linear',
            });
          }
          const HSPMMCT = timeFrame.weaponstatsPerSide[weaponStatsPerSide].hSPercentage.data;
          if (HSPMMCT !== null) {
            weaponsMMPerSide[index][weaponStatsPerSide].push({
              label: 'HS Percentage',
              data: HSPMMCT.sort((a, b) => (a.x > b.x ? 1 : -1)),
              backgroundColor: '#00000000',
              borderColor: '#348cc9',
              borderWidth: 2,
              pointBackgroundColor: '#4b95c9',
              pointHoverBorderColor: '#348cc9',
              pointHoverBorderWidth: 1,
              pointHoverRadius: 4,
              pointRadius: 4,
              yAxisID: 'y-axis-linear',
            });
          }

          const AVAMMCT = timeFrame.weaponstatsPerSide[weaponStatsPerSide].averageVelocityAttacker.data;
          if (AVAMMCT !== null) {
            weaponsMMPerSide[index][weaponStatsPerSide].push({
              label: 'Average velocity',
              data: AVAMMCT.sort((a, b) => (a.x > b.x ? 1 : -1)),
              backgroundColor: '#00000000',
              borderColor: '#AAAAAA',
              borderWidth: 2,
              pointBackgroundColor: '#AAAAAA',
              pointHoverBorderColor: '#AAAAAA',
              pointHoverBorderWidth: 1,
              pointHoverRadius: 4,
              pointRadius: 4,
              yAxisID: 'y-axis-velocity',
            });
          }
        }
      });
    });

    setWeaponsStatsMMPerMonth(weaponsMM[2]);
    setGeneralsStatsMMPerMonth({
      global: generalMM[2],
      CT: weaponsMMPerSide[2][3],
      T: weaponsMMPerSide[2][2],
    });

    setWeaponsStatsMMPerWeek(weaponsMM[1]);
    setGeneralsStatsMMPerWeek({
      global: generalMM[1],
      CT: weaponsMMPerSide[1][3],
      T: weaponsMMPerSide[1][2],
    });

    setWeaponsStatsMM(weaponsMM[0]);
    setGeneralsStatsMM({
      global: generalMM[0],
      CT: weaponsMMPerSide[0][3],
      T: weaponsMMPerSide[0][2],
    });

    setDisplayedStatsMM(generalMM[0]);
  }, [demos]);

  function changeDisplayedMM(weaponClass, statsType, side, periodMergeBy) {
    if (weaponClass !== '0') {
      weaponDisplaySelectorMM(weaponClass, statsType, side, periodMergeBy);
      return;
    }

    if (side === 0) {
      if (periodMergeBy === 0) {
        setDisplayedStatsMM(generalsStatsMM.global);
      } else if (periodMergeBy === 1) {
        setDisplayedStatsMM(generalsStatsMMPerWeek.global);
      } else {
        setDisplayedStatsMM(generalsStatsMMPerMonth.global);
      }
    } else if (side === 1) {
      if (periodMergeBy === 0) {
        setDisplayedStatsMM(generalsStatsMM.CT);
      } else if (periodMergeBy === 1) {
        setDisplayedStatsMM(generalsStatsMMPerWeek.CT);
      } else {
        setDisplayedStatsMM(generalsStatsMMPerMonth.CT);
      }
    } else if (periodMergeBy === 0) {
      setDisplayedStatsMM(generalsStatsMM.T);
    } else if (periodMergeBy === 1) {
      setDisplayedStatsMM(generalsStatsMMPerWeek.T);
    } else {
      setDisplayedStatsMM(generalsStatsMMPerMonth.T);
    }
  }

  function weaponDisplaySelectorMM(weaponClass, statsType, sideToDisplay, periodMergeBy) {
    setWeaponClassSelectedMM(weaponClass);
    setStatsTypesSelectedMM(statsType);
    if (weaponClass === '0') {
      changeDisplayedMM(weaponClass, statsType, sideToDisplay, periodMergeBy);
      return;
    }

    const wDisplayed = [];

    if (periodMergeBy === 0) {
      weaponsStatsMM.forEach((weapon) => {
        if (weapon.statsType === statsType && weapon.weaponClass.toString() === weaponClass) {
          wDisplayed.push({
            label: weapon.weaponName,
            data: [...weapon.data],
            backgroundColor: '#00000000',
            borderColor: colors[wDisplayed.length][1],
            borderWidth: 2,
            pointBackgroundColor: colors[wDisplayed.length][0],
            pointHoverBorderColor: colors[wDisplayed.length][1],
            pointHoverBorderWidth: 1,
            pointHoverRadius: 4,
            pointRadius: 4,
          });
        }
      });
    } else if (periodMergeBy === 1) {
      weaponsStatsMMPerWeek.forEach((weapon) => {
        if (weapon.statsType === statsType && weapon.weaponClass.toString() === weaponClass) {
          wDisplayed.push({
            label: weapon.weaponName,
            data: [...weapon.data],
            backgroundColor: '#00000000',
            borderColor: colors[wDisplayed.length][1],
            borderWidth: 2,
            pointBackgroundColor: colors[wDisplayed.length][0],
            pointHoverBorderColor: colors[wDisplayed.length][1],
            pointHoverBorderWidth: 1,
            pointHoverRadius: 4,
            pointRadius: 4,
          });
        }
      });
    } else {
      weaponsStatsMMPerMonth.forEach((weapon) => {
        if (weapon.statsType === statsType && weapon.weaponClass.toString() === weaponClass) {
          wDisplayed.push({
            label: weapon.weaponName,
            data: [...weapon.data],
            backgroundColor: '#00000000',
            borderColor: colors[wDisplayed.length][1],
            borderWidth: 2,
            pointBackgroundColor: colors[wDisplayed.length][0],
            pointHoverBorderColor: colors[wDisplayed.length][1],
            pointHoverBorderWidth: 1,
            pointHoverRadius: 4,
            pointRadius: 4,
          });
        }
      });
    }

    setDisplayedStatsMM(wDisplayed);
  }

  if (displayedStatsMM === undefined) {
    return (
      <Container maxWidth={false}>
        <Box
          display="flex"
          alignItems="flex-end"
        >
          <Button variant="contained" color="primary">
            Pick the maps
          </Button>
          <ButtonGroup variant="contained" color="primary">
            <Button>
              Per Days
            </Button>
            <Button>
              Weeks
            </Button>
            <Button>
              Months
            </Button>
          </ButtonGroup>
          <ButtonGroup variant="contained" color="primary">
            <Button style={{ backgroundColor: '#000000' }}>
              Global
            </Button>
            <Button style={{ backgroundColor: '#4374b0' }}>
              CT
            </Button>
            <Button style={{ backgroundColor: '#C7A247' }}>
              T
            </Button>
          </ButtonGroup>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardDatePicker
              disableToolbar
              variant="inline"
              label="From"
              margin="normal"
              KeyboardButtonProps={{
                'aria-label': 'change date',
              }}
            />
            <IconButton
              edge="end"
              size="small"
            >
              <ClearIcon />
            </IconButton>
            <KeyboardDatePicker
              disableToolbar
              variant="inline"
              label="To"
              margin="normal"
              KeyboardButtonProps={{
                'aria-label': 'change date',
              }}
            />
            <IconButton
              edge="end"
              size="small"
            >
              <ClearIcon />
            </IconButton>
          </MuiPickersUtilsProvider>
          <FormControl>
            <Select
              value={weaponClassSelectedMM}
            >
              {weaponsTypes.map((wTypes) => (
                <MenuItem key={wTypes.label} value={wTypes.value}>{wTypes.label}</MenuItem>
              ))}
            </Select>
          </FormControl>
        </Box>

        <div style={{
          position: 'relative', margin: 'auto', width: '80vw', height: '30vh',
        }}
        >
          <Skeleton variant="rect" height={400} />
        </div>
      </Container>
    );
  }

  return (
    <Container maxWidth={false}>
      <Box
        display="flex"
        alignItems="flex-end"
      >
        <MapsPicker
          maps={maps}
          setMaps={setMaps}
          label="Pick the maps"
        />
        <ButtonGroup variant="contained" color="primary">
          <Button
            onClick={() => {
              setMergedBy(0);
              changeDisplayedMM(weaponClassSelectedMM, statsTypesSelectedMM, sideDisplay, 0);
            }}
          >
            Per Days
          </Button>
          <Button
            onClick={() => {
              setMergedBy(1);
              changeDisplayedMM(weaponClassSelectedMM, statsTypesSelectedMM, sideDisplay, 1);
            }}
          >
            Weeks
          </Button>
          <Button
            onClick={() => {
              setMergedBy(2);
              changeDisplayedMM(weaponClassSelectedMM, statsTypesSelectedMM, sideDisplay, 2);
            }}
          >
            Months
          </Button>
        </ButtonGroup>
        <ButtonGroup variant="contained" color="primary">
          <Button
            onClick={() => {
              setSideDisplay(0);
              weaponDisplaySelectorMM('0', 0, 0, mergedBy);
            }}
            style={{
              backgroundColor: '#000000',
            }}
          >
            Global
          </Button>
          <Button
            onClick={() => {
              setSideDisplay(1);
              weaponDisplaySelectorMM('0', 0, 1, mergedBy);
            }}
            style={{
              backgroundColor: '#4374b0',
            }}
          >
            CT
          </Button>
          <Button
            onClick={() => {
              setSideDisplay(2);
              weaponDisplaySelectorMM('0', 0, 2, mergedBy);
            }}
            style={{
              backgroundColor: '#C7A247',
            }}
          >
            T
          </Button>
        </ButtonGroup>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardDatePicker
            disableToolbar
            variant="inline"
            format="MM/dd/yyyy"
            margin="normal"
            id="date-picker-inline"
            label="From"
            value={selectedDateStart}
            onChange={(date) => { setSelectedDateStart(date.setHours(0, 0, 0, 0)); }}
            KeyboardButtonProps={{
              'aria-label': 'change date',
            }}
          />
          <IconButton
            edge="end"
            size="small"
            disabled={!selectedDateStart}
            onClick={() => { setSelectedDateStart(null); }}
          >
            <ClearIcon />
          </IconButton>
          <KeyboardDatePicker
            disableToolbar
            variant="inline"
            format="MM/dd/yyyy"
            margin="normal"
            id="date-picker-inline"
            label="To"
            value={selectedDateEnd}
            onChange={(date) => { setSelectedDateEnd(date.setHours(0, 0, 0, 0)); }}
            KeyboardButtonProps={{
              'aria-label': 'change date',
            }}
          />
          <IconButton
          // style={{ padding: 0 }}
            edge="end"
            size="small"
            disabled={!selectedDateEnd}
            onClick={() => { setSelectedDateEnd(null); }}
          >
            <ClearIcon />
          </IconButton>
        </MuiPickersUtilsProvider>
        <FormControl>
          <Select
            value={weaponClassSelectedMM}
            onChange={(event) => weaponDisplaySelectorMM(event.target.value, statsTypesSelectedMM, sideDisplay, mergedBy)}
          >
            {weaponsTypes.map((wTypes) => (
              <MenuItem key={wTypes.label} value={wTypes.value}>{wTypes.label}</MenuItem>
            ))}
          </Select>
        </FormControl>

        {weaponClassSelectedMM !== '0' ? (
          <FormControl>
            <Select
              value={statsTypesSelectedMM}
              onChange={(event) => weaponDisplaySelectorMM(weaponClassSelectedMM, event.target.value, sideDisplay, mergedBy)}
            >
              {statsTypes.map((sTypes) => (
                <MenuItem key={sTypes.label} value={sTypes.value}>{sTypes.label}</MenuItem>
              ))}
            </Select>
          </FormControl>
        ) : false}
      </Box>

      <div style={{
        position: 'relative', margin: 'auto', width: '80vw', height: '30vh',
      }}
      >
        <Line
          data={{
            datasets: JSON.parse(JSON.stringify(displayedStatsMM)),
          }}
          options={{
            maintainAspectRatio: false,
            scales: {
              xAxes: [{
                type: 'time',
                time: {
                  displayFormats: {
                    day: 'YYYY MMM DD',
                  },
                  // parse unix timestamp seconds
                  parser: (timestamp) => moment(timestamp, 'X'),
                  unit: 'day',
                  unitStepSize: 1,
                },
              }],
              yAxes: [{
                ticks: {
                  beginAtZero: true,
                  min: 0,
                  max: 100,
                },
                type: 'linear',
                position: 'left',
                id: 'y-axis-linear',
              }, {
                ticks: {
                  beginAtZero: true,
                  min: 0,
                  max: 100,
                },
                type: 'linear',
                position: 'right',
                id: 'y-axis-linear2',
              }, {
                ticks: {
                  beginAtZero: true,
                  min: 0,
                  max: 250,
                },
                display: false,
                type: 'linear',
                id: 'y-axis-velocity',
              }],
            },
            title: {
              display: true,
              text: 'Progression',
              fontSize: 20,
            },
            legend: {
              display: true,
              position: 'right',
            },
            tooltips: {
              callbacks: {
                label: (tooltipItem) => `${tooltipItem.xLabel} | ${Math.round(tooltipItem.yLabel * 1000) / 1000}`,
              },
            },
          }}
        />
      </div>
    </Container>
  );
}

export default MM;
