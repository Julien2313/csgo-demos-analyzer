import React from 'react';
import {
  Box,
  Typography,
} from '@material-ui/core';
import { HeatMap } from '@nivo/heatmap';

function HeatMapDuels(values) {
  const { data } = values;
  const { keys } = values;

  return (
    <Box display="flex" flexDirection="column">
      <Box display="flex" justifyContent="center">
        <Typography variant="h4">
          Duels
        </Typography>
      </Box>
      <HeatMap
        width={500}
        height={400}
        data={data}
        indexBy="matchup"
        margin={{
          top: 20,
        }}
        keys={keys}
        forceSquare
        axisTop={{
          orient: 'top', tickSize: 5, tickPadding: 5,
        }}
        axisRight={null}
        axisBottom={null}
        axisLeft={{
          orient: 'left',
          tickSize: 5,
          tickPadding: 5,
          tickRotation: 0,
        }}
        animate={false}
        hoverTarget="cell"
        maxValue={1}
        minValue={0}
        colors="RdYlGn"
        tooltip={({
          xKey, yKey, value,
        }) => (
          <>
            {yKey}
            vs
            {xKey}
            :
            <b>{value}</b>
          </>
        )}
      />
    </Box>
  );
}

export default HeatMapDuels;
