import React from 'react';
import {
  Box,
  Typography,
} from '@material-ui/core';
import { HeatMap } from '@nivo/heatmap';

function ArrayKills(values) {
  const { data } = values;
  const { keys } = values;

  return (
    <Box display="flex" flexDirection="column">
      <Box display="flex" justifyContent="center">
        <Typography variant="h4">
          Matrix kills
        </Typography>
      </Box>
      <HeatMap
        width={500}
        height={500}
        data={data}
        indexBy="killer"
        margin={{
          top: 40, right: 100, left: 100,
        }}
        keys={keys}
        forceSquare
        axisTop={{
          orient: 'top', tickSize: 5, tickPadding: 5, tickRotation: -90,
        }}
        axisRight={null}
        axisBottom={null}
        axisLeft={{
          orient: 'left',
          tickSize: 5,
          tickPadding: 5,
          tickRotation: 0,
        }}
        animate={false}
        tooltip={false}
        colors="RdYlGn"
      />
    </Box>
  );
}

export default ArrayKills;
