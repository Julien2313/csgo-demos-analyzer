import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import GoHome from '../buttons/goHome';
import GoProgress from '../buttons/goProgress';
import GoPattern from '../buttons/goPattern';
import GoProfile from '../buttons/goProfile';
import GoHeatMap from '../buttons/goHeatMap';
import GoLogin from '../buttons/goLogin';
import GoTokens from '../buttons/goTokens';
import GoUpload from '../buttons/goUpload';
import GoFAQ from '../buttons/goFAQ';
import Logout from '../user/logout';
import BackButton from '../buttons/backHistory';

function AppBarMenu(isAuth) {
  const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    toolbar: theme.mixins.toolbar,
    title: {
      flexGrow: 1,
    },
  }));

  const classes = useStyles();

  if (isAuth.isAuth) {
    return (
      <AppBar position="static">
        <Toolbar>
          <BackButton />
          <img height="50" width="50" alt="poulet" src={`${process.env.PUBLIC_URL}/img/julienlepoulet.png`} />
          <Typography variant="h6" className={classes.title}>
            CSGO Facts
          </Typography>
          <GoHome />
          <GoProfile />
          <GoHeatMap />
          <GoProgress />
          <GoPattern />
          <GoTokens />
          <GoUpload />
          <GoFAQ />
          <Logout />
        </Toolbar>
      </AppBar>
    );
  }

  return (
    <AppBar position="static">
      <Toolbar>
        <BackButton />
        <img height="50" width="50" alt="poulet" src={`${process.env.PUBLIC_URL}/img/julienlepoulet.png`} />
        <Typography variant="h6" className={classes.title}>
          CSGO Facts
        </Typography>
        <GoLogin />
        <GoFAQ />
      </Toolbar>
    </AppBar>
  );
}

export default AppBarMenu;
