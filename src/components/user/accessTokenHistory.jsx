import React from 'react';
import { useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';

import { toast } from 'react-toastify';

import apiUser from '../../api/user';

function AccessTokenHistory() {
  const history = useHistory();
  const { register, handleSubmit, errors } = useForm();

  const setAuthenticationCodeHitory = async (authenticationCodeHitoryForm) => {
    try {
      await apiUser.setAuthenticationCodeHitory(authenticationCodeHitoryForm.token);
      toast.success('access token changed', {
        position: 'top-center',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    } catch (err) {
      if (err.response === undefined) {
        toast.error('An error occurred', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        return;
      }
      if (err.response.status === 403 && err.response.data.title === 'ErrBadTokenConnection') {
        toast.info('You have been disconnected', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        history.push({
          pathname: '/',
        });
        return;
      }

      switch (err.response.status) {
        case 400:
          toast.error('Not a good access token', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          break;
        case 429:
          toast.error('Too many request !', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          break;
        default:
          toast.error('An error occurred', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          break;
      }
    }
  };

  return (
    <div>
      <form onSubmit={handleSubmit(setAuthenticationCodeHitory)}>
        <input name="token" placeholder="5JVP-AF9M0-P3DA" ref={register({ required: true })} />
        {errors.token && <span>This field is required</span>}

        <input type="submit" value="set authentication code history" />
      </form>
    </div>
  );
}

export default AccessTokenHistory;
