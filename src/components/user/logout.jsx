import React, { useContext } from 'react';

import { useHistory } from 'react-router-dom';
import { IconButton } from '@material-ui/core';
import Tooltip from '@material-ui/core/Tooltip';
import { useCookies } from 'react-cookie';

import ExitToAppIcon from '@material-ui/icons/ExitToApp';

import { toast } from 'react-toastify';

import { ContextAuth } from '../../context';
import apiUser from '../../api/user';

function Logout() {
  const history = useHistory();
  const { setIsAuth } = useContext(ContextAuth);
  const [, setCookies] = useCookies(['steamID']);

  const logout = async () => {
    try {
      await apiUser.logout();
      toast.info('Logged out', {
        position: 'top-center',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });

      setIsAuth(false);
      setCookies('steamID', undefined, { path: '/' });
      history.push('/');
    } catch (err) {
      if (err.response === undefined) {
        toast.error('An error occurred', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        return;
      }
      if (err.response.status === 403 && err.response.data.title === 'ErrBadTokenConnection') {
        toast.info('You have been disconnected', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        history.push({
          pathname: '/',
        });
        setIsAuth(false);
        return;
      }

      toast.error('An error occurred', {
        position: 'top-center',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  };

  return (
    <Tooltip
      title="Log Out"
      arrow
    >
      <IconButton
        color="inherit"
        onClick={logout}
      >
        <ExitToAppIcon />
      </IconButton>
    </Tooltip>
  );
}

export default Logout;
