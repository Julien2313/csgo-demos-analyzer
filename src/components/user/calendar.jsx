import React, { useEffect, useState } from 'react';

import {
  Box,
} from '@material-ui/core';

import CalendarHeatmap from 'react-calendar-heatmap';
import ReactTooltip from 'react-tooltip';
import 'react-calendar-heatmap/dist/styles.css';

import apiUser from '../../api/user';

function CalendarGames() {
  const [dataCalendar, setDataCalendar] = useState([{}]);

  useEffect(() => {
    const getAllDemosCalendar = async () => {
      try {
        const steamIDURL = window.location.pathname.split('/')[2];
        const response = await apiUser.getDateCalendar(steamIDURL);
        setDataCalendar(response.data.data);
      } catch (err) {
        console.log(err);
      }
    };

    getAllDemosCalendar();
  }, []);

  function shiftDate(date, numDays) {
    const newDate = new Date(date);
    newDate.setDate(newDate.getDate() + numDays);
    return newDate;
  }

  return (
    <Box display="flex" justifyContent="space-around">
      <CalendarHeatmap
        startDate={shiftDate(new Date(), -365)}
        endDate={new Date()}
        values={dataCalendar}
        classForValue={(value) => {
          if (!value) {
            return 'color-empty';
          }
          return `color-github-${value.count}`;
        }}
        tooltipDataAttrs={(value) => ({
          'data-tip': `${!value.date
            ? 'no game'
            : `${value.date} : ${value.count} games`}`,
        })}
        showWeekdayLabels
      />
      <ReactTooltip />
    </Box>
  );
}

export default CalendarGames;
