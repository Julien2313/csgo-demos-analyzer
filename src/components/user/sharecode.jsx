import React from 'react';
import { useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';

import { toast } from 'react-toastify';

import apiUser from '../../api/user';

function ShareCode() {
  const history = useHistory();
  const { register, handleSubmit, errors } = useForm();

  const setShareCode = async (shareCodeForm) => {
    try {
      await apiUser.setShareCode(shareCodeForm.shareCode);
      toast.success('sharecode changed', {
        position: 'top-center',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    } catch (err) {
      if (err.response === undefined) {
        toast.error('An error occurred', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        return;
      }
      if (err.response.status === 403 && err.response.data.title === 'ErrBadTokenConnection') {
        toast.info('You have been disconnected', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        history.push({
          pathname: '/',
        });
        return;
      }

      switch (err.response.status) {
        case 400:
          toast.error('Not a good sharecode', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          break;
        case 429:
          toast.error('Too many request !', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          break;
        default:
          toast.error('An error occurred', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          break;
      }
    }
  };

  return (
    <div>
      <form onSubmit={handleSubmit(setShareCode)}>
        <input name="shareCode" placeholder="CSGO-RKEnB-sxJzi-oZFqr-Z8TwC-cpEVF" ref={register({ required: true })} />
        {errors.shareCode && <span>This field is required</span>}

        <input type="submit" value="set sharecode" />
      </form>
    </div>
  );
}

export default ShareCode;
