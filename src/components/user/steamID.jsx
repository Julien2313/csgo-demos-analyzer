import React from 'react';
import { useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';

import { toast } from 'react-toastify';
import { useCookies } from 'react-cookie';

import apiUser from '../../api/user';

function SteamID() {
  const history = useHistory();
  const { register, handleSubmit, errors } = useForm();
  const [, setCookies] = useCookies(['steamID']);

  const setSteamID = async (steamIDForm) => {
    try {
      await apiUser.setSteamID(steamIDForm.ID);
      toast.success('steamID changed', {
        position: 'top-center',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      setCookies('steamID', steamIDForm.ID, { path: '/' });
    } catch (err) {
      if (err.response === undefined) {
        toast.error('An error occurred', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        return;
      }
      if (err.response.status === 403 && err.response.data.title === 'ErrBadTokenConnection') {
        toast.info('You have been disconnected', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        history.push({
          pathname: '/',
        });
        return;
      }

      switch (err.response.status) {
        case 400:
          toast.error('Not a good steamID', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          break;
        case 429:
          toast.error('Too many request !', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          break;
        default:
          toast.error('An error occurred', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          break;
      }
    }
  };

  return (
    <div>
      <form onSubmit={handleSubmit(setSteamID)}>
        <input name="ID" placeholder="76685423698547563" ref={register({ required: true })} />
        {errors.ID && <span>This field is required</span>}

        <input type="submit" value="set steamID" />
      </form>
    </div>
  );
}

export default SteamID;
