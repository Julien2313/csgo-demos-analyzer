import React from 'react';

import {
  Container,
} from '@material-ui/core';

import Tuto from './tuto';
import AccessTokenHistory from './accessTokenHistory';
import CheckTokens from './checkTokens';
import ShareCode from './sharecode';
import SteamID from './steamID';

function Profile() {
  return (
    <Container>
      <Tuto />
      <AccessTokenHistory />
      <ShareCode />
      <SteamID />
      <CheckTokens />
    </Container>
  );
}

export default Profile;
