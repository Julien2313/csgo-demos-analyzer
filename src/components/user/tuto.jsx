import React from 'react';

import picTuto1 from '../../media/tuto1.png';
import picTuto2 from '../../media/tuto2.png';


function Tuto() {
  return (
    <div>
      First of all, we need some informations about your steam account.
      You need to provide your authentication code, your sharecode and your steamID.
      <br />
      You can find the first 2
      {' '}
      <a href="https://help.steampowered.com/en/wizard/HelpWithGameIssue/?appid=730&issueid=128">here</a>
      . You will need to connect to your steam account and generate an authentication code
      in &apos;&apos;Access to Your Match History&apos;&apos;.
      <br />
      <img src={picTuto1} alt="first pic of tuto" />
      <br />
      You will see another token, just below
      &apos;Your most recently completed match token:&apos;. We need this one too.
      <br />
      <img src={picTuto2} alt="second pic of tuto" />
      <br />
      To find your SteamID You can use this website :
      {' '}
      <a href="https://support.ubisoft.com/en-GB/Faqs/000027522/Finding-your-Steam-ID">https://support.ubisoft.com/en-GB/Faqs/000027522/Finding-your-Steam-ID</a>
      .
      <br />
      Don&apos;t forget to click on the &apos;set...&apos; buttons !
      You will only need to do this one time.
    </div>
  );
}

export default Tuto;
