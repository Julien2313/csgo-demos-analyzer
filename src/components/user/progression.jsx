import React, { useContext } from 'react';
import { Scatter } from 'react-chartjs-2';
import moment from 'moment';

import Button from '@material-ui/core/Button';
import { toast } from 'react-toastify';

import apiUser from '../../api/user';
import { ContextDemos } from '../../context';

function Progression() {
  const { progression, setProgression } = useContext(ContextDemos);

  const getProgression = async () => {
    try {
      const response = await apiUser.getProgression();
      setProgression(response.data.data);
      toast.success('Progression retrieved', {
        position: 'top-center',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    } catch (err) {
      if (err.response === undefined) {
        toast.error('An error occurred', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        return;
      }
      if (err.response.status === 403 && err.response.data.title === 'ErrBadTokenConnection') {
        toast.info('You have been disconnected', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        history.push({
          pathname: '/',
        });
        return;
      }

      switch (err.response.status) {
        case 403:
          toast.error('You first have to give your steamID in your profile', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          break;
        case 429:
          toast.error('Too many request !', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          break;
        default:
          toast.error('An error occurred', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          break;
      }
    }
  };

  return (
    <div>
      <Button variant="contained" onClick={getProgression}>
        get Progression
      </Button>
      <div>
        <Scatter
          data={{
            datasets: [{
              label: 'Flashes score',
              data: progression.data,
              backgroundColor: '#f9c74f',
              borderColor: '#daae45',
              borderWidth: 3,
              pointBackgroundColor: '#f9c74f',
              pointHoverBorderColor: '#daae45',
              pointHoverBorderWidth: 3,
              pointHoverRadius: 8,
              pointRadius: 8,
            }],
          }}
          width={100}
          height={500}
          options={{
            maintainAspectRatio: false,
            scales: {
              xAxes: [{
                type: 'time',
                time: {
                  displayFormats: {
                    day: 'YY MMM DD',
                  },
                  // parse unix timestamp seconds
                  parser: (timestamp) => moment(timestamp, 'X'),
                  unit: 'day',
                  unitStepSize: 1,
                },
              }],
            },
            title: {
              display: true,
              text: 'Progression',
              fontSize: 20,
            },
            legend: {
              display: true,
              position: 'right',
            },
            tooltips: {
              callbacks: {
                label: (tooltipItem) => `${tooltipItem.xLabel} | ${Math.round(tooltipItem.yLabel * 1000) / 1000}`,
              },
            },
          }}
        />
      </div>
    </div>
  );
}

export default Progression;
