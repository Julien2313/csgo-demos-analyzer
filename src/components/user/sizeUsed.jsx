import React, { useState, useEffect } from 'react';

import Typography from '@material-ui/core/Typography';
import { toast } from 'react-toastify';
import apiUser from '../../api/user';

function SizeUsed() {
  const [sizeUsed, setSizeUsed] = useState(0);

  useEffect(() => {
    const getCurrentUser = async () => {
      try {
        const currentUser = await apiUser.me();
        setSizeUsed(currentUser.data.data.size_used);
      } catch (err) {
        if (err.response === undefined) {
          toast.error('An error occurred', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          return;
        }
        switch (err.response.status) {
          case 404:
            toast.error("This user doesn't exists", {
              position: 'top-center',
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
            return;
          case 429:
            toast.error('Too many request !', {
              position: 'top-center',
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
            break;
          case 400:
            setSizeUsed(0);
            return;
          default:
            toast.error('An error occurred', {
              position: 'top-center',
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
            break;
        }
      }
    };
    getCurrentUser();
  }, []);

  return (
    <Typography variant="caption" display="block" gutterBottom>
      {`Space used: ${sizeUsed.toFixed(2)}GB`}
    </Typography>
  );
}

export default SizeUsed;
