import React from 'react';
import {
  Box,
} from '@material-ui/core';


function Weapon(values) {
  const { weapon } = values;

  if (weapon === '') {
    return false;
  }

  return (
    <Box style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
      <img alt={weapon} src={`${process.env.PUBLIC_URL}/weapons/${weapon}.png`} />
    </Box>
  );
}
export default Weapon;
