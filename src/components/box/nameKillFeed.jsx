import React from 'react';
import {
  Box,
} from '@material-ui/core';

function NameKillFeed(values) {
  const { name, color } = values;

  return (
    <Box
      fontFamily="Rajdhani"
      m={1}
      color={color}
      style={{
        lineHeight: 0.8,
        fontSize: '1.0rem',
        letterSpacing: '0.0em',
      }}
    >
      {name}
    </Box>
  );
}
export default NameKillFeed;
