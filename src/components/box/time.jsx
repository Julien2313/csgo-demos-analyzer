import React from 'react';
import {
  Box,
} from '@material-ui/core';


function Time(values) {
  const { time } = values;

  return (
    <Box
      fontFamily="Rajdhani"
      m={1}
      weight="55px"
      style={{
        lineHeight: 0.8,
        fontSize: '1.0rem',
        letterSpacing: '0.0em',
      }}
    >
      {time}
    </Box>
  );
}
export default Time;
