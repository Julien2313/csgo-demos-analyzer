import React from 'react';

import { useCookies } from 'react-cookie';
import { useHistory } from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import MapIcon from '@material-ui/icons/Map';


function GoHeatMap() {
  const history = useHistory();
  const [cookies] = useCookies(['steamID']);
  return (
    <Tooltip
      title="Heat maps"
      arrow
    >
      <IconButton
        color="inherit"
        onClick={() => history.push(`/heatmaps/${cookies.steamID}`)}
      >
        <MapIcon />
      </IconButton>
    </Tooltip>
  );
}

export default GoHeatMap;
