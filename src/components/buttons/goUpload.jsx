import React from 'react';

import { useHistory } from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import PublishIcon from '@material-ui/icons/Publish';

function GoUpload() {
  const history = useHistory();
  return (
    <Tooltip
      title="Upload"
      arrow
    >
      <IconButton
        color="inherit"
        onClick={() => history.push({
          pathname: '/upload',
        })}
      >
        <PublishIcon />
      </IconButton>
    </Tooltip>
  );
}

export default GoUpload;
