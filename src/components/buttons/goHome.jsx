import React from 'react';

import { useCookies } from 'react-cookie';
import { useHistory } from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import HomeIcon from '@material-ui/icons/Home';

function GoHome() {
  const history = useHistory();
  const [cookies] = useCookies(['steamID']);
  return (
    <Tooltip
      title="Home"
      arrow
    >
      <IconButton
        color="inherit"
        onClick={() => history.push(`/home/${cookies.steamID}`)}
      >
        <HomeIcon />
      </IconButton>
    </Tooltip>
  );
}

export default GoHome;
