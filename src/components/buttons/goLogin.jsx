import React from 'react';

import { useHistory } from 'react-router-dom';
import LockIcon from '@material-ui/icons/Lock';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';

function GoProgress() {
  const history = useHistory();
  return (
    <Tooltip
      title="Log In"
      arrow
    >
      <IconButton
        color="inherit"
        onClick={() => history.push({
          pathname: '/',
        })}
      >
        <LockIcon />
      </IconButton>
    </Tooltip>
  );
}

export default GoProgress;
