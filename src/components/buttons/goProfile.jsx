import React from 'react';

import { useHistory } from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';


function GoProfile() {
  const history = useHistory();
  return (
    <Tooltip
      title="My Profile"
      arrow
    >
      <IconButton
        color="inherit"
        onClick={() => history.push({
          pathname: '/profile',
        })}
      >
        <AccountCircle />
      </IconButton>
    </Tooltip>
  );
}

export default GoProfile;
