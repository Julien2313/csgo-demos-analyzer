import React from 'react';

import { useCookies } from 'react-cookie';
import { useHistory } from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';

function GoPattern() {
  const history = useHistory();
  const [cookies] = useCookies(['steamID']);

  return (
    <Tooltip
      title="My Patterns"
      arrow
    >
      <IconButton
        color="inherit"
        onClick={() => history.push(`/patterns/${cookies.steamID}`)}
      >
        <img alt="" src={`${process.env.PUBLIC_URL}/utils/gun-pointer.png`} />
      </IconButton>
    </Tooltip>
  );
}

export default GoPattern;
