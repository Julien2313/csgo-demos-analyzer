import React from 'react';

import { useHistory } from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';

function GoProgress() {
  const history = useHistory();
  return (
    <Tooltip
      title="My Progression"
      arrow
    >
      <IconButton
        color="inherit"
        onClick={() => history.push({
          pathname: '/progress',
        })}
      >
        <TrendingUpIcon />
      </IconButton>
    </Tooltip>
  );
}

export default GoProgress;
