import React from 'react';

import { useHistory } from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import LiveHelpIcon from '@material-ui/icons/LiveHelp';


function GoFAQ() {
  const history = useHistory();
  return (
    <Tooltip
      title="FAQ"
      arrow
    >
      <IconButton
        color="inherit"
        onClick={() => history.push({
          pathname: '/FAQ',
        })}
      >
        <LiveHelpIcon />
      </IconButton>
    </Tooltip>
  );
}

export default GoFAQ;
