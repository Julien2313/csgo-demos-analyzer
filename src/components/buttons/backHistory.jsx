import React from 'react';

import { useHistory } from 'react-router-dom';
import Button from '@material-ui/core/Button';

import ArrowBackIcon from '@material-ui/icons/ArrowBack';

function BackButton() {
  const history = useHistory();
  return (
    <>
      <Button
        variant="contained"
        color="primary"
        onClick={() => history.goBack()}
        size="large"
      >
        <ArrowBackIcon />
      </Button>
    </>
  );
}

export default BackButton;
