import React from 'react';

import { useHistory } from 'react-router-dom';
import { IconButton } from '@material-ui/core';
import {
  CloudDownload as CloudDownloadIcon,
} from '@material-ui/icons';

import { toast } from 'react-toastify';

import apiDemo from '../../api/demo';

function CheckNewDemo(value) {
  const { setIsNewDemo } = value;
  const history = useHistory();
  const checkNewDemo = async () => {
    try {
      const response = await apiDemo.checkNewDemo();
      const isNewDemo = response.data.data;
      if (isNewDemo) {
        toast.info('At least one new demo has been found and is beeing analyzed', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        setIsNewDemo(isNewDemo);
      } else {
        toast.info('No new demo found', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    } catch (err) {
      if (err.response === undefined) {
        toast.error('An error occurred', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        return;
      }
      if (err.response.status === 403 && err.response.data.title === 'ErrBadTokenConnection') {
        toast.info('You have been disconnected', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        history.push({
          pathname: '/',
        });
        return;
      }

      switch (err.response.status) {
        case 403:
          toast.info('Your must set your token in your profile before using this button', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          return;
        case 422:
          toast.info('Your tokens might not be valids, please recheck them in your profile', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          return;
        case 429:
          toast.error('Too many request !', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          break;
        default:
          toast.error('An error occurred', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          break;
      }
    }
  };

  return (
    <IconButton onClick={checkNewDemo}>
      <CloudDownloadIcon />
    </IconButton>
  );
}

export default CheckNewDemo;
