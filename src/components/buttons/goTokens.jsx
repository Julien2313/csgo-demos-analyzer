import React from 'react';

import { useHistory } from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import VpnKeyIcon from '@material-ui/icons/VpnKey';

function GoFAQ() {
  const history = useHistory();
  return (
    <Tooltip
      title="Tokens"
      arrow
    >
      <IconButton
        color="inherit"
        onClick={() => history.push({
          pathname: '/tokens',
        })}
      >
        <VpnKeyIcon />
      </IconButton>
    </Tooltip>
  );
}

export default GoFAQ;
