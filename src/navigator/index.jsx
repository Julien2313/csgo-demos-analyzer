/* eslint-disable object-property-newline */
import React, { useEffect, useState } from 'react';
import {
  BrowserRouter, Switch, Route, Redirect,
} from 'react-router-dom';
import { CookiesProvider, useCookies } from 'react-cookie';

import Landingpage from '../scenes/landingpage';
import Home from '../scenes/home';
import HeatMap from '../scenes/heatmaps';
import FAQ from '../scenes/FAQ';
import Tokens from '../scenes/tokens';
import Upload from '../scenes/upload';

import Stats from '../components/demo/stats';
import Profile from '../components/user/profile';
import Progression from '../scenes/progression';
import Patterns from '../scenes/patterns';
import AppBarMenu from '../components/bar/appBarMenu';

import { ContextDemos, ContextAuth } from '../context';
import apiUser from '../api/user';

import 'react-toastify/dist/ReactToastify.css';

export default function () {
  const [isAuth, setIsAuth] = useState(true);
  const [demosDisplay, setDemosDisplay] = useState({ demos: [], isGettingDemos: false });
  const [progression, setProgression] = useState([]);
  const [limit, setLimit] = useState(10);
  const [offset, setOffset] = useState(0);
  const [selectedDateStart, setSelectedDateStart] = useState(null);
  const [selectedDateEnd, setSelectedDateEnd] = useState(null);
  const [, setCookies] = useCookies(['steamID']);

  const checkAuth = async () => {
    try {
      const response = await apiUser.checkAuth();
      setCookies('steamID', response.data.data, { path: '/' });
      setIsAuth(true);
    } catch (err) {
      setIsAuth(false);
    }
  };

  function PrivateRoute({ children }) {
    return (
      <Route
        render={() => (isAuth ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: '/',
            }}
          />
        ))}
      />
    );
  }

  useEffect(() => {
    checkAuth();
  }, []);

  return (
    <CookiesProvider>
      <ContextDemos.Provider value={{
        demosDisplay, setDemosDisplay,
        progression, setProgression,
        limit, setLimit,
        offset, setOffset,
        selectedDateStart, setSelectedDateStart,
        selectedDateEnd, setSelectedDateEnd,
      }}
      >
        <ContextAuth.Provider value={{
          isAuth, setIsAuth,
        }}
        >
          <BrowserRouter>
            <AppBarMenu isAuth={isAuth} />
            <Switch>
              <PrivateRoute path="/profile">
                <Profile />
              </PrivateRoute>
              <Route exact path="/">
                <Landingpage />
              </Route>
              <PrivateRoute path="/progress">
                <Progression />
              </PrivateRoute>
              <Route path="/patterns">
                <Patterns />
              </Route>
              <PrivateRoute exact path="/home">
                <Home />
              </PrivateRoute>
              <PrivateRoute exact path="/upload">
                <Upload />
              </PrivateRoute>
              <PrivateRoute exact path="/tokens">
                <Tokens />
              </PrivateRoute>
              <Route path="/home/">
                <Home />
              </Route>
              <Route path="/heatmaps/">
                <HeatMap />
              </Route>
              <Route path="/FAQ">
                <FAQ />
              </Route>
              <Route path="/demo/stats">
                <Stats />
              </Route>
            </Switch>
          </BrowserRouter>
        </ContextAuth.Provider>
      </ContextDemos.Provider>
    </CookiesProvider>
  );
}
