package main

import (
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/server"
	"gitlab.com/Julien2313/CSGO-demos-analyzer/backend/store"
)

func main() {
	s := &server.Server{SchemaDB: store.SchemaProd}
	s.Start("./.env")

	defer func() {
		s.Shutdown()
	}()

	quit := make(chan os.Signal, 10)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
}
