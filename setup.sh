docker exec -ti csgo-demos-analyzer_db_1 pg_dumpall -c -U csgo | gzip > dump.sql
docker exec -ti csgo-demos-analyzer_db_1 psql -U csgo "GRANT CREATE ON DATABASE csgo TO csgo;"

DIR="/home/csgo/"
PATH=$PATH:/usr/local/go/bin
REACT_APP_BASEURL="/api/v1"

git pull

go build -o ./server.exe .
sudo mv ./server.exe $DIR/server.exe

npm run-script build
sudo -E rm -rf $DIR/build
sudo -E mv build $DIR

sudo systemctl restart csgoserver.service
