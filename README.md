# CSGO-demos-analyzer

## Docker

```shell
sudo docker build -t julien2313/csgo-analyzer
sudo docker login docker.io
sudo docker push julien2313/csgo-analyzer
```

### Golang

```shell
go version
```
Version should be at least 1.13.5

### Yarn
https://nodejs.org/en/download/package-manager/
TODO
```shell
node -v
npm -v
```

## HTTPS

Dans le dossier du projet, exécutez : 
```shell
mkdir tls
openssl genrsa -out ./tls/server.key 2048
openssl req -new -x509 -key ./tls/server.key -out ./tls/server.pem -days 365
```

## Protobuf

Proto 2.5.0 (because of Boiler :/)
https://developers.google.com/protocol-buffers/docs/gotutorial
```shell
cd /path/to/$GOPATH/or/CSGO-demo-analyzer
sudo apt-get install g++ make automake autoconf libtool
wget https://github.com/google/protobuf/releases/download/v2.5.0/protobuf-2.5.0.tar.gz
tar xvf protobuf-2.5.0.tar.gz
cd protobuf-2.5.0
./autogen.sh
./configure --prefix=/usr
make
sudo make install
protoc --version
go install google.golang.org/protobuf/cmd/protoc-gen-go
sudo mv ~/go/bin/protoc-gen-go /usr/local/go/bin/
```

Dans le dossier ./backend/model
`protoc --go_out=. *.proto`

### Start servers

```shell
go run .
npm start
```
## Redirection

```shell
sudo apt-get install nginx
sudo cp ./setupNGINX/default /etc/nginx/sites-enable/default
sudo nginx -s reload
```

## Installation docker and docker-compose
Install [docker CE](https://docs.docker.com/install/) and [docker-compose](https://docs.docker.com/compose/install/)

On Linux follow [those steps](https://docs.docker.com/install/linux/linux-postinstall/) 
to use docker without beeing root

To check the installation :
```shell
docker -v
docker run hello-world
```

## X2Go
As steam need to be started for boiler, we need a graphical interface in our server.
https://wiki.x2go.org/doku.php/doc:installation:x2goserver

sudo apt-get install x2goclient

## Service

Créeer un fichier comme ceci :
```
[Unit]
Description=Serveur CSGO

[Service]
ExecStartPre=/bin/sleep 30
ExecStart=/home/csgo/server.exe
Restart=always
RestartSec=10

User=csgo

WorkingDirectory=/home/csgo

[Install]
WantedBy=multi-user.target
```

Enregistrer ce fichier ici : `/etc/systemd/system/csgoserver.service`
```
[Unit]
Description=Serveur CSGO

[Service]
ExecStartPre=/bin/sleep 30
ExecStart=/home/csgo/dev/server.exe
Restart=always
RestartSec=10

User=csgo

WorkingDirectory=/home/csgo/dev

[Install]
WantedBy=multi-user.target
```

Enregistrer ce fichier ici : `/etc/systemd/system/csgoserverdev.service`


```shell
sudo systemctl enable csgoserver.service
sudo systemctl enable csgoserverdev.service
```

## Back up and test
```shell
docker exec -ti csgo-demos-analyzer_db_1 pg_dumpall -c -U csgo | gzip > csgo$DATE.sql.gz
scp -p $IP:$PATH/csgo$DATE.sql .

# need to empty databse
sudo gunzip -c csgo$DATE.sql.gz | sudo docker exec -i csgo-demo-analyzer_db_1 psql -U csgo
```